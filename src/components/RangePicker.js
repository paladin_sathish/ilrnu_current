import React, { Component } from 'react';
import {StyleSheet,View,Text} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';
import color from '../Helpers/color';
import dateFormat from 'dateformat';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Toast from 'react-native-simple-toast';
import {Right,Top,Icon, Button,Content,Container} from 'native-base'
Array.prototype.diff = function(arr2) {
    var ret = [];
    for(var i in this) {   
        if(arr2.indexOf(this[i]) > -1){
            ret.push(this[i]);
        }
    }
    return ret;
};
Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}
function getDiffDates(date1,date2){
  var d1 = date1;
  var d2 = date2;
  var timeDiff = d2.getTime() - d1.getTime();
  var DaysDiff = timeDiff / (1000 * 3600 * 24);
  return DaysDiff;
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(dateFormat(currentDate,'yyyy-mm-dd'));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}
class RangePicker extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {visible:false,selectedEndDate:null,selectedStartDate:null};
	}

onDateChange=(date, type)=> {
  console.log('shidid',date); 
   console.log("shidid", type);
    if (type === 'END_DATE') {
      // console.log("enddate",new Date(date));
      this.setState({
        selectedEndDate: new Date(date),
      },function(){
      this.checkvalidates(new Date(this.state.selectedStartDate),new Date(date));
      });
    } else {
      console.log("startdate",new Date(date));
      this.setState({
        selectedStartDate: new Date(date),
        selectedEndDate: null,
      },function(){
          
      });
    } 

   
  }
  checkvalidates= async (date1,date2)=>{
    var getthedates=await getDates(date1,date2);
    var blockedDates=this.props.blockDates.diff(getthedates);
    console.log(blockedDates.length);
    if(blockedDates.length==0){
      if(this.props.type!=2){
        var getdiffernceofDates=getDiffDates(date1,date2);
        if(getdiffernceofDates==0){
            this.calenderPickerRef.resetSelections();
         this.clearDates();
         Toast.show('this is a '+(this.renderRange(this.props.type)==6?'weekly':'monthly')+' venue you have to choose atleast '+parseInt(parseInt(this.renderRange(this.props.type))+1) +" days", Toast.LONG);
        }else{

        }
      }else{ 
        this.setState({ selectedEndDate: date2 }, () => this.submitDates());
      }
    }else{
      this.calenderPickerRef.resetSelections();
        this.clearDates();
         Toast.show('Dates in between already booked please choose some other dates', Toast.LONG);
    }
  }
  renderRange=(type)=>{
    if(type){
      if(type==3){
        return 6;
      }else if(type==4){
        return 29;
      }
    }
  }
  clearDates=()=>{
    this.setState({selectedStartDate:null,selectedEndDate:null});
  }
  submitDates=()=>{
    if(this.state.selectedStartDate==null|| this.state.selectedEndDate==null){
      Toast.show("From Date or To Date is Empty",Toast.LONG)

    }else{
      console.log('oksubmitrange',this.state);
      var obj={fromTime:this.state.selectedStartDate,toTime:this.state.selectedEndDate};
      this.props.selectedDate&&this.props.selectedDate(obj);
      this.props.closeRangePicker&&this.props.closeRangePicker();
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: "white" }}>
        <CalendarPicker
          ref={el => (this.calenderPickerRef = el)}
          minRangeDuration={this.renderRange(this.props.type)}
          allowRangeSelection={true}
          // maxRangeDuration={(this.renderRange(this.props.type)+1)*(4)}
          disabledDates={
            this.props.blockDates.length > 0 ? this.props.blockDates : []
          }
          startFromMonday={true}
          // allowRangeSelection={this.props.type ? true : false}
          minDate={this.props.minDate ? this.props.minDate : new Date()}
          maxDate={this.props.maxDate ? this.props.maxDate : new Date()}
          todayBackgroundColor="#f2e6ff"
          selectedDayColor="#7300e6"
          selectedDayTextColor="#FFFFFF"
          onDateChange={this.onDateChange}
          onMonthChange={this.props.monthChange}
        />
      </View>
    );
  }
  componentDidMount(){
    var self=this;
  }
}

const styles = StyleSheet.create({
    blue_btn:{
        // backgroundColor:color.blue,
        // height:hp('3%'),
        // paddingLeft:8,
        // paddingRight:8,
      
        // paddingBottom:5,
        // alignItems:'center'
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.blue,
        height:hp('3%'),
        borderRadius:3,
        paddingTop:8,
        paddingBottom:10

    },
    btn_txt:{
        color:color.white,
        fontSize:hp('1.5%'),


    },
});


export default RangePicker;