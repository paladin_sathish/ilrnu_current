import React,{Component} from 'react'
import {View,StyleSheet,Text,ScrollView,FlatList} from 'react-native'
import color from '../Helpers/color';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class orange_square extends Component{
    render(){
        return(
            <View style={styles.container} >

                <ScrollView horizontal={true}
                   showsHorizontalScrollIndicator={false}>

                {this.props.fav_List&&this.props.fav_List.map((obj,key)=>
                {
				    return(
          
                    <View style={styles.square}>
                        <Text style={styles.text_style}>
                                {obj.fav}
                        </Text>
                    </View>
            
			            )
			    })
			    }

                </ScrollView>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    square:{
        margin:5,
        height:hp('8%'),
        width:hp('10%'),
        borderRadius:5,
        backgroundColor:color.orange,
        justifyContent:'center',
            
    },
    text_style:{
        color:color.white,
        textAlign:"center",
        fontSize:hp('1.6%'),
    }
})