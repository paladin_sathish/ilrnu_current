import React, { Component } from "react";

import { StyleSheet, View, Text } from "react-native";
import LabelTextbox from "../components/labeltextbox";

class DynamicForm extends Component {
  constructor(props) {
    super(props);

    this.state = { facilityData: [], commonArray: [] };
  }
  componentWillReceiveProps(props) {
    // alert(JSON.stringify(props));
    if (props.facilityData) {
      this.setState({ facilityData: props.facilityData });
    }
    if (props.commonArray) {
      this.setState({ commonArray: props.commonArray });
    }
  }
  render() {
    // alert(JSON.stringify(this.state.facilityData));
    return (
      <View>
		  
        {this.state.commonArray &&
          this.state.commonArray.map((obj, key) => {
            return (
              <LabelTextbox
                styleobj={obj}
                error={obj.error}
                errormsg={obj.errormsg}
                changeText={(data, data2) =>
                  this.props.changeText &&
                  this.props.changeText(data, data2, obj, key)
                }
                type={obj.spec_det_datatype1}
                value={obj.spec_det_datavalue1}
                capitalize={obj.nostringformat ? false : true}
                labelname={obj.spec_det_name}
                inputtype={
                  obj.spec_det_datatype1 == "number" ? "numeric" : null
                }
              />
            );
          })}

        {this.state.facilityData &&
          this.state.facilityData.map((obj, key) => {
            return (
              <LabelTextbox
                inputtype={
                  obj.spec_det_datatype1 == "number" ? "numeric" : null
                }
                changeText={(data, data2) =>
                  this.props.changeText &&
                  this.props.changeText(data, data2, obj, key)
                }
                type={obj.spec_det_datatype1}
                value={obj.spec_det_datavalue1}
                capitalize={true}
                labelname={obj.spec_det_name}
              />
            );
          })}
      </View>
    );
  }

  componentDidMount() {
    if (this.props.facilityData) {
      this.setState({ facilityData: this.props.facilityData });
    }
    if (this.props.commonArray) {
      this.setState({ commonArray: this.props.commonArray });
    }
  }
}

const styles = StyleSheet.create({});

export default DynamicForm;
