"use strict";

import React, { Component } from "react";
import { StyleSheet, View, Image, Text,TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import placeholder_img from "../images/placeholder_img.png";
import StarRating from "react-native-star-rating";
import color from "../Helpers/color";

class CourseCard extends Component { 


  onBook=(data)=>{
    this.props.onBook(data)
  }
  render() {
    console.log('called',this.props.item)
    var item = this.props.item;
    return (
      <View > 
        <TouchableOpacity onPress={()=>this.onBook(item)}>
        {this.props.star && (
          <StarRating
            // disabled={false}
            maxStars={5}
            rating={4}
            selectedStar={rating => null}
            halfStarColor={color.blue}
            fullStarColor={color.blue}
            starSize={8}
            containerStyle={styles.RatingBar}
            starStyle={styles._starstyle}
            emptyStarColor={color.ash6}
          />
        )}
      
       
        {item && item.uploadData.length > 0 ? (
          <Image
            style={styles.image_style}
            source={{
              uri: item.uploadData[0].trainer_upload_path
            }}
          />
        ) : (
          <Image style={styles.image_style} source={placeholder_img} />
        )}

        {/* )} */}
        {item.visible == true && (
          <View
            style={{
              position: "absolute",
              bottom: 0,
              top: hp("4%"),
              alignItems: "center",
              height: "100%",
              left: hp("1%"),
              zIndex: 1
            }}
          ></View>
        )}
        <Text numberOfLines={1} style={[styles.h3,{color:color.blueactive}]}>
          {item.course_title}
        </Text>
        <Text style={styles.text_style2}>{item.course_desc}</Text>

        
           </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image_style: {
    height: hp("10%"),
    width: hp("15%"),
    borderRadius: 5,
    justifyContent: "center"
  },
  text_style2: {
    //  textAlign:"center",
    fontSize: hp("1.6%"),
    paddingLeft: hp("0.45%"),
    paddingTop: hp("0.45%")
  },
  RatingBar: {
    height: hp("2.5%"),
    width: wp("13%")
  },
  _starstyle: {
    fontSize: 12,
    marginRight: 3
  },
  h3: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  }
});

export default CourseCard;
