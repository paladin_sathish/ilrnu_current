import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Alert,StatusBar,Modal,Image,TouchableHighlight,ToastAndroid,TouchableOpacity,ImageBackground} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input} from 'native-base';
import color from '../Helpers/color';
import { Actions,Router,Scene } from 'react-native-router-flux';
import ModalComp from '../components/ModalComp';
import Toast from 'react-native-simple-toast';

export default class SetPassword extends React.Component{
	constructor(props){
		super(props);
		this.state={pass:'',cpass:'',disabled:false}
	}
	loaddisclaimer=()=>{
		// Actions.signup();
		// var obj={'type':'disclaimer'};
		var obj=this.props.signupdata;
		if(this.props.forgot==false){
		obj.pass=this.state.pass;
		obj.type='disclaimer';
	}
			// alert(JSON.stringify(obj));
			if(this.state.pass==""||this.state.cpass==""){
				this.setState({disabled:false})
				 Toast.show('Field is Empty', Toast.LONG);
				
			}else{
				if(this.state.pass!=this.state.cpass){
					this.setState({disabled:false})
					Toast.show('Password & Confirm Password are not same', Toast.LONG);
				}else{
					  var passwordPTN = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,20}$/;
					  var testingpassword=this.state.pass.match(passwordPTN);
					  if(!testingpassword){
					  	Toast.show('Please follow the instruction below to set the password',Toast.LONG);
					  	return;
					  }
					if(this.props.forgot==false){
					if(this.props.loaddisclaimer){
						this.setState({disabled:true})
			this.props.loaddisclaimer(obj);
		}	
		}else{
			this.props.resetPassword(this.state.pass);
		}		
				}
			}
		
	}
	passwordData=(data,key)=>{
		this.setState({[key]:data});
	}
render(){
	return(
		<Content>
		<View style={{alignItems:'center'}}>
		{/*<View style={styles.circle}>
			<Image source={require('../images/venue.jpg')} style={[styles.innercircle]}>
			</Image>
		</View>*/}
		<View style={styles.loginheader}>
		<Text style={styles.smalltitle}>Welcome</Text>
		<Text style={styles.logintitle}>{this.props.forgot==false?this.props.signupdata.name:this.props.forgotdata.user_name}</Text>
		{this.props.forgot==false&&
		<Text style={styles.loginsubtitle}>{this.props.loginttype?this.props.loginttype:'Book your Venue'}</Text>
	}{this.props.forgot==true&&
		<Text style={styles.loginsubtitle}>Reset Your Password</Text>
	}
		</View>
	<View style={{width:'100%',padding:12}}>
          <Item regular style={[styles.texboxpadding,styles.itemsize]}>
            <Input style={styles.textboxsize} placeholderTextColor={color.ash3}  secureTextEntry placeholder='Enter Password' onChangeText={(data)=>this.passwordData(data,'pass')} />
          </Item>
          <Item regular style={[styles.texboxpadding1,styles.itemsize]}>
            <Input style={styles.textboxsize} placeholderTextColor={color.ash3}  secureTextEntry placeholder='Re-enter Password' onChangeText={(data)=>this.passwordData(data,'cpass')}  />
          </Item>
     </View>
     <View style={styles.submitbox}>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button disabled={this.state.disabled} onPress={()=>this.loaddisclaimer()} style={[styles.actionbtn,{backgroundColor:this.state.disabled?color.ash:color.blue}]}>
           <Text disabled={this.state.disabled} style={[styles.actionbtntxt,{color:this.state.disabled?color.black:color.white}]}>CONFIRM</Text>
          </Button>
          </View>
     </View>
     <View style={styles.sociallogin}>
	<Text style={styles.footerheading}>Password Specifications</Text>
	<Text style={styles.footersubtext}>Password Should have Alphabet, Numbers, Special Characters</Text>
	{/*<Text style={styles.footersubtext}>Other Specification here</Text>*/}
     </View>
		</View>
     </Content>
		)
}

}
const styles={

	circle:{
		width:hp('17%'),
		height:hp('17%'),
		borderRadius:hp('17%')/2,
		marginTop:'5%',
		borderWidth:1,
		alignItems:'center',
		justifyContent:'center',
		borderColor:color.orange,
	},
	innercircle:{
		position:'absolute',
		width:hp('15.5%'),
		height:hp('15.5%'),
		borderRadius:hp('15.5%')/2,
		margin:'auto',
		backgroundColor:color.ash,
		margin:0,
	},
	loginheader:{
		marginTop:hp('2%'),
		alignItems:'center'
	},
	smalltitle:{
fontSize:hp('2%'),
		color:color.black1
	},
	logintitle:{
		fontSize:hp('3%'),
		color:color.orange
	},
	loginsubtitle:{
		fontSize:hp('2.3%'),
		color:color.black,
	},
	texboxpadding:{
		marginBottom:24,
	},
	texboxpadding1:{
		marginBottom:10,
	},
	submitbox:{
		flex:1,
		flexDirection:'row',
		paddingLeft:12,
		paddingRight:12,
		paddingBottom:15,
		borderBottomWidth:0.5
	},
	actionbtn:{
		borderRadius:5,
		width:hp('19%'),
		backgroundColor:color.orangeBtn,
		justifyContent:'center',
	},
	actionbtntxt:{
		textAlign:'center',
		// color:color.white,
		fontSize:hp('2.3%')
	},
	sociallogin:{
		backgroundColor:color.ash1,
		height:hp('24%'),
		width:'100%',
		alignItems:'flex-start',
		padding:18,
		// justifyContent:'space-between'
	},
	socialiconlogin:{
		marginTop:10,
		flexDirection:'row'
	},
	socialicon:{
		width:hp('4%'),
		height:hp('4%'),
		margin:5
	},
	footerheading:{
		fontSize:hp('3%'),
		fontWeight:'bold',
	},
	footersubtext:{
		fontSize:hp('2.2%'),
		color:color.ash3
	},
	textboxsize:{
		fontSize:hp('2.2%')
	},
	itemsize:{
		height:hp('5.5%')
	}
}