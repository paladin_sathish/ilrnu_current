import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator
} from 'react-native';
import ModalComp from './Modal';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class LoadingOverlay extends Component {
  render() {
    return (
       <ModalComp center noClose={true} visible={true}>
                  <View style={{marginTop:60,padding:12}}>
       <Text style={{textAlign:'center',fontSize:hp('2.5%'),paddingBottom:12}}>Please Wait...</Text>
  <ActivityIndicator size="large" color="#0000ff" />
  </View>
                  </ModalComp>
    );
  }
}

const styles = StyleSheet.create({

});


export default LoadingOverlay;