import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList,} from 'react-native';
import {Right,Top,Icon, Button,ListItem,} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import Search_box from './searchbox'
import Text_box from './text_box'
import Location from '../images/location.png'
import search from '../images/Search.png'
import Check_Box from './checkbox'

export default class Search_your_venue_form extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={{flex:.8}}>    
                    <Text style={styles.text1}>Search your Venue</Text>
                    <View style={styles.view1}></View>
                    <Search_box placeHolder={''}/> 
                    <View style={styles.view2}>
                        <View style={[styles.view3,{flexDirection:'row'}]}>
                            
                        {/* <ListItem>
                            <CheckBox  />
                            <Text style={styles.blue_txt}>Near Me</Text>
                        </ListItem> */}
                           <Check_Box label={'Near Me'} active={true} ></Check_Box>
                            {/* <Text style={styles.blue_txt}>Near Me</Text> */}
                        </View>
                        <View style={styles.view4}>
                            <Text style={styles.blue_txt}>Advance Search</Text>
                        </View>
                    </View>  
                    <View style={styles.view5}>
                        <View style={styles.flex_row}>
                            <View style={styles.view6}>
                                <Text>Where</Text>
                            </View>
                            <View style={styles.view7}>
                                <Text_box></Text_box>
                            </View>
                            <View style={styles.view8}>
                                <Image source={Location} style={styles.image_style1}></Image>
                            </View>
                        </View>
                        <View style={[styles.flex_row,styles.view1]}>
                            <View style={styles.view6}>
                                <Text>When</Text>
                            </View>
                            <View style={{flex:.35}}>
                                <Text_box></Text_box>
                            </View>
                            <View style={{flex:.3}}>
                                
                            </View>
                        </View>
                        <View style={[styles.flex_row,styles.view1]}>
                            <View style={styles.view6}>
                                <Text>Price Range</Text>
                            </View>
                            <View style={{flex:.35}}>
                                <Text_box></Text_box>
                            </View>
                            <View style={{flex:.3}}>
                                
                            </View>
                        </View>
                        <View style={[styles.flex_row,styles.view1]}>
                            <View style={styles.view6}>
                                <Text>Amenities</Text>
                            </View>
                            <View style={styles.view7}>
                                <Text_box></Text_box>
                            </View>
                            <View style={styles.view8}>
                                <Image source={search} style={styles.image_style1}></Image>
                            </View>
                        </View>
                        <View style={[styles.flex_row,styles.view1]}>
                            <View style={styles.view6}>
                                <Text>capacity</Text>
                            </View>
                            <View style={{flex:.3}}>
                                <Text_box></Text_box>
                            </View>
                            <View style={{flex:.35}}>
                                
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{flex:.2}}>
                <View style={styles.view11}>
                    <View style={styles.view12}>
                        <Button style={styles.cancel_btn}>
                            <Text style={styles.cancel_txt}> CANCEL </Text>
                        </Button>
                    </View>
                    <View style={styles.view13}>
                        <Button style={styles.book_nw_btn}>
                            <Text style={styles.cancel_txt}> SEARCH </Text>
                        </Button>
                    </View>
                </View>
                </View>    
            </View>
        )
    }
} 
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white,
        flex:1,
        padding:hp('2%')
    },
    text1:{
        color:color.top_red,
        fontSize:hp('2%')
    },
    blue_txt:{
        color:color.blue,
        fontSize:hp('1.4')
    },
    view1:{
        marginTop:hp('2%')
    },
    view2:{
        flexDirection:'row',
        paddingTop:3,
        paddingBottom:3
    },
    view3:{
        flex:1,
        justifyContent:'flex-start'
    },
    view4:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end'
    },
    view5:{
        marginTop:hp('2%')
    },
    flex_row:{
        flexDirection:'row'
    },
    view6:{
        flex:.35
    },
    view7:{
        flex:.55
    },
    view8:{
        flex:.1
    },
    image_style1:{
        padding: 5,
        margin: 5,
        height: hp('2%'),
        width: hp('2%'),
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    view11:{
        flexDirection:'row',
        paddingTop:15,
        paddingBottom:15
    },
    view12:{
        flex:1,
    },
    view13:{
        flex:1,
        justifyContent:'flex-end',
        flexDirection:'row'
    },
    cancel_txt:{
        fontSize:hp('2%'),
        color:color.white
    },
    cancel_btn:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.orange,
        height:hp('5%')
    },
    book_nw_btn:{
        backgroundColor:color.blue,
        paddingLeft:8,
        paddingRight:8,
        height:hp('5%')
    }

})