import React, { Component } from "react";
import {
  Modal,
  Text,
  TouchableHighlight,
  View,
  Alert,
  PermissionsAndroid,
  StyleSheet,
  Dimensions
} from "react-native";
import Geolocation from "react-native-geolocation-service";
import MapView, {
  Marker,
  AnimatedRegion,
  PROVIDER_GOOGLE
} from "react-native-maps";
import {
  Container,
  Header,
  Content,
  Button,
  Form,
  Item,
  H1,
  H2,
  H3,
  Input,
  Label,
  List,
  ListItem,
  Icon
} from "native-base";
var self = "";
const { width, height } = Dimensions.get("window");

import color from "../Helpers/color";
export default class AddressComp extends Component {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      modalVisible: props.modalstate,
      predictions: [],
      addressText: "",
      currentlocation: null,
      chosenlocation: { lat: 0, lng: 0 }
    };
  }
  async getaccess() {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {}
    );
    // console.log('granted',granted);
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  }
  componentWillReceiveProps(props) {
    if (props.modalstate) {
      this.setState({ modalVisible: props.modalstate });
      // this.setState({addressText:props.userAddress})
      // this.changeText(props.userAddress);
    }
  }
  getAddressByLatLng = data => {
    fetch(
      "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
        data.latitude +
        "," +
        data.longitude +
        "&sensor=false&key=AIzaSyDS9ePaBsFgdZt5v2wQrciYrLGhVJmvTWE"
    )
      .then(response => response.json())
      .then(responsejson => {
        var obj = { lat: data.latitude, lng: data.longitude };
        this.props.receiveAddress({
          usertxt: responsejson.results[0].formatted_address,
          location: obj,
          position: data
        });
        // alert(JSON.stringify(responsejson));
        this.setState({
          addressText: responsejson.results[0].formatted_address
        });
        this.changeText(responsejson.results[0].formatted_address);
        this.setState({ modalVisible: false });
        this.setState({ addressText: "" });
        this.setState({ predictions: [] });
      });
  };
  componentWillMount = () => {
    // this.loadcurrentLocation();
  };
  async loadcurrentLocation() {
    this.setState({ currentlocation: "blue" });
    const granted = await this.getaccess();
    if (granted == true) {
      Geolocation.getCurrentPosition(
        position => {
          // alert(JSON.stringify(position));
          this.setState({
            chosenlocation: {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            }
          });
          this.getAddressByLatLng(position.coords);
        },
        error => this.setState({ error: error.message }),
        { timeout: 30000, maximumAge: 0 }
      );
    } else {
      alert("Access Denied to get your location");
    }
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  changeText = data => {
    this.setState({ addressText: data });
    // this.setState({userAddress:data})
    fetch(
      "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" +
        data +
        "&key=AIzaSyAxAOjk0YqwUFok3BJ1h97gGafYEBBYgdM"
    )
      .then(response => response.json())
      .then(responsejson => {
        // alert(JSON.stringify(responsejson));
        this.setState({ predictions: responsejson.predictions });
      });
  };
   
  printAddress() {
    return (
      <List>
        {this.state.predictions.map((data, key) => {
          return (
            <ListItem key={key} onPress={() => this.changeAddress(data)}>
              <Text>{data.description}</Text>
            </ListItem>
          );
        })}
      </List>
    );
  }
  hideAddressbar = () => {
    this.setState({ modalVisible: false });
    // this.props.receiveAddress({usertxt:this.state.addressText})
    this.setState({ addressText: "" });
    this.setState({ predictions: [] });
    this.props.onClose && this.props.onClose();
  };
  changeAddress = data => {
    // alert(data.place_id);
    fetch(
      "https://maps.googleapis.com/maps/api/place/details/json?placeid=" +
        data.place_id +
        "&fields=name,rating,geometry,formatted_phone_number&key=AIzaSyAxAOjk0YqwUFok3BJ1h97gGafYEBBYgdM"
    )
      .then(response => response.json())
      .then(responsejson => {
        var chosenlocation = responsejson.result.geometry.location;
        console.log(responsejson.result);
        this.setState({
          chosenlocation: { lat: chosenlocation.lat, lng: chosenlocation.lng }
        });
        this.props.receiveAddress({
          usertxt: data.description,
          location: chosenlocation,
          position: {
            latitude: chosenlocation.lat,
            longitude: chosenlocation.lng
          }
        });
        this.setState({ modalVisible: false });
        this.setState({ addressText: "" });
        this.setState({ predictions: [] });
      });

    // alert(data.description);

    // this.setState({'userAddress':data.description})
  };
  currentlocation = () => {
    this.setState({
      currentlocation: this.state.currentlocation == null ? "blue" : null
    });
  };
  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            // this.setState({})
            this.hideAddressbar();
            // Alert.alert('Modal has been closed.');
          }}
        >
          <Container>
            <Content>
              <Item regular>
                <Icon
                  type="Ionicons"
                  style={{ color: this.state.currentlocation ? "blue" : null }}
                  name="locate"
                  onPress={() => this.loadcurrentLocation()}
                />
                <Input
                  autoFocus={true}
                  style={{ width: "50%" }}
                  onChangeText={this.changeText}
                  value={this.state.addressText}
                  placeholder="Please Type Your Address"
                />
                {this.state.addressText.length > 0 && (
                  <Icon
                    type="FontAwesome"
                    name="times-circle-o"
                    onPress={() => {
                      this.setState({ addressText: "" });
                      this.setState({ predictions: [] });
                    }}
                  />
                )}
                {this.state.addressText.length == 0 && (
                  <Icon
                    type="FontAwesome"
                    name="times-circle-o"
                    onPress={() => {
                      this.hideAddressbar();
                    }}
                  />
                )}
              </Item>

              {this.state.predictions.length == 0 && (
                <Text style={{ textAlign: "center" }}>No Address Found</Text>
              )}
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    position: "absolute",
                    zIndex: 999999,
                    width: "100%",
                    backgroundColor: color.white
                  }}
                >
                  {this.printAddress()}
                </View>
                <MapView
                  provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                  style={styles.map}
                  region={{
                    latitude: this.state.chosenlocation.lat,
                    longitude: this.state.chosenlocation.lng,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121
                  }}
                >
                  <MapView.Marker
                    coordinate={{
                      latitude: this.state.chosenlocation.lat,
                      longitude: this.state.chosenlocation.lng
                    }}
                    title={this.state.userAddress}
                    description={this.state.userAddress}
                    onDragEnd={e => {
                      this.setState({
                        chosenlocation: {
                          lat: e.nativeEvent.coordinate.latitude,
                          lng: e.nativeEvent.coordinate.longitude
                        }
                      });
                      this.getAddressByLatLng(e.nativeEvent.coordinate);
                    }}
                  />
                </MapView>
              </View>
            </Content>
          </Container>
        </Modal>
      </View>
    );
  }
  componentDidMount() {
    // this.textInput._root.focus();
    // this.textInput.props.onChangeText(this.props.inputValue);
  }
}
const styles = StyleSheet.create({
  Container: {
    flex: 0.65
  },
  map: {
    width: width,
    height: height
  }
});
