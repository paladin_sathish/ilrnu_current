import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Picker,
  TouchableOpacity,
  Image,
  TouchableHighlight,
  FlatList,
  ActivityIndicator
} from "react-native";
import { Actions, Router, Scene } from "react-native-router-flux";
import { Right, Top, Icon, Container } from "native-base";
import color from "../Helpers/color";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import Toast from "react-native-simple-toast";
import Right_arrow from "../images/rightarrow.png";
import Dropdown_conatiner from "../components/drpdown_container";
import Geolocation from "react-native-geolocation-service";
import links from "../Helpers/config";
import DatePicker from "react-native-datepicker";
import moment from "moment";

var do_options = ["Celebrate", "Play", "Party", "Gather", "Meet", "Conference"];
var what_options = [
  "Basketball",
  "Cricket",
  "Tennis",
  "Base Ball",
  "Table Tennis",
  "Badminton"
];
var where_options = ["Old Trafford", "San Siro", "OlympiaStadion"];
export default class TopComp extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     loading: false,
                     when: moment(),
                     doDropdown: {
                       id: "venue_spec_id",
                       name: "venue_spec_name",
                       dropdown: []
                     },
                     whatDropdown: {
                       id: "venue_spec_id",
                       name: "venue_spec_name",
                       dropdown: []
                     },
                     whereDropdown: {
                       id: "trn_venue_area",
                       name: "trn_venue_area",
                       dropdown: []
                     },
                     dodata: null,
                     whatdata: null,
                     wheredata: null,
                     TOS: 0,
                     chosenlocation: { latitude: 0, longitude: 0 }
                   };
                 }

                 _dropdown_5_show() {
                   this._dropdown_5 && this._dropdown_5.show();

                   //   return false;
                 }
                 _dropdown_5_willShow() {
                   this._dropdown_5 && this._dropdown_5.show();
                   //     return false;
                 }
                 sendDropdownData = (data, key) => {
                   console.log("datadsfsd", data);
                   console.log(key);

                   this.setState({ [key]: data });
                   console.log(this.state.wheredata);
                   if (key == "whatdata") {
                     this.setState({ wheredata: null, TOS: 1 });
                     this.clearwhere();
                     this.loadwhere(data.venue_spec_id, 0);
                   } else if (key == "wheredata") {
                     // this.clearwhere();
                     this.setState({ TOS: 2 });
                   }
                 };
                 clearwhat = () => {
                   var whatDropdown = this.state.whatDropdown;
                   whatDropdown.dropdown = [];
                   this.setState({ whatDropdown, whatdata: null });
                 };
                 clearwhere = () => {
                   var whereDropdown = this.state.whereDropdown;
                   whereDropdown.dropdown = [];
                   this.setState({ whereDropdown, wheredata: null });
                 };

                 _changedrop() {
                   this.setState((backgroundColor = color.orange));
                 }
                 loaddo() {
                   fetch(links.APIURL + "do", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({})
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       var doDropdown = this.state.doDropdown;
                       doDropdown.dropdown = responseJson.data;
                       this.setState({ doDropdown });
                       console.log(this.state.doDropdown);
                       //  alert(JSON.stringify(responseJson.data));
                       // alert(JSON.stringify(this.state.doDropdown));
                     });
                 }

                 loadwhat() {
                   fetch(links.APIURL + "getWhatSpecDropdown", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({})
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       var whatDropdown = this.state.whatDropdown;
                       whatDropdown.dropdown = responseJson.data;
                       this.setState({ whatDropdown });
                       console.log("what", this.state.whatDropdown);
                     });
                 }

                 loadOvalCount() {
                   fetch(Apilink.apiurl + "getCount", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify()
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log("responseJson", responseJson.data);
                       this.setState({ OvalButtonArray: responseJson.data });
                     });
                 }

                 loadwhere(data, data1) {
                   console.log("where_id", data);
                   fetch(links.APIURL + "getAreaDropdown", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       venueSpecId: data
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       var whereDropdown = this.state.whereDropdown;
                       whereDropdown.dropdown = responseJson.data;
                       this.setState({ whereDropdown });
                       console.log("where", responseJson);
                     });
                 }
                 arrowClick = () => {
                   this.setState({ loading: true });
                   fetch(links.APIURL + "dropdownSearchILRNU", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       venueSpecId: this.state.whatdata
                         ? this.state.whatdata.venue_spec_id
                         : null,
                       whereKey: this.state.wheredata
                         ? this.state.wheredata.trn_venue_area
                         : null,
                       TOS: this.state.TOS,
                       lat: this.state.chosenlocation.latitude,
                       long: this.state.chosenlocation.longitude,
                       offset: 0,
                       selectedDate: moment(this.state.when).format(
                         "YYYY-MM-DD"
                       )
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log("jsonformar", responseJson);
                       this.setState({ loading: false });
                       if (responseJson.status && responseJson.status == 1) {
                         Toast.show("Options are empty to search", Toast.LONG);
                       } else {
                         if (responseJson.length == 0) {
                           this.props.nxtpge([], "", this.state);
                           Toast.show("No Records Found", Toast.LONG);
                         } else {
                           this.props.nxtpge(
                             responseJson.data,
                             "",
                             this.state,
                             responseJson.data.length
                           );
                         }
                       }
                     })
                     .catch(resp => {
                       this.setState({ loading: false });
                       Toast.show("Network Failed", Toast.LONG);
                     });
                   //this.props.receivesearch(data);
                   //this.setState({visible:true})
                 };
                 componentWillMount() {
                   if (this.props.topcompobj) {
                     this.state = this.props.topcompobj;
                   } else {
                     this.loadwhat();
                   }
                   Geolocation.getCurrentPosition(
                     position => {
                       this.setState({
                         chosenlocation: {
                           latitude: position.coords.latitude,
                           longitude: position.coords.longitude
                         }
                       });
                     },

                     error => {
                       console.log(error.code, error.message);
                     },
                     { timeout: 10000, maximumAge: 0 }
                   );
                 }

                 componentWillReceiveProps(props) {
                   if (props.topcompobj) {
                     this.state = props.topcompobj;
                   }
                 }

                 render() {
                   console.log("whatData", this.state.whatdata);
                   return (
                     <View style={styles.top_container}>
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "flex-start",
                           flexWrap: "wrap",
                           flexDirection: "row",
                           alignItems: "center"
                         }}
                       >
                         <Text style={styles.text_style}>I want to</Text>

                         <View
                           style={{
                             paddingLeft: 5,
                             paddingRight: 5,
                             marginBottom: 5
                           }}
                         >
                           <Dropdown_conatiner
                             drop_items={"What"}
                             values={this.state.whatDropdown.dropdown}
                             keyvalue={"venue_spec_name"}
                             action={true}
                             value={
                               this.state.whatdata
                                 ? this.state.whatdata.venue_spec_name
                                 : "What"
                             }
                             sendDropdownData={data =>
                               this.sendDropdownData(data, "whatdata")
                             }
                             _width={50}
                           />
                         </View>
                         <View
                           style={{
                             paddingLeft: 5,
                             paddingRight: 5,
                             marginBottom: 5
                           }}
                         >
                           <Dropdown_conatiner
                             drop_items={"Where"}
                             action={true}
                             values={this.state.whereDropdown.dropdown}
                             keyvalue={"trn_venue_area"}
                             value={
                               this.state.wheredata
                                 ? this.state.wheredata.trn_venue_area
                                 : "Where"
                             }
                             sendDropdownData={data =>
                               this.sendDropdownData(data, "wheredata")
                             }
                             _width={50}
                           />
                         </View>

                         <View
                           style={{
                             alignItems: "center",
                             justifyContent: "center",
                             marginLeft: 3,
                             marginRight: 3
                           }}
                         >
                           <Text style={styles.text_style}>in</Text>
                         </View>

                         <View
                           style={{
                             paddingLeft: 5,
                             paddingRight: 5,
                             marginBottom: 5
                           }}
                         >
                           <DatePicker
                             style={{
                               width: 80,
                               borderRight: "transparent"
                             }}
                             date={this.state.when}
                             mode="date"
                             format="DD MMM YYYY"
                             confirmBtnText="Confirm"
                             cancelBtnText="Cancel"
                             iconComponent={<></>}
                             customStyles={{
                               dateIcon: {},
                               dateInput: {
                                 marginLeft: 0,
                                 backgroundColor: color.white,
                                 borderRadius: 15,
                                 height: 27,
                                 borderColor: "transparent",
                                 borderWidth: 0,
                                 zIndex: 9999
                               },
                               dateText: {
                                 fontSize: 12,
                                 color: "#5F6368"
                               }
                               // ... You can check the source to find the other keys.
                             }}
                             onDateChange={date => {
                               console.log("pikcer", date);
                               console.log(
                                 "pikcer",
                                 moment(date).format("YYYY MM DD")
                               );
                               this.setState({
                                 when: date
                               });
                             }}
                           />
                         </View>
                         <View style={{ justifyContent: "center" }}>
                           <View
                             style={{
                               marginLeft: 3,
                               backgroundColor: color.blueactive,
                               borderRadius: hp("1.5%"),
                               width: hp("5%")
                             }}
                           >
                             {this.state.loading == true && (
                               <ActivityIndicator
                                 size="small"
                                 color={color.white}
                                 style={{ paddingTop: 3, paddingBottom: 3 }}
                               />
                             )}
                             {!this.state.loading && (
                               <TouchableOpacity
                                 style={{
                                   alignSelf: "center",
                                   paddingTop: 5,
                                   paddingBottom: 5
                                 }}
                                 onPress={() => this.arrowClick()}
                               >
                                 <Image
                                   style={{
                                     width: hp("2%"),
                                     height: hp("2%"),
                                     alignSelf: "center"
                                   }}
                                   source={Right_arrow}
                                 ></Image>
                               </TouchableOpacity>
                             )}
                           </View>
                         </View>
                       </View>
                     </View>
                   );
                 }
               }
const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: color.white
  },
  top_container: {
    //    height:hp('10%'),
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    flexDirection: "row",
    backgroundColor: color.orange,
    padding: hp("2%"),
    paddingLeft: hp("4%")
  },
  text_style: {
    color: color.white,
    fontSize: wp("4%")
  },
  drop_down_container: {
    //    flex:1,
    justifyContent: "center",
    marginBottom: hp("2.8%"),
    marginTop: hp("2.8%"),
    marginRight: 3,
    minWidth: hp("11%")
  },
  drop_down: {
    backgroundColor: color.white,
    borderRadius: hp("2%"),
    width: hp("10%"),
    paddingLeft: 10
  },
  homeBgImage: {
    width: wp("100%"),
    height: hp("25%")
  },
  play_button_container: {
    position: "absolute",
    flexDirection: "row",
    right: 0,
    top: 0,
    paddingTop: hp("1.5%"),
    paddingRight: hp("1.5%")
  },
  png_style: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    height: hp("3%"),
    width: hp("3%")
  },
  png_back: {
    backgroundColor: color.white,
    height: hp("4%"),
    width: hp("4%"),
    borderRadius: hp("4%") / 2,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    elevation: 1
  },
  flex_row: {
    flexDirection: "row"
  },
  flex_sep: {
    flex: 1
  },
  down_aarow: {
    height: hp("1%"),
    width: hp("1%"),
    justifyContent: "flex-end",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    alignContent: "flex-end",
    margin: 3
  },
  fav_item_container: {
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    paddingLeft: "1%"
  },
  text_near: {
    fontSize: hp("1.9%")
  },
  text_fav: {
    fontSize: hp("1.9%"),
    color: color.blue,
    fontWeight: "bold"
  },
  text_center: {
    alignItems: "center"
  },
  line: {
    backgroundColor: color.ash,
    width: "100%",
    height: 1
  },
  add_symbol_container: {
    backgroundColor: color.blue,
    justifyContent: "flex-end",
    justifyContent: "center",
    alignSelf: "flex-end",
    marginRight: wp("5%"),
    height: hp("2.5%"),
    width: hp("2.5%")
  },
  add_symbol: {
    height: hp("1.3%"),
    width: hp("1.3%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center"
  },
  right_arr_style: {
    height: hp("2%"),
    width: hp("2%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    margin: 3
  },
  right_arr_container: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    backgroundColor: color.white
  },
  mar_adjuse: {
    marginTop: 3,
    marginBottom: 3
  },
  dropdown_container: {
    width: "100%",
    height: hp("4%"),
    justifyContent: "center",
    alignSelf: "center"
  },
  dropdown_text: {
    //   fontSize: hp('2%'),
    justifyContent: "center"
  },
  dropdown_options: {
    marginTop: 10,
    width: wp("30%")
  },
  drop_down_top: {
    height: hp("1%"),
    width: hp("1%"),
    justifyContent: "center",
    alignSelf: "center"
  }
});
