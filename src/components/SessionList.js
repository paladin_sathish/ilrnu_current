import React, { Component } from "react";
import {
  Image,
  Platform,
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  BackHandler,
  Alert,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { Router, Scene, Actions } from "react-native-router-flux";
import {
  H3,
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  List,
  ListItem
} from "native-base"; 

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import colors from "../Helpers/colors";
export default class SessionList extends Component {
  venuePage = () => {
    if (this.props.action == false) {
      // alert("actions not configured");
    } else {
        if (this.props.subtitle == "Add My Venue")
         Actions.venuepage();
         else
         this.props._onload()
    }
  };
  sideClick = () => {
    this.scrollimage.scrollToEnd();
  };
  render() {
    return (
      <View style={styles.flexrow}>
        <View style={styles.col3}>
          <Text
            style={[
              styles.alignLeft,
              {
                color: colors.blueactive,
                fontSize: hp("2.3%"),
                paddingRight: 12
              }
            ]}
          >
            {this.props.title}
          </Text>
          <TouchableOpacity onPress={() => this.venuePage()}>
            <Text
              style={{
                marginTop: hp("1%"),
                fontSize: hp("1.8%"),
                color: colors.orange
              }}
            >
              {this.props.subtitle}{" "}
              {this.props.icon && (
                <Icon
                  style={{ fontSize: hp("2%"), color: colors.grey }}
                  type="FontAwesome"
                  name="angle-right"
                />
              )}{" "}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.col7}>
          <ScrollView
            style={{ flex: 1 }}
            horizontal={true}
            ref={c => {
              this.scrollimage = c;
            }}
          >
            {this.props.imageList &&
              this.props.imageList.map((obj, key) => {
                return (
                  <Image
                    key={key}
                    style={styles.listImage}
                    source={obj.image}
                  />
                );
              })}
          </ScrollView>
          <TouchableOpacity style={styles.rightArrw} onPress={this.sideClick}>
            <Icon
              style={{ fontSize: hp("3%"), color: colors.ash3 }}
              type="FontAwesome"
              name="angle-right"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = {
  listImage: {
    width: hp("21%"),
    height: hp("11%"),
    borderRadius: 5,
    marginRight: 7
  },
  rightArrw: {
    flexDirection: "row",
    alignItems: "center",
    height: "100%",
    paddingLeft: 7
  },
  col3: {
    flex: 0.3,
    justifyContent: "center"
  },
  col7: {
    flex: 0.69,
    flexDirection: "row"
  },
  flexrow: {
    flex: 1,
    flexDirection: "row",
    // margin:12,
    padding: 12,
    borderBottomWidth: 0.5
  }
};
