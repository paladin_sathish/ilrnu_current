import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Picker,
  TouchableOpacity,
  Image,
  Alert,
  FlatList,
  ScrollView
} from "react-native";
import { Button, CheckBox } from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import Down_arrow from "../images/arrow_down.png";
import StarRating from "react-native-star-rating";
import Tick_mark from "../components/TickComp";
import Triangle from "../components/triangles";
import Line from "../components/Line";
import { Actions, Router } from "react-native-router-flux";
const near_ground_list = [
  {
    id: 1,
    starCount: 3,
    image: require("../images/slider1.png"),
    ground_name: "Brand new indoor soccer center",
    kms: "10 kms",
    mins: "35 mins",
    visible: true
  },
  {
    id: 2,
    starCount: 4,
    image: require("../images/slider2.png"),
    ground_name: "Soccer Club Academy",
    kms: "5 kms",
    mins: "15 mins"
  },
  {
    id: 3,
    starCount: 2.5,
    image: require("../images/slider3.png"),
    ground_name: "Best Soccer Pitche Ground",
    kms: "5 kms",
    mins: "15 mins"
  },
  {
    id: 4,
    starCount: 4,
    image: require("../images/slider4.png"),
    ground_name: "Brand new indoor soccer center",
    kms: "6 kms",
    mins: "20 mins"
  }
]; 

export default class lrnusearch extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     slectdata: "",
                     visible: false,
                     sliderData: [],
                     listingdetails: false,
                     activeCategory: null,
                     showvisible: false,
                     checkboxes1: [
                       {
                         id: 1,
                         name: "Booked",
                         price: 250,
                         _checked: false
                       },
                       {
                         id: 2,
                         name: "Top 20",
                         _checked: false,
                         price: 250
                       },
                       {
                         id: 2,
                         name: "Top 20",
                         _checked: false,
                         price: 250
                       },
                       {
                         id: 2,
                         name: "Top 20",
                         _checked: false,
                         price: 250
                       },
                       {
                         id: 2,
                         name: "Top 20",
                         _checked: false,
                         price: 250
                       },
                       {
                         id: 2,
                         name: "Top 20",
                         _checked: false,
                         price: 250
                       },
                       {
                         id: 3,
                         name: "Highly Rated",
                         _checked: false,
                         price: 250
                       },
                       {
                         id: 4,
                         name: "Activity Based",
                         _checked: false,
                         price: 250
                       }
                     ]
                   };
                 } 

                 componentWillReceiveProps=(props)=>{
                    if(props.venueclicked != this.state.visible)
                    this.setState({visible:props.venueclicked})
                 }

                 renderSearch = item => {
                   return <View></View>;
                 };
                 onPress = (item, index) => {
                   this.setState({visible:!this.state.visible})
                   
                 };

                 componentWillMount = () => {};

                 onStarRatingPress(rating) {
                   this.setState({
                     starCount: rating
                   });
                 }
                 componentWillReceiveProps(props) {
                   if (props.sendsliderdata) {
                     this.setState({ sliderData: props.sendsliderdata });
                   }
                   if (!props.arrowclicked) {
                   } else {
                     this.setState({ activeCategory: null });
                   }
                   // alert(JSON.stringify(props.sendsliderdata))
                 }
                 toggleCheckbox1 = id => {
                   let changedCheckbox = this.state.checkboxes1.find(
                     cb => cb.id === id
                   );
                   changedCheckbox._checked = !changedCheckbox._checked;
                   let chkboxes = this.state.checkboxes;
                   for (let i = 0; i < chkboxes.length; i++) {
                     if (chkboxes[i].id === id) {
                       chkboxes.splice(i, 1, changedCheckbox);
                     }
                   }
                   this.setState({ checkboxes1: chkboxes });
                 };

                 render() {
                   return (
                     <React.Fragment>
                       <View style={styles.container}>
                         <ScrollView
                           directionalLockEnabled={false}
                           horizontal={true}
                           pagingEnabled={false}
                           onScroll={false}
                           showsHorizontalScrollIndicator={false}
                         >
                           {near_ground_list &&
                             near_ground_list.map((item, index) => {
                               return (
                                 <View style={styles.square}>
                                   <TouchableOpacity
                                     onPress={() => this.onPress(item, index)}
                                   >
                                     <View style={{ marginTop: 10 }}>
                                       <StarRating
                                         // disabled={false}
                                         maxStars={5}
                                         rating={4}
                                         selectedStar={rating =>
                                           this.onStarRatingPress(rating)
                                         }
                                         halfStarColor={color.blue}
                                         fullStarColor={color.blue}
                                         starSize={8}
                                         containerStyle={styles.RatingBar}
                                         starStyle={styles._starstyle}
                                         emptyStarColor={color.ash6}
                                       />
                                     </View>
                                     <View
                                       style={{
                                         display: "flex",
                                         justifyContent: "flex-start",
                                         alignItems: "flex-start"
                                       }}
                                     >
                                       <Text style={styles.text_style2}>
                                         How to cook asian foods
                                       </Text>

                                       <Text
                                         style={{
                                           color: color.orange,
                                           fontSize: 12,
                                           borderBottomWidth: 1,
                                           borderBottomColor: "rgba(0,0,0,0.1)",
                                           paddingVertical: 4
                                         }}
                                       >
                                         {" "}
                                         Beginner Level
                                       </Text>

                                       <Text style={styles.text_style4}>
                                         {" "}
                                         $120 / Day
                                       </Text>

                                       <Text
                                         style={{
                                           color: color.orange,
                                           fontSize: 12,
                                           paddingVertical: 4
                                         }}
                                       >
                                         {" "}
                                         By Sathish
                                       </Text>
                                     </View>
                                   </TouchableOpacity>
                                   {item.visible == true && (
                                     <View
                                       style={{
                                         alignItems: "center",
                                         justifyContent: "flex-start",
                                         flexDirection: "row",
                                         marginLeft: 10,
                                         marginBottom: -5
                                       }}
                                     >
                                       <View>
                                         <Triangle
                                           width={hp("4%")}
                                           height={hp("3.9%")}
                                           color={color.ash5}
                                           direction={"down-left"}
                                         ></Triangle>
                                       </View>
                                       <View
                                         style={{
                                           top: hp("0.3%"),
                                           left: hp("0.15%"),
                                           position: "absolute",
                                           zIndex: 9999
                                         }}
                                       >
                                         <Triangle
                                           width={hp("3.9%")}
                                           height={hp("3.9%")}
                                           color={color.white}
                                           direction={"down-left"}
                                         ></Triangle>
                                       </View>
                                     </View>
                                   )}
                                 </View>
                               );
                             })}
                         </ScrollView>
                         <Line />
                         {this.state.visible && (
                           <ScrollView>
                             <View>
                               <View
                                 style={{
                                   paddingVertical: 5,
                                   paddingHorizontal: 10,
                                   borderBottomColor: "rgba(0,0,0,0.1)",
                                   borderBottomWidth: 1
                                 }}
                               >
                                 <View>
                                   <View
                                     style={{
                                       borderBottomColor: "rgba(0,0,0,0.1)",
                                       borderBottomWidth: 1
                                     }}
                                   >
                                     <Text
                                       style={{
                                         fontSize: 19,
                                         fontStyle: "normal",
                                         color: color.black
                                       }}
                                     >
                                       How to Cook asian foods
                                     </Text>
                                     <Text
                                       style={{
                                         color: color.orange,
                                         fontSize: 15
                                       }}
                                     >
                                       Beginners Level - Video Based
                                     </Text>
                                   </View>

                                   <View style={{ flexDirection: "row" }}>
                                     <Text style={styles.text_style2}>
                                       $120 / Day{" "}
                                     </Text>

                                     <Text
                                       style={[
                                         styles.text_style3,
                                         { padding: 5 }
                                       ]}
                                     >
                                       {" "}
                                       By Author / Organisation
                                     </Text>
                                   </View>

                                   <Text style={{ textAlign: "left" }}>
                                     simply dummy text of the printing and
                                     typesetting industry. Lorem Ipsum has been
                                     the industry's standard dummy text ever
                                     since the 1500s,
                                   </Text>
                                 </View>

                                 <View
                                   style={{
                                     flexDirection: "row",
                                     justifyContent: "space-between",
                                     alignItems: "center",
                                     paddingVertical: 7
                                   }}
                                 >
                                   <View>
                                     <StarRating
                                       // disabled={false}
                                       maxStars={5}
                                       rating={4}
                                       selectedStar={rating =>
                                         this.onStarRatingPress(rating)
                                       }
                                       halfStarColor={color.blue}
                                       fullStarColor={color.blue}
                                       starSize={8}
                                       containerStyle={styles.RatingBar}
                                       starStyle={styles._starstyle}
                                       emptyStarColor={color.ash6}
                                     />
                                   </View>

                                   <Button
                                     style={{
                                       paddingHorizontal: 12,
                                       height: 30
                                     }}
                                   >
                                     <Text style={{ color: "white" }}>
                                       View Details
                                     </Text>
                                   </Button>
                                 </View>
                               </View>

                               <View
                                 style={{
                                   display: "flex",
                                   justifyContent: "flex-start",
                                   backgroundColor: "white !important"
                                 }}
                               >
                                 {this.state.checkboxes1.map((cb, i) => {
                                   return (
                                     <View
                                       style={{
                                         display: "flex",
                                         flexDirection: "row",
                                         paddingVertical: 10,
                                         justifyContent: "space-between",
                                         borderBottomWidth: 1,
                                         borderBottomColor: "rgba(0,0,0,0.1)"
                                       }}
                                     >
                                       <View style={{ flexDirection: "row" }}>
                                         <View>
                                           <CheckBox
                                             checked={cb._checked}
                                             color={color.blue}
                                             onPress={() =>
                                               this.toggleCheckbox(cb.id)
                                             }
                                           />
                                         </View>
                                         <View>
                                           <Text
                                             style={{
                                               color: "black",
                                               fontSize: 16,
                                               paddingLeft: 20
                                             }}
                                           >
                                             hihiohod
                                           </Text>
                                         </View>
                                       </View>
                                       <View
                                         style={{
                                           display: "flex",
                                           justifyContent: "flex-end",
                                           alignItems: "center",
                                           flexDirection: "row"
                                         }}
                                       >
                                         <Text
                                           style={{
                                             color: color.bluesub,
                                             fontStyle: "normal",
                                             fontSize: 16
                                           }}
                                         >
                                           {" "}
                                           250 INR
                                         </Text>
                                       </View>
                                     </View>
                                   );
                                 })}
                               </View>
                             </View>
                           </ScrollView>
                         )}
                       </View>
                     </React.Fragment>
                   );
                 }
               }



const styles = StyleSheet.create({
  container: {
      flex:1,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  flex_row: {
    flexDirection: "row"
  },
  text_style1: {
    fontSize: hp("2%"),
    marginRight: 10
  },
  text_style: {
    color: color.blue,
    fontSize: hp("2%")
  },
  Down_arrowstyle: {
    height: hp("1.4%"),
    width: hp("1.4%")
  },
  RatingBar: {
    height: hp("2.5%"),
    width: wp("13%")
  },
  _starstyle: {
    fontSize: hp("1.8%"),
    marginRight: 1
  },
  square: {
    width: hp("15%"),
    paddingHorizontal: 10,
    borderRightColor: "rgba(0,0,0,0.1)",
    borderRightWidth: 1,
  },
  image_style: {
    height: hp("10%"),
    width: hp("15%"),
    borderRadius: 5,
    justifyContent: "center"
  },
  text_style2: {
    //  textAlign:"center",
    fontSize: hp("2.1%"),
    color: color.blueactive
  },
  text_style4: {
    //  textAlign:"center",
    fontSize: hp("2.1%"),
    color: color.bluesub
  },
  text_style3: {
    color: color.orange,
    fontSize: hp("1.4%")
  },
  selected: {
    backgroundColor: color.blue
  },
  activeparent: {
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999999,
    margin: "auto",
    left: 0,
    right: 0
  }
});