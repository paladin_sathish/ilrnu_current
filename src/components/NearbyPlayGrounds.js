import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,PermissionsAndroid,ActivityIndicator} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png';
import placeholder_img from '../images/placeholder_img.png';
import links from '../Helpers/config';
import Line from '../components/Line';
import TabLoginProcess from '../components/TabLoginProcess';
import ModalDropdown  from 'react-native-modal-dropdown';
import Geolocation from 'react-native-geolocation-service';
import {Actions,Router,Scene} from 'react-native-router-flux';
import CheckAuthentication from '../Helpers/CheckAuthentication';
import ListingDetails from '../pages/ListingDetails';
import AsyncStorage from '@react-native-community/async-storage';
import Tick_mark from '../components/TickComp'
import VenueCardComp from '../components/VenueCardComp';



// import MapView ,{Marker,AnimatedRegion,PROVIDER_GOOGLE}from 'react-native-maps';
const playgr_list=[
    {image:require('../images/slider1.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider2.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider3.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider4.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider5.png'),text:'Meadow Crest Playground'}
]

export default class NearbyPlayGrounds extends Component{
    constructor(props) {
      super(props);
      this.state = {manuallocation:null,activeCategory:null,loginDetails:null,login:null,NoRecords:false,catDropdown:{id:'venue_cat_id',name:'venue_cat_name',dropdown:[]},catvalue:'',currentlocation:null,
    chosenlocation:{latitude:0,longitude:0},corporatedata:[],venuecatname:'',spinner:true,loginState:false};
     
  }  

componentWillMount = () => {
 
};

  
    checkAuth=(data)=>{
      // alert("");
      // var renderData=CheckAuthentication.authenticateLogin();
      // Actions.venueform();
          if(this.state.loginDetails){
        if(this.state.loginDetails.user_cat_id==1){
           if(data){

          Actions.venueform({logindata:this.state.loginDetails,welcomemsg:'User Logged in Successfully'});
        }else{
         Actions.venueform({logindata:this.state.loginDetails});
        }
        }else{
          if(data){
          Actions.corporateform({logindata:this.state.loginDetails,welcomemsg:'User Logged in Successfully'});
        }else{
          Actions.corporateform({logindata:this.state.loginDetails});
        }
        }
      }else{
        this.setState({loginState:true});
      // Actions.venuepage({raiselogin:true})
      }
    }
tabAction=(data)=>{
  // alert(JSON.stringify(data));
  // if(data.uer_)
  this.setState({loginState:false});
  this.setState({loginDetails:data},function(){
    this.checkAuth(data);
  })
}   
    componentWillReceiveProps(props){
        if(props.visiblescreenData){
        if(props.visiblescreenData!='nearby'){
          var corporatedata=this.state.corporatedata;
          corporatedata.map((obj)=>obj.visible=false);
          this.setState({activeCategory:null,corporatedata});
        }
        // return;
      }
      if(props.category){
         this.setState({activeCategory:null});
        // this.setState({ven})
        var findIndex=this.state.catDropdown.dropdown.findIndex((obj)=>obj.venue_cat_id==props.category.venue_cat_id);
        if(findIndex!=-1){
        this.dropdown.select(findIndex);
        this.NearbyPlayground(this.state.chosenlocation,props.category.venue_cat_id,props.category.venue_cat_name);
        }
      }
    }
    renderButtonText=(data)=>{
      this.setState({catvalue:data})
      this.NearbyPlayground(this.state.chosenlocation,data.venue_cat_id,data.venue_cat_name);
      return data.venue_cat_name;
    }
    categoryListing=()=>{
    
    fetch(links.APIURL+'listVenueCategory', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({user_cat_id:1}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      // alert(JSON.stringify(responseJson))
        var catDropdown=this.state.catDropdown;
    catDropdown.dropdown=responseJson.data;
    this.setState({catDropdown})
    this.NearbyPlayground(this.state.chosenlocation,responseJson.data.length>0?responseJson.data[0].venue_cat_id:null,responseJson.data.length>0?responseJson.data[0].venue_cat_name:'');
    // alert(JSON.stringify(responseJson.data));
    // alert(JSON.stringify(this.state.catDropdown.dropdown[0].venue_cat_name))
       // console.log("dropvalues",JSON.stringify(this.state.catDropdown));
    })  
  }
  async getaccess(){
  const granted= await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{})
  // console.log('granted',granted);
  alert(granted)
  if(granted===PermissionsAndroid.RESULTS.GRANTED){
   return true;
  }else{
    return false;
  }
}
   loadcurrentLocation =async ()=>{ 
   const manuallocation = JSON.parse(await AsyncStorage.getItem('chosenlocation'));
   console.log("nearybygetlat", manuallocation);
  this.setState({currentlocation:'blue'});
console.log("calling Twice1");
  // Geolocation.getCurrentPosition(
  //      (position) => { 

    let lat = manuallocation.lat
    let lng = manuallocation.lng;
    this.setState({manuallocation:{lat:lat,lng:lng}})
         this.setState({chosenlocation:{latitude:lat,longitude:lng}},function(){
          this.categoryListing();
         })
         
} 


clearActiveCategory=()=>{
      var corporatedata=this.state.corporatedata;
    corporatedata.map((obj)=>obj.visible=false);
    this.setState({corporatedata,activeCategory:null});
} 


NearbyPlayground=(location,data,data1)=>{
    // alert('hii');
    if(!data){
      return;
    } 

    
  
const {manuallocation} = this.state;
  // var lat=this.state.currentLatLng.latitude;
  // console.log("lati",this.state.currentLatLng.latitude)
this.setState({venuecatname:data1});
  this.setState({spinner:true,activeCategory:null});
  fetch(links.APIURL + "nearbyLocation/", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    // body: JSON.stringify({"lat":location.latitude,"long":location.longitude,"userCatId":'1','venueCatId':data}),
    body: JSON.stringify({
      lat: manuallocation.lat,
      long: manuallocation.lng,
      userCatId: "1",
      venueCatId: data
    })
  })
    .then(response => response.json())
    .then(responseJson => {
      this.setState({ spinner: false });
      if (responseJson.length > 0) {
        this.setState({ NoRecords: false });
      } else {
        this.setState({ NoRecords: true });
      }
      this.setState({
        corporatedata: responseJson.length > 0 ? responseJson : []
      });
      // alert(JSON.stringify(responseJson[0].venue_cat_name));
    }); 
  }
  selectedData=(data)=>{
    // alert(JSON.stringify(this.state.catDropdown.dropdown[data]));
    this.setState({catvalue:this.state.catDropdown.dropdown[data]});
  }
    async componentDidMount(){
        const data=await AsyncStorage.getItem('loginDetails');
// alert(data);
if(data){
  // alert(JSON.stringify(data));
  this.setState({loginDetails:JSON.parse(data)});

}
    // this.categoryListing();
   
    this.loadcurrentLocation();

  }
  receiveFram=(data)=>{
    // data.height=null;
    return data;
  }
  showDropdown=()=>{
    this.dropdown.show();
  }
  changeItemSlider=(item,index,visible)=>{
    this.props.visiblescreen&&this.props.visiblescreen('nearby');
    var corporatedata=this.state.corporatedata;
    corporatedata.map((obj)=>obj.visible=false);
    corporatedata[index].visible=visible?!visible:true;
    this.setState({corporatedata});
    if(corporatedata[index].visible==true){
    this.setState({activeCategory:item})

    }else{
      this.setState({activeCategory:null})
    }

  }
    render(){ 
      console.log('render near')
        return(
            <View style={styles.container}>
                <View style={styles.flex_row}>
                    <View style={styles.flex_row}>
                        <Text style={[styles.text_style1,{marginRight:2}]}>Near by</Text>
                        <ModalDropdown ref={el => this.dropdown = el}
                          adjustFrame={this.receiveFram}
                                    style={{marginRight:3}}    
                                    textStyle={[styles.text_style]}
                                    dropdownStyle={styles.dropdown_options}
                                    // dropdownTextStyle={[styles.dropTextstyle]}
                                    defaultValue={this.state.venuecatname&&this.state.venuecatname?this.state.venuecatname:'Select'}
                                    renderButtonText={this.renderButtonText}
                                    showsVerticalScrollIndicator={false}    
                                            options={this.state.catDropdown.dropdown}                    
                                    renderRow={  
                    (rowData) =>  
                        <Text style={{fontSize: 14,padding:5}}>{rowData.venue_cat_name}</Text>}>
                     </ModalDropdown>

                        <TouchableOpacity onPress={()=>this.showDropdown()} style={{width:20,flexDirection:'column',justifyContent:'center'}}>
                            <Image source={Down_arrow} style={styles.Down_arrowstyle}></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.flex_sep}>
                        <TouchableOpacity onPress={()=>this.checkAuth()}  style={styles.add_symbol_container} >
                            <Image style={styles.add_symbol} source={ADD_img}>
                            </Image>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.state.spinner==true &&
     <ActivityIndicator
      animating
      color="#615f60"
      size="small"
      style= {{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
     
  }}
    />
  }
               {this.state.corporatedata.length>0&&
                <ScrollView
                  
                   horizontal={true}>
                  {
                   this.state.corporatedata.map((item,index)=>{
                     return (
                       <TouchableOpacity
                         style={styles.square}
                         onPress={() =>
                           Actions.BookDetails({
                             lat: this.state.chosenlocation.latitude,
                             long: this.state.chosenlocation.longitude,
                             id: item.venue_id
                           })
                         }
                         key={index}
                       >
                         <VenueCardComp item={item}/>
                       </TouchableOpacity>
                     );


                   })

                 }
                   </ScrollView> 
          
}
{this.state.NoRecords==true&&
<View ><Text style={{textAlign:'center',padding:10}}>No Records</Text></View>
}
{this.state.activeCategory&&
  <Line/>
}
  {this.state.activeCategory&&
                    <ListingDetails activeCategory={this.state.activeCategory}  clearList={()=>this.clearActiveCategory()}>
                    <Text>Cancel</Text>
                    </ListingDetails>
                }
        {this.state.loginState==true&&
        <TabLoginProcess  closetabmodal={()=>this.setState({loginState:false})} loginttype="Add your Venue" type='login' visible={true} sendlogindet={this.tabAction}/>
      }

            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
    image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },
    text_style2:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
        paddingLeft:hp('0.45%'),
        paddingTop:hp('0.45%')
    },
    flex_row:{
        flexDirection:'row'
    },
    text_style1:{
        fontSize:hp('2%')
    },
    text_style:{
        color:color.blue,
        fontSize:hp('2%'),
        fontWeight:'bold'
    },
    Down_arrowstyle:{
        height:hp('1.4%'),
        width:hp('1.4%')
    },
    flex_sep:{
        flex:1
    },
    add_symbol_container:{
        backgroundColor:color.blue,
        justifyContent:'flex-end',
        justifyContent:'center',
        alignSelf:'flex-end',
        marginRight:wp('5%'),
        height:hp('2.5%'),
        width:hp('2.5%'),
       
    },
    add_symbol:{
        height:hp('1.3%'),
        width:hp('1.3%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
    },
    dropdown_options:{
        backgroundColor:color.white,
        // padding:10,
        paddingLeft:10,
        paddingRight:10,
        paddingTop:10,
        paddingBottom:10,
        marginBottom:5,
        fontSize:hp('1.9%'),
        width:wp('40%'),
        borderRadius:5,
        borderWidth:2,
        minHeight:hp('2%'),
        borderColor:color.ash

    },
    dropTextstyle:{
      // padding:10
      marginBottom:10
    }
   
})