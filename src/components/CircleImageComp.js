'use strict';
import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Tabs, Tab, TabHeading,Content} from 'native-base';
import {StyleSheet, View, Image, StatusBar, Picker, Text, TouchableOpacity, ScrollView } from 'react-native';
import color from '../Helpers/color';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class CircleImageComp extends Component {
  circleStyles=()=>{
    return {
      width:hp(this.props.width),
      height:hp(this.props.height),
      borderRadius:hp(this.props.height)/2
    }
  } 
   circleCountStyles=()=>{
    return {
      width:hp('4%'),
      height:hp('4%'),
      borderRadius:hp('4%')/2,
      alignItems:"center",
      justifyContent:'center'
    }
  }
  render() {
    var circlestyles=this.circleStyles();
    return (
      <View style={[styles.circleMain]}>
          <TouchableOpacity onPress={()=>this.props.sendText(this.props.data)} 
              style={[styles.circle,circlestyles,this.props.active?styles.activecolor:'']}
              >
              <Image tintColor={this.props.active?"white":color.blueactive} 
                style={styles.imagecomp} source={this.props.icon}/>
          </TouchableOpacity>
               <Text  numberOfLines={2} style={{marginTop:4,fontSize:hp('1.9%'),textAlign:'center',
               color:this.props.active?color.blueactive:color.black}}>{this.props.text}</Text>
                {this.props.circleCount&&
          <View style={[styles.circleCount,this.circleCountStyles()]}>
            <Text style={{color:'black',textAlign:'center',fontSize:hp('1.5%')}}>
                {this.props.circleCount}
            </Text>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  circleMain:{
    alignItems:'center',
    paddingBottom:25
  },
circle:{
  flexDirection:'row',
  justifyContent:'center',
  alignItems:'center',
  borderWidth:1.5,
  borderColor:color.top_red,
  // width:hp('12%'),
  // height:hp('12%'),
  // borderRadius:hp('12%')/2
},
activecolor:{
  backgroundColor:color.top_red
},
imagecomp:{
  width:'60%',
  height:'60%',
},
circleCount:{
  position:'absolute',
  top:-4,
  right:-4,
  backgroundColor:'white',
  borderWidth:1,
  alignItems:'center'
}
});


export default CircleImageComp;