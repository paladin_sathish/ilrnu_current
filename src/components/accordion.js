import React, { Component } from 'react';

import color from '../Helpers/color';
import { Alert, LayoutAnimation, StyleSheet, View, Text, ScrollView, UIManager, TouchableOpacity, Platform, Image,TextInput } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title,Content } from 'native-base';
import FromToDuration from '../pages/fromtoduration';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class Expandable_ListView extends Component {
  constructor(props) {
    super(props);
    this.state = {

      layout_Height: 0,moredetails:props.sendDateObj.moredetails,
      fromtodate:{from:'',to:''}

    }
  }

  componentWillReceiveProps(nextProps) {
// alert('asdfafsddsf')
// alert(JSON.stringify(nextProps));
this.setState({moredetails:nextProps.sendDateObj.moredetails});
    // this.setState({moredetails:nextProps.sendDateObj.moredetails});
    if (nextProps.item.expanded) {
      this.setState(() => {
        return {
          layout_Height: null
        }
      });
    }
    else {
      this.setState(() => {
        return {
          layout_Height: 0
        }
      });
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.state.layout_Height !== nextState.layout_Height) {
  //     return true;
  //   }
  //   return false;
  // }

  show_Selected_Category = (item) => {

    // Write your code here which you want to execute on sub category selection.

  }
  todate=(data)=>{
  // alert(data);
  // this.setState()
  var fromtodate=data;

  this.props.senddate(fromtodate)
}
fromdate=(data)=>{
  var fromtodate=data;
  this.props.senddate(fromtodate)
}
  render() {
    // alert(JSON.stringify(this.props));
    return (
      <View style={styles.Panel_Holder}>

        <TouchableOpacity activeOpacity={0.8} onPress={this.props.onClickFunction} style={styles.category_View}>

          <Text style={styles.category_Text}>{this.props.item.title} </Text>
{
  this.props.item.expanded==true?<Icon style={styles.fontarrowicon} type="FontAwesome" name='angle-down'/>:<Icon style={styles.fontarrowicon} type="FontAwesome" name='angle-up'/>
}
       

        </TouchableOpacity>

        <View style={{ height: this.state.layout_Height, overflow: 'hidden' }}>
        {this.props.item.type=='date'&&
        <FromToDuration sendDateObj={this.props.sendDateObj} from={this.fromdate} to={this.todate}/>
      }
        {this.props.item.type!='date'&&
       <TextInput
    multiline={true}
    numberOfLines={4}
     value={this.state.moredetails}
    onChangeText={(e)=>this.props.moredetailsdata&&this.props.moredetailsdata(e)}
    style={{borderColor:color.black1,borderWidth:1,margin:10}}
   />
      }
        
        </View>

      </View>

    );
  }
}

export default class AccordionView extends Component {

  constructor(props) {
    super(props);
    if (Platform.OS === 'android') {

      UIManager.setLayoutAnimationEnabledExperimental(true)

    }

    const array = props.datas;

    this.state = { AccordionData: [...array],moredet:"" }
  }

  update_Layout = (index) => {

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    const array = [...this.state.AccordionData];

    array[index]['expanded'] = !array[index]['expanded'];

    this.setState(() => {
      return {
        AccordionData: array
      }
    });
  }

  componentDidMount=()=>{
 // const array = this.props.datas.filter((key,obj)=>a);
}
componentWillReceiveProps(props){

  // this.setState({moredet:props.moredetails})

}

senddate=(data)=>{
  // alert(data);
  this.props.sendFromDate(data);
}
moredetails=(data)=>{
  // alert(data);
  this.props.moredetailsdata(data);
// this.setState({moredet:data})
}
  render() {
  
    return (
      <View style={styles.MainContainer}>

        <ScrollView>
          {
            this.state.AccordionData.map((item, key) =>
              (
                <Expandable_ListView  moredetailsdata={(e)=>this.moredetails(e)} sendDateObj={this.props.sendDateObj} senddate={(data)=>this.senddate(data)} key={item.title} onClickFunction={this.update_Layout.bind(this, key)} item={item} />
              ))
          }
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    // justifyContent: 'center',
    backgroundColor: color.white,
  },

  iconStyle: {

    width: 30,
    height: 30,
    justifyContent: 'flex-end',
    alignItems: 'center',
    tintColor: '#fff'

  },

  sub_Category_Text: {
    color: '#000',
  },

  category_Text: {
    textAlign: 'left',
    color: '#fff',
    fontSize: 21,
    padding: 10,
    paddingLeft:hp('2.5%')
  },

  category_View: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: color.ash
  },

  Btn: {
    backgroundColor: '#FF6F00'
  },
  fontarrowicon:{
    fontSize:hp('2.5%'),
    paddingRight:hp('2%'),
    color:color.black1
  }

});