import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Alert,StatusBar,Modal,TouchableHighlight,TouchableOpacity}
 from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Icon,Subtitle,Header,Content,Body,Right,Title,Container } from 'native-base';
import color from '../Helpers/color'

class ModalComp extends React.Component{
  constructor(props) {
    super(props);
    this.state = { modalVisible: props.visible};
  }

  setModalVisible=(visible)=>{
    this.setState({modalVisible: visible});
    if(this.props.closemodal){
      this.props.closemodal(visible);
    }
  } 


componentWillReceiveProps=(props)=>{
    // this.setModalVisible(false); 
   
    this.setState({modalVisible:props.visible})
  
} 



	render(){
		 var modalBackgroundStyle = {
      backgroundColor: 'rgba(0, 0, 0, 0.5)'
    };
    var innerContainerTransparentStyle = {overflow:'scroll',backgroundColor: '#fff',width:'96%',
    marginLeft:'2%',borderRadius:5,alignItems:this.props.header==true?'stretch':(this.props.center?'stretch':'flex-end'),flexDirection:this.props.header==true?'column':(this.props.center?'row':'row')};
		return(


        <Modal

         	animationType='slide'
          transparent={true}
          visible={this.state.modalVisible}
          >
          <View  style={[styles.container, modalBackgroundStyle,{alignItems:this.props.center?'center':'flex-end'}]}  >
            <View style={[innerContainerTransparentStyle]}>
            {this.props.header==true&&
              	 <View  style={styles.modalheader}>
                 <Body style={{alignItems:'flex-start',justifyContent:'center',paddingLeft:22}}>
            <Title style={{color:this.props.titlecolor,fontSize:hp('4%')}}>{this.props.modaltitle}</Title>
            {this.props.modalsubtitle&&
            <Subtitle style={{color:color.black,fontSize:hp('2.2%')}}>{this.props.modalsubtitle}</Subtitle>
          }
            </Body>
            <View style={{position:'absolute',right:18,top:12,zIndex:99}}>
          <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Icon type="Ionicons" name="close" style={{fontSize:hp('5%')}}/>
              </TouchableHighlight>
              </View>
                 </View>
            }
            {!this.props.header&&
              <View style={styles.closemodal}>
            <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Icon type="Ionicons" name="close" style={{fontSize:hp('5%')}}/>
              </TouchableHighlight>
              </View>
            }
            <Content>
               {this.props.children}
               </Content>
              </View>
            </View>

        </Modal>

			)
	}
}
export default ModalComp;
const styles = StyleSheet.create({
  container: {
    flex: 1,
   	backgroundColor: '#ecf0f1',
     alignItems:'flex-end',
     flexDirection:'row'
  },
  modalheader:{
    backgroundColor:'white',
    flexDirection:'row',
    alignItems:'center',
  },
  closemodal:{
    position:'absolute',
    top:12,
    right:24,
    zIndex:99
  }
});
