import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'

export default class Modeldropdowncomp extends Component{
    containerStyle=()=>{
        return {
     //     height:hp(this.props.height),
        }
      } 
    render(){
        var containerstyle=this.containerStyle();
        return(
            <View>
                <ModalDropdown 
                    style={[styles.dropdown_container,containerstyle]}
                    textStyle={[styles.dropdown_text]}
                    dropdownStyle={[styles.dropdown_options,containerstyle]}
                    defaultValue={this.props.drop_items}
                    showsVerticalScrollIndicator={false}	
                    dropdownTextStyle={{paddingRight:50}}
                    options={this.props.values}
                    //options={['Celebrate', 'Play','Party','Gather','Meet','Conference']}
                     />                        
            </View>
        )
    }
}
const styles=StyleSheet.create({
    dropdown_container: {
        width:'100%',
        height:hp('4%'),
        justifyContent:'center',
        alignSelf:'center',
       
  },
  dropdown_text:{
    //   fontSize: hp('2%'),
       justifyContent:'center',
     
     },
     dropdown_options:{
       marginTop:10,
       width:wp('30%'),
       minHeight:hp('8%')
      
     },

})