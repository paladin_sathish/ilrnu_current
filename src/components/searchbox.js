import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,TextInput,} from 'react-native';
import {Right,Top,Icon, Button} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import search from '../images/Search.png'

export default class Searchbox extends Component{
    render(){
        return(
            <View style={styles.container}>
       
                <View style={styles.SectionStyle}> 
                    <TextInput
                        style={{flex:1,fontSize:hp('2%')}}
                        placeholder={this.props.placeHolder}
                        underlineColorAndroid="transparent"
                    />
                     <Image source={search} style={styles.ImageStyle} />
        
                </View>
     
          </View>
        )
    }
}
const styles=StyleSheet.create({
    container: {
        backgroundColor:color.white,
      },
      
      SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor:color.ash6,
        height:hp('5%'),
    },
     
    ImageStyle: {
        padding: 5,
        margin: 5,
        height: hp('2%'),
        width: hp('2%'),
        resizeMode : 'stretch',
        alignItems: 'center'
    },
     
})