'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

{
  /*  onPress={() =>
              this.props.changePagination && this.props.changePagination(key)
            } */
}
class DynamicPagination extends Component {
constructor(props) {
  super(props);

  	  var arraylist=[];
 for(var i=0;i<props.dots;i++){
		arraylist.push(i);
	}
	  this.state = {activedot:0,arraylist:arraylist};
}

  render() {
    return (
      <View>
      <View style={styles.flexrow}>
      {this.state.arraylist.map((obj,key)=>{
      	var percent=""+(100/this.state.arraylist.length)+"%";
      	return (
          <TouchableOpacity
            onPress={() =>
              this.props.changePagination && this.props.changePagination(key)
            }
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "space-between",
              height: "100%"
            }}
          >
            <View style={{ justifyContent: "center", flexDirection: "row" }}>
              <Text
                style={{
                  textAlign: "center",
                  width: wp("1%"),
                  height: wp("1%"),
                  backgroundColor: "white"
                }}
              ></Text>
            </View>
            <View>
              <Text
                style={{
                  textAlign: "center",
                  color: "white",
                  fontSize: hp("1.5%"),
                  opacity: this.props.activeid == obj ? 1 : 0
                }}
              >
                {obj + 1} of {this.state.arraylist.length}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
      </View>
      </View>
    );
  }
}

const styles = {
flexrow:{flex:1,flexDirection:'row',alignItems:'flex-start',backgroundColor:color.blueactive,minHeight:28}
};


export default DynamicPagination;