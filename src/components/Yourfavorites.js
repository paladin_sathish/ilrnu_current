import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import Greyright from '../images/greyright.png'
import ModalDropdown  from 'react-native-modal-dropdown'
import links from '../Helpers/config';

const fav_list=[
    {fav:'Birthday Party',id:0},
    {fav:'Soccer Ground',id:1},
    {fav:'Family Gathering Venues',id:2},
    {fav:'Meeting Hall',id:3},
    {fav:'Wedding Venue',id:4},
   ];

export default class YourFavorites extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                     listFav: [],
                     scrollViewWidth: 0,
                     currentXOffset: 0
                   };
                 }
                 componentWillMount() {
                   this.LookforList();
                 }

                 _handleScroll = event => {
                   console.log(
                     "currentXOffset =",
                     event.nativeEvent.contentOffset.x
                   );
                   newXOffset = event.nativeEvent.contentOffset.x;
                   this.setState({ currentXOffset: newXOffset });
                 };

                 leftArrow = () => {
                   eachItemOffset = this.state.scrollViewWidth / 10; // Divide by 10 because I have 10 <View> items
                   _currentXOffset = this.state.currentXOffset - eachItemOffset;
                   this.refs.scrollView.scrollTo({
                     x: _currentXOffset,
                     y: 0,
                     animated: true
                   });
                 };

                 rightArrow = () => {
                   eachItemOffset = this.state.scrollViewWidth / 10; // Divide by 10 because I have 10 <View> items
                   _currentXOffset = this.state.currentXOffset + eachItemOffset*4.5;
                   this.refs.scrollView.scrollTo({
                     x: _currentXOffset,
                     y: 0,
                     animated: true
                   });
                 };

                 LookforList = () => {
                   fetch(links.APIURL + "youMayLookFor", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: ""
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       // console.log("you may look for response",responseJson.data);
                       this.setState({ listFav: responseJson });
                       // alert(JSON.stringify(responseJson));
                     });
                 };
                 dropdownShow = data => {
                   // alert(JSON.stringify(data));
                   return false;
                 };

                 scrollRight = () => {
                   console.log("fired");
                 };
                 render() {
                   return (
                     <View style={styles.container}>
                       <View style={[styles.flex_row, styles.text_center]}>
                         <Text style={styles.text_near}> Your </Text>
                         <ModalDropdown
                           style={{ marginRight: 3 }}
                           textStyle={[styles.text_fav]}
                           dropdownStyle={styles.dropdown_options}
                           defaultValue={"Favorites"}
                           showsVerticalScrollIndicator={false}
                           onDropdownWillShow={this.dropdownShow}
                           options={[
                             "Near You",
                             "Already Booked",
                             "with OFFER *",
                             "Low Cost"
                           ]}
                         />
                         {/*<Image source={Down_arrow} style={styles.down_aarow}></Image>*/}
                       </View>
                       <View style={styles.flex_row}>
                         
                         <ScrollView
                           horizontal={true}
                           showsHorizontalScrollIndicator={false}
                           contentContainerStyle={{
                             alignItems: "center"
                           }}
                           pagingEnabled={true}
                           ref="scrollView"
                           onContentSizeChange={(w, h) =>
                             this.setState({ scrollViewWidth: w })
                           }
                           scrollEventThrottle={16}
                           scrollEnabled={true} // remove if you want user to swipe
                           onScroll={this._handleScroll}
                         >
                           {this.state.listFav &&
                             this.state.listFav.map(item => {
                               return (
                                 <TouchableOpacity
                                   style={styles.square}
                                   onPress={() =>
                                     this.props.sendFavouries &&
                                     this.props.sendFavouries(item)
                                   }
                                 >
                                   <Text style={styles.text_style}>
                                     {item.venue_cat_name}
                                   </Text>
                                 </TouchableOpacity>
                               );
                             })}
                         </ScrollView>

                         <View style={styles.right_arr_container}>
                           <TouchableOpacity onPress={() => this.rightArrow()}>
                             <Image
                               source={Greyright}
                               style={styles.right_arr_style}
                             ></Image>
                           </TouchableOpacity>
                         </View>
                       </View>
                       {this.state.listFav.length == 0 && (
                         <View>
                           <Text style={{ textAlign: "center" }}>
                             No Records
                           </Text>
                         </View>
                       )}
                     </View>
                   );
                 }
               }
const styles=StyleSheet.create({
    container:{
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        backgroundColor:color.white
    },
    square:{
        margin:5,
        height:hp('8%'),
        width:hp('10%'),
        borderRadius:5,
        backgroundColor:color.orange,
        justifyContent:'center',
        padding:2
            
    },
    text_style:{
        color:color.white,
        textAlign:"center",
        fontSize:hp('1.6%'),
    },
    flex_row:{
        flexDirection:'row'
    },
    text_center:{
        alignItems:'center',
    },
    text_near:{
        fontSize:hp('1.9%'),
       
    },
    text_fav:{
        fontSize:hp('1.9%'),
        color:color.blue,
        fontWeight:'bold'    
    },
    down_aarow:{
        height:hp('1%'),
        width:hp('1%'),
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignSelf:'flex-end',
        alignContent:'flex-end',
        margin:3
    },
    right_arr_style:{
        height:hp('2%'),
        width:hp('2%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        margin:3
    },
    right_arr_container:{
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        backgroundColor:color.white
    },
    fav_item_container:{
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        paddingLeft:('1%')
    },
    add_symbol_container:{
        backgroundColor:color.blue,
        justifyContent:'flex-end',
        justifyContent:'center',
        alignSelf:'flex-end',
        marginRight:wp('5%'),
        height:hp('2.5%'),
        width:hp('2.5%'),
       
    },
    
})