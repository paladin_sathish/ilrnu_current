import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Alert,StatusBar,Modal,Image,TouchableHighlight,TouchableOpacity,ScrollView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input} from 'native-base';
import color from '../Helpers/color';
import CountryList from '../components/countrypicker';
import Toast from "react-native-simple-toast"; 
import links from '../Helpers/config';
export default class ForgotPassword extends React.Component{
	constructor(props){
		super(props);
		this.state={
		  callingCode:'91',
      mobile:'',
			Disabled:true,
            otp0:"",otp1:"",otp2:"",otp3:"",
            otpInput:[],disabled:false
			
		}

	}
	 otpTextInput = [];
componentWillReceiveProps(props){

}
	 

	verify=()=>{

    // Actions.signup();
    // var obj={'type':'disclaimer'};
    var obj=this.props.signupdata;
   
    obj.type='password';
   
      // alert(JSON.stringify(obj));
    var otpdata=this.state.otp0.toString()+this.state.otp1.toString()+this.state.otp2.toString()+this.state.otp3.toString();
    this.setState({disabled:true})
fetch(links.APIURL+'checkOTP', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'mobileNumber':this.props.signupdata.mobile.mobile,
        otp:otpdata

      }),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      if(responseJson.status==0){
this.setState({disabled:false})
        console.log("responseJson",responseJson);
         this.props.loadotp(obj);
    // alert("OTP send Successfully");
  }
  else{
    this.setState({disabled:false})
    alert(responseJson.msg);
  }
})


  }
	verification =() =>{
    var otpdata=this.state.otp0.toString()+this.state.otp1.toString()+this.state.otp2.toString()+this.state.otp3.toString();
//     
this.props.sendotp(otpdata);    
    console.log("otpdata",otpdata);
}
	
changeText=(data,key)=>{ 

     this.setState({[key]:data})
      
}

 mycode=(data)=>{
   this.setState({callingCode:data.callingCode})
 }  
onPressFlag(){
return false;
}
checkMobileNumber=()=>{ 
  var phoneno = /^\d{10}$/;
  if((this.state.mobile.match(phoneno)))
        {var obj = {
           countryCode: this.state.callingCode,
           mobileNumber: this.state.mobile
         };
     this.props.checkMobileNumber(obj);
        }
      else
        {
       Toast.show("Phone No Not Valid!!", Toast.LONG);
        // this.setState({ [key]: '' });
        return false;
        }



}
render(){
	return(
    <Content>
     <Header noShadow={true} style={{backgroundColor:'transparent',margin:12}}>
        <Body style={{alignItems:'center',justifyContent:'center',paddingTop:hp('5%')}}>
                <Title style={{color:color.orange,fontSize:hp('3.5%')}}>Forgot Password</Title>
                <Subtitle style={{color:color.black,fontSize:hp('2.2%')}}>Please Enter Your Mobile Number</Subtitle>
                </Body>
                </Header>


    
               <View style={{width:'100%',padding:12}}>
          <Item regular style={[styles.texboxpadding,styles.itemsize]}>
            <CountryList mycode={this.mycode}/>
           
            <Input placeholderTextColor={color.ash3} placeholder='Mobile Number'
             onChangeText={(data)=>this.changeText(data,'mobile')} 
             keyboardType="numeric"
             />
          </Item>
          </View>
                <View style={styles.submitbox}>
     <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-start'}}>
       <Button onPress={()=>this.props.cancelForgotPassword()} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
            <Text style={styles.actionbtntxt}>CANCEL</Text>
          </Button>
          </View>

          <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button disabled={this.state.mobile.length<7} 
          onPress={()=>this.checkMobileNumber()}
           style={[styles.actionbtn,{backgroundColor:this.state.mobile.length<7?color.ash:color.blue}]}>
           <Text style={[styles.actionbtntxt,{color:this.state.mobile.length<7?color.black:color.white}]}>Submit</Text>
          </Button>
          </View>
     </View>
                 </Content>

		)
}

}
const styles={
gridPad: { flexDirection:'row',flex:1},
	container: {
  backgroundColor: '#FFF',
    alignItems:'center',
    marginTop:hp('4%')

},
loginContainer:{
  },
  actionbtn:{
		borderRadius:5,
		width:hp('17%'),
		justifyContent:'center',
		// backgroundColor:color.orange
	},
	actionbtntxt:{
		textAlign:'center',
		color:color.white,
		fontSize:hp('2.3%')
	},
	 txtMargin: { margin: 2,},
    inputRadius: { textAlign: 'center',color:'#403c3b',backgroundColor:'transparent'},
    borderLine:{
    	width:50,
    	borderColor:'transparent'
 },
 texboxpadding:{
    marginBottom:20,
  },
  itemsize:{
    height:hp('5.5%')
  },  submitbox:{
    flex:1,
    flexDirection:'row',
    padding:12,
    borderBottomWidth:0.5
  }
}