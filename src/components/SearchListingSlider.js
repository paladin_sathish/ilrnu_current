import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,Alert,FlatList,} from 'react-native';
import {} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import StarRating  from 'react-native-star-rating'
import Tick_mark from '../components/TickComp'

const near_ground_list=[{id:1,starCount:3,image:require('../images/slider1.png'),ground_name:'Brand new indoor soccer center',kms:'10 kms',mins:'35 mins'},
                        {id:2,starCount:4,image:require('../images/slider2.png'),ground_name:'Soccer Club Academy',kms:'5 kms',mins:'15 mins'},
                        {id:3,starCount:2.5,image:require('../images/slider3.png'),ground_name:'Best Soccer Pitche Ground',kms:'5 kms',mins:'15 mins'},
                        {id:4,starCount:4,image:require('../images/slider4.png'),ground_name:'Brand new indoor soccer center',kms:'6 kms',mins:'20 mins'}
                    ]

export default class GroundNearYou extends Component{
    constructor(props) {
        super(props);
        this.state= {
            slectdata:'',
            isHidden: false,
        }
        
    }
    onPress=(item,index)=>{ 
       // alert('hii');
     // alert(JSON.stringify(item));
        //     Alert.alert(item.ground_name)
      //  this.setState({isHidden: !this.state.isHidden})
        this.setState({isHidden:true})
          this.setState({slectdata:item.id});
      }
   

      onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
      }
    render(){
       
        return(
            <View style={styles.container}>
                <View style={styles.flex_row}>
                    <Text style={styles.text_style1}>Soccer Ground </Text>
                    <Text style={styles.text_style}>Near You </Text>
                    <View style={{flexDirection:'column',justifyContent:'center'}}>
                        <Image source={Down_arrow} style={styles.Down_arrowstyle}></Image>
                    </View>
                </View>

                <FlatList 
                    data={near_ground_list}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({item,index})=>( 
                        
                    <View style={styles.square}>
                        <TouchableOpacity       
                            onPress={()=>this.onPress(item,index)}                 
                            style={this.state.slectdata === item.id ?  
                            backgroundColor:color.black}  > 

                            <View style={{marginTop:10}}>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={item.starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    halfStarColor={color.blue}
                                    fullStarColor={color.blue}
                                    starSize={8}
                                    containerStyle={styles.RatingBar}
                                    starStyle={styles._starstyle}
                                    emptyStarColor={color.ash6}
                                />
                            </View>       
                    
                            <Image style={styles.image_style} source= {item.image}>               
                            </Image>
                            <View style={{position:'absolute',top:hp('8%'),left:hp('2%')}}>
                        
                           {this.state.isHidden == true ? 
                         <View><Text>asdfasdf</Text></View>:<View><Text>asdfasdf1</Text></View>
                            }
 
                            </View>             
                            <Text style={styles.text_style2}>
                            {item.ground_name}
                            </Text>    
                            <View style={[styles.flex_row,{marginTop:5}]}>
                                <Text style={styles.text_style3}>10 Kms </Text>
                                <Text style={styles.text_style3}> | </Text>
                                <Text style={styles.text_style3}> 15 mins </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    )}/> 
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    flex_row:{
        flexDirection:'row'
    },
    text_style1:{
        fontSize:hp('2%')
    },
    text_style:{
        color:color.blue,
        fontSize:hp('2%')
    },
    Down_arrowstyle:{
        height:hp('1.4%'),
        width:hp('1.4%')
    },
    RatingBar:{
       height:hp('2.5%'),
       width:wp('13%')
    },
    _starstyle_starstyle:{
        marginRight:1
    },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
    image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },
    text_style2:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
    },
    text_style3:{
        color:color.orange,
        fontSize:hp('1.4%')
    },
    selected:{
        backgroundColor:color.blue
    }

  
})