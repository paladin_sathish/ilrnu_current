import Autocomplete from 'react-native-autocomplete-input';
import React, { Component } from 'react';
import {
  Modal,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Alert,
  PermissionsAndroid,
  StyleSheet,
  Dimensions,
} from "react-native";
import color from "../Helpers/color";
import links from "../Helpers/config";
import {
  Icon,
  Subtitle,
  Header,
  Content,
  Body,
  Right,
  Title,
  Container,
} from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
const API = 'https://swapi.co/api';
const ROMAN = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII'];

class AutocompleteExample extends Component {
  

  constructor(props) {
    super(props);
    this.state = {
      films: [],
      query: "",
      modalVisible: props.modalstate,
    };
  }
  componentWillReceiveProps(props) {
    if (props.modalstate) {
      this.setState({ modalVisible: props.modalstate });
      // this.setState({addressText:props.userAddress})
      // this.changeText(props.userAddress);
    }
  }
  componentDidMount() {
    fetch(`${API}/films/`)
      .then((res) => res.json())
      .then((json) => {
        const { results: films } = json;
        this.setState({ films });
      });
  }

  findFilm(query) {
    if (query === "") {
      return [];
    }

    const { films } = this.state;
    const regex = new RegExp(`${query.trim()}`, "i");
    return films.filter((film) => film.title.search(regex) >= 0);
  }


   hideAddressbar = () => {
    this.setState({ modalVisible: false });
    // this.props.receiveAddress({usertxt:this.state.addressText})
    this.setState({ typeText: "" });
    this.props.onClose && this.props.onClose();
  };



  render() {
    const { query } = this.state;
    const films = this.findFilm(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            // this.setState({})
            this.hideAddressbar();
            // Alert.alert("Modal has been closed.");
          }}
          onDismiss={()=>this.props.onClose(false)}
        >
          <View
            style={{ position: "absolute", right: 18, top: 12, zIndex: 99 }}
          >
            <TouchableHighlight
              onPress={() => {
                 this.hideAddressbar();
              }}
            >
              <Icon
                type="Ionicons"
                name="close"
                style={{ fontSize: hp("5%") }}
              />
            </TouchableHighlight>
          </View>

          <View style={styles.container}>
            <Autocomplete
              autoCapitalize="none"
              autoCorrect={false}
              containerStyle={styles.autocompleteContainer}
              data={this.props._data}
              defaultValue={this.props.value}
              onChangeText={(text) => this.props._onChange(text)}
              placeholder="Enter Text"
              renderItem={({ item, i }) => (
                <TouchableOpacity
                  style={{
                    position: "relative",
                    paddingVertical: 15,
                    fontSize: 22,
                    height: 40,
                    width:'100%',
                    textAlign: "center",
                  }}
                  onPress={() =>
                    this.props._onClick(item.course_title)
                  }
                >
                  <Text>{item.course_title}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F5FCFF",
    flex: 1,
    paddingTop: 65,
  },
  autocompleteContainer: {
    marginLeft: 5,
    marginRight: 5,
  },
  itemText: {
    fontSize: 15,
  },
  descriptionContainer: {
    // `backgroundColor` needs to be set otherwise the
    // autocomplete input will disappear on text input.
    backgroundColor: "#F5FCFF",
    marginTop: 8,
  },
  infoText: {
    textAlign: "center",
  },
  titleText: {
    fontSize: 18,
    fontWeight: "500",
    marginBottom: 10,
    marginTop: 10,
    textAlign: "center",
  },
  directorText: {
    color: "grey",
    fontSize: 12,
    marginBottom: 10,
    textAlign: "center",
  },
  openingText: {
    textAlign: "center",
  },
});

export default AutocompleteExample;