import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  StatusBar,
  Modal,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  Container,
  Icon,
  Button,
  Subtitle,
  Header,
  Content,
  Body,
  Right,
  Title,
  Item,
  Input,
  Toast,
  Root,
} from "native-base";
import color from "../Helpers/color";
import { Actions, Router, Scene } from "react-native-router-flux";
import ModalComp from "../components/Modal";
export default class Disclaimer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { agree: false, opentoast: false };
  }
  onClose = () => {
    // alert("");
    this.setState({ opentoast: null });
  };
  sharemore = () => {
    // alert("share more")
    if (this.props.loadsharemore) {
      var obj = { type: "sharemore" };
      if (this.state.agree == false) {
        this.props.loaderrormessage("Please Agree the condition");
      } else {
        var obj = this.props.signupdata;
        obj.type = "sharemore";
        obj.agree = this.state.agree == true ? 1 : 0;
        this.props.loadsharemore(obj);
      }
    }
  };

  render() {
    return (
      <Content style={{ flex: 1 }}>
        <View>
          {/* <Header
          noShadow={true}
		  style={{ backgroundColor: "transparent",
		   marginHorizontal:12,marginTop:12,marginBottom:6}}
        >
          <Body
            style={{
              alignItems: "flex-start",
              justifyContent: "center",
              padding: 0,
            }}
          >
            <Title style={{ color: color.orange, fontSize: hp("4%") }}>
              Disclaimer
            </Title>
          </Body>
        </Header> */}
          <View>
            <View
              style={{
                alignItems: "flex-start",
                justifyContent: "center",
                paddingLeft: 8,
                marginHorizontal: 12,
        
              }}
            >
              <Title style={{ color: color.orange, fontSize: hp("4%") }}>
                Disclaimer
              </Title>
            </View>
          </View>
          <View style={{ padding: 18 }}>
            <Text style={styles.headingUser}>
              Welcome{" "}
              <Text style={{ color: color.orange }}>
                {this.props.signupdata.name},
              </Text>
            </Text>
            <Text>
              Information presented on this website, is considered public
              information (unless otherwise noted). We will make any legally
              required disclosures of any breach of the security,
              confidentiality, or integrity of your unencrypted electronically
              stored "personal data" (as defined in applicable state statutes on
              security breach. The site governs the manner in which iLRNU
              collects, uses, maintains and discloses information collected from
              users of the https://www.iLRNU.com/" website (“Site”). This
              privacy policy applies to the Site and all products and services
              offered by iLRNU (collectively referred to as “Applications”).This
              policy also describes your data protection rights, including a
              right to object to some of iLRNU processing. The Policy does not
              apply to information collected by any third party, including
              through any third-party application or content (including
              advertising) that links to or is accessible from our Applications
              or websites.Please note that iLRNU does not control and cannot
              guarantee the relevance, timeliness, or accuracy of these outside
              materials.
            </Text>
            <View style={{ flexDirection: "row", paddingTop: 12 }}>
              <View style={{ flex: 0.5, alignItems: "flex-start" }}>
                <Button
                  iconLeft
                  transparent
                  style={{ width: "auto" }}
                  onPress={() => this.setState({ agree: !this.state.agree })}
                >
                  <View style={styles.checkbox}>
                    <View
                      style={[
                        styles.checkboxinner,
                        {
                          backgroundColor:
                            this.state.agree == true
                              ? color.orange
                              : color.ash4,
                        },
                      ]}
                    ></View>
                  </View>
                  <Text
                    style={{
                      textAlign: "left",
                      paddingLeft: 6,
                      fontSize: hp("2.2%"),
                    }}
                  >
                    I Agree
                  </Text>
                </Button>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <Button
                  onPress={() => this.sharemore()}
                  style={[styles.actionbtn, { backgroundColor: color.orange }]}
                >
                  <Text style={styles.actionbtntxt}>Next</Text>
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Content>
    );
  }
}
const styles = {
  headingUser: {
    fontSize: hp("2.8%"),
    color: color.ash3,
  },
  checkbox: {
    borderWidth: 1,
    width: hp("3%"),
    height: hp("3%"),
    alignItems: "center",
    // padding:5,
    borderColor: color.ash3,
    justifyContent: "center",
  },
  checkboxinner: {
    position: "absolute",
    width: hp("1.5%"),
    height: hp("1.5%"),
    backgroundColor: "grey",
  },
  actionbtn: {
    borderRadius: 5,
    minWidth: hp("23%"),
    justifyContent: "center",
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2%"),
    paddingLeft: 10,
    paddingRight: 10,
  },
};
