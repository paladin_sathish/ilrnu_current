import React,{Fragment} from "react";
import {
  SafeAreaView,
  TouchableOpacity,
  TouchableWithoutFeedback ,
  FlatList,
  StyleSheet,
  Text,
  View
} from "react-native";
import {Icon} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";


const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "First Item"
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Second Item"
  }
];  





function RenderBox ({item,onEdit,onDelete})  {
   return item.map(value => {
    
     return (
       <View style={styles.box}>
         <TouchableOpacity style={styles.button}>
           <Text
             style={{
               textAlign: "center",
               color:'white'
             }}
           >
             {value.label}
           </Text>
         </TouchableOpacity>
       </View>
     );
   });
 };



function RenderItem({ data,onEdit,onDelete,index }) {  
 console.log('render once again ')
  return (
    <Fragment>
      <View style={{ flexDirection: "row" }}>
        <Text style={{ fontSize: 17 }}>{data.slots_name}</Text>
        <Text>{"     "}</Text>
        <TouchableOpacity onPress={() => onEdit(data, index)}>
          <Icon
            type="FontAwesome"
            name="pencil"
            style={{ color: "blue", fontSize: 16, paddingTop: 3 }}
          />
        </TouchableOpacity>

        <Text>{"     "}</Text>
        <TouchableOpacity onPress={() => onDelete(data,index)}>
          <Icon
            type="FontAwesome"
            name="trash-o"
            style={{ color: "orange", fontSize: 16, paddingTop: 3 }}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.container}>
        <RenderBox item={data.value} onEdit={onEdit} onDelete={onDelete} />
      </View>
    </Fragment>
  ); 


}

export default function SplitView({data,onEdit,onDelete}) {
  const [selected, setSelected,] = React.useState(new Map());
  const [splitView,setsplitView]=React.useState(data);
  console.log('called',data) 

  const onSelect = React.useCallback(
    id => {
      const newSelected = new Map(selected);
      newSelected.set(id, !selected.get(id));

      setSelected(newSelected);
    },
    [selected]
  );

  return (
    <SafeAreaView style={styles.app}>
      <View style={{flexDirection:'column',display: 'flex',}}>

      {data.length>0&&data.map((item,index)=>{
        return (
          <RenderItem
            data={item}
            onEdit={onEdit}
            onDelete={onDelete}
            index={index}
          />
        );

      })}
      </View>
      
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16
  },
  title: {
    fontSize: 32
  },
  button: {
    backgroundColor: "#eb5b00",
    width: wp("40%"),
    marginTop: 4,
    padding: 8
  },
  box: {
    padding: wp("1%"),

  },
   app: {
    display: "flex",
    flexDirection: "row",
    padding: 20,
    minHeight:150,
    justifyContent: "space-between",
    flexWrap: "wrap"
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap"
  }
});
