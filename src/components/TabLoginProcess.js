import React, { Component } from 'react';
import {Image, Platform,TouchableHighlight, StyleSheet, Text, View, BackHandler, Alert, TouchableOpacity,ToastAndroid,ScrollView,ActivityIndicator } from 'react-native';
import {H3,Container, Content, Header, Left, Body, Right, Button, Icon, Title,List,ListItem,Root } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Toaster, { ToastStyles } from 'react-native-toaster'
import Toast from 'react-native-simple-toast';

// import HeaderComp from '../components/Header';
// import SubHeaderComp from '../components/Subheader';
// import FooterComp from '../components/Footer';
// import DropDown from '../components/dropdown';
import ModalComp from './Modal';
// import SessionList from '../components/sessionList';
import Login from './Login';
import Signup from './Signup';
import SetPassword from './setpassword';
import Disclaimer from './Disclaimer';
import OtpScreen from './otpscreen';
import ShareMore from './ShareMore';
import ForgotPassword from './forgotpassword';
import color from '../Helpers/color';
import links from '../Helpers/config';
// import LoginRoutes from '.././loginroutes';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class TabLoginProcess extends Component {
	constructor(props){
		super(props);
		this.state={forgotdata:null,forgot:false,loading:null,type:this.props.type,headervisible:false,modalsubtitle:null,title:null,visible:true,loadtoast:null};
	}
	componentWillReceiveProps(props){
		// alert('alert');
		// alert(JSON.stringify(props));
		// this.setState({visible:true});
		if(this.props.type=='signup'){
		
		this.setState({headervisible:true})
		this.setState({type:this.props.type})
		}else{
this.setState({headervisible:false})
this.setState({modalsubtitle:null})
		this.setState({type:this.props.type})
		}
	}
	modalclose=()=>{
		this.setState({visible:true});

	}
	
	submitlogin=(obj)=>{
		this.setState({loading:true})
		fetch(links.APIURL+'venuLogin', {
     method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({

'email':obj.email,
    'password':obj.password,
      }),
}).then((response)=>response.json())
   .then((responseJson)=>{
   	this.setState({loading:null})
  
if(responseJson.status==0){
 AsyncStorage.setItem("loginDetails",JSON.stringify(responseJson.data[0]));
 this.setState({visible:false});
 // alert("User Logged in Successfully");
                  // Toast.show('User Logged in Successfully', Toast.LONG)
 
// if(this.props.successlogin){
// 	this.props.successlogin(true);
// 	var self=this;
// 	setTimeout(()=>{
// 		self.props.closetabmodal();
// 		self.setState({visible:false});
// 	},1000);
// }
  // Actions.Venue_search();

 	if(this.props.sendlogindet){
 	this.props.sendlogindet(responseJson.data[0]);
 	}
}else{
	 	 // Toast.show({
    //             text: "Invalid Username (or) Password!",
    //             buttonText: "Okay",
    //             duration: 3000,
    //             type:"danger"
    //           })
                  Toast.show('Invalid Username (or) Password!', Toast.LONG)

   // ToastAndroid.show('Invalid Username and Password', ToastAndroid.SHORT);
}
   }).catch((response)=>{
                  Toast.show('Network Failed', Toast.LONG)
 
   	this.setState({loading:null})
   })
		// Actions.venueform();
	}
	onClose=()=>{
		this.setState({loadtoast:null})
	}
	loaderrormessage=(msg)=>{
		this.setState({loadtoast:true});
		var self=this;
                  Toast.show(msg, Toast.LONG)
	// 	setTimeout(()=>{
	// 	Toast.show({
 //                text: msg?msg:'',
 //                buttonText: "Okay",
 //                duration: 3000,
 //                onClose:()=>{self.onClose()}
 //              	});
	// },200)
	}
	submitData=(data)=>{
		// alert('hii');
		// alert(JSON.stringify(data));
		var signupdata=data.signupdata;
		// this.setState({loading:true})
		var obj={catId:'1',typeId:2,name:signupdata.name,password:signupdata.pass,'surname':signupdata.surname, 'location':signupdata.location, 'mobileno':signupdata.mobile.mobile,'ccode':signupdata.mobile.countrycode, 'mail':signupdata.mail, 'dob':signupdata.dob.yy+"-"+signupdata.dob.mm+"-"+signupdata.dob.dd, 'disclaimer':signupdata.agree,questionAnswer:signupdata.questionAnswer};
		// alert(JSON.stringify(obj));
		fetch(links.APIURL+'venuSignup/', {
	     method: 'POST',
	   	headers: {
	     Accept: 'application/json',
	     'Content-Type': 'application/json',
	   },
	   body: JSON.stringify(obj),
	 }).then((response)=>response.json())
	    .then((responseJson)=>{
	    	// alert(JSON.stringify(responseJson));

			this.setState({loading:null})
			this.setState({type:'login',forgot:false})
                  Toast.show("user added successfully...", Toast.LONG)


	    	// alert('user added successfully..');
	    }).catch((data)=>{
	    	alert(data);
	    })

	}
	closemodal=()=>{
		if(this.props.closetabmodal){
			this.props.closetabmodal();
		}
	}
	loadsignup=(data)=>{
		// alert(JSON.stringify(data));
		if(data.type=='signup'){
		this.setState({title:'Signup'})
		// this.setState({modalsubtitle:'A Venue Provider'})
		// this.setState({headervisible:true})
		}else if(data.type=='disclaimer'){
		// this.setState({title:'Disclaimer'})
		// this.setState({modalsubtitle:null})
		// this.setState({headervisible:true})
		}else if(data.type=='otpscreen'){
		// this.setState({title:'Disclaimer'})
		// this.setState({modalsubtitle:null})
		// this.setState({headervisible:true})
		}else if(data.type=='password'){
		this.setState({modalsubtitle:null})
		this.setState({headervisible:false})
		}
		this.setState({type:data.type})
		this.setState({data:data})
	}
	checkMobileNumber=(data)=>{
		this.setState({loading:true})
		// alert(JSON.stringify(data));
		// alert(JSON.stringify(obj));
		fetch(links.APIURL+'forgotOTP', {
	     method: 'POST',
	   	headers: {
	     Accept: 'application/json',
	     'Content-Type': 'application/json',
	   },
	   body: JSON.stringify(data),
	 }).then((response)=>response.json())
		.then((responsejson)=>{
			this.setState({loading:false})
			if(responsejson.status==0){
				// alert(JSON.stringify(responsejson.data))
				responsejson.data[0].mobildata=data;
				this.setState({forgotdata:responsejson.data[0]})
				this.setState({type:'otpscreen'});

			}else{
                  Toast.show(responsejson.msg, Toast.LONG)

				// alert(responsejson.msg);

				// Toast.show({
    //             text: responsejson.msg?responsejson.msg:'',
    //             buttonText: "Okay",
    //             duration: 3000,
    //             type:'danger',
    //             onClose:()=>{this.onClose()}
    //           	});
			}
			// alert(JSON.stringify(responsejson));
		})
	}
	loadForgotPasword=(data)=>{
		this.setState({type:data.type});
		this.setState({forgot:true});
	}
	resetPassword=(data)=>{
		this.setState({loading:true})
 fetch(links.APIURL+"updatePassword",{method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({mobileNumber:this.state.forgotdata.mobildata.mobileNumber,password:data})})
            .then((resp)=>resp.json())
            .then((respjson)=>{
              console.log("respjson",respjson);
            this.setState({loading:false});
            if(respjson.status==0){
            	this.setState({type:'login',forgot:false})
              // this.props.gobacktoLogin&&this.props.gobacktoLogin();
                  Toast.show('Password Resets Successfully', Toast.LONG)
          
            }else{
                  Toast.show('Password Resets Failed', Toast.LONG)
            }

            })
	}
	sendSocialMedia=(data)=>{
		console.log("socialMediaData",data);
		this.setState({loading:true});
		 fetch(links.APIURL+'socialMediaLogin', {
     method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
}).then((response)=>response.json())
   .then((responseJson)=>{
   	// console.log(responseJson);
   	this.setState({loading:null})
   	if(responseJson.status==0){
 AsyncStorage.setItem("loginDetails",JSON.stringify(responseJson.data[0]));
 this.setState({visible:false});
 // alert("User Logged in Successfully");
                  // Toast.show('User Logged in Successfully', Toast.LONG)
 
// if(this.props.successlogin){
// 	this.props.successlogin(true);
// 	var self=this;
// 	setTimeout(()=>{
// 		self.props.closetabmodal();
// 		self.setState({visible:false});
// 	},1000);
// }
  // Actions.Venue_search();

 	if(this.props.sendlogindet){
 	this.props.sendlogindet(responseJson.data[0]);
 	}
 }else{
                  Toast.show('Problem in receiving data', Toast.LONG)

 }

}).catch((response)=>{
                  Toast.show('Network Failed', Toast.LONG)
 
   	this.setState({loading:null})
   })
	}
	render(){
		return(
			
			<ModalComp disabled={this.state.loading} header={this.state.headervisible}  titlecolor={color.orange} closemodal={this.closemodal} modaltitle={this.state.title} modalsubtitle={this.state.modalsubtitle} visible={this.state.visible} >
			<Root>
			{this.state.loading&&
			 <View style={{width:'100%',zIndex:9999,flex:1,justifyContent:'center',alignItems:'center',position:'absolute',height:'100%',backgroundColor:'rgba(0,0,0,0.8)',top:0}}>
        <ActivityIndicator size="large" color={color.orange} />
        <Text style={{color:color.white}}>Please Wait ....</Text>
        </View>
			}
				{this.state.type=='login'&&
					<Login socialMediaData={(data)=>this.sendSocialMedia(data)} forgotPassword={(data)=>this.loadForgotPasword(data)} loginttype={this.props.loginttype} submitlogin={this.submitlogin} loadsignup={(data)=>this.loadsignup(data)}/>
				}{this.state.type=='signup'&&
					<Signup loginttype={this.props.loginttype} loadverify={(data)=>this.loadsignup(data)} />
				}{this.state.type=='otpscreen'&&
					<OtpScreen forgot={this.state.forgot} forgotdata={this.state.forgotdata} loadotp={(data)=>this.loadsignup(data)} signupdata={this.state.data}/>
				}{this.state.type=='password'&&
					<SetPassword resetPassword={(data)=>this.resetPassword(data)} forgotdata={this.state.forgotdata} forgot={this.state.forgot} loginttype={this.props.loginttype} loaddisclaimer={(data)=>this.loadsignup(data)} signupdata={this.state.data} />
				}{this.state.type=='disclaimer'&&
					<Disclaimer  loaderrormessage={(msg)=>this.loaderrormessage(msg)} loadsharemore={(data)=>this.loadsignup(data)} signupdata={this.state.data} />
				}{this.state.type=='sharemore'&&
					<ShareMore modalclose={()=>this.modalclose()} signupdata={this.state.data} submitData={this.submitData} />
				}{this.state.type=='forgot'&&
					<ForgotPassword checkMobileNumber={(data)=>this.checkMobileNumber(data)} cancelForgotPassword={()=>this.setState({type:'login',forgot:false})}/>
				}
				
			</Root>
			</ModalComp>
			)
	}
}