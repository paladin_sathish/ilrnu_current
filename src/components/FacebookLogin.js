import React, { Component } from 'react';
import {View,Text} from 'react-native';
import PropTypes from 'prop-types';
import Toast from 'react-native-simple-toast';
var { FBLogin, FBLoginManager } = require('react-native-facebook-login');
export default class FacebookLogin extends Component {
  render() {
    var _this = this;
    return (
      <FBLogin style={{ marginBottom: 10, }}
        ref={(fbLogin) => { this.fbLogin = fbLogin }}
        permissions={["email","user_friends"]}
        buttonView={this.props.children}
        onClickColor={"transparent"}
        loginBehavior={FBLoginManager.LoginBehaviors.NativeOnly}
        onLogin={function(data){
          console.log("Logged in!");
          console.log(data);
          var userData=data.profile;
          var obj={firstName:userData.first_name,lastName:userData.last_name,picture:userData.picture?userData.picture.data.url:null,email:userData.email,oauthProvider:'facebook',outhId:userData.id};
          _this.props.sendSocialDetails&&_this.props.sendSocialDetails(obj);

          // _this.setState({ user : data.credentials });
        }}
        onLogout={function(){
          console.log("Logged out.");
          // _this.setState({ user : null });
          
        }}
        onLoginFound={function(data){
          console.log("Existing login found.");
          console.log(data);
          // _this.setState({ user : data.credentials });
        }}
        onLoginNotFound={function(){
          console.log("No user logged in.");
          // _this.setState({ user : null });
        }}
        onError={function(data){
          console.log("ERROR");
          // console.log(data);
          Toast.show("facebook is not installed in your mobile",Toast.LONG);
        }}
        onCancel={function(){
          console.log("User cancelled.");
          Toast.show("User cancelled",Toast.LONG);
        }}
        onPermissionsMissing={function(data){
          Toast.show("Permission Missing",Toast.LONG);
        }}
      ></FBLogin>
    );
  }
};