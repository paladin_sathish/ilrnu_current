import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import {View,Image,StatusBar,Picker,Text,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class SubHeaderComp extends Component {
	render(){
		return(
			<View style={[styles.subheader,{backgroundColor:this.props.bgcolor}]}>
				{this.props.children}
			</View>
			)
	}
}
const styles={
	subheader:{
	}
}