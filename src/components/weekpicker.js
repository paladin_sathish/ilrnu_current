import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import CalendarStrip from "../components/weekpicker/CalendarStrip";
import moment from "moment";
import links from "../Helpers/config"; 
import dateFormat from 'dateformat';
let datesWhitelist = [
  {
    start: moment(),
    end: moment().add(7, "years") // total 4 days enabled
  }
];

export default class WeekPicker extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     selectedDate: null,
                     data:'',
                     slots:[]
                   };
                   this.setSelectedDate = this.setSelectedDate.bind(this);
                 }

                 setSelectedDate = date => {
                   console.log(date);
                 };  




                 componentWillMount = () => {
                   var start = moment();
                   this.getSlots(start)
                 };

                

                 getSlots(date){
                      
                        var formatDate = date.format("YYYY-MM-DD"); 
                        this.props.loading&&this.props.loading(true);
                        console.log(formatDate)

                   fetch(links.APIURL + "providerCalendarHourly", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       date: formatDate,
                       venueId: this.props.venueId
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       // alert(JSON.stringify(responseJson));
                       this.props.loading&&this.props.loading(false);
                       this.setState({ spinner: false });
                       if (responseJson.status == 1) {
                         this.setState({ NoRecords: false });
                         this.setState({data: []});
                         this.props.setSlots([]);
                         return;
                       }else{
  
                       }
 

if (
  responseJson.status==0&&responseJson.data.length>0&&this.props.hourlychosenslots&&this.props.hourlychosenslots.length > 0
) {
  for (var i = 0; i < responseJson.data[0].availability.length; i++) {
  // alert(JSON.stringify(this.props.hourlychosenslots))
    // debugger;
    // alert(dateFormat(this.props.selectedDate, "yyyy-mm-dd"));
    responseJson.data[0].availability[i].hourlySlots.map((obj2, index) => {
      this.props.hourlychosenslots
        .filter(
          obj5 => obj5.selectedDate == formatDate
        )
        .map(obj3 => {
          var findindex = obj3.selectedSlots.findIndex(
            obj4 =>
              obj2.venue_slot_start_time == obj4.venue_slot_start_time &&
              obj2.venue_slot_end_time == obj4.venue_slot_end_time
          );
          console.log('findindex',findindex);
          if (findindex != -1) {
             responseJson.data[0].availability[i].hourlySlots[
               index
             ].checked = true;
          }
        });
    });
  }
} 
this.setState({
  data: JSON.parse(
    JSON.stringify(responseJson.data[0].availability[0].hourlySlots)
  )
});
 


                       this.setState({
                         isLoading: false,
                         data:
                           responseJson.status == 0
                             ? JSON.parse(
                                 JSON.stringify(
                                   responseJson.data[0].availability[0]
                                     .hourlySlots
                                 )
                               )
                             : []
                       }); 



                       this.props.setSlots(this.state.data);
                       this.props.setSelected(formatDate);
                     });

                 }

                 render() {
                   const { selectedDate } = this.state;
                   return (
                     <View style={{ flex: 1 }}>
                       <CalendarStrip
                         calendarAnimation={{ type: "sequence", duration: 30 }}
                         daySelectionAnimation={{
                           type: "border",
                           duration: 200,
                           borderWidth: 2,
                           borderHighlightColor: "#eb5b00"
                         }}
                         style={{
                           height: 140,
                           paddingTop: 20,
                           paddingBottom: 20
                         }}
                         showMonth={true}
                         calendarHeaderStyle={{ color: "#959595" }}
                         calendarColor={"#f1f1f1"}
                         dateNumberStyle={{ color: "#959595" }}
                         dateNameStyle={{ color: "#959595" }}
                         highlightDateNumberStyle={{ color: "#eb5b00" }}
                         highlightDateNameStyle={{ color: "#eb5b00" }}
                         highlightColor={"#073cb2"}
                         disabledDateNameStyle={{ color: "grey" }}
                         disabledDateNumberStyle={{ color: "grey" }}
                         datesWhitelist={datesWhitelist}
                         iconContainer={{ flex: 0.1 }}
                         onDateSelected={date => this.props.setSelected(date)}
                       />

                     
                     </View>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection: 'row',
    justifyContent:'center',
    flexWrap:'wrap'
  }
});
