import React, { Component } from 'react'
import { StyleSheet, View, ScrollView, Alert, Text,Modal,TouchableHighlight,Dimensions  } from 'react-native'
import {Icon,Subtitle,Header,Content,Body,Right,Title,Container } from 'native-base';
import Video from 'react-native-af-video-player';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color'
import url from '../video/guide_video.mp4';
import ModalComp from '../components/ModalComp'
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
	container:{
		flex:1
	},
   container1: {
    flex: 1,
   	backgroundColor: '#ecf0f1',
     alignItems:'center',
     flexDirection:'row'
  },
  modalheader:{
    backgroundColor:'white',
    flexDirection:'row',
    alignItems:'center',
  },
  closemodal:{
    position:'absolute',
    top:12,
    right:24,
    zIndex:99
  }
})

class VideoComp extends Component {
 state = {
    modalVisible: true,
  };
  static navigationOptions = ({ navigation }) => {
    const { state } = navigation
    // Setup the header and tabBarVisible status
    const header = state.params && (state.params.fullscreen ? undefined : null)
    const tabBarVisible = state.params ? state.params.fullscreen : true
    return {
      // For stack navigators, you can hide the header bar like so
      header,
      // For the tab navigators, you can hide the tab bar like so
      tabBarVisible,
    }
  }

  onFullScreen(status) {
    // Set the params to pass in fullscreen status to navigationOptions
    this.props.navigation.setParams({
      fullscreen: !status
    })
  }
setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
//   onFullScreen(fullScreen) {

// }

 

  render() {

      return (
     
       <ModalComp 
        closemodal={()=>this.props.closevideo()} 
        center={true} visible={true} header={false} modaltitle={'How'} modalsubtitle={'venue'}  >
       <View style={{padding:10}}>
       <Header noShadow style={{backgroundColor:'transparent'}}>
          
          <Body>
            <Title style={{color:color.top_red}}>{this.props.headerText}</Title>
            <Subtitle style={{color:color.bluesub}}>A Guided Video</Subtitle>
          </Body>
         
       </Header>

       <Video  
       url={url}
       resizeMode="contain"
      inlineOnly={true}
       />
         </View>
       </ModalComp>

       
     
    )
  }
}

export default VideoComp