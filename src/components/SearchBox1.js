import React, {Component} from 'react';
import {Platform, StyleSheet, TextInput,Text, View,Alert,StatusBar,Modal,Image,TouchableHighlight,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input,Label,Textarea,Form} from 'native-base';
import color from  '../Helpers/color';
export default class SearchBox1 extends React.Component{
	render(){
		return(
            <View style={{backgroundColor:color.white,height:'auto'}}>          
                <TextInput  style={{padding:0,paddingLeft:12,width:'100%',borderColor:color.ash5,borderWidth:7}} 
                                placeholder="Search Here"/>
                <View style={[styles.triangle,styles.arrowUp]}/>
                <View style={[styles.triangle1,styles.arrowUp1]}/>                
            </View>
			)
	}
}
const styles = {
	
	triangle: {
        width: 0,
        height: 0,
        margin:'auto',
        backgroundColor: 'transparent',
        borderStyle: 'solid',
    },
    arrowUp: {
         position: 'absolute',
        bottom: 0,
        top:hp('-1.7%'),
        right:wp('13%'),
        borderTopWidth: 0,
        borderRightWidth: 12,
        borderBottomWidth: 12,
        borderLeftWidth: 12,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: color.ash5,
        borderLeftColor: 'transparent',
    },
    triangle1: {
        width: 0,
        height: 0,
        margin:'auto',
        backgroundColor: 'transparent',
        borderStyle: 'solid',
    },
    arrowUp1: {
        position: 'absolute',
        bottom: 0,
        top:hp('-0.6%'),
        right:wp('13%'),
        borderTopWidth: 0,
        borderRightWidth: 12,
        borderBottomWidth: 12,
        borderLeftWidth: 12,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: color.white,
        borderLeftColor: 'transparent',
    }
    
 
}