import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList,} from 'react-native';
import {Right,Top,Icon, Button} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import Sliderimage from '../images/cricket.jpg'
import Line from './Line'

export default class ListingDetails extends Component{
    render(){
        return(
            <View style={styles.container}>

                <View style={styles.view1}>
                    <View style={styles.view11}>
                        <Text style={{fontSize:hp('2%')}}>
                            United Soccer Club Academy
                        </Text>
                    </View>
                    <View style={styles.view12}>
                        <StarRating
                            maxStars={1}
                            fullStarColor={color.yellow}
                            starSize={20}
                            containerStyle={styles.RatingBar}
                            />
                    </View>    
                </View>
              
                <View style={{marginBottom:hp('1.5%')}}>
                   
                    <View style={{paddingBottom:hp('1.5%'),flexDirection:'row',height:hp('15%')}}>
                        <View style={{flex:.4,paddingTop:hp('1.5%')}}>
                            <Image source={Sliderimage} style={{height:hp('15%'),width:'100%'}}>

                            </Image>
                        </View>
                        <View style={{flex:.6,backgroundColor:color.white,flexDirection:'column'}}>
                            <View style={{flex:.8,paddingTop:hp('1.5%'),paddingLeft:hp('1.5%'),}}>
                                <Text style={{fontSize:hp('1.5%'),}}>
                                    The acadamy is concept that is based on the professional soccer club systems fromaround the world
                                </Text>
                            </View>

                             {/* <View style={{flex:.2,flexDirection:'row'}}>
                                <View style={{flexDirection:'row',flex:1}} > 
                                    <Text style={styles.orange_text}> 15 Kms </Text>
                                    <Text style={styles.orange_text}> | </Text>
                                    <Text style={styles.orange_text}> 18 mins </Text>
                                </View>
                                <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                    <Button style={[styles.booK_nw_btn]}>
                                        <Text style={[styles.booK_nw_txt]} >Book Now</Text>
                                    </Button>
                                </View>
                            </View>    */}
                        </View>
                    </View>
                    <View style={{flexDirection:'row',bottom:10}}>
                            <View style={{flex:.4}}>
                            </View>
                            
                            <View style={{flex:.6,flexDirection:'row',paddingLeft:hp('1.5%')}}> 
                                <View style={{flexDirection:'row',flex:.5}} > 
                                    <Text style={styles.orange_text}> 15 Kms </Text>
                                    <Text style={styles.orange_text}> | </Text>
                                    <Text style={styles.orange_text}> 18 mins </Text>
                                </View>
                                <View style={{flex:.5,flexDirection:'row',justifyContent:'flex-end'}}>
                                    <Button style={[styles.booK_nw_btn]}>
                                        <Text style={[styles.booK_nw_txt]} >Book Now</Text>
                                    </Button>
                                </View>
                            </View>
                    </View>   
                </View>

               
                <View style={styles.bottom_container}>
                    <Line/>
                    <View style={{flexDirection:'row',paddingTop:6,paddingBottom:6}}>
                        <View style={{flex:1,}}>
                            <Text style={styles.bottom_text}>Price Per Hour</Text>
                        </View>
                        <View style={{flex:1,}}>
                            <Text style={styles.bottom_text}>USD 20 $</Text>
                        </View>
                    </View>
                    <Line/>
                    {/*
                    <View style={{flexDirection:'row',paddingTop:6,paddingBottom:6}}>
                        <View style={{flex:1,}}>
                            <Text style={styles.bottom_text}>Offer Available</Text>
                        </View>
                        <View  style={{flex:1,}}>
                            <Text style={styles.bottom_text}>YES</Text>
                        </View>
                    </View>
                    <Line/>
                    <View style={{flexDirection:'row',paddingTop:6,paddingBottom:6}}>
                        <View  style={{flex:1,}}>
                            <Text style={styles.bottom_text}>Trainers Availability</Text>
                        </View>
                        <View style={{flex:1,}}>
                            <Text style={styles.bottom_text}>YES</Text>
                        </View>
                    </View>
                    <Line/>
                */}
                </View>    

            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white,
        paddingLeft:hp('1.5%'),
        paddingRight:hp('1.5%')
    },
    view1:{
        flexDirection:'row',
  //      flex:1
    },
    text_style1:{
        fontSize:hp('2%')
    },
    RatingBar:{
        height:hp('2%'),
        width:hp('2%'),
        paddingRight:hp('2%')
    },
    view11:{
        flex:1
    },
    view12:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
       
    },
    orange_text:{
        color:color.orange,
        fontSize:hp('1.4%')
    },
    booK_nw_btn:{
        backgroundColor:color.blue,
        height:hp('3%'),
        paddingLeft:8,
        paddingRight:8,
    },
    booK_nw_txt:{
        color:color.white,
        fontSize:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8
    },
    bottom_container:{
        flexDirection:'column',
        marginTop:hp('1.5%'),
    },
        _starstyle:{
      fontSize:hp('1.8%'),
        marginRight:1,
      },
    bottom_text:{
        fontSize:hp('1.7%')
    }
})