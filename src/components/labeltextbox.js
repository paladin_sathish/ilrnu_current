import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  TextInput,
  Text,
  View,
  Alert,
  StatusBar,
  Modal,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Picker
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  Container,
  Icon,
  Button,
  Subtitle,
  Header,
  Content,
  Body,
  Right,
  Title,
  Item,
  Input,
  Label,
  Textarea,
  Form
} from "native-base";
import color from "../Helpers/color";
import CountryList from "./countrypicker";
import ADD_img from "../images/add_icon.png";
var obj = { dd: "", mm: "", yy: "" };
var countrycode = { mobile: "", countrycode: "91" };
function checkdate(date) {
  try {
    var checkdate = new Date(date);
    return !isNaN(checkdate.getTime());
  } catch (err) {
    // alert(err)
  }
}
function isValidDate(year, month, day) {
  var d = new Date(year, month, day);
  if (d.getFullYear() == year && d.getMonth() == month && d.getDate() == day) {
    return true;
  }
  return false;
}
export default class LabelTextbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      obj: JSON.parse(JSON.stringify(obj)),
      mobile: JSON.parse(JSON.stringify(countrycode))
    };
    // alert(obj);
  }
  componentWillReceiveProps(props) {
    obj = JSON.parse(JSON.stringify(obj));
  }
  changeText = text => {
    this.props.changeText(text);
  };
  _focusNextField(nextField) {
    this.refs[nextField].focus();
  }
  mycode = data => {
    var mobile = this.state.mobile;
    mobile.countrycode = data.callingCode;
    this.setState({ mobile });
    this.props.changeText(mobile);
    // this.setState({countryCallCode:data.callingCode})
  };
  changeMob = data => {
    var mobile = this.state.mobile;
    mobile.mobile = data;

    this.setState({ mobile });
    this.props.changeText(mobile);
  };

  _renderPicker = (item, i) => {
    return (
      <Picker.Item
        key={i}
        label={item.SubSpecificName}
        value={item.SubSpecificId}
      />
    );
  };

  changedob = (text, key, obj) => {
    if (key == "dd") {
      text.length >= 2 ? this.mm.focus() : "";
    } else if (key == "mm") {
      text.length >= 2 ? this.yyyy.focus() : "";
      // if(parseInt(text)>12){
      // 	text=12;
      // }
    }
    // alert(text);
    obj[key] = text;
    // alert(obj.yy.length)
    var datecheck = isValidDate(obj.yy, obj.mm - 1, obj.dd);
    // alert(JSON.stringify(obj));
    // alert(datecheck);
    if (obj.mm > 12) {
      var obj = this.state.obj;
      obj.mm = "12";
      this.setState({ obj });
    }

    if (obj.yy.length == 4 && obj.yy >= 1901) {
      if (datecheck == true) {
        if (
          new Date(obj.mm + "/" + obj.dd + "/" + obj.yy).getTime() <=
          new Date().getTime()
        ) {
          this.props.changeText(obj);
        } else {
          this.props.changeText("future");
        }
      } else {
        // obj.yy=1901;
        // this.setState({obj});
        this.props.changeText("errordob");
      }
    } else {
      this.props.changeText("errordob");
    }
  };
  renderInput = data => {
    if (data.type == "DOB") {
      var { obj } = this.state;
      // var dateobj=JSON.parse(JSON.stringify(obj));
      return (
        <View
          style={{
            flex: 0.7,
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Item
            regular
            style={[
              styles.itemsize,
              { flex: 0.26, borderWidth: 0.5, borderColor: color.black1 }
            ]}
          >
            <TextInput
              value={obj.dd}
              maxLength={2}
              keyboardType="numeric"
              onChangeText={data => this.changedob(data, "dd", obj)}
              style={{
                flex: 1,
                borderColor: "yellow",
                padding: 0,
                paddingLeft: 5
              }}
              ref="dd"
              returnKeyType="next"
              blurOnSubmit={false}
              placeholder="DD"
            />
          </Item>
          <Item
            regular
            style={[
              styles.itemsize,
              { flex: 0.26, borderWidth: 0.5, borderColor: color.black1 }
            ]}
          >
            <TextInput
              value={obj.mm}
              maxLength={2}
              keyboardType="numeric"
              returnKeyType="next"
              style={{ flex: 1, padding: 0, paddingLeft: 5 }}
              onChangeText={data => this.changedob(data, "mm", obj)}
              ref={input => {
                this.mm = input;
              }}
              blurOnSubmit={false}
              placeholder="MM"
            />
          </Item>
          <Item
            regular
            style={[
              styles.itemsize,
              { flex: 0.4, borderWidth: 0.5, borderColor: color.black1 }
            ]}
          >
            <TextInput
              value={obj.yy}
              maxLength={4}
              keyboardType="numeric"
              style={{ flex: 1, padding: 0, paddingLeft: 5 }}
              onChangeText={data => this.changedob(data, "yy", obj)}
              returnKeyType="done"
              ref={input => {
                this.yyyy = input;
              }}
              placeholder="YYYY"
            />
          </Item>
          {this.props.error && (
            <Text
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </View>
      );
    } else if (data.type == "textarea") {
      return (
        <Form style={[{ flex: 0.7 }]}>
          <Textarea
            bordered={true}
            value={this.props.value}
            onChangeText={data => this.props.changeText(data)}
            rowSpan={this.props.rowspan ? this.props.rowspan : 4}
            placeholder={this.props.placeholder}
          />
          {this.props.error && (
            <Text
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </Form>
      );
    } 
    else if (data.type == "textareamap") {
      return (
        <Form style={[{ flex: 0.7 }]}>
          <Textarea
            bordered={true}
            value={this.props.value}
            onChangeText={data => this.props.changeText(data)}
            rowSpan={this.props.rowspan ? this.props.rowspan : 4}
            placeholder={this.props.placeholder}
          /> 

            <TouchableOpacity
            onPress={() => this.props.changeText(data, "address")}
            style={{ paddingLeft: 12, paddingRight: 12 }}
          >
            <Text
              style={{
                textAlign: "right",
                color: color.blueactive,
                fontSize: hp("1.5%")
              }}
            >
              Location Map
            </Text>
          </TouchableOpacity>


          {this.props.error && (
            <Text
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </Form>
      );
    }
    else if (data.type == "textarea1") {
      return (
        <Form style={[{ flex: 1 }]}>
          <Textarea
            bordered={true}
            value={this.props.value}
            onChangeText={data => this.props.changeText(data)}
            rowSpan={this.props.rowspan ? this.props.rowspan : 4}
            placeholder={this.props.placeholder}
          />
          {this.props.error && (
            <Text
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </Form>
      );
    } else if (data.type == "codeinput") {
      return (
        <Item
          regular
          style={[
            styles.itemsize,
            { borderWidth: 0.5, flex: 0.7, borderColor: color.black1 }
          ]}
        >
          <CountryList mycode={this.mycode} />
          <Input
            style={{ borderLeftWidth: 0.5, height: "100%", padding: 0 }}
            keyboardType={this.props.inputtype}
            value={this.props.value}
            onChangeText={data => this.changeMob(data)}
          />
          {this.props.error && (
            <Text
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </Item>
      );
    } else if (data.type == "btn_address") {
      return (
        <Item regular style={[styles.itemsize, { flex: 0.7 }]}>
          <Input
            keyboardType={
              data.spec_det_datatype1 == "number" ? "numeric" : null
            }
            style={{
              borderWidth: 0.5,
              borderColor: color.black1,
              height: "100%",
              padding: 0
            }}
            keyboardType={this.props.inputtype}
            value={this.props.value}
            onChangeText={data => this.props.changeText(data)}
          />
          <TouchableOpacity
            onPress={() => this.props.changeText(data, "address")}
            style={{ paddingLeft: 12, paddingRight: 12 }}
          >
            <Text
              style={{
                textAlign: "right",
                color: color.blueactive,
                fontSize: hp("2.1%")
              }}
            >
              Location Map
            </Text>
          </TouchableOpacity>
          {this.props.error && (
            <Text
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </Item>
      );
    } else if (data.type == "btn_add") {
      return (
        <Item
          regular
          style={[
            styles.itemsize,
            { flex: 0.7, flexDirection: "row", justifyContent: "space-between" }
          ]}
        >
          <Input
            disabled={data.styleobj && data.styleobj.readOnly ? true : false}
            keyboardType={
              data.spec_det_datatype1 == "number" ? "numeric" : null
            }
            style={{
              borderWidth: 0.5,
              borderColor: color.black1,
              height: "100%",
              marginRight: 18,
              padding: 0
            }}
            keyboardType={this.props.inputtype}
            value={this.props.value}
            onChangeText={data => this.props.changeText(data)}
          />
          <TouchableOpacity
            onPress={() => this.props.changeText(data, "addlocation")}
            underlayColor={color.orange}
            style={[styles.add_symbol_container]}
          >
            <Image style={styles.add_symbol} source={ADD_img}></Image>
          </TouchableOpacity>

          {this.props.error && (
            <Text
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </Item>
      );
    } else if (data.type == "text1") {
      return (
        <Item regular style={[styles.itemsize, { flex: 1 }]}>
          <Input
            disabled={data.styleobj && data.styleobj.readOnly ? true : false}
            style={{
              borderWidth: data.styleobj && data.styleobj.noborder ? 0 : 0.5,
              height: "100%",
              borderColor: data.readOnly ? "#fff" : color.black1,
              padding: 0
            }}
            keyboardType={this.props.inputtype}
            value={this.props.value}
            onChangeText={data => this.props.changeText(data)}
          />

          {this.props.children}
          {this.props.error && (
            <Text
              editable={false}
              style={{
                position: "absolute",
                bottom: -hp("2.5%"),
                fontSize: hp("1.5%"),
                color: color.red
              }}
            >
              {this.props.errormsg}
            </Text>
          )}
        </Item>
      );
    } else if (data.type == "picker") {
             return (
               <View style={[styles.pickerBorder, { flex: 0.7 }]}>
                 <Picker
                   selectedValue={this.props.value}
                   style={{
                     height: 45
                   }}
                   onValueChange={(language, itemIndex) =>
                     this.props.changeText(language, "sport_name")
                   }
                 >
                   {this.props.styleobj &&
                     this.props.styleobj.picker_data &&
                     this.props.styleobj.picker_data.map((item, i) =>
                       this._renderPicker(item, i)
                     )}
                 </Picker>
               </View>
             );
           } else {
             return (
               <Item regular style={[styles.itemsize, { flex: 0.7 }]}>
                 <Input
                   disabled={
                     data.styleobj && data.styleobj.readOnly ? true : false
                   }
                   style={{
                     borderWidth:
                       data.styleobj && data.styleobj.noborder ? 0 : 0.5,
                     height: "100%",
                     borderColor: data.readOnly ? "#fff" : color.black1,
                     padding: 0
                   }}
                   keyboardType={this.props.inputtype}
                   value={this.props.value}
                   onChangeText={data => this.props.changeText(data)}
                 />

                 {this.props.children}
                 {this.props.error && (
                   <Text
                     style={{
                       position: "absolute",
                       bottom: -hp("2.5%"),
                       fontSize: hp("1.5%"),
                       color: color.red
                     }}
                   >
                     {this.props.errormsg}
                   </Text>
                 )}
               </Item>
             );
           }
  };
  render() { 
	 
    if (
      this.props.type != "textarea1" &&
      this.props.type != "text1" &&
      this.props.flex != "column"
    ) {
      return (
        <View style={styles.flexrow}>
          <View style={styles.col3}>
            <Label
              style={{
                textTransform: this.props.capitalize ? "capitalize" : "none"
              }}
            >
              {this.props.labelname}
            </Label>
          </View>
          {this.renderInput(this.props)}
        </View>
      );
    } else if (this.props.flex == "column") {
      return (
        <View>
          <View>
            <Label
              style={{
                textTransform: this.props.capitalize ? "capitalize" : "none"
              }}
            >
              {this.props.labelname}
            </Label>
          </View>
          {this.renderInput(this.props)}
        </View>
      );
    } else {
      return (
        <View>
          <View>
            <Label
              style={{
                textTransform: this.props.capitalize ? "capitalize" : "none"
              }}
            >
              {this.props.labelname}
            </Label>
          </View>
          {this.renderInput(this.props)}
        </View>
      );
    }
  }
}

const styles = {
  flexrow: {
    flexDirection: "row",
    marginBottom: 25
  },
  col3: {
    flex: 0.3,
    // backgroundColor:'grey',
    // alignItems:'center',
    justifyContent: "center"
  },
  col7: {
    flex: 0.7
    // backgroundColor:'blue'
  },
  itemsize: {
    height: hp("5.5%"),
    padding: 0,
    borderColor: "#fff"
  },
  add_symbol_container: {
    backgroundColor: color.blue,
    height: hp("5%"),
    width: hp("5%"),
    // marginLeft:-12,
    justifyContent: "center"
  },
  add_symbol: {
    height: hp("1.5%"),
    width: hp("1.5%"),
    alignSelf: "center"
  },
  pickerBorder: {
    borderColor: "#d6d6d6",
    borderWidth: 1,
    borderStyle: "solid",
    marginVertical: hp("0.5%")
  }
};
