import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title,List,ListItem,Content } from 'native-base';
import {View,Image,StatusBar,Picker,Text,TouchableOpacity} from 'react-native';
import colors from '../Helpers/colors';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class DropDown extends Component {
listcallback=(obj)=>{
	// alert("");
	this.props.listcallback(obj);
}
render(){
	return(

		<View style={{zIndex:9999999,position:'absolute',width:'100%',backgroundColor:'white'}}>
          <List>
          {this.props.listofcources.map((obj,key)=>{
          	return(
          		 <ListItem  onPress={()=>this.listcallback(obj)} noIndent={true} key={key}>
              <Text style={styles.itemstyle}>{obj.name}</Text>
            </ListItem>
          		)
          })}
            
          </List>
		</View>

		)
}

}
const styles={
	itemstyle:{
	marginLeft:10,
	color:colors.blueactive,
	fontSize:hp('1.9%')
}
}