// import React, { Component } from 'react';
// import { View, TouchableHighlight, Text, StyleSheet, Image, AppRegistry, Platform } from 'react-native';
// import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox } from 'native-base';
// import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

// import color from '../Helpers/color';

// export default class Checkbox1 extends React.Component
// {
//    constructor(props){
//       super(props);

//       this.state = { checked: props.active }
//    }
//    componentWillMount()
//    {
//       if(this.props.active)
//       {
//         this.setState({ checked: true }, () =>
//         {
//           this.props.selectedArrayObject.setItem({ 'key': this.props.keyvalue, 'label': this.props.label });
//         });
//       }
//       else
//       {
//         this.setState({ checked: false });
//       }
//    }

//    toggleState(key, label,keyvalue)
//    {
//       this.setState({ checked: !this.state.checked }, () =>
//       {
//          if(this.state.checked)
//          {

//            this.props.selectedArrayObject.setItem({key:key,'label': label,key:keyvalue });
//          this.props.selectedData({'label': label,key:keyvalue });
//          }
//          else
//          {

//             var filteredobj=this.props.selectedArrayObject.getArray().filter((obj)=>obj.key==keyvalue);
//             if(filteredobj.length>0){
//             filteredobj[0].unselect=true;
//          this.props.selectedData(filteredobj[0]);
//        }
//             this.props.selectedArrayObject.getArray().splice( this.props.selectedArrayObject.getArray().findIndex(x => x.key == keyvalue), 1 );
//          }
//          // alert(JSON.stringify(this.props.selectedArrayObject.getArray()));
//       });

//    }

//    render()
//    {
//       return(
//         <TouchableHighlight style={{width:'100%',}} onPress = { this.toggleState.bind(this, this.props.keyValue, this.props.label,this.props.keyvalue) } underlayColor = "transparent" style = { styles.checkBoxButton }>
//            <View style = { styles.checkBoxHolder }>
//               <View style = {{ width: this.props.size, height: this.props.size,borderRadius:this.props.size/2,borderWidth:!this.state.checked?0.3:0, padding: 0.3,alignItems:'center',justifyContent:'center',borderColor:'black' }}>
//               {
//                  (this.state.checked)
//                  ?
//                     (<View style = { [styles.checkedView,{backgroundColor:this.props.color,borderRadius:this.props.size/2}] }>
//                       <Icon style={{color:'white',fontSize:this.props.size/1.7}} type='Ionicons' name='checkmark'/>
//                     </View>)
//                  :
//                     (<View style = { [styles.uncheckedView,{borderRadius:this.props.size/2}] }/>)
//               }
//               </View>
//               <View style={{width:hp('14%')}}>
//               <Text numberOfLines={2} style = {[ styles.checkBoxLabel, {width:'100%', color: 'black',fontWeight:this.state.checked?'bold':null }]}>{ this.props.label }</Text>
//               </View>
//            </View>
//         </TouchableHighlight>
//       );
//    }
// }



// const styles = StyleSheet.create(
// {
//   container:
//   {
//     paddingTop: (Platform.OS === 'ios') ? 20 : 0,
//     paddingHorizontal: 25,
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center'
//   },

//   selectedArrayItemsBtn:
//   {
//     marginTop: 20,
//     padding: 10,
//     backgroundColor: 'rgba(0,0,0,0.6)',
//     alignSelf: 'stretch'
//   },

//   btnText:
//   {
//     color: 'white',
//     textAlign: 'center',
//     alignSelf: 'stretch',
//     fontSize: 18
//   },

//   checkBoxButton:
//   {
//   },

//   checkBoxHolder:
//   {
//      flexGrow: 1,
//     flexDirection: 'row',
//     alignItems: 'center',
//   },

//   checkedView:
//   {
//     flex: 1,
//     position:'absolute',
//     justifyContent: 'center',
//     alignItems: 'center',
//     width:'100%',
//     height:'100%'
//   },

//   checkedImage:
//   {
//     height: '80%',
//     width: '80%',
//     tintColor: 'white',
//     resizeMode: 'contain'
//   },

//   uncheckedView:
//   {
//      position:'absolute',
//     justifyContent: 'center',
//     alignItems: 'center',
//     width:'100%',
//     height:'100%',
//     top:0,
//     backgroundColor:color.ash1
//   },

//   checkBoxLabel:
//   {
//     flexWrap:'wrap',
//     fontSize: hp('2%'),
//     paddingLeft: 10
//   }
// });




