'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  Text
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import placeholder_img from '../images/placeholder_img.png';
import StarRating  from 'react-native-star-rating';
import color from '../Helpers/color'

class VenueCardComp extends Component {
  render() {
  	var item=this.props.item;
    return (
      <View>
      {this.props.star&&
      <StarRating
                                    // disabled={false}
                                    maxStars={5}
                                    rating={4}
                                    selectedStar={(rating) => null}
                                    halfStarColor={color.blue}
                                    fullStarColor={color.blue}
                                    starSize={8}
                                    containerStyle={styles.RatingBar}
                                    starStyle={styles._starstyle}
                                    emptyStarColor={color.ash6}
                                />}
                     {item.photos&&item.photos.length > 0 &&
                           item.photos[0].venue_image_path && (
                           	<View style={[styles.image_style,{backgroundColor:color.ash}]}>
                             <Image
                               style={styles.image_style}
                               alt="No Image"
                               source={{
                                 uri:
                                   item.photos.length > 0 &&
                                   item.photos[0].venue_image_path
                               }}
                             ></Image>
                             </View>
                           )}
                         {!(
                           item.photos.length > 0 &&
                           item.photos[0].venue_image_path
                         ) && (
                           <Image
                             style={styles.image_style}
                             alt="No Image"
                             source={placeholder_img}
                           ></Image>
                         )}
                         {item.visible == true && (
                           <View
                             style={{
                               position: "absolute",
                               bottom: 0,
                               top: hp("4%"),
                               alignItems: "center",
                               height: "100%",
                               left: hp("1%"),
                               zIndex: 1
                             }}
                           >
                           </View>
                         )}
                          <Text numberOfLines={1} style={styles.text_style2}>
                           {item.trn_venue_name}
                         </Text>
                         <Text style={styles.text_style2}>
                           {item.Distance} | {item.Time}
                         </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },text_style2:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
        paddingLeft:hp('0.45%'),
        paddingTop:hp('0.45%')
    },
    RatingBar:{
       height:hp('2.5%'),
       width:wp('13%')
    },
    _starstyle:{
      fontSize:12,
        marginRight:3
    },
});


export default VenueCardComp;