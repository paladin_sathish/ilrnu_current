import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Alert,StatusBar,Modal,Image,TouchableHighlight,TouchableOpacity,ScrollView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input} from 'native-base';
import color from '../Helpers/color';
// import ModalComp from '../components/Modal';
import LabelTextbox from '../components/labeltextbox';
import ValidationLibrary from '../Helpers/validationfunction';
import { Grid, Col } from 'react-native-easy-grid';
import links from '../Helpers/config';
var formkeys=[{name:'name',type:'default',labeltype:'text'},{name:'surname',type:'default',labeltype:'text'},{name:'location',type:'default',labeltype:'text'},{name:'mobile',type:'numeric',labeltype:'text'},{name:'email',type:'default',labeltype:'text'},{name:'dob',type:'default',labeltype:'DOB'}]
export default class OtpScreen extends React.Component{
	constructor(props){
		super(props);
		this.state={
			
			Disabled:true,
            otp0:"",otp1:"",otp2:"",otp3:"",
            otpInput:[],disabled:false
			
		}

	}
	 otpTextInput = [];
componentWillReceiveProps(props){

}
	 componentDidMount() {
        this.otpTextInput[0]._root.focus();
    }

	verify=()=>{

    // Actions.signup();
    // var obj={'type':'disclaimer'};
    var obj=this.props.signupdata;
   if(this.props.forgot==false){
    obj.type='password';
  }
   
      // alert(JSON.stringify(obj));
    var otpdata=this.state.otp0.toString()+this.state.otp1.toString()+this.state.otp2.toString()+this.state.otp3.toString();
    this.setState({disabled:true})
fetch(links.APIURL+'checkOTP', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'mobileNumber':this.props.forgot==false?this.props.signupdata.mobile.mobile:this.props.forgotdata.mobildata.mobileNumber,
        otp:otpdata

      }),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      if(responseJson.status==0){
this.setState({disabled:false})
        console.log("responseJson",responseJson);
        if(this.props.forgot==false){
         this.props.loadotp(obj);
        }else{
          this.props.loadotp({type:'password'})
        }
    // alert("OTP send Successfully");
  }
  else{
    this.setState({disabled:false})
    alert(responseJson.msg);
  }
})


  }
	verification =() =>{
    var otpdata=this.state.otp0.toString()+this.state.otp1.toString()+this.state.otp2.toString()+this.state.otp3.toString();
//     
this.props.sendotp(otpdata);    
    console.log("otpdata",otpdata);
}
	
	
	 renderInputs() {
        const inputs = Array(4).fill(0);
        const txt = inputs.map(
            (i, j) => <Item  style={[styles.inputRadius,styles.borderLine]}>
                <Input
                style={{borderWidth:1,textAlign:'center',margin:3}}
                   
                    secureTextEntry={true}
                    underlineColorAndroid='transparent'
                    keyboardType="numeric"
                    maxLength={1}
                    onChangeText={(v) => this.focusNext(j, v)}
                    onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                    ref={ref => this.otpTextInput[j] = ref}
                />
            </Item>

        );
        return txt;
    }

    focusPrevious(key, index) {
        //console.log(index);
        if (key === 'Backspace' && index !== 0)
            this.otpTextInput[index - 1]._root.focus();
    }

    focusNext(index, value) {
        // console.log(index);
        
        console.log(value);
        if (index < this.otpTextInput.length - 1 && value)
            this.otpTextInput[index + 1]._root.focus();
        this.setState({["otp"+index]:value});


    }

render(){
	return(
		 <View style={styles.container}>
            <View style={styles.loginContainer}>
		<View style={{alignItems: 'center',
        justifyContent: 'center',flex:.5}}>

{this.props.forgot==false&&
        <Text style={{textAlign:'center',fontSize:hp('3.5%'), color:color.ash6}}> Welcome</Text>
      }
        <View style={{width:'80%'}}>
        {this.props.forgot==false&&
        <Text style={{textAlign:'center',fontSize:hp('3.7%'), color:color.orange,padding:10}}>{this.props.signupdata.name}</Text>
      } 
        
         <Text style={{textAlign:'center',fontSize:hp('3%'), color:color.black}}>Please Enter Your OTP {this.props.forgot==true&&"For Reset Your Password"} </Text>
         </View>
                 </View>
                  <View style={{flex:.5}}>
                 <Content>
                 <View style={styles.gridPad}>
                  {this.renderInputs()}
                 </View>
                  <View style={{flexDirection:'row',justifyContent:'center',marginTop:hp('3%')}}>
       <Button disabled={this.state.disabled} onPress={()=>this.verify()}  style={[styles.actionbtn,{backgroundColor:this.state.disabled?color.ash:color.orange}]}>
            <Text disabled={this.state.disabled} style={[styles.actionbtntxt,{color:this.state.disabled?color.black:color.white}]}>VERIFY</Text>
          </Button>
          </View>
                 </Content>
                 </View>

		</View>
		</View>
		)
}

}
const styles={
gridPad: { flexDirection:'row',flex:1},
	container: {
    flex: 1,
  backgroundColor: '#FFF',
    alignItems:'center',
    height:hp('60%')

},
loginContainer:{
    flex:1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  actionbtn:{
		borderRadius:5,
		width:hp('17%'),
		justifyContent:'center',
		// backgroundColor:color.orange
	},
	actionbtntxt:{
		textAlign:'center',
		// color:color.white,
		fontSize:hp('2.3%')
	},
	 txtMargin: { margin: 2,},
    inputRadius: { textAlign: 'center',color:'#403c3b',backgroundColor:'transparent'},
    borderLine:{
    	width:50,
    	borderColor:'transparent'
 },
}