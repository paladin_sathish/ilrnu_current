'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';


 import TabLoginProcess from '../components/TabLoginProcess';
import { Actions} from 'react-native-router-flux'
import { Container, Header, Title,Subtitle, Content, Button, Icon, Left,Right,  Body,List,ListItem } from 'native-base';
import color from '../Helpers/color';
import Toast from 'react-native-simple-toast';
import { GoogleSignin, GoogleSigninButton,statusCodes } from 'react-native-google-signin';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';

var itemsidebar = [
  { name: "Home", active: true, key: 0 },
  { name: "Action Items", active: false, key: 1, hide: true },
  { name: "Dashboard", active: false, key: 2 },
  { name: "Enquiries", active: false, key: 3, hide: true },
  { name: "Loyalty Program", active: false, key: 4, hide: true },
  { key: 7, name: "Refer & Earn", active: false },
  { key: 11, name: "My Purchased Course", active: false },
  { key: 10, name: "My Venues", active: false },
  { key: 12, name: "My Courses", active: false },
  { key: 12, name: "My Messages", active: false },
  { key: 11, name: "My Calendar", active: false },
  { name: "Logout", active: false, key: 5 },
  { key: 6, name: "Login", active: false }
];
class SideBar extends Component {
	constructor(props) {
	  super(props);
	  this.state = {googlesignedin:false,loginSuccess:true,sidebar:JSON.parse(JSON.stringify(itemsidebar)),loginDetails:null};
	}
  signOut = async () => {
  try {
    if(this.state.googlesignedin==true){
    await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();
  }
  } catch (error) {
    console.error(error);
  }
}; 




  changeItem=async(obj)=>{
    var sidebar=this.state.sidebar;
    sidebar.map((obj)=>obj.active=false);
    var index=this.state.sidebar.findIndex((data)=>data.name==obj.name);
    // alert(index);
    sidebar[index].active=true;
    this.setState({loginSuccess:true})
    if(obj.name=='Logout'){
      if(this.state.loginDetails.is_social_login==1){
        this.signOut();
      }
       AsyncStorage.removeItem('loginDetails');
       this.setState({loginDetails:null})
       // Actions.drawerClose();
       // Actions.home();
      //  if(Actions.currentScene=='home'){
      //    Actions.refresh({ key: 'home'});
      // }else{
        
      Actions.reset('home1');
      // }
    }else if(obj.name=='Home'){
      if(Actions.currentScene=='home'){
      Actions.refresh({ key: 'home'});
      }else{

      Actions.reset('home1');
      }
    }else if(obj.name=="Dashboard"){
      Actions.push('dashboard');
    }
    else if(obj.name=="Refer & Earn"){
      Actions.push('referearn');
    }
     else if(obj.name=="My Purchased Course"){
      Actions.push("PurchasedCourse");
    }
    else if (obj.name == "My Venues") {
            Actions.push("myvenues");
          } else if (obj.name == "My Courses") {
                   Actions.push("MyCourseList");
                 } else if (obj.name == "My Calendar") {
                   Actions.push("MyCalendar");
                 } else if (obj.name == "My Messages") {
                   Actions.push("MyMessage");
                 } else if (obj.name == "Login") {
                   const isSignedIn = await GoogleSignin.isSignedIn();
                   // alert(isSignedIn);
                   this.setState({ googlesignedin: isSignedIn });
                   this.setState({ loginSuccess: false });
                 }
      Actions.drawerClose();
    this.setState({sidebar});
  }
  loadLoginDetails=()=>{
    // alert("");
    AsyncStorage.getItem('loginDetails', async(err, result) => {
      
       if(result!=null){
         const isSignedIn = await GoogleSignin.isSignedIn();
    // alert(isSignedIn);
    this.setState({googlesignedin:isSignedIn});
    this.setState({loginDetails:JSON.parse(result)})
      }else{
    this.setState({loginDetails:null})

      }
     });
  }
  componentWillReceiveProps(){
this.loadLoginDetails()
  }
  async componentWillMount (){
   
    
    GoogleSignin.configure({// what API you want to access on behalf of the user, default is email and profile
  webClientId: '991993261926-40eh1aficetf270dknlf6ut67edj5b74.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
  accountName: '', // [Android] specifies an account name on the device that should be used
  iosClientId: '', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});
     this.loadLoginDetails();
  }
  sendLoginDetails=(data)=>{
console.log('loggeddata',data);
Toast.show('User Logged in Successfully', Toast.LONG)
Actions.refresh({key: Math.random()})
this.setState({loginSuccess:true});
  }
  render() {
    return (
       <Container>
        <Header style={{backgroundColor:color.white,height:hp('10%')}}>
        <View style={{alignItems:'center',flexDirection:'row',marginRight:0}}>
        {/*}
        <Image style={styles.headerImage} source={require('../images/venue.jpg')}/>*/}
        </View>
        <Body style={{paddingLeft:wp('3.2%')}}>
        <Text style={{fontSize:hp('3%'),color:color.black1,width:'95%'}}>Welcome<Text style={{color:color.orange}}> {this.state.loginDetails && this.state.loginDetails.user_name?this.state.loginDetails.user_name:'Guest User' }</Text></Text>
         {this.state.loginDetails&&
        <Text style={{color:color.black1,fontSize:hp('1.7%')}}>{this.state.loginDetails.user_cat_id==1?'Venue Provider':'Corporte User'}</Text>
      }
        </Body>
        <View style={{flexDirection:'row',alignItems:'center'}}>
        <TouchableOpacity onPress={()=>Actions.drawerClose()}>
        <Icon style={{color:color.orange}} name="menu"/>
        </TouchableOpacity>
        </View>
        </Header>
         <Content>
          <List>
          {this.state.sidebar.map((obj,key)=>{
          	return (
              <View>
                {this.state.loginDetails && obj.key == 5 && !obj.hide && (
                  <ListItem
                    key={key}
                    style={{ borderWidth: 0, marginLeft: 0, paddingLeft: 20 }}
                    onPress={() => this.changeItem(obj)}
                  >
                    <Left>
                      <Text
                        style={{ color: obj.active == true ? "orange" : null }}
                      >
                        {obj.name}
                      </Text>
                    </Left>
                    <Right></Right>
                  </ListItem>
                )}
                {!this.state.loginDetails && obj.key == 6 && !obj.hide && (
                  <ListItem
                    key={key}
                    style={{ borderWidth: 0, marginLeft: 0, paddingLeft: 20 }}
                    onPress={() => this.changeItem(obj)}
                  >
                    <Left>
                      <Text
                        style={{ color: obj.active == true ? "orange" : null }}
                      >
                        {obj.name}
                      </Text>
                    </Left>
                    <Right></Right>
                  </ListItem>
                )}
                {this.state.loginDetails &&
                  (obj.key == 11 || obj.key == 10 || obj.key == 12) &&
                  !obj.hide && (
                    <ListItem
                      key={key}
                      style={{ borderWidth: 0, marginLeft: 0, paddingLeft: 20 }}
                      onPress={() => this.changeItem(obj)}
                    >
                      <Left>
                        <Text
                          style={{
                            color: obj.active == true ? "orange" : null
                          }}
                        >
                          {obj.name}
                        </Text>
                      </Left>
                      <Right></Right>
                    </ListItem>
                  )}
                {obj.key != 5 &&
                  obj.key != 6 &&
                  obj.key != 11 &&
                  obj.key != 10 &&
                  !obj.hide &&
                  obj.key != 12 && (
                    <ListItem
                      key={key}
                      style={{ borderWidth: 0, marginLeft: 0, paddingLeft: 20 }}
                      onPress={() => this.changeItem(obj)}
                    >
                      <Left>
                        <Text
                          style={{
                            color: obj.active == true ? "orange" : null
                          }}
                        >
                          {obj.name}
                        </Text>
                      </Left>
                      <Right></Right>
                    </ListItem>
                  )}
              </View>
            );
          })}
           
            </List>
      {this.state.loginSuccess==false&&
        <TabLoginProcess closetabmodal={()=>this.setState({loginSuccess:true})} loginttype="List your Venue" type='login' visible={true} sendlogindet={(data)=>this.sendLoginDetails(data)}/>
      }
         </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
headerImage:{
	width:hp('6%'),
	height:hp('6%'),
	borderRadius:hp('6%')/2
}
});


export default SideBar;