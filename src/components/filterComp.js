import React,{Component} from 'react';
import {View,Text,StyleSheet } from 'react-native';
import {  CheckBox } from "native-base";
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Toast from 'react-native-simple-toast';
import Dropdown_conatiner from '../components/drpdown_container'
import Geolocation from 'react-native-geolocation-service';
import links from '../Helpers/config';


const filterCheckbox=[
    "Booked",
    "Top 20",
   "Highly Rated",
"Activity Based"];
export default class FilterComp extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     loading: false,
                     checkboxes: [
                       {
                         id: 1,
                         name: "Booked",
                         _checked: false
                       },
                       {
                         id: 2,
                         name: "Top 20",
                         _checked: false
                       },
                       {
                         id: 3,
                         name: "Highly Rated",
                         _checked: false
                       },
                       {
                         id: 4,
                         name: "Activity Based",
                         _checked: false
                       }
                     ],
                     doDropdown: {
                       id: "venue_act_id",
                       name: "venue_act_name",
                       dropdown: []
                     },
                     whatDropdown: {
                       id: "venue_purpose_id",
                       name: "venue_purpose_name",
                       dropdown: []
                     },
                     whereDropdown: {
                       id: "venue_cat_id",
                       name: "venue_cat_name",
                       dropdown: null
                     },
                     dodata: null,
                     whatdata: null,
                     wheredata: null,
                     TOS: 0,
                     chosenlocation: { latitude: 0, longitude: 0 }
                   };
                 }

                 _dropdown_5_show() {
                   this._dropdown_5 && this._dropdown_5.show();

                   //   return false;
                 }
                 _dropdown_5_willShow() {
                   this._dropdown_5 && this._dropdown_5.show();
                   //     return false;
                 }
                 sendDropdownData = (data, key) => {
                   this.setState({ [key]: data });
                   if (key == "dodata") {
                     this.setState({ whatdata: null, TOS: 1 });
                     this.clearwhat();
                     this.clearwhere();
                     this.loadwhat(data.venue_act_id);
                   } else if (key == "whatdata") {
                     this.clearwhere();
                     this.setState({ wheredata: null, TOS: 2 });
                     //this.loadwhere(data.venue_purpose_id,this.state.dodata.venue_act_id);
                   } else if (key == "wheredata") {
                     this.setState({ TOS: 3 });
                   }
                 };
                 clearwhat = () => {
                   var whatDropdown = this.state.whatDropdown;
                   whatDropdown.dropdown = null;
                   this.setState({ whatDropdown, whatdata: null });
                 };
                 clearwhere = () => {
                   var whereDropdown = this.state.whereDropdown;
                   whereDropdown.dropdown = null;
                   this.setState({ whereDropdown, wheredata: null });
                 };

                 _changedrop() {
                   this.setState((backgroundColor = color.orange));
                 }
                 loaddo() {
                   fetch(links.APIURL + "do", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({})
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       var doDropdown = this.state.doDropdown;
                       doDropdown.dropdown = responseJson.data;
                       this.setState({ doDropdown });
                       console.log(this.state.doDropdown);
                       // alert(JSON.stringify(responseJson.data));
                       // alert(JSON.stringify(this.state.doDropdown));
                     });
                 }

                 loadwhat(data) {
                   fetch(links.APIURL + "whatpurpose", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({ venue_act_id: data })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       var whatDropdown = this.state.whatDropdown;
                       whatDropdown.dropdown = responseJson.data;
                       this.setState({ whatDropdown });
                       // alert(JSON.stringify(responseJson.data));
                       // console.log("what",this.state.whatDropdown);
                     });
                 }

                 loadwhere(data, data1) {
                   // console.log("where_id",data);
                   fetch(links.APIURL + "wherecategory", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       venue_act_id: data1,
                       venue_purpose_id: data
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       var whereDropdown = this.state.whereDropdown;
                       whereDropdown.dropdown = responseJson.data;
                       this.setState({ whereDropdown });
                       // alert(JSON.stringify(responseJson.data));
                       // console.log("where",responseJson);
                     });
                 }
                 arrowClick = () => {
                   this.setState({ loading: true });
                   fetch(links.APIURL + "dropdownSearchpurpose", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       venue_act_id: this.state.dodata
                         ? this.state.dodata.venue_act_id
                         : null,
                       venue_purpose_id: this.state.whatdata
                         ? this.state.whatdata.venue_purpose_id
                         : null,
                       venue_cat_id: this.state.wheredata
                         ? this.state.wheredata.venue_cat_id
                         : null,
                       TOS: this.state.TOS,
                       lat: this.state.chosenlocation.latitude,
                       long: this.state.chosenlocation.longitude
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       this.setState({ loading: false });
                       if (responseJson.status && responseJson.status == 1) {
                         Toast.show("Options are empty to search", Toast.LONG);
                       } else {
                         if (responseJson.length == 0) {
                           this.props.nxtpge([], "", this.state);
                           Toast.show("No Records Found", Toast.LONG);
                         } else {
                           this.props.nxtpge(responseJson, "", this.state);
                         }
                       }
                       // Toast.show('This is a long toast.', Toast.LONG);
                       // Actions.Search_Home(JSON.stringify(responseJson));
                       // alert(JSON.stringify(responseJson));

                       // this.setState({searchdata:responseJson});
                       // console.log(responseJson);
                       // this.props.sendsearchdata(responseJson.data);
                     })
                     .catch(resp => {
                       this.setState({ loading: false });
                       Toast.show("Network Failed", Toast.LONG);
                     });
                   //this.props.receivesearch(data);
                   //this.setState({visible:true})
                 };
                 componentWillMount() {
                   // alert(this.props.topcompobj)
                   if (this.props.topcompobj) {
                     // var state=this.state;
                     // state=this.props.topcompobj;
                     // this.setState({state});
                     this.state = this.props.topcompobj;
                   } else {
                     this.loaddo();
                   }
                   Geolocation.getCurrentPosition(
                     position => {
                       // alert(JSON.stringify(position));
                       this.setState({
                         chosenlocation: {
                           latitude: position.coords.latitude,
                           longitude: position.coords.longitude
                         }
                       });
                       //this.getAddressByLatLng(position.coords);
                       // this.getAddressByLatLng(position.coords);
                     },

                     error => {
                       // See error code charts below.
                       console.log(error.code, error.message);
                     },
                     { timeout: 10000, maximumAge: 0 }
                   );
                   // this.loadwhat();
                   // this.loadwhere();
                 }
                 componentWillReceiveProps(props) {}

                 toggleCheckbox = id => {
             

                   let changedCheckbox = this.state.checkboxes.find(
                     cb => cb.id === id
                   );
                   changedCheckbox._checked = !changedCheckbox._checked;
                   let chkboxes = this.state.checkboxes;
                   for (let i = 0; i < chkboxes.length; i++) {
                     if (chkboxes[i].id === id) {
                       chkboxes.splice(i, 1, changedCheckbox);
                     }
                   }
                   this.setState({ checkboxes: chkboxes });
                 };

               
                 render() {
                   return (
                     <React.Fragment>
                       <View
                         style={[
                           styles.top_container,
                           this.props.topcompobj
                             ? { backgroundColor: color.blue }
                             : { backgroundColor: color.blue }
                         ]}
                       >
                         <View
                           style={{
                             flex: 1,
                             justifyContent: "flex-start",
                             flexWrap: "wrap",
                             flexDirection: "row",
                             alignItems: "center"
                           }}
                         >
                           <View
                             style={{
                               paddingVertical: 5,
                               paddingHorizontal: 5
                             }}
                           >
                             <Dropdown_conatiner
                               drop_items={"Do"}
                               values={this.state.doDropdown.dropdown}
                               keyvalue={"venue_act_name"}
                               action={true}
                               value={
                                 this.state.dodata
                                   ? this.state.dodata.venue_act_name
                                   : "Do"
                               }
                               sendDropdownData={data =>
                                 this.sendDropdownData(data, "dodata")
                               }
                               _width={50}
                             />
                           </View>
                           <View
                             style={{
                               paddingVertical: 5,
                               paddingHorizontal: 5
                             }}
                           >
                           <Dropdown_conatiner
                             drop_items={"How"}
                             action={true}
                             values={this.state.whatDropdown.dropdown}
                             keyvalue={"venue_purpose_name"}
                             value={
                               this.state.whatdata
                                 ? this.state.whatdata.venue_purpose_name
                                 : "How"
                             }
                             sendDropdownData={data =>
                               this.sendDropdownData(data, "whatdata")
                             }
                             _width={50}
                           />
                         </View>

                         <View
                           style={{ paddingVertical: 5, paddingHorizontal: 5 }}
                         >
                           <Dropdown_conatiner
                             drop_items={"How"}
                             action={true}
                             values={this.state.whatDropdown.dropdown}
                             keyvalue={"venue_purpose_name"}
                             value={
                               this.state.whatdata
                                 ? this.state.whatdata.venue_purpose_name
                                 : "How"
                             }
                             sendDropdownData={data =>
                               this.sendDropdownData(data, "whatdata")
                             }
                             _width={50}
                           />
                         </View>

                         <View
                           style={{ paddingVertical: 5, paddingHorizontal: 5 }}
                         >
                           <Dropdown_conatiner
                             drop_items={"How"}
                             action={true}
                             values={this.state.whatDropdown.dropdown}
                             keyvalue={"venue_purpose_name"}
                             value={
                               this.state.whatdata
                                 ? this.state.whatdata.venue_purpose_name
                                 : "How"
                             }
                             sendDropdownData={data =>
                               this.sendDropdownData(data, "whatdata")
                             }
                             _width={50}
                           />
                         </View>
                         <View
                           style={{ paddingVertical: 5, paddingHorizontal: 5 }}
                         >
                           <Dropdown_conatiner
                             drop_items={"How"}
                             action={true}
                             values={this.state.whatDropdown.dropdown}
                             keyvalue={"venue_purpose_name"}
                             value={
                               this.state.whatdata
                                 ? this.state.whatdata.venue_purpose_name
                                 : "How"
                             }
                             sendDropdownData={data =>
                               this.sendDropdownData(data, "whatdata")
                             }
                             _width={50}
                           />
                         </View>

                         <View
                           style={{ paddingVertical: 5, paddingHorizontal: 5 }}
                         >
                           <Dropdown_conatiner
                             drop_items={"How"}
                             action={true}
                             values={this.state.whatDropdown.dropdown}
                             keyvalue={"venue_purpose_name"}
                             value={
                               this.state.whatdata
                                 ? this.state.whatdata.venue_purpose_name
                                 : "How"
                             }
                             sendDropdownData={data =>
                               this.sendDropdownData(data, "whatdata")
                             }
                             _width={50}
                           />
                           </View>
                         </View>
                       </View>

                       <View
                         style={{
                           flex: 1,
                           flexDirection: "row",
                           justifyContent: "space-around",
                           backgroundColor: color.blue
                         }}
                       >
                         {this.state.checkboxes.map((cb, i) => {
                           return (
                             <View
                               style={{
                                 paddingVertical: 5,
                                 flexDirection: "row"
                               }}
                             >
                               <CheckBox
                                 checked={cb._checked}
                                 color={color.orange}
                                 onPress={() => this.toggleCheckbox(cb.id)}
                               />
                               <Text
                                 style={{
                                   fontSize: 10,
                                   textAlign: "center",
                                   color: "white",
                                   marginLeft: 15
                                 }}
                               >
                                 {" "}
                                 {cb.name}
                               </Text>
                             </View>
                           );
                         })}
                       </View>
                     </React.Fragment>
                   );
                 }
               }
    const styles=StyleSheet.create({
        conatiner:{
            flex:1, 
            backgroundColor:color.white   
        },
        top_container:{
        //    height:hp('10%'),
            flexDirection:'row',
            justifyContent:"space-around",
            padding:hp('1%')
          
    
        },
        text_style:{
            color:color.white,
            fontSize:wp('3%')
        },
        drop_down_container:{
        //    flex:1,
            justifyContent:'center',
            marginBottom:hp('2.8%'),
            marginTop:hp('2.8%'),
            marginRight:3,
            minWidth:hp('11%')
        },
        drop_down:{
            backgroundColor:color.white,
            borderRadius:hp('2%'),
            width:hp('10%'),
            paddingLeft:10,
           
                  
        },
        homeBgImage:{
            width:wp('100%'),
            height:hp('25%')
        },
        play_button_container:{
            position:'absolute',
            flexDirection:'row',
            right:0,
            top:0,
            paddingTop:hp('1.5%'),
            paddingRight:hp('1.5%')
        },
        png_style:{
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            height:hp('3%'),
            width:hp('3%'),
        },
        png_back:{
            backgroundColor:color.white,
            height:hp('4%'),
            width:hp('4%'),
            borderRadius:hp('4%')/2,
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            elevation:1
        },
        flex_row:{
            flexDirection:'row'
        },
        flex_sep:{
            flex:1
        },
        down_aarow:{
            height:hp('1%'),
            width:hp('1%'),
            justifyContent:'flex-end',
            alignItems:'flex-end',
            alignSelf:'flex-end',
            alignContent:'flex-end',
            margin:3
        },
        fav_item_container:{
            paddingTop:hp('1%'),
            paddingBottom:hp('1%'),
            paddingLeft:('1%')
        },
        text_near:{
            fontSize:hp('1.9%'),
           
        },
        text_fav:{
            fontSize:hp('1.9%'),
            color:color.blue,
            fontWeight:'bold'    
        },
        text_center:{
            alignItems:'center',
        },
        line:{
            backgroundColor:color.ash,
            width:'100%',
            height:1
        },
        add_symbol_container:{
            backgroundColor:color.blue,
            justifyContent:'flex-end',
            justifyContent:'center',
            alignSelf:'flex-end',
            marginRight:wp('5%'),
            height:hp('2.5%'),
            width:hp('2.5%'),
           
        },
        add_symbol:{
            height:hp('1.3%'),
            width:hp('1.3%'),
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
        },
        right_arr_style:{
            height:hp('2%'),
            width:hp('2%'),
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            margin:3
        },
        right_arr_container:{
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            backgroundColor:color.white
        },
        mar_adjuse:{
            marginTop:3,
            marginBottom:3
        },
        dropdown_container: {
            width:'100%',
            height:hp('4%'),
            justifyContent:'center',
            alignSelf:'center',
           
      },
      dropdown_text:{
     //   fontSize: hp('2%'),
        justifyContent:'center',
      
      },
      dropdown_options:{
        marginTop:10,
        width:wp('30%'),
      },
      drop_down_top:{
        height:hp('1%'),
        width:hp('1%'),
        justifyContent:'center',alignSelf:'center'
      }
    })
    

