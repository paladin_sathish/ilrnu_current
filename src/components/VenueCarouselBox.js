import React, { Component } from 'react';
import { ImageBackground,View,TouchableOpacity,Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';
import placeholderImg from '../images/placeholder_img.png';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import LoadCarouselModal from '../pages/loadCarousel';
import CircularProgressBar1 from '../components/circularprogressbar1';
export default class VenueCarouselBox extends Component {
  loadCarousel=(data)=>{
    if(data.length>0){
    this.setState({carouselImages:data},function(){
      this.setState({carouselVisible:true})
    })
  }else{
    this.setState({carouselVisible:false});
  }
  }
  constructor(props) {
    super(props);
  
    this.state = {carouselVisible:false,carouselImages:[]};
  }
checkImageURL(url) {
     if(url.split('.').length>0){
  var imageurl=url.split('.')[url.split('.').length-1].toLowerCase();
  return ['jpg','jpeg','png','gif'].includes(imageurl);
}else{
  return false;
}
}
  render() {
    const {item}=this.props;
    console.log(item); 
    return (
        <Content>
        {this.state.carouselVisible==true&&
            <LoadCarouselModal items={this.state.carouselImages} closemodal={()=>this.setState({carouselVisible:false})} />
          }

          <Card style={{flex: 0}}>
            <CardItem>
              <Body>
              <TouchableOpacity activeOpacity={1} onPress={()=>this.loadCarousel(item.photos)}>
                             <View style={{height: hp('30%'),width:wp('100%')-35,backgroundColor:color.ash}}>

                <Image  source={item.photos.length>0?(this.checkImageURL(item.photos[0].venue_image_path)?{uri:item.photos[0].venue_image_path}:placeholderImg):placeholderImg} style={{height: hp('30%'),width:wp('100%')-35}}>
                  </Image>
                  </View>
                {item.photos.length>1&&
                <View style={{position:'absolute',bottom:12,right:12}} ><Text style={{color:color.white}}><Text style={{fontWeight:'bold',color:color.white}}>+ {item.photos.length-1}</Text> more photos</Text></View>
                  }
                </TouchableOpacity>
                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:12,marginBottom:12}}>
                <View style={{flex:0.7}}>
                 <Text numberOfLines={1} style={{fontWeight:'bold'}}>{item.trn_venue_name}</Text>
                  <Text style={{marginTop:5}}>{item.venue_cat_name}</Text>
                    <View style={{marginTop:5}}>
                  {item.trn_venue_status==1&&
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                     <Icon type="FontAwesome" name="circle" style={{fontSize:hp('2%'),color:color.green}}> </Icon>
                     <Text>Live</Text>
                     </View>
                  } 
                  {item.trn_venue_status==0&&
                     <View style={{flexDirection:'row',alignItems:'center'}}>
                     <Icon type="FontAwesome" name="circle" style={{fontSize:hp('2%'),color:color.orangeBtn}}> </Icon>
                     <Text>Pending</Text>
                     </View>
                  }{item.trn_venue_status==2&&
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                     <Icon type="FontAwesome" name="circle" style={{fontSize:hp('2%'),color:color.red}}> </Icon>
                     <Text>Rejected</Text>
                     </View>
                  } 
                    
                  </View>
                  
                  </View> 
                 
  <View style={{justifyContent:'flex-end',flexDirection:'column',flex:0.3}}>
  {item.autoSave&&
  <CircularProgressBar1
            percent={item.percentageOfCompletion}
            radius={hp('5%')}
            shadowColor={color.white}
            borderWidth={3}
            color={color.blueactive}
            bgColor={color.white}
        >
            <Text style={{ fontSize: hp('2.1%') }}>{item.percentageOfCompletion+"%"}</Text>
        </CircularProgressBar1>
    }

            <Button onPress={()=>this.props.editVenue&&this.props.editVenue(item)} style={{backgroundColor:color.orangeBtn,width:hp('15%'),marginRight:0,marginBottom:12,justifyContent:'center',borderRadius:5,padding:0,marginTop:12}}><Text style={{textAlign:'right',fontSize:hp('2')}}>Edit</Text></Button>
            </View>
                 
                  
                  
                  </View>
                
              </Body>
            </CardItem>
           
          </Card>
        </Content>
    );
  }
}