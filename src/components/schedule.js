import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Button, Right } from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen"; 
import dateFormat from "dateformat";
import moment from "moment";
import color from '../Helpers/color';




export default class SplitPopUp extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     selected: [],
                     list: props.list
                   };
                   this._onPressButton = this._onPressButton.bind(this);
                 }

                 _submit=()=>{ 
                   let checkedListSlots = this.state.list.filter((item,i)=>item.checked==true);
                   if (checkedListSlots.length) 
                   { this.props.onSubmit(checkedListSlots);
                     

                   }
                  
                   else return;
    
                 }

                 _onPressButton = (value, checked) => {
                  
                   var list = this.state.list;
                   list[value].checked = checked ? !checked : true;
                   this.setState({ list }, () => this._submit());
                   //  var findindex = this.state.selected.findIndex(
                   //    obj => obj == value
                   //  );
                   //  var selected = this.state.selected;
                   //  if (findindex != -1) {
                   //    selected.splice(findindex, 1);
                   //  } else {
                   //    selected.push(value);
                   //  }
                   //  this.setState({ selected });
                   //  this.props.onClick(this.state.selected);
                 };

                 componentDidUpdate = (prevProps, prevState) => {
                
                 };

                 componentWillReceiveProps(props) {
                   this.setState({
                     list: props.list
                   });
                 }

                 renderTimeFormat = date => {
                   return moment(date, "HH:mm").format("hh:mm A");
                 }; 

                
                 renderSchedule = isOnClick => { 
                   const {selectedDate,oldWeekData} = this.props; 
                  //  alert(oldWeekData.length);
                  var oldSelected = oldWeekData.length >0&& oldWeekData.filter((data,j)=>{
                     if(data.selectedDate==selectedDate)
                     { 
                     
                       return data 
                     }
                   })
                   return (
                     this.state.list &&
                     this.state.list.map((value, i) => { 

                      // 
                     var match = oldSelected.length>0 && oldSelected[0].selectedSlots.filter(
                       item => {
                         if(item.venue_slot_start_time ==
                           value.venue_slot_start_time)
                           return item
                       }
                     );
                    
                       //  alert();
                       // console.log(value.checked);
                       var findindex = this.state.selected.findIndex(
                         obj => obj == value.trn_booking_slot_id
                       );
                       let selected;

                       if (value.checked == true) {
                         //  console.log("iruku");
                         selectedBox = {
                           backgroundColor: "#eb5b00",
                           color: "white"
                         };
                       } else selectedBox = { backgroundColor: "white" };

                       var notAvail =
                         value.bookingslottype == 2 
                           ? {
                               backgroundColor: "rgb(210,213,227)",
                               borderColor: "rgb(50,65,146)",
                               borderWidth: 1
                             }
                           : {}; 

                           var booked =
                             value.bookingslottype == 1
                               ? {
                                   backgroundColor: "#d5d4d4",
                                   borderColor: "rgb(156, 155, 155)",
                                   borderWidth: 1
                                 }
                               : {}; 


                       return (
                         <View style={[styles.box]} key={i}>
                           <TouchableOpacity
                             onPress={() =>
                               value.bookingslottype == 2 ||
                               value.bookingslottype == 1
                                 ? ""
                                 : this._onPressButton(i, value.checked)
                             }
                             style={[
                               styles.button,
                               selectedBox,
                               notAvail,
                               booked
                             ]}
                           >
                             <Text
                               style={[
                                 {
                                   textAlign: "center"
                                 },
                                 selectedBox,
                                 value.bookingslottype == 2
                                   ? {
                                       backgroundColor: "transparent",
                                       color: "rgb(50,65,146)",
                                       borderColor: " rgb(50, 65, 146)"
                                     }
                                   : {},
                                 value.bookingslottype == 1
                                   ? {
                                       backgroundColor: "transparent",
                                       color: "rgb(50,65,146)",
                                       borderColor: " rgb(50, 65, 146)"
                                     }
                                   : {}
                               ]}
                             >
                               {this.renderTimeFormat(
                                 value.venue_slot_start_time
                               )}{" "}
                               -{" "}
                               {this.renderTimeFormat(
                                 value.venue_slot_end_time
                               )}
                             </Text>
                           </TouchableOpacity>
                         </View>
                       );
                     })
                   );
                 };

                 render() {
                   const {
                     boxColor,
                     isOnClick,
                     list,
                     oldWeekData
                   } = this.props;

                   console.log("thisprop", oldWeekData);
                   return (
                     <React.Fragment>
                       <View
                         style={[
                           styles.container,
                           { backgroundColor: boxColor }
                         ]}
                       >
                         {this.renderSchedule(isOnClick)}
                       </View>
                       {!this.props.disableSubmit && (
                         <View
                           style={{
                             flexDirection: "row",
                             justifyContent: "flex-end",
                             alignItems: "flex-end",
                             paddingRight: 30,
                             backgroundColor: "#f6f6f6"
                           }}
                         >
                           <Button
                             onPress={() => this._submit()}
                             style={styles.actionbtn}
                           >
                             <Text style={styles.actionbtntxt}>Submit</Text>
                           </Button>
                         </View>
                       )}
                     </React.Fragment>
                   );
                 }
               }

const styles = StyleSheet.create({
  button: {
    backgroundColor:'#fff',
    color: "#000",
    minWidth: wp("40%"),
    marginTop:4,
    padding:12
  },
  box: {
    paddingVertical: hp("1.4%")
  },
  container: {
    flex: 1,
    flexDirection: "row",
    padding: 20,
    justifyContent: "space-between",
    flexWrap: "wrap",
    
  },
   actionbtn: {
                   borderRadius: 5,
                   width: hp("17%"),
                   justifyContent: "center",
                   backgroundColor: color.blue
                 },
                 actionbtntxt: {
                   textAlign: "center",
                   color: color.white,
                   fontSize: hp("2.3%")
                 }
});
