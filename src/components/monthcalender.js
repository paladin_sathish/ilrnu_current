import React, { PureComponent } from "react";
import {View} from 'react-native';
import CalendarPicker from "react-native-calendar-picker";

export default class MonthCalender extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: null,
      selectedEndDate: null
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date, type) {
    alert();
    console.log("date");
    if (type === "END_DATE") {
      this.setState({
        selectedEndDate: date
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null
      });
    }
  }

  render() {
    const { selectedStartDate, selectedEndDate } = this.state;
    const minDate = new Date(); // Today
    const maxDate = new Date(2020, 6, 3);
    const startDate = selectedStartDate ? selectedStartDate.toString() : "";
    const endDate = selectedEndDate ? selectedEndDate.toString() : "";

    return (
      <React.Fragment>
        <View style={{backgroundColor:'white',marginBottom:15}}>
          <CalendarPicker
            startFromMonday={true}
            allowRangeSelection={true}
            minDate={minDate}
            maxDate={maxDate}
            todayBackgroundColor="#ea5b02"
            selectedDayColor="#ea5b02"
            selectedDayTextColor="#fff"
            onDateChange={ this.onDateChange}
          />
        </View>
      </React.Fragment>
    );
  }
}


