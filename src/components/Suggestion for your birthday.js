import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import ModalDropdown  from 'react-native-modal-dropdown'
import {Actions,Router,Scene} from 'react-native-router-flux';


const playgr_list=[
    {image:require('../images/slider1.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider2.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider3.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider4.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider5.png'),text:'Meadow Crest Playground'}
]

export default class NearbyPlayGrounds extends Component{
    constructor(props) {
      super(props);
    
      this.state = {loginDetails:null};
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.flex_row}>
                    <View style={styles.flex_row}>
                        <Text style={[styles.text_style1,{marginRight:2}]}>Suggestion for your</Text>
                        <ModalDropdown   
                                    style={{marginRight:3}}    
                                    textStyle={[styles.text_style]}
                                    dropdownStyle={styles.dropdown_options}
                                    defaultValue={'Birthday'}
                                    showsVerticalScrollIndicator={false}	
                                                                
                                options={['Near You', 'Already Booked','with OFFER *','Low Cost']} />

                        <View style={{flexDirection:'column',justifyContent:'center'}}>
                            <Image source={Down_arrow} style={styles.Down_arrowstyle}></Image>
                        </View>
                    </View>
                    <View style={styles.flex_sep}>
                        <TouchableOpacity onPress={()=>Actions.venueform()} style={styles.add_symbol_container} >
                            <Image style={styles.add_symbol} source={ADD_img}>
                            </Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <FlatList
                   data={playgr_list}
                   horizontal={true}
                   showsHorizontalScrollIndicator={false}
                   renderItem={({item,index})=>( 
                    <View style={styles.square}>
                        <Image style={styles.image_style} 
                            source= {item.image}>       
                        </Image>
                        <Text style={styles.text_style2}>
                            {item.text}
                        </Text>    
                    </View>
                )}/> 

            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
    image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },
    text_style2:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
    },
    flex_row:{
        flexDirection:'row'
    },
    text_style1:{
        fontSize:hp('2%')
    },
    text_style:{
        color:color.blue,
        fontSize:hp('2%')
    },
    Down_arrowstyle:{
        height:hp('1.4%'),
        width:hp('1.4%')
    },
    flex_sep:{
        flex:1
    },
    add_symbol_container:{
        backgroundColor:color.blue,
        justifyContent:'flex-end',
        justifyContent:'center',
        alignSelf:'flex-end',
        marginRight:wp('5%'),
        height:hp('2.5%'),
        width:hp('2.5%'),
       
    },
    add_symbol:{
        height:hp('1.3%'),
        width:hp('1.3%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
    },
})