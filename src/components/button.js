import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import color from "../Helpers/color";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
const Next = ({ onClick, name, _width }) => (
  <View style={styles.container}>
    <View style={[styles.buttonContainer,{width:_width}]}>
      <TouchableOpacity onPress={() => onClick()}>
        <Text
          style={{
            color: "white",
            fontSize: hp("2%"),
            fontWeight: "bold",
            textAlign: "center"
          }}
        >
          {name}
        </Text>
      </TouchableOpacity>
    </View>
  </View>
); 

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: color.blueactive,
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 20,
    shadowOpacity: 0.25,
    alignSelf: "center"
  },
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  }
});

export default Next;
