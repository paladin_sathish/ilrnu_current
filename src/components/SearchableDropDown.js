import React, { Component } from 'react';
import { View,Text,Image } from 'react-native';
import links from '../Helpers/config';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import search from '../images/Search.png'

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const items = [
  // this is the parent or 'item'
  {
    name: 'Ameneties',
    id: -1,
    // these are the children or 'sub items'
    children: [
     
    ],
  },
];

export default class SearchDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItems: [],
      items:[],
      selectedArray:[]
    };
    console.log(props);
  }
  componentWillReceiveProps=(props)=>{
    // console.log(props)
    if(props.amenitiesDropdownData.length>=0){
      var selectedItems=this.state.selectedItems;
      selectedItems=props.amenitiesDropdownData.map((obj)=>obj.id);
      this.setState({selectedItems});
    }
  }
  onSelectedItemsChange = (selectedItems) => {
    var selectedArray=[];
this.setState({ selectedItems });
// alert(JSON.stringify(selectedItems));
for(var i in selectedItems){
  if(Number(selectedItems[i])){
var obj={id:selectedItems[i],name:this.renderItems(selectedItems[i])}
selectedArray.push(obj)
  }
}
this.props.selectedDataArray&&this.props.selectedDataArray(selectedArray)

// this.setState({selectedArray})
// alert(JSON.stringify(this.state.selectedArray));
  };
receiveAmeneties=(data)=>{
  if(data.length>0){
    fetch(links.APIURL+'searchAmenities', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
        'amenitiesName':data
        }),
    }).then((response)=>response.json())
    .then((responsejson)=>{
      var items=this.state.items;
      items=responsejson.data
      this.setState({items})
    })
  }
}
renderItems=(index)=>{
  var findIndex=this.state.items.findIndex((obj)=>obj.amenities_id==index);
  if(findIndex!=-1){
    return this.state.items[findIndex].amenities_name;
  }else{
    return 'undefined';
    // this.state.selectedItems.splice(index,1);
  }
}
renderSelectText=()=>{
   const { selectedItems } = this.state
  return(
    <View style={{flex:1,flexDirection:'row',marginLeft:-10}}>
     <View style={styles.view6}><Text style={{textAlign:'left'}}>Amenities</Text></View>
    <View style={{borderWidth:0.5,flex:0.55,height:hp('5%'),alignItems:'center',flexDirection:'row',paddingLeft:8}}>
    
        <Text>{selectedItems.length>0?selectedItems.length+" Selected":''} </Text>
       
    </View>
     <View style={styles.view8}>
                                <Image source={search}  style={styles.image_style1}></Image>
                            </View>
    </View>
    )
}
  render() {
    return (
      <View style={{flex:1,paddingLeft:0}}>
        <SectionedMultiSelect
        styles={{borderWidth:1,borderColor:'black'}}
        searchAdornment={(data)=>this.receiveAmeneties(data)}
          items={this.state.items.length>0?this.state.items:[]}
          uniqueKey="amenities_id"
          selectToggleIconComponent={<Image/>}
          displayKey="amenities_name"
          showRemoveAll={true}
          showChips={false}
          searchPlaceholderText="Search Amenities"
          renderSelectText={()=>this.renderSelectText()}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={this.state.selectedItems}
        />
      </View>
    );
  }
}
const styles={

  view6:{
        flex:.35,
        flexDirection:'row',
        justifyContent:'flex-start',
        marginRight:12
    },
     view8:{
        flex:.1
    },
       image_style1:{
        padding: 5,
        margin: 5,
        height: hp('2.7%'),
        width: hp('2.7%'),
        resizeMode : 'stretch',
        alignItems: 'center'
    },
}