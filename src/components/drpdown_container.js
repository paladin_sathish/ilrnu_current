import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Tabs,
  Tab,
  TabHeading,
  Content,
} from "native-base";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  Picker,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Down_array_black from "../images/down_arrow_black.png";
import ModalDropdown from "react-native-modal-dropdown";
import color from "../Helpers/color";

export default class Dropdown_conatiner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bg: false,
    };
  }

  onLayout = (event) => {
    const { x, y, height, width } = event.nativeEvent.layout;

    // alert(JSON.stringify(event.nativeEvent.layout))

    this.setState({ LayoutProp: event.nativeEvent.layout });
  };
  renderButtonText = (data) => {
    // alert(JSON.stringify(data));
    if (this.props.sendDropdownData) {
      this.props.sendDropdownData(data);
    }
  };
  dropdownShow = (data) => {
    // alert('');
    // if(this.props.activestate){
    //   return false;
    // }
    // if(!this.props.values){

    //   return false;
    // }
    // if(this.props.values&&this.props.values.length==0){

    //   return false;
    // }
    return false;
  };
  receiveFram = (data) => {
    // alert(JSON.stringify(data));

    // return data;
    if (data.top) {
      if (this.props._width || this.props.fakewidth) {
        data.top = data.top + this.state.LayoutProp.height - 16;
      } else {
        data.top = data.top + this.state.LayoutProp.height - 38;
      }
    }

    if (data.left) {
      data.left = data.left - 5;
    }
    if (data.right) {
      data.right = data.right - 5;
    }
    if (this.props.activestate) {
      data.width = wp("55%");
    }
    if (this.props._width) {
      // data.width=this.props._width;
    }
    if (this.props.fullwidth) {
      data.width = this.state.LayoutProp.width;
    }
    // data.width=this.state.LayoutProp.width;
    // alert(JSON.stringify(data));
    return data;
  };
  showDropdown = () => {
    // alert(JSON.stringify(this.props));
    if (!this.props.values) {
      return;
    }
    if (this.props.values && this.props.values.length == 0) {
      return;
    }
    if (this.props.noarrow) {
      return;
    }
    this.setState({ bg: true });
    this.dropdown.show();
  };
  render() {
 
    return (
      <View onLayout={this.onLayout}>
        {/* <View style={styles.container}> */}
        {/* <View style={[styles.view1,{minWidth:this.props.drop_items==""?wp('55%'):null}]}> */}

        <ModalDropdown
          ref={(el) => (this.dropdown = el)}
          adjustFrame={this.receiveFram}
          style={{
            paddingLeft: 5,
            paddingRight: 5,
            alignItems: "center",
            borderRadius: this.props._width || this.props.fakewidth ? 12 : 0,
            borderWidth: this.props._borwidth ? this.props._borwidth : 0,
            borderColor: this.props._borColor
              ? this.props._borColor
              : "transparent",
            flexDirection: "row",
            height: this.props._width || this.props.fakewidth ? 25 : 40,
            backgroundColor: this.props.action
              ? this.state.bg
                ? color.blue
                : color.white
              : color.white,
            minWidth: this.props.fakewidth
              ? this.props.value == ""
                ? wp("55%")
                : null
              : this.props._width,
          }}
          textStyle={[styles.dropdown_text]}
          dropdownStyle={[styles.dropdown_options]}
          defaultValue={this.props.drop_items}
          onDropdownWillHide={(data) => this.setState({ bg: false })}
          onDropdownWillShow={(data) => this.dropdownShow(data)}
          showsVerticalScrollIndicator={true}
          renderRow={(rowData) => (
            <View>
              <Text style={{ fontSize: 14, padding: 10 }}>
                {this.props.keyvalue ? rowData[this.props.keyvalue] : rowData}
              </Text>
            </View>
          )}
          renderButtonText={this.renderButtonText}
          dropdownTextStyle={{ paddingRight: 50 }}
          options={this.props.values}
        >
          <TouchableOpacity
            style={[
              styles.dropdownButton,
              {
                tintColor: this.props.action
                  ? this.state.bg
                    ? color.white
                    : null
                  : null,
              },
            ]}
            onPress={() => this.showDropdown()}
          >
            <View
              style={{
                minwidth: "100%",
                width: this.props.fullwidth && "100%",
                paddingRight: 12,
              }}
            >
              <Text
                style={[
                  styles.dropdownCurrentText,
                  {
                    color: this.props.action
                      ? this.state.bg
                        ? color.white
                        : null
                      : null,
                  },
                  this.props.font && {
                    fontSize: this.props.font ? this.props.font : "auto",
                  },
                ]}
                numberOfLines={1}
                ellipsizeMode={"tail"}
              >
                {this.props.value}
              </Text>
            </View>

            <View style={styles.dropdownIconWrap}>
              {!this.props.noarrow && (
                <Image
                  tintColor={
                    this.props.action
                      ? this.state.bg
                        ? color.white
                        : null
                      : null
                  }
                  style={styles.drop_down_top}
                  source={Down_array_black}
                />
              )}
            </View>
          </TouchableOpacity>
        </ModalDropdown>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // height:hp('4%'),
    borderRadius: 15,
    backgroundColor: color.white,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: "row",
  },
  dropdownIconWrap: {
    paddingRight: 2,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    position: "absolute",
    right: 0,
    width: 20,
    height: "100%",
    // backgroundColor:'blue',
    zIndex: 9999,
  },
  text_style: {
    fontSize: hp("2%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
  },
  drop_down_top: {
    height: hp("1%"),
    width: hp("1%"),
  },
  view1: {
    //    flex:.9,
    marginLeft: 1,
    justifyContent: "flex-start",
  },
  view2: {
    //  flex:.1,
    flexDirection: "row",
    position: "absolute",
    //   justifyContent:'center',
    alignItems: "center",
    marginRight: 5,
    marginLeft: 5,
    right: 0,
    top: hp("1.5%"),
  },
  dropdown_options: {},
  dropdownButton: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 5,
  },
  dropdown_text: {
    width: "100%",
  },
});
