 const colors={
	white:'#fff',
	orange:'#FF9008',
	blueactive:'#24479D',
	grey:'grey',
	black:'#000',
	black1:'#959595',
	ash:'#f2f2f2',
	ash1:'#f8f8f8',
	ash2:'#dadada',
	grey1:'#fefefe',
	red:'red',
	ash3:'#c9c9c9',
	orangeBtn:'#FF9008',
	ash4:'#e4e4e4',
	ash5:'#e5e5e5',
	ash6:'#c2c2c2',
	blue:'#007bff'
}
export default colors;