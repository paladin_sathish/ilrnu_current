

import React, { Component } from 'react';
import dateFormat from 'dateformat';
import moment from 'moment';
 class DateFunctions extends Component {


getHoursInterval(time1,time2,interval){

 var arr = [];
  while(time1 < time2){
    var nextTime=new Date(time1.toString());
    var AddedMinutes=new Date(nextTime.setMinutes(nextTime.getMinutes() + interval));
    var formattedFrmTime=dateFormat(time1,'hh:MM TT');
    console.log(formattedFrmTime);
    var formattedToTime=dateFormat(AddedMinutes,'hh:MM TT');
    arr.push({id:new Date().getTime(),label:formattedFrmTime+" - "+formattedToTime,dateobj:{fromTime:dateFormat(time1,"yyyy-mm-dd'T'HH:MM:00"),toTime:dateFormat(AddedMinutes,"yyyy-mm-dd'T'HH:MM:00")}});
    time1.setMinutes(time1.getMinutes() + interval);
  }
  return arr;
}
checkDateString(date){
	if(typeof date=='string'){
		return new Date(date);
	}else{
		return date;
	}
}
enumerateDaysBetweenDates(startDate, endDate) {
    startDate = new Date(startDate);
    endDate = new Date(endDate);
    var dateArray = [];
    var currentDate = startDate;
    while (currentDate <= endDate) {
    console.log("checking",currentDate +"<="+ endDate)
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

getMobileDate(dateString) { 
  // dateString *HAS* to be in this format "YYYY-MM-DD HH:MM:SS"
  let dateParam = dateString.split(/[\s-:]/)  
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString()  
  // alert(dateParam);
  return new Date(...dateParam)  
}
converttime24Convertor(time) {
    var PM = time.match('PM') ? true : false
    
    time = time.split(':')
    var min = time[0]
    
    if (PM) {
        var hour = 12 + parseInt(time[0],10)
        var min = time[1].replace('PM', '')
    } else {
        var hour = time[0]
        var min = time[1].replace('AM', '')       
    }
    
    return (hour+':'+min+':00').replace(/ /g, '')
}
 converttime24to12 (time) {
var timeString = time;
var H = +timeString.substr(0, 2);
var h = H % 12 || 12;
var ampm = (H < 12 || H === 24) ? "AM" : "PM";
timeString = (h<10?("0"+h):h) + timeString.substr(2, 3) +" "+ ampm;
return timeString
}
monthDiff(d1, d2) {
var a = d1;
var b = d2;

// Months between years.
var months = (b.getFullYear() - a.getFullYear()) * 12;

// Months between... months.
months += b.getMonth() - a.getMonth();

// Subtract one month if b's date is less that a's.
if (b.getDate() < a.getDate())
{
    months--;
}
return months;

}
    generateTimestamp(){
    var d = new Date().getTime();//Timestamp
    var d2 =  0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
      }
}
export default new DateFunctions();
