const color = {
  white: "#ffffff",
  black: "#000000",
  top_red: "#a70000",
  drop_down_icon: "#fb8332",
  orange: "#FF9008",
  blue: "#073cb2",
  bluesub: "#335DBE",
  circle_red: "#a70000",
  orangeDark: "#EB5C00",
  brown: "#A10000",
  //orange:'#FF9008',
  blueactive: "#24479D",
  grey: "grey",
  green: "#218721",
  yellow: "#ffc700",
  black1: "#959595",
  ash: "#f2f2f2",
  ash1: "#f8f8f8",
  ash2: "#dadada",
  grey1: "#fefefe",
  red: "red",
  ash3: "#c9c9c9",
  orangeBtn: "#FF9008",
  ash4: "#e4e4e4",
  ash5: "#e5e5e5",
  ash6: "#c2c2c2",
  skyblue: "#6d8ad0",
  purple: "#473080"

  //	blue:'#007bff'
};
export default color;