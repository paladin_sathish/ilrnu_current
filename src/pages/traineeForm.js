import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Tabs,
  Tab,
  TabHeading,
  Content,
  Root
} from "native-base";
import {
  View,
  Image,
  StatusBar,
  Picker,
  Text,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  BackHandler,
  ToastAndroid
} from "react-native";
import color from "../Helpers/color";
import links from "../Helpers/config";
import SubHeaderComp from "../components/subHeader";
import TraineeType from "../pages/traineeType";
import TraineeCategory from "../pages/traineeCategory";
import TraineeDetails from "../pages/traineeDetails";
import { Actions } from "react-native-router-flux";
import Chair from "../images/svg/chair.png";
import DeskF from "../images/svg/deskf.png";
import Access from "../images/svg/accessibility.png";
import Laptop from "../images/svg/laptop.png";
import Seats from "../images/svg/seats.png";
import Bathroom from "../images/svg/bathroom.png";
import Parking from "../images/svg/parking.png";
import Coffee from "../images/svg/coffee-cup.png";
import Door from "../images/svg/door.png";
import AmenitiesPNG from "../images/TabPNG/AmenitiesPNG.png";
import AvailabilityPNG from "../images/TabPNG/AvailabilityPNG.png";
import FacilityPNG from "../images/TabPNG/FacilityPNG.png";
import PricecardPNG from "../images/TabPNG/PricecardPNG.png";
import ReviewPNG from "../images/TabPNG/ReviewPNG.png";
import UploadphotoPNG from "../images/TabPNG/UploadphotoPNG.png";
import TaglistPNG from "../images/TabPNG/TaglistPNG.png";
import VenuetypePNG from "../images/TabPNG/VenuetypePNG.png";
import AsyncStorage from "@react-native-community/async-storage";
import DynamicPagination from "../components/DynamicPagination";
import Toast from "react-native-simple-toast"; 
import ValidationLibrary from "../Helpers/validationfunction";
import TraineeMode from './traineeMode';
import LoadAPIPost from "../Helpers/xhrapi";
import TraineeSuccess from "./TraineeSuccess";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
var datearray = [
  { id: 1, name: "USD" },
  { id: 2, name: "INR" },
  { id: 3, name: "MYR" }
];
var datasHourlyWeekly = [
  { name: "Hourly", id: 1, keyname: "Hour" },
  { name: "Daily", id: 2, keyname: "Day" },
  { name: "Weekly", id: 3, keyname: "Week" },
  { name: "Monthly", id: 4, keyname: "Month" }
];

const datas = [
  { name: "Seating", id: 1, circleCount: null, active: false, icon: Chair },
  { name: "Physical\nInfrastructure", id: 2, icon: DeskF },
  { name: "Accessibility", id: 3, icon: Access },
  { name: "Training Equipments", id: 4, icon: Laptop },
  { name: "IT Infra", id: 5, icon: DeskF },
  { name: "Resource", id: 6, circleCount: null, active: false, icon: Seats },
  { name: "Toilets", id: 7, icon: Bathroom },
  { name: "Parking", id: 8, icon: Parking },
  { name: "Pantry", id: 9, icon: Coffee },
  { name: "Additional Rooms", id: 10, icon: Door }
];

datas.push({ name: "Hourly", id: 1, activeId: 1 });
datas.push({ name: "Hourly", id: 1, activeId: 1 });
const newtags = [
  {
    id: 1,
    name: "Add (or) Tag the related General Keywords",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false },
      { id: 4, name: "Special Tag Items display here", state: false },
      { id: 5, name: "Corporate Trainings", state: false }
    ]
  },
  {
    id: 2,
    name: "What category on venue type",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false },
      { id: 4, name: "Special Tag Items display here", state: false }
    ]
  },
  {
    id: 3,
    name: "Suitable Training- IT / Soft Skill / Handson",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false }
    ]
  }
];
// const newtags1=[{id:1,name:'Add (or) Tag the related General Keywords',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:2,name:'What category on venue type',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:3,name:'Suitable Training- IT / Soft Skill / Handson',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]}]
var commonDataForFacility = [
  {
    spec_det_id: "idname",
    venue_spec_id: 1,
    spec_det_name: "Sport Name",
    spec_det_sortorder: 0,
    spec_det_datatype1: "picker",
    spec_det_datavalue1: "1",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    picker_data: [],
    validation: [{ name: "required" }, { name: "minLength", params: 30 }],
    error: null,
    errormsg: ""
  },
  {
    spec_det_id: "idname1",
    venue_spec_id: 1,
    spec_det_name: "Experience (Years)",
    spec_det_sortorder: 0,
    spec_det_datatype1: "exp",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 30 }],
    error: null,
    errormsg: ""
  },

  {
    spec_det_id: "idname2",
    venue_spec_id: 1,
    spec_det_name: "Experience (Months)",
    spec_det_sortorder: 0,
    spec_det_datatype1: "exp",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 30 }],
    error: null,
    errormsg: ""
  },

  {
    spec_det_id: "idname3",
    venue_spec_id: 1,
    spec_det_name: "Certification",
    spec_det_sortorder: 0,
    spec_det_datatype1: "btn_address1",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 300 }],
    error: null,
    errormsg: ""
  },
  {
    spec_det_id: "idname4",
    venue_spec_id: 1,
    spec_det_name: "Achievements",
    spec_det_sortorder: 0,
    spec_det_datatype1: "textarea",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 1000 }],
    error: null,
    errormsg: ""
  },
  {
    spec_det_id: "idname5",
    venue_spec_id: 1,
    spec_det_name: "Location",
    spec_det_sortorder: 0,
    spec_det_datatype1: "btn_address",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 300 }],
    error: null,
    errormsg: ""
  }
];

const mode = [
  {
    amenities_id: 162,
    amenities_name: "ILT",
    amenities_icon: "https://www.ivneu.com/ivneuUploadsDir/toilet-55.png",
    amenities_array: [1, 2, 3, 4]
  },
  {
    amenities_id: 163,
    amenities_name: "Live Stream",
    amenities_icon: "https://www.ivneu.com/ivneuUploadsDir/toilet-55.png",
    amenities_array: [1, 2, 3, 4]
  },
  {
    amenities_id: 164,
    amenities_name: "Video Base",
    amenities_icon: "https://www.ivneu.com/ivneuUploadsDir/toilet-55.png",
    amenities_array: [1, 2, 3, 4]
  }
];
export default class TraineeForm extends Component {
                 constructor(props) {
                   super(props);
                   // Actions.corporateform();
                   var venueform = JSON.parse(
                     JSON.stringify(require("../Helpers/traineeform.json"))
                   );
                   this.state = {
                     chooseVenueData: "",
                     chooseVenueState: null,
                     autosave: true,
                     init: true,
                     list1: null,
                     splitData: null,
                     commonArray: [
                       {
                         spec_det_id: "idname",
                         venue_spec_id: 1,
                         spec_det_name: "Sport Name",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "picker",
                         spec_det_datavalue1: "1",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         picker_data: [],
                         validation: [
                           { name: "required" },
                           { name: "minLength", params: 30 },
                         ],
                         error: null,
                         errormsg: "",
                       },
                       {
                         spec_det_id: "idname1",
                         venue_spec_id: 1,
                         spec_det_name: "Experience (Years)",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "number",
                         spec_det_datavalue1: "",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         validation: [
                           { name: "required" },
                           { name: "minLength", params: 30 },
                         ],
                         error: null,
                         errormsg: "",
                       },

                       {
                         spec_det_id: "idname2",
                         venue_spec_id: 1,
                         spec_det_name: "Experience (Months)",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "number",
                         spec_det_datavalue1: "",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         validation: [
                           { name: "required" },
                           { name: "minLength", params: 30 },
                         ],
                         error: null,
                         errormsg: "",
                       },

                       {
                         spec_det_id: "idname3",
                         venue_spec_id: 1,
                         spec_det_name: "Certification",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "btn_address1",
                         spec_det_datavalue1: "",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         validation: [
                           { name: "required" },
                           { name: "minLength", params: 300 },
                         ],
                         error: null,
                         errormsg: "",
                       },
                       {
                         spec_det_id: "idname4",
                         venue_spec_id: 1,
                         spec_det_name: "Achievements",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "textarea",
                         spec_det_datavalue1: "",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         validation: [
                           { name: "required" },
                           { name: "minLength", params: 1000 },
                         ],
                         error: null,
                         errormsg: "",
                       },
                       {
                         spec_det_id: "idname5",
                         venue_spec_id: 1,
                         spec_det_name: "Address",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "textareamap",
                         spec_det_datavalue1: "",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "12.9716,77.5946",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         validation: [
                           { name: "required" },
                           { name: "minLength", params: 300 },
                         ],
                         error: null,
                         errormsg: "",
                       },
                       {
                         spec_det_id: "idname6",
                         venue_spec_id: 1,
                         spec_det_name: "LandMark ",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "text",
                         spec_det_datavalue1: "",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         validation: [],
                         error: null,
                         errormsg: "",
                       },
                       {
                         spec_det_id: "idname7",
                         venue_spec_id: 1,
                         spec_det_name: "Area",
                         spec_det_sortorder: 0,
                         spec_det_datatype1: "text",
                         spec_det_datavalue1: "",
                         spec_det_datatype2: "",
                         spec_det_datavalue2: "",
                         spec_det_datatype3: "",
                         spec_det_datavalue3: "",
                         validation: [
                           { name: "required" },
                           { name: "minLength", params: 300 },
                         ],
                         error: null,
                         errormsg: "",
                       },
                     ],

                     availabilityState: null,
                     courseDetailData: null,
                     courseDetailState: null,
                     courseDetail1Data: null,
                     courseDetail1State: null,
                     purposeData: null,
                     showSkip: null,
                     loginDetails: this.props.navigation.state.params.logindata,
                     facilityvalidations: null,
                     facilityIndex: 0,
                     currentIndex: 0,
                     facitlityactive: null,
                     traineemodedata: null,
                     activeparent: "tab1",
                     venuetypetext: "",
                     venueheadertext: "",
                     list: [
                       { id: "1", name: "tab1", image: VenuetypePNG },
                       { id: "2", name: "tab2", image: FacilityPNG },
                       { id: "3", name: "tab3", image: AvailabilityPNG },
                       { id: "4", name: "tab4", image: AmenitiesPNG },
                       { id: "5", name: "tab5", image: TaglistPNG },
                       { id: "6", name: "tab6", image: PricecardPNG },
                       { id: "7", name: "tab7", image: UploadphotoPNG },
                       { id: "8", name: "tab8", image: ReviewPNG },
                     ],
                     _scrollToBottomY: 0,
                     facilityData: {
                       roomname: "",
                       seats: "",
                       floor: "",
                       address: "",
                       landmark: "",
                       mobile: "",
                       mail: "",
                     },
                     venuetypedata: null,
                     facilitytypedata: null,
                     availability: {
                       type: null,
                       days: null,
                       from: null,
                       to: null,
                       moredetails: null,
                     },
                     ameneties: null,
                     venTags: null,
                     venueprice: null,
                     venueform: venueform,
                     venueformData: new FormData(),
                     validations: [
                       {
                         id: 1,
                         name: "venuetype",
                         errmsg: "Choose Venue Type",
                         state: false,
                       },
                       {
                         id: 2,
                         name: "venuetype",
                         errmsg: "Choose Venue Facility",
                         state: false,
                       },
                       {
                         id: 3,
                         name: "facility",
                         errmsg: "Please Fill up facility details",
                         state: false,
                       },
                       {
                         id: 4,
                         name: "availability",
                         errmsg: "Please Fill up avaialability details",
                         state: false,
                       },
                       {
                         id: 5,
                         name: "price",
                         errmsg: "Please Fill up price Details",
                         state: false,
                       },
                       {
                         id: 6,
                         name: "purposes",
                         errmsg: "Please Choose purposes and actions",
                         state: false,
                       },
                     ],
                   };
                   if (this.props.navigation.state.params.welcomemsg) {
                     Toast.show(
                       this.props.navigation.state.params.welcomemsg,
                       Toast.LONG
                     );
                   }
                 }
                 goToRight = () => {
                   // this.scroll.scrollTo({ x: 0, y: 0, animated: true });
                 };
                 changeTab = (data) => {
                   // alert(data);
                   var list = this.state.list;
                   var filterrecords = list.filter((obj) => obj.id == data + 1);

                   this.setState({ currentIndex: data });
                   this.setState({ activeparent: filterrecords[0].name });
                 };

                 async componentWillMount() {
                   const data = await AsyncStorage.getItem("loginDetails");
                   var parsedata = JSON.parse(data);
                   this.setState({ loginDetails: parsedata });
                   var venueform = this.state.venueform;
                   venueform.trainer.userId = parsedata.user_id;
                   this.setState({ venueform });
                 }

                 componentDidMount() {
                   if (this.props.Trainee) {
                     // alert("");
                     var venueform = this.state.venueform;
                     var editData = this.props.Trainee;
                     venueform.trainer.userId = this.state.loginDetails.user_id;
                     venueform.trainer.trainerId = this.props.Trainee.trainer_id;
                     venueform.trainer.trainerImage =
                       editData.aliasImageNameHelper;
                     
                     var venueformData = this.state.venueformData;

                    

                     this.setState({ edit: true, autosave: false }, () => {
                       console.log(
                         this.state.edit && !this.state.autosave
                           ? "editTrainerDetails/"
                           : "addTrainerDetails"
                       );
                     });
                     console.log("willmounttraineedit", this.props.Trainee);
                     console.log("edit", editData.trainer_exp_year);

                     //load Venue category
                     this.sendtraineetypedata(
                       {
                         CategoryId: editData.trainer_cat_id,
                         CategoryName: editData.training_catName,
                       },
                       true
                     );

                     this.sendtraineemodetypedata(
                       {
                         ModeOfTrainingId: editData.trainer_mode,
                         ModeOfTrainingName: "",
                       },
                       true
                     );

                     //load specDetails
                     this.sendfacilitytypedata(
                       {
                         SpecificId: editData.trainer_spec_id,
                         SpecificName: editData.training_spec_name,
                       },
                       true
                     );

                     var commonData = JSON.parse(
                       JSON.stringify(this.state.commonArray)
                     ); 

                     commonData[0].spec_det_datavalue1 = editData.sub_spec_id;
                     commonData[1].spec_det_datavalue1 = editData.trainer_exp_year.toString();
                     commonData[2].spec_det_datavalue1 = editData.trainer_exp_month.toString();

                     commonData[3].spec_det_datavalue1 =
                       editData.trainer_certification_details;

                     commonData[4].spec_det_datavalue1 =
                       editData.trainer_achivements;

                     commonData[5].spec_det_datavalue1 =
                       editData.trainer_address;

                     commonData[5].spec_det_datavalue2 =
                       editData.trainer_location;
                     commonData[6].spec_det_datavalue1 =
                       editData.tariner_landmark;

                     commonData[7].spec_det_datavalue1 = editData.trainer_area;

                      if (
                        editData.aliasImageNameHelper != "" &&
                        editData.aliasImageNameHelper != "undefined" &&
                        editData.aliasImageNameHelper!=null
                      ){ 
                        var myphotos = [
                          {
                            uid: "",
                            name: editData.trainer_image.split("/")[
                              editData.trainer_image.split("/").length - 1
                            ],
                            status: "done",
                            url: editData.trainer_image,
                            exist: true,
                          },
                        ]; 

                        venueform.formArray = myphotos;

                      }
                         

                    //  venueform.formArray.push(myphotos);


                     
                     // this.setState({formimagearray:null,}) 

                     
                     console.log("formarra", venueform.formArray);
                     var venueformData = this.state.venueformData;
                     venueformData.set("existImages", JSON.stringify(myphotos));
                     venueformData.set("trainer", JSON.stringify(venueform.trainer));


                     //  this.setState({commonArray});
                     this.sendfaciltiydata(
                       this.state.facilitytypedata,
                       commonData,
                       true,
                       "dontupdate"
                     );

                     this.setState({
                       venueform,venueformData
                     }); 

                     //  //load specificDetails
                     //  var commonData = JSON.parse(
                     //    JSON.stringify(commonDataForFacility)
                     //  );
                     //  commonData[0].spec_det_datavalue1 =
                     //    editData.trn_venue_name;
                     //  commonData[1].spec_det_datavalue1 =
                     //    editData.trn_venue_address;
                     //  commonData[1].spec_det_datavalue2 =
                     //    editData.trn_venue_location;
                     //  commonData[2].spec_det_datavalue1 = "landmark";
                     //  commonData[3].spec_det_datavalue1 =
                     //    editData.trn_venue_desc;
                     //  // this.setState({commonArray});
                   }
                 }

                 componentWillUnmount() {
                   BackHandler.removeEventListener(
                     "hardwareBackPress",
                     this.handleBackButton
                   );
                 }

                 handleBackButton = () => {
                   if (!this.state.loading) {
                     // Actions.pop();
                   } else {
                     return true;
                   }
                   // ToastAndroid.show('Back button is disabled', ToastAndroid.SHORT);
                 };

                 getmoredetails = (data) => {
                   var availability = this.state.availability;
                   availability.moredetails = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_moredetails = data;
                   this.setState({ venueform });
                 };

                 getBusinessForm = (data, totalState) => {
                   this.setState({
                     availabilityState: totalState,
                   });
                   var availability = this.state.availability;
                   availability.businessForm = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.businessForm = data;
                   this.setState({ venueform });
                 };

                 getSlots = (data) => {
                   var availability = this.state.availability;
                   availability.slots = data.splitData;
                   availability.businessForm = data.businessForm;
                   availability.paxContent = data.paxContent;
                   this.setState({ availability });

                   var venueform = this.state.venueform;
                   venueform.availability.slots = data.splitData;
                   venueform.availability.businessForm = data.businessForm;
                   venueform.availability.paxContent = data.paxContent;
                   this.setState({ venueform });

                   const {
                     from,
                     to,
                     minTime,
                     maxTime,
                     availableFrom,
                     availableTo,
                     selectedType,
                     checkboxes,
                     activeType,
                   } = this.state.availability.businessForm;

                   var obj = {
                     activeobj: this.state.availability.type,
                     hourobj: null,
                     businessform: this.state.availability.businessForm,
                     SplitSlots: data.splitData,
                     selectedSlots: data.selectedSlots,
                     slotType: selectedType,
                     hourSlots: data.allSlots,
                     moreDetails: data.paxContent,
                   };

                   this.setState(
                     {
                       availabilityState: data.availabilityState,
                       splitData: data.splitData,
                     },
                     () => {
                       this.gotoNext();
                     }
                   );
                 };

                 renderActiveTab() {
                   return <View style={styles.activeparent}></View>;
                 }

                 //change label text
                 changeLabelText = (data) => {
                   this.setState({ venuetypetext: data.labeltext });
                 };

                 //change header text
                 changeHeaderText = (data) => {
                   this.setState({ venueheadertext: data.headertext });
                   this.setState({
                     showSkip: data.showskip ? data.showskip : null,
                   });
                 };

                 //send facility data
                 sendfaciltiydata = (data, data1, error, update) => {
                   console.log("init");
                   this.setState({ traineedata: data });
                   this.setState({ commonArray: data1 });
                   var venueform = this.state.venueform;

                   venueform.trainer.userId = this.state.loginDetails.user_id;

                   venueform.trainer.subSpecificId =
                     data1[0].spec_det_datavalue1;

                   venueform.trainer.yearOfExp = data1[1].spec_det_datavalue1;
                   venueform.trainer.monthOfExp = data1[2].spec_det_datavalue1;
                   venueform.trainer.certificationContent =
                     data1[3].spec_det_datavalue1;
                   venueform.trainer.achivementContent =
                     data1[4].spec_det_datavalue1;
                   venueform.trainer.address = data1[5].spec_det_datavalue1;
                   venueform.trainer.location = data1[5].spec_det_datavalue2;
                   venueform.trainer.landMark = data1[6].spec_det_datavalue1;
                   venueform.trainer.area = data1[7].spec_det_datavalue1;

                   if (error != null) {
                     var validations = this.state.validations;
                     validations[2].state = error;
                     this.setState({ validations });
                   }
                   venueform.keytoupdate = ["TRAINER_BASIC"]; //autosave
                   venueform.trainer.lastActivityTab = 2;
                   var venueformData = this.state.venueformData;

                   venueformData.set(
                     "trainer",
                     JSON.stringify(venueform.trainer)
                   );

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform }, () =>
                     console.log("venuedetails", this.state.venueform)
                   );
                   //  this.setState({ venueformData });
                 };

                 //send venuetypedata
                 sendtraineetypedata = (data, update) => {
                   this.setState({ venuetypedata: data });
                   var validations = this.state.validations;
                   validations[0].state = true;
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.trainer.categoryId = data.CategoryId;
                   this.setState({ venueform }, () => {
                     if (!update) this.gotoNext();
                   });
                 }; 

                 sendtraineemodetypedata = (data, update) => {
                   this.setState({ traineemodedata: data });
                   var validations = this.state.validations;
                   validations[0].state = true;
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   var venueformData = this.state.venueformData;
                   venueform.trainer.courseModeId = data.ModeOfTrainingId; 

                    venueformData.set(
                      "trainer",
                      JSON.stringify(venueform.trainer)
                    );

                   console.log("data.ModeOfTrainingId", data.ModeOfTrainingId);
                   this.setState({ venueform,venueformData },()=>console.log('vnue',venueform));
                   
                  };

                 //send trainee specific
                 sendfacilitytypedata = (data, update) => {
                   this.setState({ facilitytypedata: data });
                   var venueform = this.state.venueform;
                   var validations = this.state.validations;
                   validations[1].state = true;
                   this.setState({ validations });
                   venueform.trainer.specificId = data.SpecificId;
                   this.setState({ venueform }, () => {
                     if (!update) this.validateGoToNext();
                   });
                 };

                 //load facility
                 loadFacility = () => {
                   if (this.state.facilityIndex == 0) {
                     return (
                       <TraineeCategory
                         venuetypedata={this.state.venuetypedata}
                         facilitytypeData={this.state.facilitytypedata}
                         sendfacilitytypedata={this.sendfacilitytypedata}
                         headertext={this.changeHeaderText}
                         labeltext={this.changeLabelText}
                       />
                     );
                   } else {
                     return (
                       <TraineeDetails
                         commonArray={this.state.commonArray}
                         venuetypedata={this.state.venuetypedata}
                         facilityData={this.state.facilitytypedata}
                         sendfaciltiydata={this.sendfaciltiydata}
                         headertext={this.changeHeaderText}
                         labeltext={this.changeLabelText}
                         onSubmit={this.venueImages}
                         localimages={this.state.venueform.formArray}
                         mainformData={this.state.venueformData}
                         edit={this.state.edit}
                       />
                     );
                   }
                 };

                 //gotoback
                 gotoback = () => {
                   if (this.state.currentIndex != 0) {
                     if (this.state.activeparent == "tab2") {
                       if (this.state.facilityIndex == 1) {
                         this.setState({ facilityIndex: 0 });
                         this.setState({ facitlityactive: 1 });
                       } else {
                         var currentitem =
                           parseInt(this.state.currentIndex) - 1;
                         var currentitemname = this.state.list[currentitem]
                           .name;
                         this.setState({ activeparent: currentitemname });
                         this.setState({ currentIndex: currentitem });
                       }
                     } else {
                       var currentitem = parseInt(this.state.currentIndex) - 1;
                       var currentitemname = this.state.list[currentitem].name;
                       this.setState({ activeparent: currentitemname });
                       this.setState({ currentIndex: currentitem });
                       this.setState({ facilityIndex: 1 });
                       this.setState({ facitlityactive: true });
                     }
                   }
                 };

                 //validate to steps
                 validateGoToNext = () => {
                   switch (this.state.activeparent) {
                     case "tab1": {
                       const current = this.state.venuetypedata;
                       if (current != null) this.gotoNext();
                       else Toast.show("Please Fill the fields", Toast.LONG);

                       break;
                     }
                     case "tab2": {
                       //  alert(JSON.stringify(this.state.facilitytypedata));
                       console.log("tab2presse", this.state.facilityIndex);
                       if (this.state.facilityIndex == 0) {
                         const current = this.state.facilitytypedata;
                          console.log(this.state.facilitytypedata);
                         if (current != null && current.SpecificId > 0)
                           this.gotoNext();
                         else Toast.show("Please Fill the fields", Toast.LONG);
                       } else if (this.state.facilityIndex == 1) {
                         var commonArray = this.state.commonArray;
                         for (var i in commonArray) {
                           var errorcheck = ValidationLibrary.checkValidation(
                             commonArray[i].spec_det_datavalue1,
                             commonArray[i].validation
                           );
                           commonArray[i].error = !errorcheck.state;
                           commonArray[i].errormsg = errorcheck.msg;
                         }
                         var errordata = commonArray.filter(
                           (obj) => obj.error == true
                         );
                         if (errordata.length != 0) {
                           Toast.show("Please fill the fields", Toast.LONG);
                         } else {
                           this.gotoNext();
                         }
                       }
                       break;
                     }
                     case "tab3": {
                       break;
                     }
                     case "tab4": {
                       const current = this.state.courseDetailData;
                       console.log("tab4_called", current);
                       if (current == null)
                         Toast.show("Please Fill the fields", Toast.LONG);
                       else {
                         if (this.state.autosave == true) {
                           var venueform = this.state.venueform;
                           var venueformData = this.state.venueformData;
                           venueform.trainer.userId = this.state.loginDetails.user_id;
                           venueform.trainer.lastActivityTab = 3;
                           this.setState({ venueform });
                           venueformData.set(
                             "trainer",
                             JSON.stringify(venueform.trainer)
                           );
                           venueformData.set(
                             "keytoupdate",
                             JSON.stringify(venueform.keytoupdate)
                           );
                           //  this.autosaveData(venueformData);
                         }
                         this.gotoNext();
                       }
                       break;
                     }
                     case "tab5": {
                       const current = this.state.courseDetail1Data;
                       console.log("tab5", current);
                       if (current != null) {
                         if (this.state.autosave == true) {
                           var venueform = this.state.venueform;
                           var venueformData = this.state.venueformData;
                           venueform.trainer.lastActivityTab = 4;
                           this.setState({ venueform });
                           venueformData.set(
                             "trainer",
                             JSON.stringify(venueform.trainer)
                           );
                           venueformData.set(
                             "keytoupdate",
                             JSON.stringify(venueform.keytoupdate)
                           );
                           this.autosaveData(venueformData);
                         }
                         this.gotoNext();
                       } else Toast.show("Please Fill the fields", Toast.LONG);
                       break;
                     }
                     case "tab6": {
                       const current = this.state.venueprice;
                       console.log("tab6", current);
                       if (current != null) this.gotoNext();
                       else Toast.show("Please Fill the fields", Toast.LONG);
                       break;
                     }
                     case "tab7": {
                       const current = this.state.chooseVenueState;
                       console.log("tab6", current);
                       if (current != null) {
                         if (this.state.autosave == true) {
                           var venueform = this.state.venueform;
                           var venueformData = this.state.venueformData;
                           venueform.trainer.lastActivityTab = 6;
                           this.setState({ venueform });
                           venueformData.set(
                             "trainer",
                             JSON.stringify(venueform.trainer)
                           );
                           venueformData.set(
                             "keytoupdate",
                             JSON.stringify(venueform.keytoupdate)
                           );
                           this.autosaveData(venueformData);
                         }
                         this.gotoNext();
                       } else Toast.show("Please Fill the fields", Toast.LONG);
                       break;
                     }
                     case "tab8": {
                       Toast.show("Please take a action", Toast.LONG);
                       break;
                     }
                     default: {
                       alert(result);
                       break;
                     }
                   }
                 };

                 //course detail submit
                 courseDetailSubmit = (data, data1, error) => {
                   this.setState({ courseDetailData: data });
                   this.setState({ courseDetailState: data1 });

                   var venueform = this.state.venueform;
                   venueform.course[0].trainingLevelId = data.trainingLevelId;
                   venueform.course[0].languageId = data.languageId;
                   venueform.course[0].courseTitle = data.courseTitle;
                   venueform.course[0].batchData[0].batchDurationHours =
                     data.batchDurationHours;
                   venueform.course[0].batchData[0].batchStartDate =
                     data.batchStartDate;
                   venueform.course[0].batchData[0].batchEndDate =
                     data.batchEndDate;

                   if (error != null) {
                     var validations = this.state.validations;
                     validations[2].state = error;
                     this.setState({ validations });
                   }

                   venueform.keytoupdate = [
                     "TRAINER_BASIC",
                     "COURSE_DETAILS",
                     "BATCH_DATA",
                   ];

                   venueform.trainer.lastActivityTab = 3;
                   var venueformData = this.state.venueformData;

                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform }, () => {
                     console.log("coursedetail updated", venueform.course[0]);
                   });
                 };

                 //coursedetail 1 on change
                 courseDetail1Submit = (data, data1, error) => {
                   console.log("coursedetail1", data);
                   this.setState({ courseDetail1Data: data });
                   this.setState({ courseDetail1State: data1 });

                   var venueform = this.state.venueform;
                   venueform.course[0].courseDesc = data.courseDesc;
                   venueform.course[0].courseOutline = data.courseOutline;
                   venueform.course[0].courseFAQ = data.courseFAQ;

                   if (error != null) {
                     var validations = this.state.validations;
                     validations[2].state = error;
                     this.setState({ validations });
                   }

                   venueform.keytoupdate = [
                     "TRAINER_BASIC",
                     "COURSE_DETAILS",
                     "BATCH_DATA",
                   ];

                   venueform.trainer.lastActivityTab = 4;
                   var venueformData = this.state.venueformData;

                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform }, () => {
                     console.log("coursedetail updated", venueform.course[0]);
                   });
                 };

                 //autosave function
                 autosaveData = (data, upload, newimagesData) => {
                   // alert(JSON.stringify(newimagesData));
                   // return;
                   console.log("autosave_init", data);
                   var self = this;
                   const field = data.getParts();
                   // alert(JSON.stringify(field));
                   // return;
                   var venueformData = this.state.venueformData;
                   if (upload) {
                     this.setState({ loading: true });
                   }
                   LoadAPIPost.LoadApi(
                     data,
                     links.APIURL + "TrainerAutoSave",
                     upload,
                     function (err, success) {
                       if (upload) {
                         venueformData.set("formArray", null);
                         self.setState({ loading: false });
                       }
                       if (err) {
                         Toast.show("serverError", Toast.LONG);
                       } else {
                         var respjson = JSON.parse(success);
                         console.log("autosave_result", respjson);
                         var venueform = self.state.venueform;
                         if (respjson.status == 0) {
                           //upload image modal overlay
                           if (newimagesData && upload) {
                             var newmodifiedimages = [];
                             if (Array.isArray(newimagesData) == true) {
                               newimagesData.push({
                                 name: respjson.existImage,
                               });
                               newmodifiedimages = newimagesData;
                             } else {
                               newmodifiedimages = newimagesData;
                             }
                             venueformData.set(
                               "existImages",
                               JSON.stringify(newmodifiedimages)
                             );
                             // alert(respjson.existImage);
                           }

                           if (respjson.trainerId) {
                             console.log("resp", respjson);

                             venueform.trainer.trainerId = respjson.trainerId;
                             self.setState({ venueform });
                             venueformData.set(
                               "trainer",
                               JSON.stringify(venueform.trainer)
                             );
                           }
                         }
                       }
                     },
                     function (progress) {
                       if (upload) {
                         self.setState({ progresscount: progress });
                       }
                     }
                   );
                 };

                 //trainee course detail1 submit

                 //go to next step
                 gotoNext = () => {
                   console.log("this", this.state.currentIndex);
                   console.log("tablist", this.state.list);
                   if (this.state.currentIndex != this.state.list.length - 1) {
                     if (this.state.activeparent == "tab2") {
                       if (this.state.facilityIndex == 0) {
                         this.setState({ facilityIndex: 1 });
                         this.setState({ facitlityactive: 1 });
                       } else {
                         var currentitem =
                           parseInt(this.state.currentIndex) + 1;

                         // alert(this.state.list[currentitem].name)
                         var currentitemname = this.state.list[currentitem]
                           .name;
                         // alert(currentitem);
                         this.setState({ activeparent: currentitemname });
                         this.setState({ currentIndex: currentitem });
                       }
                     } else {
                       var currentitem = parseInt(this.state.currentIndex) + 1;

                       // alert(this.state.list[currentitem].name)
                       var currentitemname = this.state.list[currentitem].name;
                       // alert(currentitem);
                       this.setState({ activeparent: currentitemname });
                       this.setState({ currentIndex: currentitem });
                       this.setState({ facilityIndex: 0 });
                       this.setState({ facitlityactive: true });
                     }
                   }
                 };

                 availablitydays = (data) => {
                   var availability = this.state.availability;
                   availability.days = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_days = data ? data.value : null;
                   this.setState({ venueform });
                 };

                 availcircleData = (data) => {
                   var availability = this.state.availability;
                   availability.type = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_avail_type = data.id;
                   this.setState({ venueform });
                 };

                 fromtodays = (data) => {
                   var availability = this.state.availability;
                   availability.from = data.from;
                   availability.to = data.to;
                   this.setState({ availability });
                   if (data.from == "" || data.to == "") {
                     var validations = this.state.validations;
                     validations[3].state = false;
                   } else {
                     var validations = this.state.validations;
                     validations[3].state = true;
                   }
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_avail_frm = availability.from;
                   venueform.availability.venue_avail_to = availability.to;
                   this.setState({ venueform });
                 };

                 AmenetiesData = (data) => {
                   this.setState({ ameneties: data });
                   var amenitiesData =
                     data.length > 0 &&
                     data.filter((obj) => obj.circleCount > 0);
                   var amentiesArray = [];
                   for (var i in amenitiesData) {
                     if (
                       amenitiesData[i].hasOwnProperty("activeArray") == true
                     ) {
                       amentiesArray = amentiesArray.concat(
                         amenitiesData[i].activeArray
                       );
                     }
                   }
                   console.log("amenity", amentiesArray);
                   var venueform = this.state.venueform;
                   venueform.ameneties = amentiesArray;
                   this.setState({ venueform }, () => {
                     this.gotoNext();
                   });
                 };

                 saveTags = (data, filterdata) => {
                   // alert(JSON.stringify(filterdata));
                   var venTags = this.state.venTags;
                   venTags = data;
                   this.setState({ venTags });
                   var tagarray = [];
                   for (var i = 0; i < filterdata.length; i++) {
                     var objdata = {
                       tag_cat_id: filterdata[i].tag_cat_id,
                       tag_details: [],
                     };
                     for (var y = 0; y < filterdata[i].data.length; y++) {
                       objdata.tag_details.push(filterdata[i].data[y].tag_name);
                     }
                     tagarray.push(objdata);
                   }
                   // alert(JSON.stringify(tagarray));
                   var venueform = this.state.venueform;
                   venueform.tagdetails = tagarray;
                   this.setState({ tagarray });
                 };

                 updateCloseModal = (data) => {
                   var availabilityState = this.state;
                   availabilityState.isAvailable = data.isAvailable;
                   availabilityState.showBusinessForm = data.showBusinessForm;
                   availabilityState.isSlotsBooking = data.isSlotsBooking;
                   this.setState({ availabilityState });
                 };

                 sendPriceData = (data) => {
                   this.gotoNext();
                   var venueprice = this.state.venueprice;
                   venueprice = data;
                   this.setState({ venueprice });
                   // alert(JSON.stringify(data));
                   if (data.amt == 0 || data.amt == "") {
                     var validations = this.state.validations;
                     validations[4].state = false;
                   } else {
                     var validations = this.state.validations;
                     validations[4].state = true;
                   }
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.pricedetails.venue_price_amt = data.amt;
                   venueform.pricedetails.venue_price_type = data.activeId;
                   venueform.pricedetails.venue_price_currency = data.days;
                   venueform.pricedetails.venue_price_name = data.type;
                   this.setState({ venueform });
                 }; 


                 
                 autosaveData1 = (data, upload, newimagesData) => {
                 
                  
                    var self = this;
                   var venueform = self.state.venueform;
                   var venueformData = self.state.venueformData;
                   if (upload) {
                     this.setState({ loading: true });
                   }     

                    // venueformData.set("formArray", null);

                   console.log("final", self.state.venueformData);


                   if(venueform.trainer.newimage!=true){
                      venueformData.set("formArray", null);
                   }

                   
                  
                   LoadAPIPost.LoadApi(
                     venueformData,
                     links.APIURL +
                       (self.state.edit && !self.state.autosave
                         ? "editTrainerDetailsNew/"
                         : "addTrainerDetailsNew/"),
                     upload,
                     function (err, success) {
                       console.log("inside");
                       if (upload) {
                         venueformData.set("formArray", null);
                         self.setState({ loading: false });
                       }
                       if (err) {
                         Toast.show("serverError", Toast.LONG);
                       } else {
                         var respjson = JSON.parse(success);

                         console.log("autosave_result", respjson);
                         var venueform = self.state.venueform;
                         if (respjson.status == 0) {
                           self.setState({ loading: null });
                           console.log("responfsd", respjson);
                           var showtext = self.state.edit ==true ? "Edited" : "Added";

                           if (self.state.edit){
   Toast.show(
                               "Trainer " + showtext + " Successfully",
                               Toast.LONG
                             );
                           setTimeout(() => {
                             Actions.reset("home1");
                           }, 100); 
                           }else
                             self.setState({ activeparent: "success" });
                          



                         } else {
                           Toast.show(respjson.data[0], Toast.LONG);
                         }
                       }
                     },
                     function (progress) {
                       if (upload) {
                         self.setState({ progresscount: progress });
                       }
                     }
                   );
                 };




                 venueImages = (data, data1, data2) => {
                   var venueformData = this.state.venueformData;
                   var venueform = this.state.venueform;

                   if (data.courseState != null) {
                     this.setState({ courseState: data.courseState });
                   }
                   
                   venueform.trainer.lastActivityTab = 1;
                  
                   venueform.formArray = data.localimages;

                   const fieldforuploads = data.formimagearray
                     .getParts()
                     .find((item) => item.fieldName === "formArray");
                   var arrayofloopimages = data.localimages.filter(
                     (obj) => obj.exist
                   ); 
                   if (fieldforuploads) {
                     
                     console.log("filed", fieldforuploads);
                      venueform.trainer.newimage = true;
                      //  venueform.trainer.trainerImage = '';
                     venueformData.set("formArray", fieldforuploads);
                     //  alert(JSON.stringify());
                     //  arrayofloopimages.push({name:data.localimages[data.localimages.length-1].fileName})
                     //  if (data.localimages.length > 0) {
                     //    arrayofloopimages.push({
                     //      name:
                     //        data.localimages[data.localimages.length - 1]
                     //          .fileName,
                     //    });
                     //  }
                   } 

                   if(data.delete){
                       venueformData.set("formArray", null);
                       venueform.trainer.trainerImage = "";
                   }

                   var existingnewimages =
                     arrayofloopimages.length > 0
                       ? arrayofloopimages.map((obj) => {
                           // var objnew=obj;
                           var newobj = { name: obj.name };
                           return newobj;
                         })
                       : [];


                   venueformData.set(
                     "trainer",
                     JSON.stringify(venueform.trainer)
                   );
                  
                   venueformData.set(
                     "existImages",
                     JSON.stringify(existingnewimages)
                   );  
                   

                   venueform.trainer.existingnewimages = existingnewimages;
                   venueform.course[0].existImages = existingnewimages;

                   // venueformData
                   this.setState({ venueform });
                   this.setState({ venueformData });

                   console.log("courseprices", venueformData);
                   if (this.state.autosave == true) {
                     //  this.autosaveData(venueformData, "upload", existingnewimages);
                     console.log("existss", existingnewimages);
                   } else if (this.state.edit) {
                     console.log("if");
                    //  this.autosaveData1(
                    //    venueformData,
                    //    "upload",
                    //    existingnewimages
                    //  );
                   }

                   if(!data.delete)
                   this.validateGoToNext();
                 };

               
                 closeSuccessModal = (data) => {
                   this.setState({ activeparent: null });
                   if (data) {
                     Actions.venuepage({ raiselogin: true });
                   } else {
                     Actions.home();
                   }
                 };
                 errorData = (data) => {
                   Toast.show(data, Toast.LONG);
                 };
                 loadedit = () => {
                   this.setState({ currentIndex: 0 });
                   this.setState({ activeparent: "tab1" });
                   // this.goToRight();
                 };

                 //tab7 on change text
                 sendPurposes = (data, data1) => {
                   this.setState({
                     chooseVenueData: data,
                     chooseVenueState: data1,
                   });

                   var venueform = this.state.venueform;

                   venueform.course[0].venueDetails[0] = data;

                   var venueformData = this.state.venueformData;
                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );

                   venueform.keytoupdate = [
                     "TRAINER_BASIC",
                     "COURSE_DETAILS",
                     "BATCH_DATA",
                     "TRAINING_VENUE",
                   ];

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform }, () => {
                     console.log("sendpurposes", venueform.course);
                   });
                 };

                 courseNext = (data) => {
                   console.log("coursedetailnextfired", data);
                   this.setState({
                     courseDetail: data,
                   });
                 };

                 render() {
                   return (
                     <Container>
                       <Root>
                         {this.state.loading && (
                           <View
                             style={{
                               width: "100%",
                               zIndex: 9999,
                               flex: 1,
                               justifyContent: "center",
                               alignItems: "center",
                               position: "absolute",
                               height: "100%",
                               backgroundColor: "rgba(0,0,0,0.8)",
                               top: 0,
                             }}
                           >
                             <ActivityIndicator
                               size="large"
                               color={color.orange}
                             />
                             <Text style={{ color: color.white }}>
                               Please Wait ....
                             </Text>
                           </View>
                         )}
                         <SubHeaderComp bgcolor={color.orange}>
                           <View style={styles.subheading}>
                             <Text style={styles.subheadingtext}>
                               Join as Trainer
                             </Text>
                           </View>
                         </SubHeaderComp>
                         <Content>
                           {this.state.activeparent != "success" && (
                             <View>
                               <DynamicPagination
                                 dots={3}
                                 activeid={this.state.currentIndex}
                                 changePagination={(data) =>
                                   this.changeTab(data)
                                 }
                               />
                               <View
                                 style={{
                                   flex: 1,
                                   flexDirection: "row",
                                   alignItems: "center",
                                   padding: 5,
                                   borderBottomWidth: 0.5,
                                   borderColor: color.black1,
                                   justifyContent: "space-between",
                                 }}
                               >
                                 <View>
                                   <Text
                                     style={{
                                       fontSize: hp("1.7%"),
                                       color: color.blue,
                                       fontWeight: "bold",
                                     }}
                                   >
                                     {this.state.venueheadertext}
                                   </Text>
                                 </View>
                                 {this.state.showSkip && (
                                   <TouchableOpacity
                                     onPress={() => this.gotoNext()}
                                   >
                                     <Text
                                       style={{
                                         fontSize: hp("1.7%"),
                                         color: color.blue,
                                       }}
                                     >
                                       Skip
                                     </Text>
                                   </TouchableOpacity>
                                 )}
                               </View>
                               <View
                                 style={{
                                   flex: 1,
                                   flexDirection: "row",
                                   alignItems: "center",
                                   padding: 5,
                                   borderBottomWidth: 0.5,
                                   borderColor: color.black1,
                                 }}
                               >
                                 <View style={{ flex: 0.1 }}>
                                   <TouchableOpacity onPress={this.gotoback}>
                                     <Icon
                                       style={[
                                         styles.arrownavicon,
                                         {
                                           color:
                                             this.state.facitlityactive &&
                                             this.state.facilityIndex == 1
                                               ? color.orange
                                               : null,
                                         },
                                       ]}
                                       type="FontAwesome"
                                       name="angle-left"
                                     />
                                   </TouchableOpacity>
                                 </View>
                                 <View
                                   style={{
                                     flex: 0.8,
                                     flexDirection: "row",
                                     alignItems: "center",
                                     justifyContent: "center",
                                   }}
                                 >
                                   <Text
                                     numberOfLines={2}
                                     style={{
                                       fontSize: hp("2%"),
                                       textAlign: "center",
                                     }}
                                   >
                                     {this.state.venuetypetext}
                                   </Text>
                                 </View>
                                 <View style={{ flex: 0.1 }}>
                                   <TouchableOpacity
                                     onPress={() => this.validateGoToNext()}
                                   >
                                     <Icon
                                       style={[
                                         styles.arrownavicon,
                                         {
                                           color:
                                             this.state.facitlityactive &&
                                             this.state.facilityIndex == 0
                                               ? color.orange
                                               : null,
                                         },
                                       ]}
                                       type="FontAwesome"
                                       name="angle-right"
                                     />
                                   </TouchableOpacity>
                                 </View>
                               </View>
                             </View>
                           )}
                           <View>
                             {this.state.activeparent == "tab1" && (
                               <TraineeType
                                 loginDetails={this.state.loginDetails}
                                 venuedetails={this.state.venuetypedata}
                                 sendvenuetypedata={this.sendtraineetypedata}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                               />
                             )}

                             {this.state.activeparent == "tab2" &&
                               this.loadFacility()}

                             {this.state.activeparent == "tab3" && (
                               <TraineeMode
                                 loginDetails={this.state.loginDetails}
                                 venuedetails={this.state.traineemodedata}
                                 sendvenuetypedata={
                                   this.sendtraineemodetypedata
                                 }
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 next={this.gotoNext}
                                 submitVenuData={this.autosaveData1}
                               />
                             )}   

                             {this.state.activeparent == "success" && (
                               <TraineeSuccess
                                 commonArray={this.state.commonArray}
                                 loginDetails={this.state.loginDetails}
                                 venueform={this.state.venueform}
                                 closemodal={(data) =>
                                   this.closeSuccessModal(data)
                                 }
                                 edit={this.state.edit}
                               />
                             )}
                           </View>
                         </Content>
                       </Root>
                     </Container>
                   );
                 }
               }

const styles = {
  active: {
    backgroundColor: "#e2e2e2",
    borderRightWidth: 0.5,
    borderColor: "#999999",
    zIndex: 99999
  },
  subheading: {
    padding: 12
  },
  subheadingtext: {
    fontSize: hp("2%"),
    color: color.white
  },
  imageparent: {
    width: hp("8%"),
    height: hp("7%"),
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: color.ash2
  },
  imageTab: {
    width: hp("4%"),
    height: hp("4%")
  },
  activeparent: {
    position: "absolute",
    right: -hp("1.4%"),
    top: hp("2%"),
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999999
  },
  bordertriangle: {
    position: "absolute",
    left: 0,
    top: hp("0.23%")
  },
  arrowbox: {
    width: hp("3%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: color.blueactive
  },
  arrowboxicon: {
    color: color.black1,
    fontSize: hp("3.2%")
  },
  arrownavicon: {
    fontSize: hp("4%"),
    textAlign: "center",
    color: color.ash2
  },
  activetextorange: {
    color: color.orange
  }
};
