'use strict';

import React, { Component } from 'react';
import Line from '../components/Line';
import { Platform, StyleSheet, Text, View, Alert, StatusBar, Modal, Image, TouchableHighlight, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Icon, Button, Subtitle, Header, Content, Body, Right, Title, Item, Input, Label, Textarea, Form } from 'native-base';
import ValidationLibrary from '../Helpers/validationfunction';
import DynamicForm from '../components/DynamicForm';
import AsyncStorage from "@react-native-community/async-storage";
import color from '../Helpers/color';
import AddressComp from '../components/addressComp';
class AddNewVenue extends Component {
  constructor(props) {
    super(props);

    this.handleLayoutMainChange = this.handleLayoutMainChange.bind(this);
    this.state = {
      commonArray: [
        {
          spec_det_id: "idname",
          venue_spec_id: 1,
          spec_det_name: "Name",
          spec_det_sortorder: 0,
          spec_det_datatype1: "text",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }, { name: "minLength", params: 30 }],
          error: null,
          errormsg: ""
        },
        {
          spec_det_id: "idname2",
          venue_spec_id: 1,
          spec_det_name: "Address",
          spec_det_sortorder: 0,
          spec_det_datatype1: "textarea",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }],
          error: null,
          errormsg: ""
        },
        {
          spec_det_id: "idname3",
          venue_spec_id: 1,
          spec_det_name: "Landmark",
          spec_det_sortorder: 0,
          spec_det_datatype1: "btn_address",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [
            { name: "required" },
            { name: "minLength", params: 300 }
          ],
          error: null,
          errormsg: ""
        },
        {
          spec_det_id: "idname4",
          venue_spec_id: 1,
          spec_det_name: "Description",
          spec_det_sortorder: 0,
          spec_det_datatype1: "textarea",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [
            { name: "required" },
            { name: "minLength", params: 1000 }
          ],
          error: null,
          errormsg: ""
        }
      ],
      facilityData: null,
      addresscomp: "",
      modalstate: false,
      location:{lat:'',lng:''},
      validations: {
        roomname: {
          error: null,
          mandatory: true,
          errormsg: "",
          validations: [{ name: "required", status: false }]
        },
        seats: {
          error: null,
          errormsg: "",
          mandatory: true,
          validations: [{ name: "required", status: false }]
        },
        floor: {
          error: null,
          errormsg: "",
          mandatory: false,
          validations: []
        },
        landmark: {
          error: null,
          errormsg: "",
          mandatory: false,
          validations: []
        },
        mobile: {
          error: null,
          errormsg: "",
          mandatory: true,
          validations: [
            { name: "required", status: false },
            { name: "mobile", status: false }
          ]
        },
        mail: {
          error: null,
          errormsg: "",
          mandatory: true,
          validations: [
            { name: "required", status: false },
            { name: "email", status: false }
          ]
        },
        address: {
          error: null,
          errormsg: "",
          mandatory: true,
          validations: [{ name: "required", status: false }]
        },
        
      }
    };
  } 

  continueAsTrainer=()=>{
    this.props.closeNewVenue()
  }

  continueAsVenue=()=>{
 this.props.closeNewVenue()
  }

  handleLayoutMainChange = () => {
    this.mainheight.measure((fx, fy, width, height, px, py) => {
      // this.setState({dropdownX})
      this.setState({ newmainheight: height });
      // var dropdown=this.state.dropdown;
      // dropdown.x=px;
      // dropdown.y=py;
      // dropdown.height=height;
      // this.setState({dropdown});
    });
  };

  componentWillMount() { 
   this.loadcurrentLocation()
  } 

  
async loadcurrentLocation(){
    
   const manuallocation = JSON.parse(
     await AsyncStorage.getItem("chosenlocation")
   );
   let lat = manuallocation != null ? manuallocation.lat : null;
   let lng = manuallocation != null ? manuallocation.lng : null;
   this.setState({
     location: {
       latitude: position.coords.latitude,
       longitude: position.coords.longitude
     }
   });
        

}


  componentWillReceiveProps(props) {}
  getAddress = () => {
    this.setState({ modalstate: true });
  };
  receiveAddress = (data, location) => {
    // alert(JSON.stringify(location));
    // var facilityData = this.state.facilityData;
    // facilityData.address = data.usertxt;
    // this.setState({ facilityData });
    this.setState({ modalstate: false });
    var commonArray = this.state.commonArray;
    commonArray[1].spec_det_datavalue1 = data.usertxt;
    commonArray[1].spec_det_datavalue2 = data.location
      ? data.location[Object.keys(data.location)[0]].toString() +
        "," +
        data.location[Object.keys(data.location)[1]].toString()
      : null;
    this.setState({ commonArray });
    this.props.sendfaciltiydata(this.state.facilityData, commonArray);
    // this.changeText(data.usertxt,'address','spec_det_datavalue1')
  };
  changeText = (data, data2, dataobj, key) => {
    var facilityData = this.state.facilityData;
    var commonArray = this.state.commonArray;
    if (data2) {
      if (data2 == "address") {
        this.getAddress();
      }
      return;
    } else {
      // alert("nothing");
    }
    if (Number.isInteger(dataobj.spec_det_id)) {
      var findIndex = facilityData.specDetails.findIndex(
        obj => obj.spec_det_id == dataobj.spec_det_id
      );
      if (findIndex != -1) {
        facilityData.specDetails[findIndex].spec_det_datavalue1 = data;
        this.setState({ facilityData });
      }
    } else {
      var findIndex = commonArray.findIndex(
        obj => obj.spec_det_id == dataobj.spec_det_id
      );
      if (findIndex != -1) {
        var errorcheck = ValidationLibrary.checkValidation(
          data,
          commonArray[findIndex].validation
        );
        console.log(errorcheck);
        commonArray[findIndex].spec_det_datavalue1 = data;
        commonArray[findIndex].error = !errorcheck.state;
        commonArray[findIndex].errormsg = errorcheck.msg;
        this.setState({ commonArray }, function() {
          this.checkValidations();
        });
      }
    }
    this.props.sendfaciltiydata(facilityData, commonArray);
    // var errormsg = ValidationLibrary.checkValidation(data, this.state.validations[key].validations);
    // var validations = this.state.validations;
    // validations[key].error = !errormsg.state;
    // validations[key].errormsg = errormsg.msg;
    // this.setState({ validations });
    // var mandatorylength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true).length;
    // var noerrorlength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true&&validations[obj].error == false).length;
    // // alert(JSON.stringify(noerrorlength));
    // var noerror = false;
    // if (mandatorylength == noerrorlength) {
    //     noerror = true;
    // }
    // this.props.sendfaciltiydata({ key: key, value: data, dbkey: dbkey, validations: validations, noerror: noerror });
    // var facilityData = this.state.facilityData;
    // facilityData[key] = data;
  };
  checkValidations = () => {
    var commonArray = this.state.commonArray;
    for (var i in commonArray) {
      var errorcheck = ValidationLibrary.checkValidation(
        commonArray[i].spec_det_datavalue1,
        commonArray[i].validation
      );
      commonArray[i].error = !errorcheck.state;
      commonArray[i].errormsg = errorcheck.msg;
    }
    var errordata = commonArray.filter(obj => obj.error == true);
    if (errordata.length != 0) {
      // alert("error");
      this.props.sendfaciltiydata(
        this.state.facilityData,
        this.state.commonArray,
        false
      );
    } else {
      // alert("noerror");
      this.props.sendfaciltiydata(
        this.state.facilityData,
        this.state.commonArray,
        true
      );
    }

    // this.setState({commonArray});
    // return errordata;
  };
  render() {
    // alert(JSON.stringify(this.state.facilityData));
    return (
      <Content>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => this.setState({ dropdown: null, dropdownstate: null })}
        >
          <View
            onLayout={event => {
              this.handleLayoutMainChange(event);
            }}
            ref={view => {
              this.mainheight = view;
            }}
          >
            <View style={{ paddingHorizontal: 30, paddingVertical: 10 }}>
              {this.props.modaltitle && (
                <View>
                  <Text
                    style={{
                      color: color.black1,
                      fontSize: 25,
                      fontStyle: "bold",
                      paddingBottom: 10
                    }}
                  >
                    {this.props.modaltitle}
                  </Text>
                  <Line />
                </View>
              )}
              {this.props.modaltitle && (
                <Text style={{ color: color.black1, fontSize: 16 }}>
                  {this.props.modalsubtitle}
                </Text>
              )}
            </View>

            <View
              style={{
                paddingLeft: 30,
                paddingRight: 30,
                paddingBottom: 30,
                marginTop: 18
              }}
            > 

              <AddressComp
                receiveAddress={this.receiveAddress}
                modalstate={this.state.modalstate}
                location={this.state.location}
              />

              <DynamicForm
                changeText={this.changeText}
                commonArray={this.state.commonArray}
              />
            </View>

            <View
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Button
                onPress={() => this.continueAsTrainer()}
                block
                style={[
                  styles.actionbtn,
                  {
                    backgroundColor: color.blue,
                    color: "white",
                    marginVertical: 8
                  }
                ]}
              >
                <Text style={styles.actionbtntxt}>
                  Save and Continue as Trainer
                </Text>
              </Button>

              <Button
                onPress={() => this.continueAsVenue()}
                block
                style={[
                  styles.actionbtn,
                  {
                    backgroundColor: color.orange,
                    color: "white",
                    marginVertical: 8
                  }
                ]}
              >
                <Text style={styles.actionbtntxt}>
                  Continue to Venue Details
                </Text>
              </Button>
            </View>
          </View>
        </TouchableOpacity>
      </Content>
    );
  }
  componentDidMount() {}
}

const styles = StyleSheet.create({
  itemsize: {
    height: hp("5.5%")
  },
  actionbtn: {
    borderRadius: 5,
    justifyContent: "center",
    paddingHorizontal: 14,
    marginHorizontal: 10,
    color: "white",
    textTransform: "uppercase",

  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2%"),
    fontWeight: "bold",
    textTransform: "uppercase"
  }
});


export default AddNewVenue;