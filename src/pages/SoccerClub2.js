import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Picker,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions
} from "react-native";
import { Actions, Router, Scene } from "react-native-router-flux";
import { List, ListItem, Thumbnail, Left, Body, H2 } from "native-base";
import color from "../Helpers/color";
import StarRating from "react-native-star-rating";
import AsyncStorage from "@react-native-community/async-storage";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import Geolocation from "react-native-geolocation-service";
import TabLoginProcess from "../components/TabLoginProcess";
import placeholder_img from "../images/placeholder_img.png";
import ModalComp from "../components/Modal";
import VenueCardComp from "../components/courseCard";
import links from "../Helpers/config";
import Preview from "../images/imageloader.png";
import Toast from "react-native-simple-toast";
import Next from "../components/button";
import CourseBooking from "../pages/courseBooking";
import RazorpayCheckout from "react-native-razorpay";


isCloseToRight = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToRight = 10;
  // alert(layoutMeasurement.width + contentOffset.x)
  return (
    layoutMeasurement.width + contentOffset.x >=
    contentSize.width - paddingToRight
  );
};


export default class SliderComp2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chosenlocation: {
        latitude: "",
        longitude: ""
      },
      showDetailDesc: false,
      loginDetails: null,
      searchhome: [],
      showbooking: false,
      activeIndex: 0,
      visible: false,
      search_venue: false,
      bookobj: null,
      booksuccess: null,
      photovisible: null,
      photoItems: [],
      currentlocation: null,
      courseDetailByTrainer: [],
      activeId: null,
      activeCourse: null,
      commonArray: [],
      order_id: null,
      loading:false
    };
  } 

                   getOrderId() {
                      console.log("getorder", {
                        amount:
                          parseInt(
                            this.state.commonArray[4].spec_det_datavalue1
                          ) * 100,
                        currency: this.state.activeCourse.batchDetailsData[0]
                          .batch_fee_currency,
                        receipt: "receipt#1",
                        payment_capture: 1
                      });
                   fetch("https://api.razorpay.com/v1/orders", {
                     method: "POST",
                     headers: {
                       "Content-Type": "application/json",
                       Authorization:
                         "Basic cnpwX3Rlc3RfbWlDc3U5VFJjZGEzM2k6VktVckx1Z2Zycnpzck00OFhuam95TEhZ",
                       Accept: "application/json",
                       "Cache-Control": "no-cache",
                       Host: "api.razorpay.com",
                     },
                     body: JSON.stringify({
                       amount:
                         parseInt(
                           this.state.commonArray[4].spec_det_datavalue1
                         ) * 100,
                       currency: "INR",
                       receipt: "receipt#1",
                       payment_capture: 1,
                     }),
                   })
                     .then((response) => response.json())
                     .then((responseJson) => {
                       console.log("responseJson", responseJson);
                       var order = responseJson.id;
                       if (order != null && order != undefined) {
                         this.setState({ order_id: order }, () => {
                           this.MakePayment();
                         });
                       } else {
                         this.setState({ loading: false });
                         alert(
                           `Error: ${responseJson.error.code} | ${responseJson.error.description}`
                         );
                         return;
                       }
                     });
                 } 

                 MakePayment=()=>{

                  console.log("makepayment", {
                    description: "iLRNU Course Booking",
                    image:
                      "https://www.ilrnu.com/static/media/Header_logo.e04394ae.svg",
                    currency: this.state.activeCourse.batchDetailsData[0]
                      .batch_fee_currency,
                    key: links.API_KEY,
                    amount: parseFloat(
                      this.state.commonArray[4].spec_det_datavalue1
                    ).toFixed(2),
                    name: this.state.activeCourse.course_title,
                    order_id: this.state.order_id, //Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
                    prefill: {
                      email: this.state.commonArray[3].spec_det_datavalue1,
                      contact: this.state.commonArray[2].spec_det_datavalue1,
                      name: this.state.commonArray[0].spec_det_datavalue1
                    },
                    theme: { color: color.orange }
                  });
                     var options = {
                       description: "iLRNU Course Booking",
                       image:
                         "https://www.ilrnu.com/static/media/Header_logo.e04394ae.svg",
                       currency: "INR",
                       key: links.API_KEY,
                       amount: parseFloat(
                         this.state.commonArray[4].spec_det_datavalue1
                       ).toFixed(2),
                       name: this.state.activeCourse.course_title,
                       order_id: this.state.order_id, //Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
                       prefill: {
                         email: this.state.commonArray[3].spec_det_datavalue1,
                         contact: this.state.commonArray[2].spec_det_datavalue1,
                         name: this.state.commonArray[0].spec_det_datavalue1
                       },
                       theme: { color: color.orange }
                     };
                     RazorpayCheckout.open(options)
                       .then(data => {
                         // handle success
                        //  alert(`Success: ${data.razorpay_payment_id}`);
                        console.log("paymentsuccess", data.razorpay_payment_id);
                        this.setState({ payment_id: data.razorpay_payment_id },()=>{
                          this.insertCourseBooking()
                        });
                       })
                       .catch(error => {
                         // handle failure
                         this.setState({loading:false})
                         alert(`Error: ${error.code} | ${error.description}`);
                       });
                 }


  courseBooking=(data,data1)=>{
    this.setState({ commonArray: data,loading:true }, () => {
      console.log("activecourse", this.state.activeCourse);
      console.log("commonarray", this.state.commonArray);
      this.getOrderId()
    });
      
  }

  componentWillReceiveProps(props) {
    // console.log(props.data);
    // alert(JSON.stringify(props));
    if (props.sendSliderData) {
      this.setState(
        { searchhome: props.sendSliderData, activeIndex: 0 },
        function() {
          // this._carousel.snapToItem(0);
        }
      );
    }
    // alert(this.state.searchhome);
  }
  loadVenueForm = () => {
    // Actions.loadVenueForm
    if (this.state.loginDetails) {
      if (this.state.loginDetails.user_cat_id == 1) {
        Actions.venueform({
          logindata: this.state.loginDetails
        });
      } else {
        Actions.corporateform({
          logindata: this.state.loginDetails
        });
      }
    } else {
      Actions.venuepage({ raiselogin: true });
    }
  };

  loadCarousel = data => {
    if (data.length > 0) {
      this.setState({ photoItems: data }, function() {
        this.setState({ photovisible: true });
      });
    } else {
      alert("no images were added");
      // alert(JSON.stringify(data));
    }
  };

  componentWillMount = () => {
    this.loadcurrentLocation();
  };

  async componentDidMount() {
    const data = await AsyncStorage.getItem("loginDetails");
    // alert(data);
    if (data) {
      // alert(JSON.stringify(data));
      this.setState({ loginDetails: JSON.parse(data) });
    }
  }
  LeftArrow = () => {
    this._carousel.snapToPrev();
    const nextIndex =
      this.state.activeIndex === 0
        ? this.state.searchhome.length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  };
  RightArrow = () => {
    this._carousel.snapToNext();
    const nextIndex =
      this.state.activeIndex === this.state.searchhome.length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  };

  closemodal = () => {
    this.setState({ search_venue: null });
    this.setState({ booksuccess: null });
    if (this.props.closetabmodal) {
      this.props.closetabmodal();
    }
  };



  insertCourseBooking = (data,error) => {    

              
                   fetch(links.APIURL + "courseBooking/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       paymentId: this.state.payment_id,
                       CourseId: this.state.activeCourse.course_id,
                       batchId: this.state.activeCourse.batchDetailsData[0]
                         .training_batch_id,
                       userId: this.state.loginDetails.user_id,
                       bookingName: this.state.commonArray[0]
                         .spec_det_datavalue1,
                       bookingPhone: this.state.commonArray[2]
                         .spec_det_datavalue1,
                       bookingEmail: this.state.commonArray[3]
                         .spec_det_datavalue1,
                       bookingCoursePrice: this.state.activeCourse
                         .batchDetailsData[0].batch_fee,
                       bookingCourseFromDate: "2020-04-01",
                       bookingCourseToDate: "2020-05-01",
                       bookingMoreInfo: this.state.commonArray[5]
                         .spec_det_datavalue1,
                       promoId: "",
                       promoType: "",
                       promoValue: "",
                       promoAmount: "",
                       finalPrice: this.state.commonArray[4]
                         .spec_det_datavalue1,
                       status: "1",
                       bookingCCode:"91"
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => { 
                       this.setState({loading:false})
                       console.log("responsd", responseJson);
                       if (responseJson.status == 0) {
                           this.setState({showbooking:false},()=>{
                              Toast.show("Course booked successfully", Toast.LONG); 
                                 Actions.reset("home1");
                           })
                       } else {
                         Toast.show(responseJson.msg, Toast.LONG);
                       }
                     });
                 };




  onLoadMore = data => {
    console.log("data", data);
    this.setState({ showDetailDesc: data, activeCourse: null });
  };

  showDescription = item => {
     console.log("activecourse", item);
    this.setState({ showDetailDesc: true, activeCourse: item });
  };

  onBook = item => {
    console.log("ressed", item);
    this.setState({ activeCourse: item }, () => {
      this.setState({ showDetailDesc: true });
    });
  };

  onLoadPayment = item => {
    this.setState({ showbooking: true,showDetailDesc:false });
  };

  BookNow = item => {
    //  Actions.BookDetails({
    //                          lat: this.state.chosenlocation.latitude,
    //                          long: this.state.chosenlocation.longitude,
    //                          id: item.venue_id
    //                        })
    //  AsyncStorage.getItem('loginDetails', (err, result) => {

    //    if(result!=null){
    // this.setState({loginDetails:JSON.parse(result)})

    // this.setState({bookobj:},function(){
    //     //  this.setState({search_venue:true});
    //      Actions.BookDetails({
    //        lat: this.state.chosenlocation.latitude,
    //        long: this.state.chosenlocation.longitude,
    //        id: 56
    //      });

    // })

    //  //Actions.Venue_search();
    //   }else{
    // this.setState({loginDetails:null})
    // this.setState({visible:true})

    //   }
    //  });
    this.setState(
      {
        activeId: item.trainer_id
      },
      () => {
        this.getCourseList();
      }
    );
  };

  getLangName = id => {
    if (id == 1) return "English";
    else if (id == 2) return "Hindi";
    else return "Tamil";
  };
  getCourseList = () => {
    this.setState({ showloading: true });
    fetch(links.APIURL + "getTrainerDetailsById/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        trainerId: this.state.activeId
        // trainerId: 1
      })
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log("getcourse", responseJson);
        this.setState({ showloading: false });
        if (
          responseJson.status == 0 &&
          responseJson.data[0].courseDetails.length > 0
        ) {
          this.setState({
            courseDetailByTrainer: responseJson.data[0].courseDetails,
            showDetailDesc: true
          });
          console.log("course", responseJson.data[0].courseDetails);
        } else {
          Toast.show("No Courses Found !!", Toast.LONG);
        }
      });
  };

  async getaccess() {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {}
    );
    // console.log('granted',granted);
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  }

  async loadcurrentLocation() {
    this.setState({ currentlocation: "blue" });
    console.log("calling Twice");
    Geolocation.getCurrentPosition(
      position => {
        this.setState({
          chosenlocation: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
        });
      },
      error => console.log(error.message),
      { timeout: 30000, maximumAge: 0 }
    );
  }

  receivelogindet = () => {
    // alert(this.state.searchhome[this.state.activeIndex]);
    this.setState(
      { bookobj: this.state.searchhome[this.state.activeIndex] },
      function() {
        this.setState({ search_venue: true });
      }
    );
  };
  booksuccess = (data, data1) => {
    data.bookdetails = data1;
    this.setState({ booksuccess: data });
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={c => {
            this.scrollimage = c;
          }}
          onScroll={({ nativeEvent }) => {
            if (isCloseToRight(nativeEvent)) {
              // enableSomeButton();
              this.props.sendScrollEnd && this.props.sendScrollEnd(true);
              // setTimeout((obj)=>{
              // self.scrollimage.scrollToEnd();
              // },200)
              // alert("reached end")
            }
          }}
          directionalLockEnabled={false}
          horizontal={true}
          pagingEnabled={false}
          showsHorizontalScrollIndicator={false}
        >
          {this.state.searchhome.length > 0 &&
            this.state.searchhome.map((item, index) => {
              return (
                <TouchableOpacity
                  style={styles.square}
                  onPress={() => this.BookNow(item)}
                  key={index}
                >
                  <VenueCardComp item={item} star={true} onBook={this.onBook} />
                </TouchableOpacity>
              );
            })}

          {this.props.children}
        </ScrollView>

        {this.state.showbooking && this.state.activeCourse != null && (
          <ModalComp
            titlecolor={color.orange}
            visible={this.state.showbooking}
            closemodal={data => this.setState({ showbooking: false })}
          >
            <View
              style={{
                flex: 1,
                paddingVertical: 15,
                paddingHorizontal: 15
              }}
            >
              <H2
                style={{
                  textWrap: "wrap",
                  width: wp("75%"),
                  borderBottomColor: color.black1,
                  borderBottomWidth: 1,
                  borderBottomStyle: "solid"
                }}
              >
                Booking Your Course
              </H2>

              <Text style={[styles.h3, { color: color.blue }]}>
                {this.state.activeCourse.course_title}
              </Text>

              <StarRating
                // disabled={false}
                maxStars={5}
                rating={4}
                selectedStar={rating => null}
                halfStarColor={color.blue}
                fullStarColor={color.blue}
                starSize={8}
                containerStyle={styles.RatingBar}
                starStyle={styles._starstyle}
                emptyStarColor={color.ash6}
              />

              <View>
                <ScrollView>
                  <CourseBooking
                    onSubmit={this.courseBooking}
                    loginDetails={this.state.loginDetails}
                    loading={this.state.loading}
                    price={
                      this.state.activeCourse &&
                      this.state.activeCourse.batchDetailsData &&
                      this.state.activeCourse.batchDetailsData[0].batch_fee
                    }
                  />
                </ScrollView>
              </View>
            </View>
          </ModalComp>
        )}

        {this.state.showDetailDesc && this.state.activeCourse != null && (
          <ModalComp
            titlecolor={color.orange}
            visible={this.state.showDetailDesc}
            closemodal={data => this.onLoadMore(data)}
          >
            <View
              style={{
                flex: 1,
                paddingVertical: 15,
                paddingHorizontal: 15
              }}
            >
              <H2 style={{ textWrap: "wrap", width: wp("75%") }}>
                Course Details
              </H2>

              <View style={{ paddingVertical: hp("2%") }}>
                <ScrollView>
                  <View
                    style={{
                      display: "flex",
                      margin: 5,
                      backgroundColor: "white",
                      padding: 5
                    }}
                  >
                    <View style={styles.imageView}>
                      <TouchableOpacity style={styles.venueimage}>
                        {this.state.activeCourse &&
                        this.state.activeCourse.uploadData.length > 0 ? (
                          <Image
                            style={styles.venueimage}
                            source={{
                              uri: this.state.activeCourse.uploadData[0]
                                .trainer_upload_path
                            }}
                          />
                        ) : (
                          <Image style={styles.venueimage} source={Preview} />
                        )}
                      </TouchableOpacity>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        display: "flex"
                      }}
                    >
                      <View>
                        <Text style={[styles.h1, { color: color.blue }]}>
                          {this.state.activeCourse &&
                            this.state.activeCourse.course_title}
                        </Text>
                      </View>
                      <View>
                        <Text>
                          {" "}
                          {this.state.activeCourse &&
                            this.getLangName(
                              this.state.activeCourse.languageId
                            )}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        margin: 3,
                        backgroundColor: color.ash1,
                        padding: 10
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                          display: "flex"
                        }}
                      >
                        <View>
                          <Text style={[styles.h6]}>
                            Course Fee {"\n"}{" "}
                            <Text style={styles.h1}>
                              {this.state.activeCourse &&
                                this.state.activeCourse.batchDetailsData
                                  .length > 0 &&
                                this.state.activeCourse.batchDetailsData[0]
                                  .batch_fee}{" "}
                              {this.state.activeCourse &&
                                this.state.activeCourse.batchDetailsData
                                  .length > 0 &&
                                this.state.activeCourse.batchDetailsData[0]
                                  .batch_fee_currency}
                            </Text>
                          </Text>
                        </View>
                        <View>
                          <Text style={[styles.h6]}> {"\n"}</Text>
                          <Text style={styles.h1}>
                            <Text style={styles.h3}>
                              {this.state.activeCourse &&
                                this.state.activeCourse.batchDetailsData
                                  .length > 0 &&
                                this.state.activeCourse.batchDetailsData[0]
                                  .batch_startDate}{" "}
                              to{" "}
                              {this.state.activeCourse &&
                                this.state.activeCourse.batchDetailsData
                                  .length > 0 &&
                                this.state.activeCourse.batchDetailsData[0]
                                  .batch_endDate}
                            </Text>
                          </Text>
                        </View>
                      </View>

                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                          display: "flex"
                        }}
                      >
                        <View>
                          <Text style={[styles.h6]}>
                            No Of Hours {"\n"}{" "}
                            <Text style={styles.h1}>
                              {" "}
                              {this.state.activeCourse &&
                                this.state.activeCourse.batchDetailsData
                                  .length > 0 &&
                                this.state.activeCourse.batchDetailsData[0]
                                  .batch_duration_hours}
                            </Text>
                          </Text>
                        </View>
                        <View>
                          <Text style={[styles.h6]}>
                            Seats {"\n"}
                            <Text style={styles.h1}>
                              {" "}
                              {this.state.activeCourse &&
                                this.state.activeCourse.batchDetailsData
                                  .length > 0 &&
                                this.state.activeCourse.batchDetailsData[0]
                                  .batch_seat_count}
                            </Text>
                          </Text>
                        </View>
                      </View>
                    </View>

                    <View style={styles.box}>
                      <Text style={styles.h1}>Course Outline </Text>
                      <Text style={styles.h6}>
                        {this.state.activeCourse &&
                          this.state.activeCourse.course_outline}
                      </Text>

                      <Text style={styles.h1}>Course Description </Text>
                      <Text style={styles.h6}>
                        {this.state.activeCourse &&
                          this.state.activeCourse.course_desc}
                      </Text>
                    </View>

                    <View
                      style={{
                        flex: 1,
                        justifyContent: "flex-end",
                        alignItems: "center",
                        bottom: 0,
                        left: 0,
                        paddingVertical: hp("2%")
                      }}
                    >
                      <Next
                        onClick={() =>
                          this.onLoadPayment(this.state.activeCourse)
                        }
                        name="Book Now"
                        _width={wp("40%")}
                      />
                    </View>
                  </View>
                </ScrollView>
              </View>
            </View>
          </ModalComp>
        )}

        {/* {this.state.visible && this.state.visible == true && (
                         <TabLoginProcess
                           sendlogindet={() => this.receivelogindet()}
                           type="login"
                           visible={true}
                           tabAction={this.tabAction}
                           closetabmodal={() =>
                             this.setState({ visible: null })
                           }
                         />
                       )} */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    padding: hp("1.5%"),
    marginBottom: hp("1.5%"),
    backgroundColor: color.white
  },
  txt1: {
    fontSize: hp("2%")
  },
  flex_row: {
    flexDirection: "row",
    height: hp("15%"),
    flex: 1
  },
  txtcontainer: {
    flex: 0.9,
    // paddingTop:hp('1.5%'),
    paddingLeft: hp("1.5%"),
    paddingBottom: hp("1.5%")
  },
  txtcontainer1: {
    flex: 1,
    width: "100%",
    paddingTop: hp("1.5%"),
    paddingLeft: hp("1.5%")
    // paddingBottom:hp('1.5%')
  },
  blue_arr: {
    height: hp("2.5%"),
    width: hp("2.5%"),
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center"
  },
  booK_nw_txt: {
    color: color.white,
    fontSize: hp("1.5%"),
    paddingLeft: 8,
    paddingRight: 8
  },
  _end: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    alignContent: "flex-end"
  },
  booK_nw_txt: {
    color: color.white,
    fontSize: hp("1.5%"),
    paddingLeft: 8,
    paddingRight: 8
  },
  view22: {
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center"
  },
  booK_nw_btn: {
    backgroundColor: color.blue,
    height: hp("3%"),
    marginRight: hp("1.5%"),
    paddingLeft: 8,
    paddingRight: 8,
    position: "absolute",
    bottom: 0,
    right: 0
  },
  square: {
    margin: 5,
    width: hp("15%")
  },
  h3: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  h6: {
    fontSize: 11,
    fontWeight: "200",
    fontStyle: "normal"
  },
  h1: {
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  imagecontainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap"
  },
  imageView: {
    width: wp("95%"),
    height: hp("17%"),
    borderRadius: 7,
    marginBottom: 15,
    paddingHorizontal: wp("5%")
  },
  venueimage: {
    position: "absolute",
    width: "100%",
    height: "100%",
    borderRadius: 7
  },
  RatingBar: {
    height: hp("2%"),
    width: wp("13%")
  },
  _starstyle: {
    fontSize: 12,
    marginRight: 3
  },
  box: {
    justifyContent: "flex-start",
    display: "flex"
  }
});
