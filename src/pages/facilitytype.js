'use strict';

import React, { Component } from 'react';
import CircleText from'../components/circlecomp';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import links from '../Helpers/config';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import Work from '../images/svg/work.png';
import Education from '../images/svg/education.png';
import Conference from '../images/svg/presentation.png';
import Lab from '../images/svg/workplace.png';
import Cabins from '../images/svg/employee.png';
var datas=[{name:'Training Room',id:1,icon:Work},{name:'Training Hall',id:2,icon:Education},{name:'Conference Room',id:3,icon:Conference},{name:'Training Lab',id:4,icon:Lab},{name:'Cabins',id:5,icon:Cabins}]

class FacilityType extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {facilitydata:null,facilityDetails:[]};
	  // alert(datas.length%2);
	}
    componentWillReceiveProps(props){
    if(props.facilitytypeData){
      this.setState({facilitydata:props.facilitytypeData});
    }
  }
	componentWillMount(){
    this.props.headertext({headertext:<Text style={{fontSize:hp('1.7%'),fontWeight:'bold',color:color.blue}}>Specific Venue</Text>});
		this.props.labeltext({labeltext:<Text>Please choose the <Text style={{color:color.orange,fontWeight:'bold'}}> Specific Venue </Text> </Text>});
	}
changeVenueType=(data,key)=>{
	this.setState({facilitydata:data});
  this.props.sendfacilitytypedata(data);
}
loadSpecifVenue(data){

  fetch(links.APIURL+'listSpecificVenue/', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({user_cat_id:"1","venue_cat_id":data}),
  }).then((response)=>response.json())
  .then((responseJson)=>{
    // console.log(responseJson.data)
    // alert(JSON.stringify(responseJson))
    if(responseJson.status==0){
      this.setState({facilityDetails:responseJson.data})
    }
    // if(responseJson.data.length==0){
    //   this.setState({length:false,loading:false})
    // }
    // this.setState({specificarray:responseJson.data,loading:false});
  })
}

  render() {
 
 
    return (
      <View style={{flex:1,flexDirection:'row',flexWrap:'wrap',padding:12,}}>
      {this.state.facilityDetails.length>0&&this.state.facilityDetails.map((obj,key)=>{
      	return(
      		 <View key={key} style={[styles.circleStyle]}>
      <CircleText  sendText={(data)=>{this.changeVenueType(data,key)}}  style={[styles.flexWidth]} width={12} height={12} text={obj.venue_spec_name} image={obj.venue_spec_icon} data={obj} active={this.state.facilitydata&&this.state.facilitydata.venue_spec_id==obj.venue_spec_id}/>
      </View>
      		)
      })}
     
      

      </View>
    );
  }
  componentDidMount(){
    if(this.props.venuetypedata){
      this.loadSpecifVenue(this.props.venuetypedata.venue_cat_id)
    }
    if(this.props.facilitytypedata){
      this.setState({facilitydata:this.props.facilitydata})
    }
}
}

const styles = StyleSheet.create({

circleStyle:{
	width:wp('30%'),
	flexDirection:'row',
	justifyContent:'center'
},
activename:{
}
});


export default FacilityType;