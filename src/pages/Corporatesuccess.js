import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList,} from 'react-native';
import {Right,Top,Icon, Button,ListItem,} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import color from '../Helpers/color';
import fb_png from '../images/fb.png';
import insta_png from '../images/insta.png';
import google_png from '../images/google.png'
import ModalComp from '../components/Modal';

export default class Corporatesuccess extends Component{
    switchIndividual=()=>{
Alert.alert(
  'Alert',
  'Switch To Individual Login?',
  [
    {
      text: 'Cancel',
      onPress: () => console.log('cancelled'),
      style: 'cancel',
    },
    {text: 'OK', onPress: () =>{
        AsyncStorage.removeItem('loginDetails');
        this.props.closemodal('individual')

    } },
  ],
  {cancelable: false},
);
}
    render(){
        return(
                 <ModalComp titlecolor={color.orange} closemodal={this.props.closemodal} visible={true}>
            <View style={styles.container}>
                <View style={{flex:.1}}></View>
                <View style={styles.view1}>
                    <View style={styles.view2}>
                        <Text style={styles.txt1}>Hello</Text>
                        <Text style={styles.txt2}>Corporate User</Text>
                    </View>
                    <View style={styles.view3}>
                        <Text style={styles.txt3}>You have successfully authorizod</Text>
                        <Text style={styles.txt3}>{this.props.locationArray?this.props.locationArray.length:0} Venue SPOCs to add the relevant</Text>
                        <Text style={styles.txt3}>venue details</Text>
                    </View>
                    <View style={styles.view3}>
                        <Text style={styles.txt3}>Message and Mails are</Text>
                        <Text style={styles.txt3}>delivered to related SPOCs</Text>
                    </View>

                    <View style={styles.view6}>
                        <Text style={styles.txt7}>Share with</Text>
                        <TouchableOpacity><Image style={styles.socialicon} source={google_png}/></TouchableOpacity>
	                    <TouchableOpacity><Image style={styles.socialicon} source={fb_png}/></TouchableOpacity>
	                    <TouchableOpacity><Image style={styles.socialicon} source={insta_png}/></TouchableOpacity>
                    </View>
                </View>    
               
                <View style={{flexDirection:'row',justifyContent:'center'}}>
                        <Text style={{ marginRight:3,color:color.orange,fontSize:hp('2.5 %'),fontWeight:'bold'}}>Individual User </Text>
                        <TouchableOpacity onPress={()=>this.switchIndividual()}>
                        <Text style={{ marginRight:3,color:color.orange,fontSize:hp('2.5%'),fontWeight:'bold'}}>Login Here</Text>
                        </TouchableOpacity>
                </View>
                
                <View style={{flex:.1,flexDirection:'row',marginTop:hp('2%'),marginBottom:hp('2%')}}>
                    <View style={styles.view12}>
                        <Button style={styles.cancel_btn} onPress={this.props.closemodal}>
                            <Text style={styles.cancel_txt}> ADD MORE </Text>
                        </Button>
                    </View>
                </View>    
            </View>
             </ModalComp>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:color.white, 
        paddingLeft:hp('2%'),
        paddingRight:hp('2%')
    },
    view1:{
        marginTop:hp('10%'),
        flex:.5,
        flexDirection:'column',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    view2:{
        flexDirection:'row',
        marginBottom:hp('3%')
    },
    view3:{
        marginTop:hp('2%'),
        marginBottom:hp('2%')
    },
    view4:{
        marginBottom:hp('2%')
    },
    view5:{
        marginTop:hp('2%'),  
    },
    view6:{
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center',
        marginTop:hp('5%'),  
        marginBottom:hp('5%'),  
    },



    txt1:{
        marginRight:3,
        fontSize:hp('3%')
    },
    txt2:{
        fontWeight:'bold',
        fontSize:hp('3%')
    },
    txt3:{
        color:color.orange,
        fontSize:hp('2.5%')
    },
    txt4:{
        color:color.orange,
        fontSize:hp('3%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt5:{
        fontSize:hp('2.5%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt6:{
        fontSize:hp('3%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt7:{
        color:color.black,
        fontSize:hp('2.5%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center',
        marginRight:8,
        
    },
    socialicon:{
		width:hp('4%'),
		height:hp('4%'),
        margin:2
    },
    view12:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    view13:{
        flex:1,
        justifyContent:'flex-end',
        alignSelf:'flex-end',alignContent:'flex-end',alignItems:'flex-end',
        flexDirection:'row'
    },
    cancel_txt:{
        fontSize:hp('2%'),
        color:color.white
    },
    cancel_btn:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.orange,
        height:hp('5%')
    },
})