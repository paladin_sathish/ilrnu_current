import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity } from "react-native";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Button,
  Card,
  CardItem
} from "native-base"; 
import PromoCode from "./promocode";
import { Actions } from "react-native-router-flux";
import UnBlockVenue from './unBlockVenue'; 


export default class calenderBooked extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     visibleslot: false,
                     makeItFree: false,
                     data: []
                   };
                 }

                 refresh=()=>{
                  
                   this.setState({
                     makeItFree: false,
                     visibleslot: false,
                     data:[]
                   },()=>{
                      this.props.refresh();
                   });
                 
                 }

                 openModal = item => {
                   console.log("itemsfsd", item);
                   this.setState({ visibleslot: true, data: item });
                 };

                 openMakeItFree = item => {
                   console.log("itemsfsd", item);
                   this.setState({ makeItFree: true, data: item });
                 };

                 _renderList = (item, i) => {
                   return (
                     <Card padder>
                       <View
                         style={{
                           display: "flex",
                           flexDirection: "row",
                           justifyContent: "space-between",
                           paddingHorizontal: 15,
                           marginVertical: 6
                         }}
                       >
                         <View>
                           <TouchableOpacity
                             onPress={() =>
                               this.openModal(item.bookedBlockedSlots)
                             }
                           >
                             <Text
                               style={{
                                 color: "#3f51b5",
                                 textDecorationLine: "underline"
                               }}
                             >
                               {item.bookedBlockedSlots.length} Slots
                             </Text>
                           </TouchableOpacity>
                         </View>
                         <View>
                           <Text style={{ fontSize: 16 }}>
                             {item.trn_booking_phone}
                           </Text>
                         </View>
                       </View>

                       <View
                         style={{
                           display: "flex",
                           flexDirection: "row",
                           justifyContent: "space-between",
                           paddingHorizontal: 15,
                           marginVertical: 6
                         }}
                       >
                         <View>
                           <Text style={{ fontSize: 16 }}>
                             {item.trn_booking_name}
                           </Text>
                         </View>
                         <View>
                           <Text style={{ fontSize: 16 }}>
                             {item.trn_booking_email}
                           </Text>
                         </View>
                       </View>

                       <View
                         style={{
                           display: "flex",
                           flexDirection: "row",
                           justifyContent: "space-between",
                           paddingHorizontal: 15,
                           marginVertical: 6
                         }}
                       >
                         <View>
                           <Text style={{ fontSize: 16 }}>
                             {item.trn_booking_event_name}
                           </Text>
                         </View>
                         <View>
                           <Button
                             ordered
                             small
                             danger
                             onPress={() => this.openMakeItFree(item)}
                           >
                             <Text>Block</Text>
                           </Button>
                         </View>
                       </View>
                     </Card>
                   );
                 };

                 _closePressed=()=>{ 
                   console.log('closed modal')
                   this.setState({
                     makeItFree: false,
                     visibleslot:false
                   });
                 }

                 render() {
                   const { data } = this.props;
                   console.log("dataldgfsd", data);

                   if (data.length == 0) {
                     return (
                       <View
                         style={{
                           display: "flex",
                           justifyContent: "center",
                           alignItems: "center",
                           paddingVertical: 20
                         }}
                       >
                         <Text style={{ fontSize: 19 }}>
                           {" "}
                           No Records Found{" "}
                         </Text>
                       </View>
                     );
                   }
                   return (
                     <View>
                       <ScrollView>
                         <List>
                           {data.length > 0 &&
                             data.map((item, i) => this._renderList(item, i))}
                         </List>

                         <PromoCode
                           data={this.state.data}
                           isPromoShow={this.state.visibleslot}
                           closePressed={this._closePressed}
                         />

                         <UnBlockVenue
                           data={this.state.data}
                           isPromoShow={this.state.makeItFree}
                           closePressed={this._closePressed}
                           refresh={this.refresh}
                         />
                       </ScrollView>
                     </View>
                   );
                 }
               }
