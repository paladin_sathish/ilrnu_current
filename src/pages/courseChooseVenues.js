'use strict';

import React, { Component } from "react";
import CircleText from "../components/circlecomp";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import ModalComp from "../components/Modal";
import Checkbox from "../components/checkbox2";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content,
  H2,
  ListItem
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  Picker,
  TouchableOpacity,
  Image,
  ScrollView
} from "react-native";
import CourseAddVenue from "../pages/courseAddVenue";
import links from "../Helpers/config";
import Next from "../components/button";
import ADD_img from "../images/add_icon.png";
import Toast from "react-native-simple-toast"; 

class SelectedArray {
  constructor() {
    this.selectedItemsArray = [];
  }

  setItem(option) {
    this.selectedItemsArray.push(option);
  }

  getArray() {
    return this.selectedItemsArray;
  }
}
var selectnewArray = new SelectedArray();
const _list = [
  {
    id: 1,
    name: "Own Venues"
  },
  {
    id: 2,
    name: "iLNRU Venues"
  }
];

export default class courseChooseVenues extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     selectedType: {
                       id: 1,
                       name: "Own Venues"
                     },
                     selectedArrayRef: new SelectedArray(),
                     visible: false,
                     keyboardshow: false,
                     AmentiesObj: null,
                     selected: "key0",
                     own_venue_res: [],
                     booked_venue_res: [],
                     selectedVenue: []
                   };
                 }

                 onSubmit = () => {
                   if (
                     this.state.selectedType.id > 0 &&
                     this.state.selectedVenue != null
                   ) {
                     this.props.onSubmit();
                   } else {
                     Toast.show("Field's Missing !!", Toast.LONG);
                     return;
                   }
                 };

                 componentWillReceiveProps(props) {
                   if (props.purposeDatas != null) {
                     this.state = props.purposeDatas;
                   }

                   if(props.chooseVenueData!= null){
                     console.log('chooseveneue',props.chooseVenueData)
                     var selectedType = this.state.selectedType;
                     selectedType.id = props.chooseVenueData.venueType;
                     this.setState({
                       selectedType
                     })

                     if(props.chooseVenueData.venueObj !=null){
                       var selectedVenue = this.state.selectedVenue;
                       selectedVenue = props.chooseVenueData.venueObj;
                       this.setState({selectedVenue},()=>{
                         console.log('redevvenueobj',this.state.selectedVenue)
                       })
                     }

                   }
                 }

                 componentWillMount() {
                   this.props.headertext({
                     headertext: "Add Course",
                     showskip: false
                   });
                   this.props.labeltext({
                     labeltext: (
                       <Text>
                         {" "}
                         Venues{" "}
                         <Text
                           style={{ color: color.orange, fontWeight: "bold" }}
                         >
                           {" "}
                           Details{" "}
                         </Text>{" "}
                       </Text>
                     )
                   });
                 }

                 onAddVenue = () => {};

                 selectedData = (key, data, obj) => {
                   console.log('selected',obj)
                   this.setState(
                     {
                       selectedVenue: obj
                     },
                     () =>
                       this.props.sendpurposes(
                         {
                           venueType: this.state.selectedType.id,
                           venueId: this.state.selectedVenue.ownVenueId
                         },
                         this.state
                       )
                   );
                 };

                 onCreate = () => {};

                 openAdd = data => {
                   this.setState({
                     visible: data
                   });
                 };

                 componentDidMount = () => {
                   console.log('called')
                   this.getOwnVenue();
                 };

                 onLoadIlrnuVenues = () => {};

                 getOwnVenue = () => {
                   fetch(links.APIURL + "getOwnVenueList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       userId:
                         this.props.loginDetails &&
                         this.props.loginDetails.user_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       if (responseJson.status == 0) {
                         this.setState({
                           own_venue_res: responseJson.data
                         });

                         
                        
                       }
                        if (responseJson.data.length <= 0)
                          Toast.show("No Own Venues Found", Toast.LONG);
                     });
                 };

                 getBookedVenueList = () => {
                   console.log(
                     "userid",
                      this.props.loginDetails.user_id
                   );
                   fetch(links.APIURL + "getBookedVenueList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       userId:
                         this.props.loginDetails &&
                         this.props.loginDetails.user_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log('no veuefsdfound',responseJson)
                       if (responseJson.status == 0) {
                         this.setState({
                           booked_venue_res: responseJson.data
                         }); 
                        
                       }

                        if (responseJson.data.length <= 0)
                          Toast.show("No iLRNU Venues Found", Toast.LONG);
                     });
                 };

                 insertOwnVenue = (data,error) => {    
                  
                  if(error)
                  {
                    Toast.show("Please fill out the fields !!", Toast.LONG);
                    return;
                }  

              
                   fetch(links.APIURL + "insertOwnVenue/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       userId:
                         this.props.loginDetails &&
                         this.props.loginDetails.user_id,
                       ownVenueName: data[0].spec_det_datavalue1,
                       ownVenueAddess: data[1].spec_det_datavalue1,
                       ownVenueLocation: data[1].spec_det_datavalue2,
                       ownVenueLandmark: data[2].spec_det_datavalue1,
                       ownVenueMobileNo: data[3].spec_det_datavalue1,
                       ownVenueMailId: data[4].spec_det_datavalue1,
                       area: data[5].spec_det_datavalue1
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log("responsd", responseJson);
                       if (responseJson.status == 1) {
                         this.setState(
                           {
                             visible: false
                           },
                           () => {
                             Toast.show(
                               "Own Venue Added Successfully",
                               Toast.LONG
                             );
                             if (this.state.selectedType.id == 1)
                               this.getOwnVenue();
                           }
                         );
                       } else {
                         Toast.show(responseJson.msg, Toast.LONG);
                       }
                     });
                 };

                 selectCheckBox = item => {
                   var selectedType = this.state.selectedType;
                   selectedType = item;
                   this.setState({ selectedType }, () => {
                     if (item.id == 1){
                       this.props.sendpurposes(
                         {
                           venueType: this.state.selectedType.id,
                           venueId: ''
                         },
                         this.state
                       );
                       this.getOwnVenue();
                        this.setState({selectedVenue:''})
                     } 
                     else { 
                        this.props.sendpurposes(
                         {
                           venueType: this.state.selectedType.id,
                           venueId: ''
                         },
                         this.state
                       );
                       
                       this.getBookedVenueList();
                      this.setState({selectedVenue:''})
                     }
                   });
                 };

                 _renderType = select => {
                   const { selectedType } = this.state;
                   return select.map(item => {
                     return (
                       <Button
                         style={{
                           paddingHorizontal: 25,
                           backgroundColor:
                             item.id == selectedType.id
                               ? color.blue
                               : "#DCDCDC",
                           borderRadius: 5
                         }}
                         onPress={() => this.selectCheckBox(item)}
                       >
                         <Text
                           style={{
                             color:
                               item.id == selectedType.id ? "white" : "black"
                           }}
                         >
                           {item.name}
                         </Text>
                       </Button>
                     );
                   });
                 };

                 _renderCheckBox = (item, i) => {
                   return (
                     <ListItem
                       key={1}
                       style={{
                         margin: 0,
                         flexDirection: "row",
                         alignItems: "center",
                         minHeight: hp("9%")
                       }}
                     >
                       <View
                         style={{
                           flex: 0.35,
                           flexWrap: "wrap",
                           flexDirection: "row"
                         }}
                       >
                         <Checkbox
                           selectedData={data =>
                             this.selectedData(i, data, item)
                           }
                           size={hp("4.5%")}
                           label={item.ownVenueName}
                           selectedArrayObject={selectnewArray}
                           active={
                             this.state.selectedVenue &&
                             this.state.selectedVenue.ownVenueId &&
                             this.state.selectedVenue.ownVenueId ==
                               item.ownVenueId
                               ? true
                               : false
                           }
                           keyvalue={i}
                           color={color.orange}
                         />
                       </View>
                     </ListItem>
                   );
                 };

                 render() {
                   return (
                     <View style={styles.container}>
                       <View
                         style={{
                           flexDirection: "row",
                           justifyContent: "space-between",
                           display: "flex",
                           backgroundColor: "#ece9ee",
                           padding: 10
                         }}
                       >
                         <View>
                           <Text
                             style={[styles.h1, { color: color.blueactive }]}
                           >
                             Add Venues
                           </Text>
                         </View>
                         <View>
                           <TouchableOpacity
                             onPress={() => this.openAdd(true)}
                             style={styles.add_symbol_container}
                           >
                             <Image
                               style={styles.add_symbol}
                               source={ADD_img}
                             ></Image>
                           </TouchableOpacity>
                         </View>
                       </View>

                       <View style={styles.select}>
                         {this._renderType(_list)}
                       </View>
                       <View style={{ paddingVertical: hp("2%") }}>
                         {this.state.selectedType.id == 1 &&
                           this.state.own_venue_res.length > 0 &&
                           this.state.own_venue_res.map((item, i) => {
                             return this._renderCheckBox(item, i);
                           })}

                         {this.state.selectedType.id == 2 &&
                           this.state.booked_venue_res.length > 0 &&
                           this.state.booked_venue_res.map((item, i) => {
                             return this._renderCheckBox(item, i);
                           })}
                           
                       </View>
                       <View
                         style={{
                           flex: 1,
                           alignSelf: "center",
                           justifyContent: "flex-end",
                           marginBottom: hp("5%")
                         }}
                       >
                         <Next
                           onClick={() => this.onSubmit()}
                           name="Submit"
                           _width={wp("40%")}
                         />
                       </View>
                       {this.state.visible && (
                         <ModalComp
                           titlecolor={color.orange}
                           visible={this.state.visible}
                           closemodal={data => this.openAdd(data)}
                         >
                           <View
                             style={{
                               flex: 1,
                               paddingVertical: 15,
                               paddingHorizontal: 15
                             }}
                           >
                             <H2
                               style={{
                                 textWrap: "wrap",
                                 width: wp("75%"),
                                 color: color.bluesub,
                                 textDecorationStyle: "dashed",
                                 borderBottomColor: color.ash,
                                 borderBottomWidth: 1,
                                 borderBottomStyle: "solid"
                               }}
                             >
                               Add New Venue
                             </H2>
                             <Text stule={{ fontSize: 12, color: color.blue }}>
                               Please Provide below the basic details to add
                               your own venue
                             </Text>
                             <View>
                               <ScrollView>
                                 <CourseAddVenue
                                   sendfaciltiydata={this.insertOwnVenue}
                                 />
                               </ScrollView>
                             </View>
                           </View>
                         </ModalComp>
                       )}
                     </View>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start"
  },
  h1: {
    fontSize: 17,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 17
  },
  h3: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  h6: {
    fontSize: 11,
    fontWeight: "200",
    fontStyle: "normal"
  },
  box: {
    marginVertical: hp("0.6%")
  },
  select: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingVertical: 10
  },
  add_symbol_container: {
    backgroundColor: color.blue,
    justifyContent: "flex-end",
    justifyContent: "center",
    alignSelf: "flex-end",
    marginRight: wp("5%"),
    height: hp("2.5%"),
    width: hp("2.5%")
  },
  add_symbol: {
    height: hp("1.3%"),
    width: hp("1.3%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center"
  }
});
