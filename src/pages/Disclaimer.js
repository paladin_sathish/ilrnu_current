import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, StatusBar, Modal, Image, TouchableHighlight, TouchableOpacity, ImageBackground,ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Icon, Button, Subtitle, Header, Content, Body, Right, Title, Item, Input,Toast,Root  } from 'native-base';
import color from '../Helpers/color';
import { Actions, Router, Scene } from 'react-native-router-flux';
import ModalComp from "../components/Modal";
export default class Disclaimer extends React.Component {
        constructor(props) {
            super(props);
            this.state={agree:false,opentoast:false};
        }
        onClose=()=>{
        	// alert("");
        	this.setState({opentoast:null});
        }
        sharemore=()=>{
        	// alert("share more")
        	if(this.props.loadsharemore){
        		var obj={type:'sharemore'};
        		if(this.state.agree==false){
        			
        			this.props.loaderrormessage("Please Agree the condition");        			
        		}else{
        			var obj=this.props.signupdata;
        			obj.type='sharemore';
        			obj.agree=this.state.agree;
        			this.props.loadsharemore(obj);
        		}
        		}
        	}

        render() {
                return (
                       <Content style={{flex:1}}>
                       
                       	<Header noShadow={true} style={{backgroundColor:'transparent',margin:12}}>
		<Body style={{alignItems:'flex-start',justifyContent:'center',padding:0}}>
            <Title style={{color:color.orange,fontSize:hp('4%')}}>Disclaimer</Title>
            </Body>
            </Header>
                        <View style={{padding:18}}>
			<Text style={styles.headingUser}>Welcome <Text style={{color:color.orange}}>{this.props.signupdata.name},</Text></Text>
			<Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.{'\n\n\n'}

				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				{'\n\n\n'}
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				{'\n\n\n'}

			
			</Text>
			<View style={{flexDirection:'row',paddingTop:12}}>
			<View style={{flex:0.5,alignItems:'flex-start'}}>
				 <Button iconLeft transparent style={{width:'auto'}} onPress={()=>this.setState({agree:!this.state.agree})}>
            <View style={styles.checkbox}>
            <View style={[styles.checkboxinner,{backgroundColor:this.state.agree==true?color.orange:color.ash4}]}></View>
            </View>
            <Text style={{textAlign:'left',paddingLeft:6,fontSize:hp('2.2%')}}>I Agree</Text>
          </Button>
          </View>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button onPress={()=>this.sharemore()} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
           <Text style={styles.actionbtntxt}>START TO ADD VENUE</Text>
          </Button>	
			</View>
			</View>
			</View>

			</Content>

        )
    }
}
const styles={
	headingUser:{
		fontSize:hp('2.8%'),
		color:color.ash3,
	},
	checkbox:{
		borderWidth:1,
		width:hp('3%'),
		height:hp('3%'),
		alignItems:'center',
		// padding:5,
		borderColor:color.ash3,
		justifyContent:'center',
	},
	checkboxinner:{
		position:'absolute',
		width:hp('1.5%'),
		height:hp('1.5%'),
		backgroundColor:'grey'
	},
		actionbtn:{
		borderRadius:5,
		minWidth:hp('23%'),
		justifyContent:'center',
	},actionbtntxt:{
		textAlign:'center',
		color:color.white,
		fontSize:hp('2%'),
		paddingLeft:10,
		paddingRight:10
	},
}