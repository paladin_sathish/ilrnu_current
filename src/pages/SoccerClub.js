import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,Dimensions} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button} from 'native-base'
import color from '../Helpers/color'
import AsyncStorage from '@react-native-community/async-storage';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'
import Geolocation from "react-native-geolocation-service";
import TabLoginProcess from '../components/TabLoginProcess';
import TopComp from '../components/TopComp'
import Home_venuelist from '../components/home_venuelist'
import placeholder_img from '../images/placeholder_img.png';
import White_arrow from '../images/white_dwn_arrow.png'
import Left_arrow from '../images/left.png'
import Right_arrow from '../images/right.png'
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import Carousel from 'react-native-snap-carousel';
import Book_your_venue from './book_your_venue';
import ModalComp from '../components/Modal';
import SuccessPage from './SuccessPage';
import LoadCarouselModal from './loadCarousel'
import VenueDetailDesc from './venue_detail_desc'
import VenueCardComp from '../components/VenueCardComp';

isCloseToRight = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToRight = 10;
    // alert(layoutMeasurement.width + contentOffset.x)
    return layoutMeasurement.width + contentOffset.x >= contentSize.width - paddingToRight;
};
export default class SliderComp extends Component{
    constructor(props) {
      super(props);
      this.state = {
        chosenlocation:{
            latitude:'',
            longitude:''
        },
        showDetailDesc: null,
        loginDetails: null,
        searchhome: [],
        activeIndex: 0,
        visible: false,
        search_venue: false,
        bookobj: null,
        booksuccess: null,
        photovisible: null,
        photoItems: [],
        currentlocation: null
      };
    }

    componentWillReceiveProps(props){
    // console.log(props.data);
    // alert(JSON.stringify(props));
    if(props.sendSliderData){

      this.setState({searchhome:props.sendSliderData,activeIndex:0},function(){
        // this._carousel.snapToItem(0);
      });
    }
    // alert(this.state.searchhome);
  }
      loadVenueForm=()=>{
      // Actions.loadVenueForm
      if(this.state.loginDetails){
        if(this.state.loginDetails.user_cat_id==1){
          Actions.venueform({logindata:this.state.loginDetails});
        }else{
          Actions.corporateform({logindata:this.state.loginDetails});
        }
      }else{
      Actions.venuepage({raiselogin:true})
      }
    }

       loadCarousel=(data)=>{
         if(data.length>0){

    
     this.setState({photoItems:data},function(){
       this.setState({photovisible:true})
     });
   }else{
     alert("no images were added")
     // alert(JSON.stringify(data));
   }
   } 



   componentWillMount = () => {
     this.loadcurrentLocation();
   };
   
    async componentDidMount(){


const data=await AsyncStorage.getItem('loginDetails');
// alert(data);
if(data){
  // alert(JSON.stringify(data));
  this.setState({loginDetails:JSON.parse(data)});

}

    }
  LeftArrow=()=>{
    this._carousel.snapToPrev();
    const nextIndex = this.state.activeIndex === 0 ? this.state.searchhome.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }
  RightArrow=()=>{
    this._carousel.snapToNext();
     const nextIndex = this.state.activeIndex === this.state.searchhome.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  } 

  
closemodal=()=>{
        this.setState({search_venue:null})
        this.setState({booksuccess:null})
        if(this.props.closetabmodal){
        this.props.closetabmodal();
        }
        }
        showDescription=(item)=>{
          this.setState({showDetailDesc:item})
        }
  BookNow=(item)=>{
     Actions.BookDetails({
                             lat: this.state.chosenlocation.latitude,
                             long: this.state.chosenlocation.longitude,
                             id: item.venue_id
                           }) 
    //  AsyncStorage.getItem('loginDetails', (err, result) => {
      
    //    if(result!=null){
    // this.setState({loginDetails:JSON.parse(result)})
   
    // this.setState({bookobj:},function(){
    //     //  this.setState({search_venue:true});
    //      Actions.BookDetails({
    //        lat: this.state.chosenlocation.latitude,
    //        long: this.state.chosenlocation.longitude,
    //        id: 56
    //      });

    // })

    //  //Actions.Venue_search();
    //   }else{
    // this.setState({loginDetails:null})
    // this.setState({visible:true})

    //   }
    //  });
  } 



 
  async getaccess(){
  const granted= await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{})
  // console.log('granted',granted);
  if(granted===PermissionsAndroid.RESULTS.GRANTED){
   return true;
  }else{
    return false;
  }
}


 async loadcurrentLocation(){
  this.setState({currentlocation:'blue'});
console.log("calling Twice");
  Geolocation.getCurrentPosition(
       (position) => {
         this.setState({chosenlocation:{latitude:position.coords.latitude,longitude:position.coords.longitude}});
       },
       (error) =>console.log( error.message),
       {timeout: 30000,maximumAge:0},
     );
}




  receivelogindet=()=>{
    // alert(this.state.searchhome[this.state.activeIndex]);
       this.setState({bookobj:this.state.searchhome[this.state.activeIndex]},function(){
         this.setState({search_venue:true});
    })
  }
  booksuccess=(data,data1)=>{
    data.bookdetails=data1;
    this.setState({booksuccess:data});
  }
    _renderItem =({item, index})=> {
        return (
          <View style={{ width: "100%" }}>
            <View>
              <Text style={styles.txt1}>{item.trn_venue_name}</Text>
              <View style={styles.flex_row}>
                <View style={{ paddingTop: hp("1.5%"), flex: 0.4 }}>
                  <TouchableOpacity
                    onPress={() => this.loadCarousel(item.photos)}
                  >
                    {item.photos && item.photos.length > 0 &&
                      item.photos[0].venue_image_path && (
                        <Image
                          style={{ height: hp("12%"), width: "100%" }}
                          alt="No Image"
                          source={{
                            uri:
                              item.photos.length > 0 &&
                              item.photos[0].venue_image_path
                          }}
                        ></Image>
                      )}
                  </TouchableOpacity>
                  {!( item.photos &&
                    item.photos.length > 0 && item.photos[0].venue_image_path
                  ) && (
                    <Image
                      style={{ height: hp("12%"), width: "100%" }}
                      alt="No Image"
                      source={placeholder_img}
                    ></Image>
                  )}
                </View>
                <View style={{ backgroundColor: color.white, flex: 0.6 }}>
                  <View style={styles.txtcontainer1}>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between"
                      }}
                    >
                      <Text style={{ fontSize: hp("1.7%") }}>
                        {item.price.length > 0 &&
                          item.price[0].trn_venue_price_currency +
                            " " +
                            item.price[0].trn_venue_price_amt}
                      </Text>
                      <Text
                        style={{ fontSize: hp("1.7%"), color: color.orange }}
                      >
                        {item.Distance} | {item.Time}
                      </Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => this.showDescription(item)}
                    >
                      <Text
                        style={{ fontSize: hp("1.7%"), marginTop: 3 }}
                        numberOfLines={4}
                      >
                        {item.trn_venue_desc && item.trn_venue_desc}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        );
    }
    render(){
        return(
            <View style={styles.container}>
            {this.state.photovisible==true&&
            
            <LoadCarouselModal visible={this.state.photovisible} items={this.state.photoItems} closemodal={()=>this.setState({photovisible:null})} />
          }
          {this.state.showDetailDesc&&
            <VenueDetailDesc visible={this.state.venuede} venuedetails={this.state.showDetailDesc} closemodal={()=>this.setState({showDetailDesc:null})} />
          }
            {/*<View style={styles.container}>
                <View>
                
                    <ScrollView
                    contentContainerStyle={{
      flexGrow: 1,
      justifyContent: 'space-between'
  }}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled={true}
                        decelerationRate={0}
                        scrollEventThrottle={32}
                        nestedScrollEnabled={true}
                        automaticallyAdjustContentInsets={true}
                        ref = {re => this.scroll = re}>
                        {this.state.searchhome&&this.state.searchhome.map((item)=>{
                    return(
                         <View>     
                            <Text style={styles.txt1}>
                            {item.trn_venue_name}
                            
                            </Text>
                            <View style={styles.flex_row}>
                                <View style={{paddingTop:hp('1.5%'),width:wp('40%')}}>
                                     {(item.photos.length>0&&item.photos[0].venue_image_path)&&
                        <Image style={{height:hp('12%'),width:'100%'}} alt="No Image"
                            source= {{uri:item.photos.length>0&&item.photos[0].venue_image_path}}>       
                        </Image>}
                         {!(item.photos.length>0&&item.photos[0].venue_image_path)&&
                        <Image style={{height:hp('12%'),width:'100%'}} alt="No Image"
                            source= {placeholder_img}>       
                        </Image>
                      }
                                </View>
                                <View style={{backgroundColor:color.white,width:wp('60%')}}>
                                    <View style={styles.txtcontainer}>
                                        <Text style={{fontSize:hp('1.5%'),}}
                                        numberOfLines={5}>
                                        {item.availability.length>0&&item.availability[0].trn_venue_moredetails}
                                        </Text>
                                    </View>                           
                                </View>

                            </View>
                        </View>
                             )
                })}
                         
                    </ScrollView>
                       
                    
                     </View> 
                    <View style={{flexDirection:'row',position:'absolute', bottom:0,}}>
                        <View style={{width:wp('40%')}}></View>
                        <View style={{width:wp('60%'),flexDirection:'row',}}>
                          {this.state.searchhome.length>1&&
                            <View style={[{flex:1,paddingLeft:hp('1.5%'),flexDirection:'row'}]}>
                                <TouchableOpacity onPress={this.LeftArrow} style={styles.view22}>
                                    <Image backgroundColor={color.white} source={Left_arrow}  style={[styles.blue_arr,styles.view22]}>

                                    </Image>
                                </TouchableOpacity>
                                
                                <Text style={[{fontSize:hp('1.5%'),},styles._end,styles.view22]}>
                                        {this.state.activeIndex+1} of {this.state.searchhome.length}
                                </Text>
                                <TouchableOpacity onPress={this.RightArrow} style={styles.view22} >
                                    <Image backgroundColor={color.white} source={Right_arrow} style={[styles.blue_arr,]}>

                                    </Image>
                                </TouchableOpacity>
                            </View> 
                            }  
                            <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                <Button style={[styles.booK_nw_btn]}
                                >
                                    <Text style={[styles.booK_nw_txt]}>Book Now</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </View>*/}
                <ScrollView ref={(c) => {this.scrollimage = c}}  onScroll={({nativeEvent}) => {
      if (isCloseToRight(nativeEvent)) {
        // enableSomeButton();
        this.props.sendScrollEnd&&this.props.sendScrollEnd(true);
        // setTimeout((obj)=>{
        // self.scrollimage.scrollToEnd();
        // },200)
        // alert("reached end")
      }
    }} directionalLockEnabled={false} horizontal={true} pagingEnabled={false}  showsHorizontalScrollIndicator={false}>
                {this.state.searchhome.length>0&&this.state.searchhome.map((item,index)=>{
                  return(
                      <TouchableOpacity
                         style={styles.square}
                         onPress={() =>
                          this.BookNow(item)
                         }
                         key={index}
                       >
                         <VenueCardComp item={item}/>
                       </TouchableOpacity>
                    )
                })}
                {this.props.children}
                </ScrollView>
           
            {/*<View style={{flexDirection:'row',position:'absolute', bottom:0,}}>
                        <View style={{width:wp('40%')}}></View>
                        <View style={{width:wp('60%'),flexDirection:'row',}}>
                          {this.state.searchhome.length>1&&
                            <View style={[{flex:1,paddingLeft:hp('1.5%'),flexDirection:'row'}]}>
                                <TouchableOpacity disabled={this.state.activeIndex==0} onPress={() =>this.LeftArrow()} style={styles.view22}>
                                    <Image backgroundColor={color.white} source={Left_arrow}  style={[styles.blue_arr,styles.view22]}>

                                    </Image>
                                </TouchableOpacity>
                                
                                <Text style={[{fontSize:hp('1.5%'),},styles._end,styles.view22]}>
                                        {this.state.activeIndex+1} of {this.state.searchhome.length}
                                </Text>
                                <TouchableOpacity disabled={this.state.activeIndex==this.state.searchhome.length-1} onPress={() => this.RightArrow()} style={styles.view22} >
                                    <Image backgroundColor={color.white} source={Right_arrow} style={[styles.blue_arr,]}>

                                    </Image>
                                </TouchableOpacity>
                            </View> 
                            }  
                            <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                <Button style={[styles.booK_nw_btn]} onPress={()=>this.BookNow()}>
                                    <Text style={[styles.booK_nw_txt]}>More Details</Text>
                                </Button>
                            </View>
                        
            </View>
            </View>*/}
            
            {this.state.search_venue&&this.state.search_venue==true&&
                  <View>
                 
              <ModalComp  header={this.state.headervisible}   borderclr={color.grey} borderwdth={2}
          Nooverlay={true} titlecolor={color.orange} closemodal={()=>this.closemodal()} modaltitle={this.state.title} modalsubtitle={this.state.modalsubtitle} visible={true} >
          {!this.state.booksuccess&&
                  <Book_your_venue booksuccess={(data,data1)=>this.booksuccess(data,data1)} cancelBooking={()=>this.setState({search_venue:null})} bookdata={this.state.bookobj&&this.state.bookobj?this.state.bookobj:null} />
              }
              {this.state.booksuccess&&
                <SuccessPage closemodal={()=>this.closemodal()} booksuccess={this.state.booksuccess} />
              }
                  </ModalComp>
                 </View>
                 }

            {this.state.visible&&this.state.visible==true&&
        <TabLoginProcess sendlogindet={()=>this.receivelogindet()} type='login' visible={true} tabAction={this.tabAction} closetabmodal={()=>this.setState({visible:null})}/>
      }
      
       
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        padding:hp('1.5%'),
        marginBottom:hp('1.5%'),
        backgroundColor:color.white
    },
    txt1:{
        fontSize:hp('2%')
    },
    flex_row:{
        flexDirection:'row',
        height:hp('15%'),
        flex:1
    },
    txtcontainer:{
        flex:.9,
        // paddingTop:hp('1.5%'),
        paddingLeft:hp('1.5%'),
        paddingBottom:hp('1.5%')
    },
    txtcontainer1:{
        flex:1,
        width:'100%',
        paddingTop:hp('1.5%'),
        paddingLeft:hp('1.5%'),
        // paddingBottom:hp('1.5%')
    },
    blue_arr:{
        height:hp('2.5%'),
        width:hp('2.5%'),
        justifyContent:'center',alignSelf:'center',alignContent:'center',alignItems:'center'
    },
    booK_nw_txt:{
        color:color.white,
        fontSize:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8
    },
    _end:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignSelf:'flex-end',
        alignContent:'flex-end',
    },
    booK_nw_txt:{
        color:color.white,
        fontSize:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8
    },
    view22:{
        justifyContent:'center',alignSelf:'center',alignContent:'center',alignItems:'center'
    },
    booK_nw_btn:{
        backgroundColor:color.blue,
        height:hp('3%'),
        marginRight:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8,
        position:'absolute',
        bottom:0,
        right:0,
        
    },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
})