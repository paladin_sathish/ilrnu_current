"use strict";

import React, { Component } from "react";
import CircleText from "../components/circlecomp";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import { StyleSheet, View, Text } from "react-native";
import links from "../Helpers/config";

var concatData = { id: 12, disable: true };
class TraineeType extends Component {
  constructor(props) {
    super(props);

    this.state = { venuedata: [], activeobj: props.venuedetails };
    // alert(datas.length%2);
  }
  componentWillReceiveProps(props) {
    if (props.venuedetails) {
      this.setState({ activeobj: props.venuedetails });
    }
  }
  componentWillMount() {
    this.props.headertext &&
      this.props.headertext({
        headertext: (
          <Text
            style={{
              fontSize: hp("1.7%"),
              fontWeight: "bold",
              color: color.blue
            }}
          >
            Training Category
          </Text>
        )
      });
    this.props.labeltext &&
      this.props.labeltext({
        labeltext: (
          <Text>
            Please choose the{" "}
            <Text style={{ color: color.orange, fontWeight: "bold" }}>
              {" "}
              Training Category{" "}
            </Text>{" "}
          </Text>
        )
      });
  }
  getVenueType = () => {
    fetch(links.APIURL + "getTrainingCategoryList/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        user_cat_id:
          this.props.loginDetails && this.props.loginDetails.user_cat_id
      })
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == 0) {
          console.log("first tab", responseJson.data);
          this.setState({ venuedata: responseJson.data });
        }
      });
  };
  componentDidMount() {
    this.getVenueType();
  }
  changeVenueType = (data, key) => {
    // var venuedata=this.state.venuedata;
    // venuedata=data.id;
    console.log('traineetype',data)

    this.setState({ activeobj: data });
    this.props.sendvenuetypedata(data);
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          flexWrap: "wrap",
          justifyContent: "space-between",
          padding: 12
        }}
      >
        {this.state.venuedata &&
          this.state.venuedata.length > 0 &&
          this.state.venuedata.concat(concatData).map((obj, key) => {
            return (
              <View key={key} style={[styles.circleStyle]}>
                {!obj.disable && (
                  <CircleText
                    image={obj.CategoryIcon}
                    sendText={data => {
                      this.changeVenueType(data, key);
                    }}
                    style={[styles.flexWidth]}
                    width={12}
                    height={12}
                    text={obj.CategoryName}
                    data={obj}
                    active={
                      this.state.activeobj &&
                      this.state.activeobj.CategoryId == obj.CategoryId
                    }
                  />
                )}
              </View>
            );
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  circleStyle: {
    width: wp("30%")
  },
  activename: {}
});

export default TraineeType;
