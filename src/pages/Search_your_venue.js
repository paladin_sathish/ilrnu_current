import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList,SafeAreaView,TextInput,BackHandler,ScrollView} from 'react-native';
import {Right,Top,Icon, Button,ListItem,Content,Container,Footer,CheckBox} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Actions,Router,Scene} from 'react-native-router-flux'
import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import Search_box from '../components/searchbox'
import SearchDropdown from '../components/SearchableDropDown';
import Text_box from './text_box'
import Location from '../images/location.png'
import search from '../images/Search.png'
// import Check_Box from './checkbox'
import Calender from '../images/svg/cal2.png'
import DatePicker from 'react-native-datepicker'
import Drop_down from '../components/drpdown_container'
import Line from '../components/Line'
import ModalComp from '../components/Modal';
import links from '../Helpers/config';
import Geolocation from 'react-native-geolocation-service';
var _location=['Old Trafford','San Siro','OlympiaStadion']

export default class Search_your_venue extends Component{
    constructor(props) {
        super(props);
        this.state= {chosenlocation:{latitude:0,longitude:0},tempdata:[],amenitiesDropdownData:[],checked:false,datePickerIsOpen:false,
          
           from:'',to:'',headervisible:false,modalsubtitle:null,title:null,visible:true,loadtoast:null,layoutdropdown:null,amenitiesdropdown:null,searchlist:[],searchvisible:false,searchdata:null,searchamenities:false,searchamenitieslist:null,searchamenitieslist:[],pagex:0,pagey:0,
           searchobj:{
  "venue_cat_id":"",
  "where":"",
  "fromDate":"",
  "toDate":"",
  "nearme":false,
  "priceFrom":"",
  "priceTo":"",
  "amenities":[
    ]
}
         
        }       
    }
     handleSelectItem(item, index) {
    const {onDropdownClose} = this.props;
    onDropdownClose();
    console.log(item);
  }
     setDate=(date,key)=>{
        // alert(date);
         var searchobj=this.state.searchobj;
        if(key=='from'){
         searchobj.fromDate=date;
        }else if(key=='to'){
         searchobj.toDate=date;
        }
        this.setState({searchobj});
    // var jsondate=JSON.stringify(new Date(date));
        this.setState({[key]:date});
        var data=this.state;
        data[key]=date;
        this.props[key](data);
      }
    
    modalclose=()=>{
        this.setState({visible:true});

    }
    closemodal=()=>{
        if(this.props.closetabmodal){
            this.props.closetabmodal();
        }
    }
    onLayout=(e)=>{
this.setState({layoutdropdown:
e.nativeEvent.layout

})
   }
    onLayout1=(e)=>{
this.setState({amenitiesdropdown:
e.nativeEvent.layout

})
if (this.marker) {
      this.marker.measure((x, y, width, height, pageX, pageY) => {
                // console.log(x, y, width, height, pageX, pageY);
this.setState({pagex:pageX,pagey:pageY});
       })
    }

   }
   dynamicChange=(data,key)=>{
     var searchobj=this.state.searchobj;
     if(key=="priceFrom"){
       searchobj.priceFrom=data;
       searchobj.priceTo=data;
     }else{
     searchobj[key]=data;
     }
     this.setState({searchobj});
   }
   searchInput=(data)=>{
     this.setState({searchdata:data});
    // alert(data.length);
    // this.setState({searchvisible:true});
this.setState({searchvisible:false});
  var searchobj=this.state.searchobj;
            searchobj.venue_cat_id="";
            this.setState({searchobj});
    if(data.length==0){
this.setState({searchlist:null})
 var searchobj=this.state.searchobj;
            searchobj.venue_cat_id="";
            this.setState({searchobj});
    }else{
  fetch(links.APIURL+'searchYourVenue', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
        'catName':data
        }),
    }).then((response)=>response.json())
        .then((responsejson)=>{
            this.setState({searchvisible:true});
            this.setState({searchlist:responsejson.data});
        })
    }

   }
   selectSearchData=(item)=>{
    this.setState({searchdata:item.venue_cat_name});
    this.setState({searchvisible:false});
    this.setState({searchlist:null})
    var searchobj=this.state.searchobj;
            searchobj.venue_cat_id=item.venue_cat_id;
            this.setState({searchobj});

   }
selectAmSearchData=(item)=>{
 this.setState({searchamenitiesdata:item.amenities_name});
 this.setState({searchamenities:false});
this.setState({searchamenitieslist:null})
}
   searchAmenities=(data)=>{
    // this.setState({searchamenities:true});
    this.setState({searchamenitiesdata:data});
    if(data.length==0){
this.setState({searchamenities:false});
this.setState({searchamenitieslist:null})
    }else{
      fetch(links.APIURL+'searchAmenities', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
        'amenitiesName':data
        }),
    }).then((response)=>response.json())
        .then((responsejson)=>{
          // alert(JSON.stringify(responsejson));
this.setState({searchamenities:true});
this.setState({searchamenitieslist:responsejson.data})
        })
    }
   }
   searchFunction=(data)=>{
     data.amenities=this.state.amenitiesDropdownData.map((obj)=>obj.id);
data.lat=this.state.chosenlocation.latitude;
data.long=this.state.chosenlocation.longitude;
data.nearme=this.state.checked;
     // alert(JSON.stringify(data));
     // alert(JSON.stringify(data));
       fetch(links.APIURL+'formSearch', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then((response)=>response.json())
        .then((responsejson)=>{
            console.log(responsejson);
            // alert(JSON.stringify(responsejson));
            if(this.props.sendsearchdata){
                this.props.sendsearchdata(responsejson);
            }
            // Actions.Venue_search({data:responsejson});

        }).catch((err)=>{
            console.log(err);
        })
      

   }
   dateClick=()=>{
this.setState({
      datePickerIsOpen: !this.state.datePickerIsOpen,
    });   }
componentWillMount(){
  Geolocation.getCurrentPosition(
       (position) => {
         
         // alert(JSON.stringify(position));
         this.setState({chosenlocation:{latitude:position.coords.latitude,longitude:position.coords.longitude}});
         //this.getAddressByLatLng(position.coords);
       },
       
       (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
       {timeout: 10000,maximumAge:0},
     );
}
            componentDidMount() {
              // alert("")
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton=()=> {
      Actions.pop();
        // alert("back button disabled");
        // ToastAndroid.show('Back button is disabled', ToastAndroid.SHORT);
        return true;
    }
    checkFunction=()=>{
      this.setState({checked:!this.state.checked})
    }
    clearAmeneties=()=>{
      this.setState({amenitiesDropdownData:[]});
    }
    selectedAmeneties=(data,items)=>{

      // console.log("ameObj",  data);
      // alert(JSON.stringify(data));
var amenitiesDropdownData=this.state.amenitiesDropdownData;
var tempdata=this.state.tempdata;
amenitiesDropdownData=[];
for(var i in data){
  if(data[i]!='null'){
  if(tempdata.length==0){
  tempdata.push(data[i]);
  }else{
    var findIndex=tempdata.findIndex((obj)=>obj.id==data[i].id);
    if(findIndex==-1){
      tempdata.push(data[i]);
    }
  }
  var getname=tempdata.filter((obj)=>obj.id==data[i].id);
  if(getname.length>0&&getname[0].name!=""){
    amenitiesDropdownData.push({id:data[i].id,name:getname[0].name});
  }
  }
}
  // alert(JSON.stringify(amenitiesDropdownData))
// alert(JSON.stringify(amenitiesDropdownData));
// if(amenitiesDropdownData.length==0){
// amenitiesDropdownData.push(data);
// }else{

  // var findIndex=amenitiesDropdownData.findIndex((obj)=>obj.id==data.id);
  // if(findIndex==-1){
  //  amenitiesDropdownData.push(data);
  // }else{

  // }
//   for(var i in items){
//   var findIndex=amenitiesDropdownData.findIndex((obj)=>obj.id!=items[i]);
//   if(findIndex!=-1){
//     amenitiesDropdownData.splice(findIndex,1);
//   }

// }
this.setState({tempdata});
this.setState({amenitiesDropdownData});
    }
    remove_Amn_Itmes=(id)=>{
      var amenitiesDropdownData=this.state.amenitiesDropdownData;
      var findIndex=amenitiesDropdownData.findIndex((obj)=>obj.id==id);
      if(findIndex!=-1){
        amenitiesDropdownData.splice(findIndex,1);
      }
      this.setState({amenitiesDropdownData});

    }
    render(){
 
 const {scrollToInput, onDropdownClose, onDropdownShow} = this.props;
        return(
            <Container style={{height:hp('86%')}}>
            <Content style={{padding:15}}>
           
           
                <View style={{flex:1}}>    
                    <Text style={styles.text1}>Search your Venue</Text>
                    <View style={styles.view1}></View>
                      <View style={styles.searchSection} onLayout={this.onLayout}>
    
    <TextInput
        style={styles.input}
        placeholder="Search.."
        onChangeText={(data) => this.searchInput(data)}
        underlineColorAndroid="transparent"
        value={this.state.searchdata&&this.state.searchdata}
    />

    
   

</View>

   {this.state.searchvisible==true &&
<View style={{borderWidth:1,borderColor:color.ash6,position:'absolute',backgroundColor:color.white,top:this.state.layoutdropdown?this.state.layoutdropdown.y+this.state.layoutdropdown.height:0,width:this.state.layoutdropdown?this.state.layoutdropdown.width:0,zIndex:9999}}>
{this.state.searchlist&&this.state.searchlist.length>0&&this.state.searchlist.map((item)=>{
    return(
<TouchableOpacity onPress={()=>this.selectSearchData(item)} style={{height:this.state.layoutdropdown?this.state.layoutdropdown.height:0,flexDirection:'row',alignItems:'center'}}><Text style={{padding:5,}}>{item.venue_cat_name}</Text></TouchableOpacity>
)
})}
</View>
}
        


 <View style={styles.view1}></View>
                    <View style={styles.view2}>
                        <View style={[styles.view3,{flexDirection:'row',marginLeft:-10}]}>
                        {/*<CheckBox checked={"one"}
            style={{ marginRight: 20 }}
            />*/}<CheckBox onPress={()=>this.checkFunction()} checked={this.state.checked} style={{marginRight:10,borderWidth:1,borderColor:color.ash4}} /><Text style={{marginLeft:5,marginTop:-1}}>Near me</Text>
                           {/*<Check_Box label={'Near Me'} active={true} ></Check_Box>*/}
                        </View>
                        <View style={styles.view4}>
                           {/* <Text style={styles.blue_txt}>Advance Search</Text>*/}
                        </View>
                    </View>  
                  
                    <View style={styles.view5}>
                        <View style={styles.flex_row}>
                            <View style={styles.view6}><Text>Where</Text></View>
                            <View style={styles.viewwhere}><Text_box changeText={(data)=>this.dynamicChange(data,'where')} _height={hp('5%')}></Text_box></View>
                            <View style={styles.view8}><Image source={Location} style={styles.image_style1}></Image></View>
                        </View>

                        <View style={[styles.flex_row,styles.view1]}>
                                <View style={{flex:.35}}><Text>From</Text></View>
                                <View style={{flex:.55,justifyContent:'center',alignItems:'center'}}>
                                    <DatePicker
                                       style={{width: '100%',height:hp('5%'),textAlign:'left',fontSize:hp('1.5%')}}
                                       date={this.state.from}
                                       placeholder="Select date"
                                       format="YYYY-MM-DD"
                                       confirmBtnText="Confirm"
                                       cancelBtnText="Cancel"
                                       showIcon={false}
                                       customStyles={{
                                           dateIcon: {
                                             position: 'absolute',
                                             right: 0,
                                             top: 0
                                           },
                                           dateInput: {
                                             borderWidth:1,
                                             marginLeft: 0,
                                             paddingLeft:8,
                                             textAlign:'left',
                                             alignItems:'flex-start',height:hp('5%'),fontSize:hp('1.5%')
                                           }
                                         
                                         }}
                                         onDateChange={(date) => this.setDate(date,'from')}
                                         ref={(ref)=>this.datePickerRef=ref}
                                   />             
                                    <TouchableOpacity onPress={() => this.datePickerRef.onPressDate()} style={styles.dateImgConta}>
                                            <Image source={Calender}  style={{width:hp('2%'),height:hp('2.2%')}}
                                            />
                                    </TouchableOpacity>           
                                         
                                </View>
                            </View>    
                            <View style={[styles.flex_row,styles.view1]}>
                                <View style={{flex:.35}}><Text>To</Text></View>
                                <View style={{flex:.55,justifyContent:'center',alignItems:'center'}}>
                                    <DatePicker
                                       style={{width: '100%',height:hp('5%'),textAlign:'left',fontSize:hp('1.5%')}}
                                       date={this.state.to}
                                       placeholder="Select date"
                                       format="YYYY-MM-DD"
                                       confirmBtnText="Confirm"
                                       cancelBtnText="Cancel"
                                       showIcon={false}
                                       customStyles={{
                                           dateIcon: {
                                             position: 'absolute',
                                             right: 0,
                                             top: 0
                                           },
                                           dateInput: {
                                             borderWidth:1,
                                             marginLeft: 0,
                                             paddingLeft:8,
                                             textAlign:'left',
                                             alignItems:'flex-start',height:hp('5%'),fontSize:hp('1.5%')
                                           }
                                         
                                         }}
                                         onDateChange={(date) => this.setDate(date,'to')}
                                         ref={(ref)=>this.todatePickerRef=ref}
                                   />             
                                    <TouchableOpacity onPress={() => this.todatePickerRef.onPressDate()} style={styles.dateImgConta}>
                                            <Image source={Calender} style={{width:hp('2%'),height:hp('2.2%')}}
                                            />
                                    </TouchableOpacity>           
                                         
                                </View>
                            </View>    
                        <View style={[styles.flex_row,styles.view1]}>
                            <View style={styles.view6}><Text>Price Range</Text></View>
                            <View style={{flex:.35}}><Text_box changeText={(data)=>this.dynamicChange(data,'priceFrom')}  keyboardType={'numeric'} _height={hp('5%')}/></View>
                            <View style={{flex:.3}}></View>
                        </View>
                        <View style={[styles.flex_row,styles.view1,{alignItems:'center'}]}>
                              <SearchDropdown amenitiesDropdownData={this.state.amenitiesDropdownData}  clearAmeneties={()=>this.clearAmeneties()} selectedDataArray={(data,allitems)=>this.selectedAmeneties(data,allitems)}/>

                             
                        </View>
                        <ScrollView horizontal style={{flexDirection:'row'}}>
                         {this.state.amenitiesDropdownData.map((obj)=>{
                             return(
                               <View style={styles.chips}>
                               <Text>{obj.name}</Text>
                                <TouchableOpacity style={{marginLeft:5,opacity:0.5}} onPress={()=>this.remove_Amn_Itmes(obj.id)} > 
                               <Icon style={{color:color.top_red}} name="times-circle-o" type="FontAwesome"/>
                               </TouchableOpacity>
                               </View>
                               )
                           })}
                        </ScrollView>
                           
                        <View style={[styles.flex_row,styles.view1]}>
                            <View style={styles.view6}><Text>Capacity</Text></View>
                            <View style={{flex:.3}}>
                                <Text_box  keyboardType={'numeric'}   _height={hp('5%')}></Text_box>
                            </View>
                            <View style={{flex:.35}}>
                                
                            </View>
                        </View>
                    </View>
                </View>
                
                </Content>
                <Footer style={{backgroundColor:'transparent',paddingRight:15,paddingLeft:15,paddingBottom:hp('10%')}}>
                <View style={{flex:1}}>
                <View style={styles.view11}>
                    <View style={styles.view12}>
                        <Button large style={styles.cancel_btn} onPress={()=>this.props.cancelSearch()}>
                            <Text style={styles.cancel_txt}> CANCEL </Text>
                        </Button>
                    </View>
                    <View style={styles.view13}>
                        <Button disabled={this.state.searchobj.venue_cat_id.length==0}  onPress={()=>this.searchFunction(this.state.searchobj)} large style={[styles.book_nw_btn,{backgroundColor:this.state.searchobj.venue_cat_id.length==0?color.ash6:color.blue}]}>
                            <Text style={styles.cancel_txt}> SEARCH </Text>
                        </Button>
                    </View>
                </View>
                </View> 
                </Footer>
                {this.state.searchamenities==true &&
<View style={{borderWidth:1,borderColor:color.ash6,position:'absolute',backgroundColor:color.white,top:((this.state.pagey)-hp('14%')+(this.state.amenitiesdropdown.height)),width:this.state.amenitiesdropdown?this.state.amenitiesdropdown.width:0,zIndex:9999999,left:this.state.pagex-8}}>
{this.state.searchamenitieslist&&this.state.searchamenitieslist.length>0&&this.state.searchamenitieslist.map((item)=>{
    return(
<TouchableOpacity onPress={()=>this.selectAmSearchData(item)} style={{height:this.state.amenitiesdropdown?this.state.amenitiesdropdown.height:0,flexDirection:'row',alignItems:'center'}}><Text style={{padding:5,}}>{item.amenities_name}</Text></TouchableOpacity>
)
})}
</View>
}
                </Container>
        )
    }
} 
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white,
        flex:1,
        padding:hp('2%')
    },
    text1:{
        color:color.top_red,
        fontSize:hp('2.5%')
    },
    blue_txt:{
        color:color.blue,
        fontSize:hp('1.8')
    },
    view1:{
        marginTop:hp('2%')
    },
    view2:{
        flexDirection:'row',
        paddingTop:3,
        paddingBottom:3
    },
    view3:{
        flex:1,
        justifyContent:'flex-start'
    },
    view4:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end'
    },
    view5:{
        marginTop:hp('2%')
    },
    flex_row:{
        flexDirection:'row'
    },
    view6:{
        flex:.35
    },
    viewwhere:{
        flex:.55
    },
    view7:{
        flex:.55,
        borderColor:color.ash6,
        borderWidth:1
    },
    view8:{
        flex:.1
    },
    image_style1:{
        padding: 5,
        margin: 5,
        height: hp('2.7%'),
        width: hp('2.7%'),
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    view11:{
        flexDirection:'row',
        paddingTop:15,
        paddingBottom:15
    },
    view12:{
        flex:1,
        justifyContent:'flex-start',
        flexDirection:'row'
    },
    view13:{
        flex:1,
        justifyContent:'flex-end',
        flexDirection:'row'
    },
    cancel_txt:{
        fontSize:hp('2.3%'),
        color:color.white
    },
    cancel_btn:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.orange,
        height:hp('6%'),
        borderRadius:5,

        // width:hp('50%')
    },
    book_nw_btn:{
        backgroundColor:color.blue,
        paddingLeft:8,
        paddingRight:8,
        height:hp('6%'),
        borderRadius:5
        // width:hp('50%')
    },
    dateContainer:{flex:1,flexDirection:'row',borderWidth:1,borderColor:color.ash6,width:'100%',height:hp('4%'),
    alignItems:'center'},
    dateImgConta:{height:'100%',position:'absolute',right:6,margin:'auto',alignItems:'center',justifyContent:'center',flexDirection:'row'},
    view9:{
        flexDirection:'row',
      
    },
    searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth:1,
    borderColor:color.ash6,
    height:hp('5.5%'),
},
searchIcon: {
    padding: 10,

},
input: {
    flex: 1,
    padding:0,
    backgroundColor: '#fff',
    color: '#424242',
    paddingLeft:5
},
chips:{
  alignItems:'center',
  flexDirection:'row',
alignSelf: 'flex-start',
padding:8,
marginRight:12,
paddingTop:3,
borderColor:color.top_red,
paddingBottom:3,
borderRadius:10,
borderWidth:0.5
}

})