import React, {Component} from 'react';
import ReactNative,{NativeModules,TextInput,Platform, StyleSheet, View,Alert,StatusBar,Modal,Image,TouchableHighlight,TouchableOpacity,Picker,Keyboard} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Left,List,Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox, Text,Accordion } from 'native-base';
import color from '../Helpers/color';
import { Actions,Router,Scene } from 'react-native-router-flux';
import Checkbox from "../components/checkbox";
import AccordionView from '../components/accordion';
import PopupMenu from '../components/popupmenu';
import Dropdown_conatiner from '../components/drpdown_container'
const dataArray = [
  { title: "USD", content:'' }
];
class SelectedArray
{
    constructor()
    {
        this.selectedItemsArray = [];
    }

    setItem(option)
    {
        this.selectedItemsArray.push(option);
    }

    getArray()
    {
    	return this.selectedItemsArray;
    }
}
var selectnewArray=new SelectedArray();
this.feedPost={};
var currency=[{id:1,name:'USD'},{id:2,name:'IND'},{id:3,name:'MLR'}]
var datearray=[{id:1,name:'Day'},{id:2,name:'Week'},{id:3,name:'Hour'},{id:4,name:'Month'}]
 
export default class TraineeChooseVenue extends React.Component{

	constructor(props){
		super(props);
		this.state={AmentiesObj:null,dropdownArray:[],selectedUpdate:'',keyboardshow:false,selectedkey:0,hourtext:'Hour',newmainheight:0,dropdownstate:null,selectedArrayRef:new SelectedArray(),selected:'key0',mainheight:0,dropdown:null,dataArray:[{key:0,statekey:false,hours:'Hour',currency:'USD',amt:'0',item:new SelectedArray(),itemname:'Locker'},{key:1,statekey:false,hours:'Hour',currency:'USD',amt:'0',item:new SelectedArray(),itemname:'Lift'},{key:2,statekey:false,hours:'Hour',currency:'USD',amt:'0',item:new SelectedArray(),itemname:'AC'},{key:3,statekey:false,hours:'Hour',currency:'USD',amt:'0',item:new SelectedArray(),itemname:'Power Backup'},{key:4,statekey:false,hours:'Hour',currency:'USD',amt:'0',item:new SelectedArray(),itemname:'Reception'}]}
		// alert(props.dataObj.hasOwnProperty('amenetiesList'));
		this.handleLayoutChange = this.handleLayoutChange.bind(this);
		this.handleLayoutMainChange = this.handleLayoutMainChange.bind(this);
		// if(props.dataObj.hasOwnProperty('amenetiesList')==true){
		// 			for(var y in this.state.dataArray){
		// 				var index=-1;
		// 	for(var i=0;i<props.dataObj.amenetiesList.length;i++){

		// 				if(props.dataObj.amenetiesList[i].key==this.state.dataArray[y].key){
		// 					index=i;
		// 					i=1001;
		// 				}
		// 			}
		// 			if(index!=-1){
		// 				var dataArray=this.state.dataArray;
		// 				dataArray[y].statekey=true;
		// 				dataArray[y]=props.dataObj.amenetiesList[index];
		// 				this.state['dataArray']=dataArray;
		// 			}
		// 		}
		// 	}
		}
	componentWillReceiveProps=(props)=>{
		// alert(props.dataObj);
this.setState({AmentiesObj:props.dataObj})
	}
	
	submitlogin=()=>{
		this.props.submitlogin();
	}
	loadsignup=()=>{
		// Actions.signup();
		var obj={'type':'signup'};
			// alert(JSON.stringify(obj));
		if(this.props.loadsignup){
		this.props.loadsignup(obj)
		}
	}
	  _keyboardDidShow=()=> {
    // alert('Keyboard Shown');
    this.setState({keyboardshow:true})
    		this.setState({dropdownstate:null});
    	this.setState({dropdown:null});
  }

  _keyboardDidHide=()=> {
    	this.setState({keyboardshow:false})
  }

	

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

	renderDropdown=()=>{
		return(
			<View style={{position:'absolute',top:this.state.dropdown.x,left:this.state.dropdown.x}}><Text>123456</Text></View>
			)
	}
	loaddropdown=()=>{
		this.setState({dropdownstate:true});
	}
handleLayoutChange=()=> {

   
  }
  selectedData=(key,data,obj)=>{
  	var AmentiesObj=this.state.AmentiesObj;
// alert(JSON.stringify(obj));
  	if(data.unselect){

  		AmentiesObj.amenities_array[key].statekey=false;

  	}else{
  		if(obj.amenities_det_datatype1=='dropdown'){
  			AmentiesObj.amenities_array[key].amenities_det_datavalued1=AmentiesObj.amenities_array[key].amenities_det_datavalued1?AmentiesObj.amenities_array[key].amenities_det_datavalued1:obj.amenities_det_datavalue1.split(',').length>0?obj.amenities_det_datavalue1.split(',')[0]:'';
  		}
  		if(obj.amenities_det_datatype2=='dropdown'){
  			AmentiesObj.amenities_array[key].amenities_det_datavalued2=AmentiesObj.amenities_array[key].amenities_det_datavalued2?AmentiesObj.amenities_array[key].amenities_det_datavalued2:obj.amenities_det_datavalue2.split(',').length>0?obj.amenities_det_datavalue2.split(',')[0]:'';
  		}
  		if(obj.amenities_det_datatype3=='dropdown'){
  			AmentiesObj.amenities_array[key].amenities_det_datavalued3=AmentiesObj.amenities_array[key].amenities_det_datavalued3?AmentiesObj.amenities_array[key].amenities_det_datavalued3:obj.amenities_det_datavalue3.split(',').length>0?obj.amenities_det_datavalue3.split(',')[0]:'';
  		}
  		AmentiesObj.amenities_array[key].statekey=true;
  	}
  this.setState({AmentiesObj});
  }
  handleLayoutMainChange=()=> {
    this.mainheight.measure( (fx, fy, width, height, px, py) => {
    	// this.setState({dropdownX})
    	this.setState({newmainheight:height});
    	// var dropdown=this.state.dropdown;
    	// dropdown.x=px;
    	// dropdown.y=py;
    	// dropdown.height=height;
    	// this.setState({dropdown});
    })
  }
  changeData=(data)=>{
  	var dataArray=this.state.dataArray;
  	dataArray[this.state.selectedkey][this.state.selectedUpdate]=data.name;
  	this.setState({dataArray});
  	// this.setState({hourtext:data.name});
  	this.setState({dropdownstate:null});
    	this.setState({dropdown:null});
  }
  opendropdown=(data,key,updatekey)=>{
  	Keyboard.dismiss();
  	this.setState({selectedkey:key});
  	this.setState({selectedUpdate:updatekey});

  	if(this.state.dropdownstate==null){
var self=this;
  		setTimeout(()=>{


  	 self[data].measure(( fx, fy,width, height, px, py) => {
      // alert(py);
      	var dropdown={};
    	dropdown.x=px;
    	dropdown.y=py;
    	dropdown.height=height;
    	dropdown.width=width;    
    	self.setState({dropdownstate:true});
    	self.setState({dropdown});
    	})
  	 },self.state.keyboardshow==true?200:0)
  	}else{

    	this.setState({dropdownstate:null});
    	this.setState({dropdown:null});

  	}
  	
  }

  changeAmentiesData=(data,key,statekey,nextstate)=>{
  	// console.log(objkey);

  	var dataArray=this.state.dataArray;
  	dataArray[key][statekey]=data;
  	if(nextstate){
  	dataArray[key][nextstate]=data.name;
  }
  	this.setState({dataArray});

  }
  submitAmeneteies=()=>{
  	var countArray=this.state.AmentiesObj.amenities_array.filter((obj)=>obj.statekey==true);
  	// alert(JSON.stringify(countArray));
// alert(JSON.stringify(this.state.AmentiesObj.amenities_array));
// alert(dataArray.length);
// if(countArray.length==0){
this.props.sendAmentiesList&&this.props.sendAmentiesList(this.state.AmentiesObj);	
// }else{
	// alert("Amenities Value should not empty");
// }
// var AmentiesObj=this.state.AmentiesObj;
// AmentiesObj['amenetiesList']=dataArray;
// alert(this.state.AmentiesObj.amenities_array.length);
// var 
// var AmentiesObj=this.state.AmentiesObj;
// AmentiesObj.activelist=
  	// alert(JSON.stringify(this.props.dataObj));

  }
  changeTextInput=(value,valuedata,key)=>{
  	var AmentiesObj=this.state.AmentiesObj;
  	AmentiesObj.amenities_array[key][valuedata]=value;
  	this.setState({AmentiesObj},function(){
  		// alert(JSON.stringify(AmentiesObj));
  	});


  }  

  submitPressed=()=>{
      this.props.submitAction()
  }

  openNewVenue=()=>{
      this.props.newVenuePressed()
  }
  sendDropdownData=(data,dropKey,key)=>{
  	var AmentiesObj=this.state.AmentiesObj;
  	AmentiesObj.amenities_array[key][dropKey]=data;
  	this.setState({AmentiesObj},function(){
  		// alert(JSON.stringify(AmentiesObj));
  	});
  }
renderInput=(data,value,key,typedata,valuedata,dropKey,amnArraykey,dropvalue)=>{
	if(data=='number' || data=='text'){
return(
<View style={{width:hp('8%'),borderWidth:0.8,borderColor:color.black1,alignSelf:'stretch',alignItems:'center',height:41.5}}>
      <TextInput onChangeText={(value)=>this.changeTextInput(value,valuedata,amnArraykey)} value={value} keyboardType={data=='number'?"number-pad":''} style={{width:hp('8%'),padding:0,height:'100%',paddingLeft:4}}/>
      </View>
	)
	}else if(data=='dropdown'){
		// var dropdownData=[];
		// value.split(',').map((obj,key)=>{
		// 			dropdownData.push({id:key+1,name:obj})
		// 	})
		return(
		 <View style={{width:'auto',borderWidth:0.8,borderColor:color.black1,alignSelf:'flex-start',marginLeft:5,marginBottom:5}}>
      <Dropdown_conatiner  sendDropdownData={(data)=>this.sendDropdownData(data,dropKey,amnArraykey)}  font={hp('1.8%')} border={true}  drop_items={'Do'}  values={value.split(',')} value={dropvalue?dropvalue:value.split(',')[0]}/>
      </View>
)
	}else{
		return null;
	}
}

  receiveDropdown=(data,index,keyvalue)=>{
  	var AmentiesObj=this.state.AmentiesObj;
  	AmentiesObj.amenities_array[index][keyvalue]=data;
  	this.setState({AmentiesObj});
  }
render(){
	return (
    <Content>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => this.setState({ dropdown: null, dropdownstate: null })}
      >
        <View
          onLayout={event => {
            this.handleLayoutMainChange(event);
          }}
          ref={view => {
            this.mainheight = view;
          }}
        >
          {this.state.dropdown && (
            <CustomDropDown
              changeData={this.changeData}
              bottom={true}
              data={
                this.state.selectedUpdate == "currency" ? currency : datearray
              }
              dropdown={this.state.dropdown}
              mainheight={this.state.newmainheight}
            />
          )}
          <View
            style={{
              alignItems: "center",
              borderBottomWidth: 0.5,
              borderColor: color.black1,
              padding: 12
            }}
          >
            <View style={styles.circle}>
              <View style={styles.innercircle}>
                <Image
                  tintColor="white"
                  source={{
                    uri: this.props.dataObj
                      ? this.props.dataObj.amenities_icon
                      : null
                  }}
                  style={{
                    position: "absolute",
                    width: "60%",
                    height: "60%",
                    margin: "auto"
                  }}
                />
              </View>
            </View>
            <View style={styles.loginheader}>
              <Text numberOfLines={1} style={styles.logintitle}>
                {this.props.dataObj ? this.props.dataObj.amenities_name : ""}
              </Text>
            </View>
          </View>

          <View style={{flexDirection: 'row',justifyContent:'flex-start',alignItems: 'center',
         paddingVertical: 10,paddingHorizontal: 20,}}>
            <Text style={{fontSize:18}}>Choose the Venue</Text>
            <Text>{"  "}</Text>

            <View
              style={{
                borderColor: color.black1,
                borderWidth: 0.5,
                borderStyle: "solid"
              }}
            >
              <Picker
                style={{ width: wp("45%") }}
                onValueChange={(itemValue, itemIndex) => <></>}
              >
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js" />
              </Picker>
            </View>
          </View>

          <View>
            {this.state.AmentiesObj &&
              this.state.AmentiesObj.amenities_array.length > 0 &&
              this.state.AmentiesObj.amenities_array.map((obj, key) => {
                return (
                  <ListItem
                    key={key}
                    style={{
                      margin: 0,
                      flexDirection: "row",
                      alignItems: "center",
                      minHeight: hp("9%")
                    }}
                  >
                    <View
                      style={{
                        flex: 0.35,
                        flexWrap: "wrap",
                        flexDirection: "row"
                      }}
                    >
                      <Checkbox
                        selectedData={data => this.selectedData(key, data, obj)}
                        size={hp("4.5%")}
                        label={obj.amenities_det_name}
                        selectedArrayObject={selectnewArray}
                        active={obj.statekey}
                        keyvalue={key}
                        color={color.orange}
                      />
                    </View>
                    {obj.statekey == true && (
                      <View
                        style={{
                          flex: 0.66,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                          flexWrap: "wrap",
                          marginLeft: 12
                        }}
                      >
                        {obj.amenities_det_datatype1 != "" &&
                          this.renderInput(
                            obj.amenities_det_datatype1,
                            obj.amenities_det_datavalue1,
                            "venue_amnts_det_datavalue1",
                            "amenities_det_datatype1",
                            "amenities_det_datavalue1",
                            "amenities_det_datavalued1",
                            key,
                            obj["amenities_det_datavalued1"]
                          )}

                        {obj.amenities_det_datatype2 != "" &&
                          this.renderInput(
                            obj.amenities_det_datatype2,
                            obj.amenities_det_datavalue2,
                            "venue_amnts_det_datavalue2",
                            "amenities_det_datatype2",
                            "amenities_det_datavalue2",
                            "amenities_det_datavalued2",
                            key,
                            obj["amenities_det_datavalued2"]
                          )}

                        {obj.amenities_det_datatype3 != "" &&
                          this.renderInput(
                            obj.amenities_det_datatype3,
                            obj.amenities_det_datavalue3,
                            "venue_amnts_det_datavalue3",
                            "amenities_det_datatype3",
                            "amenities_det_datavalue3",
                            "amenities_det_datavalued3",
                            key,
                            obj["amenities_det_datavalued3"]
                          )}
                      </View>
                    )}
                  </ListItem>
                );
              })}
          </View>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: hp("2%")
            }}
          >
            <Button 
             onPress={()=>this.openNewVenue()
             }
              style={[
                styles.actionbtn,
                { backgroundColor: color.blue, color: "white" }
              ]}
            >
              <Text style={styles.actionbtntxt}>ADD NEW VENUE</Text>
            </Button>

            <Button 
            onPress={()=>this.submitPressed()}
              block
              style={[
                styles.actionbtn,
                { backgroundColor: color.orange, color: "white" }
              ]}
            >
              <Text style={styles.actionbtntxt}>SAVE</Text>
            </Button>
          </View>
        </View>
      </TouchableOpacity>
    </Content>
  );
}
componentDidMount(){
	// alert(JSON.stringify(this.props.dataObj))
	this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
	// alert(JSON.stringify(this.props));
	if(this.props.dataObj){
		// alert(JSON.stringify(this.props.dataObj));
		var amenitiesobj=JSON.parse(JSON.stringify(this.props.dataObj));
		// amenitiesobj.amenities_array.map((obj)=>!obj.datatypevalue2?obj.datatypevalue2=obj.amenities_det_datavalue2.split(',')[0]:obj.datatypevalue2)
		// amenitiesobj.amenities_array.map((obj)=>!obj.datatypevalue3?obj.datatypevalue3=obj.amenities_det_datavalue3.split(',')[0]:obj.datatypevalue3)
		// amenitiesobj.amenities_array.map((obj)=>!obj.amt?obj.amt="0":obj.amt);
		// alert(JSON.stringify(amenitiesobj));
		this.setState({AmentiesObj:amenitiesobj});
	}
}

}
const styles = {
  circle: {
    width: hp("17%"),
    height: hp("17%"),
    borderRadius: hp("17%") / 2,
    marginTop: "5%",
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    borderColor: color.orange
  },
  innercircle: {
    position: "absolute",
    width: hp("15.5%"),
    height: hp("15.5%"),
    borderRadius: hp("15.5%") / 2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: color.orange,
    margin: "auto"
  },
  loginheader: {
    width: "100%",
    marginTop: hp("1%"),
    alignItems: "center"
  },
  logintitle: {
    width: "100%",
    fontSize: hp("2.8%"),
    textAlign: "center",
    color: color.black1
  },
  loginsubtitle: {
    fontSize: hp("2%"),
    color: color.black1
  },
  texboxpadding: {
    marginBottom: 20
  },
  submitbox: {
    flex: 1,
    flexDirection: "row",
    padding: 12,
    borderBottomWidth: 0.5
  },
  actionbtn: {
    borderRadius: 5,
    justifyContent: "center",
    paddingHorizontal: 14,
    marginHorizontal: 10,
    color: "white"
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2%"),
    fontWeight: "bold"
  },
  sociallogin: {
    backgroundColor: color.ash1,
    height: hp("24%"),
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  socialiconlogin: {
    marginTop: 10,
    flexDirection: "row"
  },
  socialicon: {
    width: hp("4%"),
    height: hp("4%"),
    margin: 5
  }
};