import React, { Component } from 'react';
import { View, Text , StyleSheet } from 'react-native';
import {Content,Button} from 'native-base';
import WeekPicker from '../components/weekpicker';
import Schedule from '../components/schedule';
import color from '../Helpers/color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { Actions } from "react-native-router-flux";

export default class weekAvailability extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     schedule:[],
                     selectedDate:'',
                     week:{},
                     selectedSlots:[],
                     loading:false
                   };
                   this.setSchedule = this.setSchedule.bind(this)
                 }

                 setSchedule=(data)=>{
                   console.log('weeek',data)
                   this.setState({
                     schedule:data,
                     slots:[]
                   })
                 } 


                  _setSelected=(data)=>{
                   console.log('date',data)
                   this.setState({
                     selectedDate:data
                   })

                 } 



                 _setSlots=(data)=>{ 

                 // console.log('slots',data)
                    this.setState({
                      slots:data
                    })
                 }


                 _submit=(data)=>{ 

                   var selectedSlots = this.state.selectedSlots;
                   selectedSlots=data;
                   this.setState({selectedSlots})
                  
                   this.props.pushData({
                     selectedDate: this.state.selectedDate,
                     selectedSlots: selectedSlots
                   }); 

                   Actions.pop();

                 }

                 render() {
                   return (
                     <Content>
                       <React.Fragment style={styles.container}>
                         <View>
                           <WeekPicker
                             loading={(data)=>this.setState({loading:data})}
                             setSelected={this._setSelected}
                             setSlots={this._setSlots}
                             venueId={this.props.venueId}
                             hourlychosenslots={this.props.oldWeekData}
                             selectedDate={this.state.selectedDate}
                           />
                         </View>
                         <View>
                           <Schedule
                           loading={this.state.loading}
                             onClick={this.setSchedule}
                             isOnClick={true}
                             list={this.state.slots}
                             onSubmit={this._submit}
                             oldWeekData={this.props.oldWeekData}
                             selectedDate={this.state.selectedDate}
                           />
                         </View>
                       </React.Fragment>
                     </Content>
                   );
                 }
               }




               const styles = StyleSheet.create({
                 container: {
                   flex: 1,
                   justifyContent: "flex-start",
                   alignItems: "center"
                 },
                 actionbtn: {
                   borderRadius: 5,
                   width: hp("17%"),
                   justifyContent: "center",
                   backgroundColor: color.blue
                 },
                 actionbtntxt: {
                   textAlign: "center",
                   color: color.white,
                   fontSize: hp("2.3%")
                 }
               });
