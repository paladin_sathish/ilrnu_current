import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  ScrollView,
  ActivityIndicator
} from "react-native";
import ModalComp from "../components/ModalComp";
import {
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  H2
} from "native-base";
import color from "../Helpers/color";
import PropTypes from "prop-types";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

export default class VenueChatList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPromo: false,
      promo_code: "",
      showLoading:true
    };
  }

  _renderList = (item, i) => {
    return (
      <Card padder>
        <TouchableHighlight onPress={()=>this.props.onPress(item)}>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              paddingHorizontal: 15,
              marginVertical: 6
            }}
          >
            <View>
              <Text
                style={{
                  color: "#3f51b5"
                }}
              >
                {item.user_name.toUpperCase()}
              </Text>
            </View>
            <View>
              <Text style={{ fontSize: 16 }}></Text>
            </View>
          </View>
        </TouchableHighlight>
      </Card>
    );
  };  


  componentDidMount = () => {
    this.setState({
      showLoading:false
    })
  };
  


    _renderVenueMember = (item, i) => {

      return (
        <ListItem
          avatar
          noBorder
          noIndent
          onPress={() => this.props.onPress(item)}
        >
          <Left>
            <Thumbnail
              source={{
                uri:
                  "https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png"
              }}
            />
          </Left>
          <Body>
            <Text> {item.user_name.toUpperCase()}</Text>
            <Text note> user</Text>
          </Body>
        </ListItem>
      );
    }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isPromoShow != this.state.isPromo) {
      this.setState({
        isPromo: nextProps.isPromoShow
      });
    }
  }

  
  

  render() {  
    
    const { data,venue } = this.props; 
      
    const { isPromo,showLoading } = this.state; 

     if (showLoading) {
       return (
         <View
           style={{
             flex: 1,
             justifyContent: "center",
             alignItems: "center"
           }}
         >
           <ActivityIndicator size="large" color="#073cb2" />
           <Text>Please Wait</Text>
         </View>
       );
     }
    return (
      <React.Fragment>
        <ModalComp
          titlecolor={color.orange}
          visible={isPromo}
          header={false}
          center={false}
        >
          <View
            style={{
              flex: 1,
              paddingVertical: 15,
              paddingHorizontal: 15
            }}
          >
            <H2 style={{ textWrap: "wrap", width: wp("75%") }}>
              {venue&&venue.trn_venue_name} Members
            </H2>
            <View>
              <ScrollView>
                <List >
                  {data.length > 0 &&
                    data.map((item, i) => this._renderVenueMember(item, i))}
                </List>
              </ScrollView>
            </View>
          </View>
        </ModalComp>
      </React.Fragment>
    );
  }
}

VenueChatList.propTypes = {
  isPromoShow: PropTypes.bool.isRequired,
  closePressed: PropTypes.func.isRequired,
  data: PropTypes.array
};

VenueChatList.defaultProps = {
  isPromoShow: false,
  data: []
};

const styles = StyleSheet.create({
  list: {},
  actionbtntxt: {
    alignSelf: "center",
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  },
  sociallogin: {
    backgroundColor: color.ash1,
    height: hp("7%"),
    alignItems: "stretch",
    justifyContent: "center"
  }
});
