import React, { Component } from "react";
import {
  Image,
  Text,
  View,
  TouchableOpacity,
  BackHandler,
  Alert,
  Dimensions
} from "react-native";
import {
  Right,
  Top,
  Icon,
  Content,
  Button,
  ListItem,
  Body,
  CheckBox,
  Item,
  Input,
  Container
} from "native-base";
import { Actions } from "react-native-router-flux";
import DropDown from "../components/dropdown";
import SessionList from "../components/SessionList";
import colors from "../Helpers/colors";
import HomeSub from "../components/home_sub";
import color from "../Helpers/color";
import Popover from "react-native-popover-view";
import AsyncStorage from "@react-native-community/async-storage";
import links from "../Helpers/config";
import Toast from "react-native-simple-toast";
import TabLoginProcess from "../components/TabLoginProcess";
import VideoComp from "../components/VideoComp";
import Carousel, { Pagination } from "react-native-snap-carousel";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";  
const screenWidth = Dimensions.get(
                                            "window"
                                          ).width;



var imageList1 = [
  { image: require("../images/guitar.png") },
  { image: require("../images/cooking.png") },
  { image: require("../images/guitar.png") },
  { image: require("../images/cooking.png") }
];
var imageList2 = [
  { image: require("../images/meetingroom.png") },
  { image: require("../images/meetings.png") },
  { image: require("../images/meetingroom.png") },
  { image: require("../images/meetings.png") }
];
var imageList3 = [
  { image: require("../images/uncle.png") },
  { image: require("../images/boy.png") },
  { image: require("../images/uncle.png") },
  { image: require("../images/boy.png") }
];

var typeoflist = {
  venue: [
    { name: "I want to List my Venue", id: 1, key: 1 },
    { name: "I'm Looking for Venue", id: 1, key: 2 }
  ],
  course: [
    { name: "I want to list my Courses", id: 2, key: 1 },
    { name: "I'm looking for Course", id: 2, key: 2 }
  ],
  trainer: [
    { name: "I want to Join as a Trainer", id: 3, key: 1 },
    { name: "I'm Looking for a Trainer", id: 3, key: 2 }
  ]
};

const playgr_list = [
   {
    image: require("../images/banner-07-min.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/banner-01-min.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/banner-02-min.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/banner-03-min.jpg"),
    text: "Meadow Crest Playground"
  }
];  
export default class Home1 extends Component {
                 constructor(props) {
                   super(props);
                   Actions.drawerClose();
                   this.state = {
                     listtype: "",
                     showdropdown: false,
                     nearme: false,
                     invisible: false,
                     isVisible: false,
                     search_string: null,
                     sessionTrue: true,
                     loadvideo: false,
                     loginDetails: null
                   };

                   this.handleBackButton = this.handleBackButton.bind(this);
                 }

                 componentWillMount = () => {

                  console.log('calledwhome')
                   //  BackHandler.addEventListener(
                   //    "hardwareBackPress",
                   //    this.handleBackButton
                   //  );
                   //
                 };


                 componentDidMount = () => {
                       console.log("calledmount");
                       
                 };
                 

                 handleBackButton = () => {
                   console.log("backbutton fired");
                   this.props.navigation.goBack(null);
                   return true;
                 };

                 submitSearch = async () => {
                   const manuallocation = JSON.parse(
                     await AsyncStorage.getItem("chosenlocation")
                   );

                   console.log("manuallocation", manuallocation);
                   let lat = manuallocation != null ? manuallocation.lat : null;
                   let long =
                     manuallocation != null ? manuallocation.lng : null;
                   console.log({
                     searchContent: this.state.search_string,
                     nearme: this.state.nearme,
                     offset: 0,
                     lat: lat,
                     long: long
                   });

                   if (this.state.search_string != null) {
                     fetch(links.APIURL + "commonSearch", {
                       method: "POST",
                       headers: {
                         Accept: "application/json",
                         "Content-Type": "application/json"
                       },
                       body: JSON.stringify({
                         searchContent: this.state.search_string,
                         nearme: this.state.nearme,
                         offset: 0,
                         lat: lat,
                         long: long
                       })
                     })
                       .then(response => response.json())
                       .then(responseJson => {
                         console.log("search res", responseJson.data);

                         if (responseJson.status == 0) {
                           Actions.Search_Home({
                             data:
                               responseJson.data.length > 0
                                 ? responseJson.data
                                 : [],
                             data1: "",
                             data2: "",
                             latlng: {
                               latitude: lat,
                               longtitude: long
                             }
                           });
                         } else {
                           Toast.show("No Records Found", Toast.LONG);
                           this.setState({ NoRecords: true });
                         }
                       });
                   } else Toast.show("Search Field Empty", Toast.LONG);
                 };
                 showPopover = () => {
                   this.setState({ isVisible: true });
                 };

                 componentWillUnmount() {
                   BackHandler.removeEventListener(
                     "hardwareBackPress",
                     this.handleBackButton
                   );
                 }

                 closePopover = () => {
                   this.setState({ isVisible: false });
                 };

                 _renderItem({ item, index }) {
                   return (
                     <Image source={item.image} style={styles.homeBgImage} />
                   );
                 } 

                 addLMSUser = () => {
      
                   fetch("https://lms.ilrnu.com/json/adduser.php", {
                     method: "POST",
                     headers: {
                       Accept: "*",
                     },
                     body: JSON.stringify({
                       users: [
{
"username":"moodleusser", 
"password":"Studefsdntf@123",
"firstname":"Moodle1",
"lastname":"User",
"email":"moodlesusefdr1@gmail.com",
"auth":"manual"
}
]
                     }),
                   })
                     .then((response) => response.json())
                     .then((responseJson) => {
                       console.log("lmsuseradd", responseJson);
                       if (responseJson.status == 0) {
                        
                       } else {
                        
                       }
                     });
                 };


                 
     getTrainerId = () => {
       console.log('thissta',this.state.loginDetails)
                   fetch(links.APIURL + "myTrainerList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       userId: this.state.loginDetails.user_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                     console.log('resp',responseJson)
                       if (responseJson.status == 0) {
                         var venueform = this.state.venueform;
                         var traineedata = responseJson.data[0];
                           if(responseJson.data.length>0)
                           { 
                               Actions.ShowTrainee({
                                 logindata: this.state.loginDetails,
                                 Trainee: traineedata
                               });
                           }
                       
                       }
                       else{
                         Actions.TraineeForm({
                           logindata: this.state.loginDetails
                         });
                       }
                     });
                 };



                 changelistype = data => {
                   this.setState({ listtype: data });
                   this.setState({ showdropdown: true });
                 };
                 closedropdown = () => {
                   this.setState({ listtype: "" });
                   this.setState({ showdropdown: false });
                 };

                 listcallback = obj => {
                   if (obj.id == 1 && obj.key == 1) {
                     // alert("list my venue");
                     Actions.venuepage();
                   } else if (obj.id == 1 && obj.key == 2) {
                     Actions.home();
                   } else if (obj.id == 3 && obj.key == 1) {
                      AsyncStorage.getItem("loginDetails", (err, result) => {
                        if (result != null) {
                          this.setState({
                            loginDetails: JSON.parse(result),
                          });
                          if (JSON.parse(result).user_cat_id == 1) {
                             this.getTrainerId();
                          } else {
                              Actions.corporateform({
                                logindata: JSON.parse(result),
                              });
                          }
                        } else {
                          this.setState({
                            sessionTrue: false,
                          });
                        }
                      });
                    
                   } else if (obj.id == 3 && obj.key == 2) {
                     AsyncStorage.getItem("loginDetails", (err, result) => {
                       this.setState({ loadvideo: false });
                       if (result != null) {
                         this.setState({ loadvideo: true });
                         // Actions.TrainerSearch();
                       } else {
                         this.setState({ loadvideo: false });

                         this.setState({ sessionTrue: false });
                       }
                     });
                   } else if (obj.id == 2 && obj.key == 2) {
                     AsyncStorage.getItem("loginDetails", (err, result) => {
                       this.setState({ loadvideo: false });
                       if (result != null) {
                         // this.setState({loadvideo:true});
                         Actions.CourseSearch();
                       } else {
                         this.setState({ loadvideo: false });

                         this.setState({ sessionTrue: false });
                       }
                     });
                   } else if (obj.id == 2 && obj.key == 1) {
                     AsyncStorage.getItem("loginDetails", (err, result) => {
                       if (result != null) {
                         this.setState({
                           loginDetails: JSON.parse(result)
                         });
                         if (JSON.parse(result).user_cat_id == 1) {
                           Actions.CourseForm({
                             logindata: JSON.parse(result)
                           });
                         } else {
                         }
                       } else {
                         this.setState({
                           sessionTrue: false
                         });
                       }
                     });
                   } else if (obj.id == 3 && obj.key == 2) {
                     Actions.push("home", { type: "trainee" });
                   }

                   this.closedropdown();
                 };

                 closevideo = () => {
                   AsyncStorage.getItem("loginDetails", (err, result) => {
                     this.setState({ loadvideo: false });
                     if (result != null) {
                       // this.setState({sessionTrue:true});
                       Actions.TrainerSearch();
                     } else {
                       this.setState({ sessionTrue: false });
                     }
                   });
                 };

                 tabAction = data => {
                   if (data.user_cat_id == 1) {
                     Actions.CourseForm({
                       logindata: data
                     });
                   } else {
                     Actions.corporateform({
                       logindata: data,
                       welcomemsg: "User Logged in Successfully"
                     });
                     // alert(JSON.stringify(data));
                   }
                 }; 

                 loadTrainer =()=>{
                      AsyncStorage.getItem("loginDetails", (err, result) => {
                        if (result != null) {
                          this.setState({
                            loginDetails: JSON.parse(result),
                          });
                          if (JSON.parse(result).user_cat_id == 1) {
                            this.getTrainerId();
                          } else {
                            Actions.corporateform({
                              logindata: JSON.parse(result),
                            });
                          }
                        } else {
                          this.setState({
                            sessionTrue: false,
                          });
                        }
                      });
                 } 


                 loadCourse=()=>{
                    AsyncStorage.getItem("loginDetails", (err, result) => {
                      if (result != null) {
                        this.setState({
                          loginDetails: JSON.parse(result),
                        });
                        if (JSON.parse(result).user_cat_id == 1) {
                          Actions.CourseForm({
                            logindata: JSON.parse(result),
                          });
                        } else {
                        }
                      } else {
                        this.setState({
                          sessionTrue: false,
                        });
                      }
                    });
                 }

                 render() {
                   return (
                     <Container>
                       <HomeSub bgcolor={colors.orange}>
                         <View style={styles.subMainRow}>
                           <View style={styles.subFirstRow}>
                             <Text style={styles.subFirstRowText}>
                               I am looking for
                             </Text>
                           </View>
                           <View style={styles.subSecondRow}>
                             <TouchableOpacity
                               style={[styles.itemsub]}
                               onPress={() => this.changelistype("course")}
                             >
                               <Text
                                 style={[
                                   styles.itemsubText,
                                   this.state.listtype == "course"
                                     ? styles.activeText
                                     : "",
                                   styles.alignCenter,
                                 ]}
                               >
                                 Course
                               </Text>
                               {this.state.listtype == "course" && (
                                 <View
                                   style={[styles.triangle, styles.arrowUp]}
                                 />
                               )}
                             </TouchableOpacity>
                             <TouchableOpacity
                               style={styles.itemsub}
                               onPress={() => this.changelistype("trainer")}
                             >
                               <Text
                                 style={[
                                   styles.itemsubText,
                                   this.state.listtype == "trainer"
                                     ? styles.activeText
                                     : "",
                                   styles.alignCenter,
                                 ]}
                               >
                                 Trainer
                               </Text>
                               {this.state.listtype == "trainer" && (
                                 <View
                                   style={[styles.triangle, styles.arrowUp]}
                                 />
                               )}
                             </TouchableOpacity>
                             <TouchableOpacity
                               style={styles.itemsub}
                               onPress={() => this.changelistype("venue")}
                             >
                               <Text
                                 style={[
                                   styles.itemsubText,
                                   this.state.listtype == "venue"
                                     ? styles.activeText
                                     : "",
                                   styles.alignCenter,
                                 ]}
                               >
                                 Venue
                               </Text>
                               {this.state.listtype == "venue" && (
                                 <Text
                                   style={[styles.triangle, styles.arrowUp]}
                                 />
                               )}
                             </TouchableOpacity>
                           </View>
                         </View>
                       </HomeSub>

                       <View
                         style={{
                           display: "flex",
                           position: "relative",
                         }}
                       >
                         {this.state.showdropdown && (
                           <DropDown
                             listofcources={typeoflist[this.state.listtype]}
                             listcallback={this.listcallback}
                           />
                         )}
                       </View>
                       <Content>
                         <View
                           style={{
                             display: "flex",
                             position: "relative",
                           }}
                         >
                           <Carousel
                             ref={(c) => {
                               this._carousel = c;
                             }}
                             data={playgr_list}
                             firstItem={0}
                             autoplay={true}
                             loop={true}
                             contentContainerCustomStyle={{ flexGrow: 0 }}
                             renderItem={this._renderItem}
                             sliderWidth={screenWidth}
                             itemWidth={screenWidth}
                             itemHeight={150}
                             inactiveSlideScale={1}
                             //  slideStyle={{ heigth: 500 }}
                           />
                         </View>

                         {this.state.sessionTrue == false && (
                           <TabLoginProcess
                             loginttype="List your Venue"
                             type="login"
                             visible={true}
                             sendlogindet={this.tabAction}
                           />
                         )}

                         <SessionList
                           icon={false}
                           title="Trending Trainings"
                           subtitle="Add My Course"
                           imageList={imageList1}
                           _onload={this.loadCourse}
                         />
                         <SessionList
                           icon={false}
                           title="Most Busy Venues"
                           subtitle="Add My Venue"
                           imageList={imageList2}
                         />
                         <SessionList
                           icon={false}
                           title="Leading Trainers"
                           subtitle="Join as Trainer"
                           imageList={imageList3}
                           _onload={this.loadTrainer}
                         />
                         <SessionList
                           icon={false}
                           title="Trending Trainings"
                           subtitle="Add My Course"
                           imageList={imageList1}
                         />

                         {this.state.loadvideo == true && (
                           <VideoComp
                             headerText={"How to search course in iLRNU?"}
                             closevideo={() => this.closevideo()}
                           />
                         )}
                       </Content>
                     </Container>
                   );
                 }
               }

const styles = {
  imagebackground: {
    flex: hp("20%"),
    backgroundColor: "grey"
  },
  listimagescroll: {
    overflow: "scroll",
    flexDirection: "row"
    // width:wp('80%')
  },
  actionbtn: {
    borderRadius: 50,
    width: hp("25%"),
    justifyContent: "center"
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2%"),
    fontWeight: "bold"
  },
  search: {
    display: "flex",
    padding: wp("2"),
    backgroundColor: "#a60202",
    justifyContent: "center",
    alignItems: "center"
  },
  arrow1: {
    left: hp("2%")
  },
  arrow2: {
    left: hp("2.5%")
  },
  arrow3: {
    left: hp("4%")
  },
  subMainRow: {
    alignItems: "center"
  },
  subFirstRow: {
    padding: 12
  },
  listImage: {
    width: hp("18%"),
    height: hp("12%"),
    borderRadius: 5,
    marginRight: 12
  },
  col3: {
    flex: 0.3,
    justifyContent: "center"
  },
  col7: {
    flex: 0.7,
    flexDirection: "row"
  },
  subFirstRowText: {
    color: colors.white,
    fontSize: hp("2%")
  },
  homeBgImage: {
    width: wp("100%"),
    height: hp("25%"),
    position:'relative',
    
  },

  subSecondRow: {
    // width: hp('30%'),
    flexDirection: "row",
    borderTopWidth: 0.5,
    borderColor: colors.white,
    alignItems: "center",
    justifyContent: "center"
  },
  alignRight: {
    textAlign: "right"
  },
  alignLeft: {
    textAlign: "left"
  },
  alignCenter: {
    textAlign: "center"
  },
  homeBgImage: {
    width: screenWidth,
    height: hp("25%"),
    resizeMode: "stretch"
  },
  itemsub: {
    minWidth: hp("30%") / 3,
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 14,
    paddingRight: 14
    // backgroundColor:"grey"
  },
  itemsubText: {
    color: colors.white,
    fontSize: hp("2.2%")
  },
  activeText: {
    color: colors.blueactive,
    fontWeight: "bold"
  },
  dropdown: {
    width: "100%",
    paddingLeft: "2%",
    position: "absolute",
    backgroundColor: colors.white
  },
  activearrow: {
    position: "absolute",
    bottom: 0,
    width: "100%"
  },
  triangle: {
    width: 0,
    height: 0,
    // textAlign:'center',

    margin: "auto",
    backgroundColor: "transparent",
    borderStyle: "solid"
  },
  arrowUp: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
    borderTopWidth: 0,
    borderRightWidth: 12,
    borderBottomWidth: 12,
    borderLeftWidth: 12,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: colors.white,
    borderLeftColor: "transparent"
  }
};
