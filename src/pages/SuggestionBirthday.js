import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,ActivityIndicator} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import links from '../Helpers/config';
import TabLoginProcess from '../components/TabLoginProcess';
import Geolocation from 'react-native-geolocation-service';
import placeholder_img from '../images/placeholder_img.png';
import ModalDropdown  from 'react-native-modal-dropdown';
import {Actions,Router,Scene} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import Tick_mark from '../components/TickComp';
import Line from '../components/Line';
import VenueCardComp from '../components/VenueCardComp';

import ListingDetails from '../pages/ListingDetails';



const playgr_list=[
    {image:require('../images/slider1.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider2.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider3.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider4.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider5.png'),text:'Meadow Crest Playground'}
]

export default class SuggestionBirthday extends Component{
    constructor(props) {
      super(props);
    
      this.state = {
        chosenlocation:{
          latitude:'',
          longitude:''
        },
        manuallocation:null,
        loginState: false,
        activeCategory: null,
        layoutdata: null,
        Norecords: false,
        suggestionDropdown: {
          id: "venue_spec_id",
          name: "venue_spec_name",
          dropdown: []
        },
        suggestiondata: [],
        suggestionvalue: "",
        catvalue: "",
        loginDetails: null
      };
    }
    loadVenueForm=(data)=>{
      // Actions.loadVenueForm
      if(this.state.loginDetails){
        if(this.state.loginDetails.user_cat_id==1){
          if(data){

          Actions.venueform({logindata:this.state.loginDetails,welcomemsg:'User Logged in Successfully'});
        }else{
         Actions.venueform({logindata:this.state.loginDetails});
        }
        }else{
       if(data){
          Actions.corporateform({logindata:this.state.loginDetails,welcomemsg:'User Logged in Successfully'});
        }else{
          Actions.corporateform({logindata:this.state.loginDetails});
        }

        }
      }else{
        this.setState({loginState:true})
      // Actions.venuepage({raiselogin:true})
      }
    }
    tabAction=(data)=>{
  // alert(JSON.stringify(data));
  // if(data.uer_)
  this.setState({loginState:false});
  this.setState({loginDetails:data},function(){
    this.loadVenueForm(data);
  })
}

 async loadcurrentLocation(){
  this.setState({currentlocation:'blue'});
console.log("calling Twice");
  
}

    async componentDidMount(){ 
      
const data=await AsyncStorage.getItem('loginDetails');
// alert(data);
if(data){
  // alert(JSON.stringify(data));
  this.setState({loginDetails:JSON.parse(data)});

}
 this.loadcurrentLocation();
 
    } 



    componentWillReceiveProps=(props)=>{
      if(props.visiblescreenData){
        if(props.visiblescreenData!='suggestion'){
          var suggestiondata=this.state.suggestiondata;
          suggestiondata.map((obj)=>obj.visible=false);
          this.setState({activeCategory:null,suggestiondata});
        }
      }
    }
    suggestionListing=()=>{
         
        fetch(links.APIURL+'suggestionDropdown', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({}),
    }).then((response)=>response.json())
    .then((responseJson)=>{

        var suggestionDropdown=this.state.suggestionDropdown;
    suggestionDropdown.dropdown=responseJson.data;
    this.setState({suggestionDropdown})
    this.suggestionBirthday(responseJson.data.length>0?responseJson.data[0]:null);

    // alert(JSON.stringify(responseJson.data));
    // alert(JSON.stringify(this.state.suggestionDropdown.dropdown[0].venue_cat_name))
       // console.log("dropvalues",JSON.stringify(this.state.catDropdown));
    }) 
 
   
  }
  renderButtonText= (data)=>{
    // alert(JSON.stringify(data));
        this.setState({catvalue:data})
        this.suggestionBirthday(data);
        return data.venue_spec_name;
    }
    suggestionBirthday=async (data)=>{
      if(!data){
        return;
      } 
const manuallocation = JSON.parse(await AsyncStorage.getItem('chosenlocation'));

        this.setState({spinner:true,activeCategory:null})
             Geolocation.getCurrentPosition(
       (position) => {
    let lat = manuallocation!=null?manuallocation.lat:position.coords.latitude;
    let lng = manuallocation!=null?manuallocation.lng:position.coords.longitude; 
    this.setState({manuallocation:{lat:lat,lng:lng}})

  fetch(links.APIURL+'suggestion/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({'specId':data?data.venue_spec_id:0,lat:lat,long:lng}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
         this.setState({spinner:false})
         if(responseJson.length>0){

            this.setState({Norecords:false});
         }else{
            this.setState({Norecords:true});
         }
     this.setState({suggestiondata:responseJson.length>0?responseJson:[]});
// alert(JSON.stringify(responseJson[0].venue_spec_name));
this.setState({suggestionvalue:data.venue_spec_name});
//       console.log(responseJson)
    })
       },
       
       (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
       {timeout: 10000,maximumAge:0},
     );
  }
  async componentWillMount(){ 
    
   
    Geolocation.getCurrentPosition(
      position => {
        this.setState({
          chosenlocation: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
        });
      },

      error => console.log(error.message),
      { timeout: 30000, maximumAge: 0 }
    );
    this.suggestionListing();
    // this.suggestionBirthday(1);

  }
onLayout=(data)=>{
  this.setState({layoutdata:data.nativeEvent.layout})
}
 receiveFram=(data)=>{
    // data.height=null;

    // data.height=(data.height-data.height%5);
    // data.height=(data.height-(data.height%4>0?(data.height/4):0))+15;
    return data;
  }
   showDropdown=()=>{
    this.dropdown.show();
  }
    changeItemSlider=(item,index,visible)=>{
      this.props.visiblescreen&&this.props.visiblescreen('suggestion');
    var suggestiondata=this.state.suggestiondata;
    suggestiondata.map((obj)=>obj.visible=false);
    suggestiondata[index].visible=visible?!visible:true;
    this.setState({suggestiondata});
    if(suggestiondata[index].visible==true){
    this.setState({activeCategory:item})

    }else{
      this.setState({activeCategory:null})
    }

  }
  clearActiveCategory=()=>{
      var suggestiondata=this.state.suggestiondata;
    suggestiondata.map((obj)=>obj.visible=false);
    this.setState({suggestiondata,activeCategory:null});
}
      render(){

        console.log('suggestion')
        return(
            <View style={styles.container} >
                <View style={styles.flex_row}>
                    <View style={styles.flex_row}>
                      <Text style={[styles.text_style1,{marginRight:0,paddingLeft:4,paddingTop:4}]}>Suggestions for your</Text>
                        <ModalDropdown  
                        ref={el => this.dropdown = el} 
                        adjustFrame={this.receiveFram}
                                    style={{marginRight:0,padding:2,marginTop:2}}    
                                    textStyle={[styles.text_style]}
                                    dropdownStyle={styles.dropdown_options}
                                    defaultValue={this.state.suggestionvalue&&this.state.suggestionvalue?this.state.suggestionvalue:'Select'}
                                    renderButtonText={this.renderButtonText}
                                    showsVerticalScrollIndicator={false}	
                                 options={this.state.suggestionDropdown.dropdown}   
                                      renderRow={  
                    (rowData) =>  
                        <Text  style={{fontSize: 14,padding:5}}>{rowData.venue_spec_name}</Text>}/>                          

                        <TouchableOpacity onPress={()=>this.showDropdown()} style={{width:20,flexDirection:'column',justifyContent:'center'}}>
                            <Image source={Down_arrow} style={styles.Down_arrowstyle}></Image>
                        </TouchableOpacity>
                    </View>
                    <View onLayout={this.onLayout} style={styles.flex_sep}>
                        <TouchableOpacity onPress={()=>this.loadVenueForm()} style={styles.add_symbol_container} >
                            <Image style={styles.add_symbol} source={ADD_img}>
                            </Image>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.state.spinner==true &&
     <ActivityIndicator
      animating
      color="#615f60"
      size="small"
      style= {{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
     
  }}
    />
  }
                {this.state.suggestiondata.length>0&&
                 <ScrollView
                   horizontal={true}>
                  {
                   this.state.suggestiondata.map((item,index)=>{
                     return (
                       <TouchableOpacity
                         style={styles.square}
                        
                         onPress={() =>
                           Actions.BookDetails({
                             lat: this.state.manuallocation.lat,
                             long: this.state.manuallocation.lng,
                             id: item.venue_id
                           })
                         }
                       >
                         <VenueCardComp item={item}/>
                       </TouchableOpacity>
                     );


                   })

                 }
                   </ScrollView> 
}
{this.state.Norecords==true&&
<View ><Text style={{textAlign:'center',padding:10}}>No Records</Text></View>
}
{this.state.activeCategory&&
  <Line/>
}
  {this.state.activeCategory&&
                    <ListingDetails layoutChange={(data)=>this.props.layoutChange&&this.props.layoutChange(data)} activeCategory={this.state.activeCategory}  clearList={()=>this.clearActiveCategory()}>
                    <Text>Cancel</Text>
                    </ListingDetails>
                }
                 {this.state.loginState==true&&
        <TabLoginProcess closetabmodal={()=>this.setState({loginState:false})} loginttype="Add your Venue" type='login' visible={true} sendlogindet={this.tabAction}/>
      }
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
    image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },
    text_style2:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
        paddingLeft:hp('0.45%'),
        paddingTop:hp('0.45%')
    },
    flex_row:{
        flexDirection:'row'
    },
    text_style1:{
        fontSize:hp('2%')
    },
    text_style:{
        color:color.blue,
        fontSize:hp('2%'),
        fontWeight:'bold'
    },
    Down_arrowstyle:{
        height:hp('1.4%'),
        width:hp('1.4%')
    },
    flex_sep:{
        flex:1
    },
    add_symbol_container:{
        backgroundColor:color.blue,
        justifyContent:'flex-end',
        justifyContent:'center',
        alignSelf:'flex-end',
        marginRight:wp('5%'),
        height:hp('2.5%'),
        width:hp('2.5%'),
       
    },
    add_symbol:{
        height:hp('1.3%'),
        width:hp('1.3%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
    },
    dropdown_options:{
        backgroundColor:color.white,
        // padding:10,
        paddingLeft:10,
        paddingRight:10,
        paddingTop:10,
        paddingBottom:10,
        marginBottom:5,
        fontSize:hp('1.9%'),
        width:wp('40%'),
        borderRadius:5,
        borderWidth:2,
        minHeight:hp('2%'),
        borderColor:color.ash

    },
})