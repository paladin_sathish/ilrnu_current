import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList} from 'react-native';
import {Right,Top,Icon, Button,ListItem,Content} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import color from '../Helpers/color';
import fb_png from '../images/fb.png';
import insta_png from '../images/insta.png';
import google_png from '../images/google.png'
import ModalComp from '../components/Modal';
import Share from "react-native-share";

export default class CourseSuccess extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {

                   };
                 }
                 switchcorporate = () => {
                   Alert.alert(
                     "Alert",
                     "Switch To Corporate Login?",
                     [
                       {
                         text: "Cancel",
                         onPress: () => console.log("cancelled"),
                         style: "cancel"
                       },
                       {
                         text: "OK",
                         onPress: () => {
                           AsyncStorage.removeItem("loginDetails");
                           this.props.closemodal("corplogin");
                         }
                       }
                     ],
                     { cancelable: false }
                   );
                 };

                 openShare=()=>{
                    const shareOptions = {
                      title: "Share via",
                      message:
                        "iLRNU is a universal platform to assist you to find the perfect course for engaging your knowledge. ",
                      url:
                        "https://www.ilrnu.com/static/media/Header_logo.e04394ae.svg",
                      whatsAppNumber: "9590020684"
                    };
Share.open(shareOptions);
                 }
                 render() {
                   // alert(JSON.stringify(this.props.loginDetails))
                   return (
                     <ModalComp
                       titlecolor={color.orange}
                       closemodal={
                         this.props.closemodal && this.props.closemodal
                       }
                       visible={true}
                     >
                       <Content style={{ height: hp("85%") }}>
                         <View
                           style={{
                             height: "100%",
                             paddingTop: hp("15%"),
                             padding: 10
                           }}
                         >
                           <View style={{ flex: 0.1 }}></View>
                           <View style={styles.view1}>
                             <View style={styles.view2}>
                               <Text style={styles.txt1}>Hello</Text>
                               <Text style={styles.txt2}>
                                 {this.props.loginDetails &&
                                   this.props.loginDetails.user_name}
                               </Text>
                             </View>
                             <View style={styles.view3}>
                               <Text style={styles.txt3}>
                                 You have Added your Course
                               </Text>
                               <Text style={styles.txt4}>Successfully!</Text>
                             </View>
                             <View style={styles.view4}>
                               <Text style={styles.txt5}>
                                 Your Course Details
                               </Text>
                             </View>
                             <View style={styles.view5}>
                               <Text style={styles.txt6}>
                                 {this.props.venueform &&
                                   this.props.venueform.course[0].courseTitle}
                               </Text>
                             
                               <Text style={styles.txt5}>
                                 View More Details
                               </Text>
                             </View>
                           </View>
                           {/*<View
                             style={{
                               flex: 0.1,
                               marginTop: hp("3%"),
                               marginBottom: hp("4%")
                             }}
                           >
                             <View style={styles.view6}>
                               <Text style={styles.txt7}>Share with</Text>
                               <TouchableOpacity>
                                 <Image
                                   style={styles.socialicon}
                                   source={google_png}
                                 />
                               </TouchableOpacity>
                               <TouchableOpacity>
                                 <Image
                                   style={styles.socialicon}
                                   source={fb_png}
                                 />
                               </TouchableOpacity>
                               <TouchableOpacity>
                                 <Image
                                   style={styles.socialicon}
                                   source={insta_png}
                                 />
                               </TouchableOpacity>
                             </View>
                           </View>*/}

                           <View style={{ flex: 0.1, flexDirection: "row" }}>
                             <View style={styles.view14}>
                               <Button
                                 style={styles.cancel_btn}
                                 onPress={() =>
                                   this.props.closemodal &&
                                   this.props.closemodal()
                                 }
                               >
                                 <Text style={styles.cancel_txt}>Close</Text>
                               </Button>
                             </View>
                             <View style={styles.view14}>
                               <Button
                                 style={styles.share_btn}
                                 onPress={() => this.openShare()}
                               >
                                 <Text style={styles.cancel_txt}>SHARE </Text>
                               </Button>
                             </View>
                           </View>
                         </View>
                       </Content>
                     </ModalComp>
                   );
                 }
               }
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: hp("80%"),
    backgroundColor: color.white,
    paddingLeft: hp("2%"),
    paddingRight: hp("2%")
  },
  view1: {
    flex: 0.5,
    flexDirection: "column",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center"
  },
  view2: {
    flexDirection: "row"
  },
  view3: {
    marginTop: hp("2%"),
    marginBottom: hp("4%")
  },
  view4: {
    marginBottom: hp("3%")
  },
  view5: {
    marginTop: hp("2%"),
    marginBottom: hp("3%")
  },
  view6: {
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center"
  },

  txt1: {
    marginRight: 3,
    fontSize: hp("2%")
  },
  txt2: {
    fontWeight: "bold",
    fontSize: hp("2%")
  },
  txt3: {
    color: color.orange,
    fontSize: hp("2.5%")
  },
  txt4: {
    color: color.orange,
    fontSize: hp("3%"),
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center",
    fontWeight: "bold"
  },
  txt5: {
    fontSize: hp("2.5%"),
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center"
  },
  txt6: {
    fontSize: hp("3%"),
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center"
  },
  txt7: {
    color: color.black,
    fontSize: hp("2%"),
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center",
    marginRight: 8
  },
  socialicon: {
    width: hp("4%"),
    height: hp("4%"),
    margin: 2
  },
  view12: {
    flex: 1,
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "flex-start",
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  view14: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  view13: {
    flex: 1,
    justifyContent: "flex-end",
    alignSelf: "flex-end",
    alignContent: "flex-end",
    alignItems: "flex-end",
    flexDirection: "row"
  },
  cancel_txt: {
    fontSize: hp("2%"),
    color: color.white,
    textAlign:'center'
  },
  cancel_btn: {
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: color.orange,
    borderRadius: 5,
    padding: 10,
    height: hp("5%")
  },
  share_btn: {
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: color.blue,
    borderRadius: 5,
    padding: 10,
    height: hp("5%")
  }
});