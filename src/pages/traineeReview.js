'use strict';

import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color'
import { Container, Header, Left, Body, Right, Button, Icon, Title,Content,ActionSheet,Root,Toast,Footer ,FooterTab} from 'native-base';
import {
  StyleSheet,
  View,
  Text,
  Picker,
  TextInput,
  Image,
  TouchableOpacity,
  PermissionsAndroid,
  TouchableWithoutFeedback 
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
var BUTTONS = ["Gallery", "Camera","Cancel"];
var datearray=[{id:1,name:'Day'},{id:2,name:'Week'},{id:3,name:'Hour'},{id:4,name:'Month'},{id:4,name:'Month'},{id:4,name:'Month'},{id:4,name:'Month'}]

import FromToDuration from '../pages/fromtoduration';
var datas=[{name:'Hourly',id:1},{name:'Daily',id:2},{name:'Weekly',id:3},{name:'Monthly',id:4}]

    datas.push({name:'Hourly',id:1,activeId:1})
    datas.push({name:'Hourly',id:1,activeId:1})
    var DurationForm=[{expanded:true,title:'',content:<FromToDuration/>}]
    var MoreDetails=[{expanded:false,title:'',content:<View><Text></Text></View>}]
var CANCEL_INDEX = 3;
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
var reviewDetails = [
  { name: "Trainer Name", value: "Nicola Tesla" },
  { name: "Category", value: "Sports - Water Sports" },
  { name: "Sports", value: "Swimming" },
  { name: "Address", value: "Address of the Venue" },
  { name: "Availability", value: "Mar 2019 - Sep 2019" },
  { name: "Venue", value: "Own Venue" },
  { name: "Certification", value: "Certification Name" },
  { name: "Photos", value: "11 Photos", drop: "true" },
  { name: "Ratecard", value: "USD - 120 Per Hour" }
];
export default class TraineeReview extends Component {

  constructor(props) {
    super(props);
    DurationForm[0].title=<Text style={{fontSize:hp('2.3%'),color:color.orange}}>{props.roomname} <Text style={{color:color.black1}}>Available Duration</Text></Text>;
    MoreDetails[0].title=<Text style={{fontSize:hp('2.3%'),color:color.orange}}>{props.roomname} <Text style={{color:color.black1}}>Add More Details</Text></Text>;
    this.state = {mainformData:'',visible:false,avatarSource:'',activeId:1,selected:'key0',DurationForm:DurationForm,MoreDetails:MoreDetails,arrayImages:[],reveiewDetails:reviewDetails};
    // alert(datas.length%2);
  }
groupamenties=(array)=>{
    var ameneties="";
  var grouping={};
  var arraylist=[];
array.map((obj,key)=>{
if(grouping[obj.nameofamenties]==undefined){
  grouping[obj.nameofamenties]=[];
  ameneties+=(key==Object.keys(grouping).length-1?obj.nameofamenties:(","+obj.nameofamenties));
arraylist.push(obj.nameofamenties);
}
})
return ameneties;
}
  componentWillReceiveProps(props){
    this.setState({mainformData:props.mainformData})
    this.setState({venueform:props.venueform})
    var amenitiesData=props.ameneties&&props.ameneties.filter((obj)=>obj.circleCount>0);
    ;
    var reveiewDetails=this.state.reveiewDetails;
    // reveiewDetails[0].value=props.venueform.venue.venuename;
    reveiewDetails[0].value=props.availability.from&&props.availability.to&&(props.availability.from+"-"+props.availability.to);
    reveiewDetails[1].value=amenitiesData&&amenitiesData.length>0&&amenitiesData.map((obj)=>obj.amenities_name).join(',');
    // reveiewDetails[2].value=props.venueform.facility.trn_venue_room_address;
    // reveiewDetails[4].value=props.venueform.facility.trn_venue_room_seats;
    // reveiewDetails[5].value=props.ameneties.ameneties.length>0?this.groupamenties(props.venueform.ameneties):'';
    reveiewDetails[2].value=props.venueform.formArray.length+" Photos";
    reveiewDetails[3].value=props.venueform.pricedetails.venue_price_currency+" "+props.venueform.pricedetails.venue_price_amt+" - "+props.venueform.pricedetails.venue_price_name;
    this.setState({reveiewDetails});
  }
  onValueChange=(value)=>{
    this.setState({
      selected: value
    });
  }
  renderValdations=(data)=>{
    var errortext="";
      data.map((obj,key)=>{
         errortext+=key==data.length-1?". "+obj.errmsg:". "+obj.errmsg+"\n"
      })
      return errortext;
  }
  editclick=()=>{
   
    this.props.loadedit();
  }
  submitlogin=()=>{
// alert();
    var self=this;
var filtererror=this.props.validations.filter((obj)=>obj.state==false);
// alert(JSON.stringify(this.state.venueform.tagdetails));
if(filtererror.length>0){
this.props.error(this.renderValdations(filtererror));
}else{

    var mainformData=this.state.mainformData;
    var venueform=this.state.venueform;
    mainformData.append('existImages',JSON.stringify(this.props.venueform.formArray.filter((obj)=>obj.exist)))
    mainformData.append('venue',JSON.stringify(venueform.venue));
    mainformData.append('tagdetails',JSON.stringify(venueform.tagdetails))
    mainformData.append('pricedetails',JSON.stringify(venueform.pricedetails))
    mainformData.append('specific',JSON.stringify(venueform.specific))
    mainformData.append('ameneties',JSON.stringify(venueform.ameneties))
    mainformData.append('availability',JSON.stringify(venueform.availability))
    mainformData.append('purpose',JSON.stringify(venueform.purposes.map((obj)=>{return {venue_act_id:obj.id,purposedetails:obj.value.join(',')}})));
    // alert('Sucesss');
    // alert(JSON.stringify(venueform.venue))
    this.props.submitVenuData(mainformData,venueform.venue.venue_id?venueform.venue.venue_id:null);
  }
  }
  componentWillMount(){
this.props.headertext({headertext:<Text style={{fontSize:hp('1.7%'),fontWeight:'bold',color:color.blue}}>Review</Text>});
    this.props.labeltext({labeltext:<Text>Before submitting <Text style={{color:color.orange,fontWeight:'bold'}}> Review </Text> the details </Text>});
  }
  render(){

    return(
      <Content>

        <View style={{padding:20}}>
        {this.props.facilitytypedata&&this.props.commonArray&&this.props.commonArray.concat(this.props.facilitytypedata&&this.props.facilitytypedata.specDetails&&this.props.facilitytypedata.specDetails.length>0&&this.props.facilitytypedata.specDetails).map((obj,key)=>{
return(
          <View key={key} style={styles.row1}>
            <View style={{flex:0.4}}>
            <Text style={styles.fonthead}>{obj.spec_det_name}</Text>
            </View>
            <View style={{flex:0.6}}>
            <Text numberOfLines = {2} style={styles.font_headvalue}>{obj.spec_det_datavalue1}</Text>
            </View>
          </View>
          )
        })}
             {this.state.reveiewDetails.map((obj,key)=>{
return(
          <View key={key} style={styles.row1}>
            <View style={{flex:0.4}}>
            <Text style={styles.fonthead}>{obj.name}</Text>
            </View>
            <View style={{flex:0.6}}>
            <Text  style={styles.font_headvalue}>{obj.value}</Text>
            </View>
          </View>
          )
        })}
          
          <View style={styles.submitbox}>
     <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-start'}}>
       <Button  onPress={this.editclick} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
            <Text style={styles.actionbtntxt}>EDIT</Text>
          </Button>
          </View>
          <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button onPress={this.submitlogin} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
           <Text style={styles.actionbtntxt}>SUBMIT</Text>
          </Button>
          </View>
     </View>
          
        </View>
     
      </Content>
   
      )
  }
}
const styles={
  row1:{
    flexDirection:'row',
    marginBottom:20
  },
  submitbox:{
    flex:1,
    flexDirection:'row',
  },
  actionbtn:{
    borderRadius:5,
    width:hp('12%'),
    justifyContent:'center',
  },
  actionbtntxt:{
    textAlign:'center',
    color:color.white,
    fontSize:hp('2.3%')
  },
  fonthead:{
    fontSize:hp('2.2%')
  },
  font_headvalue:{
    fontSize:hp('2.2%'),
    fontWeight:'bold'

  }
}