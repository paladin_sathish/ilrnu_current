import React, { Component } from 'react';
import { View, Text, StyleSheet,TouchableOpacity,ScrollView} from "react-native"; 
import color from "../Helpers/color";

import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,Textarea,Label,
  Button,
  ScrollableTab,
  Icon,
  Form
} from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import Schedule1 from "../components/schedule1";
import SplitPopUp from "./splitPopUp";
import SplitView from "../components/splitview"; 
import DateFunctions from "../Helpers/DateFunctions";
import moment from "moment";




export default class SlotsBooking extends Component {
                 constructor(props) {
                   super(props);
                   
                   this.setSchedule = this.setSchedule.bind(this);
                   this.setActiveSplit = this.setActiveSplit.bind(this);

                   this.editClicked = this.editClicked.bind(this);
                   this.deleteClicked = this.deleteClicked.bind(this);

                   this.state = {
                     selected: null,
                     paxContent:'',
                     isSplitPopUp: false,
                     splitlist: "",
                     editid:'',
                     splitData: [],
                     list: [],
                     splitSlots: [],
                     allSlots: [],
                     edit: false,
                     isShowEdit:false,
                     editIndex:null,
                     editName:null,
                     activeTab:0,
                     selectedSlots:[]
                   };

                 }  

               

                 componentWillMount = () => {
                   //get list of splits 
console.log('slotsstate',this.props.slotsBookingState )
                    if (
                      this.props.slotsBookingState != null &&
                      this.props.slotsBookingState!= this.state
                    ) {
                      console.log("if");
                      this.state = this.props.slotsBookingState;
                    } 
                     
                 };   

                 


                   componentWillReceiveProps = (props) => {
                   //get list of splits 
                   // alert(JSON.stringify(previous));
                   // console.log('newslots props',this.props.slotsBookingState)
  
                    // if (
                    //   this.props.slotsBookingState != null 
                    //  ) {
                    //   console.log("inside");
                    //   this.state = this.props.slotsBookingState;
                    //  alert(JSON.stringify(this.props.slotsBookingState));
                     
                    // } 
                    // alert(JSON.stringify(this.props._addForm));
                    // if(this.props._addForm.toTime !=props._addForm.toTime || this.props._addForm.fromTime !=props._addForm.fromTime){
                      //time changed
                      if(props._addForm.toTime!=''){
                      this.generateSlots(); 
                      }
                      // this.setState({splitData:[]})
                    // }

                    // alert()
                    // if(props.splitData){
                      // this.setState({splitData:props.splitData});
                    // }
                     
                 };

                 changePaxContent=(data)=>{
                   this.setState({
                     paxContent:data
                   })
                  
                 }

                 generateSlots=()=>{
                      // alert(this.props._addForm.fromTime);
                      var fromDate = this.props._addForm.fromDate.concat(
                     " ",
                    DateFunctions.converttime24Convertor(this.props._addForm.fromTime)
                   );

                    var toDate = this.props._addForm.fromDate.concat(
                     " ",
                    DateFunctions.converttime24Convertor(this.props._addForm.toTime)
                   );
                      var result = DateFunctions.getHoursInterval(
                        DateFunctions.getMobileDate(fromDate),
                        DateFunctions.getMobileDate(toDate),
                        60
                      );  

                      this.setState({ list: result || [] });
                      this.setState({ allSlots: result || [] });
                      
                 }


                 componentDidMount = () => {
                   this.generateSlots();
                   // alert(JSON.stringify(this.props.splitData));
                   this.setState({splitData:this.props.splitData},function(){
                     this.updateListProps();
                   })
                 };
                 

                 addSplit = (data)=>{
                     // alert('');
                     const {edit,editIndex} =this.state; 
                    
                      var selectedSlots = this.state.selectedSlots;
                      for (let j = 0; j < data.value.length; j++) {
                      
                         selectedSlots.push(data.value[j])
                      }
                     this.setState({selectedSlots})

                   var splitData =  this.state.splitData;
                   if (edit && editIndex>=0){
                    console.log('if')
                     for (let index = 0; index < splitData.length; index++) {
                       if(index==editIndex){
                         splitData[index]=data
                       }
                     }
                       this.setState({
                     splitData
                   },()=>{
                    console.log('callback')
                   this.updateListProps()
                   })   

               

                   }else{  
                     console.log('e;se')
                      splitData = [...splitData, data];
                      this.setState(
                        {
                          splitData
                        },
                        () => {
                          this.updateListProps();
                        }
                      ); 

                   } 
                     console.log('addsplitPop')
                 }


                 unVisibleEditSelected=(editPicked)=>{
                    let list = this.state.list;

                    for (let index = 0; index < list.length; index++) {
                      var id = list[index].id;
                      if(id==editPicked.id)
                      list[index].visible = null;

                    }
                    this.setState({ list });

                 } 
                

                   updateEditProps =(index)=>{

                     var tetmp=this.state.splitData &&
                        this.state.splitData
                          .filter((obj3, i) => i != index);
                          console.log("filter", tetmp);

                    let list = this.state.list;

                    for (let index = 0; index < list.length; index++) {
                      var id = list[index].id;
                  
                         tetmp.length>0&& tetmp.map(obj2 => {
                            var findIndex =
                              obj2.value.length > 0 &&
                              obj2.value.findIndex(obj => obj.id == id);
                             console.log('match',findIndex)
                            if(findIndex !=-1){
                              list[index].checked = false;
                              list[index].edit = false;
                            }
                          });
                    }
                    this.setState({ list });

                    
                 }


                  _eraseEditSelected=()=>{

                    if(this.state.edit){
                      this.setState({isShowEdit:false})
                    }
                    } 

                    
                    slotsSubmit=()=>{ 
                        const {splitData,list,selectedSlots,allSlots}=this.state;
 
 let passData = {
   splitData: splitData.length > 0? splitData:[],
   paxContent: this.state.paxContent,
   selectedSlots: selectedSlots,
   allSlots: allSlots,
   slotsBookingState:this.state
 }; 



                      if(splitData.length>0)
                      this.props.getSlots(passData)
                      else
                      this.props.getSlots(
                       passData
                      );
                       
                    }

                 _eraseSelected=()=>{

                    this.setState({  edit: false });
                     let list = this.state.list;
                    for (let index = 0; index < list.length; index++) {
                      list[index].checked=false;
                      list[index].edit=false;
                    }
                    this.setState({list,editName:null}) 
                   
                 }

                 updateListProps =()=>{

                 
                    let list = this.state.list;

                    for (let index = 0; index < list.length; index++) {
                      var id = list[index].id;
                  
                      this.state.splitData &&
                        this.state.splitData.map(obj2 => {
                          var findIndex =
                            obj2.value.length > 0 &&
                            obj2.value.findIndex(obj => obj.id == id);
                        
                          if (findIndex == -1) {
                          } 
                          else if (findIndex >= 0) {
                            list[index].visible = false;
                            list[index].checked = false;
                          } else console.log("else part");
                        });
                    }
                    // var splitData=this.state.splitData;
                    this.setState({ list },function(){
                      var self=this;
                      setTimeout(()=>{
                      // self.slotsSubmit();

                      },500)
                    });
                    if(this.state.edit)
                    { 
                      this.setState({isShowEdit:false})
                    }
                      // this.slotsSubmit();

                 }

 changeActiveTab=({ref,i})=>{
   // console.log(i)
   // alert(i);
   // this.
   this.props.sendSlot_type&&this.props.sendSlot_type(i+1)
 }

                 editAddForm =()=>{ 
                   this.props._editBusinessForm()
                 }
                 setSchedule = data => {
                 
                 };
                 onEdit;
                 setActiveSplit = () => {
                   isSplitPopUp = this.state.isSplitPopUp;
                   isSplitPopUp = true;
                   this.setState({ isSplitPopUp });
                 };

                 editClicked = (data,index) => { 

                  var withOutEdit=this.state.splitData.filter((obj3,i)=>i!=index);
                      let list = this.state.list;
                      for (let index = 0; index < list.length; index++) {
                        var labelIndex = list[index].label;

                        var findIndex =
                          data.value.length > 0 &&
                          data.value.findIndex(obj => obj.label == labelIndex);
                     
                         if (findIndex !=-1) {
                          list[index].checked = true;
                          list[index].edit = true;
                        } 
                       
                       

                      }  
                      

                      this.setState({ list }); 
                      this.updateEditProps(index)
                      this.setState({edit:true,isShowEdit:true,editIndex:index,editName:data.slots_name,editid:data.id})

                 };

                 deleteClicked = (data,index) => {
               

                 
                   var splitData =  this.state.splitData;
                   splitData =splitData.filter((item,i)=> i !=index);
                   
                   this.setState({
                     splitData
                   },()=>{

                      let list = this.state.list;
                    
                      for (let index = 0; index < list.length; index++) {
                       
                        var id = list[index].id;
                         
    var findIndex = data.value.length>0 && data.value.findIndex(obj => obj.id == id);

    if (findIndex == -1) {
    } 
    else if(findIndex>=0){
 list[index].visible = true;
  list[index].checked = false;
    }
    else
    console.log('else part')

                      }
                      this.setState({ list });

                   })   

                 


                 
                 };

                 render() {  

                 console.log('type34',this.props._addForm.selectedType.id);
                  const {_type} = this.props; 
            
                  console.log(this.state.list)
                    return (
                      <ScrollView>

                            
                          <List style={{ backgroundColor: "white" }}>

                            <ListItem>
                              <Body>
                                <Text style={{ fontSize: 18 }}>
                                  Business Hours
                                </Text>
                                <Text note numberOfLines={1}>
                                  <Icon
                                    type="FontAwesome"
                                    name="clock-o"
                                    style={{
                                      color: "#3b5998",
                                      fontSize: 13,
                                      paddingTop: 3
                                    }}
                                  />
                                  <Text> </Text>
                                  {this.props._addForm.fromTime} <Text> </Text>
                                  <Icon
                                    type="FontAwesome"
                                    name="long-arrow-right"
                                    style={{
                                      color: "#3b5998",
                                      fontSize: 13,
                                      paddingTop: 3
                                    }}
                                  />
                                  <Text> </Text>
                                  {this.props._addForm.toTime}
                                </Text>
                              </Body>
                              <Right>
                                <Button
                                  rounded
                                  style={{ backgroundColor: "#FF9008" }}
                                  onPress={() => this.editAddForm()}
                                >
                                  <Icon
                                    type="FontAwesome"
                                    name="pencil"
                                    style={{
                                      color: "#fff"
                                    }}
                                  />
                                </Button>
                              </Right>
                            </ListItem>

                            <ListItem>
                              <Body>
                                <Text style={{ fontSize: 18 }}>
                                  Availability Dates
                                </Text>
                                <Text note numberOfLines={1}>
                                  <Icon
                                    type="FontAwesome"
                                    name="clock-o"
                                    style={{
                                      color: "#3b5998",
                                      fontSize: 13,
                                      paddingTop: 3
                                    }}
                                  />
                                  <Text> </Text>
                                  {this.props._addForm.fromDate} <Text> </Text>
                                  <Icon
                                    type="FontAwesome"
                                    name="long-arrow-right"
                                    style={{
                                      color: "#3b5998",
                                      fontSize: 13,
                                      paddingTop: 3
                                    }}
                                  />
                                  <Text> </Text>
                                  {this.props._addForm.toDate}
                                </Text>
                              </Body>
                              <Right />
                            </ListItem>
                            <ListItem>
                              <Body>
                                <Text style={{ fontSize: 18 }}>
                                  Exclude Days
                                </Text>
                                <Text note numberOfLines={1}>
                                  {this.props._addForm.exclude.length <= 0 &&
                                    "None"}
                                  {this.props._addForm.exclude &&
                                    this.props._addForm.exclude.map(item => {
                                      return <Text>{item.name} </Text>;
                                    })}
                                </Text>
                              </Body>
                              <Right />
                            </ListItem>
                            <ListItem>
                              <Body>
                                <Text style={{ fontSize: 18 }}>
                                  Venue Type
                                </Text>
                                <Text note numberOfLines={1}>
                                  {this.props._addForm.selectedType.id==1?"Normal":(this.props._addForm.selectedType.id==2?`Pax (${this.props._addForm.paxList.length})`:`Seat (${this.props._addForm.seatList.length})`)}
                                </Text>
                              </Body>
                              <Right />
                            </ListItem>
                          </List>
                          <View
                            style={{ height: 8, backgroundColor: "#edeef2" }}
                          />
                          
                          

                        
                        {
                            (this.props._addForm.selectedType.id == 1||this.props._addForm.selectedType.id == 3) &&this.props.checkhourly&&(
                              <Tabs
                                tabBarUnderlineStyle={{
                                  borderBottomWidth: 4,
                                  borderBottomColor: "#5067FF"
                                }}
                                initialPage={this.props._addForm.slot_type-1}
                                    onChangeTab={this.changeActiveTab}
                                renderTabBar={() => (
                                  <ScrollableTab
                                    style={{ backgroundColor: "white" }}
                                  />
                                )}
                              >
                                <Tab
                                  style={styles.tab}
                                  heading="All Slots"
                                  tabStyle={{ backgroundColor: "white" }}
                                  textStyle={{ color: "orange" }}
                                  activeTabStyle={{
                                    backgroundColor: "transparent"
                                  }}
                                  activeTextStyle={{
                                    color: "#5067FF",
                                    fontWeight: "normal"
                                  }}
                                >
                                  <React.Fragment style={styles.tab1}>
                                    <View style={styles.container}>
                                      <Schedule1
                                        boxColor="#edeef2"
                                        isOnClick={false}
                                        list={this.state.allSlots}
                                        static={true}
                                      />
                                    </View>
                                  </React.Fragment>
                                </Tab>
                                <Tab
                                  style={styles.tab}
                                  heading="Split Slots"
                                  tabStyle={{ backgroundColor: "white" }}
                                  textStyle={{ color: "orange" }}
                                  activeTabStyle={{
                                    backgroundColor: "transparent"
                                  }}
                                  activeTextStyle={{
                                    color: "#5067FF",
                                    fontWeight: "normal"
                                  }}
                                >
                                

                                  <SplitPopUp
                                    _addSplit={this.addSplit}
                                    list={this.state.list}
                                    checkedSlots={this.state.splitData}
                                    edit={this.state.edit}
                                    editid={this.state.editid}
                                    isShowEdit={this.state.isShowEdit}
                                    editIndex={this.state.editIndex}
                                    editName={this.state.editName}
                                    eraseSelected={this._eraseSelected}
                                    eraseEditSelected={this._eraseEditSelected}
                                    unVisibleEditSelected={
                                      this.unVisibleEditSelected
                                    }
                                  /> 

                                    <SplitView
                                    data={this.state.splitData}
                                    onDelete={this.deleteClicked}
                                    onEdit={this.editClicked}
                                    editIndex={this.state.editIndex}

                                  />
                                  
                                </Tab>
                              </Tabs>
                            )}
                            <View
                            style={{
                              flex: 1,
                              flexDirection: "row",
                              justifyContent: "flex-end",
                              alignItems: "flex-end",
                              paddingRight: 30,
                              paddingVertical: 15
                            }}
                          >
                            <Button
                              onPress={() => this.slotsSubmit()}
                              style={styles.actionbtn}
                            >
                              <Text style={styles.actionbtntxt}>Submit</Text>
                            </Button>
                          </View>
                      </ScrollView>
                    );
                 }
               }


const styles = StyleSheet.create({
  container: {
    backgroundColor: "#edeef2",
    display: "flex",
    justifyContent: "flex-start"
  },
  head: {
    backgroundColor: "#edeef2"
  },
  tab: {
    backgroundColor: "#edeef2"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  }
});