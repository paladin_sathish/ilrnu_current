import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Tabs,
  Tab,
  TabHeading,
  Content,
  Root
} from "native-base";
import {
  View,
  Image,
  StatusBar,
  Picker,
  Text,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  BackHandler,
  ToastAndroid
} from "react-native";
import color from "../Helpers/color";
import links from "../Helpers/config";
import SubHeaderComp from "../components/subHeader";
import TraineeType from "../pages/traineeType";
import TraineeCategory from "../pages/traineeCategory";
import TraineeDetails from "../pages/traineeDetails";
import { Actions } from "react-native-router-flux";
import Chair from "../images/svg/chair.png";
import DeskF from "../images/svg/deskf.png";
import Access from "../images/svg/accessibility.png";
import Laptop from "../images/svg/laptop.png";
import Seats from "../images/svg/seats.png";
import Bathroom from "../images/svg/bathroom.png";
import Parking from "../images/svg/parking.png";
import Coffee from "../images/svg/coffee-cup.png";
import Door from "../images/svg/door.png";
import AmenitiesPNG from "../images/TabPNG/AmenitiesPNG.png";
import AvailabilityPNG from "../images/TabPNG/AvailabilityPNG.png";
import FacilityPNG from "../images/TabPNG/FacilityPNG.png";
import PricecardPNG from "../images/TabPNG/PricecardPNG.png";
import ReviewPNG from "../images/TabPNG/ReviewPNG.png";
import UploadphotoPNG from "../images/TabPNG/UploadphotoPNG.png";
import TaglistPNG from "../images/TabPNG/TaglistPNG.png";
import VenuetypePNG from "../images/TabPNG/VenuetypePNG.png";
import AsyncStorage from "@react-native-community/async-storage";
import DynamicPagination from "../components/DynamicPagination";
import Toast from "react-native-simple-toast"; 

import CourseDetail from '../pages/courseDetail';
import CourseDetail1 from "../pages/courseDetail1";
import CoursePrice from '../pages/coursePrice';
import CourseChooseVenues from "./courseChooseVenues";
import CourseReview from "./courseReview";
import CourseMode from "./courseMode";
import LoadAPIPost from "../Helpers/xhrapi";
import CourseSuccess from '../pages/courseSuccess';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
var datearray = [
  { id: 1, name: "USD" },
  { id: 2, name: "INR" },
  { id: 3, name: "MYR" }
];
var datasHourlyWeekly = [
  { name: "Hourly", id: 1, keyname: "Hour" },
  { name: "Daily", id: 2, keyname: "Day" },
  { name: "Weekly", id: 3, keyname: "Week" },
  { name: "Monthly", id: 4, keyname: "Month" }
];

const datas = [
  { name: "Seating", id: 1, circleCount: null, active: false, icon: Chair },
  { name: "Physical\nInfrastructure", id: 2, icon: DeskF },
  { name: "Accessibility", id: 3, icon: Access },
  { name: "Training Equipments", id: 4, icon: Laptop },
  { name: "IT Infra", id: 5, icon: DeskF },
  { name: "Resource", id: 6, circleCount: null, active: false, icon: Seats },
  { name: "Toilets", id: 7, icon: Bathroom },
  { name: "Parking", id: 8, icon: Parking },
  { name: "Pantry", id: 9, icon: Coffee },
  { name: "Additional Rooms", id: 10, icon: Door }
];

datas.push({ name: "Hourly", id: 1, activeId: 1 });
datas.push({ name: "Hourly", id: 1, activeId: 1 });
const newtags = [
  {
    id: 1,
    name: "Add (or) Tag the related General Keywords",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false },
      { id: 4, name: "Special Tag Items display here", state: false },
      { id: 5, name: "Corporate Trainings", state: false }
    ]
  },
  {
    id: 2,
    name: "What category on venue type",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false },
      { id: 4, name: "Special Tag Items display here", state: false }
    ]
  },
  {
    id: 3,
    name: "Suitable Training- IT / Soft Skill / Handson",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false }
    ]
  }
];
// const newtags1=[{id:1,name:'Add (or) Tag the related General Keywords',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:2,name:'What category on venue type',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:3,name:'Suitable Training- IT / Soft Skill / Handson',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]}]
var commonDataForFacility = [
  {
    spec_det_id: "idname",
    venue_spec_id: 1,
    spec_det_name: "Sport Name",
    spec_det_sortorder: 0,
    spec_det_datatype1: "picker",
    spec_det_datavalue1: "1",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    picker_data: [],
    validation: [{ name: "required" }, { name: "minLength", params: 30 }],
    error: null,
    errormsg: ""
  },
  {
    spec_det_id: "idname1",
    venue_spec_id: 1,
    spec_det_name: "Experience (Years)",
    spec_det_sortorder: 0,
    spec_det_datatype1: "exp",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 30 }],
    error: null,
    errormsg: ""
  },

  {
    spec_det_id: "idname2",
    venue_spec_id: 1,
    spec_det_name: "Experience (Months)",
    spec_det_sortorder: 0,
    spec_det_datatype1: "exp",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 30 }],
    error: null,
    errormsg: ""
  },

  {
    spec_det_id: "idname3",
    venue_spec_id: 1,
    spec_det_name: "Certification",
    spec_det_sortorder: 0,
    spec_det_datatype1: "btn_address1",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 300 }],
    error: null,
    errormsg: ""
  },
  {
    spec_det_id: "idname4",
    venue_spec_id: 1,
    spec_det_name: "Achievements",
    spec_det_sortorder: 0,
    spec_det_datatype1: "textarea",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 1000 }],
    error: null,
    errormsg: ""
  },
  {
    spec_det_id: "idname5",
    venue_spec_id: 1,
    spec_det_name: "Location",
    spec_det_sortorder: 0,
    spec_det_datatype1: "textareamap",
    spec_det_datavalue1: "",
    spec_det_datatype2: "",
    spec_det_datavalue2: "",
    spec_det_datatype3: "",
    spec_det_datavalue3: "",
    validation: [{ name: "required" }, { name: "minLength", params: 300 }],
    error: null,
    errormsg: ""
  }
];

const mode = [
  {
    amenities_id: 162,
    amenities_name: "ILT",
    amenities_icon: "https://www.ivneu.com/ivneuUploadsDir/toilet-55.png",
    amenities_array: [1, 2, 3, 4]
  },
  {
    amenities_id: 163,
    amenities_name: "Live Stream",
    amenities_icon: "https://www.ivneu.com/ivneuUploadsDir/toilet-55.png",
    amenities_array: [1, 2, 3, 4]
  },
  {
    amenities_id: 164,
    amenities_name: "Video Base",
    amenities_icon: "https://www.ivneu.com/ivneuUploadsDir/toilet-55.png",
    amenities_array: [1, 2, 3, 4]
  }
];
export default class CourseForm extends Component {
                 constructor(props) {
                   super(props);
                   // Actions.corporateform();
                   var venueform = JSON.parse(
                     JSON.stringify(require("../Helpers/traineeform.json"))
                   );
                   this.state = {
                     courseState: null,
                     chooseVenueData: "",
                     chooseVenueState: null,
                     autosave: true,
                     init: true,
                     courseData: null,
                     list1: null,
                     splitData: null,
                     availabilityState: null,
                     courseDetailData: null,
                     courseDetailState: null,
                     courseDetail1Data: null,
                     courseDetail1State: null,
                     purposeData: null,
                     showSkip: null,
                     edit: false,
                     loginDetails: this.props.navigation.state.params.logindata,
                     facilityvalidations: null,
                     facilityIndex: 0,
                     currentIndex: 0,
                     facitlityactive: null,
                     traineemodedata: null,
                     activeparent: "tab1",
                     venuetypetext: "",
                     venueheadertext: "",
                     list: [
                       { id: "1", name: "tab1", image: VenuetypePNG },
                       { id: "2", name: "tab2", image: FacilityPNG },
                       { id: "3", name: "tab3", image: AvailabilityPNG },
                       { id: "4", name: "tab4", image: AmenitiesPNG },
                       { id: "5", name: "tab5", image: TaglistPNG },
                       { id: "6", name: "tab6", image: PricecardPNG },
                       { id: "7", name: "tab7", image: UploadphotoPNG },
                       { id: "8", name: "tab8", image: ReviewPNG },
                     ],
                     _scrollToBottomY: 0,
                     facilityData: {
                       roomname: "",
                       seats: "",
                       floor: "",
                       address: "",
                       landmark: "",
                       mobile: "",
                       mail: "",
                     },
                     venuetypedata: null,
                     facilitytypedata: null,
                     availability: {
                       type: null,
                       days: null,
                       from: null,
                       to: null,
                       moredetails: null,
                     },
                     ameneties: null,
                     venTags: null,
                     venueprice: null,
                     venueform: venueform,
                     venueformData: new FormData(),
                     validations: [
                       {
                         id: 1,
                         name: "venuetype",
                         errmsg: "Choose Venue Type",
                         state: false,
                       },
                       {
                         id: 2,
                         name: "venuetype",
                         errmsg: "Choose Venue Facility",
                         state: false,
                       },
                       {
                         id: 3,
                         name: "facility",
                         errmsg: "Please Fill up facility details",
                         state: false,
                       },
                       {
                         id: 4,
                         name: "availability",
                         errmsg: "Please Fill up avaialability details",
                         state: false,
                       },
                       {
                         id: 5,
                         name: "price",
                         errmsg: "Please Fill up price Details",
                         state: false,
                       },
                       {
                         id: 6,
                         name: "purposes",
                         errmsg: "Please Choose purposes and actions",
                         state: false,
                       },
                     ],
                   };
                   if (this.props.navigation.state.params.welcomemsg) {
                     Toast.show(
                       this.props.navigation.state.params.welcomemsg,
                       Toast.LONG
                     );
                   }
                 }
                 goToRight = () => {
                   // this.scroll.scrollTo({ x: 0, y: 0, animated: true });
                 };
                 changeTab = (data) => {
                   // alert(data);
                   var list = this.state.list;
                   var filterrecords = list.filter((obj) => obj.id == data + 1);

                   this.setState({ currentIndex: data });
                   this.setState({ activeparent: filterrecords[0].name });
                 };

                 async componentWillMount() {
                   const data = await AsyncStorage.getItem("loginDetails");
                   var parsedata = JSON.parse(data);
                   this.setState({ loginDetails: parsedata });
                   var venueform = this.state.venueform;
                   venueform.userId = parsedata.user_id;
                   this.setState({ venueform }, () => {});
                   if (!this.props.edit) {
                     this.getTrainerId();
                   }
                 }

                 componentDidMount() {
                   if (this.props.Course && this.props.edit) {
                     //  alert('edit')
                     var venueform = this.state.venueform;
                     var editData = this.props.Course;
                     venueform.userId = this.state.loginDetails.user_id;
                     venueform.trainerId = this.props.Course.trainer_id;
                     venueform.course[0].courseId = this.props.Course.course_id;
                     var venueformData = this.state.venueformData;

                     venueformData.set(
                       "trainerId",
                       JSON.stringify(venueform.trainerId)
                     );

                     venueformData.set(
                       "userId",
                       JSON.stringify(venueform.userId)
                     );

                     this.setState({ edit: true, autosave: false });

                     console.log("willmountcoruseedit ", this.props.Course);

                     this.sendtraineemodetypedata(
                       {
                         ModeOfTrainingId: editData.course_mode,
                         ModeOfTrainingName: "",
                       },
                       true
                     );

                     var obj = {
                       courseTitle: editData.course_title,
                       languageId: editData.language_id,
                       trainingLevelId: editData.training_level_id,
                     };

                     var myphotos =
                       editData.uploadData &&
                       editData.uploadData.map((obj,index) => {
                        
                         return {
                           uid: obj.trainer_upload_id,
                           name: obj.trainer_upload_path.split("/")[
                             obj.trainer_upload_path.split("/").length - 1
                           ],
                           status: "done",
                           url: obj.trainer_upload_path,
                           exist: true,
                         };
                       });

                     // this.setState({formimagearray:null,}) 
                     console.log('log',myphotos)

                     venueform.formArray = myphotos;
                     venueform.course[0].existImages =JSON.stringify(myphotos);
                     var venueformData = this.state.venueformData;
                     venueformData.set("existImages", JSON.stringify(myphotos));

                     this.setState({
                       courseData: obj,
                       courseState: "",
                     });

                     venueform.course[0].trainingLevelId = obj.trainingLevelId;
                     venueform.course[0].languageId = obj.languageId;
                     venueform.course[0].courseTitle = obj.courseTitle;

                     venueform.keytoupdate = [
                       "TRAINER_UPLOAD",
                       "COURSE_DETAILS",
                       "BATCH_DATA",
                       "TRAINING_VENUE",
                     ];

                     venueformData.set("userId", venueform.userId);
                     venueformData.set("trainerId", venueform.trainerId);
                     venueformData.set(
                       "keytoupdate",
                       JSON.stringify(venueform.keytoupdate)
                     );
                     
                     venueformData.set("course", JSON.stringify(venueform.course));

                     //tab3
                     var obj1 = {
                       courseDesc: editData.course_desc,
                       courseOutline: editData.course_outline,
                       courseFAQ: editData.course_faq,
                     };

                     this.courseDetail1Submit(obj1, "");
                     //end

                     //tab4
                     if(editData.batchDetailsData.length>0){
                         var obj2 = {
                           batchSeatCount: editData.batchDetailsData[0].batch_seat_count.toString(),
                           batchDurationHours: editData.batchDetailsData[0].batch_duration_hours.toString(),
                           keyHiglights:
                             editData.batchDetailsData[0].batch_key_higlights,
                           batchFee: editData.batchDetailsData[0].batch_fee.toString(),
                           batchFeeCurrency:
                             editData.batchDetailsData[0].batch_fee_currency,
                           batchStartDate:
                             editData.batchDetailsData[0].batch_startDate,
                           batchEndDate:
                             editData.batchDetailsData[0].batch_endDate,
                         };

                         console.log("coursedt", obj2);

                         this.courseDetailSubmit(obj2, "");

                     }
                   

                     //end

                     //tab5
                     // var obj3 = {
                     //   venueType:
                     //     editData.batchDetailsData[0].batch_venue_type,
                     //   venueId: editData.batchDetailsData[0].venue_id
                     // };

                     // if (editData.batchDetailsData[0].batch_venue_type==1)
                     // {
                     //   obj3.venueObj =
                     //     editData.batchDetailsData[0].batchVenueDetails.ownvenueList[0];

                     // }else
                     // { obj3.venueObj =
                     //     editData.batchDetailsData[0].batchVenueDetails.bookedVenueList[0];

                     // }

                     //   this.sendPurposes(obj3, "");

                     //end

                     // this.venueImages({
                     //   formimagearray: editData.uploadData,
                     //   localimages: editData.uploadData,
                     //   coursePrice: obj,
                     //   courseState: this.state.courseState
                     // });

                     //  //load specDetails
                     //  this.sendfacilitytypedata(
                     //    {
                     //      SpecificId: editData.trainer_spec_id,
                     //      SpecificName: editData.training_spec_name
                     //    },
                     //    true
                     //  );

                     //   var commonData = JSON.parse(
                     //     JSON.stringify(this.state.commonArray)
                     //   );

                     //  commonData[0].spec_det_datavalue1 = editData.sub_spec_id;
                     //  commonData[1].spec_det_datavalue1 = editData.trainer_exp_year.toString();
                     //  commonData[2].spec_det_datavalue1 =
                     //    editData.trainer_exp_month.toString();

                     //  commonData[3].spec_det_datavalue1 =
                     //    editData.trainer_certification_details;

                     //  commonData[4].spec_det_datavalue1 =
                     //    editData.trainer_achivements;

                     //  commonData[5].spec_det_datavalue1 =
                     //    editData.trainer_address;

                     //         commonData[5].spec_det_datavalue2 =
                     //           editData.trainer_location;
                     //              commonData[6].spec_det_datavalue1 =
                     //                editData.tariner_landmark;

                     //  this.setState({commonArray});
                     //  this.sendfaciltiydata(
                     //    this.state.facilitytypedata,
                     //    commonData,
                     //    true,
                     //    "dontupdate"
                     //  );

                     this.setState({
                       venueform
                     });
                   }
                 }

                 componentWillUnmount() {
                   BackHandler.removeEventListener(
                     "hardwareBackPress",
                     this.handleBackButton
                   );
                 }

                 handleBackButton = () => {
                   if (!this.state.loading) {
                     // Actions.pop();
                   } else {
                     return true;
                   }
                   // ToastAndroid.show('Back button is disabled', ToastAndroid.SHORT);
                 };

                 getmoredetails = (data) => {
                   var availability = this.state.availability;
                   availability.moredetails = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_moredetails = data;
                   this.setState({ venueform });
                 };

                 getBusinessForm = (data, totalState) => {
                   console.log("businessstae", totalState);

                   this.setState({
                     availabilityState: totalState,
                   });
                   var availability = this.state.availability;
                   availability.businessForm = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.businessForm = data;
                   this.setState({ venueform });

                   console.log("avaiala", this.state.availability);
                 };

                 getSlots = (data) => {
                   console.log("avaialbilyt state", data.availabilityState);

                   var availability = this.state.availability;
                   availability.slots = data.splitData;
                   availability.businessForm = data.businessForm;
                   availability.paxContent = data.paxContent;
                   this.setState({ availability });

                   var venueform = this.state.venueform;
                   venueform.availability.slots = data.splitData;
                   venueform.availability.businessForm = data.businessForm;
                   venueform.availability.paxContent = data.paxContent;
                   this.setState({ venueform });

                   console.log(
                     "business form",
                     this.state.availability.businessForm
                   );

                   const {
                     from,
                     to,
                     minTime,
                     maxTime,
                     availableFrom,
                     availableTo,
                     selectedType,
                     checkboxes,
                     activeType,
                   } = this.state.availability.businessForm;

                   var obj = {
                     activeobj: this.state.availability.type,
                     hourobj: null,
                     businessform: this.state.availability.businessForm,
                     SplitSlots: data.splitData,
                     selectedSlots: data.selectedSlots,
                     slotType: selectedType,
                     hourSlots: data.allSlots,
                     moreDetails: data.paxContent,
                   };

                   console.log("next", obj);
                   this.setState(
                     {
                       availabilityState: data.availabilityState,
                       splitData: data.splitData,
                     },
                     () => {
                       this.gotoNext();
                     }
                   );
                 };

                 renderActiveTab() {
                   return <View style={styles.activeparent}></View>;
                 }

                 //change label text
                 changeLabelText = (data) => {
                   this.setState({ venuetypetext: data.labeltext });
                 };

                 //change header text
                 changeHeaderText = (data) => {
                   this.setState({ venueheadertext: data.headertext });
                   this.setState({
                     showSkip: data.showskip ? data.showskip : null,
                   });
                 };

                 //send facility data
                 sendfaciltiydata = (data, data1, error) => {
                   this.setState({ traineedata: data });
                   this.setState({ commonArray: data1 });
                   var venueform = this.state.venueform;

                   venueform.trainer.userId = this.state.loginDetails.user_id;
                   venueform.trainer.trainerId = "";
                   venueform.trainer.subSpecificId =
                     data1[0].spec_det_datavalue1;
                   venueform.trainer.certificationContent =
                     data1[1].spec_det_datavalue1;
                   venueform.trainer.yearOfExp = data1[2].spec_det_datavalue1;
                   venueform.trainer.monthOfExp = data1[3].spec_det_datavalue1;
                   venueform.trainer.achivementContent =
                     data1[4].spec_det_datavalue1;
                   venueform.trainer.address = data1[5].spec_det_datavalue1;
                   venueform.trainer.location = data1[5].spec_det_datavalue2;
                   venueform.trainer.landMark = data1[6].spec_det_datavalue1;

                   if (error != null) {
                     var validations = this.state.validations;
                     validations[2].state = error;
                     this.setState({ validations });
                   }
                   venueform.keytoupdate = ["TRAINER_BASIC"]; //autosave
                   venueform.trainer.lastActivityTab = 2;
                   var venueformData = this.state.venueformData;
                   venueformData.set(
                     "trainer",
                     JSON.stringify(venueform.trainer)
                   );

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform });

                   this.setState({ venueformData });
                 };

                 //send venuetypedata
                 sendtraineetypedata = (data) => {
                   console.log("traineetypereceive", data);
                   this.setState({ venuetypedata: data });
                   var validations = this.state.validations;
                   validations[0].state = true;
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.trainer.categoryId = data.CategoryId;
                   this.setState({ venueform }, () => {
                     this.gotoNext();
                   });
                 };

                 sendtraineemodetypedata = (data, update) => {
                   this.setState({ traineemodedata: data });
                   var validations = this.state.validations;
                   validations[0].state = true;
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.course[0].courseModeId = data.ModeOfTrainingId;
                   this.setState({ venueform }, () => {
                     console.log("coursedata", venueform);
                   });
                 };

                 //send trainee specific
                 sendfacilitytypedata = (data) => {
                   this.setState({ facilitytypedata: data });
                   var venueform = this.state.venueform;
                   var validations = this.state.validations;
                   validations[1].state = true;
                   this.setState({ validations });
                   venueform.trainer.specificId = data.SpecificId;
                   this.setState({ venueform }, () => {
                     this.gotoNext();
                   });
                 };

                 //load facility
                 loadFacility = () => {
                   if (this.state.facilityIndex == 0) {
                     return (
                       <TraineeCategory
                         venuetypedata={this.state.venuetypedata}
                         facilitytypeData={this.state.facilitytypedata}
                         sendfacilitytypedata={this.sendfacilitytypedata}
                         headertext={this.changeHeaderText}
                         labeltext={this.changeLabelText}
                       />
                     );
                   } else {
                     return (
                       <TraineeDetails
                         commonArray={this.state.commonArray}
                         venuetypedata={this.state.venuetypedata}
                         facilityData={this.state.facilitytypedata}
                         sendfaciltiydata={this.sendfaciltiydata}
                         headertext={this.changeHeaderText}
                         labeltext={this.changeLabelText}
                       />
                     );
                   }
                 };

                 //gotoback
                 gotoback = () => {
                   if (this.state.currentIndex != 0) {
                     var currentitem = parseInt(this.state.currentIndex) - 1;
                     var currentitemname = this.state.list[currentitem].name;
                     this.setState({ activeparent: currentitemname });
                     this.setState({ currentIndex: currentitem });
                     this.setState({ facilityIndex: 1 });
                     this.setState({ facitlityactive: true });
                   }
                 };

                 //validate to steps
                 validateGoToNext = () => {
                   switch (this.state.activeparent) {
                     case "tab1": {
                       this.gotoNext();
                       break;
                     }
                     case "tab2": {
                       const current = this.state.courseDetailData;
                       console.log("tab2_called", current);
                       if (current == null)
                         Toast.show("Please fill the fields", Toast.LONG);
                       else {
                         this.gotoNext();
                       }
                       break;
                     }
                     case "tab3": {
                       const current = this.state.courseDetail1Data;
                       console.log("tab3", this.state.venueform.existingnewimages);
                       if (current != null && current.courseDesc != null) {
                         if (this.state.autosave == true) {
                           var venueform = this.state.venueform;

                           var venueformData = this.state.venueformData;
                           venueform.trainer.lastActivityTab = 2;
                           this.setState({ venueform });
                           venueformData.set(
                             "trainer",
                             JSON.stringify(venueform.trainer)
                           );

                           venueform.keytoupdate = [
                             "COURSE_DETAILS",
                             "BATCH_DATA",
                             "TRAINER_UPLOAD",
                           ];

                           venueformData.set(
                             "keytoupdate",
                             JSON.stringify(venueform.keytoupdate)
                           );
                           if (venueform.existingnewimages.length >= 1) {
                             console.log("uploadcalling");
                             this.autosaveData(
                               venueformData,
                               "upload",
                               venueform.existingnewimages
                             );
                           } else {
                             console.log("notcalled");
                             this.autosaveData(venueformData);
                           }
                         }
                         this.gotoNext();
                       } else Toast.show("Please fill the fields", Toast.LONG);
                       break;
                     }
                     case "tab4": {
                       const current = this.state.courseDetailData;
                       console.log("tab4", current);
                       if (current != null) {
                         if (this.state.autosave == true) {
                           console.log("fsd");
                           var venueform = this.state.venueform;
                           var venueformData = this.state.venueformData;
                           venueform.trainer.lastActivityTab = 3;
                           this.setState({ venueform });
                           venueformData.set(
                             "trainer",
                             JSON.stringify(venueform.trainer)
                           );
                           venueformData.set(
                             "keytoupdate",
                             JSON.stringify(venueform.keytoupdate)
                           );
                           this.autosaveData(venueformData);
                         }
                         this.gotoNext();
                       } else Toast.show("Please fill the fields", Toast.LONG);
                       break;
                     }
                     case "tab6": {
                       const current = this.state.chooseVenueData;
                       console.log("tab6", current);
                       if (
                         current != null &&
                         this.state.chooseVenueData.venueId > 0 &&
                         this.state.chooseVenueData.venueType > 0
                       ) {
                         if (this.state.autosave == true) {
                           var venueform = this.state.venueform;
                           var venueformData = this.state.venueformData;
                           venueform.trainer.lastActivityTab = 6;
                           this.setState({ venueform });
                           venueformData.set(
                             "trainer",
                             JSON.stringify(venueform.trainer)
                           );
                           venueformData.set(
                             "keytoupdate",
                             JSON.stringify(venueform.keytoupdate)
                           );
                           this.autosaveData(venueformData);
                         }
                         this.gotoNext();
                       } else Toast.show("Please fill the fields", Toast.LONG);
                       break;
                     }
                     case "tab5": {
                       Toast.show("Please take a action", Toast.LONG);
                       break;
                     }
                     default: {
                       alert(result);
                       break;
                     }
                   }
                 };

                 //course detail submit
                 courseDetailSubmit = (data, data1, error) => {
                   this.setState({ courseDetailData: data });
                   if (data1 != "") this.setState({ courseDetailState: data1 });
                   var venueform = this.state.venueform;

                   venueform.course[0].batchData[0].batchDurationHours =
                     data.batchDurationHours;
                   venueform.course[0].batchData[0].batchStartDate =
                     data.batchStartDate;
                   venueform.course[0].batchData[0].batchEndDate =
                     data.batchEndDate;
                   venueform.course[0].batchData[0].batchSeatCount =
                     data.batchSeatCount;
                   venueform.course[0].batchData[0].keyHiglights =
                     data.keyHiglights;
                   venueform.course[0].batchData[0].batchFee = data.batchFee;
                   venueform.course[0].batchData[0].batchFeeCurrency =
                     data.batchFeeCurrency;

                   if (error != null) {
                     var validations = this.state.validations;
                     validations[2].state = error;
                     this.setState({ validations });
                   }

                   venueform.keytoupdate = [
                     "COURSE_DETAILS",
                     "BATCH_DATA",
                     "TRAINER_UPLOAD",
                   ];

                   venueform.trainer.lastActivityTab = 3;
                   var venueformData = this.state.venueformData;

                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform }, () => {
                     console.log("coursedetail updated", venueform.course);
                   });
                 };

                 //coursedetail 1 on change
                 courseDetail1Submit = (data, data1, error) => {
                   console.log("coursedetail1", data);

                   this.setState({ courseDetail1Data: data });
                   if (data1 != "")
                     this.setState({ courseDetail1State: data1 });

                   var venueform = this.state.venueform;
                   console.log("imagess", venueform.existingnewimages);
                   venueform.course[0].courseDesc = data.courseDesc;
                   venueform.course[0].courseOutline = data.courseOutline;
                   venueform.course[0].courseFAQ = data.courseFAQ;

                   if (error != null) {
                     var validations = this.state.validations;
                     validations[2].state = error;
                     this.setState({ validations });
                   }

                   venueform.keytoupdate = ["COURSE_DETAILS", "TRAINER_UPLOAD"];
                   venueform.trainer.lastActivityTab = 2;
                   var venueformData = this.state.venueformData;

                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform }, () => {
                     console.log("coursedetail1 updated", venueform.course);
                   });
                 };

                 autosaveData1 = (data, upload, newimagesData) => {
                 
                   console.log("autosave_init", data.formArray);
                    var self = this;
                    const field = data.getParts();
                
  
                   var venueformData = self.state.venueformData;
                   if (upload) {
                     this.setState({ loading: true });
                   }
                   LoadAPIPost.LoadApi(
                     data,
                     links.APIURL + "uploadData",
                     upload,
                     function (err, success) {
                       console.log("inside");
                       if (upload) {
                         venueformData.set("formArray", null);
                         self.setState({ loading: false });
                       }
                       if (err) {
                         Toast.show("serverError", Toast.LONG);
                       } else {
                         var respjson = JSON.parse(success);

                         console.log("autosave_result", respjson);
                         var venueform = self.state.venueform;
                         if (respjson.status == 0) {
                          
                        if (respjson.existImage.length>0){  
                          console.log('inside')

                            venueform.course[0].existImages[0] = {
                              name: respjson.existImage[0]
                            };
                            venueformData.set(
                              "course",
                              JSON.stringify(venueform.course)
                            );
                            

                            venueformData.set("formArray", null);

                            self.setState({ venueform },()=>
                            console.log('updatedd',
                            self.state.venueform.course));
                            
                            self.setState({ venueformData });

                        }
                        


                         }
                       }
                     },
                     function (progress) {
                       if (upload) {
                         self.setState({ progresscount: progress });
                       }
                     }
                   );
                 };

                 //autosave functioncast
                 autosaveData = (data, upload, newimagesData) => {
                   // alert(JSON.stringify(newimagesData));
                   // return;
                   console.log("autosave_init", data);
                   var self = this;
                   const field = data.getParts();
                   // alert(JSON.stringify(field));
                   // return;
                   var venueformData = this.state.venueformData;
                   if (upload) {
                     this.setState({ loading: true });
                   }
                   LoadAPIPost.LoadApi(
                     data,
                     links.APIURL + "courseAutoSave",
                     upload,
                     function (err, success) {
                       console.log("inside");
                       if (upload) {
                         venueformData.set("formArray", null);
                         self.setState({ loading: false });
                       }
                       if (err) {
                         Toast.show("serverError", Toast.LONG);
                       } else {
                         var respjson = JSON.parse(success);

                         console.log("autosave_result", respjson);
                         var venueform = self.state.venueform;
                         if (respjson.status == 0) {
                           //upload image modal overlay
                           if (newimagesData && upload) {
                             var newmodifiedimages = [];
                             if (Array.isArray(newimagesData) == true) {
                               newimagesData.push({
                                 name: respjson.existImage,
                               });
                               newmodifiedimages = newimagesData;
                             } else {
                               newmodifiedimages = newimagesData;
                             }
                              if (respjson.existImage.length > 0) {
                                venueform.course[0].existImages[0] = {
                                  name: respjson.existImage[0],
                                };
                                venueformData.set(
                                  "course",
                                  JSON.stringify(venueform.course)
                                );
                              }
                              
                           }

                        
                           if (respjson.courseId) {

                             venueform.course[0].courseId = respjson.courseId;
                            
                           } 

                            self.setState({ venueform });
                            venueformData.set(
                              "course",
                              JSON.stringify(venueform.course)
                            );
                         }
                       }
                     },
                     function (progress) {
                       if (upload) {
                         self.setState({ progresscount: progress });
                       }
                     }
                   );
                 };

                 //trainee course detail1 submit

                 getTrainerId = () => {
                   fetch(links.APIURL + "myTrainerList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json",
                     },
                     body: JSON.stringify({
                       userId: this.state.loginDetails.user_id,
                     }),
                   })
                     .then((response) => response.json())
                     .then((responseJson) => {
                       console.log("fsdfsd", responseJson);
                       if (responseJson.status == 0) {
                         var venueform = this.state.venueform;
                         venueform.trainerId = responseJson.data[0].trainer_id;
                         //  venueform.trainerId = 10;
                         this.setState({ venueform }, () => {
                           console.log("trainerlsit1", venueform);
                         });
                       } else {
                         Toast.show("Trainer Profile Not Found !!", Toast.LONG);
                         setTimeout(() => {
                           Actions.reset("home1");
                         }, 100);
                       }
                     });
                 };

                 //go to next step
                 gotoNext = () => {
                   console.log("this", this.state.currentIndex);
                   console.log("tablist", this.state.list);
                   if (this.state.currentIndex != this.state.list.length - 1) {
                     var currentitem = parseInt(this.state.currentIndex) + 1;

                     // alert(this.state.list[currentitem].name)
                     var currentitemname = this.state.list[currentitem].name;
                     // alert(currentitem);
                     this.setState({ activeparent: currentitemname });
                     this.setState({ currentIndex: currentitem });
                     this.setState({ facilityIndex: 0 });
                     this.setState({ facitlityactive: true });
                   }
                 };

                 availablitydays = (data) => {
                   var availability = this.state.availability;
                   availability.days = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_days = data ? data.value : null;
                   this.setState({ venueform });
                 };

                 availcircleData = (data) => {
                   var availability = this.state.availability;
                   availability.type = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_avail_type = data.id;
                   this.setState({ venueform });
                 };

                 fromtodays = (data) => {
                   var availability = this.state.availability;
                   availability.from = data.from;
                   availability.to = data.to;
                   this.setState({ availability });
                   if (data.from == "" || data.to == "") {
                     var validations = this.state.validations;
                     validations[3].state = false;
                   } else {
                     var validations = this.state.validations;
                     validations[3].state = true;
                   }
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_avail_frm = availability.from;
                   venueform.availability.venue_avail_to = availability.to;
                   this.setState({ venueform });
                 };

                 AmenetiesData = (data) => {
                   this.setState({ ameneties: data });
                   var amenitiesData =
                     data.length > 0 &&
                     data.filter((obj) => obj.circleCount > 0);
                   var amentiesArray = [];
                   for (var i in amenitiesData) {
                     if (
                       amenitiesData[i].hasOwnProperty("activeArray") == true
                     ) {
                       amentiesArray = amentiesArray.concat(
                         amenitiesData[i].activeArray
                       );
                     }
                   }
                   console.log("amenity", amentiesArray);
                   var venueform = this.state.venueform;
                   venueform.ameneties = amentiesArray;
                   this.setState({ venueform }, () => {
                     this.gotoNext();
                   });
                 };

                 saveTags = (data, filterdata) => {
                   // alert(JSON.stringify(filterdata));
                   var venTags = this.state.venTags;
                   venTags = data;
                   this.setState({ venTags });
                   var tagarray = [];
                   for (var i = 0; i < filterdata.length; i++) {
                     var objdata = {
                       tag_cat_id: filterdata[i].tag_cat_id,
                       tag_details: [],
                     };
                     for (var y = 0; y < filterdata[i].data.length; y++) {
                       objdata.tag_details.push(filterdata[i].data[y].tag_name);
                     }
                     tagarray.push(objdata);
                   }
                   // alert(JSON.stringify(tagarray));
                   var venueform = this.state.venueform;
                   venueform.tagdetails = tagarray;
                   this.setState({ tagarray });
                 };

                 updateCloseModal = (data) => {
                   var availabilityState = this.state;
                   availabilityState.isAvailable = data.isAvailable;
                   availabilityState.showBusinessForm = data.showBusinessForm;
                   availabilityState.isSlotsBooking = data.isSlotsBooking;
                   this.setState({ availabilityState });
                 };

                 sendPriceData = (data) => {
                   this.gotoNext();
                   var venueprice = this.state.venueprice;
                   venueprice = data;
                   this.setState({ venueprice });
                   // alert(JSON.stringify(data));
                   if (data.amt == 0 || data.amt == "") {
                     var validations = this.state.validations;
                     validations[4].state = false;
                   } else {
                     var validations = this.state.validations;
                     validations[4].state = true;
                   }
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.pricedetails.venue_price_amt = data.amt;
                   venueform.pricedetails.venue_price_type = data.activeId;
                   venueform.pricedetails.venue_price_currency = data.days;
                   venueform.pricedetails.venue_price_name = data.type;
                   this.setState({ venueform });
                 };

                 venueImages = (data, data1, data2) => {
                   var venueformData = this.state.venueformData;
                   var venueform = this.state.venueform;

                   this.setState({
                     courseData: data.coursePrice,
                   }); 

                   if (data.courseState != null) {
                     this.setState({ courseState: data.courseState });
                   }
                   console.log("data.localimages", data.localimages.length);
                   venueform.userId = this.state.loginDetails.user_id;
                   venueform.trainerId = this.state.venueform.trainerId;
                   venueform.formArray = data.localimages;
                  //  venueformData.set("formArray", venueform.formArray);

                   venueform.course[0].trainingLevelId =
                     data.coursePrice.trainingLevelId;
                   venueform.course[0].languageId = data.coursePrice.languageId;
                   venueform.course[0].courseTitle =
                     data.coursePrice.courseTitle;

                   venueform.keytoupdate = ["COURSE_DETAILS"];

                   venueformData.set("userId", venueform.userId);
                   venueformData.set("trainerId", venueform.trainerId);
                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );
                   venueform.trainer.lastActivityTab = 1;

                   const fieldforuploads = data.formimagearray
                     .getParts()
                     .find((item) => item.fieldName === "formArray");
                   var arrayofloopimages = data.localimages.filter(
                     (obj) => obj.exist
                   ); 

                   if (fieldforuploads) {
                     console.log("filed", fieldforuploads);
                     venueformData.set("formArray", fieldforuploads);
                  
                      // arrayofloopimages.push({name:data.localimages[data.localimages.length-1].fileName})
                     if (data.localimages.length > 0) {
                       arrayofloopimages.push({
                         name:
                           data.localimages[data.localimages.length - 1]
                             .fileName,
                       });
                     }
                   }else
                   {
                     venueformData.set("formArray", null);
                   }

                   var existingnewimages =
                     arrayofloopimages.length > 0
                       ? arrayofloopimages.map((obj) => {
                           // var objnew=obj;
                           var newobj = { name: obj.name };
                           return newobj;
                         })
                       : []; 

                   
                   venueform.existingnewimages = existingnewimages;
                   
                   if(this.state.edit)
                   venueform.course[0].existImages = existingnewimages;

                   venueformData.set(
                     "trainer",
                     JSON.stringify(venueform.trainer)
                   ); 
                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );   

                   
                       console.log("existing", existingnewimages);
                       





                   // venueformData
                   this.setState({ venueform, venueformData },
                    ()=>console.log('updadsfsdfsdte',this.state.venueform.course));
             

                   if (this.state.autosave == true) {
                     //  this.autosaveData(venueformData, "upload", existingnewimages);
                     console.log("existss", existingnewimages);
                   }
                   else if(this.state.edit ){
                     console.log('if');
                      this.autosaveData1(
                        venueformData,
                        "upload",
                        existingnewimages
                      );
                   }
                   
                   this.gotoNext();
                 };

                 submitVenuData = (data, edit) => {
                   // alert(edit);
                   // return;
                   var self = this;
                   var venueformData = self.state.venueformData;
                   var venueform = self.state.venueform;
                   this.setState({ loading: true });
                  

                   venueformData.set(
                     "formArray",
                     JSON.stringify(venueform.formArray)
                   );
                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   venueformData.set(
                     "userId",
                     JSON.stringify(venueform.userId)
                   );

                   venueformData.set(
                     "trainerId",
                     JSON.stringify(venueform.trainerId)
                   );

                   if (self.state.autosave == true) {
                     venueformData.set("formArray", null);
                   }

                   if (this.state.edit) {
                     console.log("finalsubmit", venueformData);
                     LoadAPIPost.LoadApi(
                       venueformData,
                       links.APIURL + "editCourseDetails",
                       this.state.autosave == true ? null : null,
                       function (err, success) {
                         self.setState({ loading: null });

                         if (err) {
                           Toast.show("serverError", Toast.LONG);
                         } else {
                           var jsonData = JSON.parse(success);
                           console.log("final", jsonData);
                           if (jsonData.status == 0) {
                             Toast.show(
                               "Course Edited Successfully",
                               Toast.LONG
                             );
                             setTimeout(() => {
                               Actions.reset("home1");
                             }, 100);
                           } else {
                             Toast.show("Course Edit Failed", Toast.LONG);
                           }
                         }
                       },
                       function (progress) {
                         self.setState({ progresscount: progress });
                       }
                     );

                     return;
                   }

                   console.log("callednext", this.state.venueform.course);

                   fetch(links.APIURL + "addCourseDetails/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json",
                     },
                     body: JSON.stringify({
                       dummyCourseId:
                         this.state.venueform.course[0].courseId != null &&
                         this.state.venueform.course[0].courseId,
                       trainerId: this.state.venueform.trainerId,
                       userId: this.state.venueform.userId,
                       course: this.state.venueform.course,
                     }),
                   })
                     .then((response) => response.json())
                     .then((responseJson) => {
                       self.setState({ loading: null });
                       console.log("responfsd", responseJson);
                       var showtext = this.state.edit ? "Edited" : "Added";
                       if (responseJson.status == 0) {
                          // Toast.show(
                          //   "Course " + showtext + " Successfully",
                          //   Toast.LONG
                          // );
                          //  setTimeout(() => {
                          //    Actions.reset("home1");
                          //  }, 100);
                         self.setState({ activeparent: "success" });
                       } else {
                         Toast.show(responseJson.data[0], Toast.LONG);
                       }
                     });
                 };

                 closeSuccessModal = (data) => {
                   this.setState({ activeparent: null });
                   if (data) {
                     Actions.venuepage({ raiselogin: true });
                   } else {
                     Actions.home1();
                   }
                 };
                 errorData = (data) => {
                   Toast.show(data, Toast.LONG);
                 };
                 loadedit = () => {
                   this.setState({ currentIndex: 0 });
                   this.setState({ activeparent: "tab1" });
                   // this.goToRight();
                 };

                 //tab7 on change text
                 sendPurposes = (data, data1) => {
                   this.setState({
                     chooseVenueData: data,
                   });
                   if (data1 != "")
                     this.setState({
                       chooseVenueState: data1,
                     });

                   var venueform = this.state.venueform;

                   venueform.course[0].venueDetails[0] = data;

                   var venueformData = this.state.venueformData;
                   venueformData.set(
                     "course",
                     JSON.stringify(venueform.course)
                   );

                   venueform.keytoupdate = [
                     "TRAINER_BASIC",
                     "COURSE_DETAILS",
                     "BATCH_DATA",
                     "TRAINING_VENUE",
                   ];

                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );

                   this.setState({ venueform }, () => {
                     console.log("sendpurposes", venueform.course);
                   });
                 };

                 courseNext = (data) => {
                   console.log("coursedetailnextfired", data);
                   this.setState({
                     courseDetail: data,
                   });
                 };

                 render() {
                   return (
                     <Container>
                       <Root>
                         {this.state.loading && (
                           <View
                             style={{
                               width: "100%",
                               zIndex: 9999,
                               flex: 1,
                               justifyContent: "center",
                               alignItems: "center",
                               position: "absolute",
                               height: "100%",
                               backgroundColor: "rgba(0,0,0,0.8)",
                               top: 0,
                             }}
                           >
                             <ActivityIndicator
                               size="large"
                               color={color.orange}
                             />
                             <Text style={{ color: color.white }}>
                               Please Wait ....
                             </Text>
                           </View>
                         )}
                         <SubHeaderComp bgcolor={color.orange}>
                           <View style={styles.subheading}>
                             <Text style={styles.subheadingtext}>
                              Add Course
                             </Text>
                           </View>
                         </SubHeaderComp>
                         <Content>
                           {this.state.activeparent != "success" && (
                             <View>
                               <DynamicPagination
                                 dots={5}
                                 activeid={this.state.currentIndex}
                                 changePagination={(data) =>
                                   this.changeTab(data)
                                 }
                               />
                               <View
                                 style={{
                                   flex: 1,
                                   flexDirection: "row",
                                   alignItems: "center",
                                   padding: 5,
                                   borderBottomWidth: 0.5,
                                   borderColor: color.black1,
                                   justifyContent: "space-between",
                                 }}
                               >
                                 <View>
                                   <Text
                                     style={{
                                       fontSize: hp("1.7%"),
                                       color: color.blue,
                                       fontWeight: "bold",
                                     }}
                                   >
                                     {this.state.venueheadertext}
                                   </Text>
                                 </View>
                                 {this.state.showSkip && (
                                   <TouchableOpacity
                                     onPress={() => this.gotoNext()}
                                   >
                                     <Text
                                       style={{
                                         fontSize: hp("1.7%"),
                                         color: color.blue,
                                       }}
                                     >
                                       Skip
                                     </Text>
                                   </TouchableOpacity>
                                 )}
                               </View>
                               <View
                                 style={{
                                   flex: 1,
                                   flexDirection: "row",
                                   alignItems: "center",
                                   padding: 5,
                                   borderBottomWidth: 0.5,
                                   borderColor: color.black1,
                                 }}
                               >
                                 <View style={{ flex: 0.1 }}>
                                   <TouchableOpacity onPress={this.gotoback}>
                                     <Icon
                                       style={[
                                         styles.arrownavicon,
                                         {
                                           color:
                                             this.state.facitlityactive &&
                                             this.state.facilityIndex == 1
                                               ? color.orange
                                               : null,
                                         },
                                       ]}
                                       type="FontAwesome"
                                       name="angle-left"
                                     />
                                   </TouchableOpacity>
                                 </View>
                                 <View
                                   style={{
                                     flex: 0.8,
                                     flexDirection: "row",
                                     alignItems: "center",
                                     justifyContent: "center",
                                   }}
                                 >
                                   <Text
                                     numberOfLines={2}
                                     style={{
                                       fontSize: hp("2%"),
                                       textAlign: "center",
                                     }}
                                   >
                                     {this.state.venuetypetext}
                                   </Text>
                                 </View>
                                 <View style={{ flex: 0.1 }}>
                                   {/* <TouchableOpacity
                                     onPress={() => this.validateGoToNext()}
                                   >
                                     <Icon
                                       style={[
                                         styles.arrownavicon,
                                         {
                                           color:
                                             this.state.facitlityactive &&
                                             this.state.facilityIndex == 0
                                               ? color.orange
                                               : null
                                         }
                                       ]}
                                       type="FontAwesome"
                                       name="angle-right"
                                     />
                                   </TouchableOpacity> */}
                                 </View>
                               </View>
                             </View>
                           )}
                           <View>
                             {this.state.activeparent == "tab1" && (
                               <CourseMode
                                 loginDetails={this.state.loginDetails}
                                 venuedetails={this.state.traineemodedata}
                                 sendvenuetypedata={
                                   this.sendtraineemodetypedata
                                 }
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 next={this.gotoNext}
                                 edit={this.state.edit}
                               />
                             )}

                             {this.state.activeparent == "tab2" && (
                               <CoursePrice
                                 sendPriceData={this.venueImages}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 localimages={this.state.venueform.formArray}
                                 mainformData={this.state.venueformData}
                                 venueform={this.state.venueform}
                                 coursePriceData={this.state.courseData}
                                 coursePriceState={this.state.courseState}
                                 edit={this.state.edit}
                               />
                             )}

                             {this.state.activeparent == "tab3" && (
                               <CourseDetail1
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 sendCourseDetail1={this.courseDetail1Submit}
                                 courseDetail1Data={
                                   this.state.courseDetail1Data
                                 }
                                 onSubmit={this.validateGoToNext}
                                 courseDetail1State={
                                   this.state.courseDetail1State
                                 }
                                 courseDetail1Data={
                                   this.state.courseDetail1Data
                                 }
                                 edit={this.state.edit}
                               />
                             )}

                             {this.state.activeparent == "tab4" && (
                               <CourseDetail
                                 sendCourseDetail={this.courseDetailSubmit}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 onSubmit={this.validateGoToNext}
                                 courseDetailData={this.state.courseDetailData}
                                 courseDetailState={
                                   this.state.courseDetailState
                                 }
                                 edit={this.state.edit}
                               />
                             )}
                             {/* 
                             {this.state.activeparent == "tab5" && (
                               <CourseChooseVenues
                                 purposeDatas={this.state.chooseVenueState}
                                 sendpurposes={this.sendPurposes}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 loginDetails={this.state.loginDetails}
                                 onSubmit={this.validateGoToNext}
                                 chooseVenueData={this.state.chooseVenueData}
                                 edit={this.state.edit}
                               />
                             )} */}

                             {this.state.activeparent == "tab5" && (
                               <CourseReview
                                 submitVenuData={this.submitVenuData}
                                 venueform={this.state.venueform}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 loadEdit={this.loadedit}
                                 edit={this.state.edit}
                               />
                             )}
                             {this.state.activeparent == "success" && (
                               <CourseSuccess
                                 commonArray={this.state.commonArray}
                                 loginDetails={this.state.loginDetails}
                                 venueform={this.state.venueform}
                                 closemodal={(data) =>
                                   this.closeSuccessModal(data)
                                 }
                                 edit={this.state.edit}
                               />
                             )}
                           </View>
                         </Content>
                       </Root>
                     </Container>
                   );
                 }
               }

const styles = {
  active: {
    backgroundColor: "#e2e2e2",
    borderRightWidth: 0.5,
    borderColor: "#999999",
    zIndex: 99999
  },
  subheading: {
    padding: 12
  },
  subheadingtext: {
    fontSize: hp("2%"),
    color: color.white
  },
  imageparent: {
    width: hp("8%"),
    height: hp("7%"),
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: color.ash2
  },
  imageTab: {
    width: hp("4%"),
    height: hp("4%")
  },
  activeparent: {
    position: "absolute",
    right: -hp("1.4%"),
    top: hp("2%"),
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999999
  },
  bordertriangle: {
    position: "absolute",
    left: 0,
    top: hp("0.23%")
  },
  arrowbox: {
    width: hp("3%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: color.blueactive
  },
  arrowboxicon: {
    color: color.black1,
    fontSize: hp("3.2%")
  },
  arrownavicon: {
    fontSize: hp("4%"),
    textAlign: "center",
    color: color.ash2
  },
  activetextorange: {
    color: color.orange
  }
};
