import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator
} from "react-native";
import {Icon} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import ModalComp from "../components/Modal";
import AsyncStorage from "@react-native-community/async-storage";
import links from "../Helpers/config";
import Toast from "react-native-simple-toast";
 import refer from "../images/referearn.png"; 
 import { Actions } from "react-native-router-flux";
 import ADD_img from "../images/add_icon.png";
 import Preview from "../images/imageloader.png";
export default class PurchasedCourse extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     visible: false,
                     review: null,
                     loginDetails: null,
                     showloading: false,
                     activeCourse: null,
                     courses: []
                   };
                 }
                 componentDidMount = () => {};

                 componentWillReceiveProps(props) {}

                 async componentWillMount() {
                   const data = await AsyncStorage.getItem("loginDetails");
                   var parsedata = JSON.parse(data);
                   this.setState({ loginDetails: parsedata }, () =>{
 console.log("login", this.state.loginDetails);
 this.getCourseList();
                   }
                   
                   );
                 }

                 getCourseList = () => {
                   this.setState({ showloading: true });
                   fetch(links.APIURL + "myBookedCourseDetails/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       userId: this.state.loginDetails.user_id
                       //  userId: 1
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       this.setState({ showloading: false });
                       if (responseJson.status == 0) {
                         this.setState({
                           courses: responseJson.data
                         });
                       } else {
                         Toast.show("No Purchase Courses Found !!", Toast.LONG);
                       }
                     });
                 };

                 onLoadEdit = item => {
                   Actions.CourseForm({
                     logindata: this.state.loginDetails,
                     edit: true,
                     Course: item
                   });
                 };

                 onDelete = item => {};

                 getLangName = id => {
                   if (id == 1) return "English";
                   else if (id == 2) return "Hindi";
                   else return "Tamil";
                 };

                 onLoadMore = data => {
                   console.log("data", data);
                   this.setState({ visible: data });
                 };

                 _renderCourseList = (item, i) => {
                   return (
                     <View
                       style={{
                         backgroundColor: color.white,
                         borderRadius: 10,
                         padding: 10,
                         marginVertical: hp("1%")
                       }}
                     >
                       <TouchableOpacity
                         onPress={() =>
                           this.setState({ activeCourse: item }, () => {
                             console.log("pressed", item);
                             this.onLoadMore(true);
                           })
                         }
                       >
                         <View
                           style={{
                             flexDirection: "row",
                             justifyContent: "space-between",
                             display: "flex",
                             position: "relative",
                             borderBottomColor: "rgba(161,155,183,1)",
                             borderBottomWidth: 1,
                             borderStyle: "dashed",
                             marginBottom: 10,
                             borderRadius: 1
                           }}
                         >
                           <View>
                             <Text style={[styles.h3, { color: color.blue }]}>
                               {item.course_title}
                             </Text>
                           </View>
                           <View>
                             <Text>
                               {item.language_id &&
                                 this.getLangName(item.language_id)}
                             </Text>
                           </View>
                         </View>

                         <View>
                           <View
                             style={{
                               flexDirection: "row",
                               justifyContent: "space-between",
                               display: "flex"
                             }}
                           >
                             <View>
                               <Text style={[styles.h6]}>
                                 Course Fee ( Per Hour){"\n"}{" "}
                                 <Text style={styles.h3}>
                                   {item.batchDetailsData.length > 0 &&
                                     item.batchDetailsData[0].batch_fee}{" "}
                                   {item.batchDetailsData.length > 0 &&
                                     item.batchDetailsData[0]
                                       .batch_fee_currency}
                                 </Text>
                               </Text>
                             </View>
                             <View>
                               <Text style={[styles.h6]}>
                                 {" "}
                                 {"\n"}
                                 <Text style={styles.h3}>
                                   {item.batchDetailsData.length > 0 &&
                                     item.batchDetailsData[0]
                                       .batch_startDate}{" "}
                                   to{" "}
                                   {item.batchDetailsData.length > 0 &&
                                     item.batchDetailsData[0].batch_endDate}
                                 </Text>
                               </Text>
                             </View>
                           </View>

                           
                         </View>
                       </TouchableOpacity>
                     </View>
                   );
                 };

                 render() {
                   const { courses, showloading } = this.state;
                   const { review } = this.state;

                   if (showloading) {
                     return (
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "center",
                           alignItems: "center"
                         }}
                       >
                         <ActivityIndicator size="large" color="#073cb2" />
                         <Text>Please Wait</Text>
                       </View>
                     );
                   }

                   if (courses.length <= 0) {
                     return (
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "center",
                           alignItems: "center",
                           backgroundColor: color.white
                         }}
                       >
                         <Image
                           source={refer}
                           style={{
                             width: hp("20%"),
                             height: hp("20%"),
                             textAlign: "center"
                           }}
                         />
                         <Text
                           style={{
                             color: color.orange,
                             fontSize: hp("3.5%"),
                             textAlign: "center",
                             fontWeight: "bold"
                           }}
                         >
                           No Records Found
                         </Text>

                         <View style={styles.buttonContainer}>
                           <TouchableOpacity
                             onPress={() => {
                               Actions.home1({
                                 logindata: this.state.loginDetails
                               });
                             }}
                           >
                             <Text
                               style={{
                                 color: "white",
                                 fontSize: hp("2%"),
                                 fontWeight: "bold"
                               }}
                             >
                               Go to Course
                             </Text>
                           </TouchableOpacity>
                         </View>
                       </View>
                     );
                   }

                   return (
                     <View style={styles.container}>
                       <View
                         style={{
                           flexDirection: "row",
                           justifyContent: "space-between",
                           display: "flex",
                           position: "relative",
                           borderBottomColor: "rgba(161,155,183,1)",
                           borderBottomWidth: 1,
                           borderStyle: "dashed",
                           marginBottom: 10,
                           borderRadius: 1
                         }}
                       >
                         <View>
                           <Text style={[styles.h3, { color: color.blue }]}>
                             My Purchased Course Details
                           </Text>
                         </View>
                         <View>
                           <TouchableOpacity
                             onPress={() => {
                               Actions.home1({
                                 logindata: this.state.loginDetails
                               });
                             }}
                             style={styles.add_symbol_container}
                           >
                             <Image
                               style={styles.add_symbol}
                               source={ADD_img}
                             ></Image>
                           </TouchableOpacity>
                         </View>
                       </View>

                       <ScrollView>
                         {courses &&
                           courses.length > 0 &&
                           courses.map((item, i) => {
                             return this._renderCourseList(
                               item.courseDetails[0],
                               i
                             );
                           })}
                       </ScrollView>

                       {this.state.visible && this.state.activeCourse != null && (
                         <ModalComp
                           titlecolor={color.orange}
                           visible={this.state.visible}
                           closemodal={data => this.onLoadMore(data)}
                         >
                           <View style={{ paddingVertical: hp("4%") }}>
                             <ScrollView>
                               <View
                                 style={{
                                   display: "flex",
                                   margin: 10,
                                   backgroundColor: "white",
                                   padding: 10
                                 }}
                               >
                                 <View style={styles.imageView}>
                                   <TouchableOpacity style={styles.venueimage}>
                                     {this.state.activeCourse &&
                                     this.state.activeCourse.uploadData.length >
                                       0 ? (
                                       <Image
                                         style={styles.venueimage}
                                         source={{
                                           uri: this.state.activeCourse
                                             .uploadData[0].trainer_upload_path
                                         }}
                                       />
                                     ) : (
                                       <Image
                                         style={styles.venueimage}
                                         source={Preview}
                                       />
                                     )}
                                   </TouchableOpacity>
                                 </View>

                                 <View
                                   style={{
                                     flexDirection: "row",
                                     justifyContent: "space-between",
                                     display: "flex"
                                   }}
                                 >
                                   <View>
                                     <Text
                                       style={[
                                         styles.h1,
                                         { color: color.blue }
                                       ]}
                                     >
                                       {this.state.activeCourse &&
                                         this.state.activeCourse.course_title}
                                     </Text>
                                   </View>
                                   <View>
                                     <Text>
                                       {" "}
                                       {this.state.activeCourse &&
                                         this.getLangName(
                                           this.state.activeCourse.languageId
                                         )}
                                     </Text>
                                   </View>
                                 </View>

                                 <View
                                   style={{
                                     margin: 3,
                                     backgroundColor: color.ash1,
                                     padding: 10
                                   }}
                                 >
                                   <View
                                     style={{
                                       flexDirection: "row",
                                       justifyContent: "space-between",
                                       display: "flex"
                                     }}
                                   >
                                     <View>
                                       <Text style={[styles.h6]}>
                                         Course Fee {"\n"}{" "}
                                         <Text style={styles.h1}>
                                           {this.state.activeCourse &&
                                             this.state.activeCourse
                                               .batchDetailsData.length > 0 &&
                                             this.state.activeCourse
                                               .batchDetailsData[0]
                                               .batch_fee}{" "}
                                           {this.state.activeCourse &&
                                             this.state.activeCourse
                                               .batchDetailsData.length > 0 &&
                                             this.state.activeCourse
                                               .batchDetailsData[0]
                                               .batch_fee_currency}
                                         </Text>
                                       </Text>
                                     </View>
                                     <View>
                                       <Text style={[styles.h6]}> {"\n"}</Text>
                                       <Text style={styles.h1}>
                                         <Text style={styles.h3}>
                                           {this.state.activeCourse &&
                                             this.state.activeCourse
                                               .batchDetailsData.length > 0 &&
                                             this.state.activeCourse
                                               .batchDetailsData[0]
                                               .batch_startDate}{" "}
                                           to{" "}
                                           {this.state.activeCourse &&
                                             this.state.activeCourse
                                               .batchDetailsData.length > 0 &&
                                             this.state.activeCourse
                                               .batchDetailsData[0]
                                               .batch_endDate}
                                         </Text>
                                       </Text>
                                     </View>
                                   </View>

                                   <View
                                     style={{
                                       flexDirection: "row",
                                       justifyContent: "space-between",
                                       display: "flex"
                                     }}
                                   >
                                     <View>
                                       <Text style={[styles.h6]}>
                                         No Of Hours {"\n"}{" "}
                                         <Text style={styles.h1}>
                                           {" "}
                                           {this.state.activeCourse &&
                                             this.state.activeCourse
                                               .batchDetailsData.length > 0 &&
                                             this.state.activeCourse
                                               .batchDetailsData[0]
                                               .batch_duration_hours}
                                         </Text>
                                       </Text>
                                     </View>
                                     <View>
                                       <Text style={[styles.h6]}>
                                         Seats {"\n"}
                                         <Text style={styles.h1}>
                                           {" "}
                                           {this.state.activeCourse &&
                                             this.state.activeCourse
                                               .batchDetailsData.length > 0 &&
                                             this.state.activeCourse
                                               .batchDetailsData[0]
                                               .batch_seat_count}
                                         </Text>
                                       </Text>
                                     </View>
                                   </View>
                                 </View>

                                 <View style={styles.box}>
                                   <Text style={styles.h1}>
                                     {" "}
                                     Course Outline{" "}
                                   </Text>
                                   <Text>
                                     {this.state.activeCourse &&
                                       this.state.activeCourse.course_outline}
                                   </Text>
                                 </View>
                                 <View style={styles.box}>
                                   <Text style={styles.h1}>
                                     Course Description{" "}
                                   </Text>
                                   <Text>
                                     {this.state.activeCourse &&
                                       this.state.activeCourse.course_desc}
                                   </Text>
                                 </View>
                               </View>
                             </ScrollView>
                           </View>
                         </ModalComp>
                       )}
                     </View>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    margin: 10,
    justifyContent: "flex-start",
    alignItems: "stretch"
  },
  h1: {
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  buttonContainer: {
    backgroundColor: color.blueactive,
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 20,
    shadowOpacity: 0.25
  },imagecontainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  imageView: {
    width: wp("95%"),
    height: hp("17%"),
    borderRadius: 7,
    marginBottom: 15,
    paddingHorizontal:wp('5%')
  },
  venueimage: {
    position: "absolute",
    width: "100%",
    height: "100%",
    borderRadius: 7
  },
  h3: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30,
  },
  h6: {
    fontSize: 11,
    fontWeight: "200",
    fontStyle: "normal"
  },
  box: {
    marginVertical: hp("0.6%")
  },
  add_symbol_container: {
    backgroundColor: color.blue,
    justifyContent: "flex-end",
    justifyContent: "center",
    alignSelf: "flex-end",
    marginRight: wp("5%"),
    height: hp("2.5%"),
    width: hp("2.5%"),
    marginTop:5
  },
  add_symbol: {
    height: hp("1.3%"),
    width: hp("1.3%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center"
  }
});
