import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,Alert,FlatList} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CircleComp from '../components/CircleImageComp'
import Corporate from '../images/svg/corporate-building.png';
import PrivatePlay from '../images/svg/private-playground.png';
import Independent from '../images/svg/independent-home.png';
import Apartments from '../images/svg/apartments.png';

var datas=[{name:'Hotel/Resort',id:1,icon:Corporate},
            {name:'Institution',id:2,icon:PrivatePlay},
            {name:'Corporate Venues',id:3,icon:Independent},
            {name:'Sports Venues',id:4,icon:Apartments},
           
          ]
datas.push({id:12,disable:true});
export default class CorporateVenue extends Component{
    constructor(props) {
        super(props);
      
        this.state = {venuedata:props.venuedetails};
        // alert(datas.length%2);
      }

     changeVenueType=(data,key)=>{
         this.setState({venuedata:data.id})
     } 
    render(){
        return(
            <View style={{flex:1,flexDirection:'row',flexWrap:'wrap',
                  justifyContent:'space-between',padding:12,backgroundColor:color.white}}>
            {datas.map((obj,key)=>{
                return(
                     <View key={key} style={[styles.circleStyle]}>
                  {!obj.disable&&
                    <CircleComp  sendText={(data)=>{this.changeVenueType(data,key)}} 
                    style={[styles.flexWidth]} width={12} height={12}
                    icon={obj.icon}  text={obj.name} data={obj} 
                    active={this.state.venuedata==obj.id}
                   // active={true}
                    />
                  }
                </View>
                      )
            })}
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    circleStyle:{
        width:wp('30%'),
    },
})