'use strict';
import React, { Component } from 'react';
import CircleText from '../components/CircleImageComp'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color'
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import Corporate from '../images/svg/corporate-building.png';
import PrivatePlay from '../images/svg/private-playground.png';
import Independent from '../images/svg/independent-home.png';
import Apartments from '../images/svg/apartments.png';
import Institution from '../images/svg/institution.png';
import Hotel from '../images/svg/hotel.png';
import BrandPromo from '../images/svg/brand-promotion.png';
import Indoor from '../images/svg/indoor-sports.png';
var datas=[{name:'Soccer',id:1,icon:Corporate},
            {name:'Basket Ball',id:2,icon:PrivatePlay},
            {name:'Tennis',id:3,icon:Independent},
            {name:'Badminton',id:4,icon:Apartments},
            {name:'BaseBall',id:5,icon:Institution},
            {name:'Hotel',id:6,icon:Hotel},
            {name:'VolleyBall',id:7,icon:BrandPromo},
            {name:'Cricket',id:8,icon:Indoor}
          ]
datas.push({id:12,disable:true});
class Specific_Venue extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {venuedata:props.venuedetails};
	  // alert(datas.length%2);
	}
  // componentWillReceiveProps(props){
  //   if(props.venuedetails){
  //     this.setState({venuedata:props.venuedetails});
  //   }
  // }
	// componentWillMount(){
	// 	this.props.labeltext({labeltext:<Text>Please choose your 
  //                                     <Text style={{color:color.top_red,fontWeight:'bold'}}>
  //                                            Venue  </Text> </Text>});
	// }
changeVenueType=(data,key)=>{

  // var venuedata=this.state.venuedata;
 // venuedata=data.id;
	this.setState({venuedata:data.id});
 // this.props.sendvenuetypedata({activeId:data.id,name:data.name});
}
  render() {
 
 
    return (
      <View style={{flex:1,flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between',padding:12,backgroundColor:color.white}}>
      {datas.map((obj,key)=>{
      	return(
      		 <View key={key} style={[styles.circleStyle]}>
            {!obj.disable&&
              <CircleText  sendText={(data)=>{this.changeVenueType(data,key)}} 
              style={[styles.flexWidth]} width={12} height={12}
              icon={obj.icon}  text={obj.name} data={obj} 
              active={this.state.venuedata==obj.id}
             // active={true}
              />
            }
          </View>
      		  )
      })}
      </View>
    );
  }
}

const styles = StyleSheet.create({

circleStyle:{
	width:wp('30%'),
},
activename:{
}
});


export default Specific_Venue;