import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,Alert,FlatList} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button,Input,Item} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DropdowContainer from '../components/drpdown_container'
import Line from '../components/Line'
import ModalComp from '../components/Modal';
import LabelTextbox from '../components/labeltextbox';

import links from '../Helpers/config';
import Cancel from '../images/cancel.png'


var datas=[{id:1,LocationName:'Phoneix'},{id:2,LocationName:'Mesa'},]

export default class Corporate_AddLocation extends Component{
    constructor(props){
        super(props);
       
             this.state = {countryDropdown:{id:'country_id',name:'country_name',dropdown:[]},stateDropdown:{id:'state_id',name:'state_name',dropdown:[]},cityDropdown:{id:'city_id',name:'city_name',dropdown:[]},countryvalue:'',statevalue:'',cityvalue:'',postal:'',locationarray:props.locationArray};
        
        
    }
    loadcountry(){
    fetch(links.APIURL+'getCountry', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      var countryDropdown=this.state.countryDropdown;
      countryDropdown.dropdown=responseJson.data;
      this.setState({countryDropdown})
      // console.log("country",this.state.countryDropdown);

    })  
  }

  loadstate(data){
    fetch(links.APIURL+'getState', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'countryId':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      var stateDropdown=this.state.stateDropdown;
      stateDropdown.dropdown=responseJson.data;
      this.setState({stateDropdown})
      console.log("country",this.state.stateDropdown);

    })  
  }
loadcity(data){
    fetch(links.APIURL+'getCity', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'stateId':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      var cityDropdown=this.state.cityDropdown;
      cityDropdown.dropdown=responseJson.data;
      this.setState({cityDropdown})
      console.log("country",this.state.cityDropdown);

    })  
  }
   Closeicon=()=>{
    alert('cancel Button');
   }
   componentWillMount(){
    this.loadcountry();
   }
      sendDropdownData=(data,key)=>{
          this.setState({[key]:data})
          if(key=='countryvalue'){
      this.setState({statevalue:null})
      this.loadstate(data.country_id); 
    }else if(key=='statevalue'){
      this.setState({cityvalue:null})
      this.loadcity(data.state_id);
    }
      }
      addLocation=()=>{
    var locationarray=this.state.locationarray;
    var findindex=locationarray.findIndex((obj)=>obj.name==(this.state.cityvalue&&this.state.cityvalue.city_name))
    console.log(findindex,"findindex");
    if(this.state.cityvalue){
        if(findindex==-1){

        locationarray.push({id:locationarray.length+1,name:this.state.cityvalue.city_name})
      }
    }
    this.setState({locationarray});
  }
   deleteItem=(index)=>{
    var locationarray=this.state.locationarray;
    console.log(locationarray)
    locationarray.splice(index,1);
    locationarray.map((item,i)=>{item.id=i+1});
    this.setState({locationarray});
  }
  sendLocation=()=>{
    console.log(this.state.locationarray);
    if(this.props.receiveLocation){
        this.props.receiveLocation(this.state.locationarray);
    }
  }
  clearLocation=()=>{
      var stateDropdown=this.state.stateDropdown;
      var cityDropdown=this.state.cityDropdown;
      stateDropdown.dropdown=[];
      cityDropdown.dropdown=[];
      this.setState({countryvalue:'',statevalue:'',cityvalue:'',stateDropdown,cityDropdown});

      // stateDropdown.dropdown=responseJson.data;
  }
    render(){
        return(
      <ModalComp  visible={true} closemodal={()=>{this.props.closemodal&&this.props.closemodal()}}>
            <View style={styles.container}>
            <Text style={{paddingTop:hp('2%'),paddingBottom:hp('2%'),paddingLeft:10,fontSize:hp('2.5%')}}>Add More Location</Text>
            <Line/>
                <View style={{flex:.9,padding:10}}>
                        <View style={styles.flex_row}>
                                <View style={styles.view1}><Text style={styles.txt_style}>Country</Text></View>
                                <View style={styles.view2}>
                                    <DropdowContainer 
                                     
                                     _borwidth={1} _borColor={color.ash6}  brdrradius={1}
                                    drop_items={'Country'}
                                    values={this.state.countryDropdown.dropdown}
                                    keyvalue={"country_name"}
                                            value={this.state.countryvalue?this.state.countryvalue.country_name:'Country'}
                                             sendDropdownData={(data)=>this.sendDropdownData(data,'countryvalue')}
                                             fullwidth={true} 
                                    />
                                </View>
                            </View> 
                            <View style={styles.flex_row}>
                                <View style={styles.view1}><Text style={styles.txt_style}>State</Text></View>
                                <View style={styles.view2}>
                                    <DropdowContainer 
                                     _borwidth={1} _borColor={color.ash6}  brdrradius={1}
                                    drop_items={'Country'}
                                    values={this.state.stateDropdown.dropdown}
                                    keyvalue={"state_name"}
                                            value={this.state.statevalue?this.state.statevalue.state_name:'State'}
                                             sendDropdownData={(data)=>this.sendDropdownData(data,'statevalue')}
                                            fullwidth={true}
                                    /></View>
                            </View>
                            <View style={styles.flex_row}>
                                <View style={styles.view1}><Text style={styles.txt_style}>City</Text></View>
                                <View style={styles.view2}>
                                <DropdowContainer  _borwidth={1} _borColor={color.ash6}  brdrradius={1}
                                    drop_items={'City'}
                                    values={this.state.cityDropdown.dropdown}
                                    keyvalue={"city_name"}
                                            value={this.state.cityvalue?this.state.cityvalue.city_name:'City'}
                                             sendDropdownData={(data)=>this.sendDropdownData(data,'cityvalue')}
                                            fullwidth={true}  
                                    />
                                </View>
                            </View>
                            <View style={styles.flex_row}>
                                <View style={styles.view1}><Text style={styles.txt_style}>Postal Code</Text></View>
                                <Item style={[{height:hp('5%'),borderWidth:1,borderColor:color.ash6},styles.view2,{justifyContent:'flex-start'}]} regular><Input onChangeText={(data)=>{this.setState({postal:data})}} style={{padding:0,textAlign:'left'}} placeholder='' /></Item>
                            </View>
                            <View style={styles.flex_row}>
                                <View style={{flex:1}}><TouchableOpacity onPress={()=>this.addLocation()}><Text style={[styles.txt_style,{color:color.orange}]}>Add</Text></TouchableOpacity></View>
                                <View style={[{flex:1,flexDirection:'row',justifyContent:'flex-end'}]}><TouchableOpacity><Text style={[styles.txt_style,{color:color.blue}]}>Locate Map</Text></TouchableOpacity></View>
                            </View>
                            <View>
                                <Line/>
                            </View>
                        <View style={{flexDirection:'row',flexWrap:'wrap',paddingTop:hp('1.5%'),}}>
                                {this.state.locationarray.length>0&&this.state.locationarray.map((obj,i)=>{
                                return(
                                    <View style={{paddingTop:hp('1.5%'),paddingRight:hp('1.5%')}}>
                                        <View  style={{borderWidth:1,borderColor:color.ash6,padding:5,flexDirection:'row',alignItems:'center'}}>
                                                <Text style={{padding:3,paddingRight:12}}>{obj.name}</Text> 
                                                 
                                                <TouchableOpacity style={{height:'100%',alignItems:'center',flexDirection:'row',width:25}} onPress={()=>this.deleteItem(i)}><Image  tintColor={color.black} source={Cancel} style={{width:8,height:8,paddingLeft:3,paddingRight:5,}}></Image></TouchableOpacity>

                                        </View>
                         
                                    </View>
                                )
                            })}
                        </View> 
                </View>
                <View style={{flex:.1,flexDirection:'row',padding:10}}>
                <View style={[styles.view12,]}>
                        <Button onPress={this.clearLocation} style={[styles.add_btn,{paddingLeft:10,paddingRight:10,}]}>
                            <Text style={styles.cancel_txt}> ADD MORE </Text>
                        </Button>
                    </View>
                    <View style={[styles.view13]}>
                        <Button onPress={this.sendLocation} style={[styles.cancel_btn,{paddingLeft:wp('5%'),paddingRight:wp('5%')}]}>
                            <Text style={styles.cancel_txt}> SAVE </Text>
                        </Button>
                    </View>
                </View>
            </View>
            </ModalComp>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        height:hp('80%'),
        backgroundColor:color.white
    },
    txt_style:{
        fontSize:hp('2.5%')
    },
    flex_row:{flexDirection:'row',paddingTop:hp('1.5%'),paddingBottom:hp('1.5%')},
    view1:{flex:.3,flexDirection:'column',justifyContent:'center'},
    view2:{flex:.7,flexDirection:'row'},
    view12:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-start',
        
    },
    view13:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
      
    },
    cancel_txt:{
        fontSize:hp('2.3%'),
        color:color.white
    },
    cancel_btn:{
         paddingLeft:10,
        paddingRight:10,
        height:hp('6%'),
        borderRadius:3,
        backgroundColor:color.orange,
    },
    add_btn:{
        backgroundColor:color.blue,
        paddingLeft:10,
        paddingRight:10,
        height:hp('6%'),
        borderRadius:3
        
    }
})