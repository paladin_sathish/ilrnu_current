import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Alert,StatusBar,Modal,Image,TouchableHighlight,TouchableOpacity,KeyboardAvoidingView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input} from 'native-base';
import color from '../Helpers/color';
import { Actions,Router,Scene } from 'react-native-router-flux';
import ModalComp from "../components/Modal";
export default class Login extends React.Component{
	constructor(props){
		super(props);
		this.state={
			username:'',
			password:''
		}
		// alert(JSON.stringify(this.props));
	}
	submitlogin=()=>{
		var obj={username:this.state.username,password:this.state.password};
		this.props.submitlogin(obj);
	}
	loadsignup=()=>{
		// Actions.signup();
		var obj={'type':'signup'};
			// alert(JSON.stringify(obj));
		if(this.props.loadsignup){
		this.props.loadsignup(obj)
		}
	}
	changeText=(data,key)=>{
this.setState({[key]:data});
// this.setState({password:data});
	}
render(){
	return(
		<Content>
		<View style={{alignItems:'center'}}>
		<View style={styles.circle}>
			<View style={styles.innercircle}>
			</View>
		</View>
		<View style={styles.loginheader}>
		<Text style={styles.logintitle}>Login</Text>
		<Text style={styles.loginsubtitle}>As a Venue Provider</Text>
		</View>
	<View style={{width:'100%',padding:12}}>
          <Item regular style={[styles.texboxpadding,styles.itemsize]}>
            <Input placeholderTextColor={color.ash3} placeholder='User Email' onChangeText={(data)=>this.changeText(data,'username')}/>
          </Item>
          <Item regular style={[styles.itemsize]}>
            <Input placeholderTextColor={color.ash3} secureTextEntry={true} placeholder='Password' onChangeText={(data)=>this.changeText(data,'password')}/>
          </Item>
     </View>
     <View style={styles.submitbox}>
     <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-start'}}>
       <Button onPress={()=>this.loadsignup()} style={styles.actionbtn}>
            <Text style={styles.actionbtntxt}>SIGN UP</Text>
          </Button>
          </View>
          <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button onPress={()=>this.submitlogin()} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
           <Text style={styles.actionbtntxt}>LOGIN</Text>
          </Button>
          </View>
     </View>
     <View style={styles.sociallogin}>
<Text>(or) Login with</Text>
<View style={styles.socialiconlogin}>
	<TouchableOpacity><Image style={styles.socialicon} source={require('../images/social_media/google.png')}/></TouchableOpacity>
	<TouchableOpacity><Image style={styles.socialicon} source={require('../images/social_media/fb.png')}/></TouchableOpacity>
	<TouchableOpacity><Image style={styles.socialicon} source={require('../images/social_media/insta.png')}/></TouchableOpacity>
</View>
     </View>
		</View>
     </Content>
		)
}

}
const styles={

	circle:{
		width:hp('17%'),
		height:hp('17%'),
		borderRadius:hp('17%')/2,
		marginTop:'5%',
		borderWidth:1,
		alignItems:'center',
		justifyContent:'center',
		borderColor:color.orange,
	},
	innercircle:{
		position:'absolute',
		width:hp('15.5%'),
		height:hp('15.5%'),
		borderRadius:hp('15.5%')/2,
		margin:'auto',
		backgroundColor:color.ash,
		margin:0,
	},
	loginheader:{
		marginTop:hp('2%'),
		alignItems:'center'
	},
	logintitle:{
		fontSize:hp('3%'),
		color:color.orange
	},
	loginsubtitle:{
		fontSize:hp('2%'),
		color:color.black1,
	},
	texboxpadding:{
		marginBottom:20,
	},
	submitbox:{
		flex:1,
		flexDirection:'row',
		padding:12,
		borderBottomWidth:0.5
	},
	actionbtn:{
		borderRadius:5,
		width:hp('20%'),
		justifyContent:'center',
	},
	actionbtntxt:{
		textAlign:'center',
		color:color.white,
		fontSize:hp('2.3%')
	},
	sociallogin:{
		backgroundColor:color.ash1,
		height:hp('24%'),
		width:'100%',
		alignItems:'center',
		justifyContent:'center'
	},
	socialiconlogin:{
		marginTop:10,
		flexDirection:'row'
	},
	socialicon:{
		width:hp('4%'),
		height:hp('4%'),
		margin:5
	},
	itemsize:{
		height:hp('5.5%')
	}
}