import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList,ToastAndroid,ScrollView} from 'react-native';
import {Right,Top,Icon, Button,Root,Content} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LoadingOverlay from '../components/LoadingOverlay';

import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import Line from '../components/Line';
import placeholder_img from '../images/placeholder_img.png';
import links from '../Helpers/config';
import Toast from 'react-native-simple-toast';
import LoadCarouselModal from '../pages/loadCarousel';
import VenueDetailDesc from '../pages/venue_detail_desc';
import ModalComp from '../components/Modal';
import Book_your_venue from './book_your_venue';
import AsyncStorage from '@react-native-community/async-storage';
import TabLoginProcess from '../components/TabLoginProcess';
import SuccessPage from './SuccessPage';
// import Toast from 'react-native-simple-toast';

export default class ListingDetails extends Component{
    constructor(props) {
      super(props);
      this.state = {loading:null,
        status: 0,loginDetails:null,search_venue:null,visible:null,booksuccess:null,booking:null,photovisible:false,photoItems:[]
      };
    }
    componentWillReceiveProps(props){
    // console.log("backprops1",props);
    if(props.navigation){
    var loadingcheck=props.navigation.state.params.loading;
    var loadingcheckmsg=props.navigation.state.params.msg;
    var successpayment=props.navigation.state.params.successpayment;
        this.setState({loading:loadingcheck?loadingcheck:null});
    if(successpayment==true){
        // Toast.show(loadingcheckmsg,Toast.LENGTH_LONG)
        alert(loadingcheckmsg);
        // Toast.show(loadingcheckmsg,Toast.LONG)
    }else{
        // this.setState({})
        if(successpayment=='error'){
        Toast.show(loadingcheckmsg,Toast.LONG)
    
    }
  }
}
// alert(JSON.stringify(props));
// this.setState({search_venue:null});
}
 componentDidMount(){
     AsyncStorage.getItem('loginDetails', (err, result) => {
      
       if(result!=null){
    this.setState({loginDetails:JSON.parse(result)})
      }else{
    this.setState({loginDetails:null})

      }
     });
  }
    booknow=()=>{
     AsyncStorage.getItem('loginDetails', (err, result) => {
      
       if(result!=null){
    this.setState({loginDetails:JSON.parse(result)})
   
    // this.setState({bookobj:this.state.searchhome[this.state.activeIndex]},function(){
         this.setState({search_venue:true});
    this.setState({visible:null})

    // })
    // alert(JSON.stringify(this.state.searchhome[this.state.activeIndex]));

     //Actions.Venue_search();
      }else{
         this.setState({search_venue:null});
    this.setState({loginDetails:null})
    this.setState({booking:true,visible:true})

      }
     });
  }
  getLoginDetails=async()=>{
    let result= await AsyncStorage.getItem('loginDetails');
    return result;
   }
   loadCarousel=(data)=>{
     if(data.length>0){

     this.setState({photoItems:data},function(){
       this.setState({photovisible:true})
     });
   }else{
     alert("no images were added")
     // alert(JSON.stringify(data));
   }
   }
 favoriteAdd=async(data)=>{
         var result= await this.getLoginDetails();
         // alert(result);
              if(result!=null){
             this.setState({loginDetails:JSON.parse(result)})
    this.setState({visible:null})
       }else{
    this.setState({visible:true,booking:null})
           return;
       }
this.setState({status:this.state.status==1?0:1})
    fetch(links.APIURL+'addFavourite/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({userId:this.state.loginDetails.user_id,"venue_id":this.props.activeCategory.venue_id,status:this.state.status==1?true:false}),
        }).then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson);
            if(this.state.status==1){
                this.setState({status:true})
                  Toast.show('Favourite Added in your List !', Toast.LONG)
                
                // ToastAndroid.show('Favourite Added in your List !', ToastAndroid.CENTER);
            }else if(this.state.status==0){
                this.setState({status:false})
                  Toast.show('Your Favourite Deleted from List !', Toast.LONG)
                
                 // ToastAndroid.show('Your Favourite Deleted from List !', ToastAndroid.CENTER);
            }
            // this.setState({"ListVenueType":responseJson.data,loading:false})
            
        })
}
receivelogindet=(data)=>{
  if(this.state.booking){  
  this.setState({loginDetails:data},function(){
    this.setState({visible:null,search_venue:true,booking:null});
  })
  }
  }
   booksuccess=(data,data1)=>{
    data.bookdetails=data1;
    this.setState({booksuccess:data});
  }
   showDescription=(item)=>{
          this.setState({showDetailDesc:item})
        }
    render(){
        return(
           
            <View style={styles.container} onLayout={(data)=>this.props.layoutChange&&this.props.layoutChange(data)}>
                        {this.state.loading&&
                <LoadingOverlay/>
                 
}
            {this.state.photovisible&&
            <LoadCarouselModal items={this.state.photoItems} closemodal={()=>this.setState({photovisible:false})} />
          }
          {this.state.showDetailDesc&&
            <VenueDetailDesc visible={this.state.venuede} venuedetails={this.state.showDetailDesc} closemodal={()=>this.setState({showDetailDesc:null})} />
          }
                <View style={styles.view1}>
                    <View style={styles.view11}>
                   
                        <Text style={{fontSize:hp('2.8%')}}>
                           {this.props.activeCategory&&this.props.activeCategory.trn_venue_name}
                        </Text>
                    </View>
                    <View style={styles.view12}>
                        <StarRating
                            maxStars={1}
                            // fullStarColor={color.yellow}
                            starSize={30}
                             rating={this.state.status}
                             selectedStar={(data) => this.favoriteAdd(data)}
                            fullStarColor={'#ffc700'}
                            />
                    </View>    
                </View>
              
                <View style={{marginTop:hp('1.5%')}}>
                   
                    <View style={{paddingBottom:hp('1.5%'),flexDirection:'row',height:hp('15%')}}>
                        <View style={{flex:.4,}}>
                            
                                    {(this.props.activeCategory&&this.props.activeCategory.photos.length>0&&this.props.activeCategory.photos[0].venue_image_path)&&
                                      <TouchableOpacity onPress={()=>this.loadCarousel(this.props.activeCategory&&(this.props.activeCategory.photos.length>0?this.props.activeCategory.photos:[]))}>
                                      <Image source={{uri:this.props.activeCategory&&this.props.activeCategory.photos.length>0&&this.props.activeCategory.photos[0].venue_image_path}}style={{height:hp('15%'),width:'100%'}}></Image>
                                      </TouchableOpacity>
                                    }
                                    {!(this.props.activeCategory&&this.props.activeCategory.photos.length>0&&this.props.activeCategory.photos[0].venue_image_path)&&
                                      <Image source= {placeholder_img} style={{height:hp('15%'),width:'100%'}}></Image>
                                    }
                                    <View style={{position:'absolute',bottom:0,left:1}}>
                                        <View style={{marginTop:10}}>
                                <StarRating
                                    // disabled={false}
                                    maxStars={5}
                                    rating={4}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    halfStarColor={color.yellow}
                                    fullStarColor={color.yellow}
                                    starSize={8}
                                    containerStyle={styles.RatingBar1}
                                    starStyle={styles._starstyle}
                                    emptyStarColor={color.ash6}
                                />
                            </View> 
                        </View>

                        </View>
                        <View style={{flex:.6,backgroundColor:color.white,flexDirection:'column'}}>
                           
                            <View style={{flex:.8,paddingTop:hp('0%'),paddingLeft:hp('1.5%'),}}>
                            <View style={{flexDirection:'row'}}>
                            <View style={{flex:0.5}}>
                             <Text style={styles.bottom_text}>{this.props.activeCategory.price[0].trn_venue_price_currency} {this.props.activeCategory.price[0].trn_venue_price_amt} </Text>
                            </View>
                            <View style={{flex:0.5,justifyContent:'flex-end',flexDirection:'row'}}>
                            {this.props.activeCategory&&this.props.activeCategory.Distance&&
                                  <View style={{flexDirection:'row'}}>
                                    <Text style={styles.orange_text}> {this.props.activeCategory&&this.props.activeCategory.Distance}</Text>
                                    <Text style={styles.orange_text}> | </Text>
                                    <Text style={styles.orange_text}> {this.props.activeCategory&&this.props.activeCategory.Time} </Text>
                                    </View>
                                  }
                            </View>
                            </View>
                            <TouchableOpacity onPress={()=>this.showDescription(this.props.activeCategory)}>
                                <Text style={{fontSize:hp('1.5%'),}} numberOfLines={3}>
                                    {this.props.activeCategory?this.props.activeCategory.trn_venue_desc:'....'}
                                </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',bottom:10}}>
                            <View style={{flex:.4}}>
                            </View>
                            
                            <View style={{flex:.6,flexDirection:'row',paddingLeft:hp('1.5%')}}> 
                                <View style={{flexDirection:'row',flex:.5}} > 
                                
                                </View>
                                <View style={{flex:.5,flexDirection:'row',justifyContent:'flex-end'}}>
                                
                                    <Button style={[styles.booK_nw_btn]} onPress={()=>this.booknow()}>
                                        <Text style={[styles.booK_nw_txt]} >Book Now</Text>
                                    </Button>
                                </View>
                            </View>

                    </View> 
                      <View  style={{padding:0,marginLeft:5,flexDirection:'row',flexWrap:'wrap'}}>
                    {this.props.activeCategory&&this.props.activeCategory.ameneties.length>0&&this.props.activeCategory.ameneties.map((obj)=>{
                      return(
                        <View style={{borderWidth:0.5,paddingLeft:5,flexDirection:'row',alignItems:'center',paddingRight:5,paddingTop:2,paddingBottom:2}}>
                        <Image source={{uri:obj.amenities_icon}} style={{width:25,height:25}}/>
                          <Text style={{fontSize:hp('2%')}}>{obj.amenities_name}</Text>
                        </View>
                      )
                    })}
                    </View>
                    {this.props.cancel&&
                      <Button style={[styles.booK_nw_btn]} onPress={()=>this.props.clearList&&this.props.clearList()}>
                                        <Text style={[styles.booK_nw_txt]} >Cancel</Text>
                                    </Button>  
                    }
                </View>

               
                <View style={styles.bottom_container}>
                    <Line/>
                    {this.props.activeCategory&&this.props.activeCategory.price.length>0&&
                    <View style={{flexDirection:'row',paddingTop:6,paddingBottom:6}}>
                        <View style={{flex:1}}>
                         <Text style={styles.bottom_text}>Price per hour</Text>
                            
                        </View>
                        <View style={{flex:1}}>
                       <Text style={styles.bottom_text}>{this.props.activeCategory.price[0].trn_venue_price_currency} {this.props.activeCategory.price[0].trn_venue_price_amt} </Text>
                        </View>

                    </View>
                    }
                    <Line/>
                    {/*
                    <View style={{flexDirection:'row',paddingTop:6,paddingBottom:6}}>
                        <View style={{flex:1,}}>
                            <Text style={styles.bottom_text}>Offer Available</Text>
                        </View>
                        <View  style={{flex:1,}}>
                            <Text style={styles.bottom_text}>YES</Text>
                        </View>
                    </View>
                    <Line/>
                    <View style={{flexDirection:'row',paddingTop:6,paddingBottom:6}}>
                        <View  style={{flex:1,}}>
                            <Text style={styles.bottom_text}>Trainers Availability</Text>
                        </View>
                        <View style={{flex:1,}}>
                            <Text style={styles.bottom_text}>YES</Text>
                        </View>
                    </View>
                    <Line/>
                  */}
                </View> 
                 {this.state.search_venue&&
                    <View>
              <ModalComp  header={this.state.headervisible}   borderclr={color.grey} borderwdth={2}
          Nooverlay={true} titlecolor={color.orange} closemodal={()=>this.setState({search_venue:null,booksuccess:null})} modaltitle={this.state.title} modalsubtitle={this.state.modalsubtitle} visible={true} >
                     {!this.state.booksuccess&&
                  <Book_your_venue booksuccess={(data,data1)=>this.booksuccess(data,data1)} cancelBooking={()=>this.setState({search_venue:null})} bookdata={this.props.activeCategory} />
              }
              {this.state.booksuccess&&
                <SuccessPage closemodal={()=>this.setState({search_venue:null,booksuccess:null})} booksuccess={this.state.booksuccess} />
              }
                  </ModalComp>
                 </View>
             }
               {this.state.visible&&
        <TabLoginProcess sendlogindet={(data)=>this.receivelogindet(data)} type='login' visible={true} tabAction={this.tabAction} closetabmodal={()=>this.setState({visible:null})}/>
      }
            </View>


        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white,
        paddingLeft:hp('1.5%'),
        paddingRight:hp('1.5%'),
        paddingTop:hp('1.5%')
    },
    view1:{
        flexDirection:'row',
  //      flex:1
    },
    text_style1:{
        fontSize:hp('2%')
    },
    RatingBar:{
        height:'100%',
        width:'100%',
        position:'absolute',
        right:0,
        top:0,
        alignItems:'flex-end',
        // paddingRight:hp('2%')
    },
    RatingBar1:{
        position:'absolute',
        bottom:10,
        left:5,
     height:'100%'
       // marginBottom:hp('2%')
    },
     _starstyle:{
        marginRight:1,
        fontSize:hp('1.8%')
    },
    view11:{
        flex:0.8
    },
    view12:{
        flex:0.2,
        flexDirection:'row',
        justifyContent:'flex-end',
        // padding:5

        // backgroundColor:color.black1
       
    },
    orange_text:{
        color:color.orange,
        fontSize:hp('1.4%')
    },
    booK_nw_btn:{
        backgroundColor:color.blue,
        height:hp('3%'),
        paddingLeft:8,
        paddingRight:8,
    },
    booK_nw_txt:{
        color:color.white,
        fontSize:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8
    },
    bottom_container:{
        flexDirection:'column',
        marginTop:hp('1.5%'),
    },
    bottom_text:{
        fontSize:hp('1.7%')
    }
})