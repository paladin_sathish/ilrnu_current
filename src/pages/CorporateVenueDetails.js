'use strict';

import React, { Component } from 'react';

import { Platform, StyleSheet, Text, View, Alert, StatusBar, Modal, Image, TouchableHighlight, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Icon, Button, Subtitle, Header, Content, Body, Right, Title, Item, Input, Label, Textarea, Form } from 'native-base';
import ValidationLibrary from '../Helpers/validationfunction';
import DynamicForm from '../components/DynamicForm';
import color from '../Helpers/color';
import Corporate_AddLocation from '../pages/Corporate_AddLocation';
import LabelTextbox from '../components/labeltextbox';
import AddressComp from '../components/addressComp';
class CorporateVenueDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            facilitydisable:false,
            locationArray:[],
            locationvisible:null,
            venuetypedata:null,
            commonArray:[{
            "spec_det_id": 'idname',
            "venue_spec_id": 1,
            "spec_det_name": "Venue",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "text",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            readOnly:true,
            noborder:true,
            validation:[{name:'required'}],
            error:null,
            errormsg:''
        },{
            "spec_det_id": 'idname2',
            "venue_spec_id": 1,
            "spec_det_name": "Venue Name",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "text",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'}],
            error:null,
            errormsg:''
        },{
            "spec_det_id": 'idname3',
            "venue_spec_id": 1,
            "spec_det_name": "Admin",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "text",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'}],
            error:null,
            errormsg:''
        },{
            "spec_det_id": 'idname4',
            "venue_spec_id": 1,
            "spec_det_name": "Mail",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "text",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'},{name:'email'}],
            error:null,
            errormsg:''
        },{
            "spec_det_id": 'idname5',
            "venue_spec_id": 1,
            "spec_det_name": "Contact",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "number",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'},{name:'mobile'}],
            error:null,
            errormsg:''
        },{
            "spec_det_id": 'idname6',
            "venue_spec_id": 1,
            "spec_det_name": "Location",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "btn_add",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'}],
             readOnly:true,
            error:null,
            errormsg:''
        },],
            facilityData: null,
            addresscomp: '',
            modalstate: false,
            validations: {
                'roomname': {
                    error: null,
                    mandatory: true,
                    errormsg: '',
                    validations: [{ name: 'required', status: false }]
                },
                'seats': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: [{ name: 'required', status: false }]
                },
                'floor': {
                    error: null,
                    errormsg: '',
                    mandatory: false,
                    validations: []
                },
                'landmark': {
                    error: null,
                    errormsg: '',
                    mandatory: false,
                    validations: []
                },
                'mobile': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: [{ name: 'required', status: false }, { name: 'mobile', status: false }]
                },
                'mail': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: [{ name: 'required', status: false }, { name: 'email', status: false }]
                },
                'address': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: [{ name: 'required', status: false }]
                }
            }
        };
    }
saveLocation=()=>{
    var errordata=this.checkValidations();
    // alert(errordata.length);
    if(errordata.length==0){
        alert("Venue Details Added Successfully")
        if(this.props.sendCorporateVenueLocation){
            this.props.sendCorporateVenueLocation(this.state.commonArray,this.state.locationArray,'success');
        }
    }else{
        if(this.props.sendCorporateVenueLocation){
            this.props.sendCorporateVenueLocation(this.state.commonArray,this.state.locationArray,'failure');
        }
    }
    // alert(JSON.stringify(this.state.commonArray));
}
    componentWillMount() {
        this.props.labeltext({ labeltext: <Text>Please add your <Text style={{color:color.orange,fontWeight:'bold'}}>Venue Details </Text></Text> });
    }
    // componentWillReceiveProps(props) {
    //     // alert(JSON.stringify(props));
    //     if (props.facilityData) {
    //         var facilityData = this.state.facilityData;
    //         facilityData = props.facilityData;
    //         this.setState({ facilityData });
    //     }
    //     if(props.commonArray){
    //         this.setState({commonArray:props.commonArray});
    //     }
    //     if (props.facilityvalidations) {
    //         this.setState({ validations: props.facilityvalidations });
    //     }
    // }
    getAddress = () => {

        this.setState({ modalstate: true });
    }
    receiveAddress = (data, location) => {
        // alert(JSON.stringify(location));
        // var facilityData = this.state.facilityData;
        // facilityData.address = data.usertxt;
        // this.setState({ facilityData });
        // this.setState({ modalstate: false });
        // var commonArray=this.state.commonArray;
        // commonArray[1].spec_det_datavalue1=data.usertxt;
        // commonArray[1].spec_det_datavalue2=data.location?data.location[Object.keys(data.location)[0]].toString()+","+data.location[Object.keys(data.location)[1]]:null;
        // this.setState({commonArray});
        // this.props.sendfaciltiydata(this.state.facilityData,commonArray);
        // this.changeText(data.usertxt,'address','spec_det_datavalue1')
    }
    changeText = (data, data2,dataobj, key) => {
        var facilityData=this.state.facilityData;
        var commonArray=this.state.commonArray;
        if(data2){
            if(data2=='address'){
                this.getAddress();
            }else if(data2=='addlocation'){
                // alert("location adding...");
                this.setState({locationvisible:true});
            }
            return;
        }else{
            // alert("nothing");
        }
        if(Number.isInteger(dataobj.spec_det_id)){
            var findIndex=facilityData.specDetails.findIndex((obj)=>obj.spec_det_id==dataobj.spec_det_id);
            if(findIndex!=-1){
                facilityData.specDetails[findIndex].spec_det_datavalue1=data;
                this.setState({facilityData})

            }
        }else{
            var findIndex=commonArray.findIndex((obj)=>obj.spec_det_id==dataobj.spec_det_id);
            if(findIndex!=-1){
                var errorcheck=ValidationLibrary.checkValidation(data,commonArray[findIndex].validation);
                console.log(errorcheck);
                commonArray[findIndex].spec_det_datavalue1=data;
                commonArray[findIndex].error=!errorcheck.state;
                commonArray[findIndex].errormsg=errorcheck.msg;
                this.setState({commonArray},function(){
                    this.checkValidations();
                });

            }
        }
        // this.props.sendfaciltiydata(facilityData,commonArray);
        // var errormsg = ValidationLibrary.checkValidation(data, this.state.validations[key].validations);
        // var validations = this.state.validations;
        // validations[key].error = !errormsg.state;
        // validations[key].errormsg = errormsg.msg;
        // this.setState({ validations });
        // var mandatorylength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true).length;
        // var noerrorlength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true&&validations[obj].error == false).length;
        // // alert(JSON.stringify(noerrorlength));
        // var noerror = false;
        // if (mandatorylength == noerrorlength) {
        //     noerror = true;
        // }
        // this.props.sendfaciltiydata({ key: key, value: data, dbkey: dbkey, validations: validations, noerror: noerror });
        // var facilityData = this.state.facilityData;
        // facilityData[key] = data;
    }
    receiveLocation=(data)=>{
        // alert(JSON.stringify(data));
        var locationArray=[];
            var locationData='';
        var commonArray=this.state.commonArray;
        if(data.length>0){
            var locationArray=this.state.locationArray;
            // locationArray.findIndex((obj)=>obj.id==)
            for(var i in this.state.locationArray){
                var findIndex=data.findIndex((obj)=>obj.id==this.state.locationArray[i].id);
                if(findIndex!=-1){
                    data[findIndex]=this.state.locationArray[i];
                }
            }
            this.setState({locationArray:data});
            locationData=data.map((obj)=>obj.name).join(',')
        }
        commonArray[5].spec_det_datavalue1=locationData;
        this.setState({commonArray});
        this.setState({locationvisible:false});
        this.checkValidations();
        // this.setState({receiveLocation:})
    }
        checkValidations=()=>{
        var commonArray=this.state.commonArray;
        for(var i in commonArray){
        var errorcheck=ValidationLibrary.checkValidation(commonArray[i].spec_det_datavalue1,commonArray[i].validation);
        commonArray[i].error=!errorcheck.state;
        commonArray[i].errormsg=errorcheck.msg;
        }
        var errordata=commonArray.filter((obj)=>obj.error==true);
        if(errordata.length!=0){
            // alert("error");
            // this.props.sendfaciltiydata(this.state.facilityData,this.state.commonArray,false);
        }else{
            // alert("noerror");
            // this.props.sendfaciltiydata(this.state.facilityData,this.state.commonArray,true);
        }

        this.setState({commonArray});
        return errordata;
    }
    addFacility=()=>{
        this.props.moveFacilities();
    }
    render() {
        // alert(JSON.stringify(this.state.facilityData));
        return (
            <Content>
            <View style={{padding:20,marginTop:18}}>
            {/*
            <LabelTextbox error={this.state.validations.roomname.error} errormsg={this.state.validations.roomname.errormsg} capitalize={true} changeText={(data)=>this.changeText(data,'roomname','trn_venue_room_name')} value={this.state.facilityData.roomname} labelname="Room Name"/>
            <View style={{flex:1,flexDirection:'row',height:hp('6%'),marginBottom:25,}}>
                <View style={{flex:0.3,justifyContent:'center'}}>
                    <Label>Seats</Label>
                </View>
                <View style={{flex:0.7,flexDirection:'row',justifyContent:'space-between'}}>
                <View style={{flex:0.25}}> 
                <Item  regular style={styles.itemsize}>
            <Input  keyboardType="number-pad" onChangeText={(data)=>this.changeText(data,'seats','trn_venue_room_seats')} value={this.state.facilityData.seats} style={{height:'100%',padding:0}}/>
            {this.state.validations.seats.error&&
            <Text style={{position:'absolute',bottom:-hp('2.5%'),fontSize:hp('1.5%'),color:color.red}}>{this.state.validations.seats.errormsg}</Text>
            }
            </Item>
                </View>
                <View style={{flex:0.65,flexDirection:'row'}}>
                <View style={{flex:1}}>
                <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'floor','trn_venue_room_floor')} value={this.state.facilityData.floor} labelname="Floor"/>

                </View>
                </View>
            </View>
            </View>
            <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'address','trn_venue_room_address')} value={this.state.facilityData.address} type='textarea'  placeholder="Address" rowspan={3} labelname="Address"/>
            <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'landmark','trn_venue_room_landmark')} value={this.state.facilityData.landmark} labelname="LandMark">
            <TouchableOpacity onPress={this.getAddress} style={{paddingLeft:12,paddingRight:12}}><Text style={{textAlign:'right',color:color.blueactive,fontSize:hp('2.1%')}}>Location Map</Text></TouchableOpacity>
            </LabelTextbox>
            <LabelTextbox  error={this.state.validations.mobile.error} errormsg={this.state.validations.mobile.errormsg} capitalize={true} changeText={(data)=>this.changeText(data,'mobile','trn_venue_room_mob')} value={this.state.facilityData.mobile} labelname="Mobile"/>
            <LabelTextbox capitalize={true} error={this.state.validations.mail.error} errormsg={this.state.validations.mail.errormsg} changeText={(data)=>this.changeText(data,'mail','trn_venue_room_mail')} value={this.state.facilityData.mail} labelname="Mail"/>
        */}
         <AddressComp receiveAddress={this.receiveAddress} modalstate={this.state.modalstate}/>
         {this.state.locationvisible&&
         <Corporate_AddLocation locationArray={this.state.locationArray} closemodal={()=>this.setState({locationvisible:false})} receiveLocation={this.receiveLocation}/>
     }
         {this.state.venuetypedata&&
        <DynamicForm changeText={this.changeText}  commonArray={this.state.commonArray} />
    }
    <View style={{flex:.1,flexDirection:'row'}}>
                    <View style={[styles.view12,]}>
                        <Button onPress={this.saveLocation}  style={[styles.cancel_btn,{paddingLeft:wp('5%'),paddingRight:wp('5%'),backgroundColor:color.blueactive}]} >
                            <Text style={styles.cancel_txt}>Save</Text>
                        </Button>
                    </View>
                   <View style={[styles.view13,]}>
                        <Button  onPress={()=>this.addFacility()} style={[styles.cancel_btn,{paddingLeft:wp('5%'),paddingRight:wp('5%'),backgroundColor:color.blueactive}]}>
                            <Text style={styles.cancel_txt}>Add Facilities</Text>
                        </Button>
                    </View>

                </View>    

            </View>
        </Content>
        );
    }
    componentDidMount(){
        // alert(JSON.stringify(this.props.facilityData));
        if(this.props.venuetypedata){
            // alert(JSON.stringify(this.props.venuetypedata));
            // this.
            var venutypedata=JSON.parse(JSON.stringify(this.props.venuetypedata));
            this.setState({venuetypedata:venutypedata})
            var commonArray=this.state.commonArray;
            commonArray[0].spec_det_datavalue1=venutypedata.venue_cat_name;
            this.setState({commonArray});
        }
        if(this.props.commonArray){
            this.setState({commonArray:JSON.parse(JSON.stringify(this.props.commonArray))})
        } if(this.props.locationArray){
            this.setState({locationArray:JSON.parse(JSON.stringify(this.props.locationArray))})
        }
        // if(this.props.commonArray){
        //     this.setState({commonArray:this.props.commonArray});
        // }
    }
}

const styles = StyleSheet.create({
    itemsize: {
        height: hp('5.5%')
    }, 
     view12:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-start',
        
    },
    view13:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
      
    },
    cancel_txt:{
        fontSize:hp('2.3%'),
        color:color.white,
        textAlign:'center'
    },
    cancel_btn:{
         paddingLeft:10,
        paddingRight:10,
        height:hp('6%'),
        borderRadius:3,
        minWidth:hp('15%'),
        justifyContent:'center',
        backgroundColor:color.orange,
    },
    add_btn:{
        backgroundColor:color.blue,
        paddingLeft:10,
        paddingRight:10,
        height:hp('6%'),
        borderRadius:3
        
    }
});


export default CorporateVenueDetails;