import React, { Component } from 'react';
import {Image, Platform,TouchableHighlight, StyleSheet, Text, View, BackHandler, Alert, TouchableOpacity,ScrollView,ToastAndroid,ActivityIndicator } from 'react-native';
import {H3,Container, Content, Header, Left, Body, Right, Button, Icon, Title,List,ListItem, } from 'native-base';
import { Actions } from 'react-native-router-flux';
import SubHeaderComp from '../components/subHeader';
import SessionList from '../components/SessionList';
import VideoComp from '../components/VideoComp';
import TabLoginProcess from '../components/TabLoginProcess';
import color from '../Helpers/color';
import SearchBox from '../components/SearchBox1';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import placeholder_img from '../images/placeholder_img.png';
import Geolocation from 'react-native-geolocation-service';

import links from '../Helpers/config';
var imageList1=[{image:require('../images/slider1.png')},{image:require('../images/slider2.png')},{image:require('../images/slider3.png')},{image:require('../images/slider4.png')}]
var imageList2=[{image:require('../images/slider2.png')},{image:require('../images/slider1.png')},{image:require('../images/slider3.png')},{image:require('../images/slider1.png')}]
var imageList3=[{image:require('../images/slider1.png')},{image:require('../images/slider2.png')},{image:require('../images/slider2.png')},{image:require('../images/slider1.png')}]
var imageList4=[{image:require('../images/slider1.png')},{image:require('../images/slider3.png')},{image:require('../images/slider2.png')},{image:require('../images/slider1.png')}]


export default class VenuePage extends Component {
	constructor(props){
		super(props);
		this.count=0;
		// alert(JSON.stringify(props))
		this.state={visible:false,sessionTrue:props.navigation.state.params.raiselogin==true?false:null,loadvideo:false,listvenue:null,showSearchData:false,nearbyVenues:[],topratedVenues:[],demandVenues:[],luxuryVeneus:[],preferredVenues:[],count:0,chosenlocation:{latitude:0,longitude:0}};
	}
// 	closepopup=()=>{
// 		this.setState({visible:false});
// 	}
nearbyvenues=(data)=>{
fetch(links.APIURL+"nearbylocation", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({lat:data.lat,long:data.lng}),
    }).then((response)=>response.json())
.then((responsejson)=>{
	// alert(JSON.stringify(responsejson));
	console.log(responsejson);
	this.setState({nearbyVenues:responsejson,count:this.state.count+1})
	// this.count+=1;
})
}
topratedvenues=(data)=>{
fetch(links.APIURL+"getTopRatedVenues", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({lat:data.lat,long:data.lng}),
    }).then((response)=>response.json())
.then((responsejson)=>{
	// alert(JSON.stringify(responsejson));
	this.setState({topratedVenues:responsejson,count:this.state.count+1})
	// this.count+=1;
}).catch((err)=>{
	console.log("topratedVenues",err)
})
}
demandVenues=(data)=>{
fetch(links.APIURL+"getDemandVenues", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({lat:data.lat,long:data.lng}),
    }).then((response)=>response.json())
.then((responsejson)=>{
	// alert(JSON.stringify(responsejson));
	this.setState({demandVenues:responsejson,count:this.state.count+1})

})
}
luxuryvenues=(data)=>{
fetch(links.APIURL+"getLuxuriesVeneus", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({lat:data.lat,long:data.lng}),
    }).then((response)=>response.json())
.then((responsejson)=>{
	// alert(JSON.stringify(responsejson));
	this.setState({luxuryVeneus:responsejson,count:this.state.count+1})
})
}
getPreferredvenues=(data)=>{
fetch(links.APIURL+"getPreferedVenues", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({lat:data.lat,long:data.lng}),
    }).then((response)=>response.json())
.then((responsejson)=>{
	// alert(JSON.stringify(responsejson));
	this.setState({preferredVenues:responsejson,count:this.state.count+1})
})
}
componentDidMount() {
              // alert("")
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
        handleBackButton=()=> {
        // alert("back button disabled");
        // ToastAndroid.show('Back button is disabled', ToastAndroid.SHORT);
        // Actions.reset('home')
        // return true;
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
	listyourvenue=()=>{

// this.setState({loadvideo:true});
// this.setState({listvenue:true});
// this.setState({sessionTrue:true});
this.closevideo();
	}
	closevideo=()=>{
		AsyncStorage.getItem('loginDetails', (err, result) => {
		this.setState({loadvideo:false});
 	if(result !=null){
 		if(JSON.parse(result).user_cat_id==1){
		// this.setState({sessionTrue:true});
		Actions.venueform({logindata:JSON.parse(result)});
	}else{

		Actions.corporateform({logindata:JSON.parse(result)});
	}
 	}else{
// this.setState({loadvideo:true});
// if(this.state.listvenue==false){
		this.setState({sessionTrue:false});
	
// }
 	}
 });
	}
	sendListingDetails=(data)=>{
		Actions.BookDetails({
                             lat: this.state.chosenlocation.latitude,
                             long: this.state.chosenlocation.longitude,
                             id: data.venue_id
                           })
		// Actions.listingdetails({activeCategory:data});
	}
	tabAction=(data)=>{
		if(data.user_cat_id==1){

		Actions.venueform({logindata:data,welcomemsg:'User Logged in Successfully'});
	}else{
		Actions.corporateform({logindata:data,welcomemsg:'User Logged in Successfully'});
		// alert(JSON.stringify(data));
	}
	}
// 	 showsearch=(data)=>{
//         this.setState({showSearchData:data})
//     }
componentWillMount(){
	  Geolocation.getCurrentPosition(
       (position) => {
       	console.log("positionarrive")
         var latlng={lat:position.coords.latitude,lng:position.coords.longitude};
         this.setState({chosenlocation:{latitude:position.coords.latitude,longitude:position.coords.longitude}});
         this.nearbyvenues(latlng)
         this.topratedvenues(latlng);
         this.demandVenues(latlng);
         this.luxuryvenues(latlng);
         this.getPreferredvenues(latlng);
         //this.getAddressByLatLng(position.coords);
      //    this.categoryListing();
      //    // this.NearbyPlayground(position.coords,null);
      //  console.log("latitude",position.coords.latitude);
      // console.log("longitude",position.coords.longitude);
       },
       
       (error) =>console.log( error.message),
       {timeout: 30000,maximumAge:0},
     );
}
	render(){
		return(
			<Container> 
			<SubHeaderComp bgcolor={color.top_red}>

				<View style={[styles.venuebtnview,{backgrounColor:color.orange}]}>
                    <Button onPress={()=>this.listyourvenue()} style={[styles.venuebtn,{backgroundColor:color.orange}]}>
                        <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <Text style={styles.venubtntext}>List your Venue Here</Text>
                            <View style={{flexDirection:'column',justifyContent:'center',marginLeft:3,}}>
                                <Icon style={{fontSize:hp('2%'),color:color.white}} type="FontAwesome" name="angle-right" />
                            </View>
                        </View>
                    </Button>
				</View>
			</SubHeaderComp>

			<Content>
			<View>
			{this.state.count==5&&
				<View>
<View>

				{this.state.preferredVenues.length>0&&
			<TouchableOpacity style={styles.flexrow}  onPress={()=>this.sendListingDetails(this.state.preferredVenues[0])}>
				     <View>
                    {(this.state.preferredVenues[0].photos.length>0&&this.state.preferredVenues[0].photos[0].venue_image_path)&&
                        <Image style={styles.headimage} alt="No Image"
                            source= {{uri:this.state.preferredVenues[0].photos.length>0&&this.state.preferredVenues[0].photos[0].venue_image_path}}>       
                        </Image>}
                         {!(this.state.preferredVenues[0].photos.length>0&&this.state.preferredVenues[0].photos[0].venue_image_path)&&
                        <Image style={styles.headimage} alt="No Image"
                            source= {placeholder_img}>       
                        </Image>
                      }
                      </View>
				<View style={{flex:1,flexDirection:'row',padding:4}}>
				    <View style={{padding:1}}>
				        <Text style={{paddingBottom:5,fontSize:hp('2.3%'),color:color.top_red}}>Most Preferred Venues</Text>
				        <Text style={{paddingBottom:4,fontSize:hp('1.8%')}}>{this.state.preferredVenues[0].trn_venue_desc}</Text>

				        <TouchableOpacity onPress={()=>this.sendListingDetails(this.state.preferredVenues[0])}>
                            {/* <Text style={{color:color.blueactive,fontSize:hp('1.8%'),marginRight:hp('1.5%')}} >More
                                <Icon style={{fontSize:hp('1.8%'),color:color.grey}} type="FontAwesome" name="angle-right" />
                            </Text> */}
                            <View style={{flexDirection:'row',marginTop:hp('1%'),}}>
                                <Text style={{fontSize:hp('1.8%'),color:color.blueactive,marginRight:hp('1.5%')}}>More</Text>
                                <View style={{flexDirection:'column',justifyContent:'center',marginLeft:2,}}>
                                    <Icon style={{fontSize:hp('2%'),color:color.grey}} type="FontAwesome" name="angle-right" /> 
                            </View>
                        </View>
                        </TouchableOpacity>
                    </View>
				    <View style={{alignItems:'center',flexDirection:'row'}}>
				        <TouchableOpacity>
				            <Icon style={{fontSize:hp('5%'),color:color.black}} type="FontAwesome" name="angle-right" />
                        </TouchableOpacity>
                    </View>
				</View>
			</TouchableOpacity>
                }
			</View>
			<SessionList sendVenueDetails={(data)=>this.sendListingDetails(data)} icon={true} title="Near By Venues" subtitle="More" action={false} imageList={this.state.nearbyVenues}/>
			<SessionList sendVenueDetails={(data)=>this.sendListingDetails(data)} icon={true} title="Top Rated Venues" subtitle="More" action={false} imageList={this.state.topratedVenues}/>
			<SessionList sendVenueDetails={(data)=>this.sendListingDetails(data)} icon={true} title="On Demand venues" subtitle="More" action={false} imageList={this.state.demandVenues}/>
			<SessionList sendVenueDetails={(data)=>this.sendListingDetails(data)} icon={true} title="Luxury Venue" subtitle="More" action={false} imageList={this.state.luxuryVeneus}/>
			
			
			{this.state.sessionTrue==false&&
				<TabLoginProcess loginttype="List your Venue" type='login' visible={true} sendlogindet={this.tabAction}/>
			}
			{this.state.loadvideo==true &&

				<VideoComp headerText={"How to Add venue in iVNEU?"} closevideo={()=>this.closevideo()} />
			}
			</View>
		}
		{this.state.count!=5&&
			<ActivityIndicator style={{marginTop:12}} size="large" color="#0000ff" />
		}
			</View>
			</Content>

			</Container>
			)
	}

}
const styles={
	venuebtnview:{
		alignSelf:'flex-end',
        backgrounColor:color.orange
	},
	col3:{
		flex:0.4,
	},
	col7:{
		flex:0.6
	},
	headimage:{
		width:hp('18%'),
		height:hp('14%'),
		borderRadius:5,
		marginRight:12
	},
	venuebtn:{
        paddingRight:10,
        paddingLeft:10,
        height:hp('5%'),
        backgrounColor:color.orange
	},
	venubtntext:{
        color:color.white,
        
	},
	flexrow:{
		flexDirection:'row',
		padding:12,
		borderBottomWidth:0.4		
	}
}