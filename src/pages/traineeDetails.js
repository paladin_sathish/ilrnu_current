"use strict";

import React, { Component } from "react";
import Next from "../components/button";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  StatusBar,
  Modal,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  PermissionsAndroid
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import links from "../Helpers/config";
import {
  Container,
  Icon,
  Button,
  Subtitle,
  Header,
  Content,
  Body,
  Right,
  Title,
  Item,
  Input,
  Label,
  Textarea,
  Form,
  ActionSheet,
  Root,
} from "native-base";
import ValidationLibrary from "../Helpers/validationfunction";
import DynamicForm from "../components/DynamicForm";
import color from "../Helpers/color";
import LabelTextbox from "../components/labeltextbox";
import AddressComp from "../components/addressComp";
import ImagePicker from "react-native-image-picker";
import PhotoInfo from "../images/svg/photoinfo.png";
import PhotoNext from "../images/photo_next.png";
import Info from "../images/info.png";
import PhotoPlus from "../images/svg/plus.png";
import ModalComp from "../components/Modal";


var BUTTONS = ["Gallery", "Camera", "Cancel"];
var MoreDetails = [
  {
    expanded: false,
    title: "",
    content: (
      <View>
        <Text></Text>
      </View>
    ),
  },
];
var CANCEL_INDEX = 3;
const options = {
  title: "Select Avatar",
  multiple: true,
  customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images",
  },
};
async function requestCameraPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "iLRNU App Camera and gallery Permission",
        message: "iLRNU App need access to your camera and gallery",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

const data = new FormData();

class TraineeDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sport_name_selection: [],
       arrayImages: [],
          arrayVenuImages: data,
          selectedImage: "",
          imagevisible: false,
          visible:false,
      commonArray: [
        {
          spec_det_id: "idname",
          venue_spec_id: 1,
          spec_det_name: "Sport Name",
          spec_det_sortorder: 0,
          spec_det_datatype1: "picker",
          spec_det_datavalue1: "1",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          picker_data: [],
          validation: [{ name: "required" }],
          error: null,
          errormsg: "",
        },
        {
          spec_det_id: "idname1",
          venue_spec_id: 1,
          spec_det_name: "Experience (Years)",
          spec_det_sortorder: 0,
          spec_det_datatype1: "number",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }, { name: "minLength", params: 30 }],
          error: null,
          errormsg: "",
        },

        {
          spec_det_id: "idname2",
          venue_spec_id: 1,
          spec_det_name: "Experience (Months)",
          spec_det_sortorder: 0,
          spec_det_datatype1: "number",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }, { name: "minLength", params: 30 }],
          error: null,
          errormsg: "",
        },

        {
          spec_det_id: "idname3",
          venue_spec_id: 1,
          spec_det_name: "Certification",
          spec_det_sortorder: 0,
          spec_det_datatype1: "btn_address1",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [
            { name: "required" },
            { name: "minLength", params: 300 },
          ],
          error: null,
          errormsg: "",
        },
        {
          spec_det_id: "idname4",
          venue_spec_id: 1,
          spec_det_name: "Achievements",
          spec_det_sortorder: 0,
          spec_det_datatype1: "textarea",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [
            { name: "required" },
            { name: "minLength", params: 1000 },
          ],
          error: null,
          errormsg: "",
        },
        {
          spec_det_id: "idname5",
          venue_spec_id: 1,
          spec_det_name: "Address",
          spec_det_sortorder: 0,
          spec_det_datatype1: "textareamap",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "12.9716,77.5946",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [
            { name: "required" },
            { name: "minLength", params: 300 },
          ],
          error: null,
          errormsg: "",
        },
        {
          spec_det_id: "idname6",
          venue_spec_id: 1,
          spec_det_name: "LandMark",
          spec_det_sortorder: 0,
          spec_det_datatype1: "text",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [],
          error: null,
          errormsg: "",
        },
        {
          spec_det_id: "idname7",
          venue_spec_id: 1,
          spec_det_name: "Area",
          spec_det_sortorder: 0,
          spec_det_datatype1: "text",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [
            { name: "required" },
            { name: "minLength", params: 300 },
          ],
          error: null,
          errormsg: "",
        },
      ],
      facilityData: null,
      addresscomp: "",
      modalstate: false,
      validations: {},
    };
  }


   modalState = (data) => {
        this.setState({ visible: data });
    }

  componentWillMount() {
    this.props.headertext({
      headertext: (
        <Text
          style={{
            fontSize: hp("1.7%"),
            fontWeight: "bold",
            color: color.blue,
          }}
        >
          Specific Training
        </Text>
      ),
    });
    this.props.labeltext({
      labeltext: (
        <Text>
          Please add your{" "}
          <Text style={{ color: color.orange, fontWeight: "bold" }}>
            Skills
          </Text>
        </Text>
      ),
    });
  }

  onSubmit = () => {

     this.props.onSubmit({
                       formimagearray: this.state.arrayVenuImages,
                       localimages: this.state.arrayImages,
                       delete:false
                     });


  };

  getSubCategory = () => {
    fetch(links.APIURL + "getTrainingSubSpecificList/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user_cat_id:
          this.props.loginDetails && this.props.loginDetails.user_cat_id,
        SpecificId: this.state.facilityData.SpecificId,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status == 0) {
          var commonArray = this.state.commonArray;
          commonArray[0].picker_data = responseJson.data;
          this.setState({ commonArray });
        }
      });
  };

  componentWillReceiveProps(props) {
    // alert(JSON.stringify(props));
    console.log("cwrp");
    if (props.facilityData) {
      var facilityData = this.state.facilityData;
      facilityData = props.facilityData;
      this.setState({ facilityData });
    }
    if (props.commonArray != this.state.commonArray) {
      console.log("commonarray");
      console.log(this.state.commonArray);
      this.setState({ commonArray: props.commonArray });
    }

    if (props.localimages) {
      if (props.localimages.length > 0) {
        this.setState({ arrayImages: props.localimages });
      }
    }

    if (props.facilityvalidations) {
      this.setState({ validations: props.facilityvalidations });
    }
  }
  getAddress = () => {
    this.setState({ modalstate: true });
  };
  receiveAddress = (data, location) => {
    // alert(JSON.stringify(location));
    // var facilityData = this.state.facilityData;
    // facilityData.address = data.usertxt;
    // this.setState({ facilityData });
    this.setState({ modalstate: false });
    var commonArray = this.state.commonArray;
    commonArray[5].spec_det_datavalue1 = data.usertxt;
    commonArray[5].spec_det_datavalue2 = data.location
      ? data.location[Object.keys(data.location)[0]].toString() +
        "," +
        data.location[Object.keys(data.location)[1]].toString()
      : null;
    this.setState({ commonArray });

    // this.props.sendfaciltiydata(this.state.facilityData, commonArray);

    this.props.sendfaciltiydata(this.state.facilityData, commonArray, true);
    // this.changeText(data.usertxt,'address','spec_det_datavalue1')
  };

  changeText = (data, data2, dataobj, key) => {
    var facilityData = this.state.facilityData;
    var commonArray = this.state.commonArray;
    if (data2) {
      if (data2 == "address") {
        this.getAddress();
        return;
      }
    } else {
      // alert("nothing");
    }

    var findIndex = commonArray.findIndex(
      (obj) => obj.spec_det_id == dataobj.spec_det_id
    );

    if (findIndex != -1) {
      var errorcheck = ValidationLibrary.checkValidation(
        data,
        commonArray[findIndex].validation
      );

      commonArray[findIndex].spec_det_datavalue1 = data;
      commonArray[findIndex].error = !errorcheck.state;
      commonArray[findIndex].errormsg = errorcheck.msg;
      this.setState({ commonArray }, function () {
        console.log("coomm", commonArray);
        this.checkValidations();
      });
    }

    this.props.sendfaciltiydata(facilityData, commonArray, true);
  };

  //check validation
  checkValidations = () => {
    var commonArray = this.state.commonArray;
    for (var i in commonArray) {
      var errorcheck = ValidationLibrary.checkValidation(
        commonArray[i].spec_det_datavalue1,
        commonArray[i].validation
      );
      commonArray[i].error = !errorcheck.state;
      commonArray[i].errormsg = errorcheck.msg;
    }
    var errordata = commonArray.filter((obj) => obj.error == true);
    if (errordata.length != 0) {
      // alert("error");
      this.props.sendfaciltiydata(
        this.state.facilityData,
        this.state.commonArray,
        false
      );
    } else {
      // alert("noerror");
      this.props.sendfaciltiydata(
        this.state.facilityData,
        this.state.commonArray,
        true
      );
    }

    // this.setState({commonArray});
    // return errordata;
  };

  loadDelete = (key, type, notexist) => {
    var count = this.state.arrayImages.filter((obj) => obj.checkdelete == true);
    var arrayImages = this.state.arrayImages;
    if (type == "long") {
      arrayImages[key].checkdelete = true;
    } else {
      if (count.length > 0) {
        if (this.state.arrayImages.length > 0) {
          arrayImages[key].checkdelete = !arrayImages[key].checkdelete;
        }
      } else {
        this.setState({ imagevisible: true });
        // const imgsource = { uri: 'data:image/jpeg;base64,' + count.data };
        this.setState({
          selectedImage: notexist
            ? "data:image/jpeg;base64," + arrayImages[key].data
            : arrayImages[key].url,
        });
        // console.log(arrayImages[key].data);
      }
    }
    // this.props.venueImages(arrayImages);
    this.setState({ arrayImages });
  };

  photoservice = () => {
    Toast.show("Request has been Sent!", Toast.LONG);

    // this.closemodal();
  };

  closeimage = () => {
    this.setState({ imagevisible: false });
  };

  loadPhotos = () => {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        title: "Upload",
      },
      (buttonIndex) => {
        this.loadGalleryCamera(buttonIndex);
      }
    );
  };
  delteSelected = () => {
    var arrayImages = this.state.arrayImages.filter(
      (obj) => obj.checkdelete != true
    );
    this.setState({ arrayImages });
    var formarrayImages = arrayImages.filter(
      (obj) => obj.checkdelete != true && !obj.exist
    );
    // alert(formarrayImages.length);
    var arrayVenuImages = new FormData();
    for (var i = 0; i < formarrayImages.length; i++) {
      arrayVenuImages.append("formArray", {
        uri: formarrayImages[i].uri,
        type: formarrayImages[i].type,
        name: formarrayImages[i].fileName,
      });
    } 
    this.setState({ arrayVenuImages: arrayVenuImages });
       this.props.onSubmit({
         formimagearray: arrayVenuImages,
         localimages: arrayImages,
         delete:true
       });

  };

  responseCamerGallery = (response) => {
    console.log(response);
    if (response.didCancel) {
      console.log("User cancelled image picker");
    } else if (response.error) {
      console.log("ImagePicker Error: ", response.error);
    } else if (response.customButton) {
      console.log("User tapped custom button: ", response.customButton);
    } else {
      var arrayImages = this.state.arrayImages;
      response.checkdelete = null;
      arrayImages.push(response);

      var arrayVenuImages = this.state.arrayVenuImages;

      arrayVenuImages.append("formArray", {
        uri: response.uri,
        type: response.type,
        name: response.fileName,
      });
      this.setState({ arrayVenuImages: arrayVenuImages });
      // this.props.venueImages({
      //   formimagearray: arrayVenuImages,
      //   localimages: arrayImages,
      // });

      // You can also display the image using data:
      // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      this.setState({ arrayImages });
    }
  };
  loadGalleryCamera = async (data) => {
    // alert(response);
    if (data == 0) {
      var response = await requestCameraPermission("gallery");
    }
    if (data == 1) {
      var response = await requestCameraPermission("camera");
    }
    if (response == true) {
      if (data == 0) {
        //loadGallery
        ImagePicker.launchImageLibrary(options, (response) => {
          // Same code as in above section!

          this.responseCamerGallery(response);
        });
      } else if (data == 1) {
        //loadCamera
        ImagePicker.launchCamera(options, (response) => {
          // Same code as in above section!
          this.responseCamerGallery(response);
        });
      } else {
        //cancel code
      }
    }
  };

  render() {
    // alert(JSON.stringify(this.state.facilityData));
    var count =
      this.state.arrayImages.length > 0 &&
      this.state.arrayImages.filter((obj) => obj.checkdelete == true);
    return (
      <Content>
        <View style={{ borderBottomWidth: 0.5, borderColor: color.black1 }}>
          <View style={{ flexDirection: "row", padding: 10, paddingLeft: 15 }}>
            <View style={{ flex: 0.4 }}>
              <Text style={{ fontSize: hp("2%") }}>Category</Text>
            </View>
            <View style={{ flex: 0.6 }}>
              <Text style={{ fontWeight: "bold", fontSize: hp("2%") }}>
                {this.props.venuetypedata &&
                  this.props.venuetypedata.CategoryName}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", padding: 10, paddingLeft: 15 }}>
            <View style={{ flex: 0.4 }}>
              <Text style={{ fontSize: hp("2%") }}>Specification</Text>
            </View>
            <View style={{ flex: 0.6 }}>
              <Text style={{ fontWeight: "bold", fontSize: hp("2%") }}>
                {this.props.facilityData &&
                  this.props.facilityData.SpecificName}
              </Text>
            </View>
          </View>
        </View>

        <AddressComp
          receiveAddress={this.receiveAddress}
          modalstate={this.state.modalstate}
        />

        <DynamicForm
          changeText={this.changeText}
          commonArray={this.state.facilityData ? this.state.commonArray : []}
          facilityData={
            this.state.facilityData ? this.state.facilityData.specDetails : []
          }
        />

        <View>
          {count.length > 0 && (
            <TouchableOpacity
              onPress={() => this.delteSelected()}
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "transparent",
              }}
            >
              <Icon
                style={{ color: "black" }}
                name="trash-o"
                type="FontAwesome"
              />
              <Text style={{ color: "black", paddingLeft: 6 }}>
                Delete {count.length} Items
              </Text>
            </TouchableOpacity>
          )}
        </View>

        <View style={styles.imagecontainer}>
          {this.state.arrayImages.map((obj, key) => {
            return (
              <View key={key} style={styles.imageView}>
                <TouchableOpacity
                  onPress={() => this.loadDelete(key, null, !obj.exist)}
                  style={styles.venueimage}
                  onLongPress={() => this.loadDelete(key, "long")}
                >
                  {obj.checkdelete == true && (
                    <View
                      style={[
                        styles.imageView,
                        {
                          alignItems: "flex-start",
                          zIndex: 9999999,
                          top: 0,
                          right: 0,
                          backgroundColor: "rgba(255,255,255,0.5)",
                        },
                      ]}
                    >
                      <Icon
                        type="FontAwesome"
                        name="check-square"
                        style={{ color: "green" }}
                      />
                    </View>
                  )}
                  <Image
                    style={styles.venueimage}
                    source={{
                      uri: !obj.exist
                        ? "data:image/jpeg;base64," + obj.data
                        : obj.url,
                    }}
                  />
                </TouchableOpacity>
              </View>
            );
          })}
          {(this.state.arrayImages.length == 2 ||
            this.state.arrayImages.length % 3 == 2) && (
            <View style={styles.imageView}></View>
          )}
        </View>


           {this.state.arrayImages.length == 0 && (

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            paddingVertical: hp("2%"),
          }}
        >
          <View>
            <TouchableOpacity onPress={() => this.modalState(true)}>
              <Image
                source={PhotoInfo}
                style={{ width: hp("3.3%"), height: hp("3.3%") }}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginLeft: 12,
              marginRight: 12,
              marginBottom: 7,
            }}
          >
            {this.state.arrayImages.length == 0 && (
              <Text style={{ fontSize: hp("2.5%") }}>Upload Photos</Text>
            )}
            {!this.state.arrayImages.length == 0 && (
              <Text style={{ fontSize: hp("2.5%") }}>Upload More Photos</Text>
            )}
          </View>
          <View>
            <TouchableOpacity onPress={() => this.loadPhotos()}>
              <Image
                source={PhotoPlus}
                style={{ width: hp("4.5%"), height: hp("4.5%") }}
              />
            </TouchableOpacity>
          </View>
        </View> 

        )}

        <ModalComp
          closemodal={() => this.closeimage()}
          center={true}
          visible={this.state.imagevisible}
          header={false}
          modaltitle={"How"}
          modalsubtitle={"Course"}
        >
          <View style={{ flex: 1 }}>
            <Image
              style={styles.venueimage}
              style={{
                width: wp("100%"),
                height: hp("50%"),
              }}
              source={{ uri: this.state.selectedImage }}
            />
          </View>
        </ModalComp>

        <View style={{ padding: 20, marginTop: 18 }}>
          {/*
            <LabelTextbox error={this.state.validations.roomname.error} errormsg={this.state.validations.roomname.errormsg} capitalize={true} changeText={(data)=>this.changeText(data,'roomname','trn_venue_room_name')} value={this.state.facilityData.roomname} labelname="Room Name"/>
            <View style={{flex:1,flexDirection:'row',height:hp('6%'),marginBottom:25,}}>
                <View style={{flex:0.3,justifyContent:'center'}}>
                    <Label>Seats</Label>
                </View>
                <View style={{flex:0.7,flexDirection:'row',justifyContent:'space-between'}}>
                <View style={{flex:0.25}}> 
                <Item  regular style={styles.itemsize}>
            <Input  keyboardType="number-pad" onChangeText={(data)=>this.changeText(data,'seats','trn_venue_room_seats')} value={this.state.facilityData.seats} style={{height:'100%',padding:0}}/>
            {this.state.validations.seats.error&&
            <Text style={{position:'absolute',bottom:-hp('2.5%'),fontSize:hp('1.5%'),color:color.red}}>{this.state.validations.seats.errormsg}</Text>
            }
            </Item>
                </View>
                <View style={{flex:0.65,flexDirection:'row'}}>
                <View style={{flex:1}}>
                <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'floor','trn_venue_room_floor')} value={this.state.facilityData.floor} labelname="Floor"/>

                </View>
                </View>
            </View>
            </View>
            <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'address','trn_venue_room_address')} value={this.state.facilityData.address} type='textarea'  placeholder="Address" rowspan={3} labelname="Address"/>
            <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'landmark','trn_venue_room_landmark')} value={this.state.facilityData.landmark} labelname="LandMark">
            <TouchableOpacity onPress={this.getAddress} style={{paddingLeft:12,paddingRight:12}}><Text style={{textAlign:'right',color:color.blueactive,fontSize:hp('2.1%')}}>Location Map</Text></TouchableOpacity>
            </LabelTextbox>
            <LabelTextbox  error={this.state.validations.mobile.error} errormsg={this.state.validations.mobile.errormsg} capitalize={true} changeText={(data)=>this.changeText(data,'mobile','trn_venue_room_mob')} value={this.state.facilityData.mobile} labelname="Mobile"/>
            <LabelTextbox capitalize={true} error={this.state.validations.mail.error} errormsg={this.state.validations.mail.errormsg} changeText={(data)=>this.changeText(data,'mail','trn_venue_room_mail')} value={this.state.facilityData.mail} labelname="Mail"/>
        */}

          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: 14,
            }}
          >
            <Next
              onClick={() => this.onSubmit()}
              name="Submit"
              _width={wp("40%")}
            />
          </View>
        </View>  



         <ModalComp
                         titlecolor={color.orange}
                         visible={this.state.visible}
                         closemodal={(data) => this.modalState(data)}
                         header={false}
                         center={true}
                       >
                         <Content>
                           <Root>
                             <View
                               style={{ paddingLeft: 20, paddingRight: 15 }}
                             >
                               <View
                                 style={{
                                   flex: 1,
                                   justifyContent: "flex-start",
                                   alignItems: "center",
                                   backgroundColor: color.white,
                                 }}
                               >
                                 <Image
                                   source={Info}
                                   style={{
                                     width: hp("10%"),
                                     height: hp("10%"),
                                     textAlign: "center",
                                   }}
                                 />
                                 <Text
                                   style={{
                                     color: color.orange,
                                     fontSize: hp("3.5%"),
                                     textAlign: "center",
                                     fontWeight: "bold",
                                   }}
                                 >
                                   Upload Instructions
                                 </Text>
                                 <View
                                   style={{
                                     display: "flex",
                                     justifyContent: "flex-start",
                                     alignItems: "stretch",
                                     paddingVertical: hp("2%"),
                                   }}
                                 >
                                   <View style={{ flexDirection: "row" }}>
                                     <Text
                                       style={{
                                         color: color.orange,
                                       }}
                                     >
                                       {"\u2022"}
                                     </Text>
                                     <Text
                                       style={{
                                         paddingLeft: 5,
                                         color: color.black1,
                                       }}
                                     >
                                       Please Upload Image in JPG or PNG format
                                     </Text>
                                   </View>
                                   <View style={{ flexDirection: "row" }}>
                                     <Text
                                       style={{
                                         color: color.orange,
                                       }}
                                     >
                                       {"\u2022"}
                                     </Text>
                                     <Text
                                       style={{
                                         paddingLeft: 5,
                                         color: color.black1,
                                       }}
                                     >
                                       For Image, Image size should be 1080px by
                                       566px
                                     </Text>
                                   </View>

                                   <View
                                     style={{
                                       flexDirection: "row",
                                       justifyContent: "flex-start",
                                       flexWrap: "wrap",
                                     }}
                                   >
                                     <Text
                                       style={{
                                         color: color.orange,
                                       }}
                                     >
                                       {"\u2022"}
                                     </Text>
                                     <Text
                                       style={{
                                         paddingLeft: 5,
                                         color: color.black1,
                                       }}
                                     >
                                       For Image size should be less than
                                       10 MB
                                     </Text>
                                   </View>
                                 </View>
                               </View>
                             </View>
                           </Root>
                         </Content>
                       </ModalComp>


      </Content>
    );
  }
  componentDidMount() {
    //alert(JSON.stringify(this.props.facilityData));
    if (this.props.facilityData) {
      this.setState({ facilityData: this.props.facilityData }, () => {
        this.getSubCategory();
      });
    }
    if (this.props.commonArray) {
      this.setState({ commonArray: this.props.commonArray });
    }
  }
}

const styles = StyleSheet.create({
  itemsize: {
    height: hp("5.5%"),
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue,
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%"),
  },
  pickerBorder: {
    borderColor: "#d6d6d6",
    borderWidth: 1,
    borderStyle: "solid",
    marginVertical: hp("0.5%"),
  },
  imagecontainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  imageView: {
    width: wp("100%"),
    height: hp("17%"),
    borderRadius: 7,
    marginBottom: 15,
    paddingHorizontal: wp("3%"),
  },
  venueimage: {
    position: "absolute",
    width: "100%",
    height: "100%",
    borderRadius: 7,
    paddingHorizontal: wp("2%"),
    marginHorizontal: wp("2.5%"),
  },
});

export default TraineeDetails;
