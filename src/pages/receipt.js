import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import color from "../Helpers/color";
import { Actions} from "react-native-router-flux";
import {
  Accordion,
  Container,
  Content,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Footer,
  FooterTab
} from "native-base";
import LoadingOverlay from '../components/LoadingOverlay'
import dateFormat from 'dateformat';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import Toast from 'react-native-simple-toast';
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import PromoCode from './promocode';
import StarRating from 'react-native-star-rating';  
import Collapse from '../components/collapse';
const jsCoreDateCreator = (dateString) => { 
  // dateString *HAS* to be in this format "YYYY-MM-DD HH:MM:SS"
  let dateParam = dateString.split(/[\s-:]/)  
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString()  
  // alert(dateParam);
  return new Date(...dateParam)  
}
const NameStar = () => (
  <React.Fragment>
    <Text>Name {""}</Text> <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
);

var formkeys = [
  {
    name: "name",
    type: "default",
    labeltype: "text1",
    label: <NameStar />
  },
  {
    name: "mobile_no",
    type: "default",
    labeltype: "text1",
    label: "Mobile No"
  },
  {
    name: "address",
    type: "default",
    labeltype: "textarea1",
    label: "address"
  },
  {
    name: "more_info",
    type: "default",
    labeltype: "textarea1",
    label: "More Info"
  }
];  
var bookingData={
  user_id:'',
  venue_id:'',
  bookingFrom:dateFormat(new Date(),'yyyy-mm-dd'),
  bookingTo:dateFormat(new Date(),'yyyy-mm-dd'),
  promodId:0,
  promoType:0,
  promoValue:0,
  trn_venue_price_amt:0,
  promoAmount:0,
  finalPrice:0,
  bookingCapacity:0,
  slots:[]
}
const splitData = [
  {
    split_name: "Mornign Event",
    value: [1, 2, 3, 4, 6, 7, 7, 83]
  },
  {
    split_name: "Evening Event",
    value: [5, 6, 7, 8, 3, 6, 6, 3, 4]
  },
  {
    split_name: "Afternoon Event",
    value: [9, 102, 13, 14]
  }
];


const excludeDays=[{label:'Sunday',value:0},{label:'Monday',value:1},{label:'Tuesday',value:2},{label:'Wednesday',value:3},{label:'Thursday',value:4},{label:'Friday',value:5},{label:'Saturday',value:6}]

export default class Receipt extends Component {
                 constructor(props) {
                   super(props);
                   this.setSchedule = this.setSchedule.bind(this);
                   this.setActiveSplit = this.setActiveSplit.bind(this);

                   this.editClicked = this.editClicked.bind(this);
                   this.deleteClicked = this.deleteClicked.bind(this);
                   this.checkAvailabilities = this.checkAvailabilities.bind(
                     this
                   );

                   this.state = {
                     checkAvailabilities: [],
                     totalSlots:0,
                     sutTotal:0,
                     isPromo: false,
                     selected: null,
                     isSplitPopUp: false,
                     splitlist: "",
                     starCount: 3.5,
                     name: "",
                     mobile_no: "",
                     address: "",
                     more_info: "",
                     rangeCalendar:false,
                     collapse: [
                       {
                         id: 1,
                         title: "Your Booking Availabilities",
                         content: "Lorem ipsum dolor sit amet",
                         icon: "th",
                         headerColor: "#f1f1f1"
                       },
                      
                     ],
                     collapse1: [
                        {
                         id: 2,
                         title: "Basic Details",
                         content: "tse",
                         icon: "user-circle-o",
                         headerColor: "#fff"
                       }
                      
                     ],
                     validations: {
                       name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       },
                       mobile_no: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "mobile", status: false }]
                       },
                       address: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: []
                       },
                       more_info: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: []
                       }
                     }
                   };
                 }
responseCallBack=(data,status)=>{
  // alert(data);
  if(status==2){
this.setState({loadingoverlay:true});
    return;
  }
  if(status==1){
 Toast.show(data, Toast.LONG);
  Actions.reset('home');
  }else if(status==2){
 Toast.show(data, Toast.LONG);
  // Actions.reset('home');
  Actions.pop();
  }else{
  // this.setState({loadingoverlay:false});
 // Toast.show(data, Toast.LONG);
 Actions.pop();
  }
}
                 submitBooking=()=>{
                   // bookingData.
                       AsyncStorage.getItem('loginDetails', (err, result) => {
      
       if(result!=null){
         var resultData=JSON.parse(result);
         // alert(result)
         bookingData.user_id=resultData.user_id;
         bookingData.venue_id=this.props.venueId,
         bookingData.trn_venue_price_amt=this.props.data&&this.props.data.price.length>0&&this.props.data.price[0].trn_venue_price_amt;
         bookingData.finalPrice=this.subTotal();
         // alert(JSON.stringify(bookingData));
         Actions.stripecheckout({bookingData:this.props.data,bookdetails:bookingData,priceamount:bookingData.finalPrice,responseCallBack:this.responseCallBack});
       }else{
         // Toast.show('User Not')
       }
     })
                 }
                 renderExcludeDays = arrayData => {
                   if (arrayData) {
                     var getData = arrayData.map(obj => {
                       var findindex = excludeDays.findIndex(
                         obj2 => obj2.value == obj
                       );
                       if (findindex != -1) {
                         return excludeDays[findindex].label;
                       }
                     });
                     return getData.join(",");
                   } else {
                     return "0";
                   }
                 };

                 pushAvailabilities = data => {
                   let checkAvailabilities = this.state.checkAvailabilities;
                   // alert(JSON.stringify(data));
                     var findindex=checkAvailabilities.findIndex((obj)=>obj.selectedDate==data.selectedDate)
                     if(findindex==-1){
                   checkAvailabilities = [...checkAvailabilities, data];
                     }else{
                       if(data.selectedSlots.length==0){
                         checkAvailabilities.splice(findindex,1);
                       }else{
                         checkAvailabilities[findindex].selectedSlots=data.selectedSlots;
                       }
                     }
                   this.setState({
                     checkAvailabilities
                   });
                   var hoursArray=[];
    var newArrayofSlots=checkAvailabilities.map((obj)=>{
      obj.selectedSlots.map((obj2)=>{
        var objdata={fromdate:obj.selectedDate,todate:obj.selectedDate,slotFromTime:obj2.venue_slot_start_time,slotToTime:obj2.venue_slot_end_time}
        hoursArray.push(objdata)
      })
    })
    bookingData.slots=hoursArray;

                   console.log("check avaialatilti", checkAvailabilities);
                 };

                 changeText = (data, key) => {
                   // alert(data);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                   if (key == "dob") {
                     if (data == "errordob") {
                       validations[key].error = true;
                       validations[key].errormsg =
                         "Invalid Date Accept Format (dd-mm-yyyy)";
                     } else {
                       validations[key].error = false;
                     }
                   } else if (key == "mobile") {
                     errormsg = ValidationLibrary.checkValidation(
                       data.mobile,
                       this.state.validations[key].validations
                     );
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   } else {
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   }
                   this.setState({ validations });
                   this.setState({
                     [key]: data
                   });
                 };


               
                 verify = () => {
                   // alert(JSON.stringify(this.state));
                   var obj = this.state;
                   obj.type = "otpscreen";
                   var result = Object.keys(obj.validations).filter(
                     (object, key) => {
                       if (object == "dob") {
                         if (obj.validations[object].error == null) {
                           this.changeText("errordob", object);
                         }
                       } else if (object == "mobile") {
                         this.changeText(this.state[object], object);
                       } else {
                         this.changeText(this.state[object], object);
                       }
                       return (
                         obj.validations[object].mandatory == true &&
                         obj.validations[object].error == true
                       );
                     }
                   );
                   // alert(result.length)
                 };

                 onStarRatingPress(rating) {
                   this.setState({
                     starCount: rating
                   });
                 }
                 receiveDates=(data)=>{
                   // rangecalendar=true;
                   // alert(rangecalendar);

                   // return;
                   var checkAvailabilities=this.state.checkAvailabilities;
                   checkAvailabilities=[data];
                   var datesArray=data.dates;
                   var hoursArray=[];
                   var newArrayofSlots=datesArray.map((obj)=>{
        var objdata={fromdate:dateFormat(new Date(obj),'yyyy-mm-dd'),todate:dateFormat(new Date(obj),'yyyy-mm-dd'),"slotFromTime":"00:00:00",
      "slotToTime":"00:00:00"}
        hoursArray.push(objdata)
    })
    bookingData.slots=hoursArray;
                   // alert(JSON.stringify(checkAvailabilities));
                   this.setState({checkAvailabilities});
                 }
                 checkAvailabilities() {
                   const { availabilityType, venueType } = this.props;
                   var result = this.generateVenueType(
                     venueType,
                     availabilityType
                   );

                   if (result == "Hour") {
                     // rangecalendar='false';
                     this.setState({rangeCalendar:false})
                     Actions.WeekAvailability({
                       venueId: this.props.venueId,
                       venueType: this.props.venueType,
                       availabilityType: this.props.availabilityType,
                       pushData: this.pushAvailabilities,
                       oldWeekData:this.state.checkAvailabilities
                     });
                   } else if (

                     result == "Day" ||
                     result == "Week" ||
                     result == "Month"
                   ) {
                     this.setState({rangeCalendar:true});
                     var minDate=dateFormat(jsCoreDateCreator(this.props.data.availability[0].trn_venue_avail_frm),'yyyy-mm-dd');
                     var maxDate=dateFormat(jsCoreDateCreator(this.props.data.availability[0].trn_venue_avail_to),'yyyy-mm-dd')
                     // alert(maxDate);
                     var currentDate=dateFormat(new Date(),'yyyy-mm-dd')
                     Actions.MonthAvailability({
                       venueId: this.props.venueId,
                       minDate:minDate<currentDate?new Date(currentDate):new Date(minDate),
                       maxDate:new Date(maxDate),
                       venueType: this.props.venueType,
                       availabilityType: this.props.availabilityType,
                       receiveDates:this.receiveDates
                     });
                   } else {
                     Toast.show('For Booking Pax & Seats will be available soon.', Toast.LONG);
                   }
                 } 



                 subTotal =()=>{
                   return this.props.data &&
                     this.props.data.price.length>0&&this.props.data.price[0].trn_venue_price_amt *
                       this.getTotalSlots();
                 }

                 generateVenueType = (type, availtype) => {
                   if (type == 2) {
                     return "Pax";
                   } else if (type == 3) {
                     return "Seats";
                   } else if ((!type || type == 1) && availtype == 1) {
                     return "Hour";
                   } else if ((!type || type == 1) && availtype == 2) {
                     return "Day";
                   } else if ((!type || type == 1) && availtype == 3) {
                     return "Week";
                   } else {
                     return "Month";
                   }
                 };

                 componentWillMount = () => {
                   //get list of splits
                   //end
                 };

                 componentWillReceiveProps = nextProps => {
                   console.log(nextProps);
                   // alert("ok");
                 };

                 setSchedule = data => {
                   console.log("data", data);
                 };
                 onEdit;
                 setActiveSplit = () => {
                   isSplitPopUp = this.state.isSplitPopUp;
                   isSplitPopUp = true;
                   this.setState({ isSplitPopUp });
                 };

                 editClicked = data => {
                   console.log("edit pressed", data);
                 };

                 deleteClicked = data => {
                   console.log("delete pressed ", data);
                 };

                 openPromoPopup = () => {
                   this.setState({
                     isPromo: true
                   });
                 };
                 cancelPromoPopup = () => {
                   this.setState({
                     isPromo: false
                   });
                 }; 


                 getTotalSlots = () => {
                   //update slots count state
                   var slotsCount = 0;
                   if(this.state.rangeCalendar==false){
                   for (const key in this.state.checkAvailabilities) {
                     if (this.state.checkAvailabilities.hasOwnProperty(key)) {
                       slotsCount += this.state.checkAvailabilities[key]
                         .selectedSlots.length;
                     }
                   } 
                 }else{
                   slotsCount=this.state.checkAvailabilities.length>0?this.state.checkAvailabilities[0].quantity:0
                 }

                  //  console.log(slotsCount);
                  //  var subTotal= this.state.subTotal*400;
                  //  this.setState({subTotal})
                  
                   return slotsCount;
                  //  this.setState({ totalSlots: slotsCount });
                   
                 };
                 

                 _renderHeader(item, expanded) { 
                   
                  if(item.id==1)
                  {
                    var hidden ={display:'none'}
                  }else
                  var hidden ={display:'visible'};
                  
                   return (
                     <View
                       style={
                         {
                           flexDirection: "row",
                           padding: 20,
                           justifyContent: "flex-start",
                           alignItems: "center",
                           backgroundColor: "#fff"
                         }
                       }
                     >
                       <TouchableOpacity>
                         <Icon
                           active
                           name={item.icon}
                           style={{ fontSize: hp('3%') }}
                           type="FontAwesome"
                         />
                       </TouchableOpacity>
                       <View>
                         <Text
                           style={{
                             textAlign: "left",
                             paddingLeft: 8,
                             fontSize: hp('2.5%')
                           }}
                         >
                           {item.title}
                         </Text>
                       </View>
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "flex-end",
                           alignItems: "flex-end",
                           right: 0
                         }}
                       >
                         {expanded ? (
                           <Icon
                             style={{ fontSize: hp('2.5%') }}
                             name="angle-down"
                             type="FontAwesome"
                           />
                         ) : (
                           <Icon
                             style={{ fontSize: hp('2.5%') }}
                             name="angle-right"
                             type="FontAwesome"
                           />
                         )}
                       </View>
                     </View>
                   );
                 }

                 _renderContent = item => { 
                   // alert(rangecalendar);
                   console.log("rangingcalendar",this.state.rangeCalendar);
                  var count=0;
                   
                   if (item.id == 2) {
                     return (
                       <React.Fragment>{this._renderForm()}</React.Fragment>
                     );
                   } else { 
                     if(this.getTotalSlots()<=0){

                     return (<Text style={{padding:12,fontSize:16,textAlign:'center'}}>No Availability Chosen..</Text>);
                     } 

                     return (
                       <React.Fragment
                         style={{
                           paddingHorizontal: 20,
                           backgroundColor: item.headerColor
                         }}
                       >
                         <List>
                           {this.state.rangeCalendar==false&&this.state.checkAvailabilities.map((itemobj, i) => { 

                               return (
                                 <ListItem>
                                   <Left>
                                     <Text>{itemobj.selectedDate} - </Text>
                                     <Text style={{ color: color.orange }}>
                                       {itemobj.selectedSlots.length} Slots{" "}
                                     </Text>
                                   </Left>
                                   <Right></Right>
                                 </ListItem>
                               );
                           })}
                           {this.state.rangeCalendar==true&&this.state.checkAvailabilities.map((item2, i) => { 

                               return (
                                 <ListItem>
                                   <Left>
                                     <Text>{item2.from} - </Text>
                                     <Text>
                                       {item2.to}
                                     </Text>
                                   </Left>
                                   <Right></Right>
                                 </ListItem>
                               );
                           })}
                         </List>
                       </React.Fragment>
                     );
                   }
                 };

                 _renderForm() {
                   return (
                     <View
                       style={{
                         paddingTop: 20,
                         paddingRight: 20,
                         paddingLeft: 20,
                         paddingBottom: 20,
                         backgroundColor: "white"
                       }}
                     >
                       {formkeys.map((obj, key) => {
                         return (
                           <View style={{marginBottom:15}}>
                           <LabelTextbox
                             inputtype={obj.type}
                             key={key}
                             type={obj.labeltype}
                             capitalize={obj.name != "dob"}
                             error={this.state.validations[obj.name].error}
                             errormsg={
                               this.state.validations[obj.name].errormsg
                             }
                             changeText={data =>
                               this.changeText(data, obj.name)
                             }
                             value={this.state[obj.name]}
                             labelname={obj.label}
                           />
                           </View>
                         );
                       })}
                     </View>
                   );
                 }

                 render() {
                     const { availabilityType, venueType } = this.props;
                   
                   return (
                     <Container>
                       <Content style={styles.head}>
                       {this.state.loadingoverlay==true&&
                       <LoadingOverlay/>
                     }
                         <List style={{ backgroundColor: "white",paddingTop:6 }}>
                           <ListItem
                             icon
                             noItent
                             style={{ paddingVertical: 10 }}
                           >
                             <Left>
                               <Icon
                                 name="building-o"
                                 type="FontAwesome"
                                 style={{ fontSize: hp('3%') }}
                               />
                             </Left>

                             <Body>
                               <Text style={{ fontSize: hp('2.5%') }} numberOfLines={1}>
                                 {this.props.data.trn_venue_name}{" "}
                               </Text>
                               <Text
                                 note
                                 numberOfLines={1}
                                 style={{ color: color.blue,marginTop:5,paddingBottom:5,marginBottom:5 }}
                               >
                                 Cancellation Policy
                               </Text>
                             </Body>
                             <Right>
                               <Text
                                 style={{
                                   fontSize: 18,
                                   justifyContent: "center",
                                   alignItems: "center"
                                 }}
                               >
                                 <Icon
                                   type="FontAwesome"
                                   name="inr"
                                   style={{
                                     color: color.blue,
                                     fontSize: hp('2%')
                                   }}
                                 />{" "}
                                 <Text style={{fontSize:hp('2%')}}>{this.props.data.price[0].trn_venue_price_amt}</Text>
                               </Text>
                               <Text note></Text>
                             </Right>
                           </ListItem>
                         </List>
                         <View
                           style={{
                             flex: 1,
                             justifyContent: "center",
                             alignItems: "center",
                             backgroundColor: "white",
                             paddingHorizontal: 5,
                             paddingVertical: 5
                           }}
                         >
                           <View>
                             <Button
                               iconLeft
                               primary
                               style={{ paddingHorizontal: 10 }}
                               onPress={() => this.checkAvailabilities()}
                             >
                               <Icon
                                 name="calendar-check-o"
                                 type="FontAwesome"
                               />
                               <Text style={{ color: "white" }}>
                                 Check Availability
                               </Text>
                             </Button>
                           </View>

                           <View style={{ paddingTop: 3,alignItems:"center" }}>
                             <Text note>
                               <Icon
                                 name="info-circle"
                                 type="FontAwesome"
                                 style={{ color: "#f1f1f1", fontSize: hp('3%') }}
                               />{" "}
                               <Text style={{fontSize:hp('2.1%') }}>{""} Exclude Days{" "}</Text>
                               <Text style={{ color: color.orange,fontSize:hp('2.1%') }}>
                                 {this.renderExcludeDays(
                                   this.props.data.availability[0].trn_venue_exclude_days?this.props.data.availability[0].trn_venue_exclude_days.split(
                                     ","
                                   ):''
                                 )}
                               </Text>
                             </Text>
                           </View>
                         </View>

                         <View
                           style={{ height: 8, backgroundColor: "#edeef2" }}
                         />

                         <View>
                           <Accordion
                             dataArray={this.state.collapse}
                             animation={true}
                             expanded={0}
                             renderHeader={this._renderHeader}
                             renderContent={this._renderContent}
                             contentStyle={{ backgroundColor: "#edeef2" }}
                           />
                         </View>
                         <View>
                           <Accordion
                             dataArray={this.state.collapse1}
                             animation={true}
                             expanded={0}
                             renderHeader={this._renderHeader}
                             renderContent={this._renderContent}
                             contentStyle={{ backgroundColor: "#edeef2" }}
                           />
                         </View>

                         <View
                           style={{ height: 8, backgroundColor: "#edeef2" }}
                         />
                         <View>
                           <PromoCode isPromoShow={this.state.isPromo} />
                         </View>

                         <View
                           style={{ height: 8, backgroundColor: "#edeef2" }}
                         />

                         {this.getTotalSlots() > 0 && (
                           <View style={{ backgroundColor: "white" }}>
                             <List>
                               <ListItem NoIndent>
                                 <Text style={{ fontSize: 18 }}>
                                   <Icon
                                     type="FontAwesome"
                                     name="inr"
                                     style={{
                                       color: "#000",
                                       fontSize: 22,
                                       paddingTop: 3
                                     }}
                                   />{" "}
                                   Bill Details
                                 </Text>
                               </ListItem>
                               <ListItem>
                                 <Left>
                                   <Text>
                                     Per {this.generateVenueType(venueType,availabilityType)}
                                     {this.props.data &&
                                       this.props.data.price[0]
                                         .trn_venue_price_amt}{" "}
                                     x {this.getTotalSlots()}
                                   </Text>
                                 </Left>
                                 <Right>
                                   <Text>
                                     {" "}
                                     <Icon
                                       name="inr"
                                       type="FontAwesome"
                                       style={{ color: "#ddd", fontSize: 14 }}
                                     />{" "}
                                     {this.subTotal()}
                                   </Text>
                                 </Right>
                               </ListItem>

                               <ListItem itemHeader>
                                 <Left>
                                   <Text
                                     style={{
                                       fontSize: 18,
                                       fontWeight: "bold"
                                     }}
                                   >
                                     Total Pay
                                   </Text>
                                 </Left>

                                 <Right>
                                   <Text>
                                     {" "}
                                     -{" "}
                                     <Icon
                                       name="inr"
                                       type="FontAwesome"
                                       style={{
                                         color: "#000",
                                         fontSize: 14,
                                         fontWeight: "bold"
                                       }}
                                     />{" "}
                                     {this.subTotal()}
                                   </Text>
                                 </Right>
                               </ListItem>
                             </List>
                           </View>
                         )}
                       </Content>

                       {this.getTotalSlots() > 0 && (
                         <Footer>
                           <FooterTab>
                             <Button full light>
                               <Text style={{ color: "#eb5b00" }}>
                                 <Icon
                                   type="FontAwesome"
                                   name="inr"
                                   style={{
                                     color: "#eb5b00",
                                     fontSize: 13,
                                     paddingTop: 3
                                   }}
                                 />{" "}
                                 {this.subTotal()}
                               </Text>
                             </Button>
                             <Button full primary onPress={()=>this.submitBooking()}>
                               <Text color="white" style={{ color: "white" }}>
                                 PROCEED TO PAY
                               </Text>
                             </Button>
                           </FooterTab>
                         </Footer>
                       )}
                     </Container>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#edeef2",
    display: "flex",
    justifyContent: "flex-start"
  },
  head: {
    backgroundColor: "#edeef2",

  },
  tab: {
    backgroundColor: "#edeef2"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  }
});
