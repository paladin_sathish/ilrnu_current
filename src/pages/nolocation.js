'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,Text
} from 'react-native';
import geolocation from '../images/geolocation.png';
import {Button} from 'native-base';
import color from '../Helpers/color';


import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


class Nolocation extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {};
	}
	getlocationAccess=()=>{
		this.props.requestAccess&&this.props.requestAccess();
	}
  render() {
    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
      <View style={{flexDiretion:'row',justifyContent:'center',flex:1,alignItems:'center',padding:25}}>
      <Image source={geolocation} style={styles.imageView}/>
      <Text style={styles.header}>Location Permission Required</Text>
      <Text style={styles.subheader}>Allow IVNEU to detect your location automatically for get your venues near by.</Text>
      </View>
      <Button onPress={()=>this.getlocationAccess()} style={styles.allow_btn} block><Text style={styles.allowtext}>Allow</Text></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
imageView:{
	width:hp('20%'),
	height:hp('20%')
},
header:{
	fontSize:hp('3%'),
	fontWeight:'bold'
},
subheader:{
	fontSize:hp('2%'),
	textAlign:'center'
},
allow_btn:{
	backgroundColor:color.purple
},
allowtext:{
	color:color.white,
	fontSize:hp('2.5%')
}
});


export default Nolocation;