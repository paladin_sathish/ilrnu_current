'use strict';

import React, { Component } from 'react';

import { Platform, StyleSheet, Text, View, Alert, StatusBar, Modal, Image, TouchableHighlight, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Icon, Button, Subtitle, Header, Content, Body, Right, Title, Item, Input, Label, Textarea, Form } from 'native-base';
import ValidationLibrary from '../Helpers/validationfunction';
import DynamicForm from '../components/DynamicForm';
import color from '../Helpers/color';
import LabelTextbox from '../components/labeltextbox';
import AddressComp from '../components/addressComp';
class FacilityDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
          commonArray: [
            {
              spec_det_id: "idname",
              venue_spec_id: 1,
              spec_det_name: "Venue Name",
              spec_det_sortorder: 0,
              spec_det_datatype1: "text",
              spec_det_datavalue1: "",
              spec_det_datatype2: "",
              spec_det_datavalue2: "",
              spec_det_datatype3: "",
              spec_det_datavalue3: "",
              validation: [
                { name: "required" },
                { name: "minLength", params: 30 }
              ],
              error: null,
              errormsg: ""
            },
            {
              spec_det_id: "idname2",
              venue_spec_id: 1,
              spec_det_name: "Venue Address",
              spec_det_sortorder: 0,
              spec_det_datatype1: "textareamap",
              spec_det_datavalue1: "",
              spec_det_datatype2: "",
              spec_det_datavalue2: "",
              spec_det_datatype3: "",
              spec_det_datavalue3: "",
              validation: [{ name: "required" }],
              error: null,
              errormsg: ""
            },
            {
              spec_det_id: "idname3",
              venue_spec_id: 1,
              spec_det_name: "Venue Area",
              spec_det_sortorder: 0,
              spec_det_datatype1: "text",
              spec_det_datavalue1: "",
              spec_det_datatype2: "",
              spec_det_datavalue2: "",
              spec_det_datatype3: "",
              spec_det_datavalue3: "",
              validation: [
                { name: "required" },
                { name: "minLength", params: 30 }
              ],
              error: null,
              errormsg: ""
            },
            {
              spec_det_id: "idname4",
              venue_spec_id: 1,
              spec_det_name: "Venue Landmark",
              spec_det_sortorder: 0,
              spec_det_datatype1: "text",
              spec_det_datavalue1: "",
              spec_det_datatype2: "",
              spec_det_datavalue2: "",
              spec_det_datatype3: "",
              spec_det_datavalue3: "",
              validation: [
                { name: "required" },
                { name: "minLength", params: 300 }
              ],
              error: null,
              errormsg: ""
            },
            {
              spec_det_id: "idname5",
              venue_spec_id: 1,
              spec_det_name: "Venue Description",
              spec_det_sortorder: 0,
              spec_det_datatype1: "textarea",
              spec_det_datavalue1: "",
              spec_det_datatype2: "",
              spec_det_datavalue2: "",
              spec_det_datatype3: "",
              spec_det_datavalue3: "",
              validation: [
                { name: "required" },
                { name: "minLength", params: 1000 }
              ],
              error: null,
              errormsg: ""
            }
          ],
          facilityData: null,
          addresscomp: "",
          modalstate: false,
          init:true,
          validations: {
            roomname: {
              error: null,
              mandatory: true,
              errormsg: "",
              validations: [{ name: "required", status: false }]
            },
            seats: {
              error: null,
              errormsg: "",
              mandatory: true,
              validations: [{ name: "required", status: false }]
            },
            floor: {
              error: null,
              errormsg: "",
              mandatory: false,
              validations: []
            },
            landmark: {
              error: null,
              errormsg: "",
              mandatory: false,
              validations: []
            },
            mobile: {
              error: null,
              errormsg: "",
              mandatory: true,
              validations: [
                { name: "required", status: false },
                { name: "mobile", status: false }
              ]
            },
            mail: {
              error: null,
              errormsg: "",
              mandatory: true,
              validations: [
                { name: "required", status: false },
                { name: "email", status: false }
              ]
            },
            address: {
              error: null,
              errormsg: "",
              mandatory: true,
              validations: [{ name: "required", status: false }]
            }
          }
        };
    }

    componentWillMount() {
        this.props.headertext({headertext:<Text style={{fontSize:hp('1.7%'),fontWeight:'bold',color:color.blue}}>Venue Details</Text>});
        this.props.labeltext({ labeltext: <Text>Please add your <Text style={{color:color.orange,fontWeight:'bold'}}>Venue </Text> Details</Text> });
    }
    componentWillReceiveProps(props) {
        // alert(JSON.stringify(props));
        if (props.facilityData) {
            var facilityData = this.state.facilityData;
            facilityData = props.facilityData;
            this.setState({ facilityData });

        } 
        if(props.commonArray){
            this.setState({commonArray:props.commonArray});
        }
        if (props.facilityvalidations) {
            this.setState({ validations: props.facilityvalidations });
        }
        
        // this.props.sendfaciltiydata(this.state.facilityData, this.state.commonArray);
    }
    getAddress = () => {

        this.setState({ modalstate: true });
    }
    receiveAddress = (data, location) => {
        // alert(JSON.stringify(location));
        // var facilityData = this.state.facilityData;
        // facilityData.address = data.usertxt;
        // this.setState({ facilityData });
        this.setState({ modalstate: false });
        var commonArray=this.state.commonArray;
        commonArray[1].spec_det_datavalue1=data.usertxt;
        commonArray[1].spec_det_datavalue2=data.location?data.location[Object.keys(data.location)[0]].toString()+","+data.location[Object.keys(data.location)[1]].toString():null;
        this.setState({commonArray},()=>console.log('cr',this.state.commonArray));
        this.props.sendfaciltiydata(this.state.facilityData,commonArray);
        // this.changeText(data.usertxt,'address','spec_det_datavalue1')
    }
    changeText = (data, data2,dataobj, key) => { 
      if(this.state.init)
        this.setState({
          init:false
        })
        var facilityData=this.state.facilityData;
        var commonArray=this.state.commonArray;
        if(data2){
            if(data2=='address'){
                this.getAddress();
            }
            return;
        }else{
            // alert("nothing");
        }
        if(Number.isInteger(dataobj.spec_det_id)){
            var findIndex=facilityData.specDetails.findIndex((obj)=>obj.spec_det_id==dataobj.spec_det_id);
            if(findIndex!=-1){
                facilityData.specDetails[findIndex].spec_det_datavalue1=data;
                this.setState({facilityData})

            }
        }else{
            var findIndex=commonArray.findIndex((obj)=>obj.spec_det_id==dataobj.spec_det_id);
            if(findIndex!=-1){
                var errorcheck=ValidationLibrary.checkValidation(data,commonArray[findIndex].validation);
                console.log('fsd',errorcheck);
                commonArray[findIndex].spec_det_datavalue1=data;
                commonArray[findIndex].error=!errorcheck.state;
                commonArray[findIndex].errormsg=errorcheck.msg;
                this.setState({commonArray},function(){
                    this.checkValidations();
                });

            }
        }
        this.props.sendfaciltiydata(facilityData,commonArray);
        // var errormsg = ValidationLibrary.checkValidation(data, this.state.validations[key].validations);
        // var validations = this.state.validations;
        // validations[key].error = !errormsg.state;
        // validations[key].errormsg = errormsg.msg;
        // this.setState({ validations });
        // var mandatorylength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true).length;
        // var noerrorlength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true&&validations[obj].error == false).length;
        // // alert(JSON.stringify(noerrorlength));
        // var noerror = false;
        // if (mandatorylength == noerrorlength) {
        //     noerror = true;
        // }
        // this.props.sendfaciltiydata({ key: key, value: data, dbkey: dbkey, validations: validations, noerror: noerror });
        // var facilityData = this.state.facilityData;
        // facilityData[key] = data;
    }
        checkValidations=()=>{
        var commonArray=this.state.commonArray;
        for(var i in commonArray){
        var errorcheck=ValidationLibrary.checkValidation(commonArray[i].spec_det_datavalue1,commonArray[i].validation);
        commonArray[i].error=!errorcheck.state;
        commonArray[i].errormsg=errorcheck.msg;
        }
        var errordata=commonArray.filter((obj)=>obj.error==true);
        if(errordata.length!=0){
            // alert("error");
            this.props.sendfaciltiydata(this.state.facilityData,this.state.commonArray,false);
        }else{
            // alert("noerror");
            this.props.sendfaciltiydata(this.state.facilityData,this.state.commonArray,true);
        }

        // this.setState({commonArray});
        // return errordata;
    }
    render() {
        // alert(JSON.stringify(this.state.facilityData));
        return (
            <Content>
            <View style={{borderBottomWidth:0.5,borderColor:color.black1,}}>
              <View style={{flexDirection:'row',padding:10,paddingLeft:15}}>
         <View style={{flex:0.4}}><Text style={{fontSize:hp('2%')}}>Category</Text></View>
         <View  style={{flex:0.6}}><Text style={{fontWeight:'bold',fontSize:hp('2%')}}>{this.props.venuetypedata&&this.props.venuetypedata.venue_cat_name}</Text></View>

         </View>
         <View style={{flexDirection:'row',padding:10,paddingLeft:15}}>
         <View style={{flex:0.4}}><Text style={{fontSize:hp('2%')}}>Specification</Text></View>
         <View style={{flex:0.6}}><Text style={{fontWeight:'bold',fontSize:hp('2%')}}>{this.props.facilityData&&this.props.facilityData.venue_spec_name}</Text></View>

         </View>
         </View>
            <View style={{padding:20,marginTop:18}}>
            {/*
            <LabelTextbox error={this.state.validations.roomname.error} errormsg={this.state.validations.roomname.errormsg} capitalize={true} changeText={(data)=>this.changeText(data,'roomname','trn_venue_room_name')} value={this.state.facilityData.roomname} labelname="Room Name"/>
            <View style={{flex:1,flexDirection:'row',height:hp('6%'),marginBottom:25,}}>
                <View style={{flex:0.3,justifyContent:'center'}}>
                    <Label>Seats</Label>
                </View>
                <View style={{flex:0.7,flexDirection:'row',justifyContent:'space-between'}}>
                <View style={{flex:0.25}}> 
                <Item  regular style={styles.itemsize}>
            <Input  keyboardType="number-pad" onChangeText={(data)=>this.changeText(data,'seats','trn_venue_room_seats')} value={this.state.facilityData.seats} style={{height:'100%',padding:0}}/>
            {this.state.validations.seats.error&&
            <Text style={{position:'absolute',bottom:-hp('2.5%'),fontSize:hp('1.5%'),color:color.red}}>{this.state.validations.seats.errormsg}</Text>
            }
            </Item>
                </View>
                <View style={{flex:0.65,flexDirection:'row'}}>
                <View style={{flex:1}}>
                <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'floor','trn_venue_room_floor')} value={this.state.facilityData.floor} labelname="Floor"/>

                </View>
                </View>
            </View>
            </View>
            <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'address','trn_venue_room_address')} value={this.state.facilityData.address} type='textarea'  placeholder="Address" rowspan={3} labelname="Address"/>
            <LabelTextbox capitalize={true} changeText={(data)=>this.changeText(data,'landmark','trn_venue_room_landmark')} value={this.state.facilityData.landmark} labelname="LandMark">
            <TouchableOpacity onPress={this.getAddress} style={{paddingLeft:12,paddingRight:12}}><Text style={{textAlign:'right',color:color.blueactive,fontSize:hp('2.1%')}}>Location Map</Text></TouchableOpacity>
            </LabelTextbox>
            <LabelTextbox  error={this.state.validations.mobile.error} errormsg={this.state.validations.mobile.errormsg} capitalize={true} changeText={(data)=>this.changeText(data,'mobile','trn_venue_room_mob')} value={this.state.facilityData.mobile} labelname="Mobile"/>
            <LabelTextbox capitalize={true} error={this.state.validations.mail.error} errormsg={this.state.validations.mail.errormsg} changeText={(data)=>this.changeText(data,'mail','trn_venue_room_mail')} value={this.state.facilityData.mail} labelname="Mail"/>
        */}
         <AddressComp 
         receiveAddress={this.receiveAddress} 
         modalstate={this.state.modalstate} 
         onClose={()=>this.setState({modalstate:false})}
         />
       
        <DynamicForm changeText={this.changeText}  commonArray={this.state.facilityData?this.state.commonArray:[]} facilityData={this.state.facilityData?this.state.facilityData.specDetails:[]} />
            </View>
        </Content>
        );
    }
    componentDidMount(){
        // alert(JSON.stringify(this.props.facilityData));
        if(this.props.facilityData){
            this.setState({facilityData:this.props.facilityData})
        }
        if(this.props.commonArray){
            this.setState({commonArray:this.props.commonArray});
        }
    }
}

const styles = StyleSheet.create({
    itemsize: {
        height: hp('5.5%')
    }
});


export default FacilityDetails;