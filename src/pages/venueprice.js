"use strict";

import React, { Component } from "react";
import CircleText from "../components/circlecomp";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content
} from "native-base";

import { StyleSheet, View, Text, Picker, TextInput } from "react-native"; 
import color from "../Helpers/color";


var datearray = [
  { id: 1, name: "USD" },
  { id: 2, name: "INR" },
  { id: 3, name: "MYR" }
];

import Triangle from "../components/triangles";
import PopupMenu from "../components/popupmenu";
import FromToDuration from "../pages/fromtoduration";
import AccordionView from "../components/accordion";
import Hourly from "../images/svg/Hourly.png";
import Daily from "../images/svg/Daily.png";
import Weekly from "../images/svg/Weekly.png";
import Calendar from "../images/svg/calendar.png";
var datas = [
  { name: "Hourly", id: 1, icon: Hourly, keyname: "Hour" },
  { name: "Daily", id: 2, icon: Daily, keyname: "Day" },
  { name: "Weekly", id: 3, icon: Weekly, keyname: "Week" },
  { name: "Monthly", id: 4, icon: Calendar, keyname: "Month" }
];
 
const currency = [
  {
    id: 1,
    name: "USD"
  },
  {
    id: 2,
    name: "INR"
  },
  {
    id: 3,
    name: "EURO"
  }
];

datas.push({ name: "Hourly", id: 1, activeId: 1 });
datas.push({ name: "Hourly", id: 1, activeId: 1 });
var DurationForm = [{ expanded: true, title: "", content: <FromToDuration /> }];
var MoreDetails = [
  {
    expanded: false,
    title: "",
    content: (
      <View>
        <Text></Text>
      </View>
    )
  }
];
class VenuePrice extends Component {
  constructor(props) {
    // alert(Hourly);
    super(props);
    DurationForm[0].title = (
      <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
        {props.roomname}{" "}
        <Text style={{ color: color.black1 }}>Available Duration</Text>
      </Text>
    ); 
    
    MoreDetails[0].title = (
      <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
        {props.roomname}{" "}
        <Text style={{ color: color.black1 }}>Add More Details</Text>
      </Text>
    );
    this.state = {
      activeCircleObj: null,
      activeId: null,
      seatList:[],
        selectedCurrency: {
          id: 9,
          name: ""
        },
      selected: "key0",
      price: props.selectedobj,
      DurationForm: DurationForm,
      MoreDetails: MoreDetails,
      datas:[
  { name: "Hourly", id: 1, icon: Hourly, keyname: "Hour" },
  { name: "Daily", id: 2, icon: Daily, keyname: "Day" },
  { name: "Weekly", id: 3, icon: Weekly, keyname: "Week" },
  { name: "Monthly", id: 4, icon: Calendar, keyname: "Month" }
],
      pricedata: { activeId: "", days: "", amt: "0", type: "" },
      activeObj: props.pricedata
        ? props.pricedata.activeobj
        : { id: 1, name: "USD", keyname: "Hour" }
    };
    // alert(datas.length%2);
  }
  componentWillReceiveProps(props) {
    // if (props.priceData) {
    //   var pricedata = this.state.pricedata;
    //   pricedata = props.priceData;
    //   this.setState({ activeId: props.priceData.activeId });
    //   this.setState({ pricedata });
    //   this.setState({
    //     activeCircleObj: props.priceData.activeCircleObj
    //       ? props.priceData.activeCircleObj
    //       : null
    //   });

    //   this.setState({
    //     activeObj: props.priceData.activeObj
    //       ? props.priceData.activeObj
    //       : { id: 1, name: "USD", keyname: "Hour" }
    //   });
    // }
  }
  
   selectCurrency = item => {
    // this.props._selectCheckBox(item);
    var selectedCurrency = this.state.selectedCurrency;
    selectedCurrency = item;
    var seatList=this.state.seatList;
  seatList.length>0?seatList.map((obj)=>{
    obj.pricing[0].currency=item.name;
    return obj;
  }):[]
    this.setState({ selectedCurrency }); 


    // var price = this.state.price;
    // price.selectedCurrency = item;
    // this.setState({price})
    // this.props.sendPriceData(this.state.price);
  };
 

  componentDidMount = () => {
// alert(JSON.stringify(this.props.seatList));
if(this.props.availType.length!=""){
var datas=this.state.datas;
datas.map((obj1)=>{
var status=this.props.availType.includes(obj1.id.toString())
if(status==true){
  obj1.checked=true;
}else{
  obj1.checked=false;
}
return obj1;
})
this.setState({datas});
}
if(this.props.venuetype==3&&this.props.seatList){
  var seatList=JSON.parse(JSON.stringify(this.props.seatList));
  if(seatList.length>0){
var currencyactive=seatList[0].pricing?(seatList[0].pricing.length>0?seatList[0].pricing[0].currency:null):'USD';
var filtercurrency=currency.filter((obj)=>obj.name==currencyactive);
// alert(JSON.stringify(filtercurrency));
      var self=this;
      setTimeout(()=>{
self.setState({selectedCurrency:filtercurrency.length>0?filtercurrency[0]:currency[0]})
        seatList.map((objdata)=>{
          if(!objdata.pricing){
        var priceobj={currency:filtercurrency.length>0?filtercurrency[0]:currency[0]};
        var filteredRecords=self.state.datas.filter((obj1)=>obj1.checked==true).map((obj)=>{
          if(obj.id==1){
          priceobj.hourCost="0";
          }else if(obj.id==2){
            priceobj.dayCost="0";
          }else if(obj.id==3){
            priceobj.weekCost="0";
          }else{
            priceobj.monthCost="0";
          }
        })

        objdata.pricing=[priceobj];
      }
      return objdata;
      })
        // alert(JSON.stringify(seatList));
        self.setState({seatList})


      },100)

    // seatList.map((obj)=>{})
  }
  // this.setState({seatList:this.props.seatList});
}
if(this.props.venuetype==1){
  // alert(JSON.stringify(this.props.priceDataList))
  var seatList=this.state.seatList;
  var priceDataList=this.props.priceDataList?JSON.parse(JSON.stringify(this.props.priceDataList)):null;
  var currencyactive=priceDataList?((typeof priceDataList.currency)=="object"?priceDataList.currency.name:priceDataList.currency):'USD';
var filtercurrency=currency.filter((obj)=>obj.name==currencyactive);

console.log('active',filtercurrency[0])
console.log(currency[0]);
this.setState({selectedCurrency:filtercurrency.length>0?filtercurrency[0]:currency[0]})
// alert(JSON.stringify(this.props.priceDataList));
priceDataList=priceDataList?priceDataList:{}
  seatList=[{name:'',pricing:[{currency:filtercurrency.length>0?filtercurrency[0]:currency[0],hourCost:priceDataList.hourCost?priceDataList.hourCost:"0",dayCost:priceDataList.dayCost?priceDataList.dayCost:"0",weekCost:priceDataList.weekCost?priceDataList.weekCost:"0",monthCost:priceDataList.monthCost?priceDataList.monthCost:"0"}]}];
  this.setState({seatList});

}
      
  };
  

  _renderCurrency = select => {
    const { selectedCurrency,price } = this.state; 
    console.log('selectedfsd',selectedCurrency)
    console.log(Object.keys(selectedCurrency).length);
    return select.map(item => {
      return (
        <Button
          style={{
            paddingHorizontal: 25,
            paddingVertical: 10,
            backgroundColor:
              item.id == selectedCurrency.id  ? color.blue : "#DCDCDC",
            borderRadius: 5
          }}
          onPress={() => this.selectCurrency(item)}
        >
          <Text
            style={{
              color: item.id == selectedCurrency.id ? "white" : "black"
            }}
          >
            {item.name}
          </Text>
        </Button>
      );
    });
  };

  onValueChange = value => {
    this.setState({
      selected: value
    });
  };
  componentWillMount() {
    this.props.headertext({
      headertext: (
        <Text
          style={{
            fontSize: hp("1.7%"),
            fontWeight: "bold",
            color: color.blue
          }}
        >
          Price
        </Text>
      )
    });
    this.props.labeltext({
      labeltext: (
        <Text>
          <Text style={{ color: color.orange }}>{this.props.roomname}</Text>{" "}
          Please provide the{" "}
          <Text style={{ color: color.orange, fontWeight: "bold" }}>
            {" "}
            Price{" "}
          </Text>{" "}
        </Text>
      )
    });
  } 


  
  changeVenueType = (data, key) => {
    this.setState({ activeId: data.id });
    this.setState({ activeCircleObj: data });
    var pricedata = this.state.pricedata;
    pricedata.type = data.name;
    pricedata.activeId = data.id;
    pricedata.activeCircleObj = data;
    pricedata.days = this.state.activeObj.name;
    this.setState({ pricedata });
    this.props.sendPriceData(pricedata);
  };
  sendpopupobj = data => {
    var pricedata = this.state.pricedata;
    pricedata.activeObj = data;
    pricedata.days = data.name;
    this.setState({ activeObj: data });
    this.props.sendPriceData(pricedata);
    // alert(JSON.stringify(data));
  };
  changeText = (value, item) => {
    // var pricedata = this.state.pricedata;
    // pricedata.amt = data;
    // this.setState({ pricedata });

    console.log(value);
    console.log("i", item);
    const { price } = this.state;
    var check = this.state.price.findIndex(list => list.id == item.id);
    if (check != -1) {
      console.log("check", check);
      price[check].price = value;
      this.setState({ price }, () => {});
    }

    this.props.sendPriceData(price);
  };

  getKeyName = obj => {
    switch (obj.id) {
      case 1:
        return "Hour";
        break;
      case 2:
        return "Day";
        break;
      case 3:
        return "Week";
        break;
      case 4:
        return "Month";
        break;

      default:
        return "";
        break;
    }
  };
changepricingText=(text,index,key)=>{
  var seatList=this.state.seatList;
  seatList[index].pricing[0][key]=parseInt(text?text:0).toString();
  // alert(JSON.stringify(seatList))
  this.setState({seatList},()=>{
     this.props.sendPriceData({
       seatList: this.state.seatList,
       venuetype: this.props.venuetype,
       check:false
     });
 
  });
}
  renderPricing=(obj,pricing,index)=>{
    if(pricing.length>0){
      if(obj.id==1){
        return (
          <View><Text>Per Hour</Text>
           <View>
           <TextInput
           style={styles.textpricebox}
           keyboardType="number-pad"
           value={pricing[0].hourCost}
           onChangeText={(data)=>this.changepricingText(data,index,'hourCost')}
           />
           </View>
          </View>
          )
      }else if(obj.id==2){
        return (
          <View><Text>Per Day</Text>
           <View>
           <TextInput
           style={styles.textpricebox}
           keyboardType="number-pad"
           value={pricing[0].dayCost}
           onChangeText={(data)=>this.changepricingText(data,index,'dayCost')}

           />
           </View>
          </View>
          )
      }else if(obj.id==3){
        return (
          <View><Text>Per Week</Text>
           <View>
           <TextInput
           style={styles.textpricebox}
           keyboardType="number-pad"
           value={pricing[0].weekCost}
           onChangeText={(data)=>this.changepricingText(data,index,'weekCost')}

           />
           </View>
          </View>
          )
      }else{
        return (
          <View><Text>Per Month</Text>
           <View>
           <TextInput
           style={styles.textpricebox}
           keyboardType="number-pad"
           value={pricing[0].monthCost}
           onChangeText={(data)=>this.changepricingText(data,index,'monthCost')}

           />
           </View>
          </View>
          )
      }
    }
  }
  submitpricing=()=>{
    this.props.sendPriceData({seatList:this.state.seatList,venuetype:this.props.venuetype,check:true});
  }
  render() {
    console.log("selected", this.state.activeObj);
    return (
      <Content>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-between",
            paddingTop: 12,
            paddingLeft: 12,
            paddingRight: 12
          }}
        >
          {this.state.datas.map((obj, key) => {
            console.log(obj);
            return (
              <View key={key} style={[styles.circleStyle]}>
                <CircleText
                disabled={this.props.venuetype==2}
                  sendText={data => {}}
                  style={[styles.flexWidth]}
                  width={10}
                  height={10}
                  svg={true}
                  image={obj.icon}
                  text={obj.name}
                  data={obj}
                  active={obj.checked == true}
                />

                {/* {!obj.activeId && this.state.activeId == obj.id && (
                  <View>
                    <View style={styles.activeparent}>
                      <Triangle
                        width={hp("5%")}
                        height={hp("2%")}
                        color={color.black1}
                        direction={"up"}
                      ></Triangle>
                    </View>
                    <View style={[styles.activeparent, { top: -hp("1.6%") }]}>
                      <Triangle
                        width={hp("5%")}
                        height={hp("2%")}
                        color={color.ash1}
                        direction={"up"}
                      ></Triangle>
                    </View>
                  </View>
                )} */}
              </View>
            );
          })}
        </View>
{this.props.venuetype!=2&&
  <React.Fragment>
        <View style={styles.select}>{this._renderCurrency(currency)}</View>
{this.state.seatList.map((objData,keyindex)=>{
  return(
    <View style={{justifyContent:'space-between',paddingLeft:15}}>
      <Text style={{fontSize:hp('2.7%'),fontWeight:'bold',marginBottom:5}}>{objData.name}</Text>
      <View style={{flexDirection:'row',flexWrap:'wrap'}}>
      {this.state.datas.filter((obj1)=>obj1.checked==true).map((obj)=>{
       return this.renderPricing(obj,objData.pricing,keyindex);
      })}
      </View>
    </View>
    )
})}
<View style={{flexDirection:'row',justifyContent: 'center'}}>
          <Button
                              onPress={() => this.submitpricing()}
                              style={styles.actionbtn}
                            >
                              <Text style={styles.actionbtntxt}>Submit</Text>
                            </Button>
                            </View>
                            </React.Fragment>
                          }
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  circleStyle: {
    width: wp("20%")
  },
  select: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingVertical: 10
  },

  triangle: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid"
  },
  arrowUp: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
    borderTopWidth: 0,
    borderRightWidth: 12,
    borderBottomWidth: 12,
    borderLeftWidth: 12,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: color.grey,
    borderLeftColor: "transparent"
  },
  borderDays: {
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderLeftColor: "transparent",
    borderRightColor: "transparent"
  },
  bordertriangle: {
    position: "absolute",
    left: 0,
    right: 0,
    margin: "auto",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center"
  },
  activeparent: {
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999999,
    margin: "auto",
    left: 0,
    right: 0
  },
  textpricebox:{
    width:100,
    borderWidth:1,
    borderColor:'#ddd',
    marginRight:12,
    padding:5,
    marginBottom:7
  },  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  }
});

export default VenuePrice;
