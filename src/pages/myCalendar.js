import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Picker,
  ActivityIndicator,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import color from "../Helpers/color";
import {
  Accordion,
  Container,
  Content,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Footer,
  FooterTab
} from "native-base";
import dateFormat from "dateformat";
import moment from "moment";
import AsyncStorage from "@react-native-community/async-storage";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import Toast from "react-native-simple-toast";
import WeekAvailability from "./weekAvailability";
import MonthAvailability from "./monthAvailability";
import CalendarPicker from "react-native-calendar-picker"; 
import links from "../Helpers/config";
import DateFunctions from "../Helpers/DateFunctions";


export default class myCalendar extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     venueType: null,
                     venueData: null,
                     showloading: true,
                     error: false,
                     minDate: this.props.minDate,
                     minDate: this.props.maxDate,
                     start:'',
                     end:'',
                     currentDate: dateFormat(new Date(), "yyyy-mm-dd"),
                     validatefromdate: null,
                     validatetodate: null,
                     selectedStartDate: null,
                     selectedVenue: null,
                     selectedType: null,
                     loginDetails: null,
                     selectedWeekDate:moment().format("YYYY-MM-DD"),
                     venueList: [],
                     activeVenue: 0,
                     list: [],
                     allSlots: [],
                     generatedSlots: [],
                     bookingDetails: [],
                     availability: [],
                     venueavailType: [],
                     blockedslots:[],
                     validations: {
                       name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       },
                       mobile_no: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "mobile", status: false }]
                       },
                       event_name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: []
                       },
                       email: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: []
                       }
                     }
                   };
                 }
                 onDateChange(date) {
                   this.setState({
                     selectedStartDate: date
                   });
                 }

                 componentWillMount = () => {
                   this.checkingLogin();
                 };

                 async checkingLogin() {
                   const data = await AsyncStorage.getItem("loginDetails");

                   if (data != null) {
                     var parsedata = JSON.parse(data);
                     this.setState(
                       { loginDetails: parsedata, showloading: true,start:moment()
                     .startOf("month")
                     .format("YYYY-MM-DD hh:mm"),
                    end:moment()
                     .endOf("month")
                     .format("YYYY-MM-DD hh:mm") },
                       function() {
                         this.getAllVenueList();
                       }
                     );
                   } else {
                   }
                 } 

               
                

                 utilDate = index => {
                   var availfromdate = this.state.venueList[index]
                     .availability[0].trn_venue_avail_frm;

                   var availtodate = this.state.venueList[index].availability[0]
                     .trn_venue_avail_to;

                   var availDetails = this.state.venueList[index].availability;

                   var venueavailtype = this.state.venueList[index]
                     .availability[0].trn_availability_type;

                   var venuetype = this.state.venueList[index].trn_venue_type;

                   var venuearray = venueavailtype.split(",");

                   this.setState(
                     {
                       validatefromdate: availfromdate,
                       validatetodate: availtodate,
                       venueavailType: venuearray,
                       venueType: venuetype,
                       availability: availDetails
                     },
                     () => {}
                   );

                   

                 };

                 onClickVenue = (itemvalue, index) => {
                   if(index!=0)
                   this.utilDate(index - 1);

                   this.setState({
                     selectedVenue: itemvalue,
                     activeVenue: index,
                     showloading: false,
                     selectedType: null,
                     generateSlots: [],
                     bookingDetails: []
                   });
                 };

                 

                 getVenueSlotsByType = data => { 
           
                   const {
                     validatetodate,
                     validatefromdate,
                     selectedType,
                     selectedVenue
                   } = this.state;
                   if (!selectedVenue>0) 
                   return ;


                       console.log("okok", {
                         venueId: this.state.selectedVenue,
                         date:
                           selectedType != 1
                             ? moment(this.state.start).format("YYYY-MM-DD")
                             : this.state.selectedWeekDate,
                         todate: moment(this.state.end).format("YYYY-MM-DD"),
                         availType: this.state.selectedType
                       });

                   fetch(links.APIURL + "providerCalendarHourly", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       venueId: this.state.selectedVenue,
                       date:
                         selectedType != 1
                           ? moment(this.state.start).format("YYYY-MM-DD")
                           : this.state.selectedWeekDate,
                       todate: moment(this.state.end).format("YYYY-MM-DD"),
                       availType: this.state.selectedType
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       this.setState({
                         arrowclicked: true,
                         searchloading: null,
                         showloading: false
                       });

                       if (responseJson.status && responseJson.status == 1) {
                         Toast.show("No Records Found", Toast.LONG);
                       } else {
                         if (responseJson.length == 0) {
                           Toast.show("No Records Found", Toast.LONG);
                         } else {
                           var data = responseJson.data;

                           this.setState({
                             generatedSlots: data[0].availability[0].hourlySlots
                           });

                           this.setState({
                             bookingDetails: data[0].bookingDetails
                           });

                           if (selectedType != 1) {
                             var blockedDates =
                               responseJson.data.length > 0
                                 ? responseJson.data[0].availability
                                 : [];
                             if (blockedDates.length > 0) {
                               var filterRecords = blockedDates
                                 .filter(obj => obj.dateBlockedChecking == true)
                                 .map(dates => dates.selected_date);
                               this.setState({ blockedslots: filterRecords });

                               // alert(filterRecords);
                             }
                           }
                         }
                       }
                     });
                 }; 




                 refresh = (data) => {
                  
                   console.log("called");
                
                   if(data!=null){
                      this.setState(
                        {
                          start: data.startofmonth,
                          end: data.endofmonth
                        },
                        () => this.getVenueSlotsByType()
                      );
                   }else
                   this.getVenueSlotsByType();
                     
                  
                 };

                 onTypeChange = (value, index) => {
                   this.setState(
                     {
                       selectedType: value
                     },
                     () => this.getVenueSlotsByType()
                   );
                 };

                 isItExclude = () => {
                   var filteredData = this.state.venueobj.availability[0]
                     .trn_venue_exclude_days
                     ? this.state.venueobj.availability[0].trn_venue_exclude_days
                         .split(",")
                         .filter(obj => obj == data.getDay())
                     : [];

                   if (filteredData > 0) return true;
                   else return false;
                 };

                 pushAvailabilities = data => {
                   let generatedSlots = this.state.generatedSlots;
                   // alert(JSON.stringify(data));
                   var findindex = generatedSlots.findIndex(
                     obj => obj.selectedDate == data.selectedDate
                   );
                   if (findindex == -1) {
                     generatedSlots = [...generatedSlots, data];
                   } else {
                     if (data.selectedSlots.length == 0) {
                       generatedSlots.splice(findindex, 1);
                     } else {
                       generatedSlots[findindex].selectedSlots =
                         data.selectedSlots;
                     }
                   }
                   this.setState({
                     generatedSlots
                   });
                   var hoursArray = [];
                   var newArrayofSlots = generatedSlots.map(obj => {
                     obj.selectedSlots.map(obj2 => {
                       var objdata = {
                         fromdate: obj.selectedDate,
                         todate: obj.selectedDate,
                         slotFromTime: obj2.venue_slot_start_time,
                         slotToTime: obj2.venue_slot_end_time
                       };
                       hoursArray.push(objdata);
                     });
                   });
                   bookingData.slots = hoursArray;

                   console.log("check avaialatilti", generatedSlots);
                 };

                 getAllVenueList = () => {
                   fetch(links.APIURL + "myVneu", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       userId: this.state.loginDetails
                         ? this.state.loginDetails.user_id
                         : "0"
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       if (responseJson.status && responseJson.status == 1) {
                         Toast.show("Something Went Wrong", Toast.LONG);
                         this.setState({ showloading: false });
                       } else {
                         if (responseJson.status != 0) {
                           Toast.show("No Records Found", Toast.LONG);
                           this.setState({ showloading: false });
                         } else if (
                           responseJson.status == 0 &&
                           responseJson.data.length > 0
                         ) {
                           var data = responseJson.data.filter(
                             item => item.trn_venue_status == 1&&item.trn_venue_type!=2&&item.trn_venue_type!=3
                           );
                           this.setState({
                             showloading: false,
                             venueList: data
                           });
                         }
                       }
                     });
                 };

                 receiveDates=()=>{

                 }

                 _renderVenuePicker = (item, i) => {
                   return (
                     <Picker.Item
                       key={i}
                       label={item.trn_venue_name}
                       value={item.venue_id}
                     />
                   );
                 };

                 getTypeName = i => {
                   if (i == 1) return "Hourly";
                   else if (i == 2) return "Daily";
                   else if (i == 3) return "Weekly";
                   else if (i == 4) return "Monthly";
                 }; 

                 changeDate=(data)=>{
                    this.setState({selectedWeekDate:data.format("YYYY-MM-DD")},()=>{
                      this.getVenueSlotsByType(data)
                    })
                 }

                 generateSlots = () => {
                   var fromDate = moment(this.state.validatefromdate).format(
                     "HH:MM:SS"
                   );

                   var toDate = moment(this.state.validatetodate).format(
                     "HH:MM:SS"
                   );
                   var result = DateFunctions.getHoursInterval(
                     DateFunctions.getMobileDate(fromDate),
                     DateFunctions.getMobileDate(toDate),
                     60
                   );

                   this.setState({ list: result || [] });
                   this.setState({ allSlots: result || [] });

                   console.log("generatedslots", result);
                 }; 

                

                 render() {
                   const {
                     venueType,
                     minDate,
                     maxDate,
                     currentDate,
                     selectedStartDate,
                     showloading,
                     venueList,
                     selectedVenue,
                     selectedType,
                     validatefromdate,
                     validatetodate,
                     venueavailType,
                     generatedSlots
                   } = this.state; 

                   console.log("slseecte", this.state.selectedWeekDate);

                   if (this.state.showloading) {
                     return (
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "center",
                           alignItems: "center"
                         }}
                       >
                         <ActivityIndicator size="large" color="#073cb2" />
                         <Text>Please Wait</Text>
                       </View>
                     );
                   }

                   if (
                     !showloading &&
                     selectedVenue == null &&
                     selectedVenue == ''
                   ) {
                     return (
                       <React.Fragment style={styles.container}>
                         <View style={{ backgroundColor: color.orange }}>
                           <List nopadder>
                             <ListItem>
                               <Left>
                                 <Picker
                                   selectedValue={this.state.selectedVenue}
                                   style={{
                                     height: 50,
                                     width: wp("60%"),
                                     color: "white"
                                   }}
                                   onValueChange={(itemValue, itemIndex) =>
                                     this.onClickVenue(itemValue, itemIndex)
                                   }
                                 >
                                   <Picker.Item
                                     label="Choose your venue"
                                     value=""
                                   />
                                   {venueList.map((item, i) =>
                                     this._renderVenuePicker(item, i)
                                   )}
                                 </Picker>
                               </Left>

                               <Right>
                                 <Picker
                                   selectedValue={this.state.selectedType}
                                   style={{
                                     height: 50,
                                     width: wp("35%"),
                                     color: "white"
                                   }}
                                   onValueChange={(itemValue, itemIndex) => this.onTypeChange(itemValue, itemIndex)
                                   }
                                 >
                                   <Picker.Item label="Choose" value="" />
                                   {venueavailType.map((option, i) => {
                                     return (
                                       <Picker.Item
                                         key={i}
                                         label={this.getTypeName(option)}
                                         value={option}
                                       />
                                     );
                                   })}
                                 </Picker>
                               </Right>
                             </ListItem>
                           </List>
                         </View>

                         <View
                           style={{
                             flex: 1,
                             justifyContent: "center",
                             alignItems: "center"
                           }}
                         >
                           <View
                             style={{
                               width: wp("55%"),
                               justifyContent: "center"
                             }}
                           >
                             <Icon
                               type="FontAwesome"
                               name="calendar"
                               style={{
                                 color: "#000",
                                 fontSize: 38,
                                 paddingTop: 3,
                                 alignSelf: "center"
                               }}
                             />
                             <Text
                               style={{
                                 color: color.black1,
                                 fontSize: 24,
                                 textAlign: "center"
                               }}
                             >
                               Before Managing Calendar Please choose your venue
                             </Text>
                           </View>
                         </View>
                       </React.Fragment>
                     );
                   }

                   return (
                     <Container>
                       <Content>
                         <View style={styles.container}>
                           <View style={{ backgroundColor: color.orange }}>
                             <List nopadder>
                               <ListItem>
                                 <Left>
                                   <Picker
                                     selectedValue={this.state.selectedVenue}
                                     style={{
                                       height: 50,
                                       width: wp("55%"),
                                       color: "white"
                                     }}
                                     onValueChange={(itemValue, itemIndex) =>
                                       this.onClickVenue(itemValue, itemIndex)
                                     }
                                   >
                                     <Picker.Item
                                       label="Choose your venue"
                                       value=""
                                     />
                                     {venueList.map((item, i) =>
                                       this._renderVenuePicker(item, i)
                                     )}
                                   </Picker>
                                 </Left>

                                 <Right>
                                   <Picker
                                     selectedValue={this.state.selectedType}
                                     style={{
                                       height: 50,
                                       width: wp("30%"),
                                       color: "white"
                                     }}
                                     onValueChange={(itemValue, itemIndex) =>
                                       this.onTypeChange(itemValue, itemIndex)
                                     }
                                   >
                                     <Picker.Item label="Choose" value="" />
                                     {venueavailType.map((option, i) => {
                                       return (
                                         <Picker.Item
                                           key={i}
                                           label={this.getTypeName(option)}
                                           value={option}
                                         />
                                       );
                                     })}
                                   </Picker>
                                 </Right>
                               </ListItem>
                             </List>
                           </View>


                           {selectedVenue >0&& (
                             <List>
                               <ListItem
                                 style={{
                                   backgroundColor: "white",
                                   textAlign: "flex-start"
                                 }}
                               >
                                 <Text style={{ fontSize: 16 }}>
                                   Business Hours :{" "}
                                   {moment(validatefromdate).format("h:mm A")}
                                   {" - "}{" "}
                                   {moment(validatetodate).format("h:mm A")}
                                   {"\n"}
                                   Availability :{" "}
                                   {moment(validatefromdate).format(
                                     "MMM Do YYYY"
                                   )}
                                   {"  "}- {"  "}
                                   {moment(validatetodate).format(
                                     "MMM Do YYYY"
                                   )}
                                 </Text>
                               </ListItem>
                             </List>
                           )}

                           <View
                             style={{ height: 8, backgroundColor: "#edeef2" }}
                           />

                           {selectedType == 1 && (
                             <View>
                               <WeekAvailability
                                 venueId={this.state.selectedVenue}
                                 venueType={this.state.venueType}
                                 availabilityType={this.state.selectedType}
                                 pushData={this.pushAvailabilities}
                                 changeDate={this.changeDate}
                                 oldWeekData={this.state.generatedSlots}
                                 bookingDetails={this.state.bookingDetails}
                                 availability={this.state.availability}
                                 userId={this.state.loginDetails.user_id}
                                 refresh={this.refresh}
                                 selectedDate={this.state.selectedWeekDate}

                               />
                             </View>
                           )}

                           {(selectedType == 3 ||
                             selectedType == 2 ||
                             (selectedType != null &&
                               selectedType != "" &&
                               selectedType != 1) ||
                             selectedType == 4) && (
                             <View>
                               <MonthAvailability
                                 onDateChange={this.onDateChange}
                                 venueId={this.state.selectedVenue}
                                 minDate={moment(validatefromdate).format(
                                   "YYYY-MM-DD"
                                 )}
                                 maxDate={moment(validatetodate).format(
                                   "YYYY-MM-DD"
                                 )}
                                 venueType={this.state.venueType}
                                 availabilityType={this.state.selectedType}
                                 receiveDates={this.receiveDates}
                                 bookingDetails={this.state.bookingDetails}
                                 blockedslots={this.state.blockedslots}
                                 availability={this.state.availability}
                                 userId={this.state.loginDetails.user_id}
                                 refresh={this.refresh}
                               />
                             </View>
                           )}
                         </View>
                       </Content>
                     </Container>
                   );
                 }
               }



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: "white"
  },
  tab: {
    backgroundColor: "#edeef2"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  },
  select: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingVertical: 10
  },
  days: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "flex-start",
    paddingVertical: 5
  },

  pickerBox: {
    flex: 1,
    backgroundColor: "#edeef2",
    paddingVertical: hp("4%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly"
  },
  daysBox: {
    backgroundColor: "#fff",
    paddingVertical: hp("2%"),
    paddingHorizontal: wp("5%")
  }
});
