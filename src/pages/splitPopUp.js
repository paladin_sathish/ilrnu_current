import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image } from "react-native";
import ModalComp from '../components/ModalComp';
import {Fab,Icon,Button} from 'native-base';
import Schedule1 from '../components/schedule1';
import color from "../Helpers/color"; 
import LabelTextbox from '../components/labeltextbox'
import { Actions } from "react-native-router-flux";
import ValidationLibrary from '../Helpers/validationfunction';
var formkeys = [{ name: 'slots_name', type: 'default', labeltype: 'text' ,label:'Slots Name'}]
 import {
   widthPercentageToDP as wp,
   heightPercentageToDP as hp
 } from "react-native-responsive-screen";
 import ADD_img from "../images/add_icon.png";

export default class Test extends Component {
                 constructor(props) {
                   super(props); 
                   this.setSchedule = this.setSchedule.bind(this)
                   this.state = {
                     splittime: false,
                     slots_name: "",
                     editid:'',
                     edit:props.edit,
                     schedule: [],
                     splitSlots:[],
                     validations: {
                       slots_name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       },
                       schedule: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       }
                     }
                   };
                 } 



                 changeText = (data, key) => {
                   // alert(data);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                   if (key == "dob") {
                     if (data == "errordob") {
                       validations[key].error = true;
                       validations[key].errormsg =
                         "Invalid Date Accept Format (dd-mm-yyyy)";
                     } else {
                       validations[key].error = false;
                     }
                   } else if (key == "mobile") {
                     errormsg = ValidationLibrary.checkValidation(
                       data.mobile,
                       this.state.validations[key].validations
                     );
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   } else {
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   }
                   this.setState({ validations });
                   this.setState({
                     [key]: data
                   });
                 }; 


                 
                 _pushToParent=(data)=>{
                   this.setState({splitSlots:data})
                 }

                 closeModal=()=>{ 
                      
                   this.props.eraseEditSelected()
                   if(!this.state.edit){
this.setState({
  splittime: false
});
                   }
                     
                 }

 
                 verify = () => {
                   // alert(JSON.stringify(this.state));
                   var obj = this.state;
                   obj.type = "otpscreen";
                   var result = Object.keys(obj.validations).filter(
                     (object, key) => {
                       if (object == "dob") {
                         if (obj.validations[object].error == null) {
                           this.changeText("errordob", object);
                         }
                       } else if (object == "mobile") {
                         this.changeText(this.state[object], object);
                       } else {
                         this.changeText(this.state[object], object);
                       }
                       return (
                         obj.validations[object].mandatory == true &&
                         obj.validations[object].error == true
                       );
                     }
                   ); 

                    if(result.length ==0 && this.state.slots_name &&this.state.schedule.filter((obj)=>obj.checked==true).length>0) { 
                      // alert(JSON.stringify(this.state.schedule));
                      // return;
                       this.props._addSplit({
                         id:this.state.edit?this.props.editid:(new Date().getTime()+Math.floor(Math.random() * Math.floor(200))),
                         slots_name: this.state.slots_name,
                         value: this.state.schedule.filter((obj)=>obj.checked==true)
                       }); 

                       
                       this.setState({slots_name:''})
                       if(!this.state.edit){
                         this.setState({
                           splittime: false,
                           schedule: []
                         }); 

                       }
                         


                    }else
                    alert('Select All those Fields')

                 };

                 setSchedule=(data)=>{
                   // alert(JSON.stringify(data));
                   this.setState({
                     schedule: data,
                   });

                  
                 }  

                 componentWillReceiveProps(nextProps) {
                   console.log('next props',nextProps)
                    //update modal visible 
                   if (nextProps.edit !== this.props.edit) {
                     
                     this.setState({ edit: nextProps.edit });
                    
                   }

                   if (nextProps.isShowEdit !== this.props.isShowEdit ) {
                     this.setState({ splittime: nextProps.isShowEdit });
                   } 
 if (nextProps.editName !== this.state.editName) {
   this.setState({
     slots_name: nextProps.editName
   });
 }
 if( nextProps.editIndex>=0 && nextProps.checkedSlots.length>0){
   // alert(JSON.stringify());
   var schedulslots=this.props.checkedSlots[nextProps.editIndex];
   if(schedulslots!=undefined){
     // this.setState
     // alert(schedulslots.value.length);
     this.setState({schedule:schedulslots.value});
   }
   // this.setState({schedule:})
 }

                   
                 }
                 



                 render() {

                   
                   return (
                     <React.Fragment>
                         <View
                         style={{
                           flexDirection: "row",
                           justifyContent: "space-between",
                           display: "flex",
                           backgroundColor: "#f6f6f6",
                           padding: 10
                         }}
                       >
                         <View>
                           <Text
                             style={[styles.h1, { color: color.blueactive }]}
                           >
                             Add Slots
                           </Text>
                         </View>
                         <View>
                           <TouchableOpacity
                             onPress={() => {
                           this.props.eraseSelected();
                           this.setState({
                             splittime: true
                           });
                         }}
                             style={styles.add_symbol_container}
                           >
                             <Image
                               style={styles.add_symbol}
                               source={ADD_img}
                             ></Image>
                           </TouchableOpacity>
                         </View>
                       </View>


                       <View>
                         {this.state.splittime && (
                           <ModalComp
                             titlecolor={color.orange}
                             visible={this.state.splittime}
                             closemodal={() => this.closeModal()}
                           >
                             <View
                               style={{
                                 marginTop: 50,
                                 paddingRight: 20,
                                 paddingLeft: 20
                               }}
                             >
                               {formkeys.map((obj, key) => {
                                 return (
                                   <LabelTextbox
                                     inputtype={obj.type}
                                     key={key}
                                     type={obj.labeltype}
                                     capitalize={obj.name != "dob"}
                                     error={
                                       this.state.validations[obj.name].error
                                     }
                                     errormsg={
                                       this.state.validations[obj.name].errormsg
                                     }
                                     changeText={data =>
                                       this.changeText(data, obj.name)
                                     }
                                     value={this.state[obj.name]}
                                     labelname={obj.label}
                                   />
                                 );
                               })}
                             </View>

                             <Schedule1
                               onClick={this.setSchedule}
                               boxColor="#edeef2"
                               isOnClick={true}
                               list={this.props.list}
                               checkedSlots={this.props.checkedSlots}
                               edit={this.state.edit}
                               unVisibleEditSelected={
                                 this.props.unVisibleEditSelected
                               }
                             />

                             <View
                               style={{
                                 right: 0,
                                 display: "flex",
                                 justifyContent: "center"
                               }}
                             >
                               <Button
                                 onPress={() => this.verify()}
                                 style={{
                                   justifyContent: "center",
                                   textAlign: "center"
                                 }}
                               >
                                 <Text style={{ color: "white" }}>SUBMIT</Text>
                               </Button>
                             </View>
                           </ModalComp>
                         )}
                       </View>
                     </React.Fragment>
                   );
                 }
               }

const styles = StyleSheet.create({
  list: {},
  actionbtntxt: {
    alignSelf: "center",
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  },
  sociallogin: {
    backgroundColor: color.ash1,
    height: hp("7%"),
    alignItems: "stretch",
    justifyContent: "center"
  },
  h1: {
    fontSize: 17,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 17
  },
  h3: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  h6: {
    fontSize: 11,
    fontWeight: "200",
    fontStyle: "normal"
  },
  box: {
    marginVertical: hp("0.6%")
  },
  add_symbol_container: {
    backgroundColor: color.blue,
    justifyContent: "flex-end",
    justifyContent: "center",
    alignSelf: "flex-end",
    marginRight: wp("5%"),
    height: hp("2.5%"),
    width: hp("2.5%")
  },
  add_symbol: {
    height: hp("1.3%"),
    width: hp("1.3%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center"
  }
});
