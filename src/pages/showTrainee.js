import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image
} from "react-native";
import {Icon} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import CourseMore from "../pages/courseMore";
import ModalComp from "../components/Modal";
import AsyncStorage from "@react-native-community/async-storage";
import links from "../Helpers/config";
import Toast from "react-native-simple-toast";
 import refer from "../images/referearn.png"; 
 import { Actions } from "react-native-router-flux";

export default class MyCourseList extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     visible: false,
                     review: null,
                     loginDetails: null,
                     courses: [],
                     trainer:null
                   };
                 }
                 componentDidMount = () => {
                    
                 };



                 componentWillReceiveProps(props) {
                      if(props.Trainee){
                       this.setState({trainer:props.Trainee})
                   }

                 }

                 async componentWillMount() {
                   const data = await AsyncStorage.getItem("loginDetails");
                   var parsedata = JSON.parse(data);
                   this.setState({ loginDetails: parsedata });

                  

                 }

                 getCourseList = () => {
                   fetch(links.APIURL + "myCourseList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       userId: this.state.loginDetails.user_id
                      // userId:10
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       if (responseJson.status == 0) {
                         this.setState({
                           courses: responseJson.data
                         });
                       } else {
                         Toast.show("No Courses Found !!", Toast.LONG);
                       }
                     });
                 };

                 onLoadEdit = () => {
                   Actions.TraineeForm({
                     logindata: this.state.loginDetails,
                     edit:true,
                     Trainee:this.props.Trainee
                   });
                 };

                 getLangName = id => {
                   if (id == 1) return "English";
                   else if (id == 2) return "Hindi";
                   else return "Tamil";
                 };

                 onLoadMore = data => {
                   this.setState({ visible: data });
                 };

                 _renderCourseList = (item, i) => {
                   return (
                     <View
                       style={{
                         backgroundColor: color.white,
                         padding: 10,
                         borderRadius: 10
                       }}
                     >
                       <TouchableOpacity>
                         <View
                           style={{
                             flexDirection: "row",
                             justifyContent: "space-between",
                             display: "flex",
                             position: "relative",
                             borderBottomColor: "rgba(161,155,183,1)",
                             borderBottomWidth: 1,
                             borderStyle: "dashed",
                             marginBottom: 10,
                             borderRadius: 1
                           }}
                         >
                           <View>
                             <Text style={[styles.h3, { color: color.blue }]}>
                               {this.props.Trainee.user_name}
                             </Text>
                           </View>
                           <View>
                             <Text></Text>
                           </View>
                         </View>

                         <View>
                           <View
                             style={{
                               flexDirection: "row",
                               justifyContent: "space-between",
                               display: "flex"
                             }}
                           >
                             <View>
                               <Text style={[styles.h6]}>
                                 Category{"\n"}{" "}
                                 <Text style={styles.h3}>
                                   {this.props.Trainee.training_catName}
                                 </Text>
                               </Text>
                             </View>
                             <View>
                               <Text style={[styles.h6,{alignSelf:'flex-end'}]}>
                                 {"Specification "}
                                 
                               </Text>
                               <Text style={styles.h3}>
                                 {this.props.Trainee.training_spec_name}
                               </Text>
                             </View>
                           </View>

                           <View
                             style={{
                               flexDirection: "row",
                               justifyContent: "space-between",
                               display: "flex"
                             }}
                           >
                             <View>
                               <Text style={[styles.h6]}>
                                 Experience{"\n"}{" "}
                                 <Text style={styles.h3}>
                                   Years - {this.props.Trainee.trainer_exp_year}{" "}
                                   Months -{" "}
                                   {this.props.Trainee.trainer_exp_month}{" "}
                                 </Text>
                               </Text>
                             </View>
                             <View>
                               <Text style={[styles.h6]}>
                                 {"Landmark"}
                                 {"\n"}
                                 <Text style={styles.h3}>
                                   {this.props.Trainee.tariner_landmark}
                                 </Text>
                               </Text>
                             </View>
                           </View>

                           <View
                             style={{
                               flexDirection: "row",
                               justifyContent: "space-between",
                               display: "flex",
                               paddingVertical: 6
                             }}
                           >
                             <View>
                               <Text style={[styles.h1, { color: color.blue }]}>
                                 {" "}
                               </Text>
                             </View>
                             <View style={{ flexDirection: "row" }}>
                               <TouchableOpacity
                                 onPress={() => this.onLoadEdit()}
                               >
                                 <Text
                                   style={{
                                     color: color.blue,
                                     fontWeight: "bold"
                                   }}
                                 >
                                   Edit{" "}
                                 </Text>
                               </TouchableOpacity>
                             </View>
                           </View>
                         </View>
                       </TouchableOpacity>
                     </View>
                   );
                 };

                 render() { 
                   const { courses } = this.state;
                   const { review } = this.state; 
                   console.log("this.props.Trainee", this.props.Trainee);

                   return (
                     <View style={styles.container}> 

                      <View
                           style={{
                             flexDirection: "row",
                             justifyContent: "space-between",
                             display: "flex",
                             position: "relative",
                             borderBottomColor: "rgba(161,155,183,1)",
                             borderBottomWidth: 1,
                             borderStyle: "dashed",
                             marginBottom: 10,
                             borderRadius: 1,

                           }}
                         >
                           <View>
                             <Text style={[styles.h3, { color: color.blue }]}>
                               Trainer Skill Details
                             </Text>
                           </View>
                           <View>
                             
                           </View>
                         </View>


                       {
                            this._renderCourseList(this.props.Trainee, 0)
                         }

                     </View>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    flex:1,
    padding: 10,
    margin: 10,
    justifyContent:'flex-start',
    alignItems:'stretch'
  },
  h1: {
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },  buttonContainer: {
    backgroundColor: color.blueactive,
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 20,
    shadowOpacity: 0.25
  },
  h3: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  h6: {
    fontSize: 11,
    fontWeight: "200",
    fontStyle: "normal"
  },
  box: {
    marginVertical: hp("0.6%")
  }
});
