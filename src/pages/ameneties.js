"use strict";

import React, { Component } from "react";
import CircleText from "../components/circlecomp";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import ModalComp from "../components/Modal";
import AmentiesList from "../pages/amentieslist";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content
} from "native-base";
import { StyleSheet, View, Text, Picker } from "react-native";
import Triangle from "../components/triangles";
import FromToDuration from "../pages/fromtoduration";
import AccordionView from "../components/accordion";
import links from "../Helpers/config";
var DurationForm = [{ expanded: true, title: "", content: <FromToDuration /> }];
var MoreDetails = [
  {
    expanded: false,
    title: "",
    content: (
      <View>
        <Text></Text>
      </View>
    )
  }
];
var concatData = { id: 12, activeId: true, id: 13, activeId: true };
class Ameneties extends Component {
  constructor(props) {
    super(props);
    const newArray = props.nextdata.map(a => Object.assign({}, a));
    DurationForm[0].title = (
      <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
        {props.roomname}{" "}
        <Text style={{ color: color.black1 }}>Available Duration</Text>
      </Text>
    );
    MoreDetails[0].title = (
      <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
        {props.roomname}{" "}
        <Text style={{ color: color.black1 }}>Add More Details</Text>
      </Text>
    );
    this.state = {
      datas: newArray,
      amentiesvisible: false,
      activeId: 1,
      selected: "key0",
      DurationForm: DurationForm,
      MoreDetails: MoreDetails,
      ameneties: null
    };
    // alert(datas.length%2);
  }

  componentWillReceiveProps(props) {
    // alert(JSON.stringify(props));
    // for(var i in props.amentiesData){
    //   var getindex=this.state.datas.findIndex((obj,key)=>obj.id==props.amentiesData[i].id);
    //   var datas=this.state.datas;
    //   datas[getindex]=props.amentiesData[i];
    //   this.setState({datas});
    // }
  }
  onValueChange = value => {
    this.setState({
      selected: value
    });
  };
  loadAmenities = data => {
    fetch(links.APIURL + "list_amenities/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ venue_spec_id: data })
    })
      .then(response => response.json())
      .then(responseJson => {
        // alert(JSON.stringify(responseJson.data));
        if (responseJson.status == 0) {
          if (responseJson.data.length > 0) {
            var responeData = responseJson.data;
            // alert(JSON.stringify(this.props.amentiesData));
            if (this.props.amentiesData != null) {
              for (var i in this.props.amentiesData) {
                var findIndex = responeData.findIndex(
                  obj =>
                    obj.amenities_id == this.props.amentiesData[i].amenities_id
                );
                console.log("finIndex", findIndex);
                if (findIndex != -1) {
                  responeData[findIndex].activeArray = this.props.amentiesData[
                    i
                  ].activeArray;
                  responeData[findIndex].circleCount = this.props.amentiesData[
                    i
                  ].circleCount;
                  responeData[findIndex].active = this.props.amentiesData[
                    i
                  ].active;
                  for (var j in responeData[findIndex].amenities_array) {
                    var findnewIndex = this.props.amentiesData[
                      i
                    ].amenities_array.findIndex(
                      obj =>
                        obj.amenities_det_id ==
                        responeData[findIndex].amenities_array[j]
                          .amenities_det_id
                    );
                    if (findnewIndex != -1) {
                      responeData[findIndex].amenities_array[
                        j
                      ] = this.props.amentiesData[i].amenities_array[
                        findnewIndex
                      ];
                      var anotherIndex = this.props.amentiesData[i].activeArray
                        ? this.props.amentiesData[i].activeArray.findIndex(
                            obj =>
                              obj.venue_amnts_id ==
                              responseJson.data[findIndex].amenities_array[j]
                                .amenities_det_id
                          )
                        : -1;
                      if (anotherIndex != -1) {
                        //already checked subamenities make it visible
                        responeData[findIndex].amenities_array[
                          j
                        ].statekey = true;
                      } else {
                        responeData[findIndex].amenities_array[
                          j
                        ].statekey = false;
                      }
                    } else {
                      responeData[findIndex].amenities_array[
                        j
                      ].statekey = false;
                    }
                  }
                }
              }
              console.log(responeData);
            }
            this.setState({ ameneties: responeData });
          } else {
            this.setState({ ameneties: [] });
          }
        } else {
          this.setState({ ameneties: [] });
        }
        // alert(JSON.stringify(responseJson));
      });
  };
  componentWillMount() {
    this.props.headertext({
      headertext: (
        <Text
          style={{
            fontSize: hp("1.7%"),
            fontWeight: "bold",
            color: color.blue
          }}
        >
          Amenities
        </Text>
      )
    });
    this.props.labeltext({
      labeltext: (
        <Text>
          <Text style={{ color: color.orange }}>{this.props.roomname}</Text>{" "}
          Please add the{" "}
          <Text style={{ color: color.orange, fontWeight: "bold" }}>
            {" "}
            Amenities{" "}
          </Text>{" "}
        </Text>
      )
    });
  }
  amenetiesList = data => {
    var countArray = data.amenities_array.filter(obj => obj.statekey == true);
    var ameneties = this.state.ameneties;
    var index = ameneties.findIndex(
      (obj, key) => obj.amenities_id == data.amenities_id
    );
    ameneties[index].active = countArray.length > 0 ? true : false;
    ameneties[index].circleCount =
      countArray.length > 0 ? countArray.length : null;
    ameneties[index].amenities_array = data.amenities_array;
    var ActiveArray = [];
    if (countArray.length > 0) {
      for (var i = 0; i < countArray.length; i++) {
        var obj = {
          venue_amnts_id: countArray[i].amenities_det_id,
          venue_amnts_det_datatype1: countArray[i].amenities_det_datatype1,
          venue_amnts_det_datavalue1: countArray[i].amenities_det_datavalue1,
          venue_amnts_det_datatype2: countArray[i].amenities_det_datatype2,
          venue_amnts_det_datavalue2: countArray[i].amenities_det_datavalue2,
          venue_amnts_det_datatype3: countArray[i].amenities_det_datatype3,
          venue_amnts_det_datavalue3: countArray[i].amenities_det_datavalue3
        };
        if (countArray[i].hasOwnProperty("amenities_det_datavalued1") == true) {
          obj.venue_amnts_det_datavalue1 =
            countArray[i].amenities_det_datavalued1;
        }
        if (countArray[i].hasOwnProperty("amenities_det_datavalued2") == true) {
          obj.venue_amnts_det_datavalue2 =
            countArray[i].amenities_det_datavalued2;
        }
        if (countArray[i].hasOwnProperty("amenities_det_datavalued3") == true) {
          obj.venue_amnts_det_datavalue3 =
            countArray[i].amenities_det_datavalued3;
        }

        ActiveArray.push(obj);
      }
      //       // alert(JSON.stringify(countArray))
      ameneties[index].activeArray = ActiveArray;
    } else {
      ameneties[index].activeArray = [];
    }
    this.setState({ amentiesvisible: false });
    //     // alert(JSON.stringify(ameneties));
    this.setState({ ameneties });

    //     datas[index].active=data.amenetiesList.length>0?true:false;
    //     datas[index].circleCount=data.amenetiesList.length>0?data.amenetiesList.length:null;
    //   this.setState({datas});
    //   this.setState({amentiesvisible:false});
    // var filterAmeneties=this.state.datas.filter((obj)=>obj.active==true);
    this.props.sendAmenetiesData(ameneties);
  };
  changeVenueType = (data, key) => {
    this.setState({ activeObj: data });
    this.setState({ amentiesvisible: true });
    this.setState({ closeModal: false });
  };
  render() {
    return (
      <Content>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-between",
            paddingTop: 12,
            paddingLeft: 12,
            paddingRight: 12
          }}
        >
          {this.state.ameneties &&
            this.state.ameneties.length > 0 &&
            this.state.ameneties.concat(concatData).map((obj, key) => {
              return (
                <View key={key} style={[styles.circleStyle]}>
                  {!obj.activeId && (
                    <CircleText
                      circleCount={obj.circleCount}
                      circleValue={10}
                      sendText={data => this.changeVenueType(data, key)}
                      image={obj.amenities_icon}
                      style={[styles.flexWidth]}
                      width={10}
                      height={10}
                      text={obj.amenities_name}
                      data={obj}
                      active={obj.active}
                    />
                  )}
                </View>
              );
            })}
        </View>
        {this.state.amentiesvisible && (
          <ModalComp
            titlecolor={color.orange}
            visible={this.state.amentiesvisible}
          >
            <AmentiesList
              sendAmentiesList={data => this.amenetiesList(data)}
              dataObj={this.state.activeObj}
            />
          </ModalComp>
        )}
      </Content>
    );
  }
  componentDidMount() {
    if (this.props.facilitytypeData) {
      this.loadAmenities(this.props.facilitytypeData.venue_spec_id);
    }
    // alert(JSON.stringify(this.props));
  }
}

const styles = StyleSheet.create({
  circleStyle: {
    width: wp("20%")
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid"
  }, 
  arrowUp: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
    borderTopWidth: 0,
    borderRightWidth: 12,
    borderBottomWidth: 12,
    borderLeftWidth: 12,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: color.grey,
    borderLeftColor: "transparent"
  },
  borderDays: {
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderLeftColor: "transparent",
    borderRightColor: "transparent"
  },
  bordertriangle: {
    position: "absolute",
    left: 0,
    right: 0,
    margin: "auto",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center"
  },
  activeparent: {
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999999,
    margin: "auto",
    left: 0,
    right: 0
  }
});

export default Ameneties;
