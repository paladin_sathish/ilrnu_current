'use strict';

import React, { Component } from 'react';
import CircleText from'../components/circlecomp';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import {Left,List,Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox,Accordion } from 'native-base';
import DropdowContainer from '../components/drpdown_container'
import Corporate_AddDetails from '../pages/Corporate_AddDetails';
import Corporate from '../images/svg/corporate-building.png';
import PrivatePlay from '../images/svg/private-playground.png';
import Independent from '../images/svg/independent-home.png';
import Apartments from '../images/svg/apartments.png';
import Institution from '../images/svg/institution.png';
import Hotel from '../images/svg/hotel.png';
import BrandPromo from '../images/svg/brand-promotion.png';
import Indoor from '../images/svg/indoor-sports.png';
import links from '../Helpers/config';
var datas=[{name:'Corporate',id:1,icon:Corporate},{name:'Private Playgrounds',id:2,icon:PrivatePlay},{name:'Independent Home',id:3,icon:Independent},{name:'Apartments',id:4,icon:Apartments},{name:'Institution',id:5,icon:Institution},{name:'Hotel',id:6,icon:Hotel},{name:'Brand Promotions',id:7,icon:BrandPromo},{name:'Indoor Sports',id:8,icon:Indoor}]
var concatData={id:12,disable:true};

class CorporateFacilities extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {venuedata:[],activeobj:null,countryvalue:null,ListTypeJson:[],spocVisible:false,commonArray:null};
	  // alert(datas.length%2);
	}

	componentWillMount(){
		this.props.labeltext({labeltext:<Text>Please Select the <Text style={{color:color.orange,fontWeight:'bold'}}> Facilities </Text> </Text>});
	}
sendDrodownData=(data,key)=>{
  // alert(JSON.stringify(data));
  if(data.spocData){
    this.setState({activeobj:data.spocData});
    this.setState({commonArray:data.spocData.spocData})
  }
  this.setState({[key]:data});
}
ListVenueType(data){
    fetch(links.APIURL+'listSpecificVenue/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({user_cat_id:"2","venue_cat_id":data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      this.setState({ListTypeJson:responseJson.data.concat(concatData)});
      // this.setState({"ListVenueType":responseJson.data.concat(listAdditonal),loading:false})
      // alert(JSON.stringify(responseJson))
    })
  }
  componentDidMount(){
if(this.props.venuedetails){
  // alert(JSON.stringify(this.props.venuedetails));
  this.ListVenueType(this.props.venuedetails.venue_cat_id);
}
  }
  changeVenueType=(data,key)=>{
    // var ListTypeJson=this.state.ListTypeJson;
    this.setState({activeobj:data});
    this.setState({spocVisible:false});

    

  }
  AuthorizeData=()=>{
  if(this.state.countryvalue.spocData&&this.state.countryvalue.spocData.venue_spec_id==this.state.activeobj.venue_spec_id){
    // this.setState({activeobj:this.state.countryvalue.spocData});
    this.setState({commonArray:this.state.countryvalue.spocData.spocData});
  }else{
    this.setState({commonArray:null});
  }
    this.setState({spocVisible:true});
    // this.setState({autho})
  }
sendSPOC=(data)=>{
  this.setState({spocVisible:false});
  // alert(JSON.stringify(this.state.countryvalue));
  var countryvalue=this.state.countryvalue;
  
  countryvalue.spocData=data;

  countryvalue.added=true;
  this.setState({countryvalue});
  this.props.spocDetails&&this.props.spocDetails(countryvalue);
  // alert(JSON.stringify(data));
}
  render() {
 
 
    return (
      <View>
      <View  style={{flex:1,padding:12,flexDirection:'row',alignItems:'center'}}>
      		 <View style={{flex:0.4}}><Text>Location</Text></View>
           <View style={{flex:0.6}}>
           <DropdowContainer 
                                     
                                     _borwidth={1} _borColor={color.ash6}  brdrradius={1}
                                    drop_items={'Country'}
                                    values={this.props.locationArray&&this.props.locationArray}
                                    keyvalue={"name"}
                                            value={this.state.countryvalue?this.state.countryvalue.name:'Locations'}
                                             sendDropdownData={(data)=>this.sendDrodownData(data,'countryvalue')}
                                             fullwidth={true} />
           </View>
           </View>
             <View style={{flex:1,flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between',padding:12,}}>
           {this.state.countryvalue&&this.state.ListTypeJson.length>0&&this.state.ListTypeJson.map((obj,key)=>{
             return(
                <View key={key} style={[styles.circleStyle]}>
           {!obj.disable&&
      <CircleText image={obj.venue_spec_icon}  sendText={(data)=>{this.changeVenueType(data,key)}}  style={[styles.flexWidth]} width={12} height={12} text={obj.venue_spec_name} data={obj} active={this.state.activeobj&&this.state.activeobj.venue_spec_id==obj.venue_spec_id}/>
    }
  </View>
               )
           })
          
  }

  </View>
   {this.state.countryvalue&&
<View style={{flexDirection:'row',justifyContent:'center',flex:1}}>
<Button onPress={this.AuthorizeData} disabled={!this.state.activeobj}  style={[styles.buttonAuthorize,{backgroundColor:!this.state.activeobj?color.ash1:color.orangeBtn}]}><Text style={styles.buttonText}>Authorize</Text></Button>
</View>
}
{this.state.spocVisible==true&&
<Corporate_AddDetails closemodal={()=>this.setState({spocVisible:false})} commonArray={this.state.commonArray} activeobj={this.state.activeobj} sendSPOC={this.sendSPOC}/>
}
      </View>

    );
  }
}

const styles = StyleSheet.create({

circleStyle:{
	width:wp('30%'),
},
buttonAuthorize:{
  backgroundColor:color.orangeBtn,
  borderRadius:5,
    minWidth:hp('20%'),
    justifyContent:'center'
},
activename:{
},
buttonText:{
color:color.white,
textAlign:'center',
fontSize:hp('2.3%')
}
});


export default CorporateFacilities;