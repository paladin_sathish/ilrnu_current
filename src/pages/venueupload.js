'use strict';

import React, { Component } from 'react';
import CircleText from '../components/circlecomp';
import ModalComp from '../components/Modal';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Toast from 'react-native-simple-toast';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content, ActionSheet, Root } from 'native-base';
import {
    StyleSheet,
    View,
    Text,
    Picker,
    TextInput,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    TouchableWithoutFeedback
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
var BUTTONS = ["Gallery", "Camera", "Cancel"];
var datearray = [{ id: 1, name: 'Day' }, { id: 2, name: 'Week' }, { id: 3, name: 'Hour' }, { id: 4, name: 'Month' }, { id: 4, name: 'Month' }, { id: 4, name: 'Month' }, { id: 4, name: 'Month' }]
import PhotoInfo from '../images/svg/photoinfo.png';
import PhotoNext from '../images/photo_next.png';
import PhotoPlus from '../images/svg/plus.png';
import Triangle from '../components/triangles';
import PopupMenu from '../components/popupmenu';
import FromToDuration from '../pages/fromtoduration';
import AccordionView from '../components/accordion';
var datas = [{ name: 'Hourly', id: 1 }, { name: 'Daily', id: 2 }, { name: 'Weekly', id: 3 }, { name: 'Monthly', id: 4 }];

datas.push({ name: 'Hourly', id: 1, activeId: 1 })
datas.push({ name: 'Hourly', id: 1, activeId: 1 })
var DurationForm = [{ expanded: true, title: '', content: <FromToDuration/> }]
var MoreDetails = [{ expanded: false, title: '', content: <View><Text></Text></View> }]
var CANCEL_INDEX = 3;
const options = {
    title: 'Select Avatar',
     multiple: true,
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
async function requestCameraPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA, {
                title: 'iLRNU App Camera and gallery Permission',
                message: 'iLRNU App need access to your camera and gallery',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
            },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
        } else {
            return false;
            console.log('Camera permission denied');
        }
    } catch (err) {
        console.warn(err);
    }
}
const data = new FormData();
export default class VenueUpload extends Component {

    constructor(props) {
        super(props);
        DurationForm[0].title = (
          <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
            {props.roomname}{" "}
            <Text style={{ color: color.black1 }}>Available Duration</Text>
          </Text>
        );
        MoreDetails[0].title = (
          <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
            {props.roomname}{" "}
            <Text style={{ color: color.black1 }}>Add More Details</Text>
          </Text>
        );
        this.state = {
          visible: false,
          avatarSource: "",
          activeId: 1,
          selected: "key0",
          DurationForm: DurationForm,
          MoreDetails: MoreDetails,
          arrayImages: [],
          arrayVenuImages: "",
          selectedImage: "",
          imagevisible: false
        };
        // alert(datas.length%2);
    }
    onValueChange = (value) => {
        this.setState({
            selected: value
        });
    }
    componentWillReceiveProps(props) {
        // alert(props.mainformData);
        if (props.mainformData) {
            this.setState({ arrayVenuImages: props.mainformData })
        }
        if (props.localimages) {
            if (props.localimages.length > 0) {
                this.setState({ arrayImages: props.localimages });
            }
        }
    }
    componentWillMount() {
      // requestCameraPermission();
this.props.headertext({headertext:"Photos","showskip":true});
        this.props.labeltext({ labeltext: <Text> Please  Add <Text style={{color:color.orange,fontWeight:'bold'}}> Photos </Text> </Text> });
    }
    responseCamerGallery = (response) => {
        console.log(response);
        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
        } else {
            var arrayImages = this.state.arrayImages;
            response.checkdelete = null;
            arrayImages.push(response);

            var arrayVenuImages = this.state.arrayVenuImages;

            arrayVenuImages.append('formArray', {
                uri: response.uri,
                type: response.type,
                name: response.fileName
            });
            this.setState({ arrayVenuImages: arrayVenuImages })
            this.props.venueImages({ formimagearray: arrayVenuImages, localimages: arrayImages });

            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.setState({ arrayImages });
        }
    }
    loadGalleryCamera = async (data) => {
        // alert(response);
        if(data==0){
        var response = await requestCameraPermission('gallery');
    }
        if(data==1){
        var response = await requestCameraPermission('camera');
    }
        if (response == true) {
            if (data == 0) { //loadGallery
                ImagePicker.launchImageLibrary(options, (response) => {
                    // Same code as in above section!

                    this.responseCamerGallery(response);
                });
            } else if (data == 1) { //loadCamera
                ImagePicker.launchCamera(options, (response) => {
                    // Same code as in above section!
                    this.responseCamerGallery(response)
                });
            } else {
                //cancel code
            }
        }



    } 

    loadPhotos = () => {
        ActionSheet.show({
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                title: "Upload"
            },
            buttonIndex => {
                this.loadGalleryCamera(buttonIndex);
            }
        )
    }
    delteSelected = () => {
        var arrayImages = this.state.arrayImages.filter((obj) => obj.checkdelete != true);
        this.setState({ arrayImages });
        var formarrayImages =arrayImages.filter((obj) => obj.checkdelete != true&&!obj.exist);
        // alert(formarrayImages.length);
        var arrayVenuImages = new FormData();
        for (var i=0;i< formarrayImages.length;i++) {

            arrayVenuImages.append('formArray', {
                uri: formarrayImages[i].uri,
                type: formarrayImages[i].type,
                name: formarrayImages[i].fileName
            });
        }
        this.setState({ arrayVenuImages: arrayVenuImages })
            this.props.venueImages({ formimagearray: arrayVenuImages, localimages: arrayImages });

    }
    modalState = (data) => {
        this.setState({ visible: data });
    }
    loadDelete = (key, type,notexist) => {
        var count = this.state.arrayImages.filter((obj) => obj.checkdelete == true);
        var arrayImages = this.state.arrayImages;
        if (type == 'long') {
            arrayImages[key].checkdelete = true;
        } else {
            if (count.length > 0) {
                if (this.state.arrayImages.length > 0) {
                    arrayImages[key].checkdelete = !arrayImages[key].checkdelete;

                }
            }else{
             this.setState({imagevisible:true});
             // const imgsource = { uri: 'data:image/jpeg;base64,' + count.data };
this.setState({selectedImage:notexist?('data:image/jpeg;base64,'+arrayImages[key].data):arrayImages[key].url});
// console.log(arrayImages[key].data);
                }
        }
        // this.props.venueImages(arrayImages);
        this.setState({ arrayImages });
    }


    photoservice=()=>{
                  Toast.show('Request has been Sent!', Toast.LONG)

       // this.closemodal();
    }
  
    closeimage=()=>{
this.setState({imagevisible:false});
    }
    render() {
        var count = this.state.arrayImages.filter(
          obj => obj.checkdelete == true
        );
        return (
          <Content>
            {count.length > 0 && (
              <TouchableOpacity
                onPress={() => this.delteSelected()}
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "transparent"
                }}
              >
                <Icon
                  style={{ color: "black" }}
                  name="trash-o"
                  type="FontAwesome"
                />
                <Text style={{ color: "black", paddingLeft: 6 }}>
                  Delete {count.length} Items
                </Text>
              </TouchableOpacity>
            )}
            <Root>
              <View style={styles.imagecontainer}>
                {this.state.arrayImages.map((obj, key) => {
                  return (
                    <View key={key} style={styles.imageView}>
                      <TouchableOpacity
                        onPress={() => this.loadDelete(key, null, !obj.exist)}
                        style={styles.venueimage}
                        onLongPress={() => this.loadDelete(key, "long")}
                      >
                        {obj.checkdelete == true && (
                          <View
                            style={[
                              styles.imageView,
                              {
                                alignItems: "flex-start",
                                zIndex: 9999999,
                                top: 0,
                                right: 0,
                                backgroundColor: "rgba(255,255,255,0.5)"
                              }
                            ]}
                          >
                            <Icon
                              type="FontAwesome"
                              name="check-square"
                              style={{ color: "green" }}
                            />
                          </View>
                        )}
                        <Image
                          style={styles.venueimage}
                          source={{
                            uri: !obj.exist
                              ? "data:image/jpeg;base64," + obj.data
                              : obj.url
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  );
                })}
                {(this.state.arrayImages.length == 2 ||
                  this.state.arrayImages.length % 3 == 2) && (
                  <View style={styles.imageView}></View>
                )}
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: hp("3%")
                }}
              >
                <View>
                  <TouchableOpacity onPress={() => this.modalState(true)}>
                    <Image
                      source={PhotoInfo}
                      style={{ width: hp("3.3%"), height: hp("3.3%") }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ marginLeft: 12, marginRight: 12 }}>
                  {this.state.arrayImages.length == 0 && (
                    <Text style={{ fontSize: hp("2.5%") }}>Upload Photos</Text>
                  )}
                  {!this.state.arrayImages.length == 0 && (
                    <Text style={{ fontSize: hp("2.5%") }}>
                      Upload More Photos
                    </Text>
                  )}
                </View>
                <View>
                  <TouchableOpacity onPress={() => this.loadPhotos()}>
                    <Image
                      source={PhotoPlus}
                      style={{ width: hp("4.5%"), height: hp("4.5%") }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </Root>
            <ModalComp
              titlecolor={color.orange}
              visible={this.state.visible}
              closemodal={data => this.modalState(data)}
            >
              <Header
                noShadow={true}
                style={{ backgroundColor: "white", borderRadius: 5 }}
              >
                <Body
                  style={{
                    margin: 8,
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Image
                    source={PhotoNext}
                    style={{
                      paddingRight: 12,
                      color: color.orange,
                      fontSize: hp("3.2%")
                    }}
                  />
                  <Text
                    style={{
                      marginLeft: 12,
                      color: color.orange,
                      fontSize: hp("3%")
                    }}
                  >
                    Photos Information
                  </Text>
                </Body>
              </Header>
              <Content>
                <Root>
                  <View style={{ paddingLeft: 20, paddingRight: 15 }}>
                    <View>
                      <Text style={{ fontSize: hp("2.3%"), color: color.ash6 }}>
                        How photos should look like?
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        height: hp("25%"),
                        justifyContent: "flex-start",
                        marginTop: 12,
                        marginBottom: 12
                      }}
                    >
                      <View
                        style={{ flex: 0.8, height: "100%", marginRight: 5 }}
                      >
                        <Image
                          style={[styles.venueimage, { borderRadius: 4 }]}
                          source={require("../images/venue.jpg")}
                        />
                      </View>
                      <View
                        style={{ flex: 0.2, justifyContent: "space-between" }}
                      >
                        <View style={{ height: "23%" }}>
                          <Image
                            style={[styles.venueimage, { borderRadius: 4 }]}
                            source={require("../images/venue.jpg")}
                          />
                        </View>
                        <View style={{ height: "23%" }}>
                          <Image
                            style={[styles.venueimage, { borderRadius: 4 }]}
                            source={require("../images/venue.jpg")}
                          />
                        </View>
                        <View style={{ height: "23%" }}>
                          <Image
                            style={[styles.venueimage, { borderRadius: 4 }]}
                            source={require("../images/venue.jpg")}
                          />
                        </View>
                        <View style={{ height: "23%" }}>
                          <Image
                            style={[styles.venueimage, { borderRadius: 4 }]}
                            source={require("../images/venue.jpg")}
                          />
                        </View>
                      </View>
                    </View>
                    <View>
                      <Text
                        style={{
                          fontSize: hp("2.3%"),
                          marginTop: 12,
                          color: color.ash6
                        }}
                      >
                        How suitable photography details improves these views
                        and booking?
                      </Text>
                    </View>

                    <View style={{ marginTop: 12 }}>
                      <Text style={{ fontSize: hp("2.1%") }}>
                        No matter how educated your website visitors are first
                        intrigue by your visual content and not written text.
                        Photography allows consumer to get engaged with your
                        venue and help them decide to book a venue. Make sure
                        you upload a good quality photo of your venue. Photos
                        are the great way to show the amenities, look and
                        services your venue offer, so be prudent while clicking
                        and posting a photo. Hence, uploading appealing pictures
                        of your venues will give users a better vision about
                        your venue and thereto higher conversion rate to your
                        venue! Venue Photos are the key for someone to choose an
                        ideal venue to book for their choice of events. Select
                        good quality photographs to increase the visibility of
                        your venue and for someone to decide to book. Ensure you
                        cover most of the important amenities and ambience along
                        with relevant team snaps so that a public user would
                        have an idea to book your venue. For more details,
                        select the Information icon next to Upload Photo button
                        to know more details about how your photograph increases
                        the booking of your venue. In case, your need any
                        support for your venue photography, Click the option
                        “Request for Photography Services” for our team to
                        allocate an empaneled vendor to do your professional
                        venue photo shoot for increasing your visibility in the
                        online space.
                      </Text>
                    </View>
                  </View>
                  <Button
                    block
                    style={styles.actionbtn}
                    onPress={() => this.photoservice()}
                  >
                    <Text style={styles.actionbtntxt}>
                      REQUEST FOR PHOTOGRAPHY SERVICE
                    </Text>
                  </Button>
                </Root>
              </Content>
            </ModalComp>
            <ModalComp
              closemodal={() => this.closeimage()}
              center={true}
              visible={this.state.imagevisible}
              header={false}
              modaltitle={"How"}
              modalsubtitle={"venue"}
            >
              <View style={{ flex: 1 }}>
                <Image
                  style={styles.venueimage}
                  style={{ width: wp("100%"), height: hp("50%") }}
                  source={{ uri: this.state.selectedImage }}
                />
              </View>
            </ModalComp>
          </Content>
        );
    }
}
const styles = {
    imagecontainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        margin: 12
    },
    imageView: {
        width: wp('28%'),
        height: hp('11%'),
        borderRadius: 7,
        marginBottom: 15
    },
    venueimage: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        borderRadius: 7,
    },
    actionbtn: {
        borderRadius: 5,
        backgroundColor: color.orange,
        justifyContent: 'center',
        marginLeft: 20,
        marginRight: 15,
        marginTop: 17,
        marginBottom: 14
    },
    actionbtntxt: {
        textAlign: 'center',
        color: color.white,
        fontSize: hp('2.3%')
    },
}