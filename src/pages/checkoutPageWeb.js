'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  WebView,
  Text
} from 'react-native';
 import { Actions, Router, Scene } from 'react-native-router-flux';

import Toast from 'react-native-simple-toast';
import LoadingOverlay from '.././components/LoadingOverlay';
const jsCode = `(function() {
                    var originalPostMessage = window.postMessage;

                    var patchedPostMessage = function(message, targetOrigin, transfer) {
                      originalPostMessage(message, targetOrigin, transfer);
                    };

                    patchedPostMessage.toString = function() {
                      return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
                    };

                    window.postMessage = patchedPostMessage;
                  })();`;
class CheckoutPageWeb extends Component {
  constructor(props) {
    super(props);
  
    this.state = {loaded:null};
  }
receiveMessageData=(data,venue_name)=>{
  var parseData=JSON.parse(data);
  // alert(parseData.status);
  if(parseData.status==true){
    Toast.show(venue_name+" Venue is booked successfully, details sent to your stripe email id,thank you.",Toast.LONG);
    setTimeout(()=>{
      Actions.reset('home');
    },200);
  }
}
  render() {
    const userId=this.props.navigation.state.params.userId;
    const venueId=this.props.navigation.state.params.venueId;
    const venuename=this.props.navigation.state.params.venuename;
    const paxseatid=this.props.navigation.state.params.paxseatid;
    var appurl="https://www.ilrnu.com";
    if(paxseatid){
    var uri=appurl+'/?/checkout/id='+venueId+'&'+paxseatid+'&mobilefor_payment&userId='+userId
    }else{
    var uri=appurl+'/?/checkout/id='+venueId+'&mobilefor_payment&userId='+userId
    }
    return (
      <View style={{flex:1,backgroundColor:'grey'}}>
      {!this.state.loaded&&
      <LoadingOverlay/>
    }
       <WebView
       javaScriptEnabled={true}
        scrollEnabled={false}
        bounces={false}
        userAgent="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
      onLoadEnd={()=>this.setState({loaded:true})}
      onMessage={(event)=> this.receiveMessageData(event.nativeEvent.data,venuename)}
        source={{uri:uri }}
      >
      </WebView>
     
      </View>
    );
  }
}

const styles = StyleSheet.create({

});


export default CheckoutPageWeb;