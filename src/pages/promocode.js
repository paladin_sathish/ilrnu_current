import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import ModalComp from "../components/ModalComp";
import { Fab, Icon, Button, List, Card  } from "native-base";
import color from "../Helpers/color";
import PropTypes  from 'prop-types';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

export default class PromoCode extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                     isPromo: false,
                     promo_code: "",
                   };
                 }

                 _renderList = (item, i) => {
                   return (
                     <Card padder>
                       <View
                         style={{
                           display: "flex",
                           flexDirection: "row",
                           justifyContent: "space-between",
                           paddingHorizontal: 15,
                           marginVertical: 6
                         }}
                       >
                         <View>
                           <Text
                             style={{
                               color: "#3f51b5",
                              
                             }}
                           >
                             {item.booking_slot_fromdate}{" "}
                             {item.booking_slot_fromtime}  -
                           </Text>
                         </View>
                         <View>
                           <Text style={{ fontSize: 16 }}>
                             {item.booking_slot_todate}{" "}
                             {item.booking_slot_totime}
                           </Text>
                         </View>
                       </View>
                     </Card>
                   );
                 };

                 componentWillReceiveProps(nextProps) {
                   if(nextProps.isPromoShow != this.state.isPromo){
  this.setState({
    isPromo: nextProps.isPromoShow
  });
                   }
                 
                 }

                 render() {
                   const { data } = this.props;

                   const { isPromo } = this.state;
                   return (
                     <React.Fragment>
                       <ModalComp
                         titlecolor={color.orange}
                         visible={isPromo}
                       >
                         <View
                           style={{
                             marginTop: 50,
                             flex: 1,
                             paddingHorizontal: 10
                           }}
                         >
                           <View>
                             <List>
                               {data.length > 0 &&
                                 data.map((item, i) =>
                                   this._renderList(item, i)
                                 )}
                             </List>
                           </View>
                         </View>
                       </ModalComp>
                     </React.Fragment>
                   );
                 }
               } 



PromoCode.propTypes = {
    isPromoShow:PropTypes.bool.isRequired,
    closePressed:PropTypes.func.isRequired,
    data:PropTypes.array,
}; 

PromoCode.defaultProps = {
  isPromoShow: false,
  data:[]
}; 



const styles = StyleSheet.create({
  list: {},
  actionbtntxt: {
    alignSelf: "center",
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  },
  sociallogin: {
    backgroundColor: color.ash1,
    height: hp("7%"),
    alignItems: "stretch",
    justifyContent: "center"
  }
});
