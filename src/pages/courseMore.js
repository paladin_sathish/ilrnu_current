import React, { Component } from 'react';
import { View, Text, StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";


export default class CourseMore extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     review: null
                   }; 
                 }
                 componentDidMount = () => {};

                 componentWillReceiveProps(props) {
                   if (props.data != null) {
                     this.setState({
                       review: props.data
                     });
                   }
                 }

                 getLangName = id => {
                   if (id == 1) return "English";
                   else if (id == 2) return "Hindi";
                   else return "Tamil";
                 };

                 render() {
                   const { review } = this.state;
                   console.log("reviewmore", this.props.data);
                   console.log('reviewmore',review)
                   return (
                     <View
                       style={{
                         display: "flex",
                         margin: 10,
                         backgroundColor: "white",
                         padding: 10
                       }}
                     >
                       <View
                         style={{
                           flexDirection: "row",
                           justifyContent: "space-between",
                           display: "flex"
                         }}
                       >
                         <View>
                           <Text style={[styles.h1, { color: color.blue }]}>
                             {this.props.data &&
                               this.props.data.course[0].courseTitle}
                           </Text>
                         </View>
                         <View>
                           <Text>
                             {" "}
                             {this.props.data &&
                               this.getLangName(
                                 this.props.data.course[0].languageId
                               )}
                           </Text>
                         </View>
                       </View>

                       <View
                         style={{
                           margin: 3,
                           backgroundColor: color.ash1,
                           padding: 10
                         }}
                       >
                         <View
                           style={{
                             flexDirection: "row",
                             justifyContent: "space-between",
                             display: "flex"
                           }}
                         >
                           <View>
                             <Text style={[styles.h6]}>
                               Course Fee {"\n"}{" "}
                               <Text style={styles.h1}>
                                 {this.props.data &&
                                   this.props.data.course[0].batchData[0]
                                     .batchFee}{" "}
                                 {this.props.data &&
                                   this.props.data.course[0].batchData[0]
                                     .batchFeeCurrency}
                               </Text>
                             </Text>
                           </View>
                           <View>
                             <Text style={[styles.h6]}> {"\n"}</Text>
                             <Text style={styles.h1}>
                               <Text style={styles.h3}>
                                 {this.props.data &&
                                   this.props.data.course[0].batchData[0]
                                     .batchStartDate}{" "}
                                 to{" "}
                                 {this.props.data &&
                                   this.props.data.course[0].batchData[0]
                                     .batchEndDate}
                               </Text>
                             </Text>
                           </View>
                         </View>

                         <View
                           style={{
                             flexDirection: "row",
                             justifyContent: "space-between",
                             display: "flex"
                           }}
                         >
                           <View>
                             <Text style={[styles.h6]}>
                               No Of Hours {"\n"}{" "}
                               <Text style={styles.h1}>
                                 {" "}
                                 {this.props.data &&
                                   this.props.data.course[0].batchData[0]
                                     .batchDurationHours}
                               </Text>
                             </Text>
                           </View>
                           <View>
                             <Text style={[styles.h6]}>
                               Seats {"\n"}
                               <Text style={styles.h1}>
                                 {" "}
                                 {this.props.data &&
                                   this.props.data.course[0].batchData[0]
                                     .batchSeatCount}
                               </Text>
                             </Text>
                           </View>
                         </View>
                       </View>

                       <View style={styles.box}>
                         <Text style={styles.h1}> Course Outline </Text>
                         <Text>
                           {this.props.data &&
                             this.props.data.course[0].courseOutline}
                         </Text>
                       </View>
                       <View style={styles.box}>
                         <Text style={styles.h1}> Course Description </Text>
                         <Text>
                           {this.props.data && this.props.data.course[0].courseDesc}
                         </Text>
                       </View>
                     </View>
                   );
                 }
               } 

const styles = StyleSheet.create({
  h1: {
    fontSize: 16,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  h6: {
    fontSize: 11,
    fontWeight: '200',
    fontStyle: "normal"
  },
  box: {
    marginVertical: hp("0.6%")
  }
});
