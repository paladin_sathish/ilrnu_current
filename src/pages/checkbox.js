import React, { Component } from 'react';
import { View, TouchableHighlight, Text, StyleSheet, Image, AppRegistry, Platform } from 'react-native';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import colors from '../Helpers/color'

export default class Checkbox extends Component{
    constructor(props)
    {
       super(props);
 
       this.state = { 
           checked: props.active 
        }
    }
    render(){
        return(
            <TouchableHighlight style={{width:'100%',}}  underlayColor = "transparent" style = { styles.checkBoxButton }>
                <View style = { styles.checkBoxHolder }>
                    <View style = {{ width: hp('2%'), height: hp('2%'),borderWidth:!this.state.checked?0.3:0,
                         padding: 0.3,alignItems:'center',justifyContent:'center',borderColor:colors.ash6,backgroundColor:colors.white }}>
                    {
                        (this.state.checked)
                        ?
                            (<View style = { [styles.checkedView,{backgroundColor:colors.blue,}] }>
                            <Icon style={{color:'white',fontSize:hp('1.5%')}} type='Ionicons' name='checkmark'/>
                            </View>)
                        :
                            (<View style = { [styles.uncheckedView] }/>)
                    }
                    </View>
                    <View style={{marginLeft:5,width:hp('14%')}}>
                        <Text numberOfLines={2} style = {[ styles.blue_txt]}>{ this.props.label }</Text>
                    </View>
                </View>
         </TouchableHighlight>
       );
    }
 }
 
 
 
 const styles = StyleSheet.create(
 {
   container:
   {
     paddingTop: (Platform.OS === 'ios') ? 20 : 0,
     paddingHorizontal: 25,
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center'
   },
 
   selectedArrayItemsBtn:
   {
     marginTop: 20,
     padding: 10,
     alignSelf: 'stretch'
   },
 
   btnText:
   {
     color: 'white',
     textAlign: 'center',
     alignSelf: 'stretch',
     fontSize: 18
   },
 
   checkBoxButton:
   {
   },
 
   checkBoxHolder:
   {
      flexGrow: 1,
     flexDirection: 'row',
     alignItems: 'center',
   },
 
   checkedView:
   {
     flex: 1,
     position:'absolute',
     justifyContent: 'center',
     alignItems: 'center',
     width:'100%',
     height:'100%'
   },
 
   checkedImage:
   {
     height: '80%',
     width: '80%',
     tintColor: 'white',
     resizeMode: 'contain'
   },
 
   uncheckedView:
   {
      position:'absolute',
     justifyContent: 'center',
     alignItems: 'center',
     width:'100%',
     height:'100%',
     top:0,
     backgroundColor:colors.ash1
   },
 
   checkBoxLabel:
   {
     flexWrap:'wrap',
     fontSize: hp('2%'),
     paddingLeft: 10
   },
   blue_txt:{
    color:colors.blue,
    fontSize:hp('1.8%')
},
 });
 
 
 
 
 