import React, { Component } from "react";
import { View, Text, StyleSheet, Alert, Picker,TouchableOpacity } from "react-native";
import {
  Container,
  Content,
  Icon
} from "native-base";
import color from "../Helpers/color";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import moment from "moment";
import Toast from "react-native-simple-toast";
import links from "../Helpers/config";
import Next from "../components/button";
import DatePicker from "react-native-datepicker";

const NameStar = ({ name }) => (
  <React.Fragment>
    <Text>
      {name} {""}
    </Text>{" "}
    <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
); 

const currencyList = [
  {
    id: 1,
    name: "USD"
  },
  {
    id: 2,
    name: "INR"
  },
  {
    id: 3,
    name: "EURO"
  }
];



var formkeys = [
  {
    name: "no_of_hours",
    type: "number",
    labeltype: "text1",
    label: <NameStar name="No of hours" />
  },
  {
    name: "no_of_seats",
    type: "number",
    labeltype: "text1",
    label: <NameStar name="No of Seats" />
  },
  {
    name: "course_fee",
    type: "number",
    labeltype: "text1",
    label: <NameStar name="Course Fee" />
  },
  {
    name: "currency",
    type: "default",
    labeltype: "picker",
    label: <NameStar name="Currency" />,
    data: currencyList
  },
  {
    name: "highlights",
    type: "default",
    labeltype: "textarea1",
    label: <NameStar name="Key highlights" />
  }
]; 


export default class CourseDetail extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     loading: false,
                     language_res: [],
                     level_res: [],
                     name: "",
                     language: "English",
                     level: "Beginner",
                    edit:false,
                     no_of_hours: "",
                     course_fee:'',
                     no_of_seats:'',
                     highlights:'',
                     from: moment().format("YYYY-MM-DD"),
                     to: moment().format("YYYY-MM-DD"),
                     currency: "INR",
                     validations: {
                       no_of_seats: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: true }]
                       },
                       no_of_hours: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: true }]
                       },
                       course_fee: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: true }]
                       },
                       highlights: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: true }]
                       }
                     }
                   };
                 }

                 componentWillReceiveProps(props) {
                   if (props.courseDetailState != null) {
                     this.state = props.courseDetailState;
                   }


                    if (props.edit) {
                     this.setState({edit:true})
                    }


                    if (props.courseDetailData != null) {
                       console.log('called');
                         this.setState(
                           {
                             no_of_hours:
                               props.courseDetailData.batchDurationHours,
                             no_of_seats: props.courseDetailData.batchSeatCount,
                             highlights: props.courseDetailData.keyHiglights,
                             course_fee: props.courseDetailData.batchFee,
                             currency: props.courseDetailData.batchFeeCurrency,
                             from: props.courseDetailData.batchStartDate,
                             to: props.courseDetailData.batchEndDate
                           },
                           () => console.log(this.state.no_of_hours)
                         );
                       }

                 }

                 componentDidMount = () => {
                   setTimeout(() => {
                     this.onSend();
                   }, 1000);
                 };

                 componentWillMount() {
                   this.props.headertext({
                     headertext: (
                       <Text
                         style={{
                           fontSize: hp("1.7%"),
                           fontWeight: "bold",
                           color: color.blue
                         }}
                       >
                         Course Details
                       </Text>
                     )
                   });
                   this.props.labeltext({
                     labeltext: (
                       <Text>
                         <Text style={{ color: color.orange }}>
                           {this.props.roomname}
                         </Text>

                         <Text
                           style={{ color: color.orange, fontWeight: "bold" }}
                         >
                           Course Details
                         </Text>
                       </Text>
                     )
                   });
                 } 

                 onClickVenue = (itemvalue, index) => {
                   console.log("changepicker", itemvalue);
                   this.setState(
                     {
                       currency: itemvalue
                     },
                     () => this.onSend()
                   );
                 };



                 _renderVenuePicker = (item, i) => {
                   return <Picker.Item key={i} label={item} value={item} />;
                 };

                 _renderLanguagePicker = (item, i) => {
                   return (
                     <Picker.Item
                       key={i}
                       label={item.LanguageName}
                       value={item.LanguageId}
                     />
                   );
                 };

                 _renderLevelPicker = (item, i) => {
                   return (
                     <Picker.Item
                       key={i}
                       label={item.TrainingLevelName}
                       value={item.TrainingLevelId}
                     />
                   );
                 };

                 getLanguage = () => {
                   fetch(links.APIURL + "getPrimaryLanguageList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       user_cat_id:
                         this.props.loginDetails &&
                         this.props.loginDetails.user_cat_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       if (responseJson.status == 0) {
                         console.log("lang", responseJson.data);
                         this.setState({
                           language_res: responseJson.data,
                           language: responseJson.data[0].LanguageId
                         });
                       }
                     });
                 };

                 getLevel = () => {
                   fetch(links.APIURL + "getTrainingLevelList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       user_cat_id:
                         this.props.loginDetails &&
                         this.props.loginDetails.user_cat_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       if (responseJson.status == 0) {
                         this.setState({
                           level_res: responseJson.data,
                           level: responseJson.data[0].TrainingLevelId
                         });
                       }
                     });
                 };

                 onSubmit = () => {
                   const {
                     no_of_hours,
                     no_of_seats,
                     highlights,
                     course_fee,
                     currency,
                     from,
                     to,
                     edit
                   } = this.state; 

                   var g1 = new Date(from); 

                   var g2 = new Date(to); 

                   var err = 0;
                   for (const key of Object.keys(this.state.validations)) {
                     if (
                       this.state.validations[key].error != false &&
                       this.state.validations[key].mandatory == true
                     )
                       err += 1;
                   }
if (
  edit &&
  (no_of_hours =="" ||
    no_of_seats =="" ||
    highlights == "" ||
    course_fee =="" ||
    currency == "" ||
    from == "" || 
    to == "" )
) { 
console.log("if", {
  batchSeatCount: no_of_seats,
  batchDurationHours: no_of_hours,
  keyHiglights: highlights,
  batchFee: course_fee,
  batchFeeCurrency: currency,
  batchStartDate: from,
  batchEndDate: to
});
  Toast.show("Please fill the fields !!", Toast.LONG);
  return;
} else if (err > 0 && !edit) {
  console.log("else");
  Toast.show("Please fill the fields !!", Toast.LONG);
  return;
} else if(from == to){
 Toast.show("Batch Date must be differ", Toast.LONG);
 return;
} 
else if (g1.getTime() > g2.getTime()) {
 Toast.show("Course start date must be lower than end date !!", Toast.LONG);
  return;
     } else {
       var obj = {
         batchSeatCount: no_of_seats,
         batchDurationHours: no_of_hours,
         keyHiglights: highlights,
         batchFee: course_fee,
         batchFeeCurrency: currency,
         batchStartDate: from,
         batchEndDate: to
       };
       console.log("coursedeta", obj);
       this.props.onSubmit(obj, this.state);
     }
                 };

                 _renderCurrencyPicker = (item, i) => {
                   return (
                     <Picker.Item key={i} label={item.name} value={item.name} />
                   );
                 };

                 _renderForm() {
                   return (
                     <View
                       style={{
                         backgroundColor: "white"
                       }}
                     >
                       {formkeys.map((obj, key) => {
                         if (obj.labeltype == "picker") {
                           return (
                             <View>
                               <Text style={{ fontSize: 16 }}>{obj.label}</Text>
                               <View style={styles.pickerBorder}>
                                 <Picker
                                   selectedValue={this.state.currency}
                                   style={{
                                     height: 45,
                                     width: wp("90%"),
                                     color: color.black
                                   }}
                                   onValueChange={(itemValue, itemIndex) =>
                                     this.onClickVenue(itemValue, itemIndex)
                                   } 
                                 >
                                   { obj.data.length >0 && obj.data.map((item, i) =>
                                     this._renderCurrencyPicker(item, i)
                                   )}
                                 </Picker>
                               </View>
                             </View>
                           );
                         }
                         return (
                           <View style={{ marginBottom: 15 }}>
                             <LabelTextbox
                               inputtype={
                                 obj.type == "number" ? "numeric" : null
                               }
                               key={key}
                               type={obj.labeltype}
                               capitalize={obj.name != "dob"}
                               error={this.state.validations[obj.name].error}
                               errormsg={
                                 this.state.validations[obj.name].errormsg
                               }
                               changeText={data =>
                                 this.changeText(data, obj.name)
                               }
                               value={this.state[obj.name]}
                               labelname={obj.label}
                             />
                           </View>
                         );
                       })}
                     </View>
                   );
                 }

                 _resetForm = () => {
                   this.setState({
                     name: "",
                     email: "",
                     event_name: "",
                     mobile_no: ""
                   });
                 };

                 changeText = (data, key) => {
                   // alert(data);
                   console.log("datachange", data);
                   console.log("datachange", key);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                   if (key == "dob") {
                     if (data == "errordob") {
                       validations[key].error = true;
                       validations[key].errormsg =
                         "Invalid Date Accept Format (dd-mm-yyyy)";
                     } else {
                       validations[key].error = false;
                     }
                   } else if (key == "mobile") {
                     errormsg = ValidationLibrary.checkValidation(
                       data.mobile,
                       this.state.validations[key].validations
                     );
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   } else {
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   }
                   this.setState({ validations });
                   this.setState(
                     {
                       [key]: data
                     },
                     () => {
                       this.onSend();
                     }
                   );
                 };

                 onSend = () => { 

                    const {
                      no_of_hours,
                      no_of_seats,
                      highlights,
                      course_fee,
                      currency,
                      from,
                      to
                    } = this.state;
                    console.log('from',from)

                  var obj = {
                    batchSeatCount: no_of_seats,
                    batchDurationHours: no_of_hours,
                    keyHiglights: highlights,
                    batchFee: course_fee,
                    batchFeeCurrency: currency,
                    batchStartDate: from,
                    batchEndDate: to
                  }; 
                   this.props.sendCourseDetail(obj, this.state);
                 };

                 render() {
                   return (
                     <Content padder style={{ backgroundColor: "white" }}>
                       <View
                         style={{
                           flexDirection: "row",
                           justifyContent: "space-between",
                           marginTop: hp("2%"),
                           paddingBottom: hp("3%")
                         }}
                       >
                         <View>
                           <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                             Course Start Date
                           </Text>

                           <DatePicker
                             style={{
                               width: wp("40%"),
                               borderRight: "transparent"
                             }}
                             date={this.state.from}
                             mode="date"
                             format="YYYY-MM-DD"
                             confirmBtnText="Confirm"
                             cancelBtnText="Cancel"
                             iconComponent={
                               <Icon
                                 active
                                 name="clock-o"
                                 style={{
                                   fontSize: 22,
                                   color: "black",
                                   paddingLeft: 4
                                 }}
                                 type="FontAwesome"
                               />
                             }
                             customStyles={{
                               dateIcon: {
                                 position: "absolute",
                                 right: 20,
                                 top: 4,
                                 marginLeft: 14,
                                 paddingLeft: 14
                               },
                               dateInput: {
                                 borderWidth: 0,
                                 borderWidth: 0.5,
                                 borderColor: color.black1,
                                 marginLeft: 0,
                                 paddingLeft: 10,
                                 textAlign: "left",
                                 alignItems: "flex-start"
                               }
                               // ... You can check the source to find the other keys.
                             }}
                             onDateChange={from => {
                               this.setState({ from }, () => this.onSend());
                             }}
                           />
                         </View>
                         <View>
                           <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                             Course End Date
                           </Text>
                           <DatePicker
                             style={{ width: wp("40%") }}
                             date={this.state.to}
                             mode="date"
                             format="YYYY-MM-DD"
                             confirmBtnText="Confirm"
                             cancelBtnText="Cancel"
                             iconComponent={
                               <Icon
                                 active
                                 name="clock-o"
                                 style={{
                                   fontSize: 22,
                                   color: "black",
                                   paddingLeft: 4
                                 }}
                                 type="FontAwesome"
                               />
                             }
                             customStyles={{
                               dateIcon: {
                                 position: "absolute",
                                 right: 20,
                                 top: 4,
                                 marginLeft: 14,
                                 paddingLeft: 14
                               },
                               dateInput: {
                                 borderWidth: 0,
                                 borderWidth: 0.5,
                                 borderColor: color.black1,
                                 marginLeft: 0,
                                 paddingLeft: 10,
                                 textAlign: "left",
                                 alignItems: "flex-start"
                               }
                             }}
                             onDateChange={to => {
                               this.setState(
                                 {
                                   to
                                 },
                                 () => this.onSend()
                               );
                             }}
                           />
                         </View>
                       </View>

                       <View>{this._renderForm()}</View>
                       <Next
                         onClick={() => this.onSubmit()}
                         name="Next"
                         _width={wp("40%")}
                       />
                     </Content>
                   );
                 }
               }

               const styles = StyleSheet.create({
                 container: {
                   flex: 1,
                   justifyContent: "flex-start",
                   alignItems: "center",
                   backgroundColor: "#f1f1f1"
                 },
                 actionbtn: {
                   borderRadius: 5,
                   width: hp("17%"),
                   justifyContent: "center",
                   backgroundColor: color.blue
                 },
               actionbtntxt: {
                   textAlign: "center",
                   color: color.white,
                   fontSize: hp("2.3%")
                 },
                 pickerBorder: {
                   borderColor: "#d6d6d6",
                   borderWidth: 1,
                   borderStyle: "solid",
                   marginVertical:hp('0.5%')
                 }
               });
