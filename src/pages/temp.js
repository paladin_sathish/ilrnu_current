import React, { Component } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import { Content, Button, Tabs, ScrollableTab, Tab } from "native-base";
import WeekPicker from "../components/weekpicker";
import Schedule from "../components/schedule";
import color from "../Helpers/color";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import moment from "moment";
import Toast from "react-native-simple-toast";
import links from "../Helpers/config";
import CalenderBooked from "./calenderBooked";

const NameStar = ({ name }) => (
  <React.Fragment>
    <Text>
      {name} {""}
    </Text>{" "}
    <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
);
var formkeys = [
  {
    name: "language",
    type: "default",
    labeltype: "picker",
    label: <NameStar name="Primary Language" />
  },
  {
    name: "name",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Name" />
  },
  {
    name: "event_name",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Event Name" />
  },
  {
    name: "mobile_no",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Phone No" />
  },
  {
    name: "email",
    type: "default",
    labeltype: "text1",
    label: "Email"
  }
];

export default class Temp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialPage: 0,
      activeTab: 0,
      schedule: [],
      selectedDate: "",
      week: {},
      selectedSlots: [],
      loading: false,
      slots: [],
      name: "",
      mobile_no: "",
      event_name: "",
      email: "",
      language: "",
      validations: {
        language: {
          error: null,
          mandatory: true,
          errormsg: "",
          validations: [{ name: "required", status: false }]
        },
        name: {
          error: null,
          mandatory: true,
          errormsg: "",
          validations: [{ name: "required", status: false }]
        },
        mobile_no: {
          error: null,
          mandatory: true,
          errormsg: "",
          validations: [{ name: "mobile", status: false }]
        },
        event_name: {
          error: null,
          mandatory: true,
          errormsg: "",
          validations: []
        },
        email: {
          error: null,
          mandatory: false,
          errormsg: "",
          validations: []
        }
      }
    };
  }

  onSubmit = () => {
    var err = 0;
    for (const key of Object.keys(this.state.validations)) {
      if (
        this.state.validations[key].error != false &&
        this.state.validations[key].mandatory == true
      )
        err += 1;
    }

    console.log("error list", err);

    if (err > 0) {
      Toast.show("Field's Missing !!", Toast.LONG);
      return;
    } else {
    }
  };

  _renderForm() {
    return (
      <View
        style={{
          paddingTop: 20,
          paddingRight: 20,
          paddingLeft: 20,
          paddingBottom: 20,
          backgroundColor: "white"
        }}
      >
        {formkeys.map((obj, key) => {
          return (
            <View style={{ marginBottom: 15 }}>
              <LabelTextbox
                inputtype={obj.type}
                key={key}
                type={obj.labeltype}
                capitalize={obj.name != "dob"}
                error={this.state.validations[obj.name].error}
                errormsg={this.state.validations[obj.name].errormsg}
                changeText={data => this.changeText(data, obj.name)}
                value={this.state[obj.name]}
                labelname={obj.label}
              />
            </View>
          );
        })}
      </View>
    );
  }

  _resetForm = () => {
    this.setState({
      name: "",
      email: "",
      event_name: "",
      mobile_no: ""
    });
  };

  changeText = (data, key) => {
    // alert(data);
    var errormsg = ValidationLibrary.checkValidation(
      data,
      this.state.validations[key].validations
    );
    var validations = this.state.validations;
    if (key == "dob") {
      if (data == "errordob") {
        validations[key].error = true;
        validations[key].errormsg = "Invalid Date Accept Format (dd-mm-yyyy)";
      } else {
        validations[key].error = false;
      }
    } else if (key == "mobile") {
      errormsg = ValidationLibrary.checkValidation(
        data.mobile,
        this.state.validations[key].validations
      );
      validations[key].error = !errormsg.state;
      validations[key].errormsg = errormsg.msg;
    } else {
      validations[key].error = !errormsg.state;
      validations[key].errormsg = errormsg.msg;
    }
    this.setState({ validations });
    this.setState({
      [key]: data
    });
  };

  render() {
    return (
      <Content>
        <View>{this._renderForm()}</View>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#f1f1f1"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  }
});
