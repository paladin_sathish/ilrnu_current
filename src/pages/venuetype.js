'use strict';

import React, { Component } from 'react';
import CircleText from'../components/circlecomp';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import Corporate from '../images/svg/corporate-building.png';
import PrivatePlay from '../images/svg/private-playground.png';
import Independent from '../images/svg/independent-home.png';
import Apartments from '../images/svg/apartments.png';
import Institution from '../images/svg/institution.png';
import Hotel from '../images/svg/hotel.png';
import BrandPromo from '../images/svg/brand-promotion.png';
import Indoor from '../images/svg/indoor-sports.png';
import links from '../Helpers/config';
var datas=[{name:'Corporate',id:1,icon:Corporate},{name:'Private Playgrounds',id:2,icon:PrivatePlay},{name:'Independent Home',id:3,icon:Independent},{name:'Apartments',id:4,icon:Apartments},{name:'Institution',id:5,icon:Institution},{name:'Hotel',id:6,icon:Hotel},{name:'Brand Promotions',id:7,icon:BrandPromo},{name:'Indoor Sports',id:8,icon:Indoor}]
var concatData={id:12,disable:true};
class VenueType extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {venuedata:[],activeobj:props.venuedetails};
	  // alert(datas.length%2);
	}
  componentWillReceiveProps(props){
    if(props.venuedetails){
      this.setState({activeobj:props.venuedetails});
    }
  }
	componentWillMount(){
     this.props.headertext &&
       this.props.headertext({
         headertext: (
           <Text
             style={{
               fontSize: hp("1.7%"),
               fontWeight: "bold",
               color: color.blue
             }}
           >
             Venue Category
           </Text>
         )
       });
		this.props.labeltext&&this.props.labeltext({labeltext:<Text>Please choose the <Text style={{color:color.orange,fontWeight:'bold'}}> Venue Category </Text> </Text>});
	}
  getVenueType=()=>{
          fetch(links.APIURL+'listVenueCategory/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({user_cat_id:this.props.loginDetails&&this.props.loginDetails.user_cat_id}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      if(responseJson.status==0){
        this.setState({venuedata:responseJson.data});
      }
    })
 }
  componentDidMount(){
    this.getVenueType();
  }
changeVenueType=(data,key)=>{
  // var venuedata=this.state.venuedata;
 // venuedata=data.id;s
	this.setState({activeobj:data});
  this.props.sendvenuetypedata(data);
}
  render() {
 
 
    return (
      <View style={{flex:1,flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between',padding:12,}}>
      {this.state.venuedata&&this.state.venuedata.length>0&&this.state.venuedata.concat(concatData).map((obj,key)=>{
      	return(
      		 <View key={key} style={[styles.circleStyle]}>
           {!obj.disable&&
      <CircleText image={obj.venue_cat_icon}  sendText={(data)=>{this.changeVenueType(data,key)}}  style={[styles.flexWidth]} width={12} height={12} text={obj.venue_cat_name} data={obj} active={this.state.activeobj&&this.state.activeobj.venue_cat_id==obj.venue_cat_id}/>
    }
      </View>
      		)
      })}
     
      

      </View>
    );
  }
}

const styles = StyleSheet.create({

circleStyle:{
	width:wp('30%'),
},
activename:{
}
});


export default VenueType;