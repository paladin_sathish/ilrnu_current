'use strict';

import React, { Component } from 'react';
import VenueCarouselBox from '../components/VenueCarouselBox';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
    RefreshControl,
     ActivityIndicator,
    SafeAreaView
} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'

import { Container, Header,Item,Input, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body } from 'native-base'
import AsyncStorage from '@react-native-community/async-storage';
import links from '../Helpers/config';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';

class MyVenues extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {loading:null,changedText:'',loginDetails:null,venueList:[],filteredVenues:[]};
	}
	  loadVenues (){
this.setState({loading:true});
		fetch(links.APIURL+'myVneu/', {
		    method: 'POST',
		    headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
		    body:JSON.stringify({userId:this.state.loginDetails?this.state.loginDetails.user_id:"0"}),
		})
		.then((response)=>response.json())
		.then((responsejson)=>{     
    // alert(JSON.stringify(responsejson)) 
		this.setState({loading:null,refreshing:false})
		// alert(JSON.stringify(responsejson));
		this.setState({venueList:responsejson.data},function(){
		this.setState({filteredVenues:responsejson.data},function(){
			this.changeFilteredData(this.state.changedText)
		});
		});
		})
  	}
  	async checkingLogin(){
  		const data=await AsyncStorage.getItem('loginDetails');
 		this.setState({refreshing:false});
 		if(data!=null){
 		var parsedata=JSON.parse(data);
    	this.setState({loginDetails:parsedata},function(){
    		this.loadVenues();
    	});
 		}else{	
 		   // alert("loggedout")
 		   this.setState({refreshing:false})
 		}
  	}
	async componentWillMount (){
 	this.checkingLogin();
	}
	changeFilteredData=(data)=>{
		this.setState({changedText:data});
		var fiteredRecords=this.state.filteredVenues.filter(obj=>obj.trn_venue_name.toLowerCase().includes(data.toLowerCase()))
		this.setState({venueList:fiteredRecords})
		// console.log(JSON.stringify(fiteredRecords));
	}
	onRefresh=()=>{
		this.setState({refreshing:true},function(){
			this.loadVenues();
		})

	}
	loadEditVenue=(data)=>{
		// console.log(data.trn_venue_name)
		Actions.venueform({editData:data,logindata:this.state.loginDetails});
	}
  render() {
    return (
    	 <SafeAreaView style={styles.container} >
    	  <Header searchBar rounded style={{backgroundColor:color.top_red}}>
          <Item>
            
            <Input value={this.state.changedText} onChangeText={(data)=>this.changeFilteredData(data)} placeholder="Search" style={{paddingLeft:5}} />
            {this.state.changedText==''&&
            <Icon name="ios-search" />
        	}
        	{this.state.changedText!=""&&
        	<TouchableOpacity onPress={()=>this.changeFilteredData('')}>
        	<Icon type="FontAwesome" name="times-circle-o"/>
        	</TouchableOpacity>
        }
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
    	 <ScrollView style={{flex:1}} refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
        }>
    	 <Content style={{paddingTop:12}}>
       <View style={{flexDirection:'row',justifyContent:'center'}}>
      {this.state.loading&&!this.state.refreshing&&
  <ActivityIndicator size="large" color="#000" />
      }
      </View>

      {!this.state.loading&&this.state.loginDetails&&this.state.venueList.length>0&&this.state.venueList.map((obj)=>{
      	return(
      		<VenueCarouselBox item={obj} editVenue={(data)=>this.loadEditVenue(data)}/>
      		)
      })}
      {!this.state.loading&&this.state.refreshing==false&&this.state.venueList.length==0&&
      	<Text style={{textAlign:'center',fontSize:hp('2.5%')}}>No Records</Text>
      }
      </Content>
      </ScrollView>
      </SafeAreaView>
    );
  }
 
	componentDidMount(){
		
	}
}

const styles = StyleSheet.create({
container:{flex:1}
});


export default MyVenues;