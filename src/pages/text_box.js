import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,TextInput,} from 'react-native';
import {Right,Top,Icon, Button} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import Line from '../components/Line'

export default class Text_box extends Component{
    render(){
        return(
            <View style={styles.container}>
                <TextInput onEndEditing={()=>this.props.onEdit&&this.props.onEdit()} onSubmitEditing={()=>this.props.onEdit&&this.props.onEdit()} value={this.props.value} maxLength={this.props.maxLength} keyboardType={this.props.keyboardType} onChangeText={this.props.changeText&&this.props.changeText}  editable = {this.props.noedit?false:true} numberOfLines={this.props._noLines} style={[styles.textInput_style,
                    {minWidth:this.props._minwidth,flex:this.props._flex,height:this.props._height}]}>
                {/* {this.props.text_hint} */}
                </TextInput>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    textInput_style:{
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:8,
        paddingRight:8,
        borderWidth:1,
       // height:hp('4%'),
        borderColor:color.ash6,
     //   minWidth:this.props._minwidth
    }
})