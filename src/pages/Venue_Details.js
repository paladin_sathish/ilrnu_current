'use strict';
import React, { Component } from 'react';
import CircleText from '../components/CircleImageComp'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color'
import {StyleSheet,View,Text} from 'react-native';
import Text_box from '../components/text_box'
import Line from '../components/Line'
import Dropdown_container from '../components/drpdown_container'
var options=['Celebrate', 'Play','Party','Gather','Meet','Conference']
class Venue_Details extends Component{
    constructor(props){
        super(props);
        this.state={
            dodata:null,
            whatdata:null,
            wheredata:null,
        
        }
        
    }
    sendDropdownData=(data,key)=>{
        this.setState({[key]:data})
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={{paddingTop:hp('1.5%'),paddingBottom:hp('1.5%'),paddingLeft:hp('1.5%'),paddingRight:hp('1.5%')}}>
                    <View style={{flexDirection:'row',padding:hp('1%')}}>
                        <View style={{flex:.3}}><Text>Category</Text></View>
                        <View style={{flex:.7}}><Text style={{color:color.black}}>Play Ground</Text></View>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('1%')}}>
                        <View style={{flex:.3}}><Text>Specification</Text></View>
                        <View style={{flex:.7}}><Text style={{color:color.black}}>Badminton</Text></View>
                    </View>
                </View>
                <Line/>
                <View style={{paddingTop:hp('1.5%'),paddingBottom:hp('1.5%'),paddingLeft:hp('1.5%'),paddingRight:hp('1.5%')}}>
                    <View style={{flexDirection:'row',padding:hp('1%')}}>
                        <View style={{flex:.3}}><Text>Ground Name</Text></View>
                        <View style={{flex:.7,flexDirection:'row',justifyContent:'flex-start',alignItems:'flex-start',alignContent:'flex-start',alignSelf:'flex-start'}}>
                        <Text_box _minwidth={'100%'} _height={hp('4%')}/>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('1%')}}>
                        <View style={{flex:.3}}><Text>No. of Courts</Text></View>
                        <View style={{flex:.7,flexDirection:'row',justifyContent:'flex-start',alignItems:'flex-start',alignContent:'flex-start',alignSelf:'flex-start'}}>
                        <Text_box _minwidth={'30%'} _height={hp('4%')}/>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('1%')}}>
                        <View style={{flex:3}}><Text>Floor Type</Text></View>
                        <View style={{flex:.7,flexDirection:'row',justifyContent:'flex-start',alignItems:'flex-start',alignContent:'flex-start',alignSelf:'flex-start'}}>
                            <View style={{flex:1,paddingLeft:8,paddingRight:8,borderWidth:1,height:hp('4%'),borderColor:color.ash6,flexDirection:'column',justifyContent:'center'}}>                          
                                {/* <Dropdown_container 
                                    value={this.state.dodata?this.state.dodata:''}
                                    sendDropdownData={(data)=>this.sendDropdownData(data,'dodata')}
                                    _width={'100%'}
                                    _height={10}/> */}
                           </View> 
                        </View>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('1%')}}>
                        <View style={{flex:.3}}><Text>About Venue</Text></View>
                        <View style={{flex:.7,flexDirection:'row',justifyContent:'flex-start',alignItems:'flex-start',alignContent:'flex-start',alignSelf:'flex-start'}}>
                            <Text_box _minwidth={'100%'} height={hp('25%')} />
                        </View>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('1%')}}>
                        <View style={{flex:.3}}><Text>Ground Name</Text></View>
                        <View style={{flex:.4,flexDirection:'row',justifyContent:'flex-start',alignItems:'flex-start',alignContent:'flex-start',alignSelf:'flex-start'}}>
                            <Text_box _minwidth={'100%'} _height={hp('4%')}/>
                        </View>
                        <View style={{flex:.3,flexDirection:'row',justifyContent:'center'}}><Text style={{color:color.blue}}>Location Map</Text></View>
                    </View>

                </View>
            </View>
        )    
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white,
        flex:1,
    }
})

export default Venue_Details