import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import ModalComp from "../components/ModalComp";
import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
  ScrollableTab,
  Icon,
  CheckBox
} from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import DateFunctions from '../Helpers/DateFunctions';
import DatePicker from "react-native-datepicker";
import color from "../Helpers/color";
import links from '../Helpers/config';

const currency = [
  {
    id: 1,
    name: "USD"
  },
  {
    id: 2,
    name: "INR"
  },
  {
    id: 3,
    name: "EURO"
  }
];

class addForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      test:10,
      finalPax:[],
      pricetypeData:[],
      addPax: {
        selectedCurrency: {
          id: 1,
          name: "USD"
        },
        paxname: "",
        maximum: 0,
        minimum: 0,
        weekdays: {
          adult: 0,
          child: 0
        },
        weekends: {
          adult: 0,
          child: 0
        },
        specialdays: {
          adult: 0,
          child: 0
        }
      },
      showAddPaxForm: false,
      addSeat: [
        {
          name: "",
          value:"0",
          uniqueId:DateFunctions.generateTimestamp(),
          add:'true'
        }
      ]
    };
  }
  loadDaytypes=(propsData)=>{
      fetch(links.APIURL+'getDayType', {
      method:'POST',
      headers:{
        Accept:'application/json',
        'Content-Type':'application/json',
      },
      body:JSON.stringify({}),
    }).then((response)=>response.json())
      .then((respjson)=>{
        if(respjson.data.length>0){
          if(propsData){
for(var k =0;k<propsData.length;k++){
  var findindex=respjson.data.findIndex((obj)=>obj.day_type_code==propsData[k].day_type_code);
  if(findindex!=-1){
    respjson.data[findindex].adult=propsData[k].adult;
    respjson.data[findindex].child=propsData[k].child;
  }
}
          }else{
        respjson.data.map((obj)=>{
          obj.adult=0;
          obj.child=0;
          return obj;
        })  
          }
      }
        this.setState({pricetypeData:respjson.data})
      })
    }
  _renderType = select => { 
    
    const { selectedType } = this.props;
    console.log(Object.keys(selectedType).length);
    return select.map(item => {
      return (
        <Button
          style={{
            paddingHorizontal: 25,
            backgroundColor:
              item.id == selectedType.id ? color.blue : "#DCDCDC",
            borderRadius: 5
          }}
          onPress={() => this.selectCheckBox(item)}
        >
          <Text
            style={{ color: item.id == selectedType.id ? "white" : "black" }}
          >
            {item.name}
          </Text>
        </Button>
      );
    });
  };

  _renderCurrency = select => {
    const { addPax } = this.state;
    console.log(Object.keys(addPax.selectedCurrency).length);
    return select.map(item => {
      return (
        <Button
          style={{
            paddingHorizontal: 25,
            paddingVertical: 10,
            backgroundColor:
              item.id == addPax.selectedCurrency.id ? color.blue : "#DCDCDC",
            borderRadius: 5
          }}
          onPress={() => this.selectCurrency(item)}
        >
          <Text
            style={{
              color: item.id == addPax.selectedCurrency.id ? "white" : "black"
            }}
          >
            {item.name}
          </Text>
        </Button>
      );
    });
  };
  decrement = i => {
    var addSeat = this.state.addSeat;
    addSeat[i].value = parseInt(addSeat[i].value) - parseInt(1);
    this.setState({ addSeat });
    console.log(this.state.addSeat);
  };

  newDynamic = j => { 
     
    var addSeat = this.state.addSeat;
    addSeat[j].add=null; 
    if(addSeat.name==""){
      alert('Field Required')
      return;
    }
    const i = addSeat.length-1;
    if (
      addSeat[i].name != "" &&
      addSeat[i].name != null &&
      addSeat[i].value > 0
    ){
  addSeat.unshift({
    name: "",
    value: 0,
    uniqueId:DateFunctions.generateTimestamp(),
    add:'true'
  });
  this.setState({addSeat})
    }
     
    else alert("Filed Required Values");
  }; 

  _renderPaxList =(data,k)=>{
  
    return (
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          justifyContent: "space-between",
          alignItems: "stretch",
          paddingHorizontal: 15,
          paddingVertical: 15,
          borderColor: color.black1,
          borderWidth: 1,
          borderRadius: 2,
          width: "94%",
          margin: 12
        }}
        key={k}
      >
        <Text
          style={{
            fontStyle: "normal",
            fontSize: 18,
            fontWeight: "bold",

            paddingHorizontal: 15,
            paddingVertical: 15,
            textAlign:'center'
          }}
        >
          {data.paxname}
          {"        "}
        </Text>
        <View
          style={{
            justifyContent: "flex-end",
            alignItems:'center',
            textAlign:'right',
            display: "flex",
            flexDirection: "row"
          }}
        >
          <Icon
            active
            name="edit"
            style={{
              fontSize: hp("3%"),
              color: color.red,
            }}
            type="FontAwesome"
            onPress={() => {
              var addPax = this.state.addPax;
              addPax = data; 
              addPax.id = k;
              this.loadDaytypes(data.priceDetails)
              // alert(JSON.stringify(data));
              this.setState({ addPax });
              this.setState({ showAddPaxForm: true }, () =>
                console.log("id", this.state.addPax)
              );
            }}

          />
          <Text> {"         "}</Text>
          <Icon
            active
            name="trash"
            style={{
              fontSize: hp("3%"),
              color: "black",
              paddingLeft: 1
            }}
            type="FontAwesome"
            onPress={() => {
              var finalPax = this.state.finalPax;
              finalPax.splice(k, 1);
              this.setState({ finalPax });
            }}
          />
        </View>
      </View>
    );
  }

  increment = (i) => { 
    console.log("index");
    var addSeat=this.state.addSeat;
    addSeat[i].value = parseInt(addSeat[i].value)+1;
    console.log("index",addSeat);
    this.setState({addSeat},function(){
      console.log(addSeat);
    });
    // const inc = this.state.addSeat;
    // inc[i].value = this.state.addSeat[i].value++;
    // this.setState({addSeat:inc},
    //   ()=>{
    //     console.log(i)
    //     console.log(this.state.addSeat[i].value++);
    //     console.log(this.state.addSeat)
    //   })
  };  
componentDidMount(){
  this.loadDaytypes();
  // alert(JSON.stringify(this.props._finalPax));
  if(this.props._addSeat.length>0){
      this.setState({addSeat:this.props._addSeat})
    }
    if(this.props._finalPax){
      this.setState({finalPax:this.props._finalPax})
    }
}

  componentWillMount = () => {
     
    // if (this.props._state != null) {
    //   this.state = this.props._state;
    // }  

  }; 

  componentWillReceiveProps(props){ 
  
    // if(props._state != null){ 
    //   this.state = props._state;
    
    //  } 
    // alert(JSON.stringify(props._finalPax))
    // if(props._addSeat.length>0){
    //   this.setState({addSeat:props._addSeat})
    // }
    // if(props._finalPax){
    //   this.setState({finalPax:props._finalPax})
    // }
  }


  actionPressed = () => { 
     const {from,to} = this.props;
     if (from != to)
       this.props._submitAddForm(this.state.finalPax, this.state.addSeat,this.state);
       else
       alert("Business Hour's Must Be Different")
  };



  removeDynamic=(i)=>{ 
    var addSeat= this.state.addSeat;
     addSeat.splice(i,1)
    this.setState({addSeat})
    console.log('staet',this.state.addSeat)
  }

  toggleCheckbox = id => {
    this.props._toggleCheckbox(id);
  }; 

  selectCheckBox = item => {
    this.props._selectCheckBox(item);
  }; 

  renderAddSeat=(item,i)=>{
    console.log("item", item);
     return (
       <View
         style={{
           flexDirection: "row",
           justifyContent: "space-evenly",
           marginVertical: hp("1.5%")
         }}
         key={i}
       >
         <TextInput
           defaultValue="0"
           value={item.name}
           keyboardType="default"
           onChangeText={data => {
             var addSeat = this.state.addSeat;
             console.log("add seat", addSeat);
             addSeat[i].name = data;
             this.setState({ addSeat });
           }}
           style={{
             borderColor: color.black1,
             borderWidth: 1,
             padding: 0,
             paddingLeft: 5,
             paddingRight: 5,
             width: wp("40%")
           }}
           returnKeyType="next"
           blurOnSubmit={false}
         />

         <View
           style={{
             borderColor: color.black1,
             paddingHorizontal: 14,
             borderWidth: 1,
             borderStyle: "solid",
             flexDirection: "row",
             alignItems: "center"
           }}
         >
           <Icon
             active
             name="minus"
             style={{
               fontSize: 20,
               color: color.black1
             }}
             type="FontAwesome"
             onPress={() => this.decrement(i)}
           />

           <TextInput
             value={item.value.toString()}
             
             onChangeText={data => {
               var addSeat = this.state.addSeat;
               addSeat[i].value = data;
               this.setState({ addSeat, test: data }, () => {
                 console.log(this.state.addSeat[i].value);
               });
             }}
             style={{
               marginHorizontal: 4,
               padding: 0,
               paddingLeft: 5,
               paddingRight: 10,
               width: wp("15%"),
               fontSize: 22,
               alignSelf: "center",
               textAlign: "center"
             }}
             returnKeyType="next"
           />
           <Icon
             active
             name="plus"
             style={{
               fontSize: 20,
               color: color.black1
             }}
             type="FontAwesome"
             onPress={() => this.increment(i)}
           />
         </View>

         {i > 0 ? (
           <Icon
             active
             name="minus"
             style={{
               fontSize: 28
             }}
             type="FontAwesome"
             onPress={() => this.removeDynamic(i)}
           />
         ) : (
           <Icon
             active
             name="plus"
             style={{
               fontSize: 28
             }}
             type="FontAwesome"
             onPress={() => this.newDynamic(i)}
           />
         )}
       </View>
     );
  }

  updateCloseAddPaxForm = () => {
    this.setState(
      {
        showAddPaxForm: false
      },
      () => {}
    ); 

    this.reset()
  };

  selectCurrency = item => {
    // this.props._selectCheckBox(item);
    var addPax = this.state.addPax;
    addPax.selectedCurrency = item;
    this.setState({ addPax });
  };

  actionAddPaxPressed = () => {  

  // alert(DateFunctions.generateTimestamp());
    if (this.state.addPax.hasOwnProperty('id')==true&&this.state.addPax.id != -1) {
      var finalPax = this.state.finalPax;
      var addpax=this.state.addPax;
      var getuniqueid=addpax.uniqueId?addpax.uniqueId:DateFunctions.generateTimestamp();
      finalPax[this.state.addPax.id] = this.state.addPax;
      finalPax[this.state.addPax.id].uniqueId=getuniqueid;
      finalPax[this.state.addPax.id].priceDetails=this.state.pricetypeData;
      this.setState({ finalPax });
    } else {
      this.state.addPax.uniqueId=DateFunctions.generateTimestamp();
      this.state.addPax.priceDetails=this.state.pricetypeData;
      this.setState({
        finalPax: [...this.state.finalPax, this.state.addPax],
        showAddPaxForm: false
      });
    } 
   

   //remove old state values
   this.updateCloseAddPaxForm()

   console.log('finalPax',this.state.finalPax)
  }; 

  reset = ()=>{
      var addPax = this.state.addPax;
      var obj = {
        selectedCurrency: {
          id: 1,
          name: "USD"
        },
        paxname: "",
        maximum: 0,
        minimum: 0,
        weekdays: {
          adult: 0,
          child: 0
        },
        weekends: {
          adult: 0,
          child: 0
        },
        specialdays: {
          adult: 0,
          child: 0
        }
      };
      addPax = obj;
      this.loadDaytypes();
      this.setState({ addPax });
  }

  render() {
    const {
      _minTime,
      _maxTime,
      _checkboxes,
      _select,
      _updateFrom,
      _updateTo,
      _updateAFrom,
      _updateATo
    } = this.props;
    return (
      <Content style={{ backgroundColor: "white" }}>
        <View style={styles.container}>
          {/* <View style={styles.select}>{this._renderType(_select)}</View> */}
          <Tabs
            tabBarUnderlineStyle={{
              borderBottomWidth: 4,
              borderBottomColor: "#5067FF"
            }}
            renderTabBar={() => (
              <ScrollableTab style={{ backgroundColor: "white" }} />
            )}
          >
            <Tab
              style={styles.tab}
              heading="Availability Information"
              tabStyle={{ backgroundColor: "white" }}
              textStyle={{ color: "orange" }}
              activeTabStyle={{
                backgroundColor: "transparent"
              }}
              activeTextStyle={{
                color: "#5067FF",
                fontWeight: "normal"
              }}
            >
              <View style={styles.tab1}>
                <View style={styles.container}>
                  <View style={{ paddingHorizontal: 10 }}>
                    <Text style={{ fontSize: 16, paddingTop: hp("2%") }}>
                      {" "}
                      <Icon
                        active
                        name="clock-o"
                        style={{ fontSize: 20 }}
                        type="FontAwesome"
                      />{" "}
                      Business Hours
                    </Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-around",
                      marginTop: hp("2%"),
                      paddingBottom: hp("3%")
                    }}
                  >
                    <View>
                      <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                        From Time
                      </Text>

                      <DatePicker
                        style={{ width: wp("40%"), borderRight: "transparent" }}
                        date={this.props.from}
                        mode="time"
                        format="hh:mm A"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconComponent={
                          <Icon
                            active
                            name="clock-o"
                            style={{
                              fontSize: 22,
                              color: "black",
                              paddingLeft: 4
                            }}
                            type="FontAwesome"
                          />
                        }
                        customStyles={{
                          dateIcon: {
                            position: "absolute",
                            right: 20,
                            top: 4,
                            marginLeft: 14,
                            paddingLeft: 14
                          },
                          dateInput: {
                            borderWidth: 0,
                            borderWidth: 0.5,
                            borderColor: color.black1,
                            marginLeft: 0,
                            paddingLeft: 10,
                            textAlign: "left",
                            alignItems: "flex-start"
                          }
                          // ... You can check the source to find the other keys.
                        }}
                        onDateChange={date => {
                          // alert(date);
                          _updateFrom(date);
                        }}
                      />
                    </View>
                    <View>
                      <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                        To Time
                      </Text>
                      <DatePicker
                        style={{ width: wp("40%") }}
                        date={this.props.to}
                        mode="time"
                        format="hh:mm A"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconComponent={
                          <Icon
                            active
                            name="clock-o"
                            style={{
                              fontSize: 22,
                              color: "black",
                              paddingLeft: 4
                            }}
                            type="FontAwesome"
                          />
                        }
                        customStyles={{
                          dateIcon: {
                            position: "absolute",
                            right: 20,
                            top: 4,
                            marginLeft: 14,
                            paddingLeft: 14
                          },
                          dateInput: {
                            borderWidth: 0,
                            borderWidth: 0.5,
                            borderColor: color.black1,
                            marginLeft: 0,
                            paddingLeft: 10,
                            textAlign: "left",
                            alignItems: "flex-start"
                          }
                          // ... You can check the source to find the other keys.
                        }}
                        onDateChange={date => {
                          _updateTo(date);
                        }}
                      />
                    </View>
                  </View>

                  <View style={styles.pickerBox}>
                    <View>
                      <Text style={{ fontSize: 18 }}>
                        <Icon
                          name="calendar-check-o"
                          type="FontAwesome"
                          style={{ fontSize: 22 }}
                        />{" "}
                        Availability Dates
                      </Text>
                    </View>

                    <View>
                      <Text style={{ paddingVertical: 10 }}>From Date</Text>

                      <DatePicker
                        style={{ width: "100%" }}
                        date={this.props.availableFrom}
                        mode="date"
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconComponent={
                          <Icon
                            active
                            name="calendar-check-o"
                            style={{
                              fontSize: 22,
                              color: "black",
                              paddingLeft: 4
                            }}
                            type="FontAwesome"
                          />
                        }
                        customStyles={{
                          dateIcon: {
                            position: "absolute",
                            right: 20,
                            top: 4,
                            marginLeft: 14,
                            paddingLeft: 14
                          },
                          dateInput: {
                            borderWidth: 0,
                            borderWidth: 0.5,
                            borderColor: color.black1,
                            marginLeft: 0,
                            paddingLeft: 4,
                            alignItems: "flex-start"
                          }
                          // ... You can check the source to find the other keys.
                        }}
                        onDateChange={date => {
                          _updateAFrom(date);
                        }}
                      />
                    </View>

                    <View>
                      <Text style={{ paddingVertical: 10 }}>To Date</Text>
                      <DatePicker
                        style={{ width: "100%" }}
                        date={this.props.availableTo}
                        mode="date"
                        minDate={this.props.availableFrom}
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconComponent={
                          <Icon
                            active
                            name="calendar-check-o"
                            style={{
                              fontSize: 22,
                              color: "black",
                              paddingLeft: 4
                            }}
                            type="FontAwesome"
                          />
                        }
                        customStyles={{
                          dateIcon: {
                            position: "absolute",
                            right: 20,
                            top: 4,
                            marginLeft: 14,
                            paddingLeft: 14
                          },
                          dateInput: {
                            borderWidth: 0,
                            borderWidth: 0.5,
                            borderColor: color.black1,
                            marginLeft: 0,
                            paddingLeft: 4,
                            alignItems: "flex-start"
                          }
                          // ... You can check the source to find the other keys.
                        }}
                        onDateChange={date => {
                          _updateATo(date);
                        }}
                      />
                    </View>
                  </View>

                  <View style={styles.daysBox}>
                    <View>
                      <Text style={{ fontSize: 18 }}>
                        <Icon
                          name="calendar-check-o"
                          type="FontAwesome"
                          style={{ fontSize: 22 }}
                        />{" "}
                        Exclude Days
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: hp("3%"),
                          alignItems: "center",
                          justifyContent: "space-between"
                        }}
                      >
                        {_checkboxes.map((cb, i) => {
                          return (
                            <View style={{ width: wp("10%") }}>
                              <Text
                                style={{ fontSize: 12, textAlign: "center" }}
                              >
                                {cb.name}
                              </Text>

                              <CheckBox
                                checked={cb._checked}
                                color={color.blue}
                                onPress={() => this.toggleCheckbox(cb.id)}
                              />
                            </View>
                          );
                        })}
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </Tab>
            {this.props.selectedType.id == 3 && (
              <Tab
                style={styles.tab}
                heading="Seat Details"
                tabStyle={{ backgroundColor: "white" }}
                textStyle={{ color: "orange" }}
                activeTabStyle={{
                  backgroundColor: "transparent"
                }}
                activeTextStyle={{
                  color: "#5067FF",
                  fontWeight: "normal"
                }}
              >
                <View key={123} style={styles.tab1}>
                  <View style={styles.container}>
                    <View
                      style={{
                        flex: 1,
                        paddingHorizontal: 20,
                        paddingVertical: 10
                      }}
                    >
                      {this.state.addSeat.map((item, i) => {
                        return this.renderAddSeat(item, i);
                      })}
                    </View>
                  </View>
                </View>
              </Tab>
            )}

            {this.props.selectedType.id == 2 && (
              <Tab
                style={styles.tab}
                heading="Pax Details"
                tabStyle={{ backgroundColor: "white" }}
                textStyle={{ color: "orange" }}
                activeTabStyle={{
                  backgroundColor: "transparent"
                }}
                activeTextStyle={{
                  color: "#5067FF",
                  fontWeight: "normal"
                }}
              >
                <View style={styles.tab1}>
                  <View>
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        paddingHorizontal: 30,
                        paddingVertical: 30,
                        justifyContent: "flex-end",
                        alignItems: "flex-end"
                      }}
                    >
                      <Button
                        rounded
                        primary
                        onPress={() => this.setState({ showAddPaxForm: true })}
                      >
                        <Icon type="FontAwesome" name="plus" />
                      </Button>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        justifyContent: "space-evenly",
                        alignItems: "flex-start"
                      }}
                    >
                      {this.state.finalPax.length > 0 &&
                        this.state.finalPax.map((list, j) => {
                          return this._renderPaxList(list, j);
                        })}
                    </View>
                  </View>
                </View>
              </Tab>
            )}
          </Tabs>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-end",
              alignItems: "flex-end",
              paddingRight: 30,
              paddingTop: 10
            }}
          >
            <Button
              onPress={() => this.actionPressed()}
              style={styles.actionbtn}
            >
              <Text style={styles.actionbtntxt}>Submit</Text>
            </Button>
          </View>

          {this.state.showAddPaxForm && (
            <ModalComp
              titlecolor={color.orange}
              visible={this.state.showAddPaxForm}
              closemodal={() => this.updateCloseAddPaxForm()}
            >
              <View style={{ paddingVertical: 50 }}>
                <View
                  style={{
                    display: "flex",
                    justifyContent: "space-around",
                    alignItems: "stretch",
                    paddingLeft: 10,
                    paddingRight: 10
                  }}
                >
                  <View style={{ paddingVertical: 15, paddingHorizontal: 15 }}>
                    <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                      Pax Name
                    </Text>

                    <TextInput
                      value={this.state.addPax.paxname}
                      keyboardType="default"
                      onChangeText={data => {
                        this.setState(prevState => ({
                          addPax: Object.assign({}, prevState.addPax, {
                            paxname: data
                          })
                        }));
                      }}
                      style={{
                        borderColor: color.black1,
                        borderWidth: 1,
                        padding: 0,
                        paddingLeft: 5
                      }}
                      returnKeyType="next"
                      blurOnSubmit={false}
                    />

                    <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                      Minimum
                    </Text>

                    <TextInput
                      value={this.state.addPax.minimum.toString()}
                      keyboardType="numeric"
                      onChangeText={data =>
                        this.setState(prevState => ({
                          addPax: Object.assign({}, prevState.addPax, {
                            minimum: data ? parseInt(data) : 0
                          })
                        }))
                      }
                      style={{
                        borderColor: color.black1,
                        borderWidth: 1,
                        padding: 0,
                        paddingLeft: 5,
                        paddingRight: 10
                      }}
                      returnKeyType="next"
                      blurOnSubmit={false}
                    />

                    <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                      Maximum
                    </Text>

                    <TextInput
                      value={this.state.addPax.maximum.toString()}
                      keyboardType="numeric"
                      onChangeText={data =>
                        this.setState(prevState => ({
                          addPax: Object.assign({}, prevState.addPax, {
                            maximum: data ? parseInt(data) : 0
                          })
                        }))
                      }
                      style={{
                        borderColor: color.black1,
                        borderWidth: 1,
                        padding: 0,
                        paddingLeft: 5,
                        paddingRight: 10
                      }}
                      returnKeyType="next"
                      blurOnSubmit={false}
                    />
                  </View>

                  <View style={styles.select}>
                    {this._renderCurrency(currency)}
                  </View>

                  <View>
                  {this.state.pricetypeData.map((obj,index)=>{
                    return(
                      <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                      <Text
                      style={{
                        fontSize: 16,
                        paddingVertical: 3,
                        alignSelf: "center"
                      }}
                    >
                      {obj.day_type_name}
                    </Text>
                    <View style={{flexDirection:'row'}}>
                    <View style={{marginRight:15}}>
                      <Text
                        style={{
                          fontSize: 16,
                          paddingVertical: 3,
                          color: color.orange,
                          alignContent: "center"
                        }}
                      >
                        Adult
                      </Text> 
 

                      <TextInput
                        value={obj.adult.toString()}
                        keyboardType="numeric"
                        onChangeText={data => {
                          var pricetypeData = this.state.pricetypeData;
                          pricetypeData[index].adult =parseInt(data);
                          this.setState({ pricetypeData });
                          // console.log(this.state.addPax);
                        }}
                        style={{
                          borderColor: color.black1,
                          borderWidth: 1,
                          padding: 0,
                          paddingLeft: 5,
                          paddingRight: 10,
                          width: hp("10%")
                        }}
                        returnKeyType="next"
                        blurOnSubmit={false}
                      />
                    </View>
                    <View>
                      <Text
                        style={{
                          fontSize: 16,
                          paddingVertical: 3,
                          color: color.orange,
                          alignContent: "center"
                        }}
                      >
                        Child
                      </Text> 
 

                      <TextInput
                        value={obj.child.toString()}
                        keyboardType="numeric"
                        onChangeText={data => {
                         var pricetypeData = this.state.pricetypeData;
                          pricetypeData[index].child =parseInt(data);
                          this.setState({ pricetypeData });
                          console.log(this.state.addPax);
                        }}
                        style={{
                          borderColor: color.black1,
                          borderWidth: 1,
                          padding: 0,
                          paddingLeft: 5,
                          paddingRight: 10,
                          width: hp("10%")
                        }}
                        returnKeyType="next"
                        blurOnSubmit={false}
                      />
                    </View>
                    </View>
                    </View>
                      )
                  })}
                    

                    

                  


                    

                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "flex-end",
                      alignItems: "flex-end",
                      paddingRight: 30,
                      paddingTop: 10
                    }}
                  >
                    <Button
                      onPress={() => this.actionAddPaxPressed()}
                      style={styles.actionbtn}
                    >
                      <Text style={styles.actionbtntxt}>Submit</Text>
                    </Button>
                  </View>
                </View>
              </View>
            </ModalComp>
          )}
        </View>
      </Content>
    );
  }
}

export default addForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: "white"
  },
  tab: {
    backgroundColor: "#edeef2"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  },
  select: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingVertical: 10
  },
  days: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "flex-start",
    paddingVertical: 5
  },

  pickerBox: {
    flex: 1,
    backgroundColor: "#edeef2",
    paddingVertical: hp("4%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly"
  },
  daysBox: {
    backgroundColor: "#fff",
    paddingVertical: hp("2%"),
    paddingHorizontal: wp("5%")
  }
});
