import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Tabs, Tab, TabHeading,Content,Root } from 'native-base';
import { View, Image, StatusBar, Picker, Text, TouchableOpacity, ScrollView,ActivityIndicator , BackHandler,ToastAndroid} from 'react-native';
import color from '../Helpers/color';
import links from '../Helpers/config';
import DateFunctions from '../Helpers/DateFunctions'
import HeaderComp from '../components/HeaderComp';
import SubHeaderComp from '../components/subHeader';
import Triangle from '../components/triangles';
import VenueType from '../pages/venuetype';
import VenueSuccess from '../pages/VenueSuccess';
import FooterComp from '../components/Footer';
import FacilityType from '../pages/facilitytype';
import FacilityDetails from '../pages/facilitydetails';
import Availability from '../pages/availability';
import Ameneties from '../pages/ameneties';
import VenueTags from '../pages/venutags';
import VenuePrice from '../pages/venueprice';
import VenueUpload from '../pages/venueupload';
import VenueReview from '../pages/venuereveiw';
import { Actions } from 'react-native-router-flux';
import Chair from '../images/svg/chair.png';
import DeskF from '../images/svg/deskf.png';
import DeskM from '../images/svg/desk.png';
import Access from '../images/svg/accessibility.png';
import Laptop from '../images/svg/laptop.png';
import Employee from '../images/svg/employee.png';
import Seats from '../images/svg/seats.png';
import Bathroom from '../images/svg/bathroom.png';
import Parking from '../images/svg/parking.png';
import Coffee from '../images/svg/coffee-cup.png';
import Door from '../images/svg/door.png';
import Hourly from '../images/svg/Hourly.png';
import Daily from '../images/svg/Daily.png';
import Weekly from '../images/svg/Weekly.png';
import AmenitiesPNG from '../images/TabPNG/AmenitiesPNG.png';
import AvailabilityPNG from '../images/TabPNG/AvailabilityPNG.png';
import FacilityPNG from '../images/TabPNG/FacilityPNG.png';
import PricecardPNG from '../images/TabPNG/PricecardPNG.png';
import ReviewPNG from '../images/TabPNG/ReviewPNG.png';
import UploadphotoPNG from '../images/TabPNG/UploadphotoPNG.png';
import TaglistPNG from '../images/TabPNG/TaglistPNG.png';
import VenuetypePNG from '../images/TabPNG/VenuetypePNG.png';
import Calendar from '../images/svg/calendar.png';
import AsyncStorage from '@react-native-community/async-storage';
import DynamicPagination from '../components/DynamicPagination';
import Toast from 'react-native-simple-toast';
import dateFormat from 'dateformat';
import moment from 'moment';
import LoadAPIPost from '../Helpers/xhrapi';
import CircularProgressBar1 from '../components/circularprogressbar1';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const currencyList = [
  {
    id: 1,
    name: "USD"
  },
  {
    id: 2,
    name: "INR"
  },
  {
    id: 3,
    name: "EURO"
  }
];


var checkboxesexclude= [
        {
          id: 0,
          name: "SUN",
          _checked: false
        },
        {
          id: 1,
          name: "MON",
          _checked: false
        },
        {
          id: 2,
          name: "TUE",
          _checked: false
        },
        {
          id: 3,
          name: "WED",
          _checked: false
        },
        {
          id: 4,
          name: "THU",
          _checked: false
        },
        {
          id: 5,
          name: "FRI",
          _checked: false
        },
        {
          id: 6,
          name: "SAT",
          _checked: false
        }
      ];
var datearray=[{id:1,name:'USD'},{id:2,name:'INR'},{id:3,name:'MYR'}]
var datasHourlyWeekly=[{name:'Hourly',id:1,keyname:'Hour'},{name:'Daily',id:2,keyname:'Day'},{name:'Weekly',id:3,keyname:'Week'},{name:'Monthly',id:4,keyname:'Month'}];

const datas = [
  { name: "Seating", id: 1, circleCount: null, active: false, icon: Chair },
  { name: "Physical\nInfrastructure", id: 2, icon: DeskF },
  { name: "Accessibility", id: 3, icon: Access },
  { name: "Training Equipments", id: 4, icon: Laptop },
  { name: "IT Infra", id: 5, icon: DeskF },
  { name: "Resource", id: 6, circleCount: null, active: false, icon: Seats },
  { name: "Toilets", id: 7, icon: Bathroom },
  { name: "Parking", id: 8, icon: Parking },
  { name: "Pantry", id: 9, icon: Coffee },
  { name: "Additional Rooms", id: 10, icon: Door }
];

datas.push({ name: "Hourly", id: 1, activeId: 1 });
datas.push({ name: "Hourly", id: 1, activeId: 1 });
const newtags = [
  {
    id: 1,
    name: "Add (or) Tag the related General Keywords",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false },
      { id: 4, name: "Special Tag Items display here", state: false },
      { id: 5, name: "Corporate Trainings", state: false }
    ]
  },
  {
    id: 2,
    name: "What category on venue type",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false },
      { id: 4, name: "Special Tag Items display here", state: false }
    ]
  },
  {
    id: 3,
    name: "Suitable Training- IT / Soft Skill / Handson",
    tags: [
      { id: 1, name: "Corporate Trainings", state: false },
      { id: 2, name: "Course Trainings", state: false },
      { id: 3, name: "Venue Special", state: false }
    ]
  }
];
     // const newtags1=[{id:1,name:'Add (or) Tag the related General Keywords',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:2,name:'What category on venue type',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:3,name:'Suitable Training- IT / Soft Skill / Handson',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]}]
     var commonDataForFacility = [
       {
         spec_det_id: "idname",
         venue_spec_id: 1,
         spec_det_name: "Venue Name",
         spec_det_sortorder: 0,
         spec_det_datatype1: "text",
         spec_det_datavalue1: "",
         spec_det_datatype2: "",
         spec_det_datavalue2: "",
         spec_det_datatype3: "",
         spec_det_datavalue3: "",
         validation: [{ name: "required" }, { name: "minLength", params: 30 }],
         error: true,
         errormsg: ""
       },
       {
         spec_det_id: "idname2",
         venue_spec_id: 1,
         spec_det_name: "Venue Address",
         spec_det_sortorder: 0,
         spec_det_datatype1: "textareamap",
         spec_det_datavalue1: "",
         spec_det_datatype2: "",
         spec_det_datavalue2: "",
         spec_det_datatype3: "",
         spec_det_datavalue3: "",
         validation: [{ name: "required" }],
         error: true,
         errormsg: ""
       },
       {
         spec_det_id: "idname3",
         venue_spec_id: 1,
         spec_det_name: "Venue Area",
         spec_det_sortorder: 0,
         spec_det_datatype1: "text",
         spec_det_datavalue1: "",
         spec_det_datatype2: "",
         spec_det_datavalue2: "",
         spec_det_datatype3: "",
         spec_det_datavalue3: "",
         validation: [{ name: "required" }],
         error: true,
         errormsg: ""
       },
       {
         spec_det_id: "idname4",
         venue_spec_id: 1,
         spec_det_name: "Venue Landmark",
         spec_det_sortorder: 0,
         spec_det_datatype1: "text",
         spec_det_datavalue1: "",
         spec_det_datatype2: "",
         spec_det_datavalue2: "",
         spec_det_datatype3: "",
         spec_det_datavalue3: "",
         validation: [{ name: "required" }, { name: "minLength", params: 300 }],
         error: true,
         errormsg: ""
       },
       {
         spec_det_id: "idname5",
         venue_spec_id: 1,
         spec_det_name: "Venue Description",
         spec_det_sortorder: 0,
         spec_det_datatype1: "textarea",
         spec_det_datavalue1: "",
         spec_det_datatype2: "",
         spec_det_datavalue2: "",
         spec_det_datatype3: "",
         spec_det_datavalue3: "",
         validation: [
           { name: "required" },
           { name: "minLength", params: 1000 }
         ],
         error: true,
         errormsg: ""
       }
     ];
export default class VenueForm extends Component {
                 constructor(props) {
                   super(props);
                   // Actions.corporateform();
                   const venueform1 = JSON.parse(
                     JSON.stringify(require("../Helpers/venueform.json"))
                   );
                   this.state = {
                     autosave: true,
                     init: true,
                     list1: null,
                     splitData: null,
                     availabilityState: null,
                     purposeData: null,
                     showSkip: null,
                     loginDetails: this.props.navigation.state.params.logindata,
                     facilityvalidations: null,
                     facilityIndex: 0,
                     currentIndex: 0,
                     facitlityactive: null,
                     activeparent: "tab1",
                     venuetypetext: "",
                     venueheadertext: "",
                     edit: false,
                     list: [
                       { id: "1", name: "tab1", image: VenuetypePNG },
                       { id: "2", name: "tab2", image: FacilityPNG },
                       { id: "3", name: "tab3", image: AvailabilityPNG },
                       { id: "4", name: "tab4", image: AmenitiesPNG },
                       { id: "5", name: "tab5", image: TaglistPNG },
                       { id: "6", name: "tab6", image: PricecardPNG },
                       { id: "7", name: "tab7", image: UploadphotoPNG },
                       { id: "8", name: "tab8", image: ReviewPNG }
                     ],
                     _scrollToBottomY: 0,
                     facilityData: {
                       roomname: "",
                       seats: "",
                       floor: "",
                       address: "",
                       landmark: "",
                       mobile: "",
                       mail: ""
                     },
                     venuetypedata: null,
                     facilitytypedata: null,
                     availability: {
                       type: null,
                       days: null,
                       from: null,
                       to: null,
                       moredetails: null,
                       slots: [],
                       avail_type: "",
                       paxContent: [],
                       seatList: [],
                       businessForm: null
                     },
                     ameneties: null,
                     venTags: null,
                     venueprice: null,
                     venueform: venueform1,
                     venueformData: new FormData(),
                     validations: [
                       {
                         id: 1,
                         name: "venuetype",
                         errmsg: "Choose Venue Type",
                         state: false
                       },
                       {
                         id: 2,
                         name: "venuetype",
                         errmsg: "Choose Venue Facility",
                         state: false
                       },
                       {
                         id: 3,
                         name: "facility",
                         errmsg: "Please Fill up facility details",
                         state: false
                       },
                       {
                         id: 4,
                         name: "availability",
                         errmsg: "Please Fill up avaialability details",
                         state: false
                       },
                       {
                         id: 5,
                         name: "price",
                         errmsg: "Please Fill up price Details",
                         state: false
                       },
                       {
                         id: 6,
                         name: "purposes",
                         errmsg: "Please Choose purposes and actions",
                         state: false
                       }
                     ],
                     priceDataList: null,
                     excludeDaysList: JSON.parse(
                       JSON.stringify(checkboxesexclude)
                     ),
                     progresscount: 0
                   };
                   if (this.props.navigation.state.params.welcomemsg) {
                     Toast.show(
                       this.props.navigation.state.params.welcomemsg,
                       Toast.LONG
                     );
                   }
                 }
                 goToRight = () => {
                   // this.scroll.scrollTo({ x: 0, y: 0, animated: true });
                 };
                 findExcludeDays = data => {
                   var filterData = [];
                   const excludeDaysList = this.state.excludeDaysList;
                   if (data && data.length > 0) {
                     var excludedays = data.split(",");
                     // alert(JSON.stringify(data));
                     excludedays.map(obj => {
                       excludeDaysList.map(obj2 => {
                         var obj3 = obj2;
                         obj3.id == obj ? (obj3._checked = true) : false;
                         return obj3;
                       });
                       // if(filteredrecords.length>0){
                       //   filterData.push(filteredrecords[0]);
                       // }
                     });
                     // alert(JSON.stringify(excludeDaysList))
                     return excludeDaysList;
                   } else {
                     return excludeDaysList;
                   }
                 };
                 changeTab = data => {
                   // alert(data);
                   var list = this.state.list;
                   // alert(JSON.stringify(list));
                   var filterrecords = list.filter(obj => obj.id == data + 1);

                   this.setState({ currentIndex: data });
                   this.setState({ activeparent: filterrecords[0].name });
                   // if(data=='tab2'){
                   //     this.setState({facitlityactive:1})
                   // }else{
                   //     this.setState({facitlityactive:null})

                   // }
                 };
                 autosaveData = (data, upload, newimagesData) => {
                   // alert(JSON.stringify(newimagesData));
                   // return;
                   var self = this;
                   const field = data.getParts();
                   // alert(JSON.stringify(field));
                   // return;
                   var venueformData = this.state.venueformData;
                   if (upload) {
                     this.setState({ loading: true });
                   }
                   LoadAPIPost.LoadApi(
                     data,
                     links.APIURL + "edit_venue_autosave",
                     upload,
                     function(err, success) {
                       if (upload) {
                         venueformData.set("formArray", null);
                         self.setState({ loading: false });
                       }
                       if (err) {
                         Toast.show("serverError", Toast.LONG);
                       } else {
                         var respjson = JSON.parse(success);
                         var venueform = self.state.venueform;
                         if (respjson.status == 0) {
                           if (newimagesData && upload) {
                             var newmodifiedimages = [];
                             if (Array.isArray(newimagesData) == true) {
                               newimagesData.push({
                                 name: respjson.existImage
                               });
                               newmodifiedimages = newimagesData;
                             } else {
                               newmodifiedimages = newimagesData;
                             }
                             venueformData.set(
                               "existImages",
                               JSON.stringify(newmodifiedimages)
                             );
                             // alert(respjson.existImage);
                           }
                           if (respjson.venueId) {
                             // alert(respjson.respjson);
                             // alert(respjson.venueId);
                             venueform.venue.venue_id = respjson.venueId;
                             self.setState({ venueform });
                             venueformData.set(
                               "venue",
                               JSON.stringify(venueform.venue)
                             );
                           }
                         }
                       }
                     },
                     function(progress) {
                       if (upload) {
                         self.setState({ progresscount: progress });
                       }
                     }
                   );
                   // if(err){
                   //   console.log("error",err);
                   // }else{
                   //   var respjson=JSON.parse(data);
                   //   var listingjson=self.state.listingjson;
                   //   if(respjson.status==0){
                   //     if(respjson.venueId){
                   //       // alert(respjson.respjson);
                   //     // console.log("responsesuccess",respjson.venueId);
                   //       listingjson.venue.venue_id=respjson.venueId
                   //       self.setState({listingjson});
                   //       formData.set('venue',JSON.stringify(listingjson.venue));
                   //     }
                   //   }

                   // }
                   // })
                 };
                 async componentWillMount() {
                   // alert(JSON.stringify(this.props))
                   const data = await AsyncStorage.getItem("loginDetails");
                   var parsedata = JSON.parse(data);
                   this.setState({ loginDetails: parsedata });
                   var venueform = this.state.venueform;
                   venueform.venue.user_id = parsedata.user_id;
                   // var venueformdata=this.state.venuef
                   // var venueformData=this.state.venueformData;

                   this.setState({ venueform });

                   this.setState({ facilityIndex: 0, facitlityactive: 1 });

                   // console.log(data);
                   // console.log("asynstore",AsyncStorage.getItem('@loginDetails'));
                 }
                 componentDidMount() {
                   // console.log(this.props.navigation.state.params.editData);
                   if (this.props.navigation.state.params.editData) {
                     this.setState({ edit: true });
                     // alert("");

                     var editData = this.props.navigation.state.params.editData;
                     var venueform = this.state.venueform;
                     if (editData.autoSave) {
                       this.setState({ autosave: true });
                       venueform.venue.venue_id =
                         editData.venue_autosave_venue_id;
                     } else {
                       this.setState({ autosave: false });
                       venueform.venue.venue_id = editData.venue_id;
                     }
                     //load Venue category
                     this.sendvenuetypedata(
                       {
                         venue_cat_id: editData.trn_venue_cat_id,
                         venue_cat_name: editData.venue_cat_name
                       },
                       "dontupdate"
                     );
                     //load specDetails
                     venueform.venue.venue_cat_id = editData.trn_venue_cat_id;
                     venueform.venue.venue_spec_id = editData.venue_spec_id;
                     this.sendfacilitytypedata(
                       {
                         venue_spec_id: editData.venue_spec_id,
                         venue_spec_name: editData.venue_spec_name
                       },
                       "dontupdate"
                     );
                     //load specificDetails
                     var commonData = JSON.parse(
                       JSON.stringify(commonDataForFacility)
                     );
                     commonData[0].spec_det_datavalue1 =
                       editData.trn_venue_name;
                     commonData[1].spec_det_datavalue1 =
                       editData.trn_venue_address;
                     commonData[1].spec_det_datavalue2 =
                       editData.trn_venue_location;
                     commonData[2].spec_det_datavalue1 =
                       editData.trn_venue_area;
                     commonData[3].spec_det_datavalue1 =
                       editData.trn_venue_landmark;
                     commonData[4].spec_det_datavalue1 =
                       editData.trn_venue_desc;
                     // this.setState({commonArray});
                     this.sendfaciltiydata(
                       {
                         venue_spec_id: editData.venue_spec_id,
                         venue_spec_name: editData.venue_spec_name,
                         specDetails: editData.specdetails
                       },
                       commonData,
                       true,
                       "dontupdate"
                     );
                     //load venue photos
                     var myphotos = editData.photos.map(obj => {
                       return {
                         uid: obj.venut_photo_id
                           ? obj.venut_photo_id
                           : obj.venut_photo_approval_id,
                         name: obj.venue_image_path.split("/")[
                           obj.venue_image_path.split("/").length - 1
                         ],
                         status: "done",
                         url: obj.venue_image_path,
                         exist: true
                       };
                     });
                     // this.setState({formimagearray:null,})
                     venueform.formArray = myphotos;
                     var venueformData = this.state.venueformData;
                     venueformData.set("existImages", JSON.stringify(myphotos));

                     // alert(editData.venue_id);

                     this.setState({ venueform });
                     //load ameneties
                     var amenetiesArray1 = JSON.parse(
                       JSON.stringify(editData.ameneties)
                     );
                     amenetiesArray1.map(
                       v => (v.amenities_array = v.amnDetails)
                     );

                     amenetiesArray1.map(
                       v => (v.active = v.amnDetails.length > 0 ? true : false)
                     );
                     amenetiesArray1.map(
                       v =>
                         (v.circleCount =
                           v.amnDetails.length > 0 ? v.amnDetails.length : 0)
                     );
                     amenetiesArray1.map(v =>
                       v.amenities_array.map(j => (j.statekey = true))
                     );
                     amenetiesArray1.map(
                       v =>
                         (v.activeArray = v.amenities_array.map(obj => {
                           return {
                             venue_amnts_id: obj.amenities_det_id,
                             venue_amnts_det_datatype1:
                               obj.amenities_det_datatype1,
                             venue_amnts_det_datatype2:
                               obj.amenities_det_datatype2,
                             venue_amnts_det_datatype3:
                               obj.amenities_det_datatype3,
                             venue_amnts_det_datavalue1:
                               obj.amenities_det_datavalue1,
                             venue_amnts_det_datavalue2:
                               obj.amenities_det_datavalue2,
                             venue_amnts_det_datavalue3:
                               obj.amenities_det_datavalue3,
                             statekey: true
                           };
                         }))
                     );

                     this.AmenetiesData(amenetiesArray1, "dontupdate");

                     //binding availabil
                     if (editData.availability.length > 0) {
                       var splittedDataFrom = editData.availability[0].trn_venue_avail_frm.split(
                         " "
                       );
                       var splittedDataTo = editData.availability[0].trn_venue_avail_to.split(
                         " "
                       );
                       var availobj = {
                         businessForm: {
                           availableFrom: splittedDataFrom[0],
                           availableTo: splittedDataTo[0],
                           from: DateFunctions.converttime24to12(
                             splittedDataFrom[1]
                           ),
                           to: DateFunctions.converttime24to12(
                             splittedDataTo[1]
                           ),
                           venue_type: editData.trn_venue_type,
                           checkboxes: this.findExcludeDays(
                             editData.availability[0].trn_venue_exclude_days
                           ),
                           slot_type: 1
                         },
                         splitData: [],
                         seatList: [],
                         paxContent: [],
                         avail_type:
                           editData.availability[0].trn_availability_type
                       }; 
                       if (
                         editData.trn_venue_type != 2 &&
                         editData.availability[0].hourlyData.length > 0 &&
                         editData.availability[0].hourlyData[0]
                           .venue_slot_type == 2
                       ) {
                         editData.availability[0].hourlyData.map(
                           (objData, i) => {
                             availobj.businessForm.slot_type =
                               objData.venue_slot_type;
                             var obj = {
                               id: objData.venue_slot_type_uniq_id,
                               slots_name: objData.venue_slot_name,
                               value: []
                             };
                             objData.hourlySlots.map((slotobj, j) => {
                               var fromTime =
                                 moment().format("YYYY-MM-DD") +
                                 "T" +
                                 slotobj.venue_slot_start_time;
                               var toTime =
                                 moment().format("YYYY-MM-DD") +
                                 "T" +
                                 slotobj.venue_slot_end_time;
                               var objtimeData = {
                                 id: slotobj.venue_slot_type_uniq_id,
                                 label:
                                   DateFunctions.converttime24to12(
                                     slotobj.venue_slot_start_time
                                   ) +
                                   " - " +
                                   DateFunctions.converttime24to12(
                                     slotobj.venue_slot_end_time
                                   ),
                                 dateobj: { fromTime: fromTime, toTime: toTime }
                               };
                               obj.value.push(objtimeData);
                             });
                             availobj.splitData.push(obj);
                           }
                         );
                       }
                       if (editData.trn_venue_type == 2) {
                         var objListing = editData.price.map(data => {
                           // data.paxDesc=data.details;
                           var objData = {
                             paxname: data.venue_pax_name,
                             minimum: data.pax_min,
                             maximum: data.pax_max,
                             details: data.venue_pax_desc,
                             priceDetails: [],
                             uniqueId: data.pax_unique_id,
                             paxDesc: "",
                             selectedCurrency: currencyList[0]
                           };
                           objData.priceDetails = data.priceDetails.map(obj => {
                             // return {}
                             var selectedfiltercurrency = currencyList.filter(
                               obj1 => obj1.name == obj.currency
                             );
                             objData.selectedCurrency =
                               selectedfiltercurrency.length > 0
                                 ? selectedfiltercurrency[0]
                                 : currencyList[0];
                             var currentobj = {
                               day_type: obj.day_type,
                               adult: obj.Adult,
                               child: obj.Child,
                               day_type_code: obj.day_type_code
                             };
                             return currentobj;
                           });
                           return objData;
                         });
                         availobj.paxContent = objListing;
                       }

                       if (editData.trn_venue_type == 3) {
                         var seatListArray = editData.price;
                         var editSeatArrayList =
                           seatListArray.length > 0
                             ? seatListArray.map(obj => {
                                 return {
                                   name: obj.seat_name,
                                   value: obj.seat_qty,
                                   pricing:
                                     obj.priceDetails.length > 0
                                       ? [
                                           {
                                             currency:
                                               obj.priceDetails[0].currency,
                                             hourCost:
                                               obj.priceDetails[0].hour_cost,
                                             dayCost:
                                               obj.priceDetails[0].day_cost,
                                             weekCost:
                                               obj.priceDetails[0].week_cost,
                                             monthCost:
                                               obj.priceDetails[0].month_cost
                                           }
                                         ]
                                       : [],
                                   uniqueId:
                                     obj.seat_unique_id == "undefined" ||
                                     obj.seat_unique_id == undefined ||
                                     !obj.seat_unique_id
                                       ? DateFunctions.generateTimestamp()
                                       : obj.seat_unique_id
                                 };
                               })
                             : [];
                         // alert(JSON.stringify(editSeatArrayList));
                         availobj.seatList = editSeatArrayList;
                         //     var objListing=editData.price.map((data)=>{
                         //   // data.paxDesc=data.details;
                         //   var objData={"paxname":data.venue_pax_name,"minimum":data.pax_min,"maximum":data.pax_max,'details':data.venue_pax_desc,priceDetails:[],uniqueId:data.pax_unique_id,paxDesc:'',selectedCurrency:currencyList[0]};
                         //   objData.priceDetails=data.priceDetails.map((obj)=>{
                         //     // return {}
                         //     var selectedfiltercurrency=currencyList.filter((obj1)=>obj1.name==obj.currency);
                         //     objData.selectedCurrency=selectedfiltercurrency.length>0?selectedfiltercurrency[0]:currencyList[0];
                         //     var currentobj={day_type:obj.day_type,adult:obj.Adult,child:obj.Child,day_type_code:obj.day_type_code};
                         //     return currentobj;
                         //   })
                         //   return objData;
                         // });
                         //       availobj.paxContent=objListing;
                       }
                       this.getSlots(availobj, "dontupdate");
                       // alert(JSON.stringify(availobj));
                       // alert(JSON.stringify(availobj));
                     }
                     // this.fromtodays({from:editData.availability[0].trn_venue_avail_frm,to:editData.availability[0].trn_venue_avail_to})
                     //binding avail circle data
                     // this.availcircleData({id:editData.availability[0].trn_availability_type})
                     //binding Purposes
                     var purposeGetIndexDatas = JSON.parse(
                       JSON.stringify(editData.purpose)
                     );
                     var getFilterRecords = purposeGetIndexDatas.filter(
                       obj => obj.purpose.length > 0
                     );
                     var purposeeditdata = {
                       purposes:
                         getFilterRecords.length > 0
                           ? getFilterRecords.map(obj => {
                               return {
                                 id: obj.venue_act_id,
                                 value: obj.purpose.map(
                                   obj => obj.venue_purpose_id
                                 )
                               };
                             })
                           : [],
                       purposeDropdown: editData.purpose
                     };
                     this.sendPurposes(
                       purposeeditdata.purposes,
                       purposeeditdata.purposeDropdown,
                       1,
                       "dontupdate"
                     );
                     //binding Tags
                     var mytags = JSON.parse(JSON.stringify(editData.tags));
                     // console.log(mytags.length);
                     // mytags.map((obj)=>obj.tag_cat_desc=obj.tag_cat_name)
                     // console.log(mytags);
                     this.saveTags(mytags, mytags, "dontupdate");
                     //binding price
                     if (
                       editData.price.length > 0 &&
                       editData.trn_venue_type != 2 &&
                       editData.trn_venue_type != 3
                     ) {
                       var priceobjData = editData.price[0];
                       // var filtercurreny=datearray.filter((obj)=>obj.name==editData.trn_venue_price_currency);
                       var priceData = {
                         name: "",
                         pricing: [
                           {
                             hourCost: priceobjData.hour_cost,
                             dayCost: priceobjData.day_cost,
                             weekCost: priceobjData.week_cost,
                             monthCost: priceobjData.month_cost,
                             currency: priceobjData.trn_venue_price_currency
                           }
                         ]
                       };
                       // this.setState({priceDataList})
                       var sendpricedData = {
                         venuetype: editData.trn_venue_type,
                         seatList: [priceData],
                         currency: priceobjData.trn_venue_price_currency
                       };
                       this.sendPriceData(sendpricedData, "dontupdate");
                     }
                     if (this.state.autosave == true && editData.autoSave) {
                       var arraytogonext = [
                         "tab1",
                         "tab2",
                         "tab3",
                         "tab4",
                         "tab5",
                         "tab6",
                         "tab7"
                       ];
                       var self = this;
                       setTimeout(() => {
                         self.setState({
                           currentIndex: editData.trn_venue_lastactivity - 1,
                           activeparent:
                             arraytogonext[
                               parseInt(editData.trn_venue_lastactivity) - 1
                             ]
                         });
                       }, 500);
                     }
                     // var priceData=editData.price[0];
                     // var filtertypename=datasHourlyWeekly.filter((obj)=>obj.id==priceData.trn_venue_price_type);
                     // var priceobj={amt:priceData.trn_venue_price_amt,activeId:priceData.trn_venue_price_type,activeObj:filtercurreny.length>0?filtercurreny[0]:null,activeCircleObj:filtertypename.length>0?filtertypename[0]:null,days:priceData.trn_venue_price_currency,type:filtertypename.length>0?filtertypename[0].name:null}
                     // alert(JSON.stringify(priceobj));
                     // this.sendPriceData(priceobj);
                   }
                   BackHandler.addEventListener(
                     "hardwareBackPress",
                     this.handleBackButton
                   );
                 }

                 componentWillUnmount() {
                   BackHandler.removeEventListener(
                     "hardwareBackPress",
                     this.handleBackButton
                   );
                 }

                 handleBackButton = () => {
                   if (!this.state.loading) {
                     // Actions.pop();
                   } else {
                     return true;
                   }
                   // ToastAndroid.show('Back button is disabled', ToastAndroid.SHORT);
                 };

                 getmoredetails = data => {
                   var availability = this.state.availability;
                   availability.moredetails = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_moredetails = data;
                   this.setState({ venueform });
                 };

                 getBusinessForm = (data, totalState) => {
                   console.log("businessstae", totalState);

                   this.setState({
                     availabilityState: totalState
                   });
                   var availability = this.state.availability;
                   availability.businessForm = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.businessForm = data;
                   this.setState({ venueform });

                   console.log("avaiala", this.state.availability);
                 };

                 getSlots = (data, dontupdate) => {
                   // alert(JSON.stringify(data.businessForm))

                   // alert(data.venue_type);
                   var availability = this.state.availability;
                   // alert(JSON.stringify(data.paxContent));
                   // return;
                   availability.slots =
                     data.splitData.length > 0
                       ? data.splitData.map(obj => {
                           var obj1 = obj;
                           obj1.value.map(obj2 => {
                             var obj3 = obj2;
                             obj3.checked = true;
                             return obj3;
                           });
                           return obj1;
                         })
                       : [];
                   availability.businessForm = data.businessForm;
                   availability.paxContent = data.paxContent;
                   availability.seatList = data.seatList;
                   availability.allSlots = data.allSlots;
                   availability.avail_type = data.avail_type;
                   // alert(JSON.stringify(availability));
                   this.setState({ availability });

                   var venueform = this.state.venueform;
                   var venuetype = data.businessForm.venue_type;
                   venueform.venue.venue_type = venuetype;
                   venueform.availability.venue_avail_frm =
                     data.businessForm.availableFrom +
                     " " +
                     DateFunctions.converttime24Convertor(
                       data.businessForm.from
                     );
                   venueform.availability.venue_avail_to =
                     data.businessForm.availableTo +
                     " " +
                     DateFunctions.converttime24Convertor(data.businessForm.to);
                   const excludeDaysData =
                     data.businessForm.checkboxes.length > 0
                       ? data.businessForm.checkboxes.filter(
                           obj2 => obj2._checked == true
                         ).length > 0
                         ? data.businessForm.checkboxes
                             .filter(obj2 => obj2._checked == true)
                             .map(obj => obj.id)
                             .join(",")
                         : ""
                       : "";
                   venueform.availability.venue_exclude_days = excludeDaysData;
                   venueform.availability.venue_avail_type = data.avail_type;
                   if (data.businessForm.slot_type == 1) {
                     var obj = {
                       slot_type: 1,
                       slot_type_uniq_id: new Date().getTime(),
                       slot_name: "",
                       slot: [
                         {
                           startTime: DateFunctions.converttime24Convertor(
                             data.businessForm.from
                           ),
                           endTime: DateFunctions.converttime24Convertor(
                             data.businessForm.to
                           )
                         }
                       ]
                     };
                     // alert(JSON.stringify(obj));
                     venueform.hourlyAvailability = [obj];
                   } else {
                     // alert(JSON.stringify(data.splitData));
                     var splitArray = [];
                     data.splitData.map((objData, i) => {
                       var obj = {
                         slot_type: 2,
                         slot_type_uniq_id: data.splitData[i].id,
                         slot_name: data.splitData[i].slots_name,
                         slot: []
                       };
                       objData.value.map((slotobj, j) => {
                         obj.slot.push({
                           startTime: moment(slotobj.dateobj.fromTime).format(
                             "HH:mm:00"
                           ),
                           endTime: moment(slotobj.dateobj.toTime).format(
                             "HH:mm:00"
                           )
                         });
                       });
                       splitArray.push(obj);
                     });
                     venueform.hourlyAvailability = splitArray;
                   }
                   if (data.businessForm.venue_type == 2) {
                     var paxListing = data.paxContent;
                     venueform.hourlyAvailability = [];
                     var objListing = paxListing.map(data => {
                       // data.paxDesc=data.details;
                       var objData = {
                         paxName: data.paxname,
                         paxMin: data.minimum,
                         paxMax: data.maximum,
                         paxDesc: data.details,
                         paxPricing: [],
                         uniqueId: data.uniqueId,
                         paxDesc: ""
                       };
                       objData.paxPricing = data.priceDetails.map(obj => {
                         // return {}
                         var currentobj = {
                           currency: data.selectedCurrency.name,
                           dayType: obj.day_type,
                           adult: obj.adult,
                           child: obj.child,
                           code: obj.day_type_code
                         };
                         return currentobj;
                       });
                       return objData;
                     });
                     venueform.packsDetails = objListing;
                     // alert(JSON.stringify(objListing));
                   } else {
                     venueform.packsDetails = [];
                   }
                   if (data.businessForm.venue_type == 3) {
                     var seatListArray = data.seatList.filter(
                       obj => obj.add != "true"
                     );
                     venueform.seatDetails =
                       seatListArray.length > 0
                         ? seatListArray.map(obj => {
                             return {
                               seatName: obj.name,
                               seatQuantity: obj.value,
                               seatPricing: obj.pricing ? obj.pricing : [],
                               uniqueId: obj.uniqueId,
                               priceType: data.avail_type
                             };
                           })
                         : [];
                     // data.busin
                     // alert(JSON.stringify(seatListArray));
                     // venueform.seatDetails=seatListArray;
                     var checkseatListArrayHavingAdd = data.seatList.filter(
                       obj => obj.add == "true"
                     );
                     // alert(JSON.stringify(data.seatList));
                     if (checkseatListArrayHavingAdd.length == 0) {
                       var data1 = data;
                       var seatListArrayAdd = [
                         {
                           name: "",
                           value: 0,
                           add: "true",
                           uniqueId: DateFunctions.generateTimestamp()
                         }
                       ].concat(data.seatList.filter(obj => obj.add != "true"));
                       // alert(JSON.stringify(seatListArrayAdd));
                       availability.seatList = seatListArrayAdd;
                       this.setState({ availability });
                       // this.setState({availobj:data1});
                     }
                   }
                   // alert(JSON.stringify())
                   var validations = this.state.validations;
                   validations[3].state = true;
                   var venueformData = this.state.venueformData;
                   if (!dontupdate && this.state.autosave == true) {
                     venueform.keytoupdate = [
                       "VENUE_DET",
                       "VENUE_AMNS",
                       "VENUE_AVAIL"
                     ];
                     venueform.venue.lastActivityTab = 4;
                     venueformData.set(
                       "hourlyAvailability",
                       JSON.stringify(venueform.hourlyAvailability)
                     );
                     venueformData.set(
                       "venue",
                       JSON.stringify(venueform.venue)
                     );
                     venueformData.set(
                       "ameneties",
                       JSON.stringify(venueform.ameneties)
                     );
                     venueformData.set(
                       "availability",
                       JSON.stringify(venueform.availability)
                     );
                     venueformData.set(
                       "packsDetails",
                       JSON.stringify(venueform.packsDetails)
                     );
                     venueformData.set(
                       "seatDetails",
                       JSON.stringify(venueform.seatDetails)
                     );
                     venueformData.set(
                       "keytoupdate",
                       JSON.stringify(venueform.keytoupdate)
                     );
                   }
                   if (this.state.autosave == true && !dontupdate) {
                     this.autosaveData(venueformData);
                   }
                   this.setState({ validations, venueform });
                   this.gotoNext();
                 };

                 renderActiveTab() {
                   return <View style={styles.activeparent}></View>;
                 }
                 changeLabelText = data => {
                   this.setState({ venuetypetext: data.labeltext });
                 };
                 changeHeaderText = data => {
                   this.setState({ venueheadertext: data.headertext });
                   this.setState({
                     showSkip: data.showskip ? data.showskip : null
                   });
                 };
                 sendfaciltiydata = (data, data1, error, dontupdate) => {
                 
                   if (this.state.init) this.setState({ init: false });

                   if (this.state.edit) this.setState({ edit: false });

                   this.setState({ facilitytypedata: data });
                   this.setState({ commonArray: data1 });
                   var venueform = this.state.venueform;
                   console.log("start");
                   venueform.venue.venue_name = data1[0].spec_det_datavalue1;
                   venueform.venue.venue_address = data1[1].spec_det_datavalue1;
                   venueform.venue.venue_location =
                     data1[1].spec_det_datavalue2;
                   venueform.venue.venue_area = data1[2].spec_det_datavalue1;
                   venueform.venue.venue_landmark =
                     data1[3].spec_det_datavalue1;
                   venueform.venue.venue_desc = data1[4].spec_det_datavalue1;
                   venueform.specific = data.specDetails;
                   console.log("half");
                   if (error != null) {
                     var validations = this.state.validations;
                     validations[2].state = error;
                     this.setState({ validations });
                   }
                   venueform.keytoupdate = ["VENUE_DET"]; //autosave
                   venueform.venue.lastActivityTab = 2;
                   var venueformData = this.state.venueformData;
                   venueformData.set("venue", JSON.stringify(venueform.venue));
                   venueformData.set(
                     "specific",
                     JSON.stringify(venueform.specific)
                   );
                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );
                  
                   
                   this.setState({ venueform });

                   console.log("ended");
                 }; 



                 sendvenuetypedata = data => {
                   this.setState({ venuetypedata: data });
                   var validations = this.state.validations;
                   validations[0].state = true;
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.venue.venue_cat_id = data.venue_cat_id;
                   // venueform.venue.venuename=data.venue_cat_name;
                   this.setState({ venueform }, () => {
                     this.gotoNext();
                   });
                 }; 


                 sendfacilitytypedata = (data, dontupdate) => {
                   // this.setState({commonArray:null});
                   this.setState({ facilitytypedata: data });
                   var venueform = this.state.venueform;
                   var validations = this.state.validations;
                   validations[1].state = true;
                   this.setState({ validations });
                   // if(this.state.autosave==true){
                   // var listingjson=this.state.venueform;
                   venueform.keytoupdate = ["VENUE_DET"]; //autosave
                   venueform.venue.lastActivityTab = 2;
                   if (venueform.venue.venue_spec_id != data.venue_spec_id) {
                     venueform.keytoupdate = ["VENUE_DET", "CLR_AMNS"];
                     venueform.ameneties = [];
                     this.setState({ ameneties: null });
                   }
                   // this.setState({ameneties:null});
                   venueform.venue.venue_spec_id = data.venue_spec_id;
                   var venueformData = this.state.venueformData;
                   venueformData.set("venue", JSON.stringify(venueform.venue));
                   venueformData.set(
                     "specific",
                     JSON.stringify(venueform.specific)
                   );
                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );
                   if (this.state.autosave == true && !dontupdate) {
                     // this.autosaveData(venueformData);
                   }
                   // alert(JSON.stringify(venueform));
                   // }
                   this.setState({ venueform }, () => {
                     this.gotoNext();
                   });
                 };
                 loadFacility = () => {
                   if (this.state.facilityIndex == 0) {
                     return (
                       <FacilityType
                         venuetypedata={this.state.venuetypedata}
                         facilitytypeData={this.state.facilitytypedata}
                         sendfacilitytypedata={this.sendfacilitytypedata}
                         headertext={this.changeHeaderText}
                         labeltext={this.changeLabelText}
                       />
                     );
                   } else {
                     return (
                       <FacilityDetails
                         commonArray={this.state.commonArray}
                         venuetypedata={this.state.venuetypedata}
                         facilityData={this.state.facilitytypedata}
                         sendfaciltiydata={this.sendfaciltiydata}
                         headertext={this.changeHeaderText}
                         labeltext={this.changeLabelText}
                       />
                     );
                   }
                 };
                 gotoback = () => {
                   if (this.state.currentIndex != 0) {
                     if (this.state.activeparent == "tab2") {
                       if (this.state.facilityIndex == 1) {
                         this.setState({ facilityIndex: 0 });
                         this.setState({ facitlityactive: 1 });
                       } else {
                         var currentitem =
                           parseInt(this.state.currentIndex) - 1;
                         var currentitemname = this.state.list[currentitem]
                           .name;
                         this.setState({ activeparent: currentitemname });
                         this.setState({ currentIndex: currentitem });
                       }
                     } else {
                       var currentitem = parseInt(this.state.currentIndex) - 1;
                       var currentitemname = this.state.list[currentitem].name;
                       this.setState({ activeparent: currentitemname });
                       this.setState({ currentIndex: currentitem });
                       this.setState({ facilityIndex: 1 });
                       this.setState({ facitlityactive: true });
                     }
                   }
                 };

                 validateGoToNext = () => {
                   // console.log(this.state.activeparent)
                   // alert(this.state.activeparent)
                   switch (this.state.activeparent) {
                     case "tab1": {
                       const current = this.state.venuetypedata;
                       console.log('current',current);
                       if (current == null && !this.state.edit ) 
                       Toast.show("Please Fill the fields", Toast.LONG);
                       else
                       this.gotoNext();
                       break;
                     }
                     case "tab2": {
                       // alert(this.state.facilityIndex);
                       if (this.state.facilityIndex == 0) {
                         const current = this.state.facilitytypedata;
                         console.log(current);
                         if (current != null) this.gotoNext();
                         else Toast.show("Please Fill the fields", Toast.LONG);
                       } else if (this.state.facilityIndex == 1) {
                         const current =
                           this.state.commonArray &&
                           this.state.commonArray.length > 0 &&
                           this.state.commonArray.some(
                             item => item.error == true || item.error ==null
                           );
                         console.log("tab3", current);
                         console.log("editmode", this.state.edit);
                         console.log("init", this.state.init);
                         console.log("common",this.state.commonArray);
                         console.log(
                           "objedit",
                           typeof this.props.navigation.state.params.editData
                         );
                         if (
                           (current == true || this.state.init) &&
                           typeof this.props.navigation.state.params.editData !=
                             "object"
                         ) {
                           Toast.show("Please fill the fields", Toast.LONG);
                         } else {
                           if (current == true && this.state.edit) {
                             console.log("elseif");
                             Toast.show("Please fill the fields", Toast.LONG);
                             break;
                           }

                           if (this.state.autosave == true) {
                             var venueform = this.state.venueform;
                             var venueformData = this.state.venueformData;
                             if (this.state.autosave == true) {
                               // venueform.keytoupdate=["VENUE_DET"];
                               venueform.venue.lastActivityTab = 2;
                               this.setState({ venueform });
                               venueformData.set(
                                 "venue",
                                 JSON.stringify(venueform.venue)
                               );
                               venueformData.set(
                                 "keytoupdate",
                                 JSON.stringify(venueform.keytoupdate)
                               );
                               this.autosaveData(venueformData);
                             }
                           }
                           this.gotoNext();
                         }
                       }
                       break;
                     }
                     case "tab3": {
                       const current = this.state.ameneties;
                       //  alert(JSON.stringify(current));
                       if (
                         current == "" ||
                         current == null ||
                         (current &&
                           Array.isArray(current) &&
                           current.length == 0) ||
                         (current &&
                           Array.isArray(current) &&
                           current.filter(obj => obj.circleCount > 0).length ==
                             0)
                       ) {
                         Toast.show("Please select amenities", Toast.LONG);
                       } else {
                         this.gotoNext();
                       }
                       break;
                     }
                     case "tab4": {
                       const current = this.state.availability.businessForm;
                       console.log("tab4", current);
                       if (current == null) {
                         Toast.show("Please select fields", Toast.LONG);
                       } else {
                         this.gotoNext();
                       }
                       break;
                     }
                     case "tab5": {
                       const current = this.state.purposeData;
                       console.log("tab5", current);
                       if (current == null || current == "") {
                         Toast.show("Please fill the fields", Toast.LONG);
                       } else {
                         this.gotoNext();
                       }
                       break;
                     }
                     case "tab6": {
                       const current = this.state.venueprice;
                       let error = false;


                       console.log("tab6", current);

                       if (current != null) {
                         let choose = this.state.venueform.availability.venue_avail_type.split(
                           ","
                         );
                         choose.forEach(element => {
                           console.log('element',element);
                           if (element == 1) {
                             const hour =
                               current.seatList[0].pricing[0].hourCost;
                             if (hour <= 0) error = true;
                           } else if (element == 2) {
                             const day = current.seatList[0].pricing[0].dayCost;
                             if (day <= 0) error = true;
                           } else if (element == 3) {
                             const week =
                               current.seatList[0].pricing[0].weekCost;
                             if (week <= 0) error = true;
                           } else if (element == 4) {
                             const month =
                               current.seatList[0].pricing[0].monthCost;
                             if (month <= 0) error = true;
                           }
                         });
                       }

                       if (
                         this.state.availability.businessForm &&
                         this.state.availability.businessForm.venue_type == 2
                       ) {
                         var validations = this.state.validations;
                         validations[4].state = true;
                         this.setState({ validations });
                       }

                       console.log('err',error)

                       if (
                         current != null ||
                         (this.state.availability.businessForm &&
                           this.state.availability.businessForm.venue_type == 2)
                       ) {
                         if (error) {
                           Toast.show("Please fill the fields", Toast.LONG);
                         } else this.gotoNext();
                       } else Toast.show("Please select fields", Toast.LONG);
                       break;
                     }
                     case "tab7": {
                       this.gotoNext();
                       break;
                     }
                     case "tab8": {
                       Toast.show("Please take a action", Toast.LONG);
                       break;
                     }
                     default: {
                       alert(result);
                       break;
                     }
                   }
                 };

                 gotoNext = () => {
                   console.log("this", this.state.currentIndex);
                   console.log("tablist", this.state.list);
                   if (this.state.currentIndex != this.state.list.length - 1) {
                     if (this.state.activeparent == "tab2") {
                       if (this.state.facilityIndex == 0) {
                         this.setState({ facilityIndex: 1 });
                         this.setState({ facitlityactive: 1 });
                       } else {
                         var currentitem =
                           parseInt(this.state.currentIndex) + 1;

                         // alert(this.state.list[currentitem].name)
                         var currentitemname = this.state.list[currentitem]
                           .name;
                         // alert(currentitem);
                         this.setState({ activeparent: currentitemname });
                         this.setState({ currentIndex: currentitem });
                       }
                     } else {
                       var currentitem = parseInt(this.state.currentIndex) + 1;

                       // alert(this.state.list[currentitem].name)
                       var currentitemname = this.state.list[currentitem].name;
                       // alert(currentitem);
                       this.setState({ activeparent: currentitemname });
                       this.setState({ currentIndex: currentitem });
                       this.setState({ facilityIndex: 0 });
                       this.setState({ facitlityactive: true });
                     }
                   }

                   console.log("executed");
                 };
                 availablitydays = data => {
                   var availability = this.state.availability;
                   availability.days = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_days = data ? data.value : null;
                   this.setState({ venueform });
                 };
                 availcircleData = data => {
                   var availability = this.state.availability;
                   availability.type = data;
                   this.setState({ availability });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_avail_type = data;
                   this.setState({ venueform });
                 };
                 fromtodays = data => {
                   var availability = this.state.availability;
                   availability.from = data.from;
                   availability.to = data.to;
                   this.setState({ availability });
                   if (data.from == "" || data.to == "") {
                     var validations = this.state.validations;
                     validations[3].state = false;
                   } else {
                     var validations = this.state.validations;
                     validations[3].state = true;
                   }
                   this.setState({ validations });
                   var venueform = this.state.venueform;
                   venueform.availability.venue_avail_frm = availability.from;
                   venueform.availability.venue_avail_to = availability.to;
                   this.setState({ venueform });
                 };
                 AmenetiesData = (data, dontupdate) => {
                   this.setState({ ameneties: data });
                   var amenitiesData =
                     data.length > 0 && data.filter(obj => obj.circleCount > 0);
                   var amentiesArray = [];
                   for (var i in amenitiesData) {
                     if (
                       amenitiesData[i].hasOwnProperty("activeArray") == true
                     ) {
                       amentiesArray = amentiesArray.concat(
                         amenitiesData[i].activeArray
                       );
                     }
                   }
                   // alert(JSON.stringify(amentiesArray));
                   var venueform = this.state.venueform;
                   venueform.ameneties = amentiesArray;
                   var venueformData = this.state.venueformData;
                   if (!dontupdate && this.state.autosave == true) {
                     venueform.keytoupdate = ["VENUE_DET", "VENUE_AMNS"];
                     venueform.venue.lastActivityTab = 3;
                     venueformData.set(
                       "venue",
                       JSON.stringify(venueform.venue)
                     );
                     venueformData.set(
                       "keytoupdate",
                       JSON.stringify(venueform.keytoupdate)
                     );
                     venueformData.set(
                       "ameneties",
                       JSON.stringify(venueform.ameneties)
                     );
                   }
                   if (this.state.autosave == true && !dontupdate) {
                     this.autosaveData(venueformData);
                   }
                   this.setState({ venueform }, () => {
                     // this.gotoNext()
                   });
                 };
                 saveTags = (data, filterdata, dontupdate) => {
                   // alert(JSON.stringify(filterdata));
                   var venTags = this.state.venTags;
                   venTags = data;
                   this.setState({ venTags });
                   var tagarray = [];
                   for (var i = 0; i < filterdata.length; i++) {
                     var objdata = {
                       tag_cat_id: filterdata[i].tag_cat_id,
                       tag_details: []
                     };
                     for (var y = 0; y < filterdata[i].data.length; y++) {
                       objdata.tag_details.push(filterdata[i].data[y].tag_name);
                     }
                     tagarray.push(objdata);
                   }
                   // alert(JSON.stringify(tagarray));
                   var venueform = this.state.venueform;
                   var venueformData = this.state.venueformData;
                   venueform.tagdetails = tagarray;
                   if (!dontupdate && this.state.autosave == true) {
                     venueform.keytoupdate = [
                       "VENUE_DET",
                       "VENUE_AMNS",
                       "VENUE_AVAIL",
                       "VENUE_TAG"
                     ];
                     venueform.venue.lastActivityTab = 5;
                     venueformData.set(
                       "venue",
                       JSON.stringify(venueform.venue)
                     );
                     venueformData.set(
                       "hourlyAvailability",
                       JSON.stringify(venueform.hourlyAvailability)
                     );
                     venueformData.set(
                       "ameneties",
                       JSON.stringify(venueform.ameneties)
                     );
                     venueformData.set(
                       "availability",
                       JSON.stringify(venueform.availability)
                     );
                     venueformData.set(
                       "packsDetails",
                       JSON.stringify(venueform.packsDetails)
                     );
                     venueformData.set(
                       "seatDetails",
                       JSON.stringify(venueform.seatDetails)
                     );
                     venueformData.set(
                       "purpose",
                       JSON.stringify(venueform.purposes)
                     );
                     venueformData.set(
                       "tagdetails",
                       JSON.stringify(venueform.tagdetails)
                     );
                     venueformData.set(
                       "keytoupdate",
                       JSON.stringify(venueform.keytoupdate)
                     );
                     this.autosaveData(venueformData);
                     this.setState({ venueformData });
                   }

                   this.setState({ tagarray });
                 };

                 updateCloseModal = data => {
                   var availabilityState = this.state;
                   availabilityState.isAvailable = data.isAvailable;
                   availabilityState.showBusinessForm = data.showBusinessForm;
                   availabilityState.isSlotsBooking = data.isSlotsBooking;
                   this.setState({ availabilityState });
                 };

                 sendPriceData = (data, dontupdate) => {
                   // alert(JSON.stringify(d));
                   var venueform = this.state.venueform;
                   var venueformData = this.state.venueformData;
                   if (data.seatList && data.venuetype == 3) {
                     // alert(JSON.stringify(data.seatList));
                     var checkseatListArrayHavingAdd = data.seatList.filter(
                       obj => obj.add == "true"
                     );
                     // alert(JSON.stringify(data.seatList));
                     var availability = this.state.availability;

                     venueform.seatDetails =
                       data.seatList.length > 0
                         ? data.seatList.map(obj => {
                             return {
                               seatName: obj.name,
                               seatQuantity: obj.value,
                               seatPricing: obj.pricing ? obj.pricing : [],
                               uniqueId: obj.uniqueId,
                               priceType: data.avail_type
                             };
                           })
                         : [];
                     if (checkseatListArrayHavingAdd.length == 0) {
                       // var data1=data;
                       var seatListArrayAdd = [
                         {
                           name: "",
                           value: 0,
                           add: "true",
                           uniqueId: DateFunctions.generateTimestamp()
                         }
                       ].concat(data.seatList.filter(obj => obj.add != "true"));
                       // alert(JSON.stringify(seatListArrayAdd));
                       availability.seatList = seatListArrayAdd;
                       // this.setState({availobj:data1});
                     }
                     if (!dontupdate && this.state.autosave == true) {
                       venueform.keytoupdate = [
                         "VENUE_DET",
                         "VENUE_AMNS",
                         "VENUE_AVAIL",
                         "VENUE_TAG"
                       ];
                       venueform.venue.lastActivityTab = 6;

                       venueformData.set(
                         "seatDetails",
                         JSON.stringify(venueform.seatDetails)
                       );
                     }
                     this.setState({ availability });
                   }
                   if (data.venuetype != 2 && data.venuetype != 3) {
                     // alert(JSON.stringify(data));
                     var availTypeList = this.state.availability.avail_type;
                     var seatList = data.seatList;
                     this.setState({
                       priceDataList:
                         seatList.length > 0 ? seatList[0].pricing[0] : null
                     });
                     var hourCost =
                       availTypeList.includes("1") == true
                         ? seatList.length > 0
                           ? seatList[0].pricing[0].hourCost
                             ? seatList[0].pricing[0].hourCost
                             : 0
                           : 0
                         : 0;
                     var dayCost =
                       availTypeList.includes("2") == true
                         ? seatList.length > 0
                           ? seatList[0].pricing[0].dayCost
                             ? seatList[0].pricing[0].dayCost
                             : 0
                           : 0
                         : 0;
                     var weekCost =
                       availTypeList.includes("3") == true
                         ? seatList.length > 0
                           ? seatList[0].pricing[0].weekCost
                             ? seatList[0].pricing[0].weekCost
                             : 0
                           : 0
                         : 0;
                     var monthCost =
                       availTypeList.includes("4") == true
                         ? seatList.length > 0
                           ? seatList[0].pricing[0].monthCost
                             ? seatList[0].pricing[0].monthCost
                             : 0
                           : 0
                         : 0;
                     var obj = {
                       venue_price_type: availTypeList,
                       hour_cost: hourCost,
                       week_cost: weekCost,
                       day_cost: dayCost,
                       month_cost: monthCost,
                       venue_price_currency:
                         seatList.length > 0
                           ? seatList[0].pricing[0].currency
                           : "",
                       venueType: data.venueType
                     };
                     // alert(JSON.stringify(obj));
                     venueform.pricedetails = obj;
                     if (!dontupdate && this.state.autosave == true) {
                       venueformData.set(
                         "pricedetails",
                         JSON.stringify(venueform.pricedetails)
                       );
                       venueform.keytoupdate = [
                         "VENUE_DET",
                         "VENUE_AMNS",
                         "VENUE_AVAIL",
                         "VENUE_TAG",
                         "VENUE_PRICE"
                       ];
                       venueform.venue.lastActivityTab = 6;

                       venueformData.set(
                         "keytoupdate",
                         JSON.stringify(venueform.keytoupdate)
                       );
                     }
                   }
                   // alert(dontupdate)
                   if (this.state.autosave == true && !dontupdate) {
                     venueformData.set(
                       "venue",
                       JSON.stringify(venueform.venue)
                     );
                     venueformData.set(
                       "hourlyAvailability",
                       JSON.stringify(venueform.hourlyAvailability)
                     );
                     venueformData.set(
                       "ameneties",
                       JSON.stringify(venueform.ameneties)
                     );
                     venueformData.set(
                       "availability",
                       JSON.stringify(venueform.availability)
                     );
                     venueformData.set(
                       "packsDetails",
                       JSON.stringify(venueform.packsDetails)
                     );
                     venueformData.set(
                       "seatDetails",
                       JSON.stringify(venueform.seatDetails)
                     );
                     this.autosaveData(venueformData);
                   }
                   // venueform.keytoupdate=["VENUE_DET",'VENUE_AMNS','VENUE_AVAIL','VENUE_TAG','VENUE_PRICE'];
                   // venueform.venue.lastActivityTab=6;
                   this.setState({ venueform });
                   var venueprice = this.state.venueprice;
                   venueprice = data;
                   this.setState({ venueprice });
                   // // alert(JSON.stringify(data));
                   // if(data.amt==0 || data.amt==''){
                   var validations = this.state.validations;
                   // validations[4].state=false;
                   // }else{
                   //       var validations=this.state.validations;
                   validations[4].state = true;
                   // }
                   this.setState({ validations });
                  if(data.check)
                   this.validateGoToNext();

                   // var venueform=this.state.venueform;
                   // venueform.pricedetails.venue_price_amt=data.amt;
                   // venueform.pricedetails.venue_price_type=data.activeId;
                   // venueform.pricedetails.venue_price_currency=data.days;
                   // venueform.pricedetails.venue_price_name=data.type;
                   // this.setState({venueform});
                 };
                 venueImages = (data, dontupdate) => {
                   var venueformData = this.state.venueformData;
                   // venueformData=data.formimagearray;
                   var venueform = this.state.venueform;
                   venueform.formArray = data.localimages;
                   venueform.keytoupdate = [
                     "VENUE_DET",
                     "VENUE_AMNS",
                     "VENUE_AVAIL",
                     "VENUE_TAG",
                     "VENUE_IMAGE"
                   ];
                   venueform.venue.lastActivityTab = 7;
                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );
                   const fieldforuploads = data.formimagearray
                     .getParts()
                     .find(item => item.fieldName === "formArray");
                   var arrayofloopimages = data.localimages.filter(
                     obj => obj.exist
                   );
                   if (fieldforuploads) {
                     venueformData.set("formArray", fieldforuploads);
                     // alert(JSON.stringify());
                     // arrayofloopimages.push({name:data.localimages[data.localimages.length-1].fileName})
                     if (data.localimages.length > 0) {
                       // arrayofloopimages.push({name:data.localimages[data.localimages.length-1].fileName});
                     }
                   }
                   var existingnewimages =
                     arrayofloopimages.length > 0
                       ? arrayofloopimages.map(obj => {
                           // var objnew=obj;
                           var newobj = { name: obj.name };
                           return newobj;
                         })
                       : [];
                   // venueform.formArray=arrayofloopimages;
                   venueformData.set("venue", JSON.stringify(venueform.venue));
                   venueformData.set(
                     "hourlyAvailability",
                     JSON.stringify(venueform.hourlyAvailability)
                   );
                   venueformData.set(
                     "ameneties",
                     JSON.stringify(venueform.ameneties)
                   );
                   venueformData.set(
                     "availability",
                     JSON.stringify(venueform.availability)
                   );
                   venueformData.set(
                     "packsDetails",
                     JSON.stringify(venueform.packsDetails)
                   );
                   venueformData.set(
                     "seatDetails",
                     JSON.stringify(venueform.seatDetails)
                   );
                   venueformData.set(
                     "purpose",
                     JSON.stringify(venueform.purposes)
                   );
                   venueformData.set(
                     "tagdetails",
                     JSON.stringify(venueform.tagdetails)
                   );
                   venueformData.set(
                     "existImages",
                     JSON.stringify(existingnewimages)
                   );
                   // venueformData
                   this.setState({ venueform });
                   if (this.state.autosave == true && !dontupdate) {
                     this.autosaveData(
                       venueformData,
                       "upload",
                       existingnewimages
                     );
                   }
                   this.setState({ venueformData });
                 };

                 
                 submitVenuData = (data, edit) => {
                   // alert(edit);
                   // return;
                   var self = this;
                   var venueformData = this.state.venueformData;
                   var venueform = this.state.venueform;
                   this.setState({ loading: true });
                   venueformData.set("dummyVenueId", venueform.venue.venue_id);
                   venueformData.set("venue", JSON.stringify(venueform.venue));
                   venueformData.set(
                     "hourlyAvailability",
                     JSON.stringify(venueform.hourlyAvailability)
                   );
                   venueformData.set(
                     "ameneties",
                     JSON.stringify(venueform.ameneties)
                   );
                   venueformData.set(
                     "availability",
                     JSON.stringify(venueform.availability)
                   );
                   venueformData.set(
                     "packsDetails",
                     JSON.stringify(venueform.packsDetails)
                   );
                   venueformData.set(
                     "seatDetails",
                     JSON.stringify(venueform.seatDetails)
                   );
                   venueformData.set(
                     "purpose",
                     JSON.stringify(venueform.purposes)
                   );
                   venueformData.set(
                     "tagdetails",
                     JSON.stringify(venueform.tagdetails)
                   );
                   venueformData.set(
                     "pricedetails",
                     JSON.stringify(venueform.pricedetails)
                   );
                   if (self.state.autosave == true) {
                     venueformData.set("formArray", null);
                   }
                   LoadAPIPost.LoadApi(
                     venueformData,
                     links.APIURL +
                       (edit && !self.state.autosave
                         ? "edit_venue/"
                         : "insert_venue"),
                     this.state.autosave == true ? null : "upload",
                     function(err, success) {
                       self.setState({ loading: null });

                       if (err) {
                         Toast.show("serverError", Toast.LONG);
                       } else {
                         var jsonData = JSON.parse(success);
                         if (edit && !self.state.autosave) {
                           Toast.show("Venue Updated Successfully", Toast.LONG);
                           setTimeout(() => {
                             Actions.reset("home");
                           }, 100);
                         } else {
                           // alert(JSON.stringify(responseJson));
                           self.setState({ activeparent: "success" });
                         }
                       }
                     },
                     function(progress) {
                       self.setState({ progresscount: progress });
                     }
                   );
                 };
                 //         fetch(), {
                 //      method: 'POST',
                 //   body: data,
                 // }).then((response)=>response.json())
                 //    .then((responseJson)=>{
                 // this.setState({loading:null});
                 // if(edit){
                 //  Toast.show("Venue Updated Successfully", Toast.LONG);
                 //  setTimeout(()=>{
                 //   Actions.reset('home');
                 //  },100)
                 // }else{
                 // // alert(JSON.stringify(responseJson));
                 // this.setState({activeparent:'success'});
                 // }
                 // })
                 // alert("venue added successfully");
                 // Actions.success();
                 // Actions.pop();
                 // if(responseJson.status==0){

                 // alert("Venue Added Successfully");
                 // this.props.closevenue();
                 // // this.props.closemodal();
                 // }else{
                 //   alert(" iErrorn Response");
                 // }
                 // })
                 closeSuccessModal = data => {
                   this.setState({ activeparent: null });
                   if (data) {
                     Actions.venuepage({ raiselogin: true });
                   } else {
                     Actions.reset("home");
                   }
                 };
                 errorData = data => {
                   Toast.show(data, Toast.LONG);
                 };
                 loadedit = () => {
                   this.setState({ currentIndex: 0 });
                   this.setState({ activeparent: "tab1" });
                   // this.goToRight();
                 };
                 sendPurposes = (data, data1, purposelength, dontupdate) => {
                   // console.log(data);
                   // console.log(data1);
                   var purposeData = this.state.purposeData;
                   if (!purposeData) {
                     purposeData = {};
                   }
                   purposeData.purposes = data;
                   purposeData.purposeDropdown = data1;
                   var validations = this.state.validations;
                   if (purposelength == 0) {
                     validations[5].state = false;
                   } else {
                     validations[5].state = true;
                   }
                   var venueform = this.state.venueform;
                   venueform.purposes = data.map(obj => {
                     return {
                       venue_act_id: obj.id,
                       purposedetails: obj.value.join(",")
                     };
                   });
                   if (!dontupdate && this.state.autosave == true) {
                     venueform.keytoupdate = [
                       "VENUE_DET",
                       "VENUE_AMNS",
                       "VENUE_AVAIL",
                       "VENUE_TAG"
                     ];

                     venueform.venue.lastActivityTab = 5;
                   }
                   var venueformData = this.state.venueformData;
                   venueformData.set(
                     "purpose",
                     JSON.stringify(venueform.purposes)
                   );
                   venueformData.set(
                     "tagdetails",
                     JSON.stringify(venueform.tagdetails)
                   );
                   venueformData.set(
                     "keytoupdate",
                     JSON.stringify(venueform.keytoupdate)
                   );
                   if (this.state.autosave == true && !dontupdate) {
                     venueformData.set(
                       "venue",
                       JSON.stringify(venueform.venue)
                     );
                     venueformData.set(
                       "hourlyAvailability",
                       JSON.stringify(venueform.hourlyAvailability)
                     );
                     venueformData.set(
                       "ameneties",
                       JSON.stringify(venueform.ameneties)
                     );
                     venueformData.set(
                       "availability",
                       JSON.stringify(venueform.availability)
                     );
                     venueformData.set(
                       "packsDetails",
                       JSON.stringify(venueform.packsDetails)
                     );
                     venueformData.set(
                       "seatDetails",
                       JSON.stringify(venueform.seatDetails)
                     );
                     this.autosaveData(venueformData);
                   }
                   this.setState({ purposeData, validations, venueform });
                 };
                 render() {
                   console.log("fds", this.state.facilityIndex);
                   console.log("fds", this.state.facitlityactive);
                   return (
                     <Container>
                       <Root>
                         {this.state.loading && (
                           <View
                             style={{
                               width: "100%",
                               zIndex: 9999,
                               flex: 1,
                               justifyContent: "center",
                               alignItems: "center",
                               position: "absolute",
                               height: "100%",
                               backgroundColor: "rgba(0,0,0,0.8)",
                               top: 0
                             }}
                           >
                             <CircularProgressBar1
                               percent={this.state.progresscount}
                               radius={50}
                               shadowColor={color.white}
                               borderWidth={4}
                               color={color.orangeBtn}
                               bgColor={color.white}
                             >
                               <Text style={{ fontSize: 18 }}>
                                 {this.state.progresscount + "%"}
                               </Text>
                             </CircularProgressBar1>
                             <Text
                               style={{ color: color.white, marginTop: 12 }}
                             >
                               Submitting Please Wait ....
                             </Text>
                           </View>
                         )}
                         <SubHeaderComp bgcolor={color.orange}>
                           <View style={styles.subheading}>
                             <Text style={styles.subheadingtext}>
                               List your Venue
                             </Text>
                           </View>
                         </SubHeaderComp>
                         <Content>
                           {this.state.activeparent != "success" && (
                             <View>
                               <DynamicPagination
                                 dots={8}
                                 activeid={this.state.currentIndex}
                                 changePagination={data => this.changeTab(data)}
                               />
                               <View
                                 style={{
                                   flex: 1,
                                   flexDirection: "row",
                                   alignItems: "center",
                                   padding: 5,
                                   borderBottomWidth: 0.5,
                                   borderColor: color.black1,
                                   justifyContent: "space-between"
                                 }}
                               >
                                 <View>
                                   <Text
                                     style={{
                                       fontSize: hp("1.7%"),
                                       color: color.blue,
                                       fontWeight: "bold"
                                     }}
                                   >
                                     {this.state.venueheadertext}
                                   </Text>
                                 </View>
                                 {this.state.showSkip && (
                                   <TouchableOpacity
                                     onPress={() => this.gotoNext()}
                                   >
                                     <Text
                                       style={{
                                         fontSize: hp("1.7%"),
                                         color: color.blue
                                       }}
                                     >
                                       Skip
                                     </Text>
                                   </TouchableOpacity>
                                 )}
                               </View>
                               <View
                                 style={{
                                   flex: 1,
                                   flexDirection: "row",
                                   alignItems: "center",
                                   padding: 5,
                                   borderBottomWidth: 0.5,
                                   borderColor: color.black1
                                 }}
                               >
                                 <View style={{ flex: 0.1 }}>
                                   <TouchableOpacity onPress={this.gotoback}>
                                     <Icon
                                       style={[
                                         styles.arrownavicon,
                                         {
                                           color:
                                             this.state.facitlityactive &&
                                             (this.state.facilityIndex == 1 ||
                                               this.state.activeparent ==
                                                 "tab8")
                                               ? color.orange
                                               : null
                                         }
                                       ]}
                                       type="FontAwesome"
                                       name="angle-left"
                                     />
                                   </TouchableOpacity>
                                 </View>
                                 <View
                                   style={{
                                     flex: 0.8,
                                     flexDirection: "row",
                                     alignItems: "center",
                                     justifyContent: "center"
                                   }}
                                 >
                                   <Text
                                     numberOfLines={2}
                                     style={{
                                       fontSize: hp("2%"),
                                       textAlign: "center"
                                     }}
                                   >
                                     {this.state.venuetypetext}
                                   </Text>
                                 </View>
                                 <View style={{ flex: 0.1 }}>
                                   <TouchableOpacity
                                     onPress={() => this.validateGoToNext()}
                                   >
                                     <Icon
                                       style={[
                                         styles.arrownavicon,
                                         {
                                           color:
                                             this.state.facitlityactive &&
                                             this.state.facilityIndex == 0 &&
                                             (this.state.activeparent !=
                                               "tab8" ||
                                               this.state.activeparent ==
                                                 "tab1")
                                               ? color.orange
                                               : null
                                         }
                                       ]}
                                       type="FontAwesome"
                                       name="angle-right"
                                     />
                                   </TouchableOpacity>
                                 </View>
                               </View>
                             </View>
                           )}
                           <View>
                             {this.state.activeparent == "tab1" && (
                               <VenueType
                                 loginDetails={this.state.loginDetails}
                                 venuedetails={this.state.venuetypedata}
                                 sendvenuetypedata={this.sendvenuetypedata}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                               />
                             )}
                             {this.state.activeparent == "tab2" &&
                               this.loadFacility()}
                             {this.state.activeparent == "tab3" && (
                               <Ameneties
                                 venuetypedata={this.state.venuetypedata}
                                 facilitytypeData={this.state.facilitytypedata}
                                 nextdata={datas}
                                 amentiesData={this.state.ameneties}
                                 sendAmenetiesData={this.AmenetiesData}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                               />
                             )}
                             {this.state.activeparent == "tab4" && (
                               <Availability
                                 list={this.state.list1}
                                 updateCloseModal={this.updateCloseModal}
                                 totalState={this.state.availabilityState}
                                 moredetails={data => this.getmoredetails(data)}
                                 businessForm={this.getBusinessForm}
                                 getSlots={this.getSlots}
                                 sendcircleid={this.availcircleData}
                                 splitData={this.state.splitData}
                                 fromtodays={this.fromtodays}
                                 availablitydays={this.availablitydays}
                                 datas={this.state.availability}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                               />
                             )}
                             {this.state.activeparent == "tab5" && (
                               <VenueTags
                                 purposeDatas={this.state.purposeData}
                                 sendpurposes={this.sendPurposes}
                                 mytags={newtags}
                                 sendTags={this.saveTags}
                                 tagDetails={this.state.venTags}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                               />
                             )}
                             {this.state.activeparent == "tab6" && (
                               <VenuePrice
                                 priceDataList={this.state.priceDataList}
                                 sendPriceData={this.sendPriceData}
                                 priceData={this.state.venueprice}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                                 activeparent={this.state.activeparent}
                                 seatList={
                                   this.state.availability &&
                                   this.state.availability.seatList.filter(
                                     obj => obj.add != "true"
                                   )
                                 }
                                 venuetype={
                                   this.state.availability &&
                                   this.state.availability.businessForm &&
                                   this.state.availability.businessForm
                                     .venue_type
                                 }
                                 availType={
                                   this.state.availability &&
                                   this.state.availability.avail_type
                                 }
                               />
                             )} 


                             
                             {this.state.activeparent == "tab7" && (
                               <VenueUpload
                                 localimages={this.state.venueform.formArray}
                                 mainformData={this.state.venueformData}
                                 venueform={this.state.venueform}
                                 venueImages={this.venueImages}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                               />
                             )}
                             {this.state.activeparent == "tab8" && (
                               <VenueReview
                                 availability={this.state.availability}
                                 ameneties={this.state.ameneties}
                                 facilitytypedata={this.state.facilitytypedata}
                                 commonArray={this.state.commonArray}
                                 error={this.errorData}
                                 loadedit={this.loadedit}
                                 submitVenuData={this.submitVenuData}
                                 mainformData={this.state.venueformData}
                                 venueform={this.state.venueform}
                                 validations={this.state.validations}
                                 labeltext={this.changeLabelText}
                                 headertext={this.changeHeaderText}
                               />
                             )}
                             {this.state.activeparent == "success" && (
                               <VenueSuccess
                                 availability={this.state.availability}
                                 commonArray={this.state.commonArray}
                                 loginDetails={this.state.loginDetails}
                                 closemodal={data =>
                                   this.closeSuccessModal(data)
                                 }
                               />
                             )}
                           </View>
                         </Content>
                       </Root>
                     </Container>
                   );
                 }
               }

const styles = {
        active: {
            backgroundColor: '#e2e2e2',
            borderRightWidth: 0.5,
            borderColor: '#999999',
            zIndex: 99999
        },
        subheading: {
            padding: 12
        },
        subheadingtext: {
            fontSize: hp('2%'),
            color: color.white
        },
        imageparent: {
            width: hp('8%'),
            height: hp('7%'),
            alignItems: 'center',
            justifyContent: 'center',
            borderWidth: 0.5,
            borderColor: color.ash2
        },
        imageTab: {
            width: hp('4%'),
            height: hp('4%'),
        },
        activeparent: { position: 'absolute', right: -hp('1.4%'), top: hp('2%'), alignItems: 'center', justifyContent: 'center', zIndex: 999999 },
    bordertriangle: {
        position: 'absolute',
        left: 0,
        top: hp('0.23%'),
    },
    arrowbox: {
        width: hp('3%'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: color.blueactive
    },
    arrowboxicon: {
        color: color.black1,
        fontSize: hp('3.2%')
    },
    arrownavicon:{
    	fontSize:hp('4%'),
    	textAlign:'center',
    	color:color.ash2
    },
    activetextorange:{
    	color:color.orange
    }
}