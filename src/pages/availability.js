"use strict";

import React, { Component } from "react";
import CircleText from "../components/circlecomp";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content,
  DatePicker,
  Fab
} from "native-base"; 
import { StyleSheet, View, Text, Picker, TextInput } from "react-native";
import Line from "../components/Line";
import Hourly from "../images/svg/Hourly.png";
import Daily from "../images/svg/Daily.png";
import Weekly from "../images/svg/Weekly.png";
import Calendar from "../images/svg/calendar.png";
import Dropdown_conatiner from "../components/drpdown_container";
import ModalComp from "../components/ModalComp";
import PopupMenu from "../components/popupmenu";
import Triangle from "../components/triangles";
import AddForm from "../pages/addForm";
import AddPax from "../pages/addPax";
import SlotsBooking from './slotsBooking';
import AccordionView from "../components/accordion";
import moment from "moment";

import { Actions } from 'react-native-router-flux';

const hour_drop = [
  { id: 1, value: "Full Day" },
  { id: 2, value: "Only Mornings" },
  { id: 3, value: "Only Evenings" },
  { id: 4, value: "5 Hours" },
  { id: 5, value: "10 Hours" },
  { id: 6, value: "All Except Few Hours" }
];
const month_drop = [{ id: 1, value: "Except Few Months" }];

const week_drop = [
  { id: 1, value: "All Weeks" },
  { id: 2, value: "All Except Few Weeks" },
  { id: 3, value: "All Except Weekends" },
  { id: 4, value: "Alternate Weeks" },
  { id: 5, value: "Only Weekends" },
  { id: 6, value: "All Except Few Days" }
]; 

var datas = [
  { name: "Hourly", id: 1, icon: Hourly, checked: true },
  { name: "Daily", id: 2, icon: Daily, checked: false },
  { name: "Weekly", id: 3, icon: Weekly, checked: false },
  { name: "Monthly", id: 4, icon: Calendar, checked: false }
]; 




var checkboxesexclude = [
  {
    id: 0,
    name: "SUN",
    _checked: false
  },
  {
    id: 1,
    name: "MON",
    _checked: false
  },
  {
    id: 2,
    name: "TUE",
    _checked: false
  },
  {
    id: 3,
    name: "WED",
    _checked: false
  },
  {
    id: 4,
    name: "THU",
    _checked: false
  },
  {
    id: 5,
    name: "FRI",
    _checked: false
  },
  {
    id: 6,
    name: "SAT",
    _checked: false
  }
];

var DurationForm = [
  {
    type: "date",
    expanded: true,
    title: "",
    content: (
      <AddForm
        from={data => this.fromdate(data)}
        to={data => this.todate(data)}
      />
    )
  }
];
var MoreDetails = [
  {
    expanded: true,
    title: "",
    content: (
      <View>
        <Text>Hiiii</Text>
      </View>
    )
  }
];
class Availability extends Component {
  constructor(props) {
    super(props);
    DurationForm[0].title = (
      <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
        {props.roomname} <Text style={{ color: color.black1 }}>Duration</Text>
      </Text>
    );
    MoreDetails[0].title = (
      <Text style={{ fontSize: hp("2.3%"), color: color.orange }}>
        {props.roomname}{" "}
        <Text style={{ color: color.black1 }}>Add More Details</Text>
      </Text>
    );
    this.state = {
      availabilityDetails: {
        type: 1,
        days: {},
        from: "",
        to: "",
        moredetails: ""
      }, 
      splitSlots:[],
      businessFormState:null,
      finalPax: [],
      addSeat: [],
      datas: [
  { name: "Hourly", id: 1, icon: Hourly, checked: true },
  { name: "Daily", id: 2, icon: Daily, checked: false },
  { name: "Weekly", id: 3, icon: Weekly, checked: false },
  { name: "Monthly", id: 4, icon: Calendar, checked: false }
],
      slot_type:1,
      slotsBookingState: null,
      showBusinessForm: false,
      isAvailable: true,
      isSlotsBooking: false,
      selected: "key0",
      DurationForm: DurationForm,
      MoreDetails: MoreDetails,
      hourobj: hour_drop[0],
      dropdownData: hour_drop,
      type: "",
      minTime: null,
      maxTime: null,
      from: moment().format("h:m A"),
      to: moment().format("h:m A"),
      availableFrom: moment().format("YYYY-MM-DD"),
      availableTo: moment().format("YYYY-MM-DD"),
      days: [],
      selectedType: {
        id: 1,
        name: "Normal"
      },
      checkboxes: [
        {
          id: 0,
          name: "SUN",
          _checked: false
        },
        {
          id: 1,
          name: "MON",
          _checked: false
        },
        {
          id: 2,
          name: "TUE",
          _checked: false
        },
        {
          id: 3,
          name: "WED",
          _checked: false
        },
        {
          id: 4,
          name: "THU",
          _checked: false
        },
        {
          id: 5,
          name: "FRI",
          _checked: false
        },
        {
          id: 6,
          name: "SAT",
          _checked: false
        }
      ],
      select: [
        {
          id: 1,
          name: "Normal"
        },
        {
          id: 2,
          name: "Pax"
        },
        {
          id: 3,
          name: "Seats"
        }
      ]
    };
  }

  selectDays = (value, item, i) => {};

  selectCheckBox = item => {
    var selectedType = this.state.selectedType;
    selectedType = item;
    this.setState({ selectedType });
  };

  updateFrom = date => {
    this.setState({
      from: date
    });
  };
  updateTo = date => {
    this.setState({
      to: date
    });
  };



  updateAFrom = date => { 

    // console.log('check future',
    //   moment(this.state.availableTo).isAfter(date)
    // );
    if(this.state.availableFrom == this.state.availableTo || !moment(this.state.availableTo).isAfter(date) )
    this.setState({
      availableTo:date
    })
    this.setState({
      availableFrom: date,
    }); 
    
  };

  minTime = date => {
    this.setState({
      minTime: date
    });
  };

  maxTime = date => {
    this.setState({
      maxTime: date
    });
  };

  submitAddForm = (finalPax,allseats,state) => {
// alert(JSON.stringify(state));
    this.setState(
      {
        isAvailable: false,
        showBusinessForm: false,
        isSlotsBooking: true,
        finalPax: finalPax,
        addSeat: allseats,
        businessFormState:state
      },
      () => {
      }
    ); 

    //uncheck all plan type    
    if(this.state.selectedType==2){
    var datas = this.state.datas;
    this.state.datas.map((run, k) => {
      datas[k].checked = false;
    });
    this.setState({ datas });
    }


    //end 

    
  };
  reciveSlotType=(data)=>{
    this.setState({slot_type:data});
   // var businessFormState=this.state.businessFormState;
   // businessFormState.slot_type=data;
   // this.setState({businessFormState});
  }
  updateATo = date => {
    this.setState({
      availableTo: date
    });
  };

  totalChecked() {
    return this.state.checkboxes.filter(props => props._checked).length;
  }

  isActiveSelect(id) {
    return this.state.checkboxes.filter(props => {
      if (props.id === id && props._checked == true) return props;
    }).length;
  }

  toggleCheckbox = id => {
    if (this.totalChecked() > 1 && this.isActiveSelect(id) == 0) return;

    let changedCheckbox = this.state.checkboxes.find(cb => cb.id === id);
    changedCheckbox._checked = !changedCheckbox._checked;
    let chkboxes = this.state.checkboxes;
    for (let i = 0; i < chkboxes.length; i++) {
      if (chkboxes[i].id === id) {
        chkboxes.splice(i, 1, changedCheckbox);
      }
    }
    this.setState({ checkboxes: chkboxes });
  };

  editBusinessForm = () => {
    this.setState({
      showBusinessForm: true,
      isAvailable: false
    });
  };

  componentWillReceiveProps(props) { 

    if (this.props.totalState != null ){
  
      // console.log('intowillreceive',this.props.totalState)
this.state = this.props.totalState;
this.forceUpdate()
    } 

     if (props.datas) {
       var availabilityDetails = this.state.availabilityDetails;
       availabilityDetails = props.datas;
       this.setState({ availabilityDetails });
     }
 
  } 

  
  onValueChange = value => {
    this.setState({
      selected: value
    });
  };

  componentWillMount() {  

      this.props.headertext({
        headertext: (
          <Text
            style={{
              fontSize: hp("1.7%"),
              fontWeight: "bold",
              color: color.blue
            }}
          >
            Availability
          </Text>
        )
      });
    this.props.labeltext({
      labeltext: (
        <Text>
          <Text style={{ color: color.orange }}>{this.props.roomname}</Text>{" "}
          Please add your{" "}
          <Text style={{ color: color.orange, fontWeight: "bold" }}>
            {" "}
            Availability{" "}
          </Text>{" "}
        </Text>
      )
    });
  }
  loadDays = id => {
    var datareturn = datas.filter((obj, key) => obj.id == id);
    if (id != "") {
      return datareturn.length > 0 ? datareturn[0].name : "";
    } else {
      return "";
    }
  }; 


  slotsCompSubmit=(data)=>{ 
    this.setState({ slotsBookingState: data });
  }

  availabilitySubmit=(data)=>{ 

   // alert(JSON.stringify(data.slotsBookingState))
   this.setState({ slotsBookingState: data.slotsBookingState },()=>{ 
     
    
    const {
      from,
      to,
      availableFrom,
      availableTo,
      selectedType,
      checkboxes,
      minTime,
      maxTime
    } = this.state;

    const generateData = {
      from: from,
      to: to,
      availableFrom: availableFrom,
      availableTo: availableTo,
      checkboxes: checkboxes.filter(data => data._checked == true),
      slot_type:this.state.slot_type,
      venue_type:selectedType.id
    };
// alert(JSON.stringify(generateData));
      // console.log("slotsBookingState", this.state.slotsBookingState);
      var typedetails=this.state.datas.filter((obj)=>obj.checked==true);
      var filtered_checked=typedetails.length>0?typedetails.map((obj)=>obj.id).join(','):'';
      // alert(filtered_checked);
     data.avail_type=filtered_checked;
     data.availabilityState = this.state;
     // data.availabilityState.type=filtered_checked;
     data.businessForm=generateData;
     data.paxContent=this.state.finalPax;
     data.seatList=this.state.addSeat;
     // console.log("slotsBookingStateprops",data);
     this.props.getSlots(data);
   });
   

  } 

  updateCloseAddFormModal=()=>{
    this.setState({showBusinessForm:false})
    // this.props.updateCloseModal({
    //   isAvailable: false,
    //   showBusinessForm: false,
    //   isSlotsBooking: true
    // });

  }
  changeVenueType = (data, key, hourdata) => {
    // console.log('pressed',data);
    const { selectedType } = this.state; 
    if (selectedType.id == 2) return;


  const { datas } = this.state;
   var check = this.state.datas.findIndex(item => item.id == data.id);
   if (check != -1) {
     // console.log("check", check);
     // console.log("res", !datas[check].checked);
     datas[check].checked = !datas[check].checked;
     this.setState({ datas }, () => {
       // console.log("updated", this.state.datas);
     });
   }
   
    // var availabilityDetails = this.state.availabilityDetails;
    // availabilityDetails.type = data.id;
    // this.setState({ availabilityDetails });
    // this.setState({ activeId: data.id });
    // var mydropData = null; 

    // if (data.id == 1) {
    //   this.setState({ dropdownData: hour_drop });
    //   this.setState({ hourobj: hourdata ? hourdata : hour_drop[0] });
    //   mydropData = hour_drop[0];
    //   // this.props.availablitydays(data);
    // } else if (data.id == 2) {
    //   this.setState({ dropdownData: [] });
    //   this.setState({ hourobj: hourdata ? hourdata : null });
    //   mydropData = null;
    // } else if (data.id == 3) {
    //   this.setState({ dropdownData: week_drop });
    //   this.setState({ hourobj: hourdata ? hourdata : week_drop[0] });
    //   mydropData = week_drop[0];
    // } else {
    //   this.setState({ dropdownData: month_drop });
    //   this.setState({ hourobj: hourdata ? hourdata : month_drop[0] });
    //   mydropData = month_drop[0];
    // }
    // if (!hourdata) {
    //   this.props.availablitydays(mydropData);
    // }
      // var typedetails=this.state.datas.filter((obj)=>obj.checked==true);
      // var filtered_checked=typedetails.length>0?typedetails.map((obj)=>obj.id).join(','):'';
      // alert(filtered_checked);
    // this.props.sendcircleid(filtered_checked); 
    
  };

  pickerData = data => {};
  sendFromDate = date => {
    // alert(JSON.stringify(date));
    var availabilityDetails = this.state.availabilityDetails;
    availabilityDetails.from = date.from;
    availabilityDetails.to = date.to;
    this.setState({ availabilityDetails });
    this.props.fromtodays(date);
    // alert(new Date(date.from));
  };
  receiveDropdown = data => {
    // alert(JSON.stringify(data));
    this.setState({ hourobj: data });
    this.props.availablitydays(data);
    // return data.name;
  };
  getmoredetails = data => {
    var availabilityDetails = this.state.availabilityDetails;
    availabilityDetails.moredetails = data;
    this.setState({ availabilityDetails });
    this.props.moredetails(data);
    // alert(data);
  };
  render() {
    const { businessForm } = this.state;
     var typedetails=this.state.datas.filter((obj)=>obj.checked==true&&obj.id==1);
     var ischeckhourly=typedetails.length>0?true:false;
    // console.log('availa',this.state.availabilityDetails.type)

    return (
      <Content>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-between",
            paddingTop: 12,
            paddingLeft: 12,
            paddingRight: 12
          }}
        >
          {this.state.datas.map((obj, key) => {
            return (
              <View key={key} style={[styles.circleStyle]}>
                {!obj.activeId && (
                  <CircleText
                  disabled={this.state.selectedType.id==2}
                    svg={true}
                    image={obj.icon}
                    sendText={data => {
                      this.changeVenueType(data, key);
                    }}
                    style={[styles.flexWidth]}
                    width={10}
                    height={10}
                    text={obj.name}
                    data={obj}
                    active={obj.checked == true}
                  />
                )}

                {/* 
                {!obj.activeId &&
                  this.state.availabilityDetails.type == obj.id && (
                    <View>
                      <View style={styles.activeparent}>
                        <Triangle
                          width={hp("5%")}
                          height={hp("2%")}
                          color={color.black1}
                          direction={"up"}
                        ></Triangle>
                      </View>
                      <View style={[styles.activeparent, { top: -hp("1.6%") }]}>
                        <Triangle
                          width={hp("5%")}
                          height={hp("2%")}
                          color={color.ash1}
                          direction={"up"}
                        ></Triangle>
                      </View>
                    </View>
                  )}  */}
              </View>
            );
          })}
        </View>
          <View style={{ borderTopWidth: 1, borderColor: color.black1 }}>
            {1 == 0 && this.state.hourobj && (
              <View
                style={[
                  styles.borderDays,
                  {
                    flexDirection: "row",
                    borderWidth: 1,
                    borderColor: color.black1,
                    backgroundColor: color.ash1,
                    width: "100%",
                    alignItems: "center"
                  }
                ]}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                    paddingLeft: hp("2%"),
                    paddingRight: hp("2%"),
                    paddingTop: 20,
                    paddingBottom: 20
                  }}
                >
                  <View style={{ flex: 0.2 }}>
                    <Text style={{ color: color.orange, fontSize: hp("2%") }}>
                      {this.loadDays(
                        this.state.availabilityDetails
                          ? this.state.availabilityDetails.type
                          : ""
                      )}
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 0.8,
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <Dropdown_conatiner
                      sendDropdownData={this.receiveDropdown}
                      fullwidth={true}
                      drop_items={"Do"}
                      keyvalue={"value"}
                      values={this.state.dropdownData}
                      value={
                        this.state.hourobj
                          ? this.state.hourobj.value
                          : "please select"
                      }
                    />
                  </View>
                </View>
              </View>
            )}

            {this.state.isSlotsBooking &&  (
              <View
                style={{
                  display: "flex",
                  paddingVertical: 10
                }}
              >
                <SlotsBooking
                  // slotsBookingState={this.state.slotsBookingState}
                  updateCurrentList={this.slotsCompSubmit}
                  sendSlot_type={(data)=>this.setState({slot_type:data})}
                  splitData={this.state.splitSlots}
                  checkhourly={ischeckhourly}
                  splittedData={(data)=>this.setState({splitSlots:data})}
                  _editBusinessForm={this.editBusinessForm}
                  _type={this.state.availabilityDetails.type}
                  getSlots={this.availabilitySubmit}
                  slotChange={(data)=>this.reciveSlotType(data)}
                  _addForm={{
                    fromTime: this.state.from,
                    toTime: this.state.to,
                    slot_type:this.state.slot_type,
                    fromDate: this.state.availableFrom,
                    toDate: this.state.availableTo,
                    selectedType: this.state.selectedType,
                    paxList:this.state.finalPax,
                    seatList:this.state.addSeat.filter((obj)=>obj.add!='true'),
                    exclude: this.state.checkboxes.filter(
                      item => item._checked == true
                    )
                  }}
                />
              </View>
            )}

            {this.state.isAvailable && (
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  paddingHorizontal: 30,
                  paddingVertical: 30,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text style={{ fontSize: 22 }}>
                  <Icon
                    active
                    name="info-circle"
                    style={{ fontSize: 26 }}
                    type="FontAwesome"
                  />{" "}
                  Add Business Details {"  "}
                </Text>
                <Button
                  rounded
                  primary
                  onPress={() => this.setState({ showBusinessForm: true })}
                >
                  <Icon type="FontAwesome" name="plus" />
                </Button>
              </View>
            )}

            {this.state.showBusinessForm==true && (
              <ModalComp
                titlecolor={color.orange}
                visible={this.state.showBusinessForm}
                closemodal={() => this.updateCloseAddFormModal()}
              >
                <View style={{ paddingVertical: 50 }}>
                  <AddForm
                    _selectDays={this.selectDays}
                    _selectCheckBox={this.selectCheckBox}
                    _toggleCheckbox={this.toggleCheckbox}
                    _select={this.state.select}
                    _checkboxes={this.state.checkboxes}
                    selectedType={this.state.selectedType}
                    from={this.state.from}
                    to={this.state.to}
                    minTime={this.state.minTime}
                    maxTime={this.state.maxTime}
                    availableFrom={this.state.availableFrom}
                    availableTo={this.state.availableTo}
                    _updateFrom={this.updateFrom}
                    _updateTo={this.updateTo}
                    _updateAFrom={this.updateAFrom}
                    _updateATo={this.updateATo}
                    _submitAddForm={this.submitAddForm}
                    _minTime={this.minTime}
                    _maxTime={this.maxTime}
                    _finalPax={this.state.finalPax}
                    _addSeat={this.state.addSeat} 
                    _state={this.state.businessFormState}
                  />
                </View>
              </ModalComp>
            )}

            {/* <AccordionView
              sendDateObj={this.state.availabilityDetails}
              sendFromDate={this.sendFromDate}
              datas={this.state.DurationForm}
            />
            <Line />
           <AccordionView sendDateObj={this.state.availabilityDetails} moredetailsdata={(data)=>this.getmoredetails(data)}  datas={this.state.MoreDetails}/>*/}
          </View>
        
      </Content>
    );
  }

  componentDidUpdate = (prevProps, prevState) => {

  };
generateVenueType=(type)=>{
  if(type==1){
    return {id: 1,
        name: "Normal"}
  }else if(type==2){
   return {id: 2,
        name: "Pax"} 
  }else {
   return {id: 3,
        name: "Seat"} 
  }
} 
  componentDidMount() {
    if (this.props.datas.businessForm) {
                                         var availDatas = this.props.datas;
                    
                    if (availDatas.businessForm.checkboxes.length<7){
                          
                       this.state.checkboxes.map((item,i)=>{
                         let findIndex = availDatas.businessForm.checkboxes.findIndex((list)=>list.id ==item.id); 
                         if(findIndex == -1)
                         {
                           availDatas.businessForm.checkboxes.push(item);
                         }
                       })   


                       availDatas.businessForm.checkboxes.sort((a,b)=>{
                        return a.id - b.id
                       })

                    }
                    // alert(JSON.stringify(availDatas.businessForm.checkboxes));
                      this.setState({
                        from: availDatas.businessForm.from,
                        to: availDatas.businessForm.to,
                        slot_type: availDatas.businessForm.slot_type,
                        availableFrom: availDatas.businessForm.availableFrom,
                        availableTo: availDatas.businessForm.availableTo,
                        splitSlots: availDatas.slots,
                        isSlotsBooking: true,
                        isAvailable: false,
                        finalPax: availDatas.paxContent,
                        addSeat: availDatas.seatList,
                        selectedType: this.generateVenueType(
                          availDatas.businessForm.venue_type
                        ),
                        checkboxes: availDatas.businessForm.checkboxes
                      });
                                         if (
                                           availDatas.avail_type.length != ""
                                         ) {
                                           var datas = this.state.datas;
                                           datas.map(obj1 => {
                                             var status = availDatas.avail_type.includes(
                                               obj1.id.toString()
                                             );
                                             if (status == true) {
                                               obj1.checked = true;
                                             } else {
                                               obj1.checked = false;
                                             }
                                             return obj1;
                                           });
                                           this.setState({ datas });
                                         }
                                       }
    //   if (this.props.datas.type) {
    //     this.changeVenueType(this.props.datas.type, "", this.props.datas.days);
    //   } else { 
   
    //     this.props.sendcircleid(this.state.datas);
    //     this.props.availablitydays(hour_drop[0]);
    //   }
    //   if (this.props.datas.from) {
    //     var availabilityDetails = this.state.availabilityDetails;
    //     availabilityDetails.from = this.props.datas.from;
    //     this.setState({ availabilityDetails });
    //   }
    //   if (this.props.datas.to) {
    //     var availabilityDetails = this.state.availabilityDetails;
    //     this.setState({ todate: this.props.datas.to });
    //     availabilityDetails.to = this.props.datas.to;
    //     this.setState({ availabilityDetails });
    //   }
    //   if (this.props.datas.moredetails) {
    //     var availabilityDetails = this.state.availabilityDetails;
    //     availabilityDetails.moredetails = this.props.datas.moredetails;
    //     this.setState({ availabilityDetails });
    //   }
    // }
  }
}

const styles = StyleSheet.create({
  circleStyle: {
    width: wp("20%")
  },

  triangle: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid"
  },
  arrowUp: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
    borderTopWidth: 0,
    borderRightWidth: 12,
    borderBottomWidth: 12,
    borderLeftWidth: 12,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: color.grey,
    borderLeftColor: "transparent"
  },
  borderDays: {
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderLeftColor: "transparent",
    borderRightColor: "transparent"
  },
  bordertriangle: {
    position: "absolute",
    left: 0,
    right: 0,
    margin: "auto",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center"
  },
  activeparent: {
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999999,
    margin: "auto",
    left: 0,
    right: 0
  }
});

export default Availability;
