import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator
} from "react-native";
import color from "../Helpers/color";

import AsyncStorage from "@react-native-community/async-storage";
import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  List,
  ListItem,
  Item,
  Input,
  Thumbnail,
  Left,
  Body,
  Right,
  Textarea,
  Label,
  Button,
  ScrollableTab,
  Icon,
  Form
} from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import moment from "moment";
import links from "../Helpers/config";
import DateFunctions from "../Helpers/DateFunctions";
import ChatView from "./chatView";
import VenueChatList from "./venueChatList";
import Toast from "react-native-simple-toast";

const default_avator = "../images/avatar.png";
export default class myMessage extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     activeTab: 0,
                     showMessage: false,
                     showVenueChatList: false,
                     changedText: "",
                     changedText1: "",
                     myVenue: [],
                     otherVenue: [],
                     cmyVenue: [],
                     cotherVenue: [],
                     error: false,
                     showLoading: false,
                     messages: [],
                     venueChatMember: [],
                     activeVenue: null,
                     activeReceive: null,
                     clear: true,
                     private: false
                   };
                 }

                 onLoadMessage = item => {
                   this.setState(
                     { showMessage: true, activeVenue: item, clear: false },
                     () => {
                       this.getVenueMessage(item);
                     }
                   );
                 };

                 onLoadVenueMemberChat = item => {
                   this.setState({
                     showVenueChatList: false,
                     showMessage: true,
                     activeReceive: item,
                     clear: false
                   });
                   var customerUserId = item.chat_senderId;
                   var activeVenue = this.state.activeVenue;
                   activeVenue.customerUserId = customerUserId;
                   this.setState({ activeVenue }, () => {
                     this.getVenueMessage(activeVenue);
                   });
                 };

                 onClickVenue = item => {
                   this.setState(
                     { showVenueChatList: true, activeVenue: item },
                     () => {
                       this.getVenueMember(item);
                     }
                   );
                 };

                 changeFilteredData = data => {
                   this.setState({ changedText: data });
                   var fiteredRecords = this.state.cmyVenue.filter(obj =>
                     obj.trn_venue_name
                       .toLowerCase()
                       .includes(data.toLowerCase())
                   );
                   this.setState({ myVenue: fiteredRecords });
                 };

                 changeFilteredData1 = data => {
                   this.setState({ changedText1: data });
                   var fiteredRecords = this.state.cotherVenue.filter(obj =>
                     obj.trn_venue_name
                       .toLowerCase()
                       .includes(data.toLowerCase())
                   );
                   this.setState({ otherVenue: fiteredRecords });
                 };

            goBack=(i)=>{
               this.changeActiveTab(i)
               this.getAllVenues()
            }

                 componentWillMount = () => { 
                   this.checkingLogin()
                 };  


                 componentWillReceiveProps(props){
             
                 
                 }



                 async checkingLogin() {
                   const data = await AsyncStorage.getItem("loginDetails");

                   if (data != null) {
                     var parsedata = JSON.parse(data);
                     this.setState(
                       {
                         loginDetails: parsedata,
                         showloading: true
                       },
                       function() {
                         this.getAllVenues();
                       }
                     );
                   } else this.getAllVenues();
                 }

                 _renderList = (item, i) => { 
                   var image  = item.venue_image_path !=null ?item.venue_image_path:default_avator;
                   return (
                     <ListItem
                       avatar
                       onPress={() =>
                         i == 0
                           ? this.onClickVenue(item)
                           : this.onLoadMessage(item)
                       }
                     >
                       <Left>
                         <Thumbnail
                           source={{
                             uri:
                               image
                           }}
                         />
                       </Left>
                       <Body>
                         <Text>{item.trn_venue_name}</Text>
                         <Text numberOfLines={1} note>
                           {item.trn_venue_address}
                         </Text>
                       </Body>
                     </ListItem>
                   );
                 };
                 componentDidMount = () => {
                   console.log("mounted");
                       
                 }; 

                 getAllVenues = () => {
                       fetch(links.APIURL + "getChatList", {
                         method: "POST",
                         headers: {
                           Accept: "application/json",
                           "Content-Type": "application/json"
                         },
                         body: JSON.stringify({
                           userId: this.state.loginDetails
                             ? this.state.loginDetails.user_id
                             : "0"
                         })
                       })
                         .then(response => response.json())
                         .then(responseJson => {
                          //  console.log("getchatlist", responseJson);

                           if (responseJson.status != 0) {
                             Toast.show("No Records Found", Toast.LONG);
                             this.setState({ showloading: false });
                           } else if (
                             responseJson.status == 0 &&
                             responseJson.data.length > 0
                           ) {
                             var data = responseJson.data[0];
                             this.setState({
                               showloading: false,
                               myVenue: data.myVenueChatList,
                               otherVenue: data.otherVenueChatedList,
                               cmyVenue: data.myVenueChatList,
                               cotherVenue: data.otherVenueChatedList
                             });
                           }
                         });
                 }; 

                 refresh = () => {
                   console.log("timer called");
                   if (!this.state.clear) this.getVenueMessage();
                 };

                 pushMessage = item => {

                   var data = item;
                   data.chatSenderId = this.state.loginDetails
                     ? this.state.loginDetails.user_id
                     : "0";

                 

                  fetch(links.APIURL + "sendMessage", {
                    method: "POST",
                    headers: {
                      Accept: "application/json",
                      "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                  })
                    .then(response => response.json())
                    .then(responseJson => {
                      console.log("mesgresposn", responseJson);
                      if (responseJson.status == 0) {
                        this.getVenueMessage();
                      }
                    });
                 };

                 changeActiveTab = index => {
                   this.setState(
                     {
                       activeTab: Math.round(index),
                       clear: true,
                       messages: [],
                       showMessage: false,
                       activeVenue: "",
                       showVenueChatList: false,
                       activeReceive:''
                     },
                     () => console.log("tabs", this.state.clear)
                   );
                 };

                 getVenueMessage = item => {
                   const { activeVenue } = this.state;
                   console.log({
                     myUserId: this.state.loginDetails
                       ? this.state.loginDetails.user_id
                       : "0",
                     customerUserId:
                       activeVenue.customerUserId > 0
                         ? activeVenue.customerUserId
                         : activeVenue.venue_user_id,
                     venueId: activeVenue.venue_id
                   });

                   fetch(links.APIURL + "getChatMessages", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       myUserId: this.state.loginDetails
                         ? this.state.loginDetails.user_id
                         : "0",
                       customerUserId: activeVenue.customerUserId > 0
                           ? activeVenue.customerUserId
                           : activeVenue.venue_user_id,
                       venueId: activeVenue.venue_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log("chattestpsdfnosd", responseJson);

                       if (responseJson.status != 0) {
                         //  Toast.show("No Records Found", Toast.LONG);
                         this.setState({ showloading: false });
                       } else if (
                         responseJson.status == 0 &&
                         responseJson.data.length > 0
                       ) {
                         var data =
                           responseJson.data.length > 0
                             ? responseJson.data
                             : [];
                         this.setState({
                           showloading: false,
                           messages: data
                         });
                       }
                     });
                 };

                 getVenueMember = item => {
                      fetch(links.APIURL + "getProviderChatListMembers", {
                        method: "POST",
                        headers: {
                          Accept: "application/json",
                          "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                          myUserId: this.state.loginDetails
                            ? this.state.loginDetails.user_id
                            : "0",
                          venueId: item.venue_id
                        })
                      })
                        .then(response => response.json())
                        .then(responseJson => {
                          console.log(responseJson);

                          if (responseJson.status != 0) {
                            Toast.show("No Records Found", Toast.LONG);
                            this.setState({ showloading: false });
                          } else if (
                            responseJson.status == 0 &&
                            responseJson.data.length > 0
                          ) {
                            var data =
                              responseJson.data.length > 0
                                ? responseJson.data
                                : [];
                            this.setState({
                              showloading: false,
                              venueChatMember: data
                            });
                          }
                        });
                 };

                 _closePressed = () => {
                   this.setState({
                     showVenueChatList: false
                   });
                 };

                 render() {
                   const {
                     showLoading,
                     myVenue,
                     otherVenue,
                     error,
                     messages,
                     venueChatMember,
                     showVenueChatList,
                     activeVenue,
                     clear
                   } = this.state;

                   if (showLoading) {
                     return (
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "center",
                           alignItems: "center"
                         }}
                       >
                         <ActivityIndicator size="large" color="#073cb2" />
                         <Text>Please Wait</Text>
                       </View>
                     );
                   }
                   return (
                     <View
                       style={{
                         flex: 1,
                         justifyContent: "center",
                         alignItems: "center"
                       }}
                     >
                       <Tabs
                         tabBarUnderlineStyle={{
                           borderBottomWidth: 4,
                           borderBottomColor: "#5067FF"
                         }}
                         initialPage={this.state.activeTab}
                         onScroll={index => {
                           this.changeActiveTab(index);
                         }}
                         renderTabBar={() => (
                           <ScrollableTab
                             style={{ backgroundColor: "white" }}
                           />
                         )}
                       >
                         <Tab
                           style={styles.tab}
                           heading="My Venues"
                           tabStyle={{ backgroundColor: "white" }}
                           textStyle={{ color: "orange" }}
                           activeTabStyle={{
                             backgroundColor: "transparent"
                           }}
                           activeTextStyle={{
                             color: "#5067FF",
                             fontWeight: "normal"
                           }}
                         >
                           {!this.state.showMessage &&
                             this.state.activeTab == 0 && (
                               <View>
                                 <Header
                                   searchBar
                                   rounded
                                   noBorder
                                   style={{ backgroundColor: "white" }}
                                 >
                                   <Item style={{ backgroundColor: "white" }}>
                                     <Input
                                       value={this.state.changedText}
                                       onChangeText={data =>
                                         this.changeFilteredData(data)
                                       }
                                       placeholder="Search"
                                       style={{ paddingLeft: 5 }}
                                     />

                                     <TouchableOpacity
                                       onPress={() =>
                                         this.changeFilteredData("")
                                       }
                                     >
                                       <Icon
                                         type="FontAwesome"
                                         name="times-circle-o"
                                         style={{ color: color.black1 }}
                                       />
                                     </TouchableOpacity>
                                   </Item>
                                   <Button transparent>
                                     <Text>Search</Text>
                                   </Button>
                                 </Header>
                                 <ScrollView>
                                   <List>
                                     {myVenue.map((item, i) => {
                                       return this._renderList(item, "0");
                                     })}
                                   </List>
                                 </ScrollView>
                                 <VenueChatList
                                   data={venueChatMember}
                                   isPromoShow={showVenueChatList}
                                   closePressed={this._closePressed}
                                   onPress={this.onLoadVenueMemberChat}
                                   venue={this.state.activeVenue}
                                 />
                               </View>
                             )}

                           {this.state.activeTab == 0 &&
                             this.state.showMessage && (
                               <View style={{ flex: 1 }}>
                                 <List
                                   style={{ backgroundColor: "white" }}
                                   noBorder
                                   noIndent
                                 >
                                   <ListItem onPress={() => this.goBack(0)}>
                                     <Text
                                       style={{
                                         color: color.orange,
                                         fontSize: 18
                                       }}
                                     >
                                       {this.state.activeVenue.trn_venue_name}
                                     </Text>
                                   </ListItem>
                                   <ListItem
                                     avatar
                                     noBorder
                                     noIndent
                                     onPress={() => this.goBack(0)}
                                   >
                                     <Left>
                                       <Thumbnail
                                         source={{
                                           uri:
                                             this.state.activeVenue
                                               .venue_image_path != null
                                               ? this.state.activeVenue
                                                   .venue_image_path
                                               : default_avator
                                         }}
                                       />
                                     </Left>
                                     <Body>
                                       <Text>
                                         {this.state.activeReceive.user_name}
                                       </Text>
                                       <Text note>Venue Provider</Text>
                                     </Body>
                                   </ListItem>
                                 </List>

                                 <ChatView
                                   messages={messages}
                                   userId={activeVenue}
                                   refresh={this.refresh}
                                   clear={this.state.clear}
                                   pushMessage={this.pushMessage}
                                 />
                               </View>
                             )}
                         </Tab>

                         <Tab
                           style={styles.tab}
                           heading="Other Venues"
                           tabStyle={{ backgroundColor: "white" }}
                           textStyle={{ color: "orange" }}
                           activeTabStyle={{
                             backgroundColor: "transparent"
                           }}
                           activeTextStyle={{
                             color: "#5067FF",
                             fontWeight: "normal"
                           }}
                         >
                           {!this.state.showMessage &&
                             this.state.activeTab == 1 && (
                               <View>
                                 <Header
                                   searchBar
                                   rounded
                                   noBorder
                                   style={{ backgroundColor: "white" }}
                                 >
                                   <Item style={{ backgroundColor: "white" }}>
                                     <Input
                                       value={this.state.changedText1}
                                       onChangeText={data =>
                                         this.changeFilteredData1(data)
                                       }
                                       placeholder="Search"
                                       style={{ paddingLeft: 5 }}
                                     />

                                     <TouchableOpacity
                                       onPress={() =>
                                         this.changeFilteredData1("")
                                       }
                                     >
                                       <Icon
                                         type="FontAwesome"
                                         name="times-circle-o"
                                         style={{ color: color.black1 }}
                                       />
                                     </TouchableOpacity>
                                   </Item>
                                   <Button transparent>
                                     <Text>Search</Text>
                                   </Button>
                                 </Header>
                                 <ScrollView>
                                   <List>
                                     {otherVenue.map((item, i) => {
                                       return this._renderList(item, 1);
                                     })}
                                   </List>
                                 </ScrollView>
                               </View>
                             )}
                           {this.state.activeTab == 1 &&
                             this.state.showMessage && (
                               <View style={{ flex: 1 }}>
                                 <List
                                   style={{ backgroundColor: "white" }}
                                   noBorder
                                   noIndent
                                 >
                                   <ListItem
                                     avatar
                                     noBorder
                                     noIndent
                                     onPress={() => this.goBack(1)}
                                   >
                                     <Left>
                                       <Thumbnail
                                         source={{
                                           uri:
                                             this.state.activeVenue
                                               .venue_image_path != null
                                               ? this.state.activeVenue
                                                   .venue_image_path
                                               : default_avator
                                         }}
                                       />
                                     </Left>
                                     <Body>
                                       <Text>
                                         {this.state.activeVenue.trn_venue_name}
                                       </Text>
                                       <Text numberOfLines={1} note>
                                         {
                                           this.state.activeVenue
                                             .trn_venue_address
                                         }
                                       </Text>
                                     </Body>
                                   </ListItem>
                                 </List>

                                 <ChatView
                                   messages={messages}
                                   userId={activeVenue}
                                   refresh={this.refresh}
                                   clear={this.state.clear}
                                   pushMessage={this.pushMessage}
                                 />
                               </View>
                             )}
                         </Tab>
                       </Tabs>
                     </View>
                   );
                 }
               }


               const styles = StyleSheet.create({
                 tab: {
                   backgroundColor: "#edeef2"
                 }
               });