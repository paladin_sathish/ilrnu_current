import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, StatusBar, Modal, Image, TouchableHighlight, TouchableOpacity, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Icon, Button, Subtitle, Header, Content, Body, Right, Title, Item, Input } from 'native-base';
import color from '../Helpers/color';
import ModalComp from '../components/ModalComp'
import LabelTextbox from '../components/labeltextbox'
import ValidationLibrary from '../Helpers/validationfunction';
import links from '../helpers/config';
var formkeys = [{ name: 'name', type: 'default', labeltype: 'text' }, 
                { name: 'surname', type: 'default', labeltype: 'text' }, 
                { name: 'location', type: 'default', labeltype: 'text' },
                { name: 'mobile', type: 'default', labeltype: 'codeinput' },
                { name: 'mail', type: 'default', labeltype: 'text' },
                { name: 'dob', type: 'default', labeltype: 'DOB' }]
export default class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            location: '',
            mobile: { mobile: '', countrycode: '' },
            mail: '',
            dob: null,
            validations: {
                'name': {
                    error: null,
                    mandatory: true,
                    errormsg: '',
                    validations: [{ name: 'required', status: false }]
                },
                'surname': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: [{ name: 'required', status: false }]
                },
                'location': {
                    error: null,
                    errormsg: '',
                    mandatory: false,
                    validations: []
                },
                'mobile': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: [{ name: 'required', status: false }, { name: 'mobile', status: false }]
                },
                'mail': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: [{ name: 'required', status: false }, 
                    { name: 'email', status: false }]
                },
                'dob': {
                    error: null,
                    errormsg: '',
                    mandatory: true,
                    validations: []
                }
            }
        }

    }
    changeText = (data, key) => {
        // alert(data);
        var errormsg = ValidationLibrary.checkValidation(data, this.state.validations[key].validations);
        var validations = this.state.validations;
        if (key == 'dob') {
            if (data == 'errordob') {
                validations[key].error = true;
                validations[key].errormsg = 'Invalid Date Accept Format (dd-mm-yyyy)';
            } else {
                validations[key].error = false;
            }
        } else if (key == 'mobile') {
            errormsg = ValidationLibrary.checkValidation(data.mobile, this.state.validations[key].validations);
            validations[key].error = !errormsg.state;
            validations[key].errormsg = errormsg.msg;

        } else {
            validations[key].error = !errormsg.state;
            validations[key].errormsg = errormsg.msg;
        }
        this.setState({ validations });
        this.setState({
            [key]: data });
    }

    verify = () => {
        // alert(JSON.stringify(this.state));
        var obj = this.state;
        obj.type = 'otpscreen';
        var result = Object.keys(obj.validations).filter((object, key) => {
            if (object == 'dob') {
                if (obj.validations[object].error == null) {
                    this.changeText('errordob', object);
                }

            } else if (object == 'mobile') {
                this.changeText(this.state[object], object);

            } else {
                this.changeText(this.state[object], object);

            }
            return obj.validations[object].mandatory == true && obj.validations[object].error == true;
        });
        // alert(result.length)
        if (result.length == 0) {


            fetch(links.APILINK + 'SendOTP', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        'mobileNumber': this.state.mobile.mobile,
                        'countryCode': this.state.mobile.countrycode,
                        'email': this.state.mail
                    }),
                }).then((response) => response.json())
                .then((responseJson) => {
                   alert(JSON.stringify(responseJson))
                    if (responseJson.status == 0) {

                        this.props.loadverify(obj);
                    } else {
                        alert(responseJson.msg);
                    }
                })
        }

    }

    render() {
        return (
		            <Content>
				<Header noShadow={true} style={{backgroundColor:'transparent',margin:12}}>
				<Body style={{alignItems:'flex-start',justifyContent:'center',padding:0}}>
		            <Title style={{color:color.orange,fontSize:hp('4%')}}>Signup</Title>
		            <Subtitle style={{color:color.black,fontSize:hp('2.2%')}}>As a Venue Provider</Subtitle>
		            </Body>
		            </Header>
				<View>
				<View style={{padding:20}}>
				{formkeys.map((obj,key)=>{
					return(
						<LabelTextbox inputtype={obj.type} key={key} type={obj.labeltype} capitalize={obj.name!='dob'} error={this.state.validations[obj.name].error} errormsg={this.state.validations[obj.name].errormsg} changeText={(data)=>this.changeText(data,obj.name)} value={this.state[obj.name]} labelname={obj.name}/>
						)
				})}
		         <View>
		             <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
		       <Button onPress={()=>this.verify()}  style={styles.actionbtn}>
		            <Text style={styles.actionbtntxt}>VERIFY</Text>
		          </Button>
		          </View>
		          </View>
		          </View>
		          <View style={styles.sociallogin}>

		     </View>
		     </View>
		     	</Content>
        )
    }

}
const styles = {

    circle: {
        width: hp('17%'),
        height: hp('17%'),
        borderRadius: hp('17%') / 2,
        marginTop: '5%',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: color.orange,
    },
    innercircle: {
        position: 'absolute',
        width: hp('15.5%'),
        height: hp('15.5%'),
        borderRadius: hp('15.5%') / 2,
        margin: 'auto',
        backgroundColor: color.ash,
        margin: 0,
    },
    loginheader: {
        marginTop: hp('2%'),
        alignItems: 'center'
    },
    logintitle: {
        fontSize: hp('3%'),
        color: color.orange
    },
    loginsubtitle: {
        fontSize: hp('2%'),
        color: color.black1,
    },
    texboxpadding: {
        marginBottom: 20,
    },
    submitbox: {
        flex: 1,
        flexDirection: 'row',
        padding: 12,
        borderBottomWidth: 0.5
    },
    actionbtn: {
        borderRadius: 5,
        width: hp('17%'),
        justifyContent: 'center',
        backgroundColor: color.orange
    },
    actionbtntxt: {
        textAlign: 'center',
        color: color.white,
        fontSize: hp('2.3%')
    },
    sociallogin: {
        backgroundColor: color.ash1,
        height: hp('7%'),
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    socialiconlogin: {
        marginTop: 10,
        flexDirection: 'row'
    },
    socialicon: {
        width: hp('4%'),
        height: hp('4%'),
        margin: 5
    },
    texboxpadding: {
        marginBottom: 20,
    },
}