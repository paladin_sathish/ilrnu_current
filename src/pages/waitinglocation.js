'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  Text,
  Animated
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import logo from "../images/svg/Header_logo.png";
import gps from '../images/gps.png';

  
class WaitingLocation extends Component {
	constructor(props) {
	  super(props);
 this.state = {fadeIn: new Animated.Value(0),
                    fadeOut: new Animated.Value(1),
                   };
   }

   fadeIn() {
     Animated.timing(
       this.state.fadeIn,           
       {
         toValue: 1,                   
         duration: 1000,              
       }
     ).start(() => this.fadeOut());                        
  }

  fadeOut() {

    Animated.timing(                  
       this.state.fadeIn,            
       {
         toValue: 0,                   
         duration: 1000,              
       }
    ).start(()=>this.fadeIn());                        
  }
    
	componentDidMount(){
		this.fadeIn();
	}
  render() {
    return (
      <View style={{flex:1}}>
      <View style={{flex:0.3,justifyContent:'center',flexDirection:'row',marginTop:30}}>
      <Image source={logo} style={styles.imagelocation}/>
      </View>
      <View style={{flex:0.4,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
      <View style={{alignItems:'center',justifyContent:'center'}}>
      	<Image source={gps} style={styles.gpslocation}/>
      	<Animated.View  style={{
        opacity: this.state.fadeIn,         // Bind opacity to animated value
      }}>
      	<Text style={styles.gpsmsg}>Getting Location Please Wait....</Text>
      	</Animated.View>
      	</View>
      </View>
      <View style={{flex:0.3}}>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
imagelocation:{
	   width:hp('17.5%'),
      height:hp('6.16%'),
},
gpslocation:{
	width:hp('12%'),
	height:hp('12%')
},
gpsmsg:{
	marginTop:12,
	fontSize:hp('2.5%')
}
});


export default WaitingLocation;