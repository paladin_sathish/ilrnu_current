'use strict';

import React, { Component } from 'react';
import CircleText from'../components/circlecomp';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import ModalComp from '../components/Modal';
import AmentiesList from '../pages/amentieslist';
import Dropdown_conatiner from '../components/drpdown_container'
import {Left,List,Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox,Accordion } from 'native-base';
import {
  StyleSheet,
  View,
  Text,
  Picker,
  TextInput,
  TouchableOpacity,Image
} from 'react-native';
import Triangle from '../components/triangles';
import FromToDuration from '../pages/fromtoduration';
import AccordionView from '../components/accordion';
import PhotoPlus from '../images/svg/plus.png';
import links from '../Helpers/config';


 class VenueTags extends Component {

  constructor(props) {
    super(props);
   const newtags = JSON.parse(JSON.stringify(props.mytags))
    this.state = {mynwtags:newtags,amentiesvisible:false,activeId:1,selected:'key0',tags:[],doDropdown:{id:'venue_act_id',name:'venue_act_name',dropdown:[]},whatDropdown:{id:'venue_purpose_id',name:'venue_purpose_name',dropdown:[]},purposes:[]};
    // alert(datas.length%2);
  }
  onValueChange=(value)=>{
    this.setState({
      selected: value
    });
  }
  componentWillReceiveProps=(props)=>{
    // alert(JSON.stringify(props.tagDetails));
    //   var tags=this.state.tags
    // }
     var mynwtags=this.state.mynwtags;
     if(props.tagDetails){
    for(var i in props.tagDetails){

     var findindex=mynwtags.findIndex((obj,key)=>obj.id==props.tagDetails[i].id);
     if(findindex!=-1){
     for(var k in props.tagDetails[i].tags){
        var secondindex=mynwtags[findindex].tags.findIndex((obj2,key2)=>obj2.id==props.tagDetails[i].tags[k].id);
        console.log(secondindex);
        if(secondindex!=-1){
        mynwtags[findindex].tags[secondindex]=props.tagDetails[i].tags[k];
      }else{
        mynwtags[findindex].tags.push(props.tagDetails[i].tags[k])
      }
     }
    this.setState({mynwtags});
   }
    }
  }
    // alert(JSON.stringify(tags));
  
  }

   loadActions=(data)=>{
   console.log('datadropdown',data);
    fetch(links.APIURL+'do', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      // alert(JSON.stringify(responseJson));
      if(responseJson.status==0){
           var doDropdown=this.state.doDropdown;
        responseJson.data.map((obj)=>obj.purpose=[]);
   
  
        // var doDropdown=this.state.doDropdown;
        // responseJson.data.map((obj)=>obj.purpose=[]);
    // doDropdown.dropdown=responseJson.data;

    if(data){
      for(var i in responseJson.data){

        var findIndex=data.findIndex((obj)=>obj.venue_act_id==responseJson.data[i].venue_act_id);
        if(findIndex!=-1&&data[findIndex].purpose){
            responseJson.data[i].purpose=data[findIndex].purpose;
        }
      }
    }
     doDropdown.dropdown=responseJson.data;
    this.setState({doDropdown});
//     console.log(responseJson.data);
//     this.setState({doDropdown})
// console.log("do",this.state.doDropdown);
}
    })  
 }

  componentWillMount(){
this.props.headertext({headertext:"Tagging","showskip":false});
    this.props.labeltext({labeltext:<Text> Please tag related <Text style={{color:color.orange,fontWeight:'bold'}}> Keyword </Text> </Text>});
  }
  sendtags=()=>{
    const tags = this.state.tags.map(a => Object.assign({}, a));
    var filtertags=tags.filter((obj)=>{
      return obj.data.some( function (obj1) {
      return obj1.isChecked==true
    });
    })
  filtertags.map((obj)=>{
  obj.data=obj.data.filter((obj1)=>obj1.isChecked==true)});
  // alert(JSON.stringify(filtertags));
  this.props.sendTags(this.state.tags,filtertags);


  }
  changeTextBox=(i1,i2,state)=>{

    var tags=this.state.tags;
    tags[i1].data[i2]['isChecked']=!state;
    this.setState({tags});
    this.sendtags();

  }
  addtags=(textkey,tagkey)=>{
    if(this.state[textkey]){
      var tags=this.state.tags;
      tags[tagkey].data.push({tag_cat_id:tags[tagkey].data.length+1,tag_name:this.state[textkey],isChecked:true});
      this.setState({tags});
      this.setState({[textkey]:null});
    this.sendtags();
    }
  }
      loadTags=()=>{
// alert("apicalling");
    fetch(links.APIURL+'getTags', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({user_cat_id:"1"}),
    }).then((response)=>response.json())
    .then((responseJson)=>{

      var response=responseJson.data;
      // console.log("response datas",response);
      response.map(v => v.data.map(d => d.isChecked = false));
      // console.log(response)
      
      // this.setState({tags:response})
      // alert(JSON.stringify(this.props.tagDetails))
      if(this.props.tagDetails){
        for(var i in this.props.tagDetails){
          var findIndex=response.findIndex((obj)=>obj.tag_cat_id==this.props.tagDetails[i].tag_cat_id);
          if(findIndex!=-1){
            response[findIndex]=this.props.tagDetails[i];
          }
        }
      }
        this.setState({tags:response});
      // alert(JSON.stringify(responseJson));
      // this.setState({ tags: response,loading:false});

    }) 
  }
   loadwhat(data){
    // console.log("whatpurpose",data);
    fetch(links.APIURL+'whatpurpose', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
    // alert("what",this.state.whatDropdown);
    var whatDropdown=this.state.whatDropdown;
    if(responseJson.data.length>0){

     responseJson.data.map((d) => d.isChecked = false);
    }
      var findData=this.state.purposes.filter((obj)=>obj.id==data);
      // console.log(findData);
    for(var i in responseJson.data){
      if(findData.length>0){


      var checkIndex=findData[0].value.findIndex((obj)=>obj==responseJson.data[i].venue_purpose_id);

     // var checkIndex=this.state.purposes.findIndex((obj)=>obj.==responseJson.data[i].venue_purpose_id);
     if(checkIndex!=-1){
     responseJson.data[i].isChecked=true;
     }else{
     responseJson.data[i].isChecked=false;
     }
   }else{
     responseJson.data[i].isChecked=false;
   }
    }
    // alert(JSON.stringify(this.state.purposes));
    whatDropdown.dropdown=responseJson.data;
    console.log(whatDropdown);
    this.setState({whatDropdown})
    })  
  }
  selectPurpose=(index,checked)=>{
    var whatDropdown=this.state.whatDropdown;
    var doDropdown=this.state.doDropdown;
    var purposes=this.state.purposes;
    whatDropdown.dropdown[index].isChecked=checked;
    var filterPurposes=whatDropdown.dropdown.filter((obj)=>obj.isChecked==true);
    var findActionKey=doDropdown.dropdown.findIndex((obj)=>obj.venue_act_id==this.state.doData.venue_act_id);
    if(findActionKey!=-1){
      doDropdown.dropdown[findActionKey].purpose=filterPurposes;
    }
    var findIndex1=this.state.purposes.findIndex((obj1)=>obj1.id==this.state.doData.venue_act_id);

     if(purposes.length==0){

    purposes.push({id:this.state.doData.venue_act_id,value:filterPurposes.map((obj)=>obj.venue_purpose_id)});
    }else{
      if(findIndex1!=-1){
        purposes[findIndex1].value=filterPurposes.map((obj)=>obj.venue_purpose_id);
        // purposes[findIndex1].value.unique();
      if(filterPurposes.length==0){
        purposes.splice(findIndex1,1);
      }
      }else{
        purposes.push({id:this.state.doData.venue_act_id,value:filterPurposes.map((obj)=>obj.venue_purpose_id)});
      }
      // purposes[i]
    }
    this.props.sendpurposes&&this.props.sendpurposes(purposes,doDropdown.dropdown,purposes.length);

    this.setState({whatDropdown,doDropdown,purposes});
  }
  sendDropdownData=(data,key)=>{
    this.setState({[key]:data});
    this.loadwhat(data.venue_act_id);

  }
   removePurposes=(purposeid,checked,obj,index,purposeIndex)=>{
var whatDropdown=JSON.parse(JSON.stringify(this.state.whatDropdown));
var doDropdown=JSON.parse(JSON.stringify(this.state.doDropdown));
var purposes=JSON.parse(JSON.stringify(this.state.purposes));

var FindIndex=whatDropdown.dropdown.findIndex((obj)=>obj.venue_purpose_id==purposeid);
var finddata=purposes.findIndex((obj1)=>obj1.id==obj.venue_act_id);
var purposeIndex1=purposes[finddata].value.findIndex((obj)=>obj==purposeid);
purposes[finddata].value.splice(purposeIndex1,1);
if(purposes[finddata].value.length==0){
  purposes.splice(finddata,1);
}
doDropdown.dropdown[index].purpose.splice(purposeIndex,1);
this.setState({purposes,doDropdown});
if(FindIndex!=-1){
  if(obj.venue_act_id==this.state.doData.venue_act_id){
whatDropdown.dropdown[FindIndex].isChecked=checked;
}else{
}
}
// this.props.sendPurposes(purposes.length);
this.props.sendpurposes&&this.props.sendpurposes(purposes,doDropdown.dropdown,purposes.length);
console.log("PURPOSES",purposes);
this.setState({whatDropdown});
  }
  renderChosenPurpose=(obj,index)=>{
    if(obj.purpose.length>0){
      return(
          <View >
          <Text>{obj.venue_act_name}</Text>
          <View style={{flexDirection:'row',flexWrap:'wrap',marginTop:12}}>
          {obj.purpose.map((objpurpose,key2)=>{
            return(
              <TouchableOpacity onPress={()=>this.removePurposes(objpurpose.venue_purpose_id,!objpurpose.isChecked,obj,index,key2)} key={key2} style={[styles.tagContainer,objpurpose.isChecked==true?styles.activeBG:'']}>
         <Text style={[styles.containerText,objpurpose.isChecked==true?styles.acitveText:'']}>{objpurpose.venue_purpose_name}</Text>
         </TouchableOpacity>
              )
          })}
          </View>
          </View>
        )
    }
  }
  render(){
    return(
      <View style={{flex:1,marginTop:0,padding:10}}>
     

      <Dropdown_conatiner
                                drop_items={'What'}
                                 // action={true}
                                 _borwidth={1} _borColor={color.black1}  brdrradius={1}
                                
                                 fullwidth={true}
                                values={this.state.doDropdown.dropdown}
                                keyvalue={"venue_act_name"}
                                value={this.state.doData?this.state.doData.venue_act_name:'What'}
                                sendDropdownData={(data)=>this.sendDropdownData(data,'doData')}
                                // _width={50}
                            />
    <View style={{flexDirection:'row',flexWrap:'wrap',marginTop:12}}>
         {this.state.whatDropdown.dropdown.map((obj,key2)=>{
           return(
            <TouchableOpacity key={key2} onPress={()=>this.selectPurpose(key2,!obj.isChecked)} style={[styles.tagContainer,obj.isChecked==true?styles.activeBG:'']}>
         <Text style={[styles.containerText,obj.isChecked==true?styles.acitveText:'']}>{obj.venue_purpose_name}</Text>
         </TouchableOpacity>
         )
         })}
        
         </View>
    <View style={{flexDirection:'row',flexWrap:'wrap',marginTop:12}}>
    {this.state.doDropdown.dropdown.map((obj,index)=>{
      return this.renderChosenPurpose(obj,index)
    })}
</View>



      {this.state.tags.map((obj,key1)=>{
        return(
 <View key={key1} style={styles.containerview}>
         <View style={styles.addContainer}>
           <TextInput placeholder={obj.tag_cat_desc?obj.tag_cat_desc:obj.tag_cat_name} value={this.state['key'+key1]} onChangeText={(text)=>{this.setState({['key'+key1]:text})}} style={styles.addButtonColor} />
           {/*<TouchableOpacity disabled={!this.state['key'+key1]} onPress={()=>this.addtags('key'+key1,key1)}><Icon type="MaterialIcons" style={{fontSize:hp('5%'),color:this.state['key'+key1]?color.orange:color.black1}} name="add-circle-outline"/></TouchableOpacity>*/}
           <TouchableOpacity disabled={!this.state['key'+key1]} onPress={()=>this.addtags('key'+key1,key1)}><Image source={PhotoPlus} style={{width:hp('4.5%'),height:hp('4.5%')}}/></TouchableOpacity>
         </View>
         <View style={{flexDirection:'row',flexWrap:'wrap'}}>
         {obj.data.map((obj,key2)=>{
           return(
            <TouchableOpacity onPress={()=>this.changeTextBox(key1,key2,obj.isChecked)} key={key2} style={[styles.tagContainer,obj.isChecked==true?styles.activeBG:'']}>
         <Text style={[styles.containerText,obj.isChecked==true?styles.acitveText:'']}>{obj.tag_name}</Text>
         </TouchableOpacity>
         )
         })}
        
         </View>
         </View>
          )

      })}
     
        
        
      </View>
      )
  }
    componentDidMount(){
    this.loadTags();
    // this.loadActions();
     if(this.props.purposeDatas){
    this.setState({purposes:JSON.parse(JSON.stringify(this.props.purposeDatas.purposes))});
    this.loadActions(JSON.parse(JSON.stringify(this.props.purposeDatas.purposeDropdown)));
  }else{
    this.loadTags();
    this.loadActions();
  }
  }
}
const styles={
  tagContainer:{
    borderColor:color.orange,marginRight:12,alignSelf: 'flex-start',borderWidth:0.7,padding:7,fontSize:hp('2.5%'),width:'auto',marginBottom:12
  },
  addButtonColor:{borderWidth:1,borderColor:color.black1,width:'80%',height:'75%',marginRight:12},
  addContainer:{flexDirection:'row',alignItems:'center',width:'100%',marginBottom:5},
  containerText:{fontSize:hp('2%'),color:color.blueactive},
  activeBG:{backgroundColor:color.orange},
  acitveText:{color:color.white},
  containerview:{borderBottomWidth:0.6,borderColor:color.black1,paddingTop:5,paddingBottom:5}
}
export default VenueTags;