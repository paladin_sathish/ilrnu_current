import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Tabs, Tab, TabHeading,Content,Root } from 'native-base';
import Toast from 'react-native-simple-toast';
import { View, Image, StatusBar, Picker, Text, TouchableOpacity, ScrollView,ActivityIndicator , BackHandler,ToastAndroid} from 'react-native';
import color from '../Helpers/color';
import links from '../Helpers/config';
import HeaderComp from '../components/HeaderComp';
import SubHeaderComp from '../components/subHeader';
import Triangle from '../components/triangles';
import VenueType from '../pages/venuetype';
import VenueSuccess from '../pages/VenueSuccess';
import FooterComp from '../components/Footer';
import FacilityType from '../pages/facilitytype';
import FacilityDetails from '../pages/facilitydetails';
import Corporate_AddDetails from '../pages/Corporate_AddDetails';
import CorporateVenueDetails from '../pages/CorporateVenueDetails';
import CorporateFacilities from '../pages/CorporateFacilities';
import Corporatesuccess from '../pages/Corporatesuccess';
import CorporateReview from '../pages/CorporateReview';
import Availability from '../pages/availability';
import Ameneties from '../pages/ameneties';
import VenueTags from '../pages/venutags';
import VenuePrice from '../pages/venueprice';
import VenueUpload from '../pages/venueupload';
import VenueReview from '../pages/venuereveiw';
import { Actions } from 'react-native-router-flux';
import Chair from '../images/svg/chair.png';
import DeskF from '../images/svg/deskf.png';
import DeskM from '../images/svg/desk.png';
import Access from '../images/svg/accessibility.png';
import Laptop from '../images/svg/laptop.png';
import Employee from '../images/svg/employee.png';
import Seats from '../images/svg/seats.png';
import Bathroom from '../images/svg/bathroom.png';
import Parking from '../images/svg/parking.png';
import Coffee from '../images/svg/coffee-cup.png';
import Door from '../images/svg/door.png';
import Hourly from '../images/svg/Hourly.png';
import Daily from '../images/svg/Daily.png';
import Weekly from '../images/svg/Weekly.png';
import AmenitiesPNG from '../images/TabPNG/AmenitiesPNG.png';
import AvailabilityPNG from '../images/TabPNG/AvailabilityPNG.png';
import FacilityPNG from '../images/TabPNG/FacilityPNG.png';
import PricecardPNG from '../images/TabPNG/PricecardPNG.png';
import ReviewPNG from '../images/TabPNG/ReviewPNG.png';
import UploadphotoPNG from '../images/TabPNG/UploadphotoPNG.png';
import TaglistPNG from '../images/TabPNG/TaglistPNG.png';
import VenuetypePNG from '../images/TabPNG/VenuetypePNG.png';
import Calendar from '../images/svg/calendar.png';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const datas=[{name:'Seating',id:1,circleCount:null,active:false,icon:Chair},{name:'Physical\nInfrastructure',id:2,icon:DeskF},{name:'Accessibility',id:3,icon:Access},{name:'Training Equipments',id:4,icon:Laptop},{name:'IT Infra',id:5,icon:DeskF},{name:'Resource',id:6,circleCount:null,active:false,icon:Seats},{name:'Toilets',id:7,icon:Bathroom},{name:'Parking',id:8,icon:Parking},{name:'Pantry',id:9,icon:Coffee},{name:'Additional Rooms',id:10,icon:Door}]

    datas.push({name:'Hourly',id:1,activeId:1})
    datas.push({name:'Hourly',id:1,activeId:1});
    const newtags=[{id:1,name:'Add (or) Tag the related General Keywords',tags:[{id:1,name:'Corporate Trainings',state:false},{id:2,name:'Course Trainings',state:false},{id:3,name:'Venue Special',state:false},{id:4,name:'Special Tag Items display here',state:false},{id:5,name:'Corporate Trainings',state:false}]},{id:2,name:'What category on venue type',tags:[{id:1,name:'Corporate Trainings',state:false},{id:2,name:'Course Trainings',state:false},{id:3,name:'Venue Special',state:false},{id:4,name:'Special Tag Items display here',state:false}]},{id:3,name:'Suitable Training- IT / Soft Skill / Handson',tags:[{id:1,name:'Corporate Trainings',state:false},{id:2,name:'Course Trainings',state:false},{id:3,name:'Venue Special',state:false}]}]
     // const newtags1=[{id:1,name:'Add (or) Tag the related General Keywords',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:2,name:'What category on venue type',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]},{id:3,name:'Suitable Training- IT / Soft Skill / Handson',tags:[{id:1,name:'corporate training',state:false},{id:2,name:'corporate training1',state:false},{id:1,name:'corporate training2',state:false}]}]
export default class CorporateForm extends Component {
    constructor(props) {
        super(props);
        var venueform=JSON.parse(JSON.stringify(require('../Helpers/venueform.json')));
        this.state = {loginDetails:this.props.navigation.state.params.logindata,facilityerror:null,commonArray:null,locationArray:null,facilityvalidations:null,facilityIndex:0,currentIndex:0,facitlityactive:null, activeparent: 'tab1',venuetypetext:'', list: [{ id: '1', name: 'tab1', image: VenuetypePNG}, { id: '2', name: 'tab2', image: FacilityPNG }, { id: '3', name: 'tab3', image: AvailabilityPNG }, { id: '4', name: 'tab4', image: AmenitiesPNG}], _scrollToBottomY: 0,facilityData:{roomname:'',seats:'',floor:'',address:'',landmark:'',mobile:'',mail:''},venuetypedata:null,facilitytypedata:null,availability:{type:null,days:null,from:null,to:null},ameneties:null,venTags:null,venueprice:null,venueform:venueform,venueformData:new FormData(),validations:[{id:1,name:'venuetype',errmsg:'Choose Venue Type',state:false},{id:2,name:'venuetype',errmsg:'Choose Venue Facility',state:false},{id:3,name:'facility',errmsg:'Please Fill up facility details',state:false},{id:4,name:'availability',errmsg:'Please Fill up avaialability details',state:false},{id:5,name:'price',errmsg:'Please Fill up price Details',state:false}]}
    }
    goToRight = () => {
        this.scroll.scrollTo({ x: 0, y: 0, animated: true });
    }
    changeTab = (data,key) => {
            this.setState({currentIndex:key});
        this.setState({ activeparent: data });
        if(data=='tab2'){
            this.setState({facitlityactive:1})
        }else{
            this.setState({facitlityactive:null})

        }
    }

async componentWillMount(){
    const data=await AsyncStorage.getItem('loginDetails');
    var parsedata=JSON.parse(data);
    var venueform=this.state.venueform;
    console.log(parsedata.user_id);
    venueform.venue.user_id=parsedata.user_id;
    this.setState({venueform});

    // console.log(data);
   // console.log("asynstore",AsyncStorage.getItem('@loginDetails'));
   }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        // ToastAndroid.show('Back button is disabled', ToastAndroid.SHORT);
        Actions.venuepage();
        return true;
    }


    renderActiveTab() {
        return (
            <View style={styles.activeparent}>
        	
		   </View>
        )
    }
    changeLabelText=(data)=>{
    	this.setState({venuetypetext:data.labeltext})
    }
    sendfaciltiydata=(data,data1,error)=>{
        this.setState({facilitytypedata:data})
        this.setState({commonArray:data1});
        var venueform=this.state.venueform;
        venueform.venue.venue_name=data1[0].spec_det_datavalue1;
        venueform.venue.venue_address=data1[1].spec_det_datavalue1;
        venueform.venue.venue_location=data1[1].spec_det_datavalue2;
        venueform.specific=data.specDetails;
        if(error!=null){
            // alert(error);
        var validations=this.state.validations;
        validations[2].state=error;
        this.setState({validations});      
        }
        // var facilityData=this.state.facilityData;
        // facilityData[data.key]=data.value;
        // this.setState({facilityData});
        // this.setState({facilityvalidations:data.validations})
        // venueform.facility[data.dbkey]=data.value;
        this.setState({venueform});
       
    }
    sendvenuetypedata=(data)=>{
        this.setState({venuetypedata:data});
        var validations=this.state.validations;
        validations[0].state=true;
        this.setState({validations})
        var venueform=this.state.venueform;
        venueform.venue.venue_cat_id=data.venue_cat_id;
        // venueform.venue.venuename=data.venue_cat_name;
        this.setState({venueform})

    } 
    sendfacilitytypedata=(data)=>{
        // this.setState({commonArray:null});
        this.setState({facilitytypedata:data});
        var venueform=this.state.venueform;
        var validations=this.state.validations;
        validations[1].state=true;
        this.setState({validations})
        venueform.venue.venue_spec_id=data.venue_spec_id;
        this.setState({venueform})
        

    }
    loadFacility=()=>{
    	if(this.state.facilityIndex==0){
    		return(<FacilityType venuetypedata={this.state.venuetypedata} facilitytypeData={this.state.facilitytypedata} sendfacilitytypedata={this.sendfacilitytypedata}  labeltext={this.changeLabelText}/>)
    	}else{
    		return(<FacilityDetails commonArray={this.state.commonArray}  facilityData={this.state.facilitytypedata} sendfaciltiydata={this.sendfaciltiydata} labeltext={this.changeLabelText}/>)
    	}

    }
    gotoback=()=>{
        if(this.state.currentIndex!=0){
                if(this.state.activeparent=='tab2'){
            if(this.state.facilityIndex==1){
                this.setState({facilityIndex:0})
                this.setState({facitlityactive:1})
            }else{
                var currentitem=parseInt(this.state.currentIndex)-1;
          var currentitemname=this.state.list[currentitem].name;
          this.setState({activeparent:currentitemname});
          this.setState({currentIndex:currentitem})
            }
        }else{
            var currentitem=parseInt(this.state.currentIndex)-1;
          var currentitemname=this.state.list[currentitem].name;
          this.setState({activeparent:currentitemname});
          this.setState({currentIndex:currentitem})

      }
    
      }        
    }
    gotoNext=()=>{

        if(this.state.currentIndex!=this.state.list.length-1){

          var currentitem=parseInt(this.state.currentIndex)+1;

          // alert(this.state.list[currentitem].name)
          var currentitemname=this.state.list[currentitem].name;
          // alert(currentitem);
          this.setState({activeparent:currentitemname});
          this.setState({currentIndex:currentitem})
          this.setState({facilityIndex:0});
          this.setState({facitlityactive:true});
    
        }
    }
    availablitydays=(data)=>{
        var availability=this.state.availability;
        availability.days=data;
        this.setState({availability});
        var venueform=this.state.venueform;
        venueform.availability.venue_days=data.value;
        this.setState({venueform});
       

    }
    availcircleData=(data)=>{
        var availability=this.state.availability;
        availability.type=data;
        this.setState({availability})
        var venueform=this.state.venueform;
        venueform.availability.venue_avail_type=data.id;
        this.setState({venueform});
    }
    fromtodays=(data)=>{
        var availability=this.state.availability;
        availability.from=data.from;
        availability.to=data.to;
        this.setState({availability});
        if(data.from=="" || data.to==""){
             var validations=this.state.validations;
        validations[3].state=false;
        }else{
        var validations=this.state.validations;
        validations[3].state=true;
        }
        this.setState({validations})
        var venueform=this.state.venueform;
        venueform.availability.venue_avail_frm=availability.from;
        venueform.availability.venue_avail_to=availability.to;
        this.setState({venueform});

    }
    AmenetiesData=(data)=>{
        // var ameneties=this.state.ameneties;
        // ameneties=data;
        // ameneties=data;
        this.setState({ameneties:data});
        // this.setState({ameneties})
        // var arrayofameneties=[];
        // for(var i in ameneties){
        //     for(var y in ameneties[i].amenetiesList){
        //     var filterobj={"trn_venue_amnts_id":ameneties[i].id,"trn_venue_amnts_name":"","trn_venue_amnts_qty":"","trn_venue_amnts_curr":0,"trn_venue_amnts_datetime":0,nameofamenties:''}
        //         filterobj.trn_venue_amnts_name=ameneties[i].amenetiesList[y].itemname;
        //         filterobj.trn_venue_amnts_qty=ameneties[i].amenetiesList[y].amt;
        //         filterobj.trn_venue_amnts_curr=ameneties[i].amenetiesList[y].currency;
        //         filterobj.trn_venue_amnts_datetime=ameneties[i].amenetiesList[y].hours;
        //         filterobj.trn_venue_amnts_datetime=ameneties[i].amenetiesList[y].hours;
        //         filterobj.nameofamenties=ameneties[i].name;
        //         arrayofameneties.push(filterobj);
        //     }

        // }
         var amenitiesData=data.length>0&&data.filter((obj)=>obj.circleCount>0);
    
    var amentiesArray=[];
    for(var i in amenitiesData){
        amentiesArray=amentiesArray.concat(amenitiesData[i].activeArray);
    }
    // alert(JSON.stringify(amentiesArray));
        var venueform=this.state.venueform;
        venueform.ameneties=amentiesArray;
        this.setState({venueform});
    }
    saveTags=(data,filterdata)=>{
        var venTags=this.state.venTags;
        venTags=data;
        this.setState({venTags});
        var tagarray=[];
        for(var i in filterdata){
            var obj={tag_cat_id:filterdata[i].tag_cat_id,tag_details:[]}
            for(var y in filterdata[i].data){
                obj.tag_details.push(filterdata[i].data[y].tag_name);
            }
            tagarray.push(obj);
        }
        // alert(JSON.stringify(tagarray));
        var venueform=this.state.venueform;
        venueform.tagdetails=tagarray;
        this.setState({tagarray});


    }
    sendPriceData=(data)=>{
        var venueprice=this.state.venueprice;
        venueprice=data;
        this.setState({venueprice});
        alert(JSON.stringify(data));
        if(data.amt==0 || data.amt==''){
              var validations=this.state.validations;
        validations[4].state=false;
        }else{
              var validations=this.state.validations;
        validations[4].state=true; 
        }
        this.setState({validations})
        var venueform=this.state.venueform;
        venueform.pricedetails.venue_price_amt=data.amt;
        venueform.pricedetails.venue_price_type=data.activeId;
        venueform.pricedetails.venue_price_currency=data.days;
        venueform.pricedetails.venue_price_name=data.type;
        this.setState({venueform});

    }
    venueImages=(data)=>{
        var venueformData=this.state.venueformData;
        venueformData=data.formimagearray;
         var venueform=this.state.venueform;
         venueform.formArray=data.localimages;
         this.setState({venueform})
        this.setState({venueformData});
    }
    submitVenuData=(data)=>{
        // alert(JSON.stringify(data))
        this.setState({loading:true});
        fetch(links.APIURL+'insert_corprate_venue/', {
     method: 'POST',
      headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
}).then((response)=>response.json())
   .then((responseJson)=>{
       if(responseJson.status==0){
this.setState({loading:null});
this.setState({activeparent:'success'});
}else{
this.setState({loading:null});
    if(responseJson.msg=='User failure'){
        Toast.show(responseJson.data.map((obj)=>obj).join(','), Toast.LONG)
 // Toast.show({
 //              text: responseJson.data.map((obj)=>obj).join(','),
 //              buttonText: 'Okay',
 //              position:'top',
 //              duration:5000
 //            })
    }else{
        Toast.show(responseJson.msg, Toast.LONG)
    }
}
   }) 
    }
    corporateLocation=(commonArray,location,error)=>{
        // this.setStatea
        this.setState({commonArray:commonArray});
        this.setState({locationArray:location});
        this.setState({facilityerror:error});
        // alert(JSON.stringify(commonArray));
        // alert(JSON.stringify(location));
    }
    errorData=(data)=>{
           
    }
    loadedit=()=>{
        this.setState({currentIndex:0});
        this.setState({activeparent:'tab1'});
        this.goToRight();
    }
    spocDetails=(data)=>{
        // alert(JSON.stringify(data));
        var findIndex=this.state.locationArray.findIndex((obj)=>obj.id==data.id)
        var locationArray=this.state.locationArray;
        locationArray[findIndex]=data;
        this.setState({locationArray});
    }
    closeSuccessModal=(data)=>{
        this.setState({activeparent:null});
        if(data=='individual'){
        Actions.venuepage({raiselogin:true});
        }else{
            Actions.home();
        }
    }
    render() {
        return (
            <Container>
            <Root>
            {this.state.loading&&
            <View style={{width:'100%',zIndex:9999,flex:1,justifyContent:'center',alignItems:'center',position:'absolute',height:'100%',backgroundColor:'rgba(0,0,0,0.8)',top:0}}>
        <ActivityIndicator size="large" color={color.orange} />
        <Text style={{color:color.white}}>Please Wait ....</Text>
        </View>
    }
            <SubHeaderComp bgcolor={color.top_red}>
			<View style={styles.subheading}>
			<Text style={styles.subheadingtext}>Corporate User - Add Venue</Text>
			</View>
			</SubHeaderComp>
			<Content>
            {this.state.activeparent!='success' && 
            <View>
			
        <View style={{flex:1,flexDirection:'row',alignItems:'center',padding:5,borderBottomWidth:0.5,borderColor:color.black1}}>
        	<View style={{flex:0.1}}>
        	<TouchableOpacity onPress={this.gotoback}>	
        	<Icon style={[styles.arrownavicon,{color:this.state.facitlityactive&&this.state.facilityIndex==1?color.orange:null}]} type="FontAwesome" name='angle-left'/>
        	</TouchableOpacity>
        	</View>
        	<View style={{flex:0.8,flexDirection:'row',alignItems:'center',justifyContent:'center',}}>
        	<Text numberOfLines={2} style={{fontSize:hp('2%'),textAlign:'center'}}>{this.state.venuetypetext}</Text>
        	</View>
        	<View style={{flex:0.1}}>
        	<TouchableOpacity onPress={this.gotoNext}>	
        	<Icon style={[styles.arrownavicon,{color:this.state.facitlityactive&&this.state.facilityIndex==0?color.orange:null}]} type="FontAwesome" name='angle-right'/>
        	</TouchableOpacity>
        	</View>
        </View>
        </View>
    }

        <View>
        {this.state.activeparent=='tab1'&&
        	<VenueType loginDetails={this.state.loginDetails} venuedetails={this.state.venuetypedata} sendvenuetypedata={this.sendvenuetypedata} labeltext={this.changeLabelText}/>
    	} 
        {this.state.activeparent=='tab2'&&
        <CorporateVenueDetails moveFacilities={()=>this.setState({activeparent:'tab3'})} commonArray={this.state.commonArray} locationArray={this.state.locationArray} sendCorporateVenueLocation={this.corporateLocation} venuetypedata={this.state.venuetypedata} labeltext={this.changeLabelText}/>
        }{this.state.activeparent=='tab3'&&
        <CorporateFacilities spocDetails={this.spocDetails} venuedetails={this.state.venuetypedata}  locationArray={this.state.locationArray} labeltext={this.changeLabelText}/>
        }{this.state.activeparent=='tab4'&&
   <CorporateReview loginDetails={this.state.loginDetails} submitCorporat={(data)=>this.submitVenuData(data)} labeltext={this.changeLabelText} facilityerror={this.state.facilityerror} editclick={()=>this.setState({activeparent:'tab1',currentIndex:0})} venuedetails={this.state.venuetypedata} commonArray={this.state.commonArray} locationArray={this.state.locationArray} />
        }{this.state.activeparent=='success'&&
         <Corporatesuccess  closemodal={(data)=>this.closeSuccessModal(data)} locationArray={this.state.locationArray} />
        }
        </View>
		</Content>
        
        
        </Root>
			</Container>
        )
    }

}

const styles = {
        active: {
            backgroundColor: '#e2e2e2',
            borderRightWidth: 0.5,
            borderColor: '#999999',
            zIndex: 99999
        },
        subheading: {
            padding: 12
        },
        subheadingtext: {
            fontSize: hp('2%'),
            color: color.white
        },
        imageparent: {
            width: hp('8%'),
            height: hp('7%'),
            alignItems: 'center',
            justifyContent: 'center',
            borderWidth: 0.5,
            borderColor: color.ash2
        },
        imageTab: {
            width: hp('4%'),
            height: hp('4%'),
        },
        activeparent: { position: 'absolute', right: -hp('1.4%'), top: hp('2%'), alignItems: 'center', justifyContent: 'center', zIndex: 999999 },
    bordertriangle: {
        position: 'absolute',
        left: 0,
        top: hp('0.23%'),
    },
    arrowbox: {
        width: hp('3%'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: color.blueactive
    },
    arrowboxicon: {
        color: color.black1,
        fontSize: hp('3.2%')
    },
    arrownavicon:{
    	fontSize:hp('4%'),
    	textAlign:'center',
    	color:color.ash2
    },
    activetextorange:{
    	color:color.orange
    }
}