"use strict";

import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text,Alert } from "react-native";
import ModalComp from "../components/ModalComp";
import AddNewVenue from '../pages/addNewVenue';
import color from "../Helpers/color";
import refer from "../images/referearn.png"; 
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

class CreateCourse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionTrue: false
    };
  }

  OpenAddCourse = () => {
      this.props._onClick();
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "flex-start",
          alignItems: "center",
          backgroundColor: color.white
        }}
      > 

        <Image
          source={refer}
          style={{ width: hp("20%"), height: hp("20%"), textAlign: "center" }}
        />
        <Text
          style={{
            color: color.orange,
            fontSize: hp("3.5%"),
            textAlign: "center",
            fontWeight: "bold"
          }}
        >
          New Course
        </Text>
        <Text
          style={{
            color: color.black1,
            fontSize: hp("2.5%"),
            textAlign: "center",
            flexWrap: "wrap",
            width: wp("65%"),
            paddingVertical: hp("1.5%")
          }}
        >
          Create a new course by adding photos, slides, PDF and work documents
          for your users
        </Text> 
        
        <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={() => this.OpenAddCourse()}>
            <Text
              style={{ color: "white", fontSize: hp("2%"), fontWeight: "bold" }}
            >
              Create Course
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: color.blueactive,
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 20,
    shadowOpacity: 0.25
  }
});

export default CreateCourse;
