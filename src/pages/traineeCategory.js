"use strict";

import React, { Component } from "react";
import CircleText from "../components/circlecomp";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import links from "../Helpers/config";
import { StyleSheet, View, Text } from "react-native";
import Work from "../images/svg/work.png";
import Education from "../images/svg/education.png";
import Conference from "../images/svg/presentation.png";
import Lab from "../images/svg/workplace.png";
import Cabins from "../images/svg/employee.png";


class TraineeCategory extends Component {
  constructor(props) {
    super(props);

    this.state = { facilitydata: null, facilityDetails: [] };
    // alert(datas.length%2);
  }
  componentWillReceiveProps(props) {
    if (props.facilitytypeData) {
      console.log("prps", props.facilitytypeData);
      this.setState({ facilitydata: props.facilitytypeData });
    }
  }
  componentWillMount() {
    this.props.headertext({
      headertext: (
        <Text
          style={{
            fontSize: hp("1.7%"),
            fontWeight: "bold",
            color: color.blue
          }}
        >
          Specific Training
        </Text>
      )
    });
    this.props.labeltext({
      labeltext: (
        <Text>
          Please choose the{" "}
          <Text style={{ color: color.orange, fontWeight: "bold" }}>
            {" "}
            Specific Training{" "}
          </Text>{" "}
        </Text>
      )
    });
  }
  changeVenueType = (data, key) => {
    this.setState({ facilitydata: data });
    this.props.sendfacilitytypedata(data);
  };
  loadSpecifVenue(data) {
    console.log(data)
    fetch(links.APIURL + "getTrainingSpecificCategoryList/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ CategoryId: data })
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log("fsdfsd", responseJson.data);
        if (responseJson.status == 0 && responseJson.data.length >0) {
          this.setState({ facilityDetails: responseJson.data });
        }else
        {
          this.setState({
            facilityDetails: {
              SpecificId:0,
              SpecificName:''
            }
          },()=>{
            this.props.sendfacilitytypedata(this.state.facilityDetails);
          });
        }
      });
  }

  render() {
    return (
      <View
        style={{ flex: 1, flexDirection: "row", flexWrap: "wrap", padding: 12 }}
      >
        {this.state.facilityDetails.length > 0 &&
          this.state.facilityDetails.map((obj, key) => {
            return (
              <View key={key} style={[styles.circleStyle]}>
                <CircleText
                  sendText={data => {
                    this.changeVenueType(data, key);
                  }}
                  style={[styles.flexWidth]}
                  width={12}
                  height={12}
                  text={obj.SpecificName}
                  image={obj.SpecificIcon}
                  data={obj}
                  active={
                    this.state.facilitydata &&
                    this.state.facilitydata.SpecificId == obj.SpecificId
                  }
                />
              </View>
            );
          })}
      </View>
    );
  }
  componentDidMount() {
    if (this.props.venuetypedata) {
      this.loadSpecifVenue(this.props.venuetypedata.CategoryId);
    }
    if (this.props.facilitytypedata) {
      this.setState({ facilitydata: this.props.facilitytypedata });
    }
  }
}

const styles = StyleSheet.create({
  circleStyle: {
    width: wp("30%"),
    flexDirection: "row",
    justifyContent: "center"
  },
  activename: {}
});

export default TraineeCategory;
