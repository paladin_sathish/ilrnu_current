import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,Alert,FlatList,ScrollView,ActivityIndicator} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button,Content} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'
import CircleComp from '../components/circlecomp'
import Dropdown_container from '../components/drpdown_container'
import Right_arrow from '../images/rightarrow.png'
import Modeldropdowncomp from '../components/ModeldropdownComp'
import links from '../Helpers/config';
import GroundNearYou from './SearchListingSlider';
import Toast from 'react-native-simple-toast';
import LoadingOverlay from '../components/LoadingOverlay'

var do_items=[{id:1,do_name:'Play'},{id:2,do_name:'Celebrate'},{id:3,do_name:'Party'},{id:4,do_name:'Gather'},
                {id:5,do_name:'Meet'},{id:6,do_name:'Conference'},{id:7,do_name:'Celebrate'},{id:8,do_name:'Celebrate'}]

var drop_down_options=['Celebrate', 'Play','Party','Gather','Meet','Conference']
function GroupArray(data){
    var arrays = [], size = 4;
while (data.length > 0)
    arrays.push(data.splice(0, size));
return arrays;
} 



export default class Venue_search extends Component{

        constructor(props){
            super(props)
           this.state= {loadingindicator:false,searchloading:null,venuedata:props.venuedetails,doDropdown:{id:'venue_act_id',name:'venue_act_name',dropdown:[]},activestate:true,doobj:null,whatDropdown:{id:'venue_purpose_id',name:'venue_purpose_name',dropdown:[]},whatactiveobj:null,whereDropdown:{id:'venue_cat_id',name:'venue_cat_name',dropdown:[]},whatobj:null,whereobj:null,activekey:1,searchsliderdata:props.data?props.data:[],arrowclicked:null,TOS:0,loading:null}
            //    _dropdown_4_onSelect(idx,value)
            //    {
            //     this.state={value:props.valuedet}
            //    }         
          }
          getvenueDetails=(offset)=>{
            this.setState({loadingindicator:true});
            var searchContent=this.props.navigation.state.params.searchContent;
            var nearme=this.props.navigation.state.params.nearme;
            var lat=this.props.navigation.state.params.latlng.latitude;
            var long=this.props.navigation.state.params.latlng.longitude;
            var searchsliderdata=this.state.searchsliderdata;
            // alert(this.props.navigation.state.params.searchContent);
             fetch(links.APIURL + "commonSearch", {
               method: "POST",
               headers: {
                 Accept: "application/json",
                 "Content-Type": "application/json"
               },
               body: JSON.stringify({
                 searchContent: searchContent,
                 nearme: nearme,
                 offset: offset,
                 lat: lat,
                 long: long
               })
             }).then((resp)=>resp.json())
             .then((respjson)=>{
                // alert(respjson.count)
               this.setState({loadingindicator:false});
               // alert(JSON.stringify(respjson));
               if(respjson.status==0){
               searchsliderdata=searchsliderdata.concat(respjson.data);
             }
               this.setState({searchsliderdata});
             })
          }
          sendDropdownData=(data,key,drop)=>{
            this.setState({arrowclicked:null})
           this.active_items(data,key,drop);
          }
          clearwhat=()=>{
            var whatDropdown=this.state.whatDropdown;
            whatDropdown.dropdown=[];
            this.setState({whatDropdown,whatobj:null});
          }
     clearwhere=()=>{
            var whereDropdown=this.state.whereDropdown;
            whereDropdown.dropdown=[];
            this.setState({whereDropdown,whereobj:null});
          }

      loaddo=()=>{
    console.log("loaddo")
    fetch(links.APIURL+'do', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
        // this.setState({doDropdown:responseJson.data});
        var doDropdown=this.state.doDropdown;
    doDropdown.dropdown=responseJson.data;
    this.setState({doDropdown})

    // alert(JSON.stringify(this.state.doDropdown.dropdown));
// console.log("do",responseJson);

    })  
    this.setState({arrowclicked:null})
  }
  loadwhat(data){
    console.log("data",data);
    fetch(links.APIURL+'whatpurpose', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
    // alert(JSON.stringify(responseJson));
    if(responseJson.data.length==0){
        if(this.state.activekey!=null){
      // alert("no data found");
      Toast.show("No Records Found", Toast.LONG);
        this.setState({activekey:1});
        this.setState({doobj:null})
        // alert(JSON.stringify(this.state.doDropdown))
        // this.loaddo();
        return;
    }
    }
         var whatDropdown=this.state.whatDropdown;
    whatDropdown.dropdown=responseJson.data;
    this.setState({whatDropdown})

   // console.log("what",responseJson);
       })  

    this.setState({arrowclicked:null})
  }
  loadwhere(data,data1){
    console.log("where_id",data);
    fetch(links.APIURL+'wherecategory', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':this.state.doobj.venue_act_id,'venue_purpose_id':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
      // alert(JSON.stringify(responseJson));
        if(responseJson.data.length==0){
    if(this.state.activekey!=null){
          // alert("no data found");
      Toast.show("No Records Found", Toast.LONG);

        this.setState({activekey:1});
        this.setState({whatobj:null})
        this.setState({doobj:null})
        // this.loaddo();
        return;
    }
}
         var whereDropdown=this.state.whereDropdown;
    whereDropdown.dropdown=responseJson.data;
    this.setState({whereDropdown})
    console.log("where",responseJson.data);

    })  

    this.setState({arrowclicked:null})
  } 
  arrowClick=(offset)=>{
    this.setState({searchloading:offset?false:true})
  fetch(links.APIURL+'dropdownSearchpurpose_new', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':this.state.doobj?this.state.doobj.venue_act_id:null,'venue_purpose_id':this.state.whatobj?this.state.whatobj.venue_purpose_id:null,'venue_cat_id':this.state.whereobj?this.state.whereobj.venue_cat_id:null,'TOS':this.state.TOS,offset:offset?offset:0}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
    this.setState({arrowclicked:true,searchloading:null})
        if(responseJson.status&&responseJson.status==1){
            Toast.show('Options are empty to search', Toast.LONG)
        }else{
             if(responseJson.data.length==0){
                  Toast.show('No Records Found', Toast.LONG)
             }else{
               if(offset){
                 var searchsliderdata=this.state.searchsliderdata;
                 searchsliderdata=searchsliderdata.concat(responseJson.data);
                 this.setState({searchsliderdata});
               }else{
                this.setState({searchsliderdata:responseJson.data});
               }
                // this.props.nxtpge(responseJson,'',this.state);
             }
        }
      // alert(JSON.stringify(responseJson));
         // Actions.Search_Home(JSON.stringify(responseJson));
        // alert(JSON.stringify(responseJson));
        // this.props.sendsliderdata(responseJson);

      

    })  
    //this.props.receivesearch(data);
    //this.setState({visible:true})
  }
  componentWillMount(){
    this.loaddo();
  }
  componentDidMount(){
    // this.getvenueDetails(0);
  }
          active_items=(data,key,drop)=>{     
          // alert(JSON.stringify(data)) ;
               // this.setState({venuedata:item.id});
               this.setState({[key]:data});
               if(key=='doobj'){
                this.loadwhat(data.venue_act_id);
                this.setState({whatobj:'',TOS:1})
                if(drop){
                      this.clearwhat();
                      this.clearwhere();
                }else{

                this.setState({activekey:2});
                }
                }else if(key=='whatobj'){
                this.loadwhere(data.venue_purpose_id,this.state.doobj.venue_act_id);
                this.setState({whereobj:'',TOS:2})
                if(drop){
                  this.clearwhere();
                }else{

                this.setState({activekey:3});
                }
                }else{

                    this.setState({activekey:null,TOS:3})
                }
         
          //      this.setState({value:item.do_name})
           
 
              }

            //   _dropdown_4_onSelect(idx,value)
            //   {
            //    alert(`idx=${idx}, value='${value}'`);
            //   }

            renderSeparator = () => {  
                return (  
                    <View  
                        style={{  
                            height: 1,  
                            width: "25%",  
                            backgroundColor: "#000",  
                            flexDirection:'row',
                            justifyContent:'center'
                        }}  
                    />  
                );  
            };  
            
componentWillReceiveProps(props){
    // console.log("backprops1",props);
    var loadingcheck=props.navigation.state.params.loading;
    var loadingcheckmsg=props.navigation.state.params.msg;
    var successpayment=props.navigation.state.params.successpayment;
        this.setState({loading:loadingcheck?loadingcheck:null});
    if(successpayment==true){
        // Toast.show(loadingcheckmsg,Toast.LENGTH_LONG)
        alert(loadingcheckmsg);
        // Toast.show(loadingcheckmsg,Toast.LONG)
    }else{
        // this.setState({})
        if(successpayment=='error'){
        Toast.show(loadingcheckmsg,Toast.LONG)
    }
    }
}
    render(){
        return(

        
        

<Content style={{backgroundColor:color.white}}>

            <View style={styles.container}> 

{this.state.loading&&
                <LoadingOverlay/>
                 
}
                {!this.props.actionshide &&
                <View style={[styles.top_container,styles.conten_container]}>

                    <View style={{flexDirection:'row',flexWrap:'wrap',marginTop:hp('2%'),
                                paddingLeft:wp('5%'),paddingRight:wp('5%'),  justifyContent:'flex-start',
                                  }}>
                        <View style={styles.view1}>
                            <Text style={styles.text_style1}>
                                I want to
                            </Text>
                        </View>
                        <View style={styles.view2}>
                            <Dropdown_container
                                style={{height:hp('3.7%'),padding:3,width:'100%'}}
                                // _width={wp('55%')}
                                fakewidth={true}
                               
                                   text_item={this.state.doobj?this.state.doobj.venue_act_name:''}
                                
                                values={this.state.doDropdown.dropdown}
                                 activestate={this.state.activekey}
                                 keyvalue={"venue_act_name"}
                                 value={this.state.doobj?this.state.doobj.venue_act_name:(!this.state.activekey?'Do':'')}
                                 
                                 sendDropdownData={(data)=>this.sendDropdownData(data,'doobj','drop')}
                                //  onSelect={(idx, value) => this._dropdown_4_onSelect(idx, value)}
                                    >
                            </Dropdown_container>    
                        </View>
                        {(this.state.doobj || !this.state.activekey)&&
                             <View style={styles.view2}>
                            <Dropdown_container
                             fakewidth={true}
                                style={{height:hp('3.7%'),padding:3,width:'100%'}}
                                   text_item={this.state.whatobj?this.state.whatobj.venue_purpose_name:''}
                                values={this.state.whatDropdown.dropdown}

                                 activestate={this.state.activekey}
                                 keyvalue={"venue_purpose_name"}
                                 value={this.state.whatobj?this.state.whatobj.venue_purpose_name:(!this.state.activekey?'What':'')}
                                 sendDropdownData={(data)=>this.sendDropdownData(data,'whatobj','drop')}
                                //  onSelect={(idx, value) => this._dropdown_4_onSelect(idx, value)}
                                    >
                            </Dropdown_container>    
                        </View>
                    }

                     {(this.state.whatobj || !this.state.activekey)&&
                             <View style={[styles.view2,{flexDirection:'row'}]}>
                             <Text style={{color:color.white,fontSize:hp('2%')}}> in </Text>
                            <Dropdown_container
                             fakewidth={true}
                                style={{height:hp('3.7%'),padding:3,width:'100%'}}
                                   text_item={this.state.whereobj?this.state.whereobj.venue_cat_name:''}
                                values={this.state.whereDropdown.dropdown}
                                 activestate={this.state.activekey}
                                  keyvalue={"venue_cat_name"}
                                 value={this.state.whereobj?this.state.whereobj.venue_cat_name:(!this.state.activekey?'Where':'')}
                                 sendDropdownData={(data)=>this.sendDropdownData(data,'whereobj','drop')}
                                //  onSelect={(idx, value) => this._dropdown_4_onSelect(idx, value)}
                                    >
                            </Dropdown_container>    
                        </View>
                    }
                        {/* <View style={styles.view2}>
                            <Dropdown_container style={{height:hp('3.7%'),padding:3}}
                            text_item={'Soccer'}>
                            </Dropdown_container> 
                        </View>
                        <View style={styles.view1}>
                            <Text style={styles.text_style1}>
                                in
                            </Text>    
                        </View>
                        <View style={styles.view2}>
                            <Dropdown_container style={{height:hp('3.7%'),padding:3}}
                            text_item={'Soccer Club Academy'}>
                                
                            </Dropdown_container> 
                        </View> */}
                        
                        <View style={styles.view3}>
{this.state.searchloading==true&&
 <TouchableOpacity style={{backgroundColor:color.blue,borderRadius:10,height:hp('4%'),paddingLeft:wp('3%'),paddingRight:wp('3%')}}>
                                <View  style={{alignSelf:"center",paddingTop:5,paddingBottom:5}}>

    <ActivityIndicator size="small" color={color.white} style={{paddingTop:0,paddingBottom:2}} />
                                    
                                </View>
                            </TouchableOpacity>
}
              {!this.state.searchloading==true&&
                            <TouchableOpacity  onPress={()=>this.arrowClick()} style={{backgroundColor:color.blue,borderRadius:10,height:hp('4%'),paddingLeft:wp('3%'),paddingRight:wp('3%')}}>
                                <View  style={{alignSelf:"center",paddingTop:5,paddingBottom:5}}>

                                    <Image style={{width:hp('2%'),height:hp('2%'),alignSelf:"center"}} 
                                        source={Right_arrow}>
                                </Image>
                                </View>
                            </TouchableOpacity>
                          }
                        </View>
                    </View>
       {/* do dropdown*/}
       {this.state.activekey==1&&
           <ScrollView horizontal={true}>
          
               <View style={{flexDirection:'row'}}>
               {this.state.doDropdown.dropdown.map((objdata,indexkey)=>{
                   return(
                         <View style={styles.list_style}>
                            <View>
                            <CircleComp
                            innerText={true}
                                sendText={(data)=>this.active_items(objdata,'doobj')}                            
                                text={objdata.venue_act_name} 
                                
                                 width={12} height={12} >
                            </CircleComp>
                            </View>
                        </View>
                       )
               })}
               </View>
             
           </ScrollView>

                     }
        {/* what dropdown*/}
        {this.state.activekey==2&&
              <ScrollView horizontal={true}>
           
               <View style={{flexDirection:'row'}}>
               {this.state.whatDropdown.dropdown.map((objdata,indexkey)=>{
                   return(
                         <View style={styles.list_style}>
                            <View>
                            <CircleComp
                            innerText={true}
                                sendText={(data)=>this.active_items(objdata,'whatobj')}                            
                                text={objdata.venue_purpose_name} 
                                
                                 width={12} height={12} >
                            </CircleComp>
                            </View>
                        </View>
                       )
               })}
               </View>
              
              </ScrollView>
                   
                     }
                 {/* where dropdown*/}
                     {this.state.activekey==3&&
                                  <ScrollView horizontal={true}>
              
               <View style={{flexDirection:'row'}}>
               {this.state.whereDropdown.dropdown.map((objdata,indexkey)=>{
                   return(
                         <View style={styles.list_style}>
                            <View>
                            <CircleComp
                            innerText={true}
                                sendText={(data)=>this.active_items(objdata,'whereobj')}                            
                                text={objdata.venue_cat_name} 
                                
                                 width={12} height={12} >
                            </CircleComp>
                            </View>
                        </View>
                       )
               })}
               </View>
              
              </ScrollView>
              
                     }
                </View>
              }
                { 

                this.state.searchsliderdata.length>0&&
                <GroundNearYou 
                sendScrollEnd={(data)=>{
                  if(this.state.arrowclicked==true){
                    this.arrowClick(this.state.searchsliderdata.length)
                  }else{
                    this.getvenueDetails(this.state.searchsliderdata.length)
                  }
                }}
                latlng={this.props.latlng} 
                arrowclicked={this.state.arrowclicked} 
                sendsliderdata={this.state.searchsliderdata}>
{this.state.loadingindicator==true &&
 <ActivityIndicator size="large" color={color.orangeBtn}  />
}
                </GroundNearYou>  
                
                }
                
           </View>          
                 </Content>
        )
    }
} 

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:color.white
    },
    top_container:{
        backgroundColor:color.orange,
        // minHeight:hp('30%'),
        justifyContent:'center',
        alignItems:'center',
           width:'100%',
           // padding:10
    },
    list_style:{
     //   flex:1,
     marginLeft:wp('1.25%'),
     marginRight:wp('1.25%'),
        marginTop:hp('2.5%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
    },
    circleStyle:{
        width:wp('20%'),
    },
    conten_container:{
   //     flex:1,
     //   flexDirection:'row',
       // justifyContent:'space-between',
       // flexWrap:'wrap',
        justifyContent:'space-between',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'space-between',
    },
    text_style1:{
        color:color.white,
        fontSize:hp('2')
    },
    view1:{
     //   flex:.3,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        paddingBottom:10
    },
    view2:{
       // flex:.5,
    //   width:'100%',
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        marginLeft:5,
        marginRight:5,
        paddingBottom:10,
    },
    view3:{
       // flex:.2,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        paddingBottom:10,
    },
    blue_btn:{
        height:hp('3.7%'),
        width:wp('10%'),
        justifyContent:'center',
        padding:3,
        backgroundColor:color.blue,
        borderRadius:15
    }

})