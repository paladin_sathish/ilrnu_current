import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,Alert,FlatList,ScrollView} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Text_box from '../components/text_box'
import ModalComp from '../components/Modal'
import DropdowContainer from '../components/drpdown_container';
import Line from '../components/Line';
import Cancel from '../images/cancel.png';
import CheckBox from '../components/checkbox1';
import LabelTextbox from '../components/labeltextbox';
import DynamicForm from '../components/DynamicForm';
import ValidationLibrary from '../Helpers/validationfunction';


var datas=[{id:1,Title:'BaseBall'},{id:2,Title:'Soccer'},{id:3,Title:'Soccer'}]

class Corporate_AddDetails extends Component{
    constructor(props) {
      super(props);
    
      this.state = { commonArray:[{
            "spec_det_id": 'idname',
            "venue_spec_id": 1,
            "spec_det_name": "SPOC Name",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "text",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'}],
            error:null,
            nostringformat:true,
            errormsg:''
        },{
            "spec_det_id": 'idname2',
            "venue_spec_id": 1,
            "spec_det_name": "Mail",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "text",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'},{name:'email'}],
            error:null,
            errormsg:''
        },{
            "spec_det_id": 'idname3',
            "venue_spec_id": 1,
            "spec_det_name": "Contact",
            "spec_det_sortorder": 0,
            "spec_det_datatype1": "text",
            "spec_det_datavalue1": "",
            "spec_det_datatype2": "",
            "spec_det_datavalue2": "",
            "spec_det_datatype3": "",
            "spec_det_datavalue3": "",
            validation:[{name:'required'},{name:'mobile'}],
            error:null,
            errormsg:''
        }]};
    }
          checkValidations=()=>{
        var commonArray=this.state.commonArray;
        for(var i in commonArray){
        var errorcheck=ValidationLibrary.checkValidation(commonArray[i].spec_det_datavalue1,commonArray[i].validation);
        commonArray[i].error=!errorcheck.state;
        commonArray[i].errormsg=errorcheck.msg;
        }
        var errordata=commonArray.filter((obj)=>obj.error==true);
        if(errordata.length!=0){
            // alert("error");
            // this.props.sendfaciltiydata(this.state.facilityData,this.state.commonArray,false);
        }else{
            // alert("noerror");
            // this.props.sendfaciltiydata(this.state.facilityData,this.state.commonArray,true);
        }

        this.setState({commonArray});
        return errordata;
    }
        changeText = (data, data2,dataobj, key) => {
        var facilityData=this.state.facilityData;
        var commonArray=this.state.commonArray;
        if(data2){
            if(data2=='address'){
                this.getAddress();
            }else if(data2=='addlocation'){
                // alert("location adding...");
                this.setState({locationvisible:true});
            }
            return;
        }else{
            // alert("nothing");
        }
        if(Number.isInteger(dataobj.spec_det_id)){
            var findIndex=facilityData.specDetails.findIndex((obj)=>obj.spec_det_id==dataobj.spec_det_id);
            if(findIndex!=-1){
                facilityData.specDetails[findIndex].spec_det_datavalue1=data;
                this.setState({facilityData})

            }
        }else{
            var findIndex=commonArray.findIndex((obj)=>obj.spec_det_id==dataobj.spec_det_id);
            if(findIndex!=-1){
                var errorcheck=ValidationLibrary.checkValidation(data,commonArray[findIndex].validation);
                console.log(errorcheck);
                commonArray[findIndex].spec_det_datavalue1=data;
                commonArray[findIndex].error=!errorcheck.state;
                commonArray[findIndex].errormsg=errorcheck.msg;
                this.setState({commonArray},function(){
                    // this.checkValidations();
                });

            }
        }
        // this.props.sendfaciltiydata(facilityData,commonArray);
        // var errormsg = ValidationLibrary.checkValidation(data, this.state.validations[key].validations);
        // var validations = this.state.validations;
        // validations[key].error = !errormsg.state;
        // validations[key].errormsg = errormsg.msg;
        // this.setState({ validations });
        // var mandatorylength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true).length;
        // var noerrorlength = Object.keys(validations).filter((obj) => validations[obj].mandatory == true&&validations[obj].error == false).length;
        // // alert(JSON.stringify(noerrorlength));
        // var noerror = false;
        // if (mandatorylength == noerrorlength) {
        //     noerror = true;
        // }
        // this.props.sendfaciltiydata({ key: key, value: data, dbkey: dbkey, validations: validations, noerror: noerror });
        // var facilityData = this.state.facilityData;
        // facilityData[key] = data;
    }
    addSpocDetails=()=>{
        var errordata=this.checkValidations();
        if(errordata.length>0){
            // alert("errorExists");
        }else{
            alert("SPOC Added Successfully");
            var activeobj=this.props.activeobj;
            activeobj.spocData=this.state.commonArray;
            this.props.sendSPOC&&this.props.sendSPOC(activeobj);

        }
    }
    render(){
        return(
                 <ModalComp titlecolor={color.orange} closemodal={()=>this.props.closemodal&&this.props.closemodal()} visible={true}>
            <Text style={{paddingTop:hp('2%'),paddingBottom:hp('2%'),paddingLeft:10,fontSize:hp('2.5%')}}>Add basic details</Text>
            <Line/>

            <View style={styles.container}>
                <View style={{flex:.9,padding:10}}>
                <ScrollView>    
             
                    <View>        
                            <Text style={{color:color.orange,fontSize:hp('3%')}}>{this.props.activeobj&&this.props.activeobj.venue_spec_name}</Text>
                        <View style={{paddingTop:hp('1.5%')}}>
                          <DynamicForm changeText={this.changeText}  commonArray={this.state.commonArray} />
                        </View> 
                    </View>
                        </ScrollView>
                </View>
                            <Line/>
                <View style={{flex:.1,flexDirection:'row',padding:10}}>
                    <View style={[styles.view12]}>
                        <Button style={[styles.cancel_btn]}>
                            <Text style={styles.cancel_txt}>EDIT</Text>
                        </Button>
                    </View>
                    <View style={[styles.view13]}>
                        <Button onPress={this.addSpocDetails} style={[styles.cancel_btn,{ backgroundColor:color.orange}]}>
                            <Text style={styles.cancel_txt}>SAVE</Text>
                        </Button>
                    </View>
                </View>    
            </View>
            </ModalComp>
        )
    }
    componentDidMount(){
        if(this.props.commonArray){
            this.setState({commonArray:this.props.commonArray});
        }
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:color.white
    },
    txt_style:{
        fontSize:hp('2%')
    },
    flex_row:{flexDirection:'row',paddingTop:hp('1.5%'),paddingBottom:hp('1.5%')},
    view1:{flex:.3,flexDirection:'column',justifyContent:'center'},
    view2:{flex:.7,flexDirection:'column',justifyContent:'center'},
    view12:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-start',
    },
    view13:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
    },
    cancel_txt:{
        fontSize:hp('2%'),
        color:color.white,
        textAlign:'center'
    },
    cancel_btn:{
        minWidth:hp('14%'),
        borderRadius:5,
        justifyContent:'center',
        backgroundColor:color.blue,
        // height:hp('5%')
        padding:5
    },
})
export default Corporate_AddDetails