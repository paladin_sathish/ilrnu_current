import React, { Component } from 'react';
import { View, Text , StyleSheet, Alert } from 'react-native';
import { Content, Button, Tabs, ScrollableTab, Tab } from "native-base";
import WeekPicker from '../components/weekpicker';
import Schedule from '../components/schedule';
import color from '../Helpers/color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import moment from 'moment';
import Toast from "react-native-simple-toast"; 
import links from "../Helpers/config";
import CalenderBooked from "./calenderBooked";


const NameStar = ({name}) => (
  <React.Fragment>
    <Text>{name} {""}</Text> <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
);
var formkeys = [
  {
    name: "name",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Name" />
  },
  {
    name: "event_name",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Event Name" />
  },
  {
    name: "mobile_no",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Phone No" />
  },
  {
    name: "email",
    type: "default",
    labeltype: "text1",
    label: "Email"
  }
];



export default class weekAvailability extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                          initialPage: 0,
      activeTab: 0,
                     schedule: [],
                     selectedDate:props.selectedDate ,
                     week: {},
                     selectedSlots: [],
                     loading: false,
                     slots: [],
                     name: "",
                     mobile_no: "",
                     event_name: "",
                     email: "",
                     validations: {
                       name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       },
                       mobile_no: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "mobile", status: false }]
                       },
                       event_name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: []
                       },
                       email: {
                         error: null,
                         mandatory: false,
                         errormsg: "",
                         validations: []
                       }
                     }
                   };

                   this.setSchedule = this.setSchedule.bind(this);
                 }

                 _resetForm=()=>{
                   this.setState({
                     name:'',
                     email:'',
                     event_name:'',
                     mobile_no:''
                   })
                 }


                 componentDidMount = () => {
                    setTimeout(() => {
                      this.setState({ activeTab: 0});
                    }, 0);
                 };
                 
                 
                 

                 _renderForm() {
                   return (
                     <View
                       style={{
                         paddingTop: 20,
                         paddingRight: 20,
                         paddingLeft: 20,
                         paddingBottom: 20,
                         backgroundColor: "white"
                       }}
                     >
                       {formkeys.map((obj, key) => {
                         return (
                           <View style={{ marginBottom: 15 }}>
                             <LabelTextbox
                               inputtype={obj.type}
                               key={key}
                               type={obj.labeltype}
                               capitalize={obj.name != "dob"}
                               error={this.state.validations[obj.name].error}
                               errormsg={
                                 this.state.validations[obj.name].errormsg
                               }
                               changeText={data =>
                                 this.changeText(data, obj.name)
                               }
                               value={this.state[obj.name]}
                               labelname={obj.label}
                             />
                           </View>
                         );
                       })}
                     </View>
                   );
                 }

                 changeText = (data, key) => {
                   // alert(data);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                   if (key == "dob") {
                     if (data == "errordob") {
                       validations[key].error = true;
                       validations[key].errormsg =
                         "Invalid Date Accept Format (dd-mm-yyyy)";
                     } else {
                       validations[key].error = false;
                     }
                   } else if (key == "mobile") {
                     errormsg = ValidationLibrary.checkValidation(
                       data.mobile,
                       this.state.validations[key].validations
                     );
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   } else {
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   }
                   this.setState({ validations });
                   this.setState({
                     [key]: data
                   });
                 };

                 setSchedule = data => {
                   console.log("weeek", data);
                   this.setState({
                     schedule: data,
                     slots: []
                   });
                 };

                 _setSelected = data => {
                   console.log("date", data);
                   this.setState(
                     {
                       selectedDate: data
                     },
                     () => this.props.changeDate(data)
                   );
                 };

                 _setSlots = data => {
                   // console.log('slots',data)
                   this.setState({
                     slots: data
                   });
                 };

                 _submit = data => {
                   var selectedSlots = this.state.selectedSlots;
                   selectedSlots = data;
                   this.setState({ selectedSlots },()=>console.log('selecteddfds',selectedSlots));

                   //  this.props.pushData({
                   //    selectedDate: this.state.selectedDate,
                   //    selectedSlots: selectedSlots
                   //  });
                 };

                 _hitBlock = (data) => {
        

                   fetch(links.APIURL + "blockDatesAndSlots", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify(data)
                   })
                     .then(response => response.json())
                     .then(responsejson => {
                       if(responsejson.status==0)
                       { 
                        Alert.alert("Slot Blocked Successfully !!");
                        this._resetForm()
                         this.props.refresh()
                       }else{
Toast.show("Something Went Wrong !!", Toast.LONG);
                       }
                      
                     });
                 };

                 BlockFormData = () => {
                                        

                                         var err = 0;
                                         
                                         for (const key of Object.keys(
                                           this.state.validations
                                         )) { 
                                           
                                           if (
                                             this.state.validations[key]
                                               .error != false &&
                                             this.state.validations[key]
                                               .mandatory == true
                                           )
                                             err += 1;
                                         }  

                                         console.log('error list',err)




                                         if (err > 0) {
                                           Toast.show(
                                             "Field's Missing !!",
                                             Toast.LONG
                                           );
                                           return ;
                                         } else {
                                           var availablity = this.props
                                             .availability;

                                           var venue_id = availablity
                                             ? availablity.trn_venue_id
                                             : null;
                                           var venuetype = availablity
                                             ? availablity.trn_availability_type
                                             : null;
                                           var obj = {
                                             venueId: this.props.venueId,
                                             venueType: this.props.venueType,
                                             userId: this.props.userId,
                                             bookingName: this.state.name,
                                             eventName: this.state.event_name,
                                             bookingPhone: this.state.mobile_no,
                                             bookingEmail: this.state.email,
                                             availType: this.props
                                               .availabilityType,
                                             fromDateTime: this.state.selectedDate ,
                                             toDateTime: this.state.selectedDate,
                                             slots: []
                                           };

                                           for (
                                             var k = 0;
                                             k <
                                             this.state.selectedSlots.length;
                                             k++
                                           ) {
                                             var slotobj = {
                                               fromdate: this.state
                                                 .selectedDate,
                                               todate: this.state.selectedDate,
                                               slotFromTime: this.state
                                                 .selectedSlots[k]
                                                 .venue_slot_start_time,
                                               slotToTime: this.state
                                                 .selectedSlots[k]
                                                 .venue_slot_end_time
                                             };
                                             obj.slots.push(slotobj);
                                           } 
                                           if (obj.slots.length > 0) {
                                             //fetch called
                                             this._hitBlock(obj);
                                            console.log('objbj',obj)
                                           } else {
                                             Toast.show(
                                               "No Slots Were Chosen !!",
                                               Toast.LONG
                                             );
                                             return ;
                                           }
                                         } //else end
                                       };

                                       

                 render() {

                   return (
                     <Content>
                       <React.Fragment style={styles.container}>
                         <View>
                           <WeekPicker
                             loading={data => this.setState({ loading: data })}
                             setSelected={this._setSelected}
                             setSlots={this._setSlots}
                             venueId={this.props.venueId}
                             hourlychosenslots={this.props.oldWeekData}
                             selectedDate={this.state.selectedDate}
                           />
                         </View>
                         <View>
                           <Schedule
                             loading={this.state.loading}
                             onClick={this.setSchedule}
                             isOnClick={true}
                             list={this.props.oldWeekData}
                             onSubmit={this._submit}
                             oldWeekData={[]}
                             selectedDate={this.state.selectedDate}
                             boxColor={"#f6f6f6"}
                             disableSubmit={true}
                           />
                         </View>
                         <View>
                           <View
                             style={{
                               height: 8,
                               backgroundColor: "#edeef2"
                             }}
                           />
                           <Tabs
                             tabBarUnderlineStyle={{
                               borderBottomWidth: 4,
                               borderBottomColor: "#5067FF"
                             }}
                             renderTabBar={() => (
                               <ScrollableTab
                                 style={{ backgroundColor: "white" }}
                               />
                             )}
                             initialPage={this.state.initialPage}
                             page={this.state.activeTab}
                           >
                             <Tab
                               style={styles.tab}
                               heading="Available Slots"
                               tabStyle={{ backgroundColor: "white" }}
                               textStyle={{ color: "orange" }}
                               activeTabStyle={{
                                 backgroundColor: "transparent"
                               }}
                               activeTextStyle={{
                                 color: "#5067FF",
                                 fontWeight: "normal"
                               }}
                             >
                               <React.Fragment>
                                 {this._renderForm()}

                                 <View
                                   style={{
                                     flex: 1,
                                     justifyContent: "center",
                                     alignItems: "center"
                                   }}
                                 >
                                   <Button
                                     style={{
                                       borderRadius: 0,
                                       backgroundColor: color.blueactive
                                     }}
                                     block
                                     onPress={() => this.BlockFormData()}
                                   >
                                     <Text style={{ color: color.white }}>
                                       Block
                                     </Text>
                                   </Button>
                                 </View>
                               </React.Fragment>
                             </Tab>

                             <Tab
                               style={styles.tab}
                               heading="Booked Details"
                               tabStyle={{ backgroundColor: "white" }}
                               textStyle={{ color: "orange" }}
                               activeTabStyle={{
                                 backgroundColor: "transparent"
                               }}
                               activeTextStyle={{
                                 color: "#5067FF",
                                 fontWeight: "normal"
                               }}
                             >
                               <React.Fragment>
                                 <CalenderBooked
                                   data={this.props.bookingDetails}
                                   refresh={this.props.refresh}
                                 />
                               </React.Fragment>
                             </Tab>
                           </Tabs>
                         </View>
                       </React.Fragment>
                     </Content>
                   );
                 }
               }




               const styles = StyleSheet.create({
                 container: {
                   flex: 1,
                   justifyContent: "flex-start",
                   alignItems: "center",
                   backgroundColor: '#f1f1f1',
                 },
                 actionbtn: {
                   borderRadius: 5,
                   width: hp("17%"),
                   justifyContent: "center",
                   backgroundColor: color.blue
                 },
                 actionbtntxt: {
                   textAlign: "center",
                   color: color.white,
                   fontSize: hp("2.3%")
                 }
               });
