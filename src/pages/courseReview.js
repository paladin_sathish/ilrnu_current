import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView
} from "react-native";
import {Icon} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import Next from '../components/button'; 
import CourseMore from "../pages/courseMore";
import ModalComp from "../components/Modal";

export default class CourseReview extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     visible: false,
                     review:null
                   };
                 }
                 componentDidMount = () => {};

                 componentWillReceiveProps(props) {
                     if(props.venueform != null)
                     {
                       this.setState({
                         review: props.venueform
                       });
                     }
                 }

                 onSubmit = () => {

                   this.props.submitVenuData('',false);
                 };

                 onLoadEdit =()=>{
                   this.props.loadEdit();
                 }

                 componentWillMount() {
                   this.props.headertext({
                     headertext: "Add Course",
                     showskip: false
                   });
                   this.props.labeltext({
                     labeltext: (
                       <Text>
                         {" "}
                         Course{" "}
                         <Text
                           style={{ color: color.orange, fontWeight: "bold" }}
                         >
                           {" "}
                           Details{" "}
                         </Text>{" "}
                       </Text>
                     )
                   });
                 } 

                 getLangName=id=>{
                   if(id ==1)
                   return "English";
                   else if(id==2)
                   return "Hindi";
                   else
                   return "Tamil";
                 }

                 onLoadMore = data => {
                   this.setState({ visible: data });
                 };

                 render() {
                 
                   const {review} = this.state;
                   return (
                     <View style={styles.container}>
                       <View
                         style={{
                           backgroundColor: "#e9ecee",
                           padding: 10,
                           borderRadius: 10
                         }}
                       >
                         <TouchableOpacity
                           onPress={() => this.onLoadMore(true)}
                         >
                           <View
                             style={{
                               flexDirection: "row",
                               justifyContent: "space-between",
                               display: "flex",
                               position: "relative",
                               borderBottomColor: "rgba(161,155,183,1)",
                               borderBottomWidth: 1,
                               borderStyle: "dashed",
                               marginBottom: 10,
                               borderRadius: 1
                             }}
                           >
                             <View>
                               <Text style={[styles.h3, { color: color.blue }]}>
                                 {review && review.course[0].courseTitle}
                               </Text>
                             </View>
                             <View>
                               <Text>
                                 {review &&
                                   this.getLangName(
                                     review.course[0].languageId
                                   )}
                               </Text>
                             </View>
                           </View>

                           <View>
                             <View
                               style={{
                                 flexDirection: "row",
                                 justifyContent: "space-between",
                                 display: "flex"
                               }}
                             >
                               <View>
                                 <Text style={[styles.h6]}>
                                   Course Fee ( Per Hour){"\n"}{" "}
                                   <Text style={styles.h3}>
                                     {review &&
                                       review.course[0].batchData[0]
                                         .batchFee}{" "}
                                     {review &&
                                       review.course[0].batchData[0]
                                         .batchFeeCurrency}
                                   </Text>
                                 </Text>
                               </View>
                               <View>
                                 <Text style={[styles.h6]}>
                                   {" "}
                                   {"\n"}
                                   <Text style={styles.h3}>
                                     {review &&
                                       review.course[0].batchData[0]
                                         .batchStartDate}{" "}
                                     to{" "}
                                     {review &&
                                       review.course[0].batchData[0]
                                         .batchEndDate}
                                   </Text>
                                 </Text>
                               </View>
                             </View>

                             <View
                               style={{
                                 flexDirection: "row",
                                 justifyContent: "space-between",
                                 display: "flex"
                               }}
                             >
                               <View>
                                 <Text
                                   style={[styles.h1, { color: color.blue }]}
                                 >
                                   {" "}
                                 </Text>
                               </View>
                               <View style={{ flexDirection: "row" }}>
                                 <TouchableOpacity
                                   onPress={() => this.onLoadEdit()}
                                 >
                                   <Text
                                     style={{
                                       color: color.blue,
                                       fontWeight: "bold"
                                     }}
                                   >
                                     Edit{" "}
                                   </Text>
                                 </TouchableOpacity>
                               </View>
                             </View>
                           </View>
                         </TouchableOpacity>
                       </View>

                       <View
                         style={{
                           flex: 1,
                           justifyContent: "flex-end",
                           alignItems: "center",
                           bottom: 0,
                           left: 0,
                           paddingVertical: hp("4%")
                         }}
                       >
                         <Next
                           onClick={() => this.onSubmit()}
                           name="Submit"
                           _width={wp("40%")}
                         />
                       </View>

                       {this.state.visible && (
                         <ModalComp
                           titlecolor={color.orange}
                           visible={this.state.visible}
                           closemodal={data => this.onLoadMore(data)}
                         >
                           <View style={{ paddingVertical: hp("4%") }}>
                             <ScrollView>
                               <CourseMore data={this.state.review} />
                             </ScrollView>
                           </View>
                         </ModalComp>
                       )}
                     </View>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    flex:1,
    padding: 10,
    margin: 10,
    justifyContent:'center',
    alignItems:'stretch'
  },
  h1: {
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  h3: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30
  },
  h6: {
    fontSize: 11,
    fontWeight: "200",
    fontStyle: "normal"
  },
  box: {
    marginVertical: hp("0.6%")
  }
});
