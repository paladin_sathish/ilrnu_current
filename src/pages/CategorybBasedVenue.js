import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,ActivityIndicator} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import links from '../Helpers/config';
import TabLoginProcess from '../components/TabLoginProcess';
import Geolocation from 'react-native-geolocation-service';
import placeholder_img from '../images/placeholder_img.png';
import ModalDropdown  from 'react-native-modal-dropdown';
import {Actions,Router,Scene} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import Tick_mark from '../components/TickComp';
import Line from '../components/Line';
import VenueCardComp from '../components/VenueCardComp';

import ListingDetails from '../pages/ListingDetails';



const playgr_list=[
    {image:require('../images/slider1.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider2.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider3.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider4.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider5.png'),text:'Meadow Crest Playground'}
]
  isCloseToRight = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToRight = 10;
    // alert(layoutMeasurement.width + contentOffset.x)
    return layoutMeasurement.width + contentOffset.x >= contentSize.width - paddingToRight;
};
export default class CategoryBasedVenue extends Component{
    constructor(props) {
      super(props);
    
      this.state = {
        chosenlocation:{
          latitude:'',
          longitude:''
        },
        manuallocation:null,
        loginState: false,
        activeCategory: null,
        layoutdata: null,
        Norecords: false,
        suggestionDropdown: {
          id: "venue_spec_id",
          name: "venue_spec_name",
          dropdown: []
        },
        venuebasedrecords: [],
        suggestionvalue: "",
        catvalue: "",
        loginDetails: null
      };
    }
    loadVenueForm=async(data)=>{
      // Actions.loadVenueForm
       var checklogindetails = await AsyncStorage.getItem(
                     "loginDetails"
                   );
      if(checklogindetails!=null){
          const loginDetails = JSON.parse(
                       await AsyncStorage.getItem("loginDetails") 

                     );
        if(loginDetails.user_cat_id==1){
          if(data){

          Actions.venueform({logindata:loginDetails,welcomemsg:'User Logged in Successfully'});
        }else{
         Actions.venueform({logindata:loginDetails});
        }
        }else{
       if(data){
          Actions.corporateform({logindata:loginDetails,welcomemsg:'User Logged in Successfully'});
        }else{
          Actions.corporateform({logindata:loginDetails});
        }

        }
      }else{
        this.setState({loginState:true})
      // Actions.venuepage({raiselogin:true})
      }
    }

tabAction=(data)=>{
  // alert(JSON.stringify(data));
  // if(data.uer_)
  this.setState({loginState:false});
  this.setState({loginDetails:data},function(){
    this.loadVenueForm(data);
  })
} 
    componentDidMount(){ 
this.categorybasedList();
    } 




  renderButtonText= (data)=>{
    // alert(JSON.stringify(data));
        this.setState({catvalue:data})
        this.suggestionBirthday(data);
        return data.venue_spec_name;
    }
    categorybasedList=async (data)=>{
      // alert();
     console.log("dataListing....");

        this.setState({spinner:true,activeCategory:null})
       //       Geolocation.getCurrentPosition(
       // (position) => {
   const manuallocation = JSON.parse(await AsyncStorage.getItem('chosenlocation'));
      // alert(JSON.stringify(manuallocation))
    let lat = manuallocation.lat;
    let lng = manuallocation.lng; 
    // alert(this.props.venuetype==2)
    this.setState({manuallocation:{lat:lat,lng:lng}})
    // if(this.props.venuetype==2){
    //   alert(JSON.stringify({nearme:true,lat:lat,long:lng,offset:data?data:0,venueType:this.props.venuetype}));
    // }
  fetch(links.APIURL+'typeBasedVenue/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({nearme:true,lat:lat,long:lng,offset:(data?data:0),venueType:this.props.venuetype}),
    }).then((response)=>response.json())
    .then((responseJson)=>{

      // if(this.props.venuetype==2){
      // alert(JSON.stringify(responseJson.data))
      // }
         this.setState({spinner:false})
         if(responseJson.status==0&&responseJson.data.length>0){

         this.setState({venuebasedrecords:this.state.venuebasedrecords.concat(responseJson.data)})
            this.setState({Norecords:false});
         }else{
         this.setState({venuebasedrecords:venuebasedrecords.length>0?venuebasedrecords:[]})

            this.setState({Norecords:true});
         }
//      this.setState({venuebasedrecords:responseJson.length>0?responseJson:[]});
// // alert(JSON.stringify(responseJson[0].venue_spec_name));
// this.setState({suggestionvalue:data.venue_spec_name});
//       console.log(responseJson)
    })
     //   },
       
     //   (error) => {
     //            // See error code charts below.
     //            console.log(error.code, error.message);
     //        },
     //   {timeout: 10000,maximumAge:0},
     // );
  }

onLayout=(data)=>{
  this.setState({layoutdata:data.nativeEvent.layout})
}
 receiveFram=(data)=>{
    // data.height=null;

    // data.height=(data.height-data.height%5);
    // data.height=(data.height-(data.height%4>0?(data.height/4):0))+15;
    return data;
  }
   showDropdown=()=>{
    this.dropdown.show();
  }
    changeItemSlider=(item,index,visible)=>{
      this.props.visiblescreen&&this.props.visiblescreen('suggestion');
    var venuebasedrecords=this.state.venuebasedrecords;
    venuebasedrecords.map((obj)=>obj.visible=false);
    venuebasedrecords[index].visible=visible?!visible:true;
    this.setState({venuebasedrecords});
    if(venuebasedrecords[index].visible==true){
    this.setState({activeCategory:item})

    }else{
      this.setState({activeCategory:null})
    }

  }
  clearActiveCategory=()=>{
      var venuebasedrecords=this.state.venuebasedrecords;
    venuebasedrecords.map((obj)=>obj.visible=false);
    this.setState({venuebasedrecords,activeCategory:null});
}
dropdownShow=()=>{
  return false;
}
      render(){

        console.log('suggestion')
        return(
            <View style={styles.container} >
                <View style={styles.flex_row}>
                    <View style={[styles.flex_row,{alignItems:'center'}]}>
                      <Text style={[styles.text_style1,{marginRight:0,paddingLeft:4,paddingTop:0}]}>{this.props.name}</Text>
                        <ModalDropdown   
                                    style={{marginRight:3,marginLeft:5,marginTop:2}}    
                                    textStyle={[styles.text_fav]}
                                    defaultValue={'NearYou'}
                                       onDropdownWillShow={this.dropdownShow}
                                    showsVerticalScrollIndicator={false}    
                                options={['Near You', 'Already Booked','with OFFER *','Low Cost']}/>
                    </View>
                    <View onLayout={this.onLayout} style={styles.flex_sep}>
                        <TouchableOpacity onPress={()=>this.loadVenueForm()} style={styles.add_symbol_container} >
                            <Image style={styles.add_symbol} source={ADD_img}>
                            </Image>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.state.venuebasedrecords.length==0&&this.state.spinner==true &&
                    <ActivityIndicator
                      animating
                      color="#615f60"
                      size="small"
                      style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: 80,
                      }}
                    />
                }
                {this.state.venuebasedrecords.length>0&&
                 <ScrollView
                  ref={(c) => {this.scrollimage = c}}  onScroll={({nativeEvent}) => {
      if (isCloseToRight(nativeEvent)) {
        // enableSomeButton();
        console.log("ended....");
        // var self=this;
        this.categorybasedList(this.state.venuebasedrecords.length);
        // setTimeout((obj)=>{
        // self.scrollimage.scrollToEnd();
        // },200)
        // alert("reached end")
      }
    }}
                   horizontal={true}>
                  {
                   this.state.venuebasedrecords.map((item,index)=>{
                     return (
                       <TouchableOpacity
                         style={styles.square}
                        
                         onPress={() =>
                           Actions.BookDetails({
                             lat: this.state.manuallocation.lat,
                             long: this.state.manuallocation.lng,
                             id: item.venue_id
                           })
                         }
                       >
                         <VenueCardComp item={item}/>
                       </TouchableOpacity>
                     );


                   })

                 }
                 {this.state.spinner==true&&
                   <ActivityIndicator
                      animating
                      color="#615f60"
                      size="large"
                      style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: 80,
                      }}
                    />

                 }
                 
                   </ScrollView> 
}
{this.state.Norecords==true&&
<View ><Text style={{textAlign:'center',padding:10}}>No Records</Text></View>
}
{this.state.activeCategory&&
  <Line/>
}
  {this.state.activeCategory&&
                    <ListingDetails layoutChange={(data)=>this.props.layoutChange&&this.props.layoutChange(data)} activeCategory={this.state.activeCategory}  clearList={()=>this.clearActiveCategory()}>
                    <Text>Cancel</Text>
                    </ListingDetails>
                }
                 {this.state.loginState==true&&
        <TabLoginProcess closetabmodal={()=>this.setState({loginState:false})} loginttype="Add your Venue" type='login' visible={true} sendlogindet={this.tabAction}/>
      }
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
    image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },
    text_style2:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
        paddingLeft:hp('0.45%'),
        paddingTop:hp('0.45%')
    },
    flex_row:{
        flexDirection:'row'
    },
    text_style1:{
        fontSize:hp('2%')
    },
    text_style:{
        color:color.blue,
        fontSize:hp('2%'),
        fontWeight:'bold'
    },
    Down_arrowstyle:{
        height:hp('1.4%'),
        width:hp('1.4%')
    },
    flex_sep:{
        flex:1
    },
    add_symbol_container:{
        backgroundColor:color.blue,
        justifyContent:'flex-end',
        justifyContent:'center',
        alignSelf:'flex-end',
        marginRight:wp('5%'),
        height:hp('2.5%'),
        width:hp('2.5%'),
       
    },
    add_symbol:{
        height:hp('1.3%'),
        width:hp('1.3%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
    },
    text_fav:{
        fontSize:hp('1.9%'),
        color:color.blue,
        fontWeight:'bold'    
    },
    dropdown_options:{
        backgroundColor:color.white,
        // padding:10,
        paddingLeft:10,
        paddingRight:10,
        paddingTop:10,
        paddingBottom:10,
        marginBottom:5,
        fontSize:hp('1.9%'),
        width:wp('40%'),
        borderRadius:5,
        borderWidth:2,
        minHeight:hp('2%'),
        borderColor:color.ash

    },
})