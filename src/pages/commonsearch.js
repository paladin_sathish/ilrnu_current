import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,BackHandler,ToastAndroid,Slider,Dimensions,ImageBackground,ActivityIndicator, Alert } from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {
  Right,
  Top,
  Icon,
  Content,
  Button,
  ListItem,
  Body,
  CheckBox,
  Item,
  Input
} from "native-base";
import color from '../Helpers/color';
import Popover from "react-native-popover-view";
import links from '../Helpers/config';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'
import TopComp from '../components/TopComp'
import HomeSearch from '../pages/home_search';
import VideoComp from '../components/VideoComp'
import ModalComp from '../components/Modal';
import NearbyPlayGrounds from '../components/NearbyPlayGrounds'
import AsyncStorage from '@react-native-community/async-storage';
import YourFavorites from '../components/Yourfavorites'
import SuggestionBirthday from '../pages/SuggestionBirthday'
import Home_venuelist from '../components/home_venuelist'
import Orange_Square from '../components/orange_square'
import White_square from '../components/white_circle_sqaure'
import LoadingOverlay from '../components/LoadingOverlay'
import Right_arrow from '../images/rightarrow.png'
import Home_image from '../images/home_image.jpg'
import Video_png from '../images/VideoPNG/PlayPNG.png'
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import Greyright from '../images/greyright.png'
import Down_array_black from '../images/down_arrow_black.png'
import FooterComp from '../components/Footer'
import Carousel from 'react-native-snap-carousel';
import Toast from 'react-native-simple-toast';
import CategoryBasedVenue from '../pages/CategorybBasedVenue';
 const screenWidth = Dimensions.get('window').width;
class CommonSearch extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {isVisible: false,nearme:false,search_string:''};
	}
	 showPopover() {
     this.setState({ isVisible: true });
   }

   closePopover() {
     this.setState({ isVisible: false });
   }
   submitSearch=()=>{
   	this.props.sendsearch&&this.props.sendsearch({nearme:this.state.nearme,search_string:this.state.search_string})
   }
  render() {
    return (
      <View style={{marginVertical:30}}>
      <View style={styles.search}>
               <View style={{ paddingVertical: 10 }}>
                 <Text
                   style={{
                     fontSize: 24,
                     color: color.orangeDark,
                     flexWrap: "wrap",
                     textAlign: "center",
                     fontStyle: "normal"
                   }}
                 >
                   Explore and Book various course types instantly{" "}
                 </Text>
               </View>
               <View>
                 <Icon
                   active
                   name="info-circle"
                   style={{ fontSize: 28, color: color.orangeDark }}
                   type="FontAwesome"
                   color="white"
                   ref={ref => (this.touchable = ref)}
                   onPress={() => this.showPopover()}
                 />
               </View>

               <View style={{ width: wp("80%"), paddingVertical: 4 }}>
                 <Item rounded style={{ backgroundColor: "white" }}>
                   <Input
                     onChangeText={value => {
                       this.setState({ search_string: value });
                     }}
                   />
                   <Icon
                     active
                     name="search"
                     style={{ fontSize: 20, color: color.black1 }}
                     type="FontAwesome"
                   />
                 </Item>
                 <ListItem noBorder>
                   <CheckBox
                     color={color.orangeDark}
                     style={{
                       backgroundColor: this.state.nearme
                         ? color.orangeDark
                         : "transparent",
                         borderColor:color.orangeDark,
                         borderWidth:this.state.nearme?1:0
                     }}
                     checked={this.state.nearme}
                     onPress={() =>
                       this.setState({ nearme: !this.state.nearme })
                     }
                   />
                   <Body>
                   <TouchableOpacity activeOpacity={1}  onPress={() =>
                       this.setState({ nearme: !this.state.nearme })
                     }>
                     <Text style={{ color: color.orangeDark }}>{"  "}Near Me</Text>
                     </TouchableOpacity>
                   </Body>
                 </ListItem>
               </View>

               <View
                 style={{
                   flex: 1,
                   flexDirection: "row",
                   justifyContent: "flex-end"
                 }}
               >
                 <Button
                   rounded
                   style={[styles.actionbtn, { backgroundColor: color.blueactive }]}
                   onPress={() => this.submitSearch()}
                 >
                   <Text style={styles.actionbtntxt}>Search</Text>
                 </Button>
               </View>
             </View>
               <Popover
               position={"left"}
               isVisible={this.state.isVisible}
               fromView={this.touchable}
               onRequestClose={() => this.closePopover()}
               arrowSize={300}
             >
               <View style={{ padding: 12, width: 150 }}>
                 <Text style={{ fontSize: 16 }}>
                   Search your course {"\n"} by Name,Trainer, Location, Category or Type.
                 </Text>
               </View>
             </Popover>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white
  },
  actionbtn: {
    borderRadius: 50,
    width: hp("25%"),
    justifyContent: "center"
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2%"),
    fontWeight: "bold"
  },
  search: {
    display: "flex",
    padding: wp("2%"),
    backgroundColor: color.white,
    justifyContent: "center",
    alignItems: "center",
  },
  top_container: {
    height: hp("10%"),
    flexDirection: "row",
    backgroundColor: color.top_red,
    padding: hp("2%")
  },
  text_style: {
    color: color.white,
    fontSize: wp("3%")
  },
  drop_down_container: {
    flex: 1,
    justifyContent: "center",
    marginBottom: hp("2.8%"),
    marginTop: hp("2.8%"),
    marginLeft: 8,
    marginRight: 8
  },
  drop_down: {
    backgroundColor: color.white,
    borderRadius: hp("2%"),
    paddingLeft: 10,
    paddingRight: 10
    // paddingLeft:wp('2%'),
    // paddingRight:wp('2.5%'),
    // paddingRight:3,
    // paddingLeft:3
  },
  homeBgImage: {
    width: screenWidth,
    height: hp("25%"),
    resizeMode: "stretch"
  },
  play_button_container: {
    position: "absolute",
    flexDirection: "row",
    right: 0,
    top: 0,
    paddingTop: hp("1.5%"),
    paddingRight: hp("1.5%")
  },
  png_style: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    height: hp("3%"),
    width: hp("3%")
  },
  png_back: {
    backgroundColor: color.white,
    height: hp("4%"),
    width: hp("4%"),
    borderRadius: hp("4%") / 2,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    elevation: 1
  },
  flex_row: {
    flexDirection: "row"
  },
  flex_sep: {
    flex: 1
  },
  down_aarow: {
    height: hp("1%"),
    width: hp("1%"),
    justifyContent: "flex-end",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    alignContent: "flex-end",
    margin: 3
  },
  searchContainer: {
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    paddingLeft: "1%"
  },
  fav_item_container: {
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    paddingLeft: "1%"
  },
  text_near: {
    fontSize: hp("1.9%")
  },
  text_fav: {
    fontSize: hp("1.9%"),
    color: color.blue,
    fontWeight: "bold"
  },
  text_center: {
    alignItems: "center"
  },
  line: {
    backgroundColor: color.ash,
    width: "100%",
    height: 1
  },
  add_symbol_container: {
    backgroundColor: color.blue,
    justifyContent: "flex-end",
    justifyContent: "center",
    alignSelf: "flex-end",
    marginRight: wp("5%"),
    height: hp("2.5%"),
    width: hp("2.5%")
  },
  add_symbol: {
    height: hp("1.3%"),
    width: hp("1.3%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center"
  },
  right_arr_style: {
    height: hp("2%"),
    width: hp("2%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    margin: 3
  },
  right_arr_container: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    backgroundColor: color.white
  },
  mar_adjuse: {
    marginTop: 3,
    marginBottom: 3
  },
  dropdown_container: {
    width: "100%",
    height: hp("4%"),
    justifyContent: "center",
    alignSelf: "center"
  },
  dropdown_text: {
    //   fontSize: hp('2%'),
    justifyContent: "center"
  },
  dropdown_options: {
    marginTop: 10,
    width: wp("30%")
  },
  drop_down_top: {
    height: hp("1%"),
    width: hp("1%"),
    alignSelf: "center"
  }
}); 


export default CommonSearch;