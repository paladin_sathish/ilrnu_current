import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Alert,
  TouchableOpacity,
  Picker,
  PermissionsAndroid,
  Image
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content,
  ActionSheet,
  Root
} from "native-base";

import ImagePicker from "react-native-image-picker";
import PhotoInfo from "../images/svg/photoinfo.png";
import PhotoNext from "../images/photo_next.png";
import Info from "../images/info.png";
import PhotoPlus from "../images/svg/plus.png";
import color from "../Helpers/color";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import moment from "moment";
import Toast from "react-native-simple-toast";
import Next from '../components/button';
import links from "../Helpers/config";
import ModalComp from "../components/Modal";
var BUTTONS = ["Gallery", "Camera", "Cancel"];

const NameStar = ({ name }) => (
  <React.Fragment>
    <Text>
      {name} {""}
    </Text>{" "}
    <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
); 

const currencyList = [
  {
    id: 1,
    name: "USD"
  },
  {
    id: 2,
    name: "INR"
  },
  {
    id: 3,
    name: "EURO"
  }
];




var formkeys = [
  {
    name: "course_title",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Course Title" />
  }
];

var CANCEL_INDEX = 3;
const options = {
    title: 'Select Avatar',
     multiple: true,
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};


async function requestCameraPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "iVNEU App Camera and gallery Permission",
        message: "iVNEU App need access to your camera and gallery",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}
const data = new FormData();

export default class CoursePrice extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                      language_res: [],
                     level_res: [],
                     activeId: 1,
                     arrayImages: [],
                     arrayVenuImages: data,
                     selectedImage: "",
                     imagevisible: false,
                     visible: false,
                     initialPage: 0,
                     activeTab: 0,
                     schedule: [],
                     selectedDate: "",
                     week: {},
                     loading: false,
                     no_of_seats: "",
                     visible: false,
                     course_fee: "",
                     arrayImages: [],
                     event_name: "",
                     email: "",
                     language: props.edit?'':"1",
                     level:1,
                     highlights: "",
                     imagevisible: false,
                     currency: "INR",
                     course_title:'',
                     validations: {
                       course_title: {
                         error: true,
                         mandatory: false,
                         errormsg: "",
                         validations: []
                       }
                     }
                   };
                 }
                 componentWillReceiveProps(props) {
                  console.log('cwrp');
                   if (props.mainformData) {
                     this.setState({ arrayVenuImages: props.mainformData });
                   }  

                   console.log("priced", props.coursePriceState);
                   console.log("images", props.localimages);
                   if (props.coursePriceState !=null && props.coursePriceState !=''){
                     console.log('state')
                     this.state = props.coursePriceState; 
                   }

                     if (
                       props.coursePriceData != null &&
                       typeof props.coursePriceState != "object"
                     ) {
                       
                       console.log("caled", props.coursePriceData.languageId);
                       this.setState(
                         {
                           course_title: props.coursePriceData.courseTitle,
                           language: props.coursePriceData.languageId,
                           level: props.coursePriceData.trainingLevelId
                         },
                         () => console.log(this.state.language)
                       );
                     }

                   if (props.localimages !=null) {
  console.log("lcoal");
                     if (props.localimages.length > 0) {
                       this.setState({ arrayImages: props.localimages });
                     }
                   }



                 }

                 componentWillMount() { 
                    console.log("cwm");
                   this.props.headertext({
                     headertext: "Add Course",
                     showskip: false
                   });
                   this.props.labeltext({
                     labeltext: (
                       <Text>
                         {" "}
                         Course{" "}
                         <Text
                           style={{ color: color.orange, fontWeight: "bold" }}
                         >
                           {" "}
                           Details{" "}
                         </Text>{" "}
                       </Text>
                     )
                   });
                    this.getLanguage();
                    this.getLevel();
                 }


                 componentDidMount = () => {
                   console.log('cdm')
                 };
                 

                 responseCamerGallery = response => {
                   console.log(response);
                   if (response.didCancel) {
                     console.log("User cancelled image picker");
                   } else if (response.error) {
                     console.log("ImagePicker Error: ", response.error);
                   } else if (response.customButton) {
                     console.log(
                       "User tapped custom button: ",
                       response.customButton
                     );
                   } else {
                     var arrayImages = this.state.arrayImages;
                     response.checkdelete = null;
                     arrayImages.push(response);

                     var arrayVenuImages = this.state.arrayVenuImages;

                     arrayVenuImages.append("formArray", {
                       uri: response.uri,
                       type: response.type,
                       name: response.fileName
                     });
                     this.setState({ arrayVenuImages: arrayVenuImages });
                     

                     // You can also display the image using data:
                     // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                     this.setState({ arrayImages });
                   }
                 };
 


                 

                 _renderVenuePicker = (item, i) => {
                   return <Picker.Item key={i} label={item} value={item} />;
                 };

                 _renderLanguagePicker = (item, i) => {
                   return (
                     <Picker.Item
                       key={i}
                       label={item.LanguageName}
                       value={item.LanguageId}
                     />
                   );
                 };

                 _renderLevelPicker = (item, i) => {
                   return (
                     <Picker.Item
                       key={i}
                       label={item.TrainingLevelName}
                       value={item.TrainingLevelId}
                     />
                   );
                 };

                 getLanguage = () => {
                   fetch(links.APIURL + "getPrimaryLanguageList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       user_cat_id:
                         this.props.loginDetails &&
                         this.props.loginDetails.user_cat_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       if (responseJson.status == 0) {
                         console.log("lang", responseJson.data);
                         this.setState({
                           language_res: responseJson.data
                         });
                       }
                     });
                 };

                 getLevel = () => {
                   fetch(links.APIURL + "getTrainingLevelList/", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       user_cat_id:
                         this.props.loginDetails &&
                         this.props.loginDetails.user_cat_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       if (responseJson.status == 0) {
                        
                         this.setState({
                           level_res: responseJson.data,
                         });
                       }
                     });
                 };



                 loadGalleryCamera = async data => {
                   // alert(response);
                   if (data == 0) {
                     var response = await requestCameraPermission("gallery");
                   }
                   if (data == 1) {
                     var response = await requestCameraPermission("camera");
                   }
                   if (response == true) {
                     if (data == 0) {
                       //loadGallery
                       ImagePicker.launchImageLibrary(options, response => {
                         // Same code as in above section!

                         this.responseCamerGallery(response);
                       });
                     } else if (data == 1) {
                       //loadCamera
                       ImagePicker.launchCamera(options, response => {
                         // Same code as in above section!
                         this.responseCamerGallery(response);
                       });
                     } else {
                       //cancel code
                     }
                   }
                 };

                 loadPhotos = () => { 
                   
                   ActionSheet.show(
                     {
                       options: BUTTONS,
                       cancelButtonIndex: CANCEL_INDEX,
                       title: "Upload"
                     },
                     buttonIndex => {
                       this.loadGalleryCamera(buttonIndex);
                     }
                   );
                 };

                 delteSelected = () => {
                   var arrayImages = this.state.arrayImages.filter(
                     obj => obj.checkdelete != true
                   );
                   this.setState({ arrayImages });
                   var formarrayImages = arrayImages.filter(
                     obj => obj.checkdelete != true && !obj.exist
                   );
                   // alert(formarrayImages.length);
                   var arrayVenuImages = new FormData();
                   for (var i = 0; i < formarrayImages.length; i++) {
                     arrayVenuImages.append("formArray", {
                       uri: formarrayImages[i].uri,
                       type: formarrayImages[i].type,
                       name: formarrayImages[i].fileName
                     });
                   } 

                   this.setState({ arrayVenuImages: arrayVenuImages });
                    // this.props.venueImages({
                    //   formimagearray: arrayVenuImages,
                    //   localimages: arrayImages
                    // });
                 };

                 modalState = data => {
                   this.setState({ visible: data });
                 };

                 loadDelete = (key, type, notexist) => {
                   console.log('key',key)
                   console.log("imag", this.state.arrayImages[key].data);
                   var count = this.state.arrayImages.filter(
                     obj => obj.checkdelete == true
                   );
                   var arrayImages = this.state.arrayImages;
                   if (type == "long") {
                     arrayImages[key].checkdelete = true;
                   } else {
                     if (count.length > 0) {
                       if (this.state.arrayImages.length > 0) {
                         arrayImages[key].checkdelete = !arrayImages[key]
                           .checkdelete;
                       }
                     } else {
                       
                       // const imgsource = { uri: 'data:image/jpeg;base64,' + count.data };
                       this.setState({
                         selectedImage: notexist
                           ? "data:image/jpeg;base64," + arrayImages[key].data
                           : arrayImages[key].url
                       },()=>console.log('selected',this.state.selectedImage));
                       this.setState({ imagevisible: true });
                       // console.log(arrayImages[key].data);
                     }
                   }
                   // this.props.venueImages(arrayImages);
                   this.setState({ arrayImages });
                 };

                 photoservice = () => {
                   Toast.show("Request has been Sent!", Toast.LONG);

                   // this.closemodal();
                 };
                 closeimage = () => {
                   this.setState({ imagevisible: false });
                 };

                 onClickVenue = (itemvalue, index) => {
                
                   console.log("changepicker", itemvalue);
                   this.setState({
                     language: itemvalue
                   });
                 };

                 closeimage = () => {
                   this.setState({ imagevisible: false });
                 };

                 onSubmit = () => {
                   const {
                     name,
                     course_fee,
                     currency,
                     highlights
                   } = this.state;

                   console.log("title", this.state.course_title);

                   var err = 0;
                   for (const key of Object.keys(this.state.validations)) {
                     if (
                       this.state.validations[key].error != false &&
                       this.state.validations[key].mandatory == true
                     )
                       err += 1;
                   }

                   console.log("error list", err);

                   if (
                     err > 0 ||
                     this.state.language == null ||
                     this.state.language == "" ||
                     this.state.level == null ||
                     this.state.level == "" ||
                     this.state.course_title == null ||
                     this.state.course_title == ""
                   ) {
                     Toast.show("Please fill out the fields !!", Toast.LONG);
                     return;
                   } else {
                     var obj = {
                       courseTitle: this.state.course_title,
                       languageId: this.state.language,
                       trainingLevelId: this.state.level
                     };
                     console.log("okg", obj);
                     //  this.props.sendPriceData(obj, this.state);
                     this.props.sendPriceData({
                       formimagearray: this.state.arrayVenuImages,
                       localimages: this.state.arrayImages,
                       coursePrice: obj,
                       courseState: this.state
                     });
                   }
                 };  


                 
                 onSend =()=>{
                    
                    this.props.sendCourseDetail(obj, this.state);
                 }




                 modalState = data => {
                   this.setState({ visible: data });
                 };

                 loadPhotos = () => {
                   ActionSheet.show(
                     {
                       options: BUTTONS,
                       cancelButtonIndex: CANCEL_INDEX,
                       title: "Upload"
                     },
                     buttonIndex => {
                       this.loadGalleryCamera(buttonIndex);
                     }
                   );
                 };

                 _renderCurrencyPicker = (item, i) => {
                   return (
                     <Picker.Item key={i} label={item.name} value={item.name} />
                   );
                 };

                 _renderForm() {
                   return (
                     <View style={{ backgroundColor: "white" }}>
                       <View
                         style={{
                           paddingTop: 10,
                           paddingRight: 20,
                           paddingLeft: 20,
                           paddingBottom: 5
                         }}
                       >
                         {formkeys.map((obj, key) => {
                        
                           return (
                             <View style={{ marginBottom: 15 }}>
                               <LabelTextbox
                                 inputtype={obj.type}
                                 key={key}
                                 type={obj.labeltype}
                                 capitalize={obj.name != "dob"}
                                 error={this.state.validations[obj.name].error}
                                 errormsg={
                                   this.state.validations[obj.name].errormsg
                                 }
                                 changeText={data =>
                                   this.changeText(data, obj.name)
                                 }
                                 value={this.state[obj.name]}
                                 labelname={obj.label}
                               />
                             </View>
                           );
                         })}
                       </View>
                     </View>
                   );
                 }

                 _resetForm = () => {
                   this.setState({
                     name: "",
                     email: "",
                     event_name: "",
                     mobile_no: ""
                   });
                 };

                 changeText = (data, key) => {
                   // alert(data);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                   if (key == "dob") {
                     if (data == "errordob") {
                       validations[key].error = true;
                       validations[key].errormsg =
                         "Invalid Date Accept Format (dd-mm-yyyy)";
                     } else {
                       validations[key].error = false;
                     }
                   } else if (key == "mobile") {
                     errormsg = ValidationLibrary.checkValidation(
                       data.mobile,
                       this.state.validations[key].validations
                     );
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   } else {
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   }
                   this.setState({ validations });
                   this.setState({
                     [key]: data
                   });
                 };

                 render() {
                   var count =this.state.arrayImages.length >0 && this.state.arrayImages.filter(
                     obj => obj.checkdelete == true
                   );
                   return (
                     <Content>
                       <View>{this._renderForm()}</View>

                       <View
                         style={{
                           paddingTop: 5,
                           paddingRight: 20,
                           paddingLeft: 20,
                           paddingBottom: 5,
                         }}
                       >
                         <Text style={{ fontSize: 16 }}>Primary Language</Text>
                         <View style={styles.pickerBorder}>
                           <Picker
                             selectedValue={this.state.language}
                             style={{
                               height: 45,
                               width: wp("90%"),
                             }}
                             onValueChange={(itemValue, itemIndex) =>
                               this.onClickVenue(itemValue, itemIndex)
                             }
                           >
                             {this.state.language_res &&
                               this.state.language_res.map((item, i) =>
                                 this._renderLanguagePicker(item, i)
                               )}
                           </Picker>
                         </View>
                       </View>

                       <View
                         style={{
                           paddingTop: 5,
                           paddingRight: 20,
                           paddingLeft: 20,
                           paddingBottom: 5,
                         }}
                       >
                         <Text style={{ fontSize: 16, paddingVertical: 3 }}>
                           Level
                         </Text>
                         <View style={styles.pickerBorder}>
                           <Picker
                             selectedValue={this.state.level}
                             style={{
                               height: 45,
                               width: wp("90%"),
                             }}
                             onValueChange={(level, itemIndex) =>
                               this.setState({
                                 level,
                               })
                             }
                           >
                             {this.state.level_res &&
                               this.state.level_res.map((item, i) =>
                                 this._renderLevelPicker(item, i)
                               )}
                           </Picker>
                         </View>
                       </View>

                       <View>
                         {count.length > 0 && (
                           <TouchableOpacity
                             onPress={() => this.delteSelected()}
                             style={{
                               flex: 1,
                               flexDirection: "row",
                               justifyContent: "center",
                               alignItems: "center",
                               backgroundColor: "transparent",
                             }}
                           >
                             <Icon
                               style={{ color: "black" }}
                               name="trash-o"
                               type="FontAwesome"
                             />
                             <Text style={{ color: "black", paddingLeft: 6 }}>
                               Delete {count.length} Items
                             </Text>
                           </TouchableOpacity>
                         )}
                       </View>

                       <View style={styles.imagecontainer}>
                         {this.state.arrayImages.map((obj, key) => {
                           return (
                             <View key={key} style={styles.imageView}>
                               <TouchableOpacity
                                 onPress={() =>
                                   this.loadDelete(key, null, !obj.exist)
                                 }
                                 style={styles.venueimage}
                                 onLongPress={() =>
                                   this.loadDelete(key, "long")
                                 }
                               >
                                 {obj.checkdelete == true && (
                                   <View
                                     style={[
                                       styles.imageView,
                                       {
                                         alignItems: "flex-start",
                                         zIndex: 9999999,
                                         top: 0,
                                         right: 0,
                                         backgroundColor:
                                           "rgba(255,255,255,0.5)",
                                       },
                                     ]}
                                   >
                                     <Icon
                                       type="FontAwesome"
                                       name="check-square"
                                       style={{ color: "green" }}
                                     />
                                   </View>
                                 )}
                                 <Image
                                   style={styles.venueimage}
                                   source={{
                                     uri: !obj.exist
                                       ? "data:image/jpeg;base64," + obj.data
                                       : obj.url,
                                   }}
                                 />
                               </TouchableOpacity>
                             </View>
                           );
                         })}
                         {(this.state.arrayImages.length == 2 ||
                           this.state.arrayImages.length % 3 == 2) && (
                           <View style={styles.imageView}></View>
                         )}
                       </View>
                       {this.state.arrayImages.length == 0 && (
                         <View
                           style={{
                             flexDirection: "row",
                             alignItems: "center",
                             justifyContent: "center",
                             paddingVertical: hp("2%"),
                           }}
                         >
                           <View>
                             <TouchableOpacity
                               onPress={() => this.modalState(true)}
                             >
                               <Image
                                 source={PhotoInfo}
                                 style={{
                                   width: hp("3.3%"),
                                   height: hp("3.3%"),
                                 }}
                               />
                             </TouchableOpacity>
                           </View>
                           <View
                             style={{
                               marginLeft: 12,
                               marginRight: 12,
                               marginBottom: 7,
                             }}
                           >
                             {this.state.arrayImages.length == 0 && (
                               <Text style={{ fontSize: hp("2.5%") }}>
                                 Upload Photos
                               </Text>
                             )}
                             {!this.state.arrayImages.length == 0 && (
                               <Text style={{ fontSize: hp("2.5%") }}>
                                 Upload More Photos
                               </Text>
                             )}
                           </View>
                           <View>
                             <TouchableOpacity
                               onPress={() => this.loadPhotos()}
                             >
                               <Image
                                 source={PhotoPlus}
                                 style={{
                                   width: hp("4.5%"),
                                   height: hp("4.5%"),
                                 }}
                               />
                             </TouchableOpacity>
                           </View>
                         </View>
                       )}

                       <View
                         style={{
                           display: "flex",
                           alignSelf: "center",
                           justifyContent: "flex-end",
                         }}
                       >
                         <Next
                           onClick={() => this.onSubmit()}
                           name="Next"
                           _width={wp("40%")}
                         />
                       </View>

                       <ModalComp
                         closemodal={() => this.closeimage()}
                         center={true}
                         visible={this.state.imagevisible}
                         header={false}
                         modaltitle={"How"}
                         modalsubtitle={"Course"}
                       >
                         <View style={{ flex: 1 }}>
                           <Image
                             style={styles.venueimage}
                             style={{
                               width: wp("100%"),
                               height: hp("50%"),
                             }}
                             source={{ uri: this.state.selectedImage }}
                           />
                         </View>
                       </ModalComp>

                       <ModalComp
                         titlecolor={color.orange}
                         visible={this.state.visible}
                         closemodal={(data) => this.modalState(data)}
                         header={false}
                         center={true}
                       >
                         <Content>
                           <Root>
                             <View
                               style={{ paddingLeft: 20, paddingRight: 15 }}
                             >
                               <View
                                 style={{
                                   flex: 1,
                                   justifyContent: "flex-start",
                                   alignItems: "center",
                                   backgroundColor: color.white,
                                 }}
                               >
                                 <Image
                                   source={Info}
                                   style={{
                                     width: hp("10%"),
                                     height: hp("10%"),
                                     textAlign: "center",
                                   }}
                                 />
                                 <Text
                                   style={{
                                     color: color.orange,
                                     fontSize: hp("3.5%"),
                                     textAlign: "center",
                                     fontWeight: "bold",
                                   }}
                                 >
                                   Upload Instructions
                                 </Text>
                                 <View
                                   style={{
                                     display: "flex",
                                     justifyContent: "flex-start",
                                     alignItems: "stretch",
                                     paddingVertical: hp("2%"),
                                   }}
                                 >
                                   <View style={{ flexDirection: "row" }}>
                                     <Text
                                       style={{
                                         color: color.orange,
                                       }}
                                     >
                                       {"\u2022"}
                                     </Text>
                                     <Text
                                       style={{
                                         paddingLeft: 5,
                                         color: color.black1,
                                       }}
                                     >
                                       Please Upload Image in JPG or PNG format
                                     </Text>
                                   </View>
                                   <View style={{ flexDirection: "row" }}>
                                     <Text
                                       style={{
                                         color: color.orange,
                                       }}
                                     >
                                       {"\u2022"}
                                     </Text>
                                     <Text
                                       style={{
                                         paddingLeft: 5,
                                         color: color.black1,
                                       }}
                                     >
                                       For Image, Image size should be 1080px by
                                       566px
                                     </Text>
                                   </View>

                                   <View
                                     style={{
                                       flexDirection: "row",
                                       justifyContent: "flex-start",
                                       flexWrap: "wrap",
                                     }}
                                   >
                                     <Text
                                       style={{
                                         color: color.orange,
                                       }}
                                     >
                                       {"\u2022"}
                                     </Text>
                                     <Text
                                       style={{
                                         paddingLeft: 5,
                                         color: color.black1,
                                       }}
                                     >
                                       For Video, Video size should be less than
                                       10 MB
                                     </Text>
                                   </View>
                                 </View>
                               </View>
                             </View>
                           </Root>
                         </Content>
                       </ModalComp>
                     </Content>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#f1f1f1"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  },
  pickerBorder: {
    borderColor: "#d6d6d6",
    borderWidth: 1,
    borderStyle: "solid",
    marginVertical: hp("0.5%")
  },
  imagecontainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  imageView: {
    width: wp("100%"),
    height: hp("17%"),
    borderRadius: 7,
    marginBottom: 15,
    paddingHorizontal:wp('3%')
  },
  venueimage: {
    position: "absolute",
    width: "100%",
    height: "100%",
    borderRadius: 7,
    paddingHorizontal:wp('2%'),
    marginHorizontal:wp('2.5%')
  }
});
