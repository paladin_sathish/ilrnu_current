"use strict";

import React, { Component } from "react";
import CircleText from "../components/circlecomp";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from "../Helpers/color";
import {
  Container,
  Content
} from "native-base";
import { StyleSheet, View, Text ,Alert} from "react-native";
import Line from "../components/Line";
import Triangle from "../components/triangles";
import CreateCourse from "../pages/createCourse";
import links from "../Helpers/config";

class CourseMode extends Component {
  constructor(props) {
    super(props);

    this.state = {
      amentiesvisible: false,
      venuedata: [],
      activeobj: props.venuedetails
    };
    // alert(datas.length%2);
  }

  componentWillReceiveProps(props) {
    if (props.venuedetails) {
      this.setState({ activeobj: props.venuedetails,amentiesvisible:true });
    }
  }

  getTraineeMode = () => {
    fetch(links.APIURL + "getModeOfTrainingList/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        user_cat_id:
          this.props.loginDetails && this.props.loginDetails.user_cat_id
      })
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == 0) {
          console.log("third tab", responseJson.data);
          this.setState({ venuedata: responseJson.data });
        }
      });
  };

  onValueChange = value => {
    this.setState({
      selected: value
    });
  };

  componentWillMount() {
   
    this.props.headertext({
      headertext: (
        <Text
          style={{
            fontSize: hp("1.7%"),
            fontWeight: "bold",
            color: color.blue
          }}
        >
          Add Course
        </Text>
      )
    });
    this.props.labeltext({
      labeltext: (
        <Text>
          <Text style={{ color: color.orange }}>{this.props.roomname}</Text>{" "}
          Mode of Training{" "}
          <Text style={{ color: color.orange, fontWeight: "bold" }}>
            Delivery{" "}
          </Text>{" "}
        </Text>
      )
    });
  }

  changeVenueType = (data, key) => {
     if (data.ModeOfTrainingId == 1) {
       Alert.alert(
         "Corona Virus Restriction",
         "Due to the lock down this service has been stopped ",
         [{ text: "OK", onPress: () => console.log("OK Pressed") }],
         { cancelable: false }
       );
       return;
     }

    console.log("traineemodetype", data);
    this.setState({ activeobj: data, amentiesvisible: true });
    this.props.sendvenuetypedata(data);
  };

  render() {
    return (
      <Container>
        <Content>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              paddingTop: 12,
              paddingLeft: 12,
              paddingRight: 12
            }}
          >
            {this.state.venuedata &&
              this.state.venuedata.length > 0 &&
              this.state.venuedata.map((obj, key) => {
                return (
                  <View key={key} style={[styles.circleStyle]}>
                    {!obj.disable && (
                      <CircleText
                        image={obj.ModeOfTrainingIcon}
                        sendText={data => {
                          this.changeVenueType(data, key);
                        }}
                        style={[styles.flexWidth]}
                        width={12}
                        height={12}
                        text={obj.ModeOfTrainingName}
                        data={obj}
                        active={
                          this.state.activeobj &&
                          this.state.activeobj.ModeOfTrainingId ==
                            obj.ModeOfTrainingId
                        }
                      />
                    )}

                    {this.state.activeobj && 
                    this.state.activeobj.ModeOfTrainingId
                     && this.state.activeobj.ModeOfTrainingId ==
                      obj.ModeOfTrainingId && (
                      <View>
                        <View style={styles.activeparent}>
                          <Triangle
                            width={hp("5%")}
                            height={hp("2%")}
                            color={color.black1}
                            direction={"up"}
                          ></Triangle>
                        </View>
                        <View
                          style={[styles.activeparent, { top: -hp("1.6%") }]}
                        >
                          <Triangle
                            width={hp("5%")}
                            height={hp("2%")}
                            color={color.ash1}
                            direction={"up"}
                          ></Triangle>
                        </View>
                      </View>
                    )}
                  </View>
                );
              })}
          </View>
          <View>
            <Line />
          </View>

          {this.state.amentiesvisible && (
            <View>
              <CreateCourse
                _onClick={() => this.props.next(this.state.activeObj)}
              />
            </View>
          )}
        </Content>
      </Container>
    );
  }
  componentDidMount() {
    this.getTraineeMode();
  }
}

const styles = StyleSheet.create({
  circleStyle: {
    width: wp("30%")
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid"
  },
  arrowUp: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
    borderTopWidth: 0,
    borderRightWidth: 12,
    borderBottomWidth: 12,
    borderLeftWidth: 12,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: color.grey,
    borderLeftColor: "transparent"
  },
  submitbox: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  actionbtn: {
    borderRadius: 5,
    justifyContent: "center",
    paddingHorizontal: 14,
    marginHorizontal: 10,
    color: "white"
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2%"),
    fontWeight: "bold",
    textTransform: "uppercase"
  },
  borderDays: {
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderLeftColor: "transparent",
    borderRightColor: "transparent"
  },
  bordertriangle: {
    position: "absolute",
    left: 0,
    right: 0,
    margin: "auto",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center"
  },
  activeparent: {
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999999,
    margin: "auto",
    left: 0,
    right: 0
  }
});

export default CourseMode;
