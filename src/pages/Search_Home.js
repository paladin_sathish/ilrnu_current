import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,ActivityIndicator} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button,Root,Container} from 'native-base'
import color from '../Helpers/color'
import NearbyPlayGrounds from '../components/NearbyPlayGrounds'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'
import TopComp from '../components/TopComp'
import placeholder_img from '../images/placeholder_img.png';
import Home_venuelist from '../components/home_venuelist'
import TabLoginProcess from '../components/TabLoginProcess';
import Left_arrow from '../images/left.png'
import Right_arrow from '../images/right.png'
import ADD_img from '../images/add_icon.png'
import SliderComp from './SoccerClub';
import links from '../Helpers/config';
import LoadingOverlay from '../components/LoadingOverlay'
import Toast from 'react-native-simple-toast';

import AsyncStorage from '@react-native-community/async-storage';

const playgr_list=[
    {image:require('../images/slider1.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider2.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider3.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider4.png'),text:'Meadow Crest Playground'},
    {image:require('../images/slider5.png'),text:'Meadow Crest Playground'}
]

export default class Search_home extends Component{
    constructor(props) {
      super(props);
      // alert();

      this.state = {
        visiblescreen: null,
        visible: false,
        searchhome: props.data,
        activeIndex: 0,
        activename: "",
        topcompobj: props.data2,
        loginDetails: null,
        suggestiondata: [],
        loading: null,
        loadingindicator:false,
        count:props.count
      };
    }
    BookNow=()=>{
        this.setState({visible:true})
    }
     loadVenueForm=()=>{
      // Actions.loadVenueForm
      if(this.state.loginDetails){
        if(this.state.loginDetails.user_cat_id==1){
          Actions.venueform({logindata:this.state.loginDetails});
        }else{
          Actions.corporateform({logindata:this.state.loginDetails});
        }
      }else{
      Actions.venuepage({raiselogin:true})
      }
    }
 
    loadSpecRecommendation=(data)=>{
  fetch(links.APIURL+'suggestion/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({'specId':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{ 
        

         if(responseJson.length>0){

            this.setState({Norecords:false});
         }else{
            this.setState({Norecords:true});
         }
        // alert(JSON.stringify(responseJson));
     this.setState({suggestiondata:responseJson.length>0?responseJson:[]});
// alert(JSON.stringify(responseJson[0].venue_spec_name));
// this.setState({suggestionvalue:responseJson[0].venue_spec_name});
//       console.log(responseJson)
    })
    }
    async componentDidMount(){
        const data=await AsyncStorage.getItem('loginDetails');
// alert(data);
if(data){
  // alert(JSON.stringify(data));
  this.setState({loginDetails:JSON.parse(data)});

}
      if(this.props.data){
      this.setState({searchhome:this.props.data});
    }
    if(this.props.data1){
        this.setState({activename:this.props.data1});
    }
    if(this.props.data2){ 
   
        console.log('propsdata',this.props.data2);
        this.setState({topcompobj:this.props.data2});
    } 

    if(this.props.spec_det_id){
        this.loadSpecRecommendation(this.props.spec_det_id);
    } 

    // alert(JSON.stringify(this.state.searchhome))
// alert(JSON.stringify(this.props));
if(this.props.data2){
    if(this.props.data2.whatdata){
        this.loadSpecRecommendation(this.props.data2.whatdata.venue_spec_id);
    }
}

    }
    closetabmodal=(data)=>{
    // alert("hiii");
    this.setState({visible:false});
  }
 componentWillReceiveProps(props){
    // console.log(props.data);
    if(props.data){
      this.setState({searchhome:props.data});
    }
      if(this.props.data1){
        this.setState({activename:this.props.data1});
    } 

     if(this.props.data2){ 
   
        this.setState({topcompobj:this.props.data2});
    } 
    if(this.props.count){
        this.setState({count:this.props.count});
    }

    if(this.props.spec_det_id){
        this.loadSpecRecommendation(this.props.spec_det_id);
    }
      var loadingcheck=props.navigation.state.params.loading;
    var loadingcheckmsg=props.navigation.state.params.msg;
    var successpayment=props.navigation.state.params.successpayment;
        this.setState({loading:loadingcheck?loadingcheck:null});
    if(successpayment==true){
        // Toast.show(loadingcheckmsg,Toast.LENGTH_LONG)
        alert(loadingcheckmsg);
        // Toast.show(loadingcheckmsg,Toast.LONG)
    }else{
        // this.setState({})
        if(successpayment=='error'){
        Toast.show(loadingcheckmsg,Toast.LONG)
    }
    }
    
  }
updatesearchhomedata=async()=>{
    const manuallocation = JSON.parse(await AsyncStorage.getItem('chosenlocation'));
      let lat = manuallocation!=null?manuallocation.lat:this.state.chosenlocation.lat;
    let lng = manuallocation!=null?manuallocation.lng:this.state.chosenlocation.lng; 
    var topcompobj=this.state.topcompobj;
    this.setState({loadingindicator:true})
    fetch(links.APIURL + "dropdownSearchpurpose_new", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }, 
    body: JSON.stringify({
      venue_act_id: topcompobj.doobj ? topcompobj.doobj.venue_act_id : null,
      venue_purpose_id: topcompobj.whatobj
        ? topcompobj.whatobj.venue_purpose_id
        : null,
      venue_cat_id: topcompobj.whereobj
        ? topcompobj.whereobj.venue_cat_id
        : null,
      lat: lat,
      long:lng,
      offset:this.state.searchhome.length,
      TOS: topcompobj.TOS
    })
  }).then((resp)=>resp.json())
    .then((respjson)=>{
    this.setState({loadingindicator:false})

        if(respjson.data.length>0){
            var searchhome=this.state.searchhome;
            searchhome=searchhome.concat(respjson.data);
            this.setState({searchhome:searchhome})
        }
        // alert(JSON.stringify(respjson));
    })
    // alert(JSON.stringify(this.state.topcompobj));
}
  receivetopcomp=(data,data1,data2,count)=>{
      // alert(data1);
    this.setState({searchhome:data})
    this.setState({activename:data1})
    this.setState({topcompobj:data2})
    this.setState({count:count})
    // alert(JSON.stringify(data));
  }
  modalnone=()=>{
    return false
  }
  visiblescreen=(data)=>{
this.setState({visiblescreen:data})
}
    render(){
        // alert(JSON.stringify(this.props));
        return (
          <Container>
            {this.state.loading && <LoadingOverlay />}

            <Root>
              <ScrollView style={styles.container}>
                <TopComp
                  topcompobj={this.state.topcompobj}
                  nxtpge={(data, data1, data2, count) =>
                    this.receivetopcomp(data, data1, data2, count)
                  }
                  style={{ height: hp("5%") }}
                ></TopComp>

                <View style={styles._view}>
                  <View style={styles.view1}>
                    <Text style={styles.text_style1}>
                      {this.state.count} Results
                    </Text>
                    <Text style={styles.text_style12}>for your search</Text>
                  </View>
                  {/* <View style={styles.view2}>
                        <View style={styles.view22}>
                            <Text style={styles.text_style13}>
                                More Filter
                            </Text>
                        </View>
                        <View style={{ justifyContent:'center'}}>
                            <Image source={White_arrow} style={styles.white_arr_style}>

                            </Image>
                        </View>
                    </View>
                    */}
                </View>

                <View>
                  {this.state.searchhome.length > 0 && (
                    <SliderComp
                      count={this.state.count}
                      sendScrollEnd={() => this.updatesearchhomedata()}
                      sendSliderData={this.state.searchhome}
                    >
                      {this.state.loadingindicator == true && (
                        <ActivityIndicator
                          size="large"
                          color={color.orangeBtn}
                        />
                      )}
                    </SliderComp>
                  )}
                </View>
                {/*}  {this.state.searchhome&&this.state.searchhome.map((item)=>{
                    return(
                            
                        <View style={{padding:hp('1.5%'),marginBottom:hp('1.5%')}}>
                
                        <Text style={{fontSize:hp('2%')}}>
                      {item.trn_venue_name}
                    </Text>
                    <View style={{flexDirection:'row',height:hp('15%')}}>
                        <View style={{flex:.4,paddingTop:hp('1.5%')}}>
                        <TouchableOpacity onPress={()=>this.loadCarousel(this.props.activeCategory&&(this.props.activeCategory.photos.length>0?this.props.activeCategory.photos:[]))}>
                         {(item.photos.length>0&&item.photos[0].venue_image_path)&&
                        <Image style={{height:hp('12%'),width:'100%'}} alt="No Image"
                            source= {{uri:item.photos.length>0&&item.photos[0].venue_image_path}}>       
                        </Image>}
                        </TouchableOpacity>
                         {!(item.photos.length>0&&item.photos[0].venue_image_path)&&
                        <Image style={{height:hp('12%'),width:'100%'}} alt="No Image"
                            source= {placeholder_img}>       
                        </Image>
                      }
                                                    </View>
                        <View style={{flex:.6,backgroundColor:color.white,minHeight:120}}>
                        
                            <View style={{flex:.9,paddingTop:hp('1.5%'),paddingLeft:hp('1.5%')}}>
                                <Text style={{fontSize:hp('1.5%'),}} numberOfLines = { 2 }>
                                    {item.availability.length>0&&item.availability[0].trn_venue_moredetails}
                                </Text>
                            </View>
                            

                             <View style={[{paddingLeft:hp('1.5%'),flex:.2,flexDirection:'row',marginTop:hp('-10%')}]}>
                                <Image source={Left_arrow} style={[styles.blue_arr,styles.view22]}>

                                </Image>
                                <Text style={[{fontSize:hp('1.5%'),},styles._end,styles.view22]}>
                                   {this.state.activeIndex+1} of {this.state.searchhome.length}
                                </Text>
                                <Image source={Right_arrow} style={[styles.blue_arr,styles.view22]}>

                                </Image>
                            </View>   
                        </View>

                    </View>
                    
                    <Button style={[styles.booK_nw_btn]} onPress={this.BookNow}>
                        <Text style={[styles.booK_nw_txt]} >Book Now</Text>
                     </Button>
                    
                </View>
               

                        )
                })}
            */}

                <View style={styles.line} />

                {this.state.suggestiondata.length > 0 && (
                  <View style={styles.fav_item_container}>
                    <View style={styles.view3}>
                      <View style={styles.view31}>
                        <Text style={styles.text_31}>
                          {this.state.activename}
                        </Text>

                        <ModalDropdown
                          //   style={styles.dropdown_container}
                          textStyle={[styles.text_32]}
                          // dropdownStyle={styles.dropdown_options}
                          defaultValue={"Recommendations"}
                          onDropdownWillShow={this.modalnone}
                          // showsVerticalScrollIndicator={false}
                          //     dropdownTextStyle={{paddingRight:50}}

                          // options={['Recommendations']}
                        />
                      </View>
                      <View style={styles.view32}>
                        <TouchableOpacity
                          onPress={() => this.loadVenueForm()}
                          style={styles.add_symbol_container}
                        >
                          <Image
                            style={styles.add_symbol}
                            source={ADD_img}
                          ></Image>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View>
                      <Home_venuelist
                        visiblescreenData={this.state.visiblescreen}
                        suggestiondata={this.state.suggestiondata}
                        visiblescreen={data => this.visiblescreen(data)}
                      />

                    </View>
                  </View>
                )}

                <View style={styles.line} />

                <View style={styles.fav_item_container}>
                  <NearbyPlayGrounds
                    visiblescreenData={this.state.visiblescreen}
                    visiblescreen={data => this.visiblescreen(data)}
                  />
                </View>

                <View style={styles.line} />
              </ScrollView>
            </Root>
          </Container>
        );
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:color.white
    },
  
    _view:{
        flexDirection:'row',
    },
    view1:{
        flex:1,
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        paddingLeft:('2%'),
        backgroundColor:color.blueactive,
        flexDirection:'row'
    },
    text_style1:{
        color:color.white,
        fontWeight:'bold',
        paddingRight:3
    },
    text_style12:{
        color:color.white
    },
    view2:{
        flex:.3,
        backgroundColor:color.blue,
        justifyContent:'center',
        paddingLeft:wp('.8%'),
        flexDirection:'row',
    },
    view22:{
        justifyContent:'center',alignSelf:'center',alignContent:'center',alignItems:'center'
    },
    text_style13:{
        color:color.white,
        paddingRight:wp('2%')
    },
    white_arr_style:{
        height:hp('1.5%'),
        width:hp('1.5%'),
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignSelf:'flex-end',
        alignContent:'flex-end',
       // margin:3
    },
    line:{
        backgroundColor:color.ash,
        width:'100%',
        height:1
    },
    blue_arr:{
        height:hp('2.5%'),
        width:hp('2.5%'),
        justifyContent:'center',alignSelf:'center',alignContent:'center',alignItems:'center'
    },
    _end:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignSelf:'flex-end',
        alignContent:'flex-end',
    },
    booK_nw_btn:{
        backgroundColor:color.blue,
        height:hp('3%'),
        marginRight:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8,
        position:'absolute',
        bottom:0,
        right:0,
        
    },
    booK_nw_txt:{
        color:color.white,
        fontSize:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8
    },
    fav_item_container:{
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%'),
        paddingLeft:hp('1.5%')
    },
    view3:{
     
        flexDirection:'row',
    },
    view31:{
        flexDirection:'row',
        flex:1,
        alignItems:'center',
        paddingBottom:hp('1.5%'),
        marginRight:12
    },
    view32:{
        flex:1,
        marginLeft:20
    },

    text_31:{
        fontSize:hp('1.9%'),
paddingRight:8
    },
    text_32:{
        fontSize:hp('1.9%'),
        color:color.blue,
        fontWeight:'bold'    
    },
    down_aarow:{
        height:hp('1%'),
        width:hp('1%'),
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignSelf:'flex-end',
        alignContent:'flex-end',
        margin:3,
        marginLeft:10,
        paddingLeft:10
    },
     add_symbol_container:{
        backgroundColor:color.blue,
        justifyContent:'flex-end',
        justifyContent:'center',
        alignSelf:'flex-end',
        marginRight:wp('5%'),
        height:hp('2.5%'),
        width:hp('2.5%'),
       
    },
    add_symbol:{
        height:hp('1.3%'),
        width:hp('1.3%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
    },
    dropdown_options:{
        backgroundColor:color.white,
        fontSize:hp('1.9%'),
        width:wp('40%'),
        borderRadius:5,
        borderWidth:2,
        borderColor:color.ash

    },
   
})