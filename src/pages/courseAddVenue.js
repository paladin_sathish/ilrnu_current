"use strict";

import React, { Component } from "react";

import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  StatusBar,
  Modal,
  Image,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {

  Content,
} from "native-base";
import Next from "../components/button";
import ValidationLibrary from "../Helpers/validationfunction";
import DynamicForm from "../components/DynamicForm";
import AddressComp from "../components/addressComp"; 
import Toast from "react-native-simple-toast"; 
class CourseAddVenue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commonArray: [
        {
          spec_det_id: "idname",
          venue_spec_id: 1,
          spec_det_name: "Venue Name",
          spec_det_sortorder: 0,
          spec_det_datatype1: "text",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }, { name: "minLength", params: 30 }],
          error: null,
          errormsg: ""
        },
        {
          spec_det_id: "idname1",
          venue_spec_id: 1,
          spec_det_name: "Venue Address",
          spec_det_sortorder: 0,
          spec_det_datatype1: "textarea",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "12.9716,77.5946",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }],
          error: null,
          errormsg: ""
        },

        {
          spec_det_id: "idname2",
          venue_spec_id: 1,
          spec_det_name: "Venue Landmark",
          spec_det_sortorder: 0,
          spec_det_datatype1: "btn_address",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [
            { name: "required" },
            { name: "minLength", params: 300 }
          ],
          error: null,
          errormsg: ""
        },
        {
          spec_det_id: "idname3",
          venue_spec_id: 1,
          spec_det_name: "Venue Mobile",
          spec_det_sortorder: 0,
          spec_det_datatype1: "number",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }, { name: "mobile", status: false }],
          error: null,
          errormsg: ""
        },
        {
          spec_det_id: "idname4",
          venue_spec_id: 1,
          spec_det_name: "Venue Mail",
          spec_det_sortorder: 0,
          spec_det_datatype1: "text",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }, { name: "email", status: false }],
          error: null,
          errormsg: ""
        },
        {
          spec_det_id: "idname5",
          venue_spec_id: 1,
          spec_det_name: "Venue Area",
          spec_det_sortorder: 0,
          spec_det_datatype1: "text",
          spec_det_datavalue1: "",
          spec_det_datatype2: "",
          spec_det_datavalue2: "",
          spec_det_datatype3: "",
          spec_det_datavalue3: "",
          validation: [{ name: "required" }],
          error: null,
          errormsg: ""
        },

      ],
      facilityData: null,
      addresscomp: "",
      modalstate: false,
      init: true
    };
  }

  componentWillReceiveProps(props) {
    // alert(JSON.stringify(props));
    if (props.facilityData) {
      var facilityData = this.state.facilityData;
      facilityData = props.facilityData;
      this.setState({ facilityData });
    }
    if (props.commonArray) {
      this.setState({ commonArray: props.commonArray });
    }
    if (props.facilityvalidations) {
      this.setState({ validations: props.facilityvalidations });
    }

  }
  getAddress = () => {
    this.setState({ modalstate: true });
  };
  receiveAddress = (data, location) => {

    // alert(JSON.stringify(location));
    // var facilityData = this.state.facilityData;
    // facilityData.address = data.usertxt;
    // this.setState({ facilityData });
    this.setState({ modalstate: false });
    var commonArray = this.state.commonArray;
    commonArray[1].spec_det_datavalue1 = data.usertxt;
    commonArray[1].spec_det_datavalue2 = data.location
      ? data.location[Object.keys(data.location)[0]].toString() +
        "," +
        data.location[Object.keys(data.location)[1]].toString()
      : null;
    this.setState({ commonArray });
   
  };
  changeText = (data, data2, dataobj, key) => {
    if (this.state.init)
      this.setState({
        init: false
      });
    var facilityData = this.state.facilityData;
    var commonArray = this.state.commonArray;
    if (data2) {
      if (data2 == "address") {
        this.getAddress();
      }
      return;
    } else {
      // alert("nothing");
    }
    if (Number.isInteger(dataobj.spec_det_id)) {
      var findIndex = facilityData.specDetails.findIndex(
        obj => obj.spec_det_id == dataobj.spec_det_id
      );
      if (findIndex != -1) {
        facilityData.specDetails[findIndex].spec_det_datavalue1 = data;
        this.setState({ facilityData });
      }
    } else {
      var findIndex = commonArray.findIndex(
        obj => obj.spec_det_id == dataobj.spec_det_id
      );
      if (findIndex != -1) {
        var errorcheck = ValidationLibrary.checkValidation(
          data,
          commonArray[findIndex].validation
        );
        console.log("fsd", errorcheck);
        commonArray[findIndex].spec_det_datavalue1 = data;
        commonArray[findIndex].error = !errorcheck.state;
        commonArray[findIndex].errormsg = errorcheck.msg;
        this.setState({ commonArray }, function() {
          this.checkValidations();
        });
      }
    }
   
  };
  checkValidations = (submit) => {
    var commonArray = this.state.commonArray;
    for (var i in commonArray) {
      var errorcheck = ValidationLibrary.checkValidation(
        commonArray[i].spec_det_datavalue1,
        commonArray[i].validation
      );
      commonArray[i].error = !errorcheck.state;
      commonArray[i].errormsg = errorcheck.msg;
    }
    var errordata = commonArray.filter(obj => obj.error == true);
    if (errordata.length != 0) {
      if(submit)Toast.show("Please fill out the fields !!", Toast.LONG);

       return false;

    } else {

       if (submit) {
         console.log('called submit');
 this.props.sendfaciltiydata(this.state.commonArray, false);
       }
      
       else
       return ;

  
    }
  
  }; 


  



  render() {
    // alert(JSON.stringify(this.state.facilityData));
    return (
      <Content>
        <View style={{ padding: 20, marginTop: 18 }}>
          <AddressComp
            receiveAddress={this.receiveAddress}
            modalstate={this.state.modalstate}
            onClose={() => this.setState({ modalstate: false })}
          />

          <DynamicForm
            changeText={this.changeText}
            commonArray={this.state.commonArray}
            facilityData={
              this.state.facilityData ? this.state.facilityData.specDetails : []
            }
          />

          <Next
            onClick={() => this.checkValidations(true)}
            name="Submit"
            _width={wp("40%")}
          />
        </View>
      </Content>
    );
  }

}

const styles = StyleSheet.create({
  itemsize: {
    height: hp("5.5%")
  }
});

export default CourseAddVenue;
