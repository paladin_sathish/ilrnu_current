import React from "react";
import { GiftedChat, Bubble } from "react-native-gifted-chat";
import color from "../Helpers/color";
import moment from "moment";
import {View} from 'react-native';

export default class ChatView extends React.Component {
                 state = {
                   messages: []
                 };

                 componentDidMount() {
                   this.setState({
                     messages: []
                   });
                 }

                 componentWillReceiveProps(props) {
                   if (props.messages != this.state.messages) {
                     var obj = []; 

                     if (props.messages.length<=0) {
                       obj.push({
                         _id: 1,
                         text: "Please Start Your Conversation",
                         createdAt: new Date(Date.UTC(2016, 5, 11, 17, 20, 0)),
                         system: true
                         
                       });
                     }
                     props.messages.forEach(element => {
                       var temp = {};
                       temp._id = element.chat_senderId;
                       temp.text = element.text;
                       temp.createdAt = element.created_at;
                       temp.user = {
                         _id: element.chat_receiverId,
                       };
                       obj.push(temp);
                     });
                     this.setState({
                       messages: obj
                     });
                   }

                   if(props.clear){
                     console.log('clear called ')
                       clearInterval(this.timer);
                   }
                 }

                 onSend(messages = []) {
                  //  this.setState(previousState => ({
                  //    messages: GiftedChat.append(
                  //      previousState.messages,
                  //      messages
                  //    )
                  //  }))

                    var obj = {
                      chatMessage: messages[0].text,
                      chatReceiverId:
                        this.props.userId.customerUserId != null
                          ? this.props.userId.customerUserId
                          : this.props.userId.venue_user_id,
                      messageTime: moment().format("YYYY-MM-DD hh:mm:ss"),
                      venueId: this.props.userId.venue_id
                    };
                    this.props.pushMessage(obj);
                 }

                 renderBubble(props) {
                   return (
                     <Bubble
                       {...props}
                       wrapperStyle={{
                         right: {
                           backgroundColor: color.orange
                         },
                         left: {
                           backgroundColor: color.white
                         }
                       }}
                     />
                   );
                 }


                 componentDidMount = () => {
                   console.log('didmount') 
                   this.timer = setInterval(() => {
                     this.props.refresh()
                   }, 5000);
                 };

                 componentDidUpdate = () => {
                   console.log('did updateview')
                 };
                 

                 componentWillUnmount() {
                    console.log("willunmount");
                   clearInterval(this.timer);
                 }

                 render() { 

                  console.log('ffdsfds',this.props.userId);
                  
                   return (
                     <View style={{flex:1}}>
                      
                       <GiftedChat
                         messages={this.state.messages}
                         onSend={messages => this.onSend(messages)}
                         alwaysShowSend={true}
                         renderBubble={this.renderBubble}
                         alignTop={true}
                         scrollToBottom={true}
                         user={{
                           _id:
                             this.props.userId.customerUserId != null
                               ? this.props.userId.customerUserId
                               : this.props.userId.venue_user_id
                         }}
                       />
                     </View>
                   );
                 }
               }
