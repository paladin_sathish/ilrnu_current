'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Text
} from 'react-native';
import ModalComp from '../components/Modal';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Icon} from 'native-base';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import placeholderImg from '../images/placeholder_img.png';

 const screenWidth = Dimensions.get('window').width;
 var self="";
class LoadCarouselModal extends Component {
constructor(props) {
  super(props);
self=this;
  this.state = {activeSlide:0};
}
checkImageURL=(url)=> {
  if(url.split('.').length>0){
  var imageurl=url.split('.')[url.split('.').length-1].toLowerCase();
  return ['jpg','jpeg','png','gif'].includes(imageurl);
}else{
  return false;
}
    // return(imageurl.match(/\.(jpeg|JPEG|jpg|JPG|gif|png|PNG)$/) != null);

}
_renderItem ({item, index}) {

        return (
                <Image   source={(self.checkImageURL(item.venue_image_path)?{uri:item.venue_image_path}:placeholderImg)} style={styles.homeBgImage}/>
        );
    }
    get pagination () {
        const {  activeSlide } = this.state;
        return (
            <Pagination
              dotsLength={this.props.items.length}
              activeDotIndex={activeSlide}
              containerStyle={{ backgroundColor: 'transparent',position:'absolute',bottom:-12,left:-5 }}
              dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 8,
                  backgroundColor: 'rgba(0, 0, 0, 0.92)'
              }}
              inactiveDotStyle={{
                  // Define styles for inactive dots here
              }}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
        );
    }
  render() {
    return (
<ModalComp center  closemodal={()=>this.props.closemodal&&this.props.closemodal()}>
              <View style={{flex:1,marginTop:45,marginBottom:0}}>
              <TouchableOpacity style={[styles.fontarrowiconLeft]} onPress={() => this._carousel.snapToPrev()}>
              <Icon style={[styles.fontAwesomImage,{opacity:this.state.activeSlide==0?0.5:1}]}  type="FontAwesome" name='angle-left'/></TouchableOpacity>


              <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.props.items}
              firstItem={0}
              autoplay={false}
              loop={false}
               onSnapToItem={(index) => this.setState({ activeSlide: index }) }
              renderItem={this._renderItem}
              sliderWidth={screenWidth}
              itemWidth={screenWidth}
              itemHeight={220}
              inactiveSlideScale={1}
              slideStyle={{height:220}}
            />
              <TouchableOpacity style={styles.fontarrowiconRight} onPress={() => this._carousel.snapToNext()}>
              <Icon style={[styles.fontAwesomImage,{opacity:this.state.activeSlide==this.props.items.length-1?0.5:1}]}  type="FontAwesome" name='angle-right'/></TouchableOpacity>
                           </View>

            </ModalComp>
        
    );
  }
}

const styles = StyleSheet.create({
    homeBgImage:{
        width:screenWidth,
    	height:hp('25%'),
      zIndex: 0
    },
    fontarrowiconRight:{
      position:'absolute',
      top:0,
      bottom:0,
      right:0,
      height:'80%',
      alignItems:'center',
      width:'auto',
      zIndex:1,
      flexDirection:'row',
      paddingLeft:12

    
    }
    ,fontarrowiconLeft:{
       position:'absolute',
      top:0,
      bottom:0,
     height:'80%',
      alignItems:'center',
      width:'auto',
      zIndex:1,
      flexDirection:'row',paddingRight:12
    },
    fontAwesomImage:{
      fontSize:hp('5%'),
      padding:15,
      color:'white'
    }
});


export default LoadCarouselModal;