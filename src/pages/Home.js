import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,BackHandler,ToastAndroid,Slider,Dimensions,ImageBackground,ActivityIndicator, Alert } from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {
  Right,
  Top,
  Icon,
  Content,
  Button,
  ListItem,
  Body,
  CheckBox,
  Item,
  Input
} from "native-base"; 
import color from '../Helpers/color';
import Popover from "react-native-popover-view";
import links from '../Helpers/config';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import HomeSearch from '../components/TopComp';
import VideoComp from '../components/VideoComp';
import NearbyPlayGrounds from '../components/NearbyPlayGrounds'
import AsyncStorage from '@react-native-community/async-storage';
import YourFavorites from '../components/Yourfavorites'
import SuggestionBirthday from '../pages/SuggestionBirthday'
import LoadingOverlay from '../components/LoadingOverlay'
import Toast from 'react-native-simple-toast';
 const screenWidth = Dimensions.get('window').width;

const fav_list=[
    {fav:'Birthday Party',id:0},
    {fav:'Soccer Ground',id:1},
    {fav:'Family Gathering Venues',id:2},
    {fav:'Meeting Hall',id:3},
    {fav:'Wedding Venue',id:4},
   ]; 


   

const playgr_list=[
    {image:require('../images/shutter/CAROUSEL-02.jpg'),text:'Meadow Crest Playground'},
    {image:require('../images/shutter/CAROUSEL-03.jpg'),text:'Meadow Crest Playground'},
    {image:require('../images/shutter/CAROUSEL-04.jpg'),text:'Meadow Crest Playground'},
    {image:require('../images/shutter/CAROUSEL-05.jpg'),text:'Meadow Crest Playground'},
    {image:require('../images/shutter/CAROUSEL-06.jpg'),text:'Meadow Crest Playground'},
    {image:require('../images/shutter/CAROUSEL-10-09.jpg'),text:'Meadow Crest Playground'},
    {image:require('../images/shutter/CAROUSEL-10.jpg'),text:'Meadow Crest Playground'},
] 




 class Home extends Component {
   constructor(props) {
     // alert(JSON.stringify(props));
     super(props);
     console.log("backprops", props);
     // this.state={
     //     visible:false,sessionTrue:null,loadvideo:true,listvenue:null,showSearchData:false
     // };

     this.state = {
       nearme: false,
       search_string: null,
       selected: "key0",
       loadvideo: false,
       favourites: null,
       visiblescreen: null,
       loading: null,
       isVisible: false
     };
   }
   onValueChange = value => {
     this.setState({
       selected: value
     });
   };
   _playvideo() {
     this.setState({ loadvideo: true });
     // Actions.Venue_search();
   } 
   
   nxtpage = (data, data1, data2,count) => {
     // alert(JSON.stringify(data));
     Actions.Search_Home({ data: data, data1: "data1", data2: data2,count:count });
   };

   componentDidMount() {
    
     console.log('called')
     // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   handleBackButton = () => {
     // alert("back button disabled");
     // ToastAndroid.show('Back button is disabled', ToastAndroid.SHORT);
     return true;
   };

   componentWillUnmount() {
     BackHandler.removeEventListener(
       "hardwareBackPress",
       this.handleBackButton
     );
   }

   // 	closepopup=()=>{
   // 		this.setState({visible:false});
   // 	}
   // 	listyourvenue=()=>{

   //       this.setState({loadvideo:true});
   //     //this.setState({listvenue:true});
   //    //this.setState({sessionTrue:true});

   //  		//
   // 	}
   closevideo = () => {
     // 		AsyncStorage.getItem('loginDetails', (err, result) => {
     this.setState({ loadvideo: false });
     //  	if(result !=null){

     // 		// this.setState({sessionTrue:true});
     // 		Actions.venueform();
     //  	}else{
     // // this.setState({loadvideo:true});
     // if(this.state.listvenue==true){
     // 		this.setState({sessionTrue:false});

     // }
     //  	}
     //  });
   };
   // 	tabAction=()=>{
   // 		Actions.venueform();
   // 	}
   // 	 showsearch=(data)=>{
   //         this.setState({showSearchData:data})
   //     }
   componentWillReceiveProps(props) {
     // console.log("backprops1",props);
     var loadingcheck = props.navigation.state.params.loading;
     var loadingcheckmsg = props.navigation.state.params.msg;
     var successpayment = props.navigation.state.params.successpayment;
     this.setState({ loading: loadingcheck ? loadingcheck : null });
     if (successpayment == true) {
       // Toast.show(loadingcheckmsg,Toast.LENGTH_LONG)
       // alert(loadingcheckmsg);
       // Toast.show(loadingcheckmsg,Toast.LONG)
     } else {
       // this.setState({})
       if (successpayment == "error") {
         Toast.show(loadingcheckmsg, Toast.LONG);
       }
     }
   } 
   visiblescreen = data => {
     this.setState({ visiblescreen: data, favourites: null });
   };
   _renderItem({ item, index }) {
     return <Image source={item.image} style={styles.homeBgImage} />;
   }

   scrollToTop = () => {
     this.scroller.scrollTo({ x: 0, y: 162 });
   };

   submitSearch = async () => {
     const manuallocation = JSON.parse(
       await AsyncStorage.getItem("chosenlocation")
     );
     let lat = manuallocation != null ? manuallocation.lat : null;
     let long = manuallocation != null ? manuallocation.lng : null;

     if (this.state.search_string != null) {
       fetch(links.APIURL + "commonSearch", {
         method: "POST",
         headers: {
           Accept: "application/json",
           "Content-Type": "application/json"
         },
         body: JSON.stringify({
           searchContent: this.state.search_string,
           nearme: this.state.nearme,
           offset: 0,
           lat: lat,
           long: long
         })
       })
         .then(response => response.json())
         .then(responseJson => {
           console.log("search res", responseJson.data);
           // alert(responseJson.count);
           if (responseJson.status == 0) {
             Actions.Venue_search({
               data: responseJson.data.length > 0 ? responseJson.data : [],
               data1: "",
               data2: "",
               offset:0,
               nearme:this.state.nearme,
               searchContent:this.state.search_string,
               latlng: {
                 latitude: lat,
                 longtitude: long
               }
             });
           } else {
            Toast.show("No Records Found", Toast.LONG);
             this.setState({ NoRecords: true });
           }
         });
            
     } else Toast.show("No Records Found", Toast.LONG);
   };
   showPopover() {
     this.setState({ isVisible: true });
   }

   closePopover() {
     this.setState({ isVisible: false });
   }
   render() {
     return (
       <View style={styles.container}>
         {this.state.loading && <LoadingOverlay />}

         <ScrollView>
           <View showsVerticalScrollIndicator={false}>
             <View style={(styles.searchContainer, { flex: 1 })}>
               {/* <HomeSearch
                    nxtpge={(data, data1, data2) =>
                      this.nxtpage(data, data1, data2)
                    }
                  ></HomeSearch> */}
               <HomeSearch
                 nxtpge={(data, data1, data2,count) =>
                   this.nxtpage(data, data1, data2,count)
                 }
               />
             </View>
             {/*
                <Carousel
                  ref={c => {
                    this._carousel = c;
                  }}
                  data={playgr_list}
                  firstItem={0}
                  autoplay={true}
                  loop={true}
                  contentContainerCustomStyle={{ flexGrow: 0 }}
                  renderItem={this._renderItem}
                  sliderWidth={screenWidth}
                  itemWidth={screenWidth}
                  itemHeight={150}
                  inactiveSlideScale={1}
                  slideStyle={{ heigth: 500 }}
                />
              */}

             {/*181
                                <View style={styles.play_button_container}>

                                    <View style={styles.png_back}>181
                                        <TouchableOpacity onPress={()=>this._playvideo()} style={{justifyContent:'center',alignItems:'center',alignSelf:'center',alignContent:'center',}}>
                                        <Image source={Video_png} style={styles.png_style}>

                                        </Image>
                                     
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <Text style={[{color:color.white,paddingLeft:wp('1%')},styles.text_near]}>
                                            How to Search Venue
                                        </Text>
                                        <Text style={[{color:color.orange,paddingLeft:wp('1%')},styles.text_near]}>
                                           A Guided video
                                        </Text>
                                    </View>
                                </View>*/}

             <View style={styles.search}>
               <View style={{ paddingVertical: 10 }}>
                 <Text
                   style={{
                     fontSize: 24,
                     color: "white",
                     flexWrap: "wrap",
                     textAlign: "center",
                     fontStyle: "normal"
                   }}
                 >
                   Explore and Book various venue types instantly{" "}
                 </Text>
               </View>
               <View>
                 <Icon
                   active
                   name="info-circle"
                   style={{ fontSize: 28, color: "white" }}
                   type="FontAwesome"
                   color="white"
                   ref={ref => (this.touchable = ref)}
                   onPress={() => this.showPopover()}
                 />
               </View>

               <View style={{ width: wp("80%"), paddingVertical: 4 }}>
                 <Item rounded style={{ backgroundColor: "white" }}>
                   <Input
                     onChangeText={value => {
                       this.setState({ search_string: value });
                     }}
                   />
                   <Icon
                     active
                     name="search"
                     style={{ fontSize: 20, color: color.black1 }}
                     type="FontAwesome"
                   />
                 </Item>
                 <ListItem noBorder>
                   <CheckBox
                     color={this.state.nearme ? "transparent" : "white"}
                     style={{
                       backgroundColor: this.state.nearme
                         ? "white"
                         : "transparent"
                     }}
                     checked={this.state.nearme}
                     onPress={() =>
                       this.setState({ nearme: !this.state.nearme })
                     }
                   />
                   <Body>
                     <Text style={{ color: "white" }}>{"  "}Near Me</Text>
                   </Body>
                 </ListItem>
               </View>

               <View
                 style={{
                   flex: 1,
                   flexDirection: "row",
                   justifyContent: "flex-end"
                 }}
               >
                 <Button
                   rounded
                   style={[styles.actionbtn, { backgroundColor: color.orange }]}
                   onPress={() => this.submitSearch()}
                 >
                   <Text style={styles.actionbtntxt}>Search Venue</Text>
                 </Button>
               </View>
             </View>

             <Popover
               position={"left"}
               isVisible={this.state.isVisible}
               fromView={this.touchable}
               onRequestClose={() => this.closePopover()}
               arrowSize={300}
             >
               <View style={{ padding: 12, width: 150 }}>
                 <Text style={{ fontSize: 16 }}>
                   Search your venue {"\n"} by Name, Location, Category or Type.
                 </Text>
               </View>
             </Popover>

             <View style={styles.fav_item_container}>
               <YourFavorites
                 sendFavouries={item => this.setState({ favourites: item })}
               />
             </View>

             <View style={styles.line} />

             <View style={styles.fav_item_container}>
               <NearbyPlayGrounds
                 visiblescreen={data => this.visiblescreen(data)}
                 visiblescreenData={this.state.visiblescreen}
                 category={this.state.favourites ? this.state.favourites : null}
               />
             </View>

             <View style={styles.line} />

             <SuggestionBirthday
               visiblescreen={data => this.visiblescreen(data)}
               visiblescreenData={this.state.visiblescreen}
             />
             <View style={styles.line} />
           </View>
         </ScrollView>

         {this.state.loadvideo == true && (
           <VideoComp
             headerText={"How to search course in iVNEU?"}
             closevideo={() => this.closevideo()}
           />
         )}
       </View>
     );
   }
 }
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white
  },
  actionbtn: {
    borderRadius: 50,
    width: hp("25%"),
    justifyContent: "center"
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2%"),
    fontWeight: "bold"
  },
  search: {
    display: "flex",
    padding: wp("2"),
    backgroundColor: "#a60202",
    justifyContent: "center",
    alignItems: "center",
  },
  top_container: {
    height: hp("10%"),
    flexDirection: "row",
    backgroundColor: color.top_red,
    padding: hp("2%")
  },
  text_style: {
    color: color.white,
    fontSize: wp("3%")
  },
  drop_down_container: {
    flex: 1,
    justifyContent: "center",
    marginBottom: hp("2.8%"),
    marginTop: hp("2.8%"),
    marginLeft: 8,
    marginRight: 8
  },
  drop_down: {
    backgroundColor: color.white,
    borderRadius: hp("2%"),
    paddingLeft: 10,
    paddingRight: 10
    // paddingLeft:wp('2%'),
    // paddingRight:wp('2.5%'),
    // paddingRight:3,
    // paddingLeft:3
  },
  homeBgImage: {
    width: screenWidth,
    height: hp("25%"),
    resizeMode: "stretch"
  },
  play_button_container: {
    position: "absolute",
    flexDirection: "row",
    right: 0,
    top: 0,
    paddingTop: hp("1.5%"),
    paddingRight: hp("1.5%")
  },
  png_style: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    height: hp("3%"),
    width: hp("3%")
  },
  png_back: {
    backgroundColor: color.white,
    height: hp("4%"),
    width: hp("4%"),
    borderRadius: hp("4%") / 2,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    elevation: 1
  },
  flex_row: {
    flexDirection: "row"
  },
  flex_sep: {
    flex: 1
  },
  down_aarow: {
    height: hp("1%"),
    width: hp("1%"),
    justifyContent: "flex-end",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    alignContent: "flex-end",
    margin: 3
  },
  searchContainer: {
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    paddingLeft: "1%"
  },
  fav_item_container: {
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    paddingLeft: "1%"
  },
  text_near: {
    fontSize: hp("1.9%")
  },
  text_fav: {
    fontSize: hp("1.9%"),
    color: color.blue,
    fontWeight: "bold"
  },
  text_center: {
    alignItems: "center"
  },
  line: {
    backgroundColor: color.ash,
    width: "100%",
    height: 1
  },
  add_symbol_container: {
    backgroundColor: color.blue,
    justifyContent: "flex-end",
    justifyContent: "center",
    alignSelf: "flex-end",
    marginRight: wp("5%"),
    height: hp("2.5%"),
    width: hp("2.5%")
  },
  add_symbol: {
    height: hp("1.3%"),
    width: hp("1.3%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center"
  },
  right_arr_style: {
    height: hp("2%"),
    width: hp("2%"),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    margin: 3
  },
  right_arr_container: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    alignContent: "center",
    backgroundColor: color.white
  },
  mar_adjuse: {
    marginTop: 3,
    marginBottom: 3
  },
  dropdown_container: {
    width: "100%",
    height: hp("4%"),
    justifyContent: "center",
    alignSelf: "center"
  },
  dropdown_text: {
    //   fontSize: hp('2%'),
    justifyContent: "center"
  },
  dropdown_options: {
    marginTop: 10,
    width: wp("30%")
  },
  drop_down_top: {
    height: hp("1%"),
    width: hp("1%"),
    alignSelf: "center"
  }
}); 


export default Home; 


