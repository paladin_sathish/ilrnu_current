import React, { Component } from "react";
import { View, Text, StyleSheet,Alert } from "react-native";
import { Content, Button, Tabs, ScrollableTab, Tab } from "native-base";
import MonthCalender from "../components/monthcalender";
import RangePicker from "../components/RangePicker";
import color from "../Helpers/color";
import dateFormat from "dateformat";
import DateFunctions from "../Helpers/DateFunctions";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { Actions } from "react-native-router-flux";
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import moment from "moment";
import Toast from "react-native-simple-toast";
import links from "../Helpers/config";
import CalenderBooked from "./calenderBooked";






const NameStar = ({ name }) => (
  <React.Fragment>
    <Text>
      {name} {""}
    </Text>{" "}
    <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
);
var formkeys = [
  {
    name: "name",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Name" />
  },
  {
    name: "event_name",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Event Name" />
  },
  {
    name: "mobile_no",
    type: "default",
    labeltype: "text1",
    label: <NameStar name="Phone No" />
  },
  {
    name: "email",
    type: "default",
    labeltype: "text1",
    label: "Email"
  }
];













export default class MonthAvailability extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     selectedSlot: [],
                     initialPage: 0,
                     activeTab: 0,
                     selectedDate: "",
                     minDate: "",
                     maxDate: "",
                     BookedDates: [],
                     blockedslots: props.blockedslots,
                     passData: {},
                     name: "",
                     mobile_no: "",
                     event_name: "",
                     email: "",
                     validations: {
                       name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       },
                       mobile_no: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "mobile", status: false }]
                       },
                       event_name: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: []
                       },
                       email: {
                         error: null,
                         mandatory: false,
                         errormsg: "",
                         validations: []
                       }
                     }
                   };
                 }



                 componentWillReceiveProps(props) {
           
                     this.setState({
                       blockedslots: props.blockedslots
                     });
                   
                 }

                 _renderForm() {
                   return (
                     <View
                       style={{
                         paddingTop: 20,
                         paddingRight: 20,
                         paddingLeft: 20,
                         paddingBottom: 20,
                         backgroundColor: "white"
                       }}
                     >
                       {formkeys.map((obj, key) => {
                         return (
                           <View style={{ marginBottom: 15 }}>
                             <LabelTextbox
                               inputtype={obj.type}
                               key={key}
                               type={obj.labeltype}
                               capitalize={obj.name != "dob"}
                               error={this.state.validations[obj.name].error}
                               errormsg={
                                 this.state.validations[obj.name].errormsg
                               }
                               changeText={data =>
                                 this.changeText(data, obj.name)
                               }
                               value={this.state[obj.name]}
                               labelname={obj.label}
                             />
                           </View>
                         );
                       })}
                     </View>
                   );
                 }

                 changeText = (data, key) => {
                   // alert(data);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                   if (key == "dob") {
                     if (data == "errordob") {
                       validations[key].error = true;
                       validations[key].errormsg =
                         "Invalid Date Accept Format (dd-mm-yyyy)";
                     } else {
                       validations[key].error = false;
                     }
                   } else if (key == "mobile") {
                     errormsg = ValidationLibrary.checkValidation(
                       data.mobile,
                       this.state.validations[key].validations
                     );
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   } else {
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   }
                   this.setState({ validations });
                   this.setState({
                     [key]: data
                   });
                 };

                 _hitBlock = data => {
                   fetch(links.APIURL + "blockDatesAndSlots", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify(data)
                   })
                     .then(response => response.json())
                     .then(responsejson => { 
                       console.log('responsejsdfsd',responsejson)
                       if (responsejson.status == 0) {
                         Alert.alert("Date's Blocked Successfully !!");
                         this._resetForm();
                         this.props.refresh();
                       } else {
                         Toast.show("Something Went Wrong !!", Toast.LONG);
                       }
                     });
                 }; 

    _resetForm=()=>{
                   this.setState({
                     name:'',
                     email:'',
                     event_name:'',
                     mobile_no:''
                   })
                 }


                 receiveDates = dates => {
                   console.log("received Dates....", dates);
                   // alert(dates);
                   var getDates = DateFunctions.enumerateDaysBetweenDates(
                     dateFormat(dates.fromTime, "yyyy-mm-dd"),
                     dateFormat(dates.toTime, "yyyy-mm-dd")
                   );
                   var quantity,
                     typeofvenue = "";
                   if (this.props.availabilityType == 4) {
                     quantity = Math.ceil(getDates.length / 30);
                     typeofvenue = "Month";
                   } else if (this.props.availabilityType == 3) {
                     quantity = Math.ceil(getDates.length / 7);
                     typeofvenue = "Week";
                   } else {
                     quantity = getDates.length;
                     typeofvenue = "Day";
                   }
                   var obj = {
                     typeofvenue: typeofvenue,
                     quantity: quantity,
                     dates: getDates,
                     from: dateFormat(dates.fromTime, "yyyy-mm-dd"),
                     to: dateFormat(dates.toTime, "yyyy-mm-dd")
                   }; 
                   this.setState({passData:obj})
                  // alert(JSON.stringify(obj));
                   // this.props.receiveDates && this.props.receiveDates(obj);

                   // alert(JSON.stringify(getDates));
                   // this.updateMonthlyData(this.props.venueId,dateFormat(dates.fromTime,'yyyy-mm-dd'),dateFormat(dates.toTime,'yyyy-mm-dd'));
                   // var obj={}
                 }; 


                 monthChange =(data)=>{  

               
  var startofmonth = moment(data)
                   .startOf("month")
                   .format("YYYY-MM-DD");
                 var endofmonth = moment(data)
                   .endOf("month")
                   .format("YYYY-MM-DD");
                
                console.log("startofmonth", startofmonth);
              
                   if (startofmonth < this.props.minDate) {
                     startofmonth = this.props.minDate; 
                     console.log('start',startofmonth)
                     
                   }
                   if (endofmonth > this.props.maxDate) {
                     endofmonth = this.props.maxDate;
               console.log("end", endofmonth);
              
                   }   

                          this.setState(
                            {
                              maxDate: endofmonth,
                              minDate: startofmonth
                            },
                            () =>
                            {
                                if (data != null)
                                  this.props.refresh({
                                    startofmonth: this.state.minDate,
                                    endofmonth: this.state.maxDate
                                  });
                            }
                          );
               
                 }

                 BlockFormData = () => { 

                   const {passData} = this.state;
                   var err = 0;

                   for (const key of Object.keys(this.state.validations)) {
                     console.log("valid", this.state.validations[key].error);
                     if (
                       this.state.validations[key].error != false &&
                       this.state.validations[key].mandatory == true
                     )
                       err += 1;
                   }

                   console.log("error list", err); 

                   if (err > 0) {
                     Toast.show("Field's Missing !!", Toast.LONG);
                     return;
                   } else {
                     var availablity = this.props.availability; 
                     if (!passData.from || !passData.to) {
                        Toast.show("Required Field's Missing !!", Toast.LONG);
                       return;
                     }

                     var venue_id = availablity
                       ? availablity.trn_venue_id
                       : null;
                     var venuetype = availablity
                       ? availablity.trn_availability_type
                       : null;
                     var obj = {
                       venueId: this.props.venueId,
                       venueType: this.props.venueType,
                       userId: this.props.userId,
                       bookingName: this.state.name,
                       eventName: this.state.event_name,
                       bookingPhone: this.state.mobile_no,
                       bookingEmail: this.state.email,
                       availType: this.props.availabilityType,
                       fromDateTime: dateFormat(
                         this.state.passData.from,
                         "yyyy-mm-dd"
                       ),
                       toDateTime: dateFormat(this.state.passData.to, "yyyy-mm-dd"),
                       slots: []
                     }; 

var listarray = passData.dates.map(obj => {
  var iteration = {
    fromdate: dateFormat(obj, "yyyy-mm-dd"),
    todate: dateFormat(obj, "yyyy-mm-dd"),
    slotFromTime: "00:00:00",
    slotToTime: "00:00:00"
  };
  return iteration;
}); 
obj.slots = listarray; 
                    
                     if (obj.slots.length > 0) {
                       //fetch called
                       
                       console.log("hitman", obj);
                       this._hitBlock(obj);
                     } else {
                       Toast.show("No Dates Were Chosen !!", Toast.LONG);
                       return;
                     }
                   } //else end
                 };

                 render() {
                console.log('maxdd',this.state.maxDate)
                   return (
                     <Content>
                       <React.Fragment key={2} style={styles.container}>
                         <View>
                           <RangePicker
                             selectedDate={dates => this.receiveDates(dates)}
                             minDate={this.state.minDate}
                             maxDate={this.state.maxDate}
                             blockDates={this.state.blockedslots}
                             type={this.props.availabilityType}
                             closeRangePicker={() => Actions.pop()}
                             monthChange={this.monthChange}
                           />
                         </View>

                         <View>
                           <View
                             style={{
                               height: 8,
                               backgroundColor: "#edeef2"
                             }}
                           />
                           <Tabs
                             tabBarUnderlineStyle={{
                               borderBottomWidth: 4,
                               borderBottomColor: "#5067FF"
                             }}
                             renderTabBar={() => (
                               <ScrollableTab
                                 style={{ backgroundColor: "white" }}
                               />
                             )}
                             initialPage={this.state.initialPage}
                             page={this.state.activeTab}
                           >
                             <Tab
                               style={styles.tab}
                               heading="Available Slots"
                               tabStyle={{ backgroundColor: "white" }}
                               textStyle={{ color: "orange" }}
                               activeTabStyle={{
                                 backgroundColor: "transparent"
                               }}
                               activeTextStyle={{
                                 color: "#5067FF",
                                 fontWeight: "normal"
                               }}
                             >
                               <React.Fragment>
                                 {this._renderForm()}

                                 <View
                                   style={{
                                     flex: 1,
                                     justifyContent: "center",
                                     alignItems: "center"
                                   }}
                                 >
                                   <Button
                                     style={{
                                       borderRadius: 0,
                                       backgroundColor: color.blueactive
                                     }}
                                     block
                                     onPress={() => this.BlockFormData()}
                                   >
                                     <Text style={{ color: color.white }}>
                                       Block
                                     </Text>
                                   </Button>
                                 </View>
                               </React.Fragment>
                             </Tab>

                             <Tab
                               style={styles.tab}
                               heading="Booked Details"
                               tabStyle={{ backgroundColor: "white" }}
                               textStyle={{ color: "orange" }}
                               activeTabStyle={{
                                 backgroundColor: "transparent"
                               }}
                               activeTextStyle={{
                                 color: "#5067FF",
                                 fontWeight: "normal"
                               }}
                             >
                               <CalenderBooked
                                 data={this.props.bookingDetails}
                                 refresh={this.props.refresh}
                               />
                             </Tab>
                           </Tabs>
                         </View>
                       </React.Fragment>
                     </Content>
                   );
                 }

                 //   {/*
                 //     updateMonthlyData = (venueid, fromdates, todates) => {
                 //     var obj = { venueId: venueid, date: fromdates, todate: todates };
                 //     fetch(links.APIURL + "providerCalendarHourly", {
                 //       method: "POST",
                 //       headers: {
                 //         Accept: "application/json",
                 //         "Content-Type": "application/json"
                 //       },
                 //       body: JSON.stringify(obj)
                 //     })
                 //       .then(resp => resp.json())
                 //       .then(respjson => {
                 //         // alert(JSON.stringify(respjson));
                 //         if (respjson.status == 0) {
                 //           var blockedDates =
                 //             respjson.data.length > 0 ? respjson.data[0].availability : [];
                 //           if (blockedDates.length > 0) {
                 //             var filterRecords = blockedDates
                 //               .filter(obj => obj.booking_slot_type != "")
                 //               .map(dates => dates.selected_date);
                 //             this.setState({ blockedslots: filterRecords });

                 //             // alert(JSON.stringify(filterRecords));
                 //           } else {
                 //             this.setState({ blockedslots: [] });
                 //           }
                 //         } else {
                 //           this.setState({ blockedslots: [] });
                 //         }
                 //       });
                 //   };
                 // */}
                 componentDidMount() {
                   // this.updateMonthlyData(this.props.venueId,dateFormat(this.props.minDate,'yyyy-mm-dd'),dateFormat(this.props.maxDate,'yyyy-mm-dd'));
                   this.monthChange()
                   

              setTimeout(() => {
                this.setState({ activeTab: 0 });
              }, 0);

                 }
               }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  }
});
