import React, { Component } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import { Content, Button, Tabs, ScrollableTab, Tab } from "native-base";
import color from "../Helpers/color";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import moment from "moment";
import Toast from "react-native-simple-toast";
import links from "../Helpers/config";
import Next from '../components/button';


const NameStar = ({ name }) => (
  <React.Fragment>
    <Text>
      {name} {""}
    </Text>{" "}
    <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
);
var formkeys = [
  {
    name: "course_description",
    type: "default",
    labeltype: "textarea1",
    flex: "column",
    label: <NameStar name="Course Description" />
  },
  {
    name: "course_outline",
    type: "default",
    labeltype: "textarea1",
    flex: "column",
    label: "Course Outline"
  },
  {
    name: "faqs",
    type: "default",
    flex: "column",
    labeltype: "textarea1",
    label: "FAQ's"
  }
];

export default class CourseDetail1 extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     course_description: "",
                     course_outline: "",
                     faqs: "",
                     error: false,
                     showloading: false,
                     edit:false,
                     validations: {
                       course_description: {
                         error: null,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       },
                       course_outline: {
                         error: null,
                         mandatory: false,
                         errormsg: "",
                         validations: []
                       },
                       faqs: {
                         error: null,
                         mandatory: false,
                         errormsg: "",
                         validations: []
                       }
                     }
                   };
                 } 




                 onSubmit = () => { 

                  const {course_description,course_outline,faqs} = this.state;
                   console.log("submit", course_description);
                   var err = 0;
                   for (const key of Object.keys(this.state.validations)) {
                     if (
                       this.state.validations[key].error != false &&
                       this.state.validations[key].mandatory == true
                     )
                       err += 1;
                   }
                   console.log("validataiondata", this.state.validations);
                   console.log("error list", err);
                   if(this.state.edit && this.state.course_description == ''){
                     console.log('if')
                     Toast.show("Please fill the fields!!", Toast.LONG);
                     return;
                   }
                   else if (err > 0  && !this.state.edit) {
                       console.log("else");
                     Toast.show("Please fill the fields!!", Toast.LONG);
                     return;
                   } else {
                     
                     var obj = {
                       courseDesc: this.state.course_description,
                       courseOutline: this.state.course_outline,
                       courseFAQ: this.state.faqs
                     };
                     this.props.onSubmit(obj, this.state);

                   }
                 }; 


                    onSend =()=>{
                    var obj = {
                      courseDesc: this.state.course_description,
                      courseOutline: this.state.course_outline,
                      courseFAQ: this.state.faqs
                    };
                    this.props.sendCourseDetail1(obj, this.state);
                 }


                 _renderForm() {
                   return (
                     <View
                       style={{
                         padding: 10,
                         backgroundColor: "white"
                       }}
                     >
                       {formkeys.map((obj, key) => {
                         return (
                           <View style={{ marginBottom: 10 }}>
                             <LabelTextbox
                               inputtype={obj.type}
                               key={key}
                               type={obj.labeltype}
                               capitalize={obj.name != "dob"}
                               error={this.state.validations[obj.name].error}
                               errormsg={
                                 this.state.validations[obj.name].errormsg
                               }
                               changeText={data =>
                                 this.changeText(data, obj.name)
                               }
                               value={this.state[obj.name]}
                               labelname={obj.label}
                             />
                           </View>
                         );
                       })}
                     </View>
                   );
                 }

                 _resetForm = () => {
                   this.setState({
                     name: "",
                     email: "",
                     event_name: "",
                     mobile_no: ""
                   });
                 };

                 changeText = (data, key) => {
                   // alert(data);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                    
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                  
                   this.setState({ validations });
                   this.setState({
                     [key]: data
                   },()=>this.onSend());
                 };


                   componentWillReceiveProps(props){ 

                     if (props.courseDetail1State!=null) {
                       this.state = props.courseDetail1State
                     } 

                       if (props.edit) {
                        this.setState({
                          edit:true
                        })
                       } 


                       if (props.courseDetail1Data != null) {
                       console.log("recieve", props.courseDetail1Data);
                         this.setState(
                           {
                             course_description:
                               props.courseDetail1Data.courseDesc,
                             course_outline:
                               props.courseDetail1Data.courseOutline,
                             faqs: props.courseDetail1Data.courseFAQ
                           }
                          
                         );
                       }


                     
                 }
                 componentWillMount() {
                   this.props.headertext({
                     headertext: (
                       <Text
                         style={{
                           fontSize: hp("1.7%"),
                           fontWeight: "bold",
                           color: color.blue
                         }}
                       >
                         Course Details
                       </Text>
                     )
                   });
                   this.props.labeltext({
                     labeltext: (
                       <Text>
                         <Text style={{ color: color.orange }}>
                           {this.props.roomname}
                         </Text>

                         <Text
                           style={{ color: color.orange, fontWeight: "bold" }}
                         >
                           Course Details
                         </Text>
                       </Text>
                     )
                   });
                 } 


                 render() {
                   return (
                     <Content>
                       <View>{this._renderForm()}</View>
                       <View style={{ marginBottom: 10 }}>
                         <Next
                           onClick={() => this.onSubmit()}
                           name="Next"
                           _width={wp("40%")}
                         />
                       </View>
                     </Content>
                   );
                 }
               }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#f1f1f1"
  },
  actionbtn: {
    borderRadius: 5,
    width: hp("17%"),
    justifyContent: "center",
    backgroundColor: color.blue
  },
  actionbtntxt: {
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  }
});
