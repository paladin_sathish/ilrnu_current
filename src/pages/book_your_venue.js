import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,ScrollView,AsyncStorage,Linking} from 'react-native';
import {Right,Top,Icon, Button,Content,Container} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import Line from '../components/Line'
import Text_box from './text_box'
import { Actions, Router, Scene } from 'react-native-router-flux';
import Drop_Downbox from '../components/drpdown_container'
import Dropdown_conatiner from '../components/drpdown_container';
import RangePicker from '../components/RangePicker';
import Calender from '../images/svg/cal2.png'
import DatePicker from 'react-native-datepicker'
import dateFormat from 'dateformat';
import links from '../Helpers/config';
import Toast from 'react-native-simple-toast';
Array.prototype.unique = function() {
  var a = this.concat();
  for(var i=0; i<a.length; ++i) {
    for(var j=i+1; j<a.length; ++j) {
      if(a[i] === a[j])
        a.splice(j--, 1);
    }
  }

  return a;
};


const jsCoreDateCreator = (dateString) => { 
  // dateString *HAS* to be in this format "YYYY-MM-DD HH:MM:SS"
  let dateParam = dateString.split(/[\s-:]/)  
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString()  
  // alert(dateParam);
  return new Date(...dateParam)  
}
var dates=[{id:1,date:1,month:8,sample:'jdv'},{id:2,date:2,month:8,sample:'ndvjd'},{id:3,date:3,month:8},{id:4,date:4,month:8},{id:5,date:5,month:8},{id:6,date:6,month:8},{id:7,date:7,month:8},
{id:8,date:8,month:8},{id:9,date:9,month:8},{id:10,date:10,month:8},{id:11,date:11,month:8},{id:12,date:12,month:8},{id:13,date:13,month:8},{id:14,date:14,month:8},{id:15,date:15,month:8}]

var times=[{id:1,timer:'8:00 AM - 9:00 AM',block:true},{id:2,timer:'9:00 AM - 10:00 AM',block:true},{id:3,timer:'10:00 AM - 11:00 AM',block:false},{id:4,timer:'11:00 AM - 12:00 PM',block:true},
{id:5,timer:'12:00 PM - 1:00 PM',block:false},{id:5,timer:'2:00 PM - 3:00 PM',block:true},{id:6,timer:'3:00 PM - 4:00 PM',block:true},]
var bookdetails={
"venue_id":"",
"user_id":"",
"trn_venue_price_amt":"",
"bookingFrom":"",
"bookingTo":"",
"moreInfo":"",
"promoId":null,
"promoType":"",
"promoValue":0,
"promoAmount":0.0,
"finalPrice":"1900"
}

export default class Book_your_venue extends Component{
    constructor(props) {
        super(props);
        // alert(JSON.stringify(props));
        this.state= {
           ratingCount:3,
           priceamount:0,
           availability:null,
           favStarCount:1,
           from:'',to:'',
           isHidden: false,
           isHidden2:false,
           avaialableSLot:null,
           maxDate:null,
           minDate:null,
           newTop:0,
           BookedDates:[],
           loginDetails:null,
           bookdetails:bookdetails,
           dateValue:props.dataValuesDetails,
           block:true,dates:null,listTime:[],frmtime:null,totime:null,enableRangePicker:null
          
        }       
    }


    _onPress()
     {
   
        this.setState({isHidden: true});
       
      }
      _onPress2(){
          this.setState({isHidden2:false})
          this.setState({isHidden:false})
      //    this.setState({_backgroundcolor:color.white})
      }
componentWillReceiveProps(props){
  // alert(JSON.stringify(props));
}
      SampleFunction=(obj)=>{
    
        // alert(JSON.stringify(obj.date));
        this.getTime(obj.date);
            this.setState({isHidden2:true})
    //        Alert.alert(obj.sample);
            // this.setState({dateValue:obj.id})
          
        
        }
        getslottime=(obj)=>{
          // alert(JSON.stringify(obj));
          this.setState({frmtime:new Date(obj.fromTime)})
          this.setState({totime:new Date(obj.toTime)})
          this._onPress2();
        }

    
    onLayout=(event)=>{
        const {x, y, height, width} = event.nativeEvent.layout;

      
        this.setState({newLeft:event.nativeEvent.layout.x+50});
        this.setState({newHeight:event.nativeEvent.layout.height-310});
        this.setState({newTop:event.nativeEvent.layout.x});
    
        this.setState({ LayoutProp: event.nativeEvent.layout });
    }
    onLayout1=(event)=>{
        const {x, y, height, width} = event.nativeEvent.layout;
        this.setState({newLeft1:event.nativeEvent.layout.x+20});
        this.setState({ LayoutProp: event.nativeEvent.layout });
    }
    setDate=(date,key)=>{
        // alert(date);
    // var jsondate=JSON.stringify(new Date(date));
        this.setState({[key]:date});
        var data=this.state;
        data[key]=date;
        this.props[key](data);
      }
    onStarRatingPress(rating) {
        this.setState({
          ratingCount: rating
        });
      }
      getDate=(start, end)=>{
    start=new Date(start);
    end=new Date(end);
    var arr = new Array();
    var dt = new Date(start);
    while (dt <= end) {
        arr.push({date:dateFormat(new Date(dt), "yyyy-mm-dd"),altdate:dateFormat(new Date(dt), "dd/mm")});
        dt.setDate(dt.getDate() + 1);
    }
    return arr;


}
componentWillMount(){
    AsyncStorage.getItem('loginDetails', (err, result) => {
      
       if(result!=null){
    this.setState({loginDetails:JSON.parse(result)})
      }else{
    this.setState({loginDetails:null})

      }
     });
}
getTime=(date,t1, t2)=>{
    // alert(t1);
    // debugger;
    t1=this.props.bookdata.availability.length>0?this.props.bookdata.availability[0].trn_venue_avail_frm:null;
    t2=this.props.bookdata.availability.length>0?this.props.bookdata.availability[0].trn_venue_avail_to:null;
    t1=jsCoreDateCreator(t1).getHours();
    t2=jsCoreDateCreator(t2).getHours();
    var timeArray = [],
    d = new Date(date),
    mill = d.getTime(),
    h = 0,
    m = 0,

    d1 = new Date(dateFormat(new Date(), "yyyy-mm-dd")),
    millNow = d1.getTime(),
    meridiem = ['AM','PM'];

    if(mill >= millNow){

      if(mill == millNow){
        d = new Date();
        mill = d.getTime();
        h = d.getHours();
        m = d.getMinutes();
      }


      for (var i = h; i < 24; ++i) {
        for (var j = i==h ? Math.ceil(m/60) : 0; j < 1; ++j) {

          if(i>=t1 && i<=t2){

            timeArray.push({fromTime:this.setDateHours(d,i),toTime:this.setDateHours(d,(i+1)),availableTime:(i%12==0?i:i%12) + ':' + (j*15||'00')+ meridiem[(i)/12|0] + '-'+((i+1)%12==0?(i+1):(i+1)%12) + ':' + ((j)*15||'00') + meridiem[(i+1)/12|0],hours:i});
            
          }
        }
      }
    }
      // return timeArray;
      // alert(JSON.stringify(timeArray));
      this.apiResponse(timeArray,date);
      
    }
apiResponse=(timeArray,date)=>{//use to get the booked slotes
      var obj={venue_id:this.props.bookdata.venue_id,date:date};
      console.log(obj);
            fetch(links.APIURL+'bookedSlots', {
     method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(obj),
}).then((response)=>response.json())
   .then((responseJson)=>{
    console.log(responseJson);
    if(responseJson.status==0){
      this.processBookedSlots(responseJson.response,timeArray);
    }else{
      this.processBookedSlots([],timeArray);
    }
 })
       
      
    }
    compinedTime(start, end){
      return Array((end-1) - start + 1).fill().map((_, idx) => start + idx)

    }
    getHours(date){
      var d=jsCoreDateCreator(date);
      // alert(d.getHours())
      return d.getHours();

    }
    renderAvailabilty=(data)=>{
      if(data){

      // this.setState({availability:data});
        if(data==1){
          return "HOURLY";
        }else if(data==2){
          return "DAILY";
        }else if(data==3){
          return "WEEKLY";
        }else{
          return "MONTHLY";
        }
      }else{

      }
    }
    processBookedSlots=(response,timeArray)=>{
  // alert(JSON.stringify(timeArray));
      var bookedSlots=[];

      for(var i=0 ; i < response.length ; i++){
        var fromHour = this.getHours(response[i].trn_booking_from_date_time);
        var toHour = this.getHours(response[i].trn_booking_to_date_time);
        // console.log(this.compinedTime(fromHour, toHour))
        bookedSlots=bookedSlots.concat(this.compinedTime(fromHour, toHour)).unique();

      }
      console.log("totalTime",timeArray)
      console.log("bookedTime",bookedSlots);

      for(var i in bookedSlots){

        timeArray.map((item)=>{
          
          if(item.hours == bookedSlots[i]){
            item.booked=true; 
          }
        });
      }

      console.log("finalArray",timeArray)
      var listTime=timeArray?timeArray:[];
      this.setState({listTime});
}

   setDateHours(date,h){
  console.log("date",date)
  console.log(h);
var hourDate=date;
  if(hourDate){
      hourDate.setHours(h);
      hourDate.setMinutes(0);
      hourDate.setSeconds(0);
      console.log("new Date()",hourDate.getTime());
      return hourDate.getTime();
  }
}

   componentDidMount(){
    var datefrom=this.props.bookdata.availability.length>0 ?this.props.bookdata.availability[0].trn_venue_avail_frm:null; 
    var dateto=this.props.bookdata.availability.length>0 ?this.props.bookdata.availability[0].trn_venue_avail_to:null;
    var status=this.props.bookdata.availability.length>0?this.props.bookdata.availability[0].trn_availability_type:null;
    // this.setS
    this.setState({priceamount:this.props.bookdata.price.length>0?this.props.bookdata.price[0].trn_venue_price_amt:0})
    this.loadBookingDates(this.props.bookdata.venue_id);
this.setState({availability:status});
this.setState({maxDate:dateto});
    if(datefrom){  
    // var mindate=()?datefrom:new Date();    
    // this.setState({minDate:mindate});
var fromdate=jsCoreDateCreator(datefrom);
var todate= jsCoreDateCreator(dateto);
var mindate=new Date(fromdate).getTime()>=new Date().getTime()?datefrom:new Date();
var datainfo=this.getDate(fromdate,todate);
    this.setState({dates:datainfo,minDate:mindate});

// alert(JSON.stringify(datainfo));
    }

    // 
    // alert(JSON.stringify(datainfo));
   }
   BookNow=()=>{
     // alert(JSON.stringify(this.state.loginDetails));
//      var booking
//   if(!this.state.avaialableSLot){
//     // alert("please choose your avaialble slots before booking.");
// return;
//   }
  console.log(this.state.bookdetails);
  var bookdetails=this.state.bookdetails;
  var LoginData=this.state.loginDetails;

  bookdetails.user_id=LoginData.user_id;
  bookdetails.venue_id=this.props.bookdata.venue_id;
  // alert(this.state.frmtime);
  bookdetails.bookingFrom=this.state.frmtime;
  bookdetails.bookingTo=this.state.totime;
  if(!bookdetails.bookingFrom || !bookdetails.bookingTo){
     alert("please choose your available slots before booking.");
return;
}else{
 bookdetails.bookingFrom=dateFormat(this.state.frmtime,'yyyy-mm-dd HH:00:00');
  bookdetails.bookingTo=dateFormat(this.state.totime,'yyyy-mm-dd HH:00:00');
}
  // bookdetails.bookingFrom=dateFormat(new Date(this.state.avaialableSLot.fromTime),'yyyy-mm-dd HH:00:00');
  // bookdetails.bookingTo=dateFormat(new Date(this.state.avaialableSLot.toTime),'yyyy-mm-dd HH:00:00');
  bookdetails.type=this.state.availability;
  bookdetails.finalPrice=this.props.bookdata.price[0].trn_venue_price_amt;
  bookdetails.trn_venue_price_amt=this.props.bookdata.price[0].trn_venue_price_amt;
  // alert(JSON.stringify(bookdetails))
  bookdetails.venudate=dateFormat(this.state.frmtime,'hh') +" - "+ dateFormat(this.state.totime,'hh TT') +","+ dateFormat(this.state.frmtime,'dd mmm yyyy')
  console.log("finalbookdetailsresponse",bookdetails);
    // this.props.booksuccess(this.props.bookdata,bookdetails)
        fetch(links.APIURL+'venueBooking', {
     method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(bookdetails),
}).then((response)=>response.json())
   .then((responseJson)=>{
    console.log(responseJson);

     if(responseJson.status==0){
       if(this.props.cancelBooking){
      this.props.cancelBooking();
       Actions.stripecheckout({bookingData:this.props.bookdata,bookdetails:bookdetails,priceamount:this.state.priceamount});
       Toast.show('Waiting For Stripe Payment Gateway',Toast.LONG);
    }
    // if(this.props.booksuccess){
    // this.props.booksuccess(this.props.bookdata,bookdetails)
    // }
     }else{
       Toast.show(responseJson.msg,Toast.LONG);

     }
    // alert('Booked Successfully');
    // this.setState({frmtime:null,toTime:null,bookdetails:null});
    // if(this.props.cancelBooking){
    //   // this.props.cancelBooking();
    // }
   })
}
changeDynamicData=(data,key)=>{
 console.log(data);
}
      onStarRatingPress(favStar) {
        this.setState({
            favStarCount: favStar
        });
      }
      loadBookingDates=(venueId)=>{//use to get the booked dates to block inside the calendar
// console.log(venueId);
        fetch(links.APIURL+'getBookedDates', {
     method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({venue_id:venueId}),
}).then((response)=>response.json())
.then((responseJson)=>{
  if(responseJson.status==0){

  BookedDates=responseJson.data;
}else{
  BookedDates=responseJson.data
}
this.setState({BookedDates:BookedDates});
  // console.log(responseJson);
  // this.setDate({bookedDates})

})
}
getNumberOfDays=(from,to)=>{
  const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
const firstDate = new Date(from);
const secondDate = new Date(to);

const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
return diffDays+1;
}

receiveDates=(dates)=>{
  this.setState({frmtime:dates.fromTime,totime:dates.toTime})
   var getnumberofdays=this.getNumberOfDays(dates.fromTime,dates.toTime);
  if(this.state.availability){
    if(this.state.availability==3){
   var amount=parseInt(this.props.bookdata.price[0].trn_venue_price_amt)*Math.floor(getnumberofdays/7);
      this.setState({priceamount:Math.floor(amount).toString()})
    }else if(this.state.availability==4){
      var amount=parseInt(this.props.bookdata.price[0].trn_venue_price_amt)*Math.floor(getnumberofdays/30);
      this.setState({priceamount:Math.floor(amount).toString()})
    }
  }
}
    render(){
      console.log('this.state,',this.state.priceamount)
      // alert(JSON.stringify(this.props));
      // alert(this.state.enableRangePicker);
      var bookdata=this.props.bookdata;
        return(
           <Container style={{height:hp('85%')}}>
            <Content style={{padding:15}}>
            {this.state.enableRangePicker&&
            <RangePicker selectedDate={(dates)=>this.receiveDates(dates)} minDate={this.state.minDate} maxDate={this.state.maxDate} blockDates={this.state.BookedDates} type={this.state.availability} closeRangePicker={()=>this.setState({enableRangePicker:null})}/>
          }
            <View style={[styles.container,{ backgroundColor:color.white,}]}> 
        
                <View style={styles.flex_row}>
                    <Text style={styles.text11}>Hello</Text>
                    <Text style={styles.text1}>{this.state.loginDetails&&this.state.loginDetails.user_name}</Text>
                    <Text style={styles.text1}>.</Text>
                </View>
                <View style={styles.view2}>
                    <Text style={styles.text2}>Book your Venue.</Text><Text style={styles.text2}>|.</Text>
                    <View style={styles.view3}><TouchableOpacity onPress={()=>this.props.cancelBooking&&this.props.cancelBooking()}><Text style={styles.text7}>Change Venue</Text></TouchableOpacity></View>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:8}}>
                        <View style={styles.view4}><Text style={styles.text4}>{bookdata&&bookdata.trn_venue_name?bookdata.trn_venue_name:'loading..'}</Text></View>
                        <Text style={{color: '#3f51b5',paddingBottom:5,fontSize:hp('1.8%')}}
      onPress={() => Linking.openURL('https://corporate.ivneu.com/cancellation-policy/')}>
  Cancellation Policy
</Text>
                        {/*<View style={styles.view5}><Text style={styles.text5}>Add to Cart</Text></View>*/}
                    </View>
                   
                </View>
                <View>
                    <Line/>
                    <View style={styles.view6}>
                        <View style={{flex:2}}>
                            <StarRating
                                style={styles.ratingStyle}
                                maxStars={5}
                                starSize={15}
                                emptyStarColor={color.ash6}
                                halfStarColor={color.blue}
                                fullStarColor={color.blue}
                                rating={this.state.ratingCount}
                                selectedStar={(rating) => this.onStarRatingPress(rating)}/>
                        </View>
                        <View style={styles.view7}>
                            <Text style={styles.orange_text}>{bookdata&&bookdata.Distance&&bookdata.Distance}</Text>
                            <Text style={styles.orange_text}>|</Text>
                            <Text style={styles.orange_text}>{bookdata&&bookdata.Time&&bookdata.Time}</Text>
                        </View>
                        <View style={styles.view8}>
                        {this.state.availability&&this.state.availability==1&&
                            <Button  onLayout={this.onLayout} style={styles.blue_btn} onPress={()=>this._onPress()}> 
                                <Text style={styles.btn_txt}>Show Available Slots</Text>
                            </Button>
                          }
                          {this.state.availability&&this.state.availability!=1&&
                            <Button  onLayout={this.onLayout} style={styles.blue_btn} onPress={()=>this.setState({enableRangePicker:true})}> 
                                <Text style={styles.btn_txt}>Choose The Dates</Text>
                            </Button>
                          }
                        </View>
                    </View>
                    <Line/>
                </View>
                <View style={{flexDirection:'row',flex:1,paddingTop:5}}>
                   
                    <View style={{flex:1,justifyContent:'center'}}>
                        <View style={styles.view9}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>Name</Text></View>
                            <View style={{flex:.6}}><Text_box changeText={(e)=>this.changeDynamicData(e,'name')} _height={hp('5%')} _minwidth={'100%'}></Text_box></View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>When</Text></View>
                            <View style={{flex:.6}}>
                            <Dropdown_conatiner noarrow={true} _borwidth={1} _borColor={color.ash6}  
                                         value={this.renderAvailabilty(bookdata&&bookdata.availability.length>0&&bookdata.availability[0].trn_availability_type)}
                                        sendDropdownData={(data)=>this.sendDropdownData(data,'hourlydata')}
                                        activestate={true} values={[]} fullwidth={true} _height={hp('5%')}
                                    />
                            </View>
                        </View>
                       
                        <View style={[styles.view9,{flex:1}]}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>From</Text></View>
                                <View style={{flex:.6,height:'100%',justifyContent:'center',alignItems:'center'}}>
                                    <DatePicker
                                    disabled={true}
                                       style={{width: '100%',height:hp('5%'),textAlign:'left',fontSize:hp('1.5%')}}
                                       date={this.state.frmtime&&this.state.frmtime}
                                       placeholder="Select date"
                                       format="YYYY-MM-DD hh:mm a"
                                       confirmBtnText="Confirm"
                                       cancelBtnText="Cancel"
                                       showIcon={false}
                                       customStyles={{
                                           dateIcon: {
                                             position: 'absolute',
                                             right: 0,
                                             top: 0
                                           },
                                          
                                           dateInput: {
                                             borderWidth:1,
                                             marginLeft: 0,
                                             paddingLeft:8,
                                             textAlign:'left',
                                             alignItems:'flex-start',height:hp('5%'),fontSize:hp('1.5%')
                                           }
                                         
                                         }}
                                         onDateChange={(date) => this.setDate(date,'from')}
                                   />             
                                    <View style={styles.dateImgConta}>
                                            <Image source={Calender} style={{width:hp('1.5%'),height:hp('1.5%'), position: 'absolute',
                                                     right: 0,top: 0}}
                                            />
                                    </View>           
                               </View>
                            </View>
                        <View style={[styles.view9,{flex:1}]}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>To</Text></View>
                            <View style={{flex:.6,height:'100%',justifyContent:'center',alignItems:'center'}}>
                                    <DatePicker
                                    disabled={true}
                                       style={{width: '100%',height:hp('5%'),textAlign:'left',fontSize:hp('1.5%')}}
                                       date={this.state.totime&&this.state.totime}
                                       mode="date"
                                       placeholder="Select date"
                                       format="YYYY-MM-DD hh:mm a"
                                       confirmBtnText="Confirm"
                                       cancelBtnText="Cancel"
                                       showIcon={false}
                                       customStyles={{
                                           dateIcon: {
                                             position: 'absolute',
                                             right: 0,
                                             top: 0
                                           },
                                           dateInput: {
                                             borderWidth:1,
                                             marginLeft: 0,
                                             paddingLeft:8,
                                             textAlign:'left',
                                             alignItems:'flex-start',height:hp('5%'),fontSize:hp('1.5%')
                                           }
                                         
                                         }}
                                        
                                         onDateChange={(date) => this.setDate(date,'to')}
                                   />             
                                    <View style={styles.dateImgConta}>
                                            <Image source={Calender} style={{width:hp('1.5%'),height:hp('1.5%'), position: 'absolute',
                                                     right: 0,top: 0}}/>
                                    </View>           
                               </View>
                        </View>
                      
                        <View style={[styles.view9,{paddingTop:5}]}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>Price</Text></View>
                            <View style={{flex:.6,flexDirection:'row'}}>
                                <View style={{flex:1}}><Text_box noedit={true} value={this.state.priceamount} _height={hp('5%')} _minwidth={'100%'}></Text_box></View>
                                <View style={styles.view10}><TouchableOpacity><Text style={styles.text7}>Use PROMO Code</Text></TouchableOpacity></View></View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>Address</Text></View>
                            <View style={{flex:.6}}><Text_box _height={hp('5%')} _minwidth={'100%'}></Text_box></View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>Mobile</Text></View>
                            <View style={{flex:.6}}><Text_box _height={hp('5%')} maxLength={10} keyboardType={'numeric'}  _minwidth={'100%'}></Text_box></View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4,justifyContent:'center'}}><Text>More Info</Text></View>
                            <View style={{flex:.6}}><Text_box _height={hp('5%')} _minwidth={'100%'}></Text_box></View>
                        </View>
                    </View>  
                 
                </View>

                <View style={styles.view11}>
                    <View style={styles.view12}>
                        <Button  onPress={()=>this.props.cancelBooking&&this.props.cancelBooking()} style={styles.cancel_btn}>
                            <Text style={styles.cancel_txt}> CANCEL </Text>
                        </Button>
                    </View>
                    <View style={styles.view13}>
                        <Button onPress={this.BookNow} style={styles.book_nw_btn}>
                            <Text style={styles.cancel_txt}> BOOK NOW </Text>
                        </Button>
                </View>
                    
                    </View>
                {    this.state.isHidden ?
                <View style={{flex:1,backgroundColor:color.white,
                  maxHeight:hp('50%'),
                  width:wp('80%'),
                           bottom:this.state.newBottom,
                            borderWidth:0.5,borderColor:color.black1,
                            position:'absolute',top:this.state.newTop+hp('15%'),right:5}}> 
                    <Line/>
                   <View style={{marginTop:5,marginBottom:5}}>
                     <View style={{flexDirection:'row'}}>
                       <View style={{flex:1}}><Text style={{marginLeft:5,marginBottom:5,fontSize:hp('3%'),fontWeight:'bold'}}>Time Slots</Text></View>
                        <View style={{flex:1}}>
                            <TouchableOpacity onPress={()=>this._onPress2()} style={{justifyContent:'center',padding:5}} ><Text style={{color:color.orange,alignSelf:'flex-end'}}>Cancel</Text></TouchableOpacity>
                        </View>
                     </View>

                       <Line/>
                   </View>
                   <View style={{padding:12}}>
                        <ScrollView horizontal={true}  showsHorizontalScrollIndicator={false}>
                            {this.state.dates&&this.state.dates.length>0&&this.state.dates.map((obj,key)=>{
                                return(
                                    <TouchableOpacity onPress={this.SampleFunction.bind(this, obj)}>
                                        <View style={[{borderWidth:1,borderColor:color.black1,flexDirection:'row',paddingTop:8,paddingBottom:8,
                                        paddingLeft:10,paddingRight:10,marginRight:5,backgroundColor:this.props._activeColor},]}>
                                            <Text style={{fontSize:hp('2%')}}> {obj.altdate} </Text>
                                           
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                        {this.state.listTime.length==0 &&
                            <Text style={{alignSelf:'center',justifyContent:'center',alignContent:'center'}}>
                                No Slot Available
                            </Text>
                        }
                         {this.state.listTime.length>0 &&
                        <ScrollView style={{height:'70%',backgroundColor:color.ash}} showsVerticalScrollIndicator={false}>
                                {this.state.listTime&&this.state.listTime.map((obj,key)=>{
                                    return(
                                        <View>
                                        {(obj.booked && obj.booked==true) ?
                                           
                                          <TouchableOpacity activeOpacity={1} >
                                            
                                            <View style={[{borderWidth:1,borderColor:color.black1,flexDirection:'row',paddingTop:8,paddingBottom:8,
                                                paddingLeft:10,paddingRight:10,margin:5,backgroundColor:color.blueactive},]}>
                                                                                          
                                                <View style={{flex:.7,}}><Text style={{fontSize:hp('2%'),color:color.white}}> 
                                                {obj.availableTime} </Text></View> 
                                               
                                                <View style={{flex:.3}}><Text numberOfLines={1} style={{color:color.white,justifyContent:'flex-end',alignItems:'flex-end',alignContent:'flex-end',alignSelf:'flex-end'}}>Booked</Text></View>  
                                               
                                            </View>
                                            </TouchableOpacity>  
                                          

                                          :
                                                  
                                        <TouchableOpacity activeOpacity={0.2} onPress={()=>this.getslottime(obj)}>
                                            
                                        <View style={[{borderWidth:1,borderColor:color.black1,flexDirection:'row',paddingTop:8,paddingBottom:8,
                                            paddingLeft:10,paddingRight:10,margin:5,backgroundColor:color.white},]}>
                                                                                      
                                            <View style={{flex:.7}}><Text style={{fontSize:hp('2%')}}> 
                                            {obj.availableTime} </Text></View> 
                                           
                                            <View style={{flex:.3}}><Text style={{color:color.blue,alignSelf:'flex-end',alignItems:'flex-end',alignContent:'flex-end'}}>Select</Text></View>  
                                           
                                        </View>
                                         
                                        </TouchableOpacity>
                                            
                                          }  
                                      </View>
                                   )                                  
                                })}
                            </ScrollView>
                          }
                   </View>

                   <View style={{width:'100%',backgroundColor:color.ash1,marginTop:10}}>
                   
                       
                        <View style={{backgroundColor:color.ash}}>
                            
                         </View>   
                      
                   </View>
                </View>
                :null
                }
            </View>
            </Content>
            </Container>
         )
    }
} 
const styles=StyleSheet.create({
    container:{
        flex:1,
        paddingLeft:8,
        paddingRight:8
    },
    _flex:{
        flex:1
    },
    flex_row:{
        flexDirection:'row'
    },
    text11:{
        fontSize:hp('1.5%')
    },
    text1:{
        fontSize:hp('1.5%'),
        paddingLeft:2,
        fontWeight:'bold'
    },
    text2:{
        color:color.orange,
        fontSize:hp('2.5%')
    },
    text3:{
        paddingLeft:2,
        color:color.blue,
        
    },
    view2:{ 
        flexDirection:'row',
        paddingTop:5
    },
    view3:{
        paddingBottom:2,
        flexDirection:'column',
        justifyContent:'flex-end'
    },
    view4:{
        paddingTop:8
    }, 
    text4:{
        fontSize:hp('3.5%')
    },
    view5:{
        paddingBottom:5
    },
    text5:{
        fontSize:hp('2%'),
        color:color.ash6
    },
    view6:{
        flexDirection:'row',
        paddingTop:8,
        paddingBottom:8
    },
    ratingStyle:{
        justifyContent:'flex-start'

    },
    orange_text:{
        color:color.orange,
        fontSize:hp('1.5%')
    },
    view7:{
        flex:3,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        alignSelf:'center'
    },
    view8:{
        flex:4.5,
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignContent:'flex-end',
        alignSelf:'flex-end'
    },
    blue_btn:{
        // backgroundColor:color.blue,
        // height:hp('3%'),
        // paddingLeft:8,
        // paddingRight:8,
      
        // paddingBottom:5,
        // alignItems:'center'
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.blue,
        height:hp('3%'),
        borderRadius:3,
        paddingTop:8,
        paddingBottom:10

    },
    btn_txt:{
        color:color.white,
        fontSize:hp('1.5%'),


    },
    view9:{
        flexDirection:'row',
        paddingTop:5,
        paddingBottom:5
    },
    text7:{
        paddingLeft:2,
        fontSize:hp('1.5%'),
        color:color.skyblue 
    },
    view10:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        alignSelf:'center'
    },
    view11:{
        flexDirection:'row',
        paddingTop:15,
        paddingBottom:15
    },
    view12:{
        flex:1,
        justifyContent:'flex-start',
        flexDirection:'row'
    },
    view13:{
        flex:1,
        justifyContent:'flex-end',
        flexDirection:'row'
    },
    cancel_txt:{
        fontSize:hp('2.3%'),
        color:color.white
    },
    cancel_btn:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.blue,
        height:hp('6%'),
        borderRadius:3,
    },
    book_nw_btn:{
        backgroundColor:color.orange,
        paddingLeft:10,
        paddingRight:10,
         height:hp('6%'),
        borderRadius:3,
    },
    cancel_txt:{
        fontSize:hp('2.3%'),
        color:color.white
    },
    
       dateContainer:{flex:1,flexDirection:'row',borderWidth:1,borderColor:color.ash6,width:'100%',height:hp('4%'),
        alignItems:'center'},
    dateImgConta:{position:'absolute',right:3,alignItems:'center',justifyContent:'center'},
    active:{
        backgroundColor:color.orange
    }
})