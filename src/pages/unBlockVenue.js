import React, { Component } from "react";
import { View, StyleSheet, Alert, ActivityIndicator } from "react-native";
import ModalComp from "../components/ModalComp";
import { Button,Text } from "native-base";
import color from "../Helpers/color";
import PropTypes  from 'prop-types';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import LabelTextbox from "../components/labeltextbox";
import ValidationLibrary from "../Helpers/validationfunction";
import Toast from "react-native-simple-toast"; import links from "../Helpers/config";


const NameStar = ({ name }) => (
  <React.Fragment>
    <Text>
      {name} {""}
    </Text>{" "}
    <Text style={{ color: "red" }}>*</Text>
  </React.Fragment>
);
var formkeys = [
  {
    name: "reason",
    type: "default",
    labeltype: "textarea",
    label: <NameStar name="Reason" />
  }];


export default class UnBlockVenue extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                     isPromo: false,
                     promo_code: "",
                     reason: "",
                     showloading:false,
                     validations: {
                       reason: {
                         error: true,
                         mandatory: true,
                         errormsg: "",
                         validations: [{ name: "required", status: false }]
                       }
                    }

                   };
                 }

                 componentWillReceiveProps(nextProps) {
                   // Any time props.email changes, update state.
                   if (nextProps.isPromoShow !== this.state.isPromo) {
                     this.setState({
                       isPromo: nextProps.isPromoShow
                     });
                   }
                 }

                 _hitBlock = (type,data) => { 

                     var err = 0;

                     for (const key of Object.keys(this.state.validations)) {
                       if (
                         this.state.validations[key].error != false &&
                         this.state.validations[key].mandatory == true
                       )
                         err += 1;
                     }

                     console.log("error list", err);

                     if (err > 0) {
                       Toast.show("Field's Missing !!", Toast.LONG);
                       return;
                     } 
  
                    var obj = {
                      bookingId:data.trn_booking_id,
                      bookingSlotType:type,
                      reason:this.state.reason,
                      bookingType:1,
                      mailId:data.trn_booking_email,
                      refundAmount:20
                    }; 
var msg = type == 2 ? "Block" : "Cancel";

                    Alert.alert(
                      "Are you sure want to " + msg,
                      "",
                      [
                        {
                          text: "Cancel",
                          onPress: () => console.log("Cancel Pressed"),
                          style: "cancel"
                        },
                        {
                          text: "OK",
                          onPress: () => {
                            this.setState({ showloading: true });
                            fetch(links.APIURL + "unblockingSlots", {
                              method: "POST",
                              headers: {
                                Accept: "application/json",
                                "Content-Type": "application/json"
                              },
                              body: JSON.stringify(obj)
                            })
                              .then(response => response.json())
                              .then(responsejson => {
                                this.setState({ showloading: false });
                                if (responsejson.status == 0) {
                                  Alert.alert("Slots Updated !!");
                                  this.setState({ reason: "" });
                                  this.props.refresh();
                                } else {
                                  Toast.show(
                                    "Something Went Wrong !!",
                                    Toast.LONG
                                  );
                                }
                              });
                          }
                        }
                      ],
                      { cancelable: false }
                    );


                  
                      

                   
                 };

                 _renderForm() {
                   return (
                     <View
                       style={{
                         backgroundColor: "white",
                         paddingVertical: 10,
                       }}
                     >
                       {formkeys.map((obj, key) => {
                         return (
                           <View style={{ marginBottom: 15 }}>
                             <LabelTextbox
                               inputtype={obj.type}
                               key={key}
                               type={obj.labeltype}
                               capitalize={obj.name != "dob"}
                               error={this.state.validations[obj.name].error}
                               errormsg={
                                 this.state.validations[obj.name].errormsg
                               }
                               changeText={data =>
                                 this.changeText(data, obj.name)
                               }
                               value={this.state[obj.name]}
                               labelname={obj.label}
                             />
                           </View>
                         );
                       })}
                     </View>
                   );
                 }

                 changeText = (data, key) => {
                   // alert(data);
                   var errormsg = ValidationLibrary.checkValidation(
                     data,
                     this.state.validations[key].validations
                   );
                   var validations = this.state.validations;
                   if (key == "dob") {
                     if (data == "errordob") {
                       validations[key].error = true;
                       validations[key].errormsg =
                         "Invalid Date Accept Format (dd-mm-yyyy)";
                     } else {
                       validations[key].error = false;
                     }
                   } else if (key == "mobile") {
                     errormsg = ValidationLibrary.checkValidation(
                       data.mobile,
                       this.state.validations[key].validations
                     );
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   } else {
                     validations[key].error = !errormsg.state;
                     validations[key].errormsg = errormsg.msg;
                   }
                   this.setState({ validations });
                   this.setState({
                     [key]: data
                   });
                 };

                 render() {
                   const { data } = this.props;
                   const { isPromo } = this.state;


                     if (this.state.showloading) {
                     return (
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "center",
                           alignItems: "center",
                           paddingVertical: 20,
                         }}
                       >
                         <ActivityIndicator size="large" color="#073cb2" />
                         <Text>Please Wait</Text>
                       </View>
                     );
                   }


                   return (
                     <React.Fragment>
                       <ModalComp
                         titlecolor={color.orange}
                         visible={isPromo}
                         closemodal={() => this.props.closePressed()}
                       >
                         <View
                           style={{
                             marginTop: 50,
                             flex: 1,
                             paddingHorizontal: 10
                           }}
                         >
                           {this._renderForm()}

                           <View style={{display: 'flex',flexDirection: 'row',
                           justifyContent:'space-evenly',paddingVertical: 20}}>

                             <Button
                               ordered
                               large
                               info
                               onPress={() => this._hitBlock(2,data)}
                             >
                               <Text>Block</Text>
                             </Button>

                             <Button
                               ordered
                               large
                               primary
                               onPress={() => this._hitBlock(3,data)}
                             >
                               <Text>Make It Available</Text>
                             </Button>
                           </View>
                         </View>
                       </ModalComp>
                     </React.Fragment>
                   );
                 }
               } 



UnBlockVenue.propTypes = {
  isPromoShow: PropTypes.bool.isRequired,
  closePressed:PropTypes.func.isRequired,
  data:PropTypes.array,
}; 

UnBlockVenue.defaultProps = {
  isPromoShow: false,
  data:[]
}; 



const styles = StyleSheet.create({
  list: {},
  actionbtntxt: {
    alignSelf: "center",
    textAlign: "center",
    color: color.white,
    fontSize: hp("2.3%")
  },
  sociallogin: {
    backgroundColor: color.ash1,
    height: hp("7%"),
    alignItems: "stretch",
    justifyContent: "center"
  }
});
