import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  SafeAreaView,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  Linking,
  WebView
} from "react-native";
import dateFormat from 'dateformat';
import AsyncStorage from '@react-native-community/async-storage';
import TabLoginProcess from '../components/TabLoginProcess';

import Toast from 'react-native-simple-toast';

import { Actions } from "react-native-router-flux";
import Carousel from "react-native-snap-carousel";
import color from '../Helpers/color';
import SeatPaxBox from '../components/SeatPaxBox';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

import links from '../Helpers/config'; 
import {
Accordion,
Container,
Content,
List,
ListItem,
Thumbnail,
Left,
Body,
Right,
Button,
Icon,
Footer,
FooterTab
} from "native-base"; 



 const screenWidth = Dimensions.get("window").width; 

 const dataArray = [
  { title: "Amenities", content: "Lorem ipsum dolor sit amet" }
];

const playgr_list = [
  {
    image: require("../images/shutter/CAROUSEL-02.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/shutter/CAROUSEL-03.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/shutter/CAROUSEL-04.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/shutter/CAROUSEL-05.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/shutter/CAROUSEL-06.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/shutter/CAROUSEL-10-09.jpg"),
    text: "Meadow Crest Playground"
  },
  {
    image: require("../images/shutter/CAROUSEL-10.jpg"),
    text: "Meadow Crest Playground"
  }
];
const jsCoreDateCreator = (dateString) => { 
  // dateString *HAS* to be in this format "YYYY-MM-DD HH:MM:SS"
  let dateParam = dateString.split(/[\s-:]/)  
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString()  
  // alert(dateParam);
  return new Date(...dateParam)  
}
export default class BookDetails extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     data: {},
                     isLoading: true,
                     loginDetails: null,
                     visible: false,
                     textShown: false,
                     lineCount: 0,
                     venue_image_path:null
                   };
                 }

                 async componentWillMount() {
                   const { id } = this.props;

                   const manuallocation = JSON.parse(
                     await AsyncStorage.getItem("chosenlocation")
                   );
                   let lat = manuallocation != null ? manuallocation.lat : null;
                   let lng = manuallocation != null ? manuallocation.lng : null;

                   console.log("rest init");

                   fetch(links.APIURL + "getVenueDetailsById", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       venueId: id,
                       lat: lat,
                       long: lng
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log(responseJson);
                       this.setState({ spinner: false });
                       if (responseJson.length > 0) {
                         // alert(JSON.stringify(responseJson[0].price));
                         this.setState({ NoRecords: false });
                       } else {
                         this.setState({ NoRecords: true });
                       }
                       this.setState({
                         isLoading: false,
                         data:
                           responseJson.length > 0
                             ? JSON.parse(JSON.stringify(responseJson[0]))
                             : []
                       });
                     });
                 }

                 onLayout = e => {
                   const { height } = e.nativeEvent.layout;
                   var count = Math.floor(height / styles.text.lineHeight);
                   // alert(count)
                 };
                 _renderHeader(item, expanded) {
                   return (
                     <View
                       style={{
                         flexDirection: "row",
                         padding: 10,
                         justifyContent: "space-between",
                         alignItems: "center"
                       }}
                     >
                       <Text style={{ fontWeight: "600" }}> Amenities</Text>
                       {expanded ? (
                         <Icon
                           style={{ fontSize: 14 }}
                           type="FontAwesome"
                           name="minus"
                         />
                       ) : (
                         <Icon
                           style={{ fontSize: 14 }}
                           type="FontAwesome"
                           name="plus"
                         />
                       )}
                     </View>
                   );
                 }

                 _renderItem({ item, index }) {
                 
                   return (
                     <View style={[styles.homeBgImage,{backgroundColor:color.ash2}]}>
                       
                       <Image
                         source={{ uri: item.venue_image_path }}
                         style={styles.homeBgImage}
                       /> 
                     </View>
                   );
                 }

                 openGoogleMap = latlong => {
                   Linking.openURL(
                     "https://www.google.com/maps/search/?api=1&query=" +
                       latlong
                   );
                 };

                 _renderContent = () => {
                   console.log(this.state.data);
                   return (
                     <View
                       style={{
                         backgroundColor: "#f1f1f1",
                         padding: 10
                       }}
                     >
                       <View>
                         {this.state.data.ameneties.length > 0 &&
                           this.renderAminities(this.state.data.ameneties)}
                       </View>
                     </View>
                   );
                 };
                 renderPriceType = (venuetype, price) => {
                   if (price.length > 0 && venuetype != 2 && venuetype != 3) {
                     var price = price[0];
                     return (
                       <React.Fragment>
                         {price.hour_cost > 0 && (
                           <TouchableOpacity style={styles.pricetypedata}>
                             <Text style={styles.pricetypedata_txt}>
                               Per Hour {price.hour_cost}{" "}
                               {price.trn_venue_price_currency}
                             </Text>
                           </TouchableOpacity>
                         )}
                         {price.day_cost > 0 && (
                           <TouchableOpacity style={styles.pricetypedata}>
                             <Text style={styles.pricetypedata_txt}>
                               Per Day {price.day_cost}{" "}
                               {price.trn_venue_price_currency}
                             </Text>
                           </TouchableOpacity>
                         )}
                         {price.week_cost > 0 && (
                           <TouchableOpacity style={styles.pricetypedata}>
                             <Text style={styles.pricetypedata_txt}>
                               Per Week {price.week_cost}{" "}
                               {price.trn_venue_price_currency}
                             </Text>
                           </TouchableOpacity>
                         )}
                         {price.month_cost > 0 && (
                           <TouchableOpacity style={styles.pricetypedata}>
                             <Text style={styles.pricetypedata_txt}>
                               Per Month {price.month_cost}{" "}
                               {price.trn_venue_price_currency}
                             </Text>
                           </TouchableOpacity>
                         )}
                       </React.Fragment>
                     );
                   } else {
                     return (
                       <TouchableOpacity style={styles.pricetypedata}>
                         <Text style={styles.pricetypedata_txt}>
                           {price.length} {venuetype == 2 ? "Pax" : "Seat"}{" "}
                           Package
                         </Text>
                       </TouchableOpacity>
                     );
                   }
                 };

                 openMessage=async(item)=>{  

                
                   var checklogindetails = await AsyncStorage.getItem(
                     "loginDetails"
                   );
                   if (checklogindetails != null) {
                     const loginDetails = JSON.parse(
                       await AsyncStorage.getItem("loginDetails") 

                     );
                       var data = item;
                   data.venue_image_path=this.state.data.photos[0].venue_image_path;
                   Actions.push("SingleChat", { data: data });

                 
                     }else{
                       this.setState({ visible: true });
                     }
                 }

                 componentWillReceiveProps(props) {
                   console.log(props);
                 }
                 generateVenueType = (type, availtype) => {
                   if (type == 2) {
                     return "Pax";
                   } else if (type == 3) {
                     return "Seat";
                   } else if ((!type || type == 1) && availtype == 1) {
                     return "Hour";
                   } else if ((!type || type == 1) && availtype == 2) {
                     return "Day";
                   } else if ((!type || type == 1) && availtype == 3) {
                     return "Week";
                   } else {
                     return "Month";
                   }
                 };

                 goToReceipt = async (availtype, type, venueId) => {
                   // return;
                   const manuallocation = JSON.parse(
                     await AsyncStorage.getItem("chosenlocation")
                   );
                   let lat = manuallocation != null ? manuallocation.lat : null;
                   let lng = manuallocation != null ? manuallocation.lng : null;
                   var checklogindetails = await AsyncStorage.getItem(
                     "loginDetails"
                   );
                   if (checklogindetails != null) {
                     const loginDetails = JSON.parse(
                       await AsyncStorage.getItem("loginDetails")
                     );
                     Actions.checkoutpage({
                       userId: loginDetails.user_id,
                       venueId: venueId
                     });
                   } else {
                     this.setState({ visible: true });
                   }
                   // Actions.checkoutpage();
                 };
                 sendpaxSeatDetails = async (paxseatid, data) => {
                   var checklogindetails = await AsyncStorage.getItem(
                     "loginDetails"
                   );
                   if (checklogindetails != null) {
                     const loginDetails = JSON.parse(
                       await AsyncStorage.getItem("loginDetails")
                     );
                     Actions.checkoutpage({
                       userId: loginDetails.user_id,
                       venueId: data.venue_id,
                       paxseatid: paxseatid,
                       venuename: data.trn_venue_name
                     });
                   } else {
                     this.setState({ visible: true });
                   }
                 };
                 receivelogindet = data => {
                   this.setState({ loginDetails: data }, function() {
                     this.setState({ visible: null });
                   });
                 };

                 toggleNumberOfLines = index => {
                   this.setState({
                     textShown: this.state.textShown === index ? -1 : index
                   });
                 };

                 renderAminities(item) {
                   return item.map((item, i) => {
                     return (
                       <React.Fragment>
                         <View
                           style={{ flexDirection: "row", paddingVertical: 10 }}
                         >
                           <View
                             style={{
                               height: 40,
                               width: 40,
                               borderRadius: 5,
                               borderWidth: 1,
                               borderColor: color.blue,
                               backgroundColor: color.blue,
                               justifyContent: "center",
                               alignItems: "center",
                               flexDirection: "row"
                             }}
                           >
                             <Image
                               source={{
                                 uri: item.amenities_icon
                               }}
                               tintColor={color.white}
                               style={{ height: 40, width: 40 }}
                             />
                           </View>
                           <View style={{ alignSelf: "center" }}>
                             <Text style={{ textAlign: "center" }}>
                               {" "}
                               {item.amenities_name}
                             </Text>
                           </View>
                         </View>
                         {item.amnDetails.length > 0 && (
                           <View style={{ flexDirection: "row" }}>
                             {this.renderSubAmenities(item.amnDetails)}
                           </View>
                         )}
                       </React.Fragment>
                     );
                   });
                 }

                 renderSubAmenities = data => {
                   return data.map((list, j) => {
                     return (
                       <View>
                         <Text>
                           {list.amenities_det_name}{" "}
                           {data.length != j + 1 ? "/" : ""}{" "}
                         </Text>
                       </View>
                     );
                   });
                 };

                 render() {
                   const { data, isLoading, NoRecords } = this.state;
                   console.log("imagsgs", data.photos);

                   if (isLoading) {
                     return (
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "center",
                           alignItems: "center"
                         }}
                       >
                         <ActivityIndicator size="large" color="#073cb2" />
                         <Text>Please Wait</Text>
                       </View>
                     );
                   }
                   if (NoRecords == true) {
                     return (
                       <View
                         style={{
                           flex: 1,
                           justifyContent: "center",
                           alignItems: "center"
                         }}
                       >
                         <Text>No Records Found</Text>
                       </View>
                     );
                   }
                   var minDate = dateFormat(
                     jsCoreDateCreator(
                       data.availability[0].trn_venue_avail_frm
                     ),
                     `dd"th" mmm yyyy`
                   );
                   var maxDate = dateFormat(
                     jsCoreDateCreator(data.availability[0].trn_venue_avail_to),
                     `dd"th" mmm yyyy`
                   );
                   var hoursfrom =
                     dateFormat(
                       jsCoreDateCreator(
                         data.availability[0].trn_venue_avail_frm
                       ),
                       `hh:MM tt`
                     ) + "";
                   var hoursto =
                     dateFormat(
                       jsCoreDateCreator(
                         data.availability[0].trn_venue_avail_to
                       ),
                       `hh:MM tt`
                     ) + "";
                   return (
                     <Container style={styles.container}>
                       {this.state.visible == true && (
                         <TabLoginProcess
                           loginttype={"Book Your Venue"}
                           sendlogindet={data => this.receivelogindet(data)}
                           type="login"
                           visible={true}
                           tabAction={this.tabAction}
                           closetabmodal={() =>
                             this.setState({ visible: false })
                           }
                         />
                       )}
                       <Content>
                         <SafeAreaView style={{ flex: 1 }}>
                           <ScrollView
                             nestedScrollEnabled={true}
                             scrollEnabled={true}
                           >
                             <View>
                               <Carousel
                                 ref={c => {
                                   this._carousel = c;
                                 }}
                                 data={data.photos}
                                 firstItem={0}
                                 autoplay={true}
                                 loop={true}
                                 contentContainerCustomStyle={{ flexGrow: 0 }}
                                 renderItem={this._renderItem}
                                 sliderWidth={screenWidth}
                                 itemWidth={screenWidth}
                                 itemHeight={150}
                                 inactiveSlideScale={1}
                               />
                             </View>

                             <View>
                               <View
                                 style={{
                                   paddingHorizontal: 15,
                                   paddingVertical: 15,
                                   flexDirection: "row"
                                 }}
                               >
                                 <View style={styles.left}>
                                   <Text
                                     style={{
                                       fontSize: 20,
                                       fontStyle: "bold",
                                       color: color.blue
                                     }}
                                   >
                                     {data.trn_venue_name}
                                   </Text>

                                   <Text style={{ fontSize: 14 }}>
                                     {data.trn_venue_address}{" "}
                                     <Text
                                       onPress={() =>
                                         this.openGoogleMap(
                                           data.trn_venue_location
                                         )
                                       }
                                     >
                                       <Icon
                                         name="map-marker"
                                         type="FontAwesome"
                                         style={{
                                           fontSize: 22,
                                           color: color.blue
                                         }}
                                       />
                                     </Text>
                                   </Text>

                                   <Text style={{ color: color.orange }}>
                                     {data.Distance} | {data.Time}
                                   </Text>
                                   <Text style={{ color: color.orange }}>
                                     Availability : {minDate} - {maxDate}
                                   </Text>
                                   <Text style={{ color: color.orange }}>
                                     Hours : {hoursfrom} - {hoursto}
                                   </Text>
                                 </View>
                               </View>
                               <View
                                 style={{
                                   flexDirection: "row",
                                   paddingHorizontal: 15,
                                   flexWrap: "wrap"
                                 }}
                               >
                                 {this.renderPriceType(
                                   data.trn_venue_type,
                                   data.price
                                 )}

                                 <TouchableOpacity
                                   style={[
                                     styles.pricetypedata,
                                     { backgroundColor: color.blue }
                                   ]}
                                   onPress={() => {
                                     this.openMessage(data);
                                   }}
                                 >
                                   <Text style={styles.pricetypedata_txt}>
                                     Message Host
                                   </Text>
                                 </TouchableOpacity>
                               </View>

                               {(data.trn_venue_type == 3 ||
                                 data.trn_venue_type == 2) &&
                                 data.price &&
                                 data.price.length > 0 && (
                                   <SeatPaxBox
                                     type={data.trn_venue_type}
                                     items={data.price}
                                     sendSeatData={data1 =>
                                       this.sendpaxSeatDetails(
                                         data.trn_venue_type == 3
                                           ? data1.seat_id
                                           : data1.venue_pax_id,
                                         data
                                       )
                                     }
                                   />
                                 )}
                               <View
                                 style={{
                                   paddingHorizontal: 15,
                                   paddingVertical: 10,
                                   backgroundColor: "white"
                                 }}
                               >
                                 <Text
                                   style={{ color: color.orange, fontSize: 16 }}
                                 >
                                   Description
                                 </Text>
                                 {this.state.textShown === false ? (
                                   <View>
                                     <Text
                                       style={{ textAlign: "left" }}
                                       numberOfLines={6}
                                       ellipsizeMode={"clip"}
                                       onTextLayout={({
                                         nativeEvent: { lines }
                                       }) => {
                                         this.setState({
                                           lineCount: lines.length
                                         });
                                       }}
                                     >
                                       {" "}
                                       {data.trn_venue_desc}
                                     </Text>
                                     {this.state.lineCount > 6 && (
                                       <Text
                                         style={{ color: color.blue }}
                                         onPress={() =>
                                           this.setState({ textShown: true })
                                         }
                                       >
                                         Read More
                                       </Text>
                                     )}
                                   </View>
                                 ) : (
                                   <View>
                                     <Text style={{ textAlign: "left" }}>
                                       {" "}
                                       {data.trn_venue_desc}{" "}
                                       <Text
                                         style={{ color: color.blue }}
                                         onPress={() =>
                                           this.setState({ textShown: false })
                                         }
                                       >
                                         {" "}
                                         Show Less
                                       </Text>
                                     </Text>
                                   </View>
                                 )}
                               </View>

                               <View>
                                 <Accordion
                                   dataArray={dataArray}
                                   animation={true}
                                   expanded={0}
                                   renderHeader={this._renderHeader}
                                   renderContent={this._renderContent}
                                 />
                               </View>

                               <View
                                 style={{
                                   paddingHorizontal: 10,
                                   paddingVertical: 10,
                                   backgroundColor: "white",
                                   flexDirection: "row"
                                 }}
                               >
                                 <Text
                                   style={{
                                     color: color.orange,
                                     fontSize: 16,
                                     fontStyle: "500",
                                     textAlign: "center"
                                   }}
                                 >
                                   Reviews{"  "}
                                   <Icon
                                     name="star"
                                     type="FontAwesome"
                                     style={{
                                       fontSize: 16,
                                       color: color.yellow,
                                       textAlign: "center"
                                     }}
                                   />
                                   {"  "}
                                   <Text
                                     style={{
                                       fontWeight: "bold",
                                       color: "black"
                                     }}
                                   >
                                     0{" "}
                                   </Text>
                                   <Text style={{ color: color.black1 }}>
                                     (0 Reviews)
                                   </Text>
                                 </Text>
                               </View>

                               {/* 

                               <View
                               style={{
                                 paddingHorizontal: 15,
                                 paddingVertical: 10,
                                 backgroundColor: "white"
                               }}
                             > 
                               <View style={{ flexDirection: "row" }}>
                                 <Image
                                   source={{
                                     uri:
                                       "https://cdn.iconscout.com/icon/free/png-256/user-avatar-contact-portfolio-personal-portrait-profile-6-5623.png"
                                   }}
                                   style={{
                                     width: 40,
                                     height: 40,
                                     paddingLeft: 1
                                   }}
                                 />

                                 <Text
                                   style={{
                                     color: color.black,
                                     fontSize: 18,
                                     fontStyle: "bold",
                                     textAlign: "center",
                                     alignSelf: "center",
                                     paddingLeft: 10
                                   }}
                                   numberOfLines={3}
                                 >
                                   Rihana {"\n"}
                                   <Text
                                     style={{
                                       color: color.black1,
                                       fontSize: 12
                                     }}
                                   >
                                     Jan 21 2019
                                   </Text>
                                 </Text>
                               </View>

                               <Text
                                 style={{ textAlign: "justify" }}
                                 numberOfLines={4}
                               >
                                 {" "}
                                 Lorem ipsum dolor sit amet, consectetur
                                 adipiscing elit, sed do eiusmod tempor
                                 incididunt ut labore et dolore magna aliqua. Ut
                                 enim ad minim veniam, quis nostrud exercitation
                                 ullamco laboris nisi ut aliquip ex ea commodo
                                 consequat. Duis aute irure dolor in
                                 reprehenderit in voluptate velit esse cillum
                                 dolore eu fugiat nulla pariatur. Excepteur sint
                                 occaecat cupidatat non proident, sunt in culpa
                                 qui officia deserunt mollit anim id est
                                 laborum.
                               </Text>
                             </View>

                               */}
                             </View>
                           </ScrollView>
                         </SafeAreaView>
                       </Content>
                       {data.trn_venue_type != 2 && data.trn_venue_type != 3 && (
                         <Footer>
                           <FooterTab>
                             <Button
                               onPress={() =>
                                 this.goToReceipt(
                                   data.availability[0].trn_availability_type,
                                   data.trn_venue_type,
                                   data.venue_id
                                 )
                               }
                               style={{
                                 justifyContent: "center",
                                 textAlign: "center"
                               }}
                               primary
                             >
                               <Text
                                 style={{ color: "white", textAlign: "center" }}
                               >
                                 Book Now
                               </Text>
                             </Button>
                           </FooterTab>
                         </Footer>
                       )}
                     </Container>
                   );
                 }
               }


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  homeBgImage: {
    width: screenWidth,
    height: hp("30%"),
    resizeMode: "stretch"
  },
  left:{
    flex:1,

  },
  right:{
      display: 'flex',
     justifyContent:'center',
     right:0,
     alignItems: 'flex-start',
  },
  pricetypedata:{
    padding:5,
    borderRadius:5,
    backgroundColor:color.orangeDark,
    marginRight:8,
    marginBottom:8

  },
  pricetypedata_txt:{
    color:color.white,
    fontSize:hp('2%')
  }
});
