'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Text
} from 'react-native';
import ModalComp from '../components/Modal';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Icon} from 'native-base';
 const screenWidth = Dimensions.get('window').width;
class VenueDetailDesc extends Component {
constructor(props) {
  super(props);

  this.state = {activeSlide:0};
}


  render() {
    return (
<ModalComp center  closemodal={()=>this.props.closemodal&&this.props.closemodal()}>
              <View style={{flex:1,marginTop:45,marginBottom:0,padding:12}}>
              <Text style={{fontWeight:'bold',fontSize:hp('3%'),marginTop:-40,width:'80%'}}>{this.props.venuedetails.trn_venue_name}</Text>
                      <Text style={{fontSize:hp('2.2%'),marginTop:10}}> {this.props.venuedetails.trn_venue_desc}</Text>
                           </View>

            </ModalComp>
        
    );
  }
}

const styles = StyleSheet.create({
    homeBgImage:{
        width:screenWidth,
    	height:hp('25%'),
      zIndex: 0
    },
    fontarrowiconRight:{
      position:'absolute',
      top:0,
      bottom:0,
      right:0,
      height:'80%',
      alignItems:'center',
      width:'auto',
      zIndex:1,
      flexDirection:'row',
      paddingLeft:12

    
    }
    ,fontarrowiconLeft:{
       position:'absolute',
      top:0,
      bottom:0,
     height:'80%',
      alignItems:'center',
      width:'auto',
      zIndex:1,
      flexDirection:'row',paddingRight:12
    },
    fontAwesomImage:{
      fontSize:hp('5%'),
      padding:15,
      color:'white'
    }
});


export default VenueDetailDesc;