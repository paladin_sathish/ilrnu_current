import React, {Component} from 'react';
import { BackHandler} from "react-native";
import Home from '../pages/Home'
import Home1 from "../pages/Home1";
import HeaderComp from '../components/HeaderComp'
import {Actions,Router,Scene} from 'react-native-router-flux'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Seacrch_Home from '../pages/Search_Home'
import Venue_search from '../pages/venue_search'
import ListingDetails from '../pages/ListingDetails'
import Stripecheckout from '../pages/stripecheckout'
import Venue_Page  from '../pages/Venue_Page'
import VenueForm from '../pages/venueform';
import Dashboard from '../pages/Dashboard';
import ReferEarn from '../pages/referEarn';
import CorporateForm from '../pages/CorporateForm';
import CorporateVenue from '../pages/CorporateVenue'
import MyVenues from '../pages/MyVenues'
import SideBar from '../components/SideBar'  
import PromoCode from '../pages/promocode'; 
import BookDetails from '../pages/BookDetails';
import CheckoutPageWeb from '../pages/checkoutPageWeb';
import MyCalendar from '../pages/myCalendar';
import MyMessage from "../pages/myMessage"; 
import SingleChat from "../pages/singleChat"; 
import Toast from "react-native-simple-toast";
import TraineePage from "../pages/traineePage";
import TraineeForm from "../pages/traineeForm";  


import CreateCourse from "../pages/createCourse";
import CoursePrice from "../pages/coursePrice";
import CourseDetail from "../pages/courseDetail";
import CourseAddVenue from '../pages/courseAddVenue';
import CourseChooseVenues from "../pages/courseChooseVenues";
import CourseForm from "../pages/courseForm"; 
import CourseMore from "../pages/courseMore";
import CourseReview from '../pages/courseReview';
import MyCourseList from '../pages/myCourseList';
import ShowTrainee from "../pages/showTrainee";
import TrainerSearch from '../pages/trainerSearch';
import Search_Home1 from '../pages/Search_Home1';
import CourseSearch from '../pages/courseSearch';
import Search_Home2 from "../pages/Search_Home2";
import PurchasedCourse from "../pages/purchasedCourse";




{
  /*
   Sathish Compoenent
  */
}
import SlotsBooking  from '../pages/slotsBooking'
import WeekAvailability from "../pages/weekAvailability";
import MonthAvailability  from '../pages/monthAvailability';
import Receipt from '../pages/receipt';
import AddForm from "../pages/addForm";
  let backButtonPressedOnceToExit = false;
const Routes = () => (
  <Router
    navBar={HeaderComp}
    backAndroidHandler={() => {
      console.log(backButtonPressedOnceToExit);
      if (backButtonPressedOnceToExit) {
        BackHandler.exitApp();
      } else {
        if (Actions.currentScene !== "home1") {
          Actions.pop();
          return true;
        } else {
          backButtonPressedOnceToExit = true;
          Toast.show("Press Back Button again to exit", Toast.LENGTH_LONG);
          setTimeout(() => {
            backButtonPressedOnceToExit = false;
          }, 2000);
          return true;
        }
      }
    }}
  >
    <Scene
      key="drawer"
      drawer={true}
      initial={true}
      contentComponent={SideBar}
      drawerPosition={"right"}
      drawerWidth={wp("85%")}
    >
      <Scene key="root">
        <Scene
          key="SlotsBooking"
          component={SlotsBooking}
          title="SlotsBooking"
        />

        <Scene
          key="WeekAvailability"
          component={WeekAvailability}
          title="WeekAvailability"
        />

        <Scene
          key="MonthAvailability"
          title="MonthAvailability"
          component={MonthAvailability}
        />

        <Scene component={MyMessage} title="MyMessage" key="MyMessage" />
        <Scene component={SingleChat} title="SingleChat" key="SingleChat" />

        <Scene
          component={CreateCourse}
          title="createCourse"
          key="createCourse"
        />
        <Scene
          component={CourseReview}
          title="CourseReview"
          key="CourseReview"
        />

        <Scene
          component={CourseAddVenue}
          title="CourseAddVenue"
          key="CourseAddVenue"
        />

        <Scene
          component={CourseChooseVenues}
          title="CourseChooseVenues"
          key="CourseChooseVenues"
        />

        <Scene
          component={CourseDetail}
          title="CourseDetail"
          key="CourseDetail"
        />

        <Scene component={CoursePrice} title="CoursePrice" key="CoursePrice" />

        <Scene
          component={MyCourseList}
          title="MyCourseList"
          key="MyCourseList"
        />

        <Scene component={ShowTrainee} title="ShowTrainee" key="ShowTrainee" />

        <Scene
          component={Search_Home1}
          title="Search_Home1"
          key="Search_Home1"
        />

        <Scene
          component={Search_Home2}
          title="Search_Home2"
          key="Search_Home2"
        />

        <Scene
          component={TrainerSearch}
          title="TrainerSearch"
          key="TrainerSearch"
        />

        <Scene
          component={CourseSearch}
          title="CourseSearch"
          key="CourseSearch"
        />

        <Scene
          component={PurchasedCourse}
          title="PurchasedCourse"
          key="PurchasedCourse"
        />

        <Scene component={CourseMore} title="CourseMore" key="CourseMore" />

        <Scene key="CourseForm" component={CourseForm} title="CourseForm" />

        <Scene key="TraineePage" component={TraineePage} />
        <Scene key="TraineeForm" component={TraineeForm} title="TraineeForm" />

        <Scene component={Receipt} title="Receipt" key="Receipt" />

        <Scene component={AddForm} title="AddForm" key="AddForm" />

        <Scene component={BookDetails} title="BookDetails" key="BookDetails" />

        <Scene component={PromoCode} title="PromoCode" key="PromoCode" />

        <Scene key="home" component={Home} title="Home" />
        <Scene key="home1" component={Home1} title="Home1" initial={true} />
        <Scene key="venueform" component={VenueForm} />
        <Scene key="corporateform" component={CorporateForm} />
        <Scene key="Search_Home" component={Seacrch_Home} />
        <Scene key="Venue_search" component={Venue_search} />
        <Scene key="venuepage" component={Venue_Page} />
        <Scene key="GroundNearYou" component={CorporateVenue} />
        <Scene key="listingdetails" component={ListingDetails} />
        <Scene key="dashboard" component={Dashboard} />
        <Scene key="referearn" component={ReferEarn} />
        <Scene key="myvenues" component={MyVenues} />
        <Scene key="MyCalendar" component={MyCalendar} title="MyCalendar" />
        <Scene key="checkoutpage" component={CheckoutPageWeb} />
        <Scene
          hideNavBar={true}
          key="stripecheckout"
          component={Stripecheckout}
        />
      </Scene>
    </Scene>
  </Router>
);
export default Routes;