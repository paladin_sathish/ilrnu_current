import React from 'react';
import { StyleSheet,View,Text,ImageBackground } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import App from './App';
import AsyncStorage from "@react-native-community/async-storage";

import SplashScreen from 'react-native-splash-screen'

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  image: {
    width: 320,
    height: 320,
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 22,
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  },
});

const slides = [
  {
    key: "somethun1",
    title: "Title 1",
    text: "Description.\nSay something cool",
    image: require("./src/images/slider/SLIDER.jpg"),
    backgroundColor: "#59b2ab"
  },
  {
    key: "somethun-dos2",
    title: "Title 2",
    text: "Other cool stuff",
    image: require("./src/images/slider/SLIDER1.jpg"),
    backgroundColor: "#febe29"
  },
  {
    key: "somethun-dos3",
    title: "Title 2",
    text: "Other cool stuff",
    image: require("./src/images/slider/SLIDER2.jpg"),
    backgroundColor: "#febe29"
  },
  {
    key: "somethun-dos4",
    title: "Title 2",
    text: "Other cool stuff",
    image: require("./src/images/slider/SLIDER3.jpg"),
    backgroundColor: "#febe29"
  },
  {
    key: "somethun-dos5",
    title: "Title 2",
    text: "Other cool stuff",
    image: require("./src/images/slider/SLIDER4.jpg"),
    backgroundColor: "#febe29"
  },
  {
    key: "somethun-dos6",
    title: "Title 2",
    text: "Other cool stuff",
    image: require("./src/images/slider/SLIDER5.jpg"),
    backgroundColor: "#febe29"
  },
  {
    key: "somethun-dos7",
    title: "Title 2",
    text: "Other cool stuff",
    image: require("./src/images/slider/SLIDER6.jpg"),
    backgroundColor: "#febe29"
  }
];

export default class Intro extends React.Component {
                 constructor(props) {
                   super(props);

                   this.state={AppIntroStatus:false}
                  
                 } 

                 componentWillMount = () => {
                     SplashScreen.hide();
                     this.getData();
                 }; 

               

                 

                 _renderItem = ({ item }) => {
                   return (
                     <View
                       style={{
                         flex: 1,
                         justifyContent: "center",
                         alignItems: "center"
                       }}
                     >
                       <ImageBackground
                         source={item.image}
                         style={{ width: "100%", height: "100%" }}
                       ></ImageBackground>
                     </View>
                   );
                 }; 

               

                 _onSkip = () => {
                   AsyncStorage.setItem("AppIntroStatus",'true');
               
                   this.setState({AppIntroStatus:true}) 
                   
                 
                 }; 

                 _onDone = () => {
                   
                   AsyncStorage.setItem("AppIntroStatus", 'true');
                    this.setState({ AppIntroStatus: true });
                
                 }; 



                

                 getData = async () => {
                   try {
                     const value = await AsyncStorage.getItem("AppIntroStatus");
                   
                     if (value === 'true') {
                       this.setState({ AppIntroStatus:true});
                     
                      
                     }
                   } catch (e) {
                     
                   }
                 };

                 render() { 
                
                   if (this.state.AppIntroStatus) {
                     return (
                       <App />
                     );
                   }
                    else {
                     return (
                         <AppIntroSlider
                         renderItem={this._renderItem}
                         slides={slides}
                         onDone={() => this._onDone()}
                         showSkipButton={true}
                         showPrevButton={true}
                         onSkip={() => this._onSkip()}
                       />
                       
                     );
                   }
                 }
               }